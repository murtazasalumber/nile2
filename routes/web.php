<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('date1', function () {
    return view('admin.date');
});
Route::get('/', function () {
    return view('welcome');
});

Route::get('db', function () {
    return view('db');
});

Route::get('version', function () {
    return view('admin.version');
});

Route::get('date', function () {
    return view('admin.datepicker');
});
Route::resource('languages','languagesController');
Route::resource('genders','gendersController');
Route::resource('city','cityController');
Route::resource('leadtypes','leadtypesController');
Route::resource('ptconditions','ptconditionsController');
Route::resource('references','referencesController');
Route::resource('relationships','relationshipsController');
Route::resource('shiftrequireds','shiftrequiredsController');
Route::resource('verticals','verticalsController');
Route::resource('employees','employeesController');

// Route for ccController index and other functions (customer care)
Route::resource('cc','ccController');

// Route for vhController index and other functions (vertical Head)
Route::resource('vh','vhController');

Route::resource('Verticalhead','VerticalheadController');

//Route for making the search filters work in different scenarios
Route::get('lead','vhController@filter');

Route::get('referalpartner','referalpartnerController@index');
Route::get('referalpartnerview','referalpartnerController@show');
Route::get('referalpartner/create','referalpartnerController@create');
Route::POST('referalpartner','referalpartnerController@store');
Route::get('provisionalview','referalpartnerController@view');

Route::resource('product','productController');
Route::POST('addservice','productController@addservice');
Route::GET('pharmacyshow','productController@pharmacyshow');
Route::GET('pharmacyedit','productController@pharmacyedit')->name('pharmacy.edit');

Route::GET('productshow','productController@productshow');

//Route for changing the status of Product details
Route::POST('productstatus','productController@productstatus');
Route::POST('pharmacystatus','productController@pharmacystatus');
Route::GET('fieldofficerview','productController@fieldofficerview');



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('admin/home','AdminController@index');
Route::get('admin/vertical','VerticalController@index');
Route::get('admin/customercare','CustomercareController@index');
Route::get('admin/management','ManagementController@index');
Route::get('admin/careprovider','CareproviderController@index');
Route::get('admin/coordinator','CoordinatorController@index');
Route::get('admin/superuser','SuperuserController@index');
Route::get('admin/referalpartner','referalController@index');
Route::get('admin/productmanager','productmanagerController@index');
Route::get('admin/pharmacymanager','pharmacymanagerController@index');
Route::get('admin/fieldofficer','fieldofficerController@index');
Route::get('admin/fieldexecutive','fieldexecutiveController@index');
Route::get('admin/marketing','marketingController@index');


// Multirole Route

Route::get('admin/Vertical_ProductManager','Vertical_ProductManagerController@index');
Route::get('admin/Vertical_PharmacyManager','Vertical_PharmacyManagerController@index');
Route::get('admin/Vertical_ProductSelling','Vertical_ProductSellingController@index');
Route::get('admin/Vertical_ProductRental','Vertical_ProductRentalController@index');
Route::get('admin/Vertical_Product_Pharmacy','Vertical_Product_PharmacyController@index');
Route::get('admin/Vertical_ProductSelling_Pharmacy','Vertical_ProductSelling_PharmacyController@index');
Route::get('admin/Vertical_ProductRental_Pharmacy','Vertical_ProductRental_PharmacyController@index');
Route::get('admin/Coordinator_ProductManager','Coordinator_ProductManagerController@index');
Route::get('admin/Coordinator_PharmacyManager','Coordinator_PharmacyManagerController@index');
Route::get('admin/Coordinator_ProductSelling','Coordinator_ProductSellingController@index');
Route::get('admin/Coordinator_ProductRental','Coordinator_ProductRentalController@index');
Route::get('admin/Coordinator_Product_Pharmacy','Coordinator_Product_PharmacyController@index');
Route::get('admin/Coordinator_ProductSelling_Pharmacy','Coordinator_ProductSelling_PharmacyController@index');
Route::get('admin/Coordinator_ProductRental_Pharmacy','Coordinator_ProductRental_PharmacyController@index');
Route::get('admin/Vertical_FieldOfficer','Vertical_FieldOfficerController@index');
Route::get('admin/Vertical_FieldExecutive','Vertical_FieldExecutiveController@index');
Route::get('admin/Coordinator_FieldOfficer','Coordinator_FieldOfficerController@index');
Route::get('admin/Coordinator_FieldExecutive','Coordinator_FieldExecutiveController@index');
Route::get('admin/BranchHead','BranchHeadController@index');









//routes for downloading csv

// Route for downloading CSV on View Leads
Route::get('coordinatordownload','CoordinatorController@assigned');

// Route for downloading CSV on Individual count
Route::get('coordinatordownloadall','CoordinatorController@assigned');

// Route for downloading CSV on View Leads
Route::get('verticaldownload','VerticalController@assigned');
Route::get('verticalproductsellingdownload','Vertical_ProductSellingController@assigned');
Route::get('verticalproductrentaldownload','Vertical_ProductRentalController@assigned');
Route::get('verticalproductmanagerdownload','Vertical_ProductManagerController@assigned');
Route::get('verticalpharmacydownload','Vertical_PharmacyManagerController@assigned');
Route::get('vppdownload','Vertical_Product_PharmacyController@assigned');
Route::get('vpspdownload1','Vertical_ProductSelling_PharmacyController@assigned');
Route::get('vprpdownload1','Vertical_ProductRental_PharmacyController@assigned');

Route::get('coordinatorproductmanagerdownload','Coordinator_ProductManagerController@assigned');
Route::get('coordinatorproductmanagerdownloadall','Coordinator_ProductManagerController@assigned');
Route::get('coordinatorproductsellingdownload','Coordinator_ProductSellingController@assigned');
Route::get('coordinatorproductsellingdownloadall','Coordinator_ProductSellingController@assigned');
Route::get('coordinatorproductrentaldownload','Coordinator_ProductRentalController@assigned');
Route::get('coordinatorproductrentaldownloadall','Coordinator_ProductRentalController@assigned');
Route::get('coordinatorpharmacydownload','Coordinator_PharmacyManagerController@assigned');
Route::get('coordinatorpharmacydownloadall','Coordinator_PharmacyManagerController@assigned');
Route::get('coordinatorproductpharmacydownload','Coordinator_Product_PharmacyController@assigned');
Route::get('coordinatorproductpharmacydownloadall','Coordinator_Product_PharmacyController@assigned');
Route::get('coordinatorproductSellingpharmacydownload','Coordinator_ProductSelling_PharmacyController@assigned');
Route::get('coordinatorproductSellingpharmacydownloadall','Coordinator_ProductSelling_PharmacyController@assigned');
Route::get('coordinatorproductRentalpharmacydownload','Coordinator_ProductRental_PharmacyController@assigned');
Route::get('coordinatorproductRentalpharmacydownloadall','Coordinator_ProductRental_PharmacyController@assigned');
Route::get('coordinatorfieldofficerdownload','Coordinator_FieldOfficerController@assigned');
Route::get('coordinatorfieldofficerdownloadall','Coordinator_FieldOfficerController@assigned');
Route::get('coordinatorfieldexecutivedownload','Coordinator_FieldExecutiveController@assigned');
Route::get('coordinatorfieldexecutivedownloadall','Coordinator_FieldExecutiveController@assigned');



// Route for downloading CSV on Individual count
Route::get('verticaldownloadall','VerticalController@assigned');
Route::get('verticalproductsellingdownloadall','Vertical_ProductSellingController@assigned');
Route::get('verticalproductrentaldownloadall','Vertical_ProductRentalController@assigned');
Route::get('verticalproductmanagerdownloadall','Vertical_ProductManagerController@assigned');

Route::get('verticalpharmacydownloadall','Vertical_PharmacyManagerController@assigned');
Route::get('vppdownloadall','Vertical_Product_PharmacyController@assigned');
Route::get('vpspdownloadall1','Vertical_ProductSelling_PharmacyController@assigned');
Route::get('vprpdownloadall1','Vertical_ProductRental_PharmacyController@assigned');

Route::get('verticalpharmacydownloadall','Vertical_FieldOfficerController@assigned');


// Route for downloading CSV on View Leads
Route::get('admindownloadservice','AdminController@assigned');

// Route for downloading CSV on Individual count
Route::get('admindownloadserviceall','AdminController@assigned');


// Route for downloading CSV on View Leads
Route::get('managementdownload','ManagementController@assigned');

// Route for downloading CSV on Individual count
Route::get('managementdownloadall','ManagementController@assigned');

// Route for downloading CSV on View Leads
Route::get('BranchHeaddownload','BranchHeadController@assigned');
Route::get('BranchHeaddownloadproduct','BranchHeadController@productassigned');

// Route for downloading CSV on Individual count
Route::get('BranchHeaddownloadall','BranchHeadController@assigned');
Route::get('BranchHeaddownloadallproduct','BranchHeadController@productassigned');

// Route for downloading CSV on View Leads
Route::get('productmanagerdownload','productmanagerController@assigned');
Route::get('adminproductmanagerdownload','AdminController@productassigned');
Route::get('managementproductmanagerdownload','ManagementController@productassigned');
Route::get('BranchHeadproductmanagerdownload','BranchHeadController@productassigned');
Route::get('vpsdownload','Vertical_ProductSellingController@assigned1');
Route::get('vprdownload','Vertical_ProductRentalController@assigned1');
Route::get('vpmdownload','Vertical_ProductManagerController@assigned1');

Route::get('cpmdownload','Coordinator_ProductManagerController@assigned1');
Route::get('cpsdownload','Coordinator_ProductSellingController@assigned1');
Route::get('cprdownload','Coordinator_ProductRentalController@assigned1');
Route::get('cpppdownload1','Coordinator_Product_PharmacyController@assigned1');
Route::get('cpphpdownload','Coordinator_Product_PharmacyController@assigned2');


Route::get('vppproductdownload','Vertical_Product_PharmacyController@assigned1');
Route::get('vproductpharmacydownload','Vertical_Product_PharmacyController@assigned3');
Route::get('vproductSellingpharmacydownload1','Vertical_ProductSelling_PharmacyController@assigned1');
Route::get('vproductSellingpharmacydownload2','Vertical_ProductSelling_PharmacyController@assigned2');
Route::get('vproductRentalpharmacydownload1','Vertical_ProductRental_PharmacyController@assigned1');
Route::get('vproductRentalpharmacydownload2','Vertical_ProductRental_PharmacyController@assigned2');


Route::get('verticalfieldofficerproductdownloadall','Vertical_FieldOfficerController@assigned1');
Route::get('verticalfieldexecutiveproductdownloadall','Vertical_FieldExecutiveController@assigned1');


// Route for downloading CSV on Individual count
Route::get('productmanagerdownloadall','productmanagerController@assigned');
Route::get('admindownloadall','productController@assigned');
Route::get('adminproductmanagerdownloadall','AdminController@productassigned');
Route::get('managementproductmanagerdownloadall','managementController@productassigned');
Route::get('BranchHeadproductmanagerdownloadall','BranchHeadController@productassigned');
Route::get('vpsdownloadall','Vertical_ProductSellingController@assigned1');
Route::get('vprdownloadall','Vertical_ProductRentalController@assigned1');
Route::get('vpmdownloadall','Vertical_ProductManagerController@assigned1');

Route::get('cpmdownloadall','Coordinator_ProductManagerController@assigned1');
Route::get('cpsdownloadall','Coordinator_ProductSellingController@assigned1');
Route::get('cprdownloadall','Coordinator_ProductRentalController@assigned1');
Route::get('cpppdownloadall1','Coordinator_Product_PharmacyController@assigned1');
Route::get('cpphpdownloadall','Coordinator_Product_PharmacyController@assigned2');

Route::get('vppproductdownloadall','Vertical_Product_PharmacyController@assigned1');
Route::get('vproductpharmacydownloadall','Vertical_Product_PharmacyController@assigned3');
Route::get('vproductSellingpharmacydownloadall1','Vertical_ProductSelling_PharmacyController@assigned1');
Route::get('vproductSellingpharmacydownloadall2','Vertical_ProductSelling_PharmacyController@assigned2');
Route::get('vproductSellingpharmacydownloadall3','Vertical_ProductSelling_PharmacyController@assigned3');
Route::get('vproductRentalpharmacydownloadall1','Vertical_ProductRental_PharmacyController@assigned1');
Route::get('vproductRentalpharmacydownloadall2','Vertical_ProductRental_PharmacyController@assigned2');
Route::get('vproductRentalpharmacydownloadall3','Vertical_ProductRental_PharmacyController@assigned3');
Route::get('Coordinatorproductpharmacydownloadall3','Coordinator_Product_PharmacyController@assigned3');
Route::get('CoordinatorproductSellingpharmacydownloadall1','Coordinator_ProductSelling_PharmacyController@assigned1');
Route::get('CoordinatorproductSellingpharmacydownloadall2','Coordinator_ProductSelling_PharmacyController@assigned2');
Route::get('CoordinatorproductSellingpharmacydownloadall3','Coordinator_ProductSelling_PharmacyController@assigned3');
Route::get('CoordinatorproductRentalpharmacydownloadall1','Coordinator_ProductRental_PharmacyController@assigned1');
Route::get('CoordinatorproductRentalpharmacydownloadall2','Coordinator_ProductRental_PharmacyController@assigned2');
Route::get('CoordinatorproductRentalpharmacydownloadall3','Coordinator_ProductRental_PharmacyController@assigned3');

Route::get('verticalfieldofficerproductdownload1','Vertical_FieldOfficerController@assigned1');
Route::get('verticalfieldexecutiveproductdownload1','Vertical_FieldExecutiveController@assigned1');


// Route for downloading CSV on View Leads
Route::get('pharmacymanagerdownload','pharmacymanagerController@assigned');
Route::get('adminpharmacymanagerdownload','AdminController@pharmacyassigned');
Route::get('managementpharmacymanagerdownload','ManagementController@pharmacyassigned');
Route::get('BranchHeadpharmacymanagerdownload','BranchHeadController@pharmacyassigned');
Route::get('verticalpharmacymanagerdownload','Vertical_PharmacyManagerController@assigned1');
Route::get('coordinatorpharmacymanagerdownload','Coordinator_PharmacyManagerController@assigned1');

Route::get('vpppharmacydownload','Vertical_Product_PharmacyController@assigned2');

Route::get('vhfodownload1','Vertical_FieldOfficerController@assigned');
Route::get('vhfedownload1','Vertical_FieldExecutiveController@assigned');


// Route for downloading CSV on Individual count
Route::get('pharmacymanagerdownloadall','pharmacymanagerController@assigned');
Route::get('adminpharmacymanagerdownloadall','AdminController@pharmacyassigned');
Route::get('managementpharmacymanagerdownloadall','ManagementController@pharmacyassigned');
Route::get('BranchHeadpharmacymanagerdownloadall','BranchHeadController@pharmacyassigned');
Route::get('verticalpharmacymanagerdownloadall','Vertical_PharmacyManagerController@assigned1');
Route::get('coordinatorpharmacymanagerdownloadall','Coordinator_PharmacyManagerController@assigned1');

Route::get('vpppharmacydownloadall','Vertical_Product_PharmacyController@assigned2');

Route::get('vhfodownloadall1','Vertical_FieldOfficerController@assigned');
Route::get('vhfedownloadall1','Vertical_FieldExecutiveController@assigned');


// Route for downloading CSV on View Leads
Route::get('fieldexecutivedownload','fieldexecutiveController@assigned');

// Route for downloading CSV on Individual count
Route::get('fieldexecutivedownloadall','fieldexecutiveController@assigned');

// Route for downloading CSV on View Leads
Route::get('fieldofficerdownload','fieldofficerController@assigned');
Route::get('coordiantorfieldofficerdownload','Coordinator_FieldOfficerController@assigned1');
Route::get('coordiantorfieldofficerdownloadall','Coordinator_FieldOfficerController@assigned1');
Route::get('coordiantorfieldexecutivedownload','Coordinator_FieldExecutiveController@assigned1');
Route::get('coordiantorfieldexecutivedownloadall','Coordinator_FieldExecutiveController@assigned1');

// Route for downloading CSV on Individual count
Route::get('fieldofficerdownloadall','fieldofficerController@assigned');


// Route for downloading CSV on View Leads
Route::get('customercaredownload','CustomercareController@assigned');
Route::get('marketingdownload','marketingController@assigned');

// Route for downloading CSV on Individual count
Route::get('customercaredownloadall','CustomercareController@assigned');
Route::get('marketingdownloadall','marketingController@assigned');

//Routes for dowloading in filtered graphs
Route::get('filterdownload','AdminController@filterassigned');
Route::get('filtermanagementdownload','ManagementController@filterassigned');

// Routes for showing details according to Status for Coordinator on Dashboard
Route::get('assignedstatus','CoordinatorController@assigned');
Route::get('coordinatorproductmanagerassignedstatus','Coordinator_ProductManagerController@assigned');
Route::get('coordinatorproductsellingmanagerassignedstatus','Coordinator_ProductSellingController@assigned');
Route::get('coordinatorproductrentalmanagerassignedstatus','Coordinator_ProductRentalController@assigned');
Route::get('coordinatorpharmacymanagerassignedstatus','Coordinator_PharmacyManagerController@assigned');
Route::get('coordinatorproductpharmacyassignedstatus','Coordinator_Product_PharmacyController@assigned');
Route::get('coordinatorproductsellingpharmacyassignedstatus','Coordinator_ProductSelling_PharmacyController@assigned');
Route::get('coordinatorproductrentalpharmacyassignedstatus','Coordinator_ProductRental_PharmacyController@assigned');
Route::get('coordinatorfieldofficerassignedstatus','Coordinator_FieldOfficerController@assigned');
Route::get('coordinatorfieldexecutiveassignedstatus','Coordinator_FieldExecutiveController@assigned');

// Routes for showing details according to Status for Vertical Head on Dashboard
Route::get('verticalassign','VerticalController@assigned');

// Routes for showing details according to Status for Vertical Head on Dashboard
Route::get('verticalproductsellingassign','Vertical_ProductSellingController@assigned');
Route::get('verticalproductRentalassign','Vertical_ProductRentalController@assigned');
Route::get('verticalproductManagerassign','Vertical_ProductManagerController@assigned');
Route::get('verticalpharmacymanagerassign','Vertical_PharmacyManagerController@assigned');

Route::get('verticalproductpharmacymanagerassign','Vertical_Product_PharmacyController@assigned');
Route::get('verticalproductsellingpharmacyassign2','Vertical_ProductSelling_PharmacyController@assigned');
Route::get('verticalproductrentalpharmacyassign2','Vertical_ProductRental_PharmacyController@assigned');

Route::get('verticalfieldofficerassign','Vertical_FieldOfficerController@assigned');
Route::get('vertical_fieldexecutiveassign','Vertical_FieldExecutiveController@assigned');


// Routes for showing details according to Status for Admin on Dashboard
Route::get('adminassign','AdminController@assigned');

// Routes for showing details according to Status for Management on Dashboard
Route::get('managementassign','ManagementController@assigned');

// Routes for showing details according to Status for Branch Head on Dashboard
Route::get('BranchHeadassign','BranchHeadController@assigned');

// Routes for showing details according to Status for Customer Care on Dashboard
Route::get('ccassign','CustomercareController@assigned');
// Routes for showing details according to Status for marketing on Dashboard
Route::get('marketingassign','marketingController@assigned');

// Routes for showing details according to Status for Product Manager on Dashboard
Route::get('productassign', 'productmanagerController@assigned');
Route::get('adminproductassign', 'AdminController@productassigned');
Route::get('managementproductassign', 'ManagementController@productassigned');
Route::get('BranchHeadproductassign', 'BranchHeadController@productassigned');

Route::get('vpsproductassign', 'Vertical_ProductSellingController@assigned1');
Route::get('vprproductassign', 'Vertical_ProductRentalController@assigned1');
Route::get('vpmproductassign', 'Vertical_ProductManagerController@assigned1');
Route::get('vphmproductassign', 'Vertical_PharmacyManagerController@assigned1');

Route::get('vppproductassign', 'Vertical_Product_PharmacyController@assigned1');
Route::get('vpppharmacyassign', 'Vertical_Product_PharmacyController@assigned2');
Route::get('verticalproductsellingpharmacyassign', 'Vertical_ProductSelling_PharmacyController@assigned1');
Route::get('verticalproductsellingpharmacyassign1', 'Vertical_ProductSelling_PharmacyController@assigned2');
Route::get('verticalproductrentalpharmacyassign', 'Vertical_ProductRental_PharmacyController@assigned1');
Route::get('verticalproductrentalpharmacyassign1', 'Vertical_ProductRental_PharmacyController@assigned2');

Route::get('verticalfieldofficerproductassign','Vertical_FieldOfficerController@assigned1');
Route::get('verticalfieldexecutiveproductassign','Vertical_FieldExecutiveController@assigned1');

Route::get('cpmproductassign', 'Coordinator_ProductManagerController@assigned1');
Route::get('cpsproductassign', 'Coordinator_ProductSellingController@assigned1');
Route::get('cprproductassign', 'Coordinator_ProductRentalController@assigned1');
Route::get('cphmproductassign', 'Coordinator_PharmacyManagerController@assigned1');
Route::get('cpsphproductassign', 'Coordinator_Product_PharmacyController@assigned1');
Route::get('cprphproductassign', 'Coordinator_Product_PharmacyController@assigned2');
Route::get('cproductsellingphproductassign', 'Coordinator_ProductSelling_PharmacyController@assigned1');
Route::get('cproductsellingpharmacyproductassign', 'Coordinator_ProductSelling_PharmacyController@assigned2');
Route::get('cproductrentalphproductassign', 'Coordinator_ProductRental_PharmacyController@assigned1');
Route::get('cproductrentalpharmacyproductassign', 'Coordinator_ProductRental_PharmacyController@assigned2');
Route::get('coordinatorFieldofficerproductassign', 'Coordinator_FieldOfficerController@assigned1');
Route::get('coordinatorFieldexecutiveproductassign', 'Coordinator_FieldExecutiveController@assigned1');


// Routes for showing details according to Status for Pharmacy Manager on Dashboard
Route::get('pharmacyassign', 'pharmacymanagerController@assigned');
Route::get('adminpharmacyassigned', 'AdminController@pharmacyassigned');
Route::get('managementpharmacyassigned', 'ManagementController@pharmacyassigned');
Route::get('BranchHeadpharmacyassigned', 'BranchHeadController@pharmacyassigned');

// Routes for showing details according to Status for Field Officer on Dashboard
Route::get('fieldofficerassign', 'fieldofficerController@assigned');

// Routes for showing details according to Status for Field Executive on Dashboard
Route::get('fieldexecutiveassign', 'fieldexecutiveController@assigned');

Route::get('referalassign','referalController@assigned');

// employee disable route
Route::get('disable','employeesController@disable');

// employee enable route
Route::get('enable','employeesController@enable');

// employee disablelist route
Route::get('disablelist','employeesController@disablelist');

//Route for SKUid and Product name dropdowns on Keyup
Route::get('productdetails','productController@productdetails');

// Route for showing details in dynamic graphs

Route::get('filtervalueshow', 'AdminController@showfiltervalues');
Route::get('managementfiltervalueshow', 'ManagementController@showfiltervalues');

//Route for showing the Login form
Route::get('admin','Admin\LoginController@showLoginForm')->name('admin.login');
Route::get('login','Admin\LoginController@showLoginForm')->name('admin.login');

// login url
Route::POST('admin','Admin\LoginController@login');
//logout url
Route::get('logout', '\App\Http\Controllers\Admin\LoginController@logout');
Route::get('admin/logout/{try}', '\App\Http\Controllers\Admin\LoginController@logout');


Route::POST('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

Route::POST('admin-password/reset','Admin\ResetPasswordController@reset');
Route::GET('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::GET('admin-register','Admin\RegisterController@showRegistrationForm')->name('admin.register');
Route::POST('admin-register','Admin\RegisterController@register');


Route::POST('logout', 'Admin\LoginController@logout')->name('logout');

// Impersonate login route
Route::get('impersonate','employeesController@impersonate');

Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');

//Route for Logger view
// Route::resource('','Logger@show');

//Route for Password Change in User Settings -- by jatin
Route::get('change-password', 'UpdateController@index');
Route::post('change-password', 'UpdateController@update');


Route::get('updatetables', function () {
if(Auth::guard('admin')->check())
        {
    $active_state = DB::table('sms_email_filter')->value('active_state');


    return view('admin.updatetables',compact('active_state'));
}
else
{
	return redirect('/admin');
        
}
})->name('updatetables');

// Route for SMS and Email Activation and Deactivation
Route::get('sms_email_filter','SMSEmailFilterController@filter')->name('sms.filter');
Route::get('alllead','vhController@filter');

Route::get('checkemployee','employeesController@filter');
Route::get('checkemployeeexist','employeesController@filter');

Route::get('allleadp','productController@filter');

Route::get('productsellingadmin','productController@adminassigned');

Route::get('vertical_productselling_productfilter','Vertical_ProductSellingController@productfilter');

Route::get('coordinator_productselling_productfilter','Coordinator_ProductSellingController@productfilter');

Route::get('coordinator_productselling_pharmacy_filter','Coordinator_ProductSelling_PharmacyController@productfilter');
Route::get('coordinator_productselling_pharmacy_filter_for_pharmacy','Coordinator_ProductSelling_PharmacyController@pharmacyfilter');
Route::get('jatin','Coordinator_ProductSelling_PharmacyController@productselling_pharmacy_allfilter');

Route::get('coordinator_productrental_pharmacy_filter','Coordinator_ProductRental_PharmacyController@productrentfilter');
Route::get('coordinator_productrental_pharmacy_filter_for_pharmacy','Coordinator_ProductRental_PharmacyController@pharmacyrentfilter');

Route::get('vertical_productselling_pharmacy_filter','Vertical_ProductSelling_PharmacyController@productfilter');
Route::get('vertical_productselling_pharmacy_filter_for_pharmacy','Vertical_ProductSelling_PharmacyController@pharmacyfilter');

Route::get('vertical_productrental_pharmacy_filter','Vertical_ProductRental_PharmacyController@productrentfilter');
Route::get('vertical_productrental_pharmacy_filter_for_pharmacy','Vertical_ProductRental_PharmacyController@pharmacyrentfilter');

Route::get('coordinator_pharmacymanager_filter', 'Coordinator_PharmacyManagerController@pharmacyfilter');

Route::get('vertical_pharmacymanager_filter', 'Vertical_PharmacyManagerController@pharmacyfilter');

Route::get('coordinator_productmanager_filter', 'Coordinator_ProductManagerController@productfilter');

Route::get('vertical_productmanager_filter', 'Vertical_ProductManagerController@productfilter');

Route::get('vertical_product_pharmacy_filter','Vertical_Product_PharmacyController@productfilter');
Route::get('vertical_product_pharmacy_filter_for_pharmacy','Vertical_Product_PharmacyController@pharmacyfilter');

Route::get('coordinator_product_pharmacy_filter','Coordinator_Product_PharmacyController@productfilter');
Route::get('coordinator_product_pharmacy_filter_for_pharmacy','Coordinator_Product_PharmacyController@pharmacyfilter');

Route::get('vertical_fieldofficer_filter','Vertical_FieldOfficerController@fieldofficerfilter1');
Route::get('vertical_fieldexecutive_filter','Vertical_FieldExecutiveController@fieldexecutivefilter');

// Route for product filter for admin and management
Route::get('allleadpa','productController@adminfilter');
Route::get('allleadfo','productController@fieldofficerfilter');

//Route for search functionality -- filter function called
Route::get('allleadc','ccController@filter');

Route::get('allleadpc','productController@ccfilter');


Route::get('log','Logger@index');
Route::get('log1','Logger1@index');
Route::get('alllogs','Logger@filter');



Route::get('test', function () {
    return view('test1');
});
// Route::get('testing', function () {

// 	$a=DB::table('employees')->where('Designation',"Vertical Head")->get();
//     return view('cc.dropdown');
// });


Route::get('testing','ccController@assignto');
Route::get('testing1','ccController@assignto1');
// Route::get('Asignto','ccController@assignto');



//Footer
Route::get('footer', function () {
    return view('layouts.footer');
});


//Cient detials
Route::get('clientdetails','productController@clientdetails');

//route to add multiple service input
Route::get('multiservice','ccController@multiservice');



//More Input fields
Route::get('inputfileds','productController@moreinput');


//More Input fields
Route::get('inputfiledsPharmacyDetails','productController@moreinputPharmacyDetails');


//Invoice
Route::get('invoice', function () {
    return view('admin.invoice');
});

// Branch Details in Admin Dashboard
Route::get('branchdeatils','ccController@branchdeatilsadmin');

//Mobile View for Admin Home cities

Route::get('homemobile','AdminController@homeadminmobile');

// Mobile brach Drop down
// Route::get('branchdeatilsmobile','ccController@branchdeatilsadminmobiledrop');

Route::get('homemobilevertical','ccController@mobilevertical');




Route::get('graph','ccController@graphmobilevertial');

Route::get('indivdualgraph','ccController@indivdualgraphvertial');

// Admin




Route::get('adminindividualgraph','AdminController@homeadminmobileindividualgraph');
Route::get('adminindividualgraphforonclick','AdminController@homeadminmobileindividualgraphonclcik');





Route::get('managementmobile','ManagementController@mobilecount');
Route::get('branchheadmobile','BranchHeadController@mobilecount');



Route::get('managementindividualgraph','ManagementController@individualgraph');



// Admin Mobilr Vertical graph





Route::get('adminverticalgraph','AdminController@verticalgraphofadmin');



Route::get('managementverticalgraph','ManagementController@managementverticalgraph1');


Route::get('searchfilteradmin','AdminController@searchfilteradmin');

Route::get('searchfiltervertical','VerticalController@searchfilter');



Route::get('searchfiltercoordinator','CoordinatorController@searchfilter');


Route::get('searchfiltermanagement','ManagementController@searchfilteradmin');

Route::get('searchfiltermanagementmobile','ManagementController@searchfiltermobile');

Route::get('searchfilteradminmobile','AdminController@searchfilteradminmobile');

Route::get('productname','productController@productname');

// // Route for running StoreHippo API
// Route::get('storehippo','StoreHippoController@index');


Route::get('vc','VerticalCoord@index');
Route::get('vca','VerticalCoord@emp');
Route::get('vcn','VerticalCoord@number');
Route::get('vcname','VerticalCoord@name');
Route::get('leadcheck','VerticalCoord@leadcheck');

Route::get('password1','VerticalCoord@password1');
Route::get('print','VerticalCoord@password1');

Route::get('json','JsonController@index');

Route::get('pdf_for_NS_and_PSC','NsandPscPdfController@index')->name('pdf_for_NS_and_PSC');
Route::get('pdf_for_Mathrutvam','MathrutvamPdfController@index')->name('pdf_for_Mathrutvam');
Route::get('pdf_for_Physiotheraphy','PhysiotheraphyPDFController@index')->name('pdf_for_Physiotheraphy');

// route for assessment done state on Dashboard
Route::get('assessmentdone','AdminController@assessmentdonelist');

//More doctors to add
Route::get('newdoc1','vhController@newdoctor');
Route::get('addmedicine','vhController@addmedicine');

// Nebulization
Route::get('nebulizationaddmedicine','vhController@nebulizationaddmedicine');
Route::get('nebulozationnewdoc','vhController@nebulizationnewdoctor');


// Immunization

Route::get('phase1','Immunization@phase1');
Route::get('phase2','Immunization@phase2');
Route::get('phase3','Immunization@phase3');
Route::get('phase4','Immunization@phase4');
Route::get('phase5','Immunization@phase5');
Route::get('phase6','Immunization@phase6');
Route::get('phase7','Immunization@phase7');
Route::get('phase8','Immunization@phase8');

Route::get('history','vhController@history');




Route::get('form', function () {
    return view('city.form');
});
