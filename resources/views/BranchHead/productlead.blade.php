 @extends('layouts.BranchHead')

@section('content')

<html lang="en">
<head>

 <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <style type="text/css">
  button[disabled], html input[disabled] {
    background: white;
    cursor: default;
  }
  </style>


  <script>
  $(document).ready(function(){
 $(function() {
    $('#country').on('change', function() {
      $('#phone1').val($(this).val());
    });



    var country = $('#country').val();
    $('#phone1').val(country);

    //  $('#sameaspermanentadress').change(function () {

    //         console.log("checked");
    // });
    $('#sameaspermanentadress').change(function () {

      if (this.checked) {
        console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#PAddress1').val(Addresd1);
        $("#PAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#PAddress2').val(Addresd2);
        $("#PAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#PCity').val(City);
        $("#PCity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#PDistrict').val(District);
        $("#PDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#PState').val(State);
        $("#PState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#PPinCode').val(Pincode);
        $("#PPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        console.log("unchecked");

        // Adrees1
        $('#PAddress1').val("");
        $("#PAddress1").prop('disabled', false);

        //Adress 2
        $('#PAddress2').val("");
        $("#PAddress2").prop('disabled', false);

        //City
        $('#PCity').val("");
        $("#PCity").prop('disabled', false);

        //District
        $('#PDistrict').val("");
        $("#PDistrict").prop('disabled', false);

        //State
        $('#PState').val("");
        $("#PState").prop('disabled', false);


        //Pincode
        $('#PPinCode').val("");
        $("#PPinCode").prop('disabled', false);

      }
    });





    $('#Esameaspermananentaddress').change(function () {

      if (this.checked) {
        // console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#EAddress1').val(Addresd1);
        $("#EAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#EAddress2').val(Addresd2);
        $("#EAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#ECity').val(City);
        $("#ECity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#EDistrict').val(District);
        $("#EDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#EState').val(State);
        $("#EState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#EPinCode').val(Pincode);
        $("#EPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        // console.log("unchecked");

        // Adrees1
        $('#EAddress1').val("");
        $("#EAddress1").prop('disabled', false);

        //Adress 2
        $('#EAddress2').val("");
        $("#EAddress2").prop('disabled', false);

        //City
        $('#ECity').val("");
        $("#ECity").prop('disabled', false);

        //District
        $('#EDistrict').val("");
        $("#EDistrict").prop('disabled', false);

        //State
        $('#EState').val("");
        $("#EState").prop('disabled', false);


        //Pincode
        $('#EPinCode').val("");
        $("#EPinCode").prop('disabled', false);

      }
    });



$('#shippingsameaspermananentaddress').change(function () {

      if (this.checked) {
        // console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#SAddress1').val(Addresd1);
        $("#SAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#SAddress2').val(Addresd2);
        $("#SAddress2").prop('disabled', true);

        //CitS

        var City =$('#City').val();
        $('#SCity').val(City);
        $("#SCity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#SDistrict').val(District);
        $("#SDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#SState').val(State);
        $("#SState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#SPinCode').val(Pincode);
        $("#SPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        // console.log("unchecked");

        // Adrees1
        $('#SAddress1').val("");
        $("#SAddress1").prop('disabled', false);

        //Adress 2
        $('#SAddress2').val("");
        $("#SAddress2").prop('disabled', false);

        //City
        $('#SCity').val("");
        $("#SCity").prop('disabled', false);

        //District
        $('#SDistrict').val("");
        $("#SDistrict").prop('disabled', false);

        //State
        $('#SState').val("");
        $("#SState").prop('disabled', false);


        //Pincode
        $('#SPinCode').val("");
        $("#SPinCode").prop('disabled', false);

      }
    });







//Alternate Phone Number
    $("#clientalternateno").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#clientalternateno').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#clientalternateno").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientalternateno").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

}
});


$("#clientalternateno").blur(function(){
      var value = $('#clientalternateno').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;
      var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
      if (numwithcountrycode.test(value) == true)
         {

             $("#clientalternateno").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
         }else
         {

                  if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientalternateno").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#clientalternateno").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#clientalternateno").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }


         }

    });





//EmergencyContact Phone Number
    $("#EmergencyContact").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#EmergencyContact').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#EmergencyContact").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientalternateno").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

}
});


$("#EmergencyContact").blur(function(){
      var value = $('#clientalternateno').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;
      var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
      if (numwithcountrycode.test(value) == true)
         {

             $("#EmergencyContact").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
         }else
         {

                  if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EmergencyContact").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }


         }

    });


    //Phone Number
    $("#phone").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#phone').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

}
});
    // $("#phone").blur(function(){
    //   var value = $('#phone').val();

    //   var reg = /^[\+?\d[\d -]{8,12}\d$/;

    //       if (reg.test(value) == false)
    //     {

    //           // console.log("Wrong Email");
    //           $("#phone").css("border-bottom", "1px solid red");
    //            $("#submitt").prop('disabled', true);
    //     }
    //      else
    //     {
    //       // console.log("Correct Email");
    //        $("#phone").css("border-bottom", "1px solid #484e51");
    //         $("#submitt").prop('disabled', false);
    //     }

    //     if(value == "")
    //     {
    //         $("#phone").css("border-bottom", "1px solid #484e51");
    //          $("#submitt").prop('disabled', false);
    //     }
    // });

$("#phone").blur(function(){
      var value = $('#phone').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;
      var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
      if (numwithcountrycode.test(value) == true)
         {

             $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
         }else
         {

                  if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#phone").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }


         }
        //   if (reg.test(value) == false)
        // {

        //       // console.log("Wrong Email");
        //       $("#phone").css("border-bottom", "1px solid red");
        //        $("#submitt").prop('disabled', true);
        // }
        //  else
        // {
        //   // console.log("Correct Email");
        //    $("#phone").css("border-bottom", "1px solid #484e51");
        //     $("#submitt").prop('disabled', false);
        // }

        // if(value == "")
        // {
        //     $("#phone").css("border-bottom", "1px solid #484e51");
        //      $("#submitt").prop('disabled', false);
        // }
    });

    // Pincode Validaion in Addres
  $("#PinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PinCode").blur(function(){
            var value = $('#PinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Present Addres
  $("#PPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PPinCode").blur(function(){
            var value = $('#PPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Emergency Addres
  $("#EPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#EEPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#EPinCode").blur(function(){
            var value = $('#EPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#EPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });


    // Client Email Id Validation Start
    $("#clientemail").keyup(function(){
      var value = $('#clientemail').val();

      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);

        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
        }

      if (reg.test(value) == false)
      {

        // console.log("Wrong Email");
        $("#clientemail").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#clientemail").css("border-bottom", "1px solid green");
      }


    });

    $("#clientemail").blur(function(){
            var value = $('#clientemail').val();

         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
        }

          if(value == "")
        {
            $("#clientemail").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }
    });

    // Cleint ValidationEmail Ends




  });
 $("#ProductName").blur(function(){
     var ProductName =$('#ProductName').val();
     $.get("{{ URL::to('productname') }}", { productname :ProductName},function(data){
              $('#SKUid').val(data);
          });
});

    var value=$('#reference').val();
    if(value == "Other")
    {
      $("#source").show();
    }else
    {
      $("#source").hide();
    }



    $("#reference").change(function(){
      var value= $("#reference").val();
      if(value == "Other")
      {
        $("#source").show();
      }else
      {
        $("#source").hide();
      }
    });


// Drop down
var servicetype = $('#servicetype').val();

if(servicetype == "")
{
      $("#branch").hide();
} else
{
  $("#branchalert").hide();
}


var branch = $('#branch').val();

if((servicetype == "") && (branch == ""))
{


  $("#assignedto").hide();

} else
{
  $("#assignedtoalert").hide();
}




$("#servicetype").change(function(){

    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
    if(servicetype == "")
    {
        $("#branchalert").show();
        $("#branch").hide();
    }else if(branch == "")
    {
      $("#branchalert").hide();
      $("#branch").show();

    } else
    {
      $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });
    }
});



$("#branch").change(function(){
    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
  if(branch == "")
  {
      $("#assignedto").hide();
      $("#assignedtoalert").show();
      $("#resrult").hide();
  }else
  {


       // $.get("test.php", { name:"Donald", town:"Ducktown" });
       $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });


      $("#assignedto").hide();
      $("#assignedtoalert").hide();
      $("#resrult").show();
  }




});


$("#clinetidsubmit").click(function(){
     var clientid =$('#clientid').val();
      $("#clientid").val("");
     // alert(clientid);
     $.get("{{ URL::to('clientdetails') }}", { clientid1 :clientid},function(data){
                $('#result').html(data);
          });
    });

var count =1;
$("#addmoreinputfiled").click(function(){

     count=count + 1;
    $("#value").val(count);
         $.get("{{ URL::to('inputfileds') }}", { noofinputfiledsadded :count},function(data){
                $('#moreinput').append(data+"<br/>");
          });
    });

var countPharmacyDetails=1;
$("#addmoreinputfiledPharmacyDetails").click(function(){

     countPharmacyDetails = countPharmacyDetails + 1;

     $("#nooffiledsPharmacyDetails").val(countPharmacyDetails);
         $.get("{{ URL::to('inputfiledsPharmacyDetails') }}", { countPharmacyDetails :countPharmacyDetails},function(data){
                $('#moreinputPharmacyDetails').append(data+"<br/>");
          });
    });
  });


  $(document).ready(function(){

  var value=$('#Type').val();
     if(value == "Sell")
    {
      $("#sell").show();
      $("#rent").hide();
    }else
    {
    if(value == "")
    {
      $("#sell").hide();
      $("#rent").hide();

    }else
    {
      $("#sell").hide();
      $("#rent").show();
    }
  }



    $("#Type").change(function(){
    var value= $("#Type").val();
    if(value == "Sell")
    {
     $("#sell").show();
      $("#rent").hide();
    }else
    {
    if(value == "")
    {
      $("#sell").hide();
      $("#rent").hide();

    }else
    {
      $("#sell").hide();
      $("#rent").show();
    }
  }
});
});
  </script>


  </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
.navbar-right
{
        text-align: -webkit-right;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}

.imgg {
       margin-top: -13px;
    margin-left: -62px;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: -3px;
    font-weight: 700;
}
.switch {
  position: relative;
  display: inline-block;
  width: 30px;
  height: 17px;
}
.footer
{
  margin-top: 70px;
}
.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .2s;
  transition: .2s;
}

input:checked + .slider {
  background-color: #009056;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
@media only screen and (max-width: 1200px) {

#checkbox
{
    margin-left: -21px
}
.imgg
    {
             padding-top: 8px;
    padding-left: 39px

    }
    #loo {
    margin-left: 82px;

}
#toggle
{
  padding-left: 28px;
}
}

</style>


</head>
<body>


<!-- title -->
<div class="container">
  <div class="row">
    <div class="col-sm-12" id="title">
      <h2> {{substr(Route::currentRouteName(),20)}} Product Lead Registration</h2>
    </div>
  </div>

</div>

<!-- title Ends -->

<form class="form-horizontal" action="/product" method="POST">
  {{csrf_field()}}

  @section('editMethod')
  @show

  <div class="container" id="main" style="    margin-top: 34px;">

    <div class="row" id="maintitle" style="    margin-left: -4px;
    width: 102%;" >

    <div class="col-sm-12" >
      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#firstform"  >Client Details<p style="text-align: -webkit-right;
        margin-top: -20px;
        margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
      </button>

    </div>

  </div>
</div>

<div class="container" id="main1" style="margin-top: 11px;">

  <div class="row collapse" id="firstform" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">
  <!-- Modal Box Code Starts -->
    <div class="col-sm-12" style="    text-align: right;">

   <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="
    margin-top: 10px;
    margin-bottom: 12px;font-size: 14px;margin-top: 10px;background: #4abde8">Existing Client</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-body" style="text-align: center;">
         <input type="text"   id="clientid"  style="    width: 50%;" placeholder="Client Id">
           <button type="button" class="btn btn-default" data-dismiss="modal" id="clinetidsubmit" style="margin-left: 16px;">Submit</button>
        </div>

      </div>

    </div>
  </div>
    </div>
    <div id="result">
    <!-- Modal box code ends -->
    <div class="col-sm-3" style="    margin-top: 4px;">

      <label>First Name <span style="color:Red; font-size: 20px;">*</span></label>


      <input type="text"  rows="5" name="clientfname" id="clientfname" value="@yield('editclientfname')" required>

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Middle Name</label>
      <input type="text"   rows="5" name="clientmname" id="clientmname" value="@yield('editclientmname')">

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Last Name</label>
      <input type="text"  rows="5"  name="clientlname" id="clientlname" value="@yield('editclientlname')" >

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Email ID</label>
      <input type="text"  rows="5" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

    </div>
    <div class="col-sm-2" id="secondrow" style="margin-top: 35px;">

      <label style="  margin-bottom: 5px;">Select Country Code</label><br>
      <select  id="country" >

        <option value="+91">India</option>
        <option value="+1">US</option>
        <option value="+44 ">UK</option>
      </select>
    </div>
    <div class="col-sm-4" style="margin-top: 26px;">


      <label>Mobile No. <span style="color:Red; font-size: 20px;">*</span></label>


          <div class="row">
              <div class="col-sm-2">
                <input type="text"  id="phone1"  rows="5" required disabled>
              </div>
              <div class="col-sm-10">
                  <input type="text"  id="phone"  rows="5" name="clientmob" value="@yield('editclientmob')" maxlength="10" required>
              </div>
          </div>
    </div>
    <div class="col-sm-3" id="secondrow">


      <label>Alternate no.</label>

      <input type="text" rows="5" name="clientalternateno" id="clientalternateno" value="@yield('editclientalternateno')">
    </div>
    <div class="col-sm-3" id="secondrow">

      <label>Emergency Contact No.</label>
      <input type="text" rows="5" name="EmergencyContact" id="EmergencyContact" value="@yield('editEmergencyContact')" maxlength="13">
    </div>

   <div class="col-sm-12"> </div>
    <div class="col-sm-3" id="secondrow" style="margin-top: 26px;">

      <label style="  margin-bottom: 5px;">Reference<span style="color:Red; font-size: 20px;">*</span></label><br>
      <select name="reference" id="reference"  required>
        <option value="@yield('editreference')">@yield('editreference')</option>
        @foreach($reference as $reference)
        <option value="{{ $reference->Reference}}">{{ $reference->Reference}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-4" id="secondrow">
      <div id="source">
        <label>Source</label>
        <input type="text"   rows="5" name="source" id="source" value="@yield('editsource')">
      </div>
    </div>
    <div class="col-sm-12" id="secondrow" >

      <h4> Permanent Address</h4>
    </div>


    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 1</label>
      <input type="text"   rows="5" name="Address1" id="Address1" value="@yield('editAddress1')">

    </div>
    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 2</label>
      <input type="text"   rows="5" name="Address2" id="Address2" value="@yield('editAddress2')">

    </div>
    <div class="col-sm-4" id="secondrow" style="margin-top: 33px;">
      <label style="  margin-bottom: 5px;">City</label><br>
      <!-- <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')"> -->

      <select  id="City"  name="City" >

          <option value="@yield('editCity')">@yield('editCity')</option>
          @for($i=0;$i<$count1;$i++)
          <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
          @endfor
          <!-- <option value="+1">US</option>
          <option value="+44 ">UK</option> -->
      </select>

      <!-- <select name="City" id="City" >
      <option value="@yield('editCity')">@yield('editCity')</option>
      @foreach($city as $city)
      <option value="{{ $city->name}}">{{ $city->name}}</option>
      @endforeach
    </select> -->

  </div>

  <div class="col-sm-4" id="secondrow" >
    <label>District</label>
    <input type="text"    rows="5" name="District" id="District" value="@yield('editDistrict')">

  </div>
  <div class="col-sm-4" id="secondrow" >
    <label>State</label>
  <!--   <input type="text"    rows="5" name="State" id="State" value="@yield('editState')"> -->
     <select name="State" id="State" value="@yield('editState')">
        <option value="@yield('editState')">@yield('editState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
        <option value="Chhattisgarh">Chhattisgarh</option>
        <option value="Chandigarh">Chandigarh</option>
        <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
        <option value="DamanandDiu">Daman and Diu</option>
        <option value="Delhi">Delhi</option>
        <option value="Goa">Goa</option>
        <option value="Haryana">Haryana</option>
         <option value="HimachalPradesh">Himachal Pradesh</option>
          <option value="JammuandKashmir">Jammu and Kashmir</option>
           <option value="Jharkhand">Jharkhand</option>
            <option value="Karnataka">Karnataka</option>
             <option value="Kerala">Kerala</option>
              <option value="MadhyaPradesh">Madhya Pradesh</option>
               <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                 <option value="Meghalaya">Meghalaya</option>
                  <option value="Mizoram">Mizoram</option>
                   <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                     <option value="Punjab">Punjab</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Rajasthan">Rajasthan</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Sikkim">Sikkim</option>
                      <option value="TamilNadu">Tamil Nadu</option>
                      <option value="Tripura">Tripura</option>
                       <option value="UttarPradesh">Uttar Pradesh</option>
                        <option value="Uttarakhand">Uttarakhand</option>
                         <option value="WestBengal">West Bengal</option>

      </select>

  </div>
  <div class="col-sm-4" id="secondrow" >
    <label>Pincode</label>
    <input type="text"    rows="5" name="PinCode" id="PinCode" value="@yield('editPinCode')" maxlength="8">

  </div>

  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PresentAddress" style="    width: 100%;
    margin-left: -1px;" >Present Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
  </button>
</div>
<div class="col-sm-12 collapse" id="PresentAddress">
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 1</label>
    <input type="text"    rows="5" name="PAddress1" id="PAddress1" value="@yield('editPAddress1')">
  </div>
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 2</label>
    <input type="text"  rows="5" name="PAddress2" id="PAddress2" value="@yield('editPAddress2')">
  </div>
  <div class="col-sm-4" style="    margin-top: 20px;">
    <label>City</label>

    <input type="text"  rows="5" name="PCity" id="PCity" value="@yield('editPCity')">



    <!--  <select name="PCity" id="PCity" >
    <option value="@yield('editPCity')">@yield('editPCity')</option>
    @foreach($pcity as $pcity)
    <option value="{{ $pcity->name}}">{{ $pcity->name}}</option>
    @endforeach
  </select> -->


</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>District</label>
  <input type="text"  rows="5" name="PDistrict" id="PDistrict" value="@yield('editPDistrict')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>State</label>
<!--   <input type="text"  rows="5" name="PState" id="PState" value="@yield('editPState')"> -->
   <select name="PState" id="PState" value="@yield('editPtate')">
        <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
        <option value="Chhattisgarh">Chhattisgarh</option>
        <option value="Chandigarh">Chandigarh</option>
        <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
        <option value="DamanandDiu">Daman and Diu</option>
        <option value="Delhi">Delhi</option>
        <option value="Goa">Goa</option>
        <option value="Haryana">Haryana</option>
         <option value="HimachalPradesh">Himachal Pradesh</option>
          <option value="JammuandKashmir">Jammu and Kashmir</option>
           <option value="Jharkhand">Jharkhand</option>
            <option value="Karnataka">Karnataka</option>
             <option value="Kerala">Kerala</option>
              <option value="MadhyaPradesh">Madhya Pradesh</option>
               <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                 <option value="Meghalaya">Meghalaya</option>
                  <option value="Mizoram">Mizoram</option>
                   <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                     <option value="Punjab">Punjab</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Rajasthan">Rajasthan</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Sikkim">Sikkim</option>
                      <option value="TamilNadu">Tamil Nadu</option>
                      <option value="Tripura">Tripura</option>
                       <option value="UttarPradesh">Uttar Pradesh</option>
                        <option value="Uttarakhand">Uttarakhand</option>
                         <option value="WestBengal">West Bengal</option>

      </select>
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>Pincode</label>
  <input type="text"  rows="5" name="PPinCode" id="PPinCode" value="@yield('editPPinCode')">
</div>

</div>


  <div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
 <input type="checkbox" name="presentcontact" value="same" style="    margin-left: -98px;" id="sameaspermanentadress">
  <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>

</div>








  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#shippingAddress" style="    width: 100%;
    margin-left: -1px;" >Shipping Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
  </button>
</div>
<div class="col-sm-12 collapse" id="shippingAddress">
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 1</label>
    <input type="text"    rows="5" name="SAddress1" id="SAddress1" value="@yield('editSAddress1')">
  </div>
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 2</label>
    <input type="text"  rows="5" name="SAddress2" id="SAddress2" value="@yield('editSAddress2')">
  </div>
  <div class="col-sm-4" style="    margin-top: 20px;">
    <label>City</label>
    <input type="text"  rows="5" name="SCity" id="SCity" value="@yield('editSCity')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>District</label>
  <input type="text"  rows="5" name="SDistrict" id="SDistrict" value="@yield('editSDistrict')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>State</label>
<!--   <input type="text"  rows="5" name="SState" id="SState" value="@yield('editSState')"> -->
   <select name="SState" id="SState" value="@yield('editSState')">
        <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
        <option value="Chhattisgarh">Chhattisgarh</option>
        <option value="Chandigarh">Chandigarh</option>
        <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
        <option value="DamanandDiu">Daman and Diu</option>
        <option value="Delhi">Delhi</option>
        <option value="Goa">Goa</option>
        <option value="Haryana">Haryana</option>
         <option value="HimachalPradesh">Himachal Pradesh</option>
          <option value="JammuandKashmir">Jammu and Kashmir</option>
           <option value="Jharkhand">Jharkhand</option>
            <option value="Karnataka">Karnataka</option>
             <option value="Kerala">Kerala</option>
              <option value="MadhyaPradesh">Madhya Pradesh</option>
               <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                 <option value="Meghalaya">Meghalaya</option>
                  <option value="Mizoram">Mizoram</option>
                   <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                     <option value="Punjab">Punjab</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Rajasthan">Rajasthan</option>
                      <option value="Pondicherry">Pondicherry</option>
                      <option value="Sikkim">Sikkim</option>
                      <option value="TamilNadu">Tamil Nadu</option>
                      <option value="Tripura">Tripura</option>
                       <option value="UttarPradesh">Uttar Pradesh</option>
                        <option value="Uttarakhand">Uttarakhand</option>
                         <option value="WestBengal">West Bengal</option>

      </select>
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>Pincode</label>
  <input type="text"  rows="5" name="SPinCode" id="SPinCode" value="@yield('editSPinCode')">
</div>
</div>


<div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
 <input type="checkbox" name="shippingcontact" value="same" style="    margin-left: -98px;" id="shippingsameaspermananentaddress">
  <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>

</div>



  </div>
</div>
</div>

<!-- Product Deatials -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ProductDetails"  >Product Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;     ">

  <div class="row collapse" id="ProductDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px;">
     <div class="col-sm-3" id="firstrow">
     <label>SKU ID</label><input type="text"  rows="5" name="SKUid" id="SKUid"  value="@yield('editSKUid')">
</div>
    <div class="col-sm-3" id="firstrow">

      <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName"  list="Products" value="@yield('editProductName')">
 <datalist id="Products">

@foreach($products as $products)
 <option value="{{ $products->Name}}" label="&#x20B9;{{ $products->Price}}">
@endforeach
  </datalist>

    </div>

    <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

      <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>
      <select name="Type" id="Type" >
        <option value="@yield('editType')">@yield('editType')</option>
        <option value="Sell">Sell</option>
        <option value="Rent">Rent</option>
      </select>

    </div>
    <div class="col-sm-3" id="firstrow">

      <label>Demo Required</label>

      <select name="DemoRequired" id="DemoRequired">
        <option value="@yield('editDemoRequired')">@yield('editDemoRequired')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>

    </div>
<div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="@yield('editAvailabilityStatus')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="@yield('editAvailabilityAddress')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Quantity</label><input type="text"  rows="5" name="Quantity" id="Quantity" value="@yield('editQuantity')">
    </div>

    <div class="col-sm-12"></div>

 <div id="sell">
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="@yield('editSellingPrice')">
    </div>



    <div class="col-sm-3" style="margin-top: 26px;">
     <label>Mode of Payment</label>
     <select name="ModeofPayment" id="ModeofPayment">
        <option value="@yield('editModeofPayment')">@yield('editModeofPayment')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div>
  <!--   <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="OrderStatus" id="OrderStatus">
        <option value="@yield('editOrderStatus')">@yield('editOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->

   </div>

<div id="rent">
    <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

      <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="@yield('editRentalPrice')">
    </div>



    <div class="col-sm-3" style="margin-top: 26px;">
     <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt" id="AdvanceAmt" value="@yield('editAdvanceAmt')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>Start Date</label><input type="date"  rows="5" name="StartDate" id="StartDate" value="@yield('editStartDate')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>End Date</label><input type="date"  rows="5" name="EndDate" id="EndDate" value="@yield('editEndDate')">
    </div>
    <div class="col-sm-12"></div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>Mode of Payment</label>
     <select name="ModeofPaymentrent" id="ModeofPaymentrent">
        <option value="@yield('editModeofPaymentrent')">@yield('editModeofPaymentrent')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>
        <option value="Cheque">Cheque</option>

      </select>
    </div>


 <!--     <div class="col-sm-3" style="margin-top: 26px;">

      <label>Order Status</label>
    <select name="OrderStatusrent" id="OrderStatusrent">
        <option value="@yield('editOrderStatusrent')">@yield('editOrderStatusrent')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
      </select>

    </div> -->

     <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Overdue Amount</label><input type="text"  rows="5" name="OverdueAmt" id="OverdueAmt" value="@yield('editOverdueAmt')">
    </div> -->
</div>

 <input type="hidden" name="nooffileds" id="value">
    <!-- Add inut flieds -->

<div class="col-sm-12">
  <div id="moreinput"></div>
</div>
<div class="col-sm-12" style="text-align: right;">
  <button type="button"  id="addmoreinputfiled" style="outline: none; background: white;
    border: 0px;">
  <img src="/img/Add-32.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
</div>




  </div>
</div>





<!-- Pharmacy Details -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PharmacyDetails"  >Pharmacy Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;     ">

  <div class="row collapse" id="PharmacyDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px;">

    <div class="col-sm-3" id="firstrow">

      <label>Medicine Name</label><input type="text"  rows="5" name="MedName" id="MedName" value="@yield('editMedName')">
    </div>
     <div class="col-sm-3" id="firstrow">

      <label>Strength</label><input type="text"  rows="5" name="Strength" id="Strength" value="@yield('editStrength')">
    </div>

    <div class="col-sm-3" id="firstrow" >

      <label>Type </label>
      <select name="MedType" id="MedType" >
        <option value="@yield('editMedType')">@yield('editMedType')</option>
        <option value="Tablet">Tablet</option>
        <option value="Capsule">Capsule</option>
        <option value="Injection">Injection</option>
        <option value="Ointment">Ointment</option>
        <option value="Scrub">Scrub</option>
         <option value="Cream">Cream</option>
         <option value="Powder">Powder</option>
         <option value="Lotion">Lotion</option>
         <option value="Syrum">Syrum</option>
         <option value="Drops">Drops</option>
         <option value="Gel">Gel</option>
         <option value="Expectorant">Expectorant</option>
         <option value="Respules">Respules</option>
         <option value="Handrub">Handrub</option>
         <option value="Syrup">Syrup</option>


      </select>

    </div>
    <div class="col-sm-3" id="firstrow">
     <label>Quantity</label><input type="text"  rows="5" name="pQuantity" id="pQuantity" value="@yield('editpQuantity')">
    </div>



    <div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="pAvailabilityStatus" id="pAvailabilityStatus" value="@yield('editpAvailabilityStatus')">
    </div>




    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice" id="pSellingPrice" value="@yield('editpSellingPrice')">
    </div>

  <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Mode of payment</label>
      <select name="PModeofpayment" id="PModeofpayment">
        <option value="@yield('editPModeofpayment')">@yield('editPModeofpayment')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div>

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="pOrderStatus" id="pOrderStatus">
        <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->




<input type="hidden" name="nooffiledsPharmacyDetails" id="nooffiledsPharmacyDetails" value="">
    <!-- Add inut flieds -->

<div class="col-sm-12">
  <div id="moreinputPharmacyDetails"></div>
</div>
<div class="col-sm-12" style="text-align: right;">
  <button type="button"  id="addmoreinputfiledPharmacyDetails" style="outline: none; background: white;
    border: 0px;">
  <img src="/img/Add-32.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
</div>

  </div>

</div>

  </div>

</div>




<!-- checkbox to add service details with product -->
<div style="margin-left: 5%; margin-top: 20px;" id="toggle">
<label class="switch">
  <input type="checkbox" name="addservice" value="add" style="" id="addservice">
  <span class="slider round"></span>

</label>&emsp;
<label>Add Services along with it..</label>

</div>

<!-- end -->

<?php $loginname=$_GET['name'];

?>

<input type="hidden" name="loginname" value="<?php echo $loginname;?>">

    <div class="container" style="    margin-top: 32px;">
          <div class="row">
                <div class="col-sm-12">

                   <center><button type="reset" class="btn btn-danger" style="    background-color: #ff3547;" >Reset</button>
              &emsp;
                <button type="submit" class="btn btn-default" id="submitt" >Submit</button>
                   </center>
                </div>
          </div>
    </div>
  </div>
</div>
@include('partial.errors')
</form>

</body>

@endsection
