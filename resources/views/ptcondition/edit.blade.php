

@section('editid', $ptcondition->id)
@section('editconditiontypes',$ptcondition->conditiontypes)

@section('editMethod')
{{method_field('PUT')}}
@endsection 


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
  <!--         <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png"> -->
   <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('[data-toggle="popover"]').popover({ animation:true,  html:true}); 
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {
         
           $("#usersetting").popover('hide');
          
        }else
        {
           $("#usersetting").popover('show');
          
          //console.log(sidenvavv);
        }
    }); 
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
          }); 
    </script>
     <style type="text/css">
        body
        {
            background:white;
        }
        @media only screen and (max-width: 1200px) {
    .imgg
    {
         padding-left: 2px;

    }
    .navbar-header
    {
      padding-left: 17px;
    }
    .logo
    {
       /* text-align: center;*/
       margin-left: 36%;
    }
}

body::-webkit-scrollbar 
{
  display: none;
}

body
{
    overflow-y: scroll;
  overflow-x: hidden;
  font-family: Raleway,sans-serif;
}

@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}

      .btn
{
        color: #FFF!important;
    background-color: green;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
      .imgg
      {
                 margin-top: -13px;
    margin-left: -70px;
            max-width: 167px;
   
      }
      .navbar {
    position: relative;
    min-height: 64px;
    margin-bottom: 20px;
    border: 1px solid transparent;
}

.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
li
{
   font-family: myFirstFont;
    font-size: 14px;
    color: #777;
        padding-top: 14px;
    padding-bottom: 14px;
    line-height: 22px;
}

.footer {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
  color: #636b6f;
}
input
{
  border-right: 0px;  
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px; 
    border-bottom:1px solid #484e51;
    padding-bottom: 0px; 
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
label
{
      color: #777;
}


    </style>

    <style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    right: 0;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
        margin-left: -40px;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
    

.navbar-right
{
   text-align: -webkit-right; 
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
    font-family: myFirstFont;
}
.navbar-right
{
   text-align: -webkit-right; 
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;
   display: none;
    z-index: 10000;
    position: fixed;
}
.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top: 9%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }

}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) { 
  .submit
  {
        margin-top: 30px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top: 9%;
  }
 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
 
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
    
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 109%;
    margin-left: -14px;
  }

  }
   /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  .submit
  {
        margin-top: 22px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
 #logo
  {
        margin-left: 27%;
  }
  
 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 110%;
    margin-left: -14px;
  }

}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
     #logo
  {
        margin-left: 27%;
  }
 
 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }
  }
</style>
</head>
@extends('layout.app')
@section('body')
<br>

<div class="navbar navbar-default navbar-fixed-top" style="background-color: white;height: 77px;">
    <div class="container">

        <div class="navbar-header">
          <!--   <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <div id="logo">
            <a class="navbar-brand" rel="home" href="/admin/home">
                <img class="imgg" 
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>
       
         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                       

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="\ptconditions">Back</a>
                                    
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>

    </div>
</div>


<!-- Update tabel view ends -->
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\ptconditions' title='' style='color:black'>Back</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">

  <div class="container" style="    margin-top: 133px;">
        <div class="row">
              <div class="col-sm-6 col-sm-offset-3 accepting">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 style="color:#636b6f;text-align: center;">   Update </h4>

                          </div>
                          <div class="panel-body">
                            <form class="form-horizontal" action="/ptconditions/@yield('editid')" method="POST">
{{csrf_field()}}
@section('editMethod')
@show
  <fieldset>
                           <div class="col-sm-8 col-sm-offset-2">
                              

                               
                                 <label>Condition</label>
                            <input type="text"  rows="5" name="conditiontypes" id="conditiontypes" value="@yield('editconditiontypes')" required >
                             <center>   <button type="submit" class="btn btn-default" style="   background: #00e25b;     margin-top: 30px;">Update</button></center>

                      </fieldset>
</form>

                          </div>
                      </div>
              </div> 
          
        </div>
    
  </div>
  </div>

 
@include('partial.errors')
@endsection