<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">



  <link rel="stylesheet" href="intlTelInput.css">
  <link rel="stylesheet" href="demo.css">


<script>
$(document).ready(function(){


  var value=$('#LabOrRadiologicalReport').val();
     if(value == "Yes")
    {
      $("#remark").show();
    }else
    {
      $("#remark").hide();
    }



    $("#LabOrRadiologicalReport").change(function(){
    var value= $("#LabOrRadiologicalReport").val();
    if(value == "Yes")
    {
      $("#remark").show();
    }else
    {
      $("#remark").hide();
    }
});



    var value=$('#AffectedArea').val();
    if(value == "Upper Limb" || value == "Lower Limb" || value == "Back" || value == "Face")
    {
      $("#remark1").show();
    }else
    {
      $("#remark1").hide();
    }


    $("#AffectedArea").change(function(){
    var value= $("#AffectedArea").val();

    if(value == "Upper Limb" || value == "Lower Limb" || value == "Back" || value == "Face")
    {
      $("#remark1").show();
    }else
    {
      $("#remark1").hide();
    }
});





var value=$('#PainPattern').val();
     if(value == "Agravating" || value == "Alleviating Factor" || value == "Night Pain" || value == "Intensity" || value == "Duration")
    {
      $("#remark2").show();
    }else
    {
      $("#remark2").hide();
    }

     $("#PainPattern").change(function(){
    var value= $("#PainPattern").val();


    if(value == "Agravating" || value == "Alleviating Factor" || value == "Night Pain" || value == "Intensity" || value == "Duration")
    {
      $("#remark2").show();
    }else
    {
      $("#remark2").hide();
    }
});






var value=$('#ExaminationReport').val();
      if(value == "Yes")
    {
      $("#remark3").show();
    }else
    {
      $("#remark3").hide();
    }


     $("#ExaminationReport").change(function(){
    var value= $("#ExaminationReport").val();


    if(value == "Yes")
    {
      $("#remark3").show();
    }else
    {
      $("#remark3").hide();
    }
});



var value=$('#assigned').val();
      if(value != "")
    {
      $("#remark4").show();
    }else
    {
      $("#remark4").hide();
    }


     $("#assigned").change(function(){
    var value= $("#assigned").val();


    if(value != "")
    {
      $("#remark4").show();
    }else
    {
      $("#remark4").hide();
    }
});


});
</script>





<!-- <script>
$(document).ready(function(){

    

    var value=$('#AffectedArea').val();
      if(value == "")
      {
        $('#comment1').prop('disabled',true);
      }else
      {
        $('#comment1').prop('disabled',false);
      }
       $('#AffectedArea').change(function()
    {

      var value=$('#AffectedArea').val();
      if(value == "")
      {
        $('#comment1').prop('disabled',true);
      }else
      {
        $('#comment1').prop('disabled',false);
      }
    
          
    });
});
</script> -->

  
</head>
<body>
        <div class="container">
        <div class="row">
        @section('body')
        @show

        </div>
        </div>

@extends('layouts.footer')

</body>

</html>
