
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $(".id").click(function(event){
            var elem = $(this);
            var id=elem.attr('id');
        event.preventDefault();
      
       $('#myModal').modal('show');
       var buttons = document.getElementById("dismiss");

        buttons.onclick = function(e) {
        document.getElementById(id).submit();
    };
    });

 $('[data-toggle="popover"]').popover({ animation:true,  html:true}); 
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {
         
           $("#usersetting").popover('hide');
          
        }else
        {
           $("#usersetting").popover('show');
          
          //console.log(sidenvavv);
        }
    }); 
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
   
});
</script>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
     <style type="text/css">
        body
        {
            background:white;
        }
        @media only screen and (max-width: 1200px) {
    .imgg
    {
         padding-left: 2px;

    }
    .navbar-header
    {
      padding-left: 17px;
    }
}
body::-webkit-scrollbar 
{
  display: none;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.6;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
body
{
    overflow-y: scroll;
  overflow-x: hidden;
  font-family: Raleway,sans-serif;
}

@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
      .imgg
{
   margin-top: -13px;
    margin-left: -70px;
    max-width: 167px;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
      .navbar {
    position: relative;
    min-height: 64px;
    margin-bottom: 20px;
    border: 1px solid transparent;
}

.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
li
{
   font-family: myFirstFont;
    font-size: 14px;
    color: #777;
        padding-top: 14px;
    padding-bottom: 14px;
    line-height: 22px;
}
h3
{
	color:#636b6f;;
}
.footer {
    position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
}
    </style>
        <style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    right: 0;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}
a
{
    text-decoration: none;
}
.dropdown-content a {
    color: black;
        margin-left: -40px;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1;text-decoration: none;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
.add
{
  text-align: right;
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
    font-family: myFirstFont;
}
.navbar-right
{
   text-align: -webkit-right; 
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;
   display: none;
    z-index: 10000;
    position: fixed;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top: 20%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top:6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }

.add
{
      float: left;
    margin-left: 36%;
    margin-top: 10px;
}

  }
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) { 
  .submit
  {
        margin-top: 30px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top:20%;
  }
 #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
 
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
    
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 109%;
    margin-left: -14px;
  }
.add
{
      float: left;
    margin-left: 36%;
    margin-top: 10px;
}

}
 /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  .submit
  {
        margin-top: 22px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 20%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
 #logo
  {
        margin-left: 27%;
  }
  
 #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 110%;
    margin-left: -14px;
  }

.add
{
      float: left;
    margin-left: 36%;
    margin-top: 10px;
}
}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 20%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
     #logo
  {
        margin-left: 27%;
  }
 
 #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }

.add
{
      float: left;
    margin-left: 36%;
    margin-top: 10px;
}
  }
@media screen and (max-height: 1200px) {
    #loo
    {
          margin-left: 36%;
    }
}
</style>
</head>
<body >

    <div id="app">
                <div class="navbar navbar-default navbar-fixed-top" style="text-align: right;">
    <div class="container">

        <div class="navbar-header">
          <!--   <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <div id="loo">
            <a class="navbar-brand" rel="home" href="/admin/home" >
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

                        @else
                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>


                                <ul class="dropdown-content">

                                  <a href="\updatetables">Back</a>
  <!-- <a href="\version">Version Notes</a> -->
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>
<!-- Update tabel view ends -->
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\updatetables' title='' style='color:black'>Back</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">

	<div class="container">
			<div class="row al">
					<div class="col-sm-12" >
						<center><h3> Shift Required Table Details </h3></center>
						
					</div>
				
			</div>
	</div>
    <div class="container">
      <div class="row">
          <div class="col-sm-12 add" >
           
            <a href="/shiftrequireds/create"  style="font-size: 15px;float: right;" data-toggle="tooltip" title="Add New Shift" data-placement="bottom"> <img src="/img/add.png" class="img-responsive" alt="add"> </a> 
          </div>
        
      </div>
  </div>
@include('partial.message')
<div class="container">


		<div class="container" style="    margin-top: 37px;">
 				<div class="row">
 							<div class="col-sm-8 col-sm-offset-2">

 										<table class="table">
    											<thead>
      													<tr>
        													<th>Id</th>
        													<th>Required Shifts</th>
        													<th>Action</th>
      													</tr>
    											</thead>
    											<tbody>
      												 @foreach ($shiftrequireds as $shiftrequired)
      													<tr>
        													<td><a href="{{'/shiftrequireds/'.$shiftrequired->id.'/edit'}}">{{$shiftrequired->id}}</a></td>
       														 <td>{{$shiftrequired->shiftrequired}}</td>
        													
        												


                                    <td>
                                  <div style="    display: -webkit-inline-box;">

 <form action="{{'/shiftrequireds/'.$shiftrequired->id}}" method="post" id="{{$shiftrequired->id}}">
{{csrf_field()}}
{{ method_field('DELETE') }}

<!-- <input type="button"   value="Delete" class="btn btn-default id" style="background: #ff4f5e" id="{{$shiftrequired->id}}" >
 -->
<input type="button"   value="" class="id" style=" background: url(/img/delete.png) no-repeat;
                cursor:pointer;    width: 29px;    height: 35px ;
                border: none;" id="{{$shiftrequired->id}}" data-toggle="tooltip" title="Delete" data-placement="bottom">
</form> &emsp;

                                 
                                  </div>

                                  </td>
     													 </tr>
      												@endforeach
    											</tbody>
  										</table>
 								
 							</div>
 					
 				</div>
           
  
		</div>



    </div>
  <!-- Footer  -->
  @extends('layouts.footer');
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
       
        <div class="modal-body">
          <p>Are you sure to Delete ?  <button type="button" class="btn btn-default" data-dismiss="modal" id="dismiss" style="margin-left: 16px;">Yes</button></p>
        </div>
       
      </div>
      
    </div>
  </div>
</div>
</div>
</body>
</html>