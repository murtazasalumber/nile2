<!-- This page acts as a version of the View Leads page 123 -->


<script>
$(document).ready(function(){
  $('.footable').footable();



});
</script>
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td style="color:red;">{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td style="color:red;">{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "AssignedTo" || $filter1 === "PAssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      
     <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
   @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td style="color:red;"> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td style="color:red;">{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
    </tr>
    @endforeach
  </tbody>
</table>


@elseif ($filter1 === "AssignedTO")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
     <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
   @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td style="color:red;">{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
    </tr>
    @endforeach
  </tbody>
</table>





@elseif ($filter1 === "orderid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
     <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
    @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td style="color:red;">{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td style="color:red;">{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "MobileNumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
     <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
    @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif



    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "PorderStatus" || $filter1 === "OrderStatus" || $filter1 === "OrderStatusrent")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
     <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
  @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td style="color:red;">{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td style="color:red;">{{$lead->OrderStatus}}</td>
@else
  <td style="color:red;">{{$lead->OrderStatusrent}}</td>
@endif


@endif
    </tr>
    @endforeach
  </tbody>
</table>



      @elseif ($filter1 === "OrderStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                    <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>order id</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
 @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td style="color:red;">{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td style="color:red;">{{$lead->OrderStatus}}</td>
@else
  <td style="color:red;">{{$lead->OrderStatusrent}}</td>
@endif


@endif


                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


 @elseif ($filter1 === "leads.id")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                    <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
 @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td style="color:red;">{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td style="color:red;">{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td >{{$lead->OrderStatus}}</td>
@else
  <td >{{$lead->OrderStatusrent}}</td>
@endif


@endif


                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


@elseif ($filter1 === "type")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                    <th  data-hide="phone,tablet"><b>S.no</b></th>
                                   <th  data-hide="phone,tablet"><b>Order ID</b></th>
                                   <th  data-hide="phone,tablet"><b>Lead ID</b></th>
                                     <th  data-hide="phone,tablet"><b>Product Type</b></th>
                                     <th  data-hide="phone,tablet"><b>Created At</b></th>
                                      <th  data-hide="phone,tablet"><b>Created By</b></th>
                                      <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                                     <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                      <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                      <th data-hide="phone,tablet" ><b>Status</b></th>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
 @if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td>{{$lead->id}}</td>
   <td>{{$lead->orderid}}</td>
  <td >{{$lead->leadid}}</td>
  <td style="color:red;">Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td>{{$lead->id}}</td>

<td>{{$lead->orderid}}</td>
<td style="color:red;">{{$lead->leadid}}</td>

  <td style="color:red;">Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td >{{$lead->OrderStatus}}</td>
@else
  <td >{{$lead->OrderStatusrent}}</td>
@endif


@endif


                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

             

<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
@endif
