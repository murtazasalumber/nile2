 <input type="text"  rows="5" name="clientfname" id="clientfname" value="{{$fName}}" >
      <input type="text"   rows="5" name="clientmname" id="clientmname" value="@yield('editclientmname')">

      <input type="text"  rows="5"  name="clientlname" id="clientlname" value="@yield('editclientlname')" >

      <input type="text"  rows="5" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

   
      <select  id="country"  name="code" >

                <option value="+91" name="country_name">India</option>
                @for($i=0;$i<$count;$i++)
                <option value="{{$country_codes[$i]['dial_code']}}" >{{ $country_codes[$i]['name']}}</option>
                @endfor
                <!-- <option value="+1">US</option>
                <option value="+44 ">UK</option> -->
            </select>
    
                <input type="text"  id="phone1"  rows="5" required disabled>
            
                  <input type="text"  id="phone"  rows="5" name="clientmob" value="@yield('editclientmob')" maxlength="10" required>
             

      <input type="text" rows="5" name="clientalternateno" id="clientalternateno" value="@yield('editclientalternateno')">
  
      <input type="text" rows="5" name="EmergencyContact" id="EmergencyContact" value="@yield('editEmergencyContact')" maxlength="13">
 
      <select name="assesmentreq" id="assesmentreq">
        <option value="@yield('editassesmentreq')">@yield('editassesmentreq')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>
 
      <select name="reference" id="reference"  required>
        <option value="@yield('editreference')">@yield('editreference')</option>
        @foreach($reference as $reference)
        <option value="{{ $reference->Reference}}">{{ $reference->Reference}}</option>
        @endforeach
      </select>
   
        <input type="text"   rows="5" name="source" id="source" value="@yield('editsource')">
  
      <input type="text"   rows="5" name="Address1" id="Address1" value="@yield('editAddress1')">

      <input type="text"   rows="5" name="Address2" id="Address2" value="@yield('editAddress2')">

      <select  id="City"  name="City" >

          <option value="@yield('editCity')">@yield('editCity')</option>
          @for($i=0;$i<$count1;$i++)
          <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
          @endfor
          <!-- <option value="+1">US</option>
          <option value="+44 ">UK</option> -->
      </select>

    

  
    <input type="text"    rows="5" name="District" id="District" value="@yield('editDistrict')">

  
      <select name="State" id="State" value="@yield('editState')">
        <option value="@yield('editState')">@yield('editState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
    
      </select>

  
    <input type="text"    rows="5" name="PinCode" id="PinCode" value="@yield('editPinCode')" maxlength="8">

  

    <input type="text"    rows="5" name="PAddress1" id="PAddress1" value="@yield('editPAddress1')">
  
    <input type="text"  rows="5" name="PAddress2" id="PAddress2" value="@yield('editPAddress2')">
  
    <select  id="PCity"  name="PCity" >

        <option value="@yield('editPCity')">@yield('editPCity')</option>
        @for($i=0;$i<$count1;$i++)
        <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
        @endfor
        <!-- <option value="+1">US</option>
        <option value="+44 ">UK</option> -->
    </select>

  
  <input type="text"  rows="5" name="PDistrict" id="PDistrict" value="@yield('editPDistrict')">

      <select name="PState" id="PState" value="@yield('editPState')">
        <option value="@yield('editPState')">@yield('editPState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
      
              </select>

  <input type="text"  rows="5" name="PPinCode" id="PPinCode" value="@yield('editPPinCode')">

  <input type="checkbox" name="presentcontact" value="same" style="    margin-left: -98px;" id="sameaspermanentadress">
  
    <input type="text"    rows="5" name="EAddress1" id="EAddress1" value="@yield('editEAddress1')">
 
    <input type="text"  rows="5" name="EAddress2" id="EAddress2" value="@yield('editEAddress2')">

    <select  id="ECity"  name="ECity" >

        <option value="@yield('editECity')">@yield('editECity')</option>
        @for($i=0;$i<$count1;$i++)
        <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
        @endfor
        <!-- <option value="+1">US</option>
        <option value="+44 ">UK</option> -->
    </select>

   
  <input type="text"  rows="5" name="EDistrict" id="EDistrict" value="@yield('editEDistrict')">

      <select name="EState" id="EState" value="@yield('editEState')">
        <option value="@yield('editEState')">@yield('editEState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
      
                         </select>

  <input type="text"  rows="5" name="EPinCode" id="EPinCode" value="@yield('editEPinCode')">

  <input type="checkbox" name="emergencycontact" value="same" style="    margin-left: -98px;" id="Esameaspermananentaddress">
  
  <input type="text"  rows="5" name="patientfname" id="patientfname" value="@yield('editpatientfname')">

<input type="text"  rows="5" name="patientmname" id="patientmname" value="@yield('editpatientmname')">
   
   <input type="text"  rows="5" name="patientlname" id="patientlname" value="@yield('editpatientlname')">
   
   <input type="text"  rows="5" name="age" id="age" value="@yield('editage')">
   

      <select name="gender" id="gender" >
        <option value ="@yield('editgender')">@yield('editgender')</option>
        @foreach($gender as $gender)
        <option value="{{ $gender->gendertypes}}">{{ $gender->gendertypes}}</option>
        @endforeach
      </select>

  

      <select name="relationship" id="relationship" >
        <option value="@yield('editrelationship')">@yield('editrelationship')</option>
        @foreach($relation as $relation)
        <option value="{{ $relation->relationshiptype}}">{{ $relation->relationshiptype}}</option>
        @endforeach
      </select>
    
    <input type="text"  rows="5" name="Occupation" id="Occupation" value="@yield('editOccupation')">
    
    <input type="text"  rows="5" name="aadhar" id="aadhar" value="@yield('editaadhar')">
    

      <select name="AlternateUHIDType" id="AlternateUHIDType"  value="@yield('editAlternateUHIDType')">
        <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
        <option value="PAN Card">PAN Card</option>
        <option value="Driving Licence">Driving Licence </option>
        <option value="Passport">Passport</option>
        <option value="Bank Passbook">Bank Passbook</option>
        <option value="Voter ID">Voter ID</option>
        <option value="Cellphone Postpaid Bill">Cellphone Postpaid Bill</option>
        <option value="Water Bill">Water Bill</option>
        <option value="Electricity Bill">Electricity Bill</option>
        <option value="Telephone Bill">Telephone Bill</option>
        <option value="Gas Consumer Number">Gas Consumer Number</option>
        <option value="Employee ID issued by defence">Employee ID issued by defence</option>

      </select>

    
    <input type="text"  rows="5" name="AlternateUHIDNumber" id="AlternateUHIDNumber" value="@yield('editAlternateUHIDNumber')">
    

      <select name="PTAwareofDisease" id="PTAwareofDisease"  value="@yield('editPTAwareofDisease')">
        <option value="@yield('editPTAwareofDisease')">@yield('editPTAwareofDisease')</option>
        <option value="No">No</option>
        <option value="Yes">Yes</option>
      </select>



                            <select name="servicetype" id="servicetype" required>
                              <option value="@yield('editservicetype')">@yield('editservicetype')</option>
                            @foreach($vertical as $vertical)
                            <option value="{{ $vertical->verticaltype}}">{{ $vertical->verticaltype}}</option>
                            @endforeach
                           </select>



                            <select name="branch" id="branch" required>
                            <option value="@yield('editbranch')">@yield('editbranch')</option>
                              @foreach($branch as $branch)
                              <option value="{{ $branch->name}}">{{ $branch->name}}</option>
                              @endforeach
                             </select>



                           <select  id="assignedto" >
                            <option value="@yield('editassignedto')">@yield('editassignedto')</option>
            @foreach($emp as $emp1)
            <option value="{{ $emp1->FirstName}}">{{ $emp1->FirstName}}  {{ $emp1->Designation}}</option>
            @endforeach
           </select>

         
                            <select name="GeneralCondition" id="GeneralCondition" >
                            <option value="@yield('editGeneralCondition')">@yield('editGeneralCondition')</option>
                              @foreach($condition as $condition)
                              <option value="{{ $condition->conditiontypes}}">{{ $condition->conditiontypes}}</option>
                              @endforeach
                            </select>

       <input type="date"  rows="5" name="requesteddate" id="requesteddate" value="@yield('editrequesteddate')">

         
                            <select name="preferedgender" id="preferedgender" >
                            <option value="@yield('editpreferedgender')">@yield('editpreferedgender')</option>
            @foreach($gender1 as $gender1)
            <option value="{{ $gender1->gendertypes}}">{{ $gender1->gendertypes}}</option>
            @endforeach
           </select>
        
                            <select name="preferedlanguage" id="preferedlanguage" >
                            <option value="@yield('editpreferedlanguage')">@yield('editpreferedlanguage')</option>
                            @foreach($language as $language)
            <option value="{{ $language->Languages}}">{{ $language->Languages}}</option>
            @endforeach
           </select>

<input type="text"  rows="5" name="quotedprice" id="quotedprice" value="@yield('editquotedprice')">

<input type="text"  rows="5" name="expectedprice" id="expectedprice" value="@yield('editexpectedprice')">

                            <textarea class="form-control" rows="5" col="20" name="remarks" id="remarks" value="@yield('editremarks')">@yield('editremarks')</textarea>
 