<!-- This page acts as a version of the View Leads page -->
<!-- This page will show the filtered content in Search functionality -->
<script>
$(document).ready(function(){
    $('.footable').footable();



});
</script>
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

<!-- If the filter value says "FirstName", do this -->
@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>
            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
<?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>



        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td >{{$lead->AssignedTo}}</td>
            <!-- if "fname" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <td>{{$lead->Source}}</td>
            <td>{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

        </tr>
        @endforeach
    </tbody>
</table>

<!-- If the filter value says "Assigned To", do this -->
@elseif ($filter1 === "AssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>
            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>
        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <!-- if "Assigned to" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;" >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <td>{{$lead->Source}}</td>
            <td>{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

        </tr>
        @endforeach
    </tbody>
</table>

<!-- If the filter value says "Lead Source(like Just Dial)", do this -->
@elseif ($filter1 === "Source")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>
            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php

                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <!-- if "Lead Source(like Just Dial)" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->Source}}</td>
            <td>{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>

<!-- If the filter value says "ServiceType(like Nursing Services, PSC)", do this -->
@elseif ($filter1 === "ServiceType")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <!-- if "ServiceType(like Nursing Services, PSC)" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

        </tr>
        @endforeach
    </tbody>
</table>

<!-- If the filter value says "Branch", do this -->
@elseif ($filter1 === "Branch")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <!-- if "Branch" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

        </tr>
        @endforeach
    </tbody>
</table>


<!-- If the filter value says "Service Status (like New, In Progress)", do this -->


@elseif ($filter1 === "ServiceStatus")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td >{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
            <!-- if "Service Status (like New, In Progress)" is selected by the user as filter, then highlight this column completely as "Red" color -->

<?php

                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td style="color:red;">{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>

<!-- If the filter value says "Mobile Number", do this -->
@elseif ($filter1 === "MobileNumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <!-- if "Mobile Number" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td >{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>
@elseif ($filter1 == "leads.Alternatenumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}}</td>
            <!-- if "Mobile Number" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td >{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>

@elseif ($filter1 === "leads.id")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                   if($status1==NULL)
                   {
                    ?>
                     <th data-hide="phone,tablet" ><b>NULL Reason</b></th>
                    <?php
                   }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td style="color:red;">{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <!-- if "Mobile Number" is selected by the user as filter, then highlight this column completely as "Red" color -->
            <td >{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td >{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>
@elseif ($filter1 === "AssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
        <tr>

            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

        </tr>
    </thead>
    <tbody>
        @foreach($data1 as $lead)
        <tr>
            <?php
            if(session()->has('name'))
            {
                $name=session()->get('name');
            }else
            {
                if(isset($_GET['name'])){
                    $name=$_GET['name'];
                }else{
                    $name=NULL;
                }
            }
            ?>
            <td>{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td  style="color:red;" >{{$lead->AssignedTo}}</td>
            <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td >{{$lead->Branch}}</td>
            <td >{{$lead->Source}}</td>
            <td >{{$lead->ServiceType}}</td>
<?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




        </tr>
        @endforeach
    </tbody>
</table>


@endif


<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
