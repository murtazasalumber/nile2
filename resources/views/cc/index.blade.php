  <!-- This page is displayed when the View Leads button is clicked by the customer care-->

<?php
$value= Session::all();

$value=session()->getId();
//echo $value;


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
  <link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
  <script src="js/footable.js" type="text/javascript"></script>

  <script>
  $(document).ready(function(){
    $('.footable').footable();
     $('[data-toggle="popover"]').popover({ animation:true,  html:true});
  $('[data-toggle="tooltip"]').tooltip();
    $("#keyword").keyup(function(){

      // take the value of the selected and input by user
      var keyword = $('#keyword').val();
      var filter = $('#filter').val();
      var status = $('#status').val();


      $.ajax({
        type: "GET",
        // this is the route
        url:'allleadc' ,
        data: {'keyword1' : keyword , 'status1':status , 'filter1' : filter,'_token':$('input[name=_token]').val() },
        // This is coming from alldata view page
        success: function(data){
          $('#result').html(data);
        }
      });
    });

//when the user moves from one filter to another, then "empty" the keyword field
$("#filter").change(function(){

              $('#keyword').val("");
             });

  });
  </script>
  <style type="text/css">
  @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
    .footer {
  font-family: myFirstFont;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
  color: #636b6f;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.imgg {
       margin-top: -14px;
    margin-left: -62px;
}
body
{
  font-family: myFirstFont;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
@media screen and (max-height:1200px) {




}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}

@media (max-width: 1200px)
{

  .identy
  {
    padding-left: 7%!important;
  }
#filter
   {
        width: 44%;
        padding-top: 19px;
    margin-bottom: 17px;
   }
.keyword1
    {
    margin-right: 50px;
    }
#loo
{
  margin-left: 37%;
}
 #buttonforcreate
  {
      margin-top: 10%;
  }

   #productleadparent
   {
              margin-top: 12px;
    margin-left: 36%;
   }
  .imggg
  {
    text-align: center;
  }
   #filter
   {
        padding-top: 19px;
    margin-bottom: 17px;
   }
   #seacrh
   {
    margin-bottom: 37px;
   }
   #download
   {
    margin-top: 5%;
    margin-left: 24%;
    width: 53%;
   }
   #second
    {
              margin-bottom: 22px;
    margin-left: 36%;
    }
    #productl
    {
          margin-bottom: 16px;
    margin-left: 26%;
    }
}
.navbar2
{
  display: none;
}
  .popover.bottom>.arrow
    {
        display: none;
    }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
.footable.breakpoint > tbody > tr > td > span.footable-toggle {
    float: right;
    display: inline-block;
    font-family: 'footable';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    -webkit-font-smoothing: antialiased;
    padding-right: 5px;
    font-size: 14px;
    color: #888888;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px)  and (orientation: portrait)
{
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }
}
@media screen and (device-width: 640px) and (device-height:360px)  and (orientation: landscape)
{
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }
}

/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }


}
@media screen and (device-width: 732px) and (device-height:412px)  and (orientation: landscape)
{
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

  }
  /*Query for iphone 5*/
@media (min-device-width: 320px) and (max-device-height: 568px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }


}
@media (min-device-width: 568px) and (max-device-height: 320px) and (orientation: landscape){ 
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }
}
/*Query for iphone 6*/
@media (min-device-width: 375px) and (max-device-height: 667px) and (orientation: portrait){ 
.navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }

}
@media (min-device-width: 667px) and (max-device-height: 375px) and (orientation: landscape){ 
   .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

}
/*Iphone 6+*/
@media (min-device-width:736px) and (max-device-height: 414px) and (orientation: landscape){
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

}
/*Iphone X*/
@media (min-device-width: 375px) and (max-device-height: 812px) and (orientation: portrait){ 
.navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }

}
/*Ipad*/
@media (min-device-width: 768px) and (max-device-height: 1024px) and (orientation: portrait){ 
  .filter
  {
    margin-left: 7%;
    width: 29%!important;
  }
}
/*Nexus 7 (Tab)*/
@media (min-device-width: 600px) and (max-device-height: 960px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }
}
@media (min-device-width: 960px) and (max-device-height: 600px) and (orientation: landscape){ 
  .filter
  {
    margin-left: 7%;
    width: 29%!important;
  }
}
</style>
</head>
<body style="font-family: myFirstFont;">
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

      <div class="navbar-header">
      <!--   <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> -->
        <div id="loo">
        <a class="navbar-brand" rel="home" href="/admin/customercare" >
          <img class="imgg"
          src="/img/healthheal_logo.png">
        </a></div>
      </div>

      <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav navbar-right" style="text-align: right;">
          <!-- Authentication Links -->

          <!-- if the admin is not logged in show the login button-->
          @if (!Auth::guard('admin')->check())
          <li><a href="\admin">Login</a></li>
          <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

          @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
              {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-content">

              <a href="/admin/customercare" style="font-family: myFirstFont;"
              >
              Home
            </a>
            <a href="\version">Version Notes</a>
            <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Logout
          </a>


          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>

        </ul>


      </li>
      @endif
    </ul>
  </div>


</div>

</div>
</div>
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="bottom" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\admin\home' title='' style='color:black'>Home</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>




  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<!-- title -->
<div class="container">
  <div class="row title">
    <div class="col-sm-12" >
      <center> <h2> Leads</h2></center>
    </div>
  </div>

</div>

<!-- title Ends -->

<!-- sending a hidden value  -->

{{csrf_field()}}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!-- Search Section -->
<div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>

                         <option value="leads.id">Lead ID</option>
                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="Alternatenumber">Alternate Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select> 
                      <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>
  <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status; ?>">

               <div class="container">
    <div class="row imggg" >
        <div class="col-sm-3 col-sm-offset-5" id="icons">

            <!-- Link image to add a New Service Lead from the View Leads Page -->
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;" data-toggle="tooltip" title="Create New Service Lead" data-placement="bottom"></a>


   <!-- Link image to add a New Product Lead from the View Leads Page -->
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" > <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"  data-toggle="tooltip" title="Create New Product Lead" data-placement="bottom"></a>



             <?php

if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?>
  <input type="hidden" name="" id="status1" value="<?php echo $check; ?>">

<!-- Download for coordinator view Leads -->
@if($check=="All")
<a href="/customercaredownloadall?status=<?php echo $check;    ?>&download=true"><!-- <input type="button" name="download" value="Download" class="btn btn-primary" id="download" style="background: #4abde8"> --> <img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>
<!-- <input type="text" value="try"> -->
@else

<!-- Download for count view leads -->
    <a href="/customercaredownload?status=<?php echo $check;    ?>&download=true1"><!-- <input type="button" name="download" value="Download" id="download" class="btn btn-primary"  style="background: #4abde8"> --><img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;    height:35px;  display: -webkit-inline-box;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>

@endif


        </div>

    </div>
</div>
<!--   <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
           <div style="    margin-left: -69px;">
          <div class="col-sm-4" id="buttonforcreate">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="second" >
                                          Create Service Lead
                                        </a>&emsp;
                  <div id="productleadparent" style="    display: -webkit-inline-box;">
                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="procductl">
                                          Create Product Lead
                                        </a>
                                        </div>
                                        </div>
          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword'>
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status; ?>">
              </div>
              <div class="col-sm-2" >
            <?php

if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?>
 -->
<!-- Download for product manager view Leads -->
<!-- @if($check=="All")
<a href="/customercaredownloadall?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" id="download" value="Download" class="btn btn-primary" style="background: #4abde8"></a> -->
<!-- <input type="text" value="try"> -->
<!-- @else -->

<!-- Download for count view leads -->
  <!--   <a href="/customercaredownload?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" value="Download" id="download" class="btn btn-primary" style="background: #4abde8"></a>

@endif
          </div>

          </div>


  </div>

</div> -->


<!-- Search Section Ends-->


<!-- This is the URL generation code for download when the User clicks on the "Download" button on View Leads, or "Individual Count" Leads -->
<!-- If the status is present in URL, retrieve from there, else retrieve from session -->


<!-- Reslts -->
<div class="container-fluid" style="margin-top: 50px;" >
  <div class="row" >

    <!-- Overwrite whatever result was received from alldate.blade.php -->
    <div class="col-sm-12" id="result">
      <table class="table footable" style="font-family: Raleway,sans-serif;">
        <thead>
          <!-- This is for displaying the view data headings -->
          <tr>
            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <th data-hide="phone,tablet"><b>Source</b></th>
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
            <?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>


          </tr>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          <?php
                $result = count($leads);

                if($result <= 4)
                {?>

                    <style type="text/css">

                      .footer
                      {
                        margin-top: 20%;
                      }
                    </style>

                    @foreach ($leads as $lead)
          <tr>
            <?php
            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
            <td class="identy">{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td>{{$lead->AssignedTo}}</td>
            <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>

            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <td>{{$lead->Source}}</td>
            <td>{{$lead->ServiceType}}</td>
 <?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>


            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach

               <?php }
                else
                {
            ?>


          @foreach ($leads as $lead)
          <tr>
              <?php
              if(session()->has('name'))
              {
                $name=session()->get('name');
              }else
              {
              if(isset($_GET['name'])){
                 $name=$_GET['name'];
              }else{
                 $name=NULL;
              }
              }
              ?>
            <td class="identy">{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td>{{$lead->AssignedTo}}</td>
            <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>

            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>
            <td>{{$lead->Source}}</td>
            <td>{{$lead->ServiceType}}</td>
            <?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach
      <?php } ?>
    </tbody>
  </table>

  <!-- This is to ensure that the pagination links have parameters that were passed using "GET" for all pages -->
<div style="text-align: -webkit-center;"> {{$leads->appends(request()->except('page'))->links()}} </div>

</div>

</div>

</div>
@extends('layouts.footer')
</body>
</html>
