<!-- {{$countPharmacyDetails}} -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var count = {{$countPharmacyDetails}};
    var MedName1 ="MedName";
    var Strength1 ="Strength";
    var MedType1 ="MedType";
    var PQuantity1 ="PQuantity";


    var cautiontoentertype11 ="cautiontoentertype1";
    var cautiontoentertypepharmacytype11 ="cautiontoentertypepharmacytype";
    var cautiontoentertypepharmacystrength11 ="cautiontoentertypepharmacystrength";
    var cautiontoentertypepharmacyquantity11 ="cautiontoentertypepharmacyquantity";
    var cautiontoentertypepharmacymedname11 ="cautiontoentertypepharmacymedname";


    var MedName= MedName1 + count;
    var Strength= Strength1 + count;
    var MedType= MedType1 + count;
    var PQuantity= PQuantity1 + count;

    var cautiontoentertypepharmacytype1= cautiontoentertypepharmacytype11 + count;
    var cautiontoentertypepharmacystrength1= cautiontoentertypepharmacystrength11 + count;
    var cautiontoentertypepharmacyquantity1= cautiontoentertypepharmacyquantity11 + count;
    var cautiontoentertypepharmacymedname1= cautiontoentertypepharmacymedname11 + count;



//make type and other information mandatory for pharmacy

            $('#'+cautiontoentertypepharmacytype1).css("display","none");
            $('#'+cautiontoentertypepharmacystrength1).css("display","none");
            $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
            $('#'+cautiontoentertypepharmacymedname1).css("display","none");





        $("#"+MedName).blur(function(){
             var checkMedName=$("#"+MedName).val();
             var checkStrength=$("#"+Strength).val();
             var checkMedType=$("#"+MedType).val();
             var checkPQuantity=$("#"+PQuantity).val();


             if(checkMedName)
             {

                $('#'+cautiontoentertypepharmacymedname1).css("display","none");

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {

                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }

              if((checkMedType)&&(checkStrength)&&(checkPQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                  if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }
               if((!checkMedType)&&(!checkStrength)&&(!checkPQuantity)&&(!checkMedName))
              {
                  $('#submitt').show();
                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
            $('#'+cautiontoentertypepharmacystrength1).css("display","none");
            $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
            $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }


             }


        });

        $("#"+Strength).blur(function(){
             var checkMedName=$("#"+MedName).val();
             var checkStrength=$("#"+Strength).val();
             var checkMedType=$("#"+MedType).val();
             var checkPQuantity=$("#"+PQuantity).val();

             if(checkStrength)
             {
                   $('#'+cautiontoentertypepharmacystrength1).css("display","none");

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }

              if((checkMedType)&&(checkMedName)&&(checkPQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                 if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }

               if((!checkMedType)&&(!checkStrength)&&(!checkPQuantity)&&(!checkMedName))
              {
                  $('#submitt').show();
                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
            $('#'+cautiontoentertypepharmacystrength1).css("display","none");
            $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
            $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
             }


        });

        $("#"+MedType).blur(function(){
             var checkMedName=$("#"+MedName).val();
             var checkStrength=$("#"+Strength).val();
             var checkMedType=$("#"+MedType).val();
             var checkPQuantity=$("#"+PQuantity).val();

             if(checkMedType)
             {
                $('#'+cautiontoentertypepharmacytype1).css("display","none");

              if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }

              if((checkMedName)&&(checkStrength)&&(checkPQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
              else
             {
                 if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }
               if((!checkMedType)&&(!checkStrength)&&(!checkPQuantity)&&(!checkMedName))
              {
                  $('#submitt').show();
                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
            $('#'+cautiontoentertypepharmacystrength1).css("display","none");
            $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
            $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
             }

        });

        $("#"+PQuantity).blur(function(){
             var checkMedName=$("#"+MedName).val();
             var checkStrength=$("#"+Strength).val();
             var checkMedType=$("#"+MedType).val();
             var checkPQuantity=$("#"+PQuantity).val();

             if(checkPQuantity)
             {

                $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if((checkMedType)&&(checkStrength)&&(checkMedName))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                if(checkMedName)
              {

                  $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacymedname1).css("display","block");
                $('#'+cautiontoentertypepharmacymedname1).css("color","red");
              }

              if(checkStrength)
              {

                  $('#'+cautiontoentertypepharmacystrength1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacystrength1).css("display","block");
                $('#'+cautiontoentertypepharmacystrength1).css("color","red");
              }

              if(checkMedType)
              {

                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacytype1).css("display","block");
                $('#'+cautiontoentertypepharmacytype1).css("color","red");
              }

              if(checkPQuantity)
              {

                  $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#'+cautiontoentertypepharmacyquantity1).css("display","block");
                $('#'+cautiontoentertypepharmacyquantity1).css("color","red");
              }
               if((!checkMedType)&&(!checkStrength)&&(!checkPQuantity)&&(!checkMedName))
              {
                  $('#submitt').show();
                  $('#'+cautiontoentertypepharmacytype1).css("display","none");
            $('#'+cautiontoentertypepharmacystrength1).css("display","none");
            $('#'+cautiontoentertypepharmacyquantity1).css("display","none");
            $('#'+cautiontoentertypepharmacymedname1).css("display","none");
              }

             }


        });




});
</script>
<style type="text/css">
  #cautiontoentertypepharmacytype1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacystrength1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacyquantity1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacymedname1
{
  display: none;
  color: red;
}
</style>

<div class="col-sm-12">
<h3> Pharmacy  {{$countPharmacyDetails}} Details </h3>
</div>
    <div class="col-sm-3" id="firstrow" >

      <label>Type </label>
      <select name="MedType{{$countPharmacyDetails}}" id="MedType{{$countPharmacyDetails}}" >
        <option value="@yield('editMedType')">@yield('editMedType')</option>
        <option value="Tablet">Tablet</option>
        <option value="Capsule">Capsule</option>
        <option value="Injection">Injection</option>
        <option value="Ointment">Ointment</option>
        <option value="Scrub">Scrub</option>
         <option value="Cream">Cream</option>
         <option value="Powder">Powder</option>
         <option value="Lotion">Lotion</option>
         <option value="Syrum">Syrum</option>
         <option value="Drops">Drops</option>
         <option value="Gel">Gel</option>
         <option value="Expectorant">Expectorant</option>
         <option value="Respules">Respules</option>
         <option value="Handrub">Handrub</option>
         <option value="Syrup">Syrup</option>


      </select>
      <p id="cautiontoentertypepharmacytype{{$countPharmacyDetails}}" style="display: block;">Please select Type </p>

    </div>
<div class="col-sm-3" id="firstrow">

      <label>Medicine Name</label><input type="text"  rows="5" name="MedName{{$countPharmacyDetails}}" id="MedName{{$countPharmacyDetails}}" value="@yield('editMedName')">
      <p id="cautiontoentertypepharmacymedname{{$countPharmacyDetails}}" style="display: block;">Please Enter Medicine Name </p>
    </div>
     <div class="col-sm-3" id="firstrow">

      <label>Strength</label><input type="text"  rows="5" name="Strength{{$countPharmacyDetails}}" id="Strength{{$countPharmacyDetails}}" value="@yield('editStrength')">
      <p id="cautiontoentertypepharmacystrength{{$countPharmacyDetails}}" style="display: block;">Please Enter Strength </p>
    </div>


    <div class="col-sm-3" id="firstrow">
     <label>Quantity &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</label><input type="text"  rows="5" name="pQuantity{{$countPharmacyDetails}}" id="PQuantity{{$countPharmacyDetails}}" value="@yield('editPQuantity')" style="width:50%;">

     <input type="text" name="QuantityType{{$countPharmacyDetails}}" id="QuantityType{{$countPharmacyDetails}}" list="QuantityTypelist"  value=""   style="width:48%;">

         <datalist id="QuantityTypelist">
             <option value="Strip">Strip</option>
             <option value="Tablets">Tablets</option>
         </datalist>

      <p id="cautiontoentertypepharmacyquantity{{$countPharmacyDetails}}" style="display: block;">Please Enter Quantity </p>
    </div>



    <div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="PAvailabilityStatus{{$countPharmacyDetails}}" id="PAvailabilityStatus{{$countPharmacyDetails}}" value="@yield('editPAvailabilityStatus')">
    </div>




    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice{{$countPharmacyDetails}}" id="pSellingPrice{{$countPharmacyDetails}}" value="@yield('editpSellingPrice')">
    </div>

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Mode of payment</label>
      <select name="PModeofpayment{{$countPharmacyDetails}}" id="PModeofpayment{{$countPharmacyDetails}}">
        <option value="@yield('editPModeofpayment')">@yield('editPModeofpayment')</option>
        <option value="Cash">Cash</option>
        <option value="COD">COD</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div> -->

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="POrderStatus{{$countPharmacyDetails}}" id="POrderStatus{{$countPharmacyDetails}}">
        <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->
