<?php
$value= Session::all();

$value=session()->getId();
  //echo $value;


?>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Health Heal</title>
        
  

       

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style type="text/css">
        body
        {
            background:white;
        }
        @media only screen and (max-width: 1200px) {
    .imgg
    {
         padding-left: 2px;

    }
    .navbar-header
    {
      padding-left: 82px;
    }
    #loo
    {
       /* text-align: center;*/
    }
}


@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}

      .imgg
      {
                
            max-width: 167px;
   
      }
      .navbar {
    position: relative;
    min-height: 64px;
    margin-bottom: 20px;
    border: 1px solid transparent;
}

.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
li
{
   font-family: myFirstFont;
    font-size: 14px;
    color: #777;
        padding-top: 14px;
    padding-bottom: 14px;
    line-height: 22px;
}

    </style>

    <style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    right: 0;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
        margin-left: -40px;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
    

.navbar-right
{
   text-align: -webkit-right; 
}
</style>

</head>
<body >
  <div id="app">
                <div class="navbar navbar-default navbar-fixed-top" style="position: fixed;">
    <div class="container">

        <div class="navbar-header">
          <!--   <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->

               <div id="loo"> <a href=""><img class="imgg"
                     src="/img/healthheal_logo.png" id="logoimage"></a></div>
 

        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                       

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">
 <?php
                                             if (session()->has('token'))
                                                 {

                                                        $token1=session()->get('token');
                                                        $logged_in_user=session()->get('logged_in_user');
                                                    }
                                                 else{
                                                                     $token1=NULL;
                                                                     $logged_in_user=NULL;
                                                 }
                                            ?>
                                   <a href="\change-password">User Settings</a>
                                    <a href="\version">Version Notes</a>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout 
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                             <?php

                                             if (session()->has('token'))
                                                 {

                                                        $token1=session()->get('token');
                                                        $logged_in_user=session()->get('logged_in_user');
                                                        
                                                    }
                                                 else{
                                                                     $token1=NULL;
                                                                     $logged_in_user=NULL;
                                                 }
                                            ?>
                                          {{--   <input type="hidden" name="token1" value="<?php echo $token1; ?>">
                                            <input type="hidden" name="logged_in_user" value="<?php echo $logged_in_user; ?>"> --}}
                                        </form>
                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>


        @yield('content')
    </div>
@extends('layouts.footer');
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>




    
</body>
</html>
