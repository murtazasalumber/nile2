  <html>
  <head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript'>
    function LoadGoogle()
    {
        if(typeof google != 'undefined' && google && google.load)
        {
            // Now you can use google.load() here...

                google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
           ['{{$branch}}', '{{$vertical}}'],
          ['New Lead',  1000],
          ['In Progress',  1170],
          ['Converted',  660],
          ['Deferred',  1030],
      ['Dropped',  1030]
        ]);

        var options = {
          title: '',
          curveType: 'none',
          legend: { position: 'none' },
          width: 830,
        height: 400,
        pointSize: 20,
          pointShape: 'square'
        };


        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
        }
        else
        {
            // Retry later...
            setTimeout(LoadGoogle, 30);
        }
    }

    LoadGoogle();
</script>
<style type="text/css">
  .footer
  {
    display: none;
  }
  .navbar-default
  {
    background: white;
  }
  body
  {
     overflow-y: scroll;
  overflow-x: hidden;
  font-family: Raleway,sans-serif;
  }
</style>
  </head>
  <body>
  <!--   <div id="curve_chart" style="width: 900px; height: 500px"></div> -->
  <div class="container" style="width: 106%;background: #eee;margin-top: -30px;">
      <div class="row" style="padding-top: 4%;">



           

                <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
                        <div class="panel panel-default">

                    <a href="/adminassign?name=Murtaza&amp;status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  1811</h3>
                        </div>

                    </a>
                </div>

                </div>

                <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/adminassign?name=Murtaza&amp;status=In%20Progress">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> 98 </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/adminassign?name=Murtaza&amp;status=Converted">

                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> 1232 </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-2 " style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/adminassign?name=Murtaza&amp;status=Deferred">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> 251 </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        <a href="/adminassign?name=Murtaza&amp;status=Dropped">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> 4385 </h3>
                            </div>
                        </a>
                    </div>
                </div>
          


    <div class="col-sm-10 col-sm-offset-1" style="margin-top:3%;    ">
                    <div class="panel panel-default" style="margin-right: 6px;
                    margin-left: -4px;text-align:center;">

                    <div class="panel-body" style="height: auto;">
                     <h3> {{$vertical}}<!--  of {{$branch}} -->  Report from {{$Fromdate}}
 to {{$Todate}}</h3>
                       <div id="curve_chart" ></div>
                    </div>
                </div>
            </div>


        
</div>
    </div>
</div>
  </body>
</html>