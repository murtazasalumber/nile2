@extends('layouts.admin')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
     <!--      <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png"> -->
   <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Including the css file here -->
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />

  <!-- Including the css file here by another way -->
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />

  <!-- Including the javascript file here -->
<script src="js/footable.js" type="text/javascript"></script>
    <script>
         $.noConflict();
jQuery(document).ready(function($){
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({ animation:true,  html:true}); 
  $('.footable').footable();
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {
         
           $("#usersetting").popover('hide');
          
        }else
        {
           $("#usersetting").popover('show');
          
          //console.log(sidenvavv);
        }
    }); 
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
});
});
</script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
     <style type="text/css">
        body
        {
            background:white;
        }
        @media only screen and (max-width: 1200px) {
    .imgg
    {
         padding-left: 2px;

    }
    .navbar-header
    {
      padding-left: 17px;
    }
}
body::-webkit-scrollbar 
{
  display: none;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}

body
{
    overflow-y: scroll;
  overflow-x: hidden;
  font-family: Raleway,sans-serif;
}

@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}

  .footer
  {
    text-align: center;
    margin-top: 23%;
  }     
       .imgg
{
  max-width: 167px;
   margin-top: -13px;
    margin-left: -80px;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
      .navbar {
    position: relative;
    min-height: 64px;
    margin-bottom: 20px;
    border: 1px solid transparent;
}

.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
li
{
   font-family: myFirstFont;
    font-size: 14px;
    color: #777;
        padding-top: 14px;
    padding-bottom: 14px;
    line-height: 22px;
}
.footable.breakpoint > tbody > tr > td > span.footable-toggle {
    float: right;
    margin-right: -101px;
    display: inline-block;
    font-family: 'footable';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    -webkit-font-smoothing: antialiased;
    padding-right: 5px;
    font-size: 14px;
    color: #888888;
}
#diableemployee
{
      padding-top: 5px;

}
h3
{
	color:#636b6f;;
}
@media screen and (max-height: 1200px) {
.add
{
     float: left;
    margin-left: 39%;
}
#diableemp
{
    margin-top: 11px;

}
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
    font-family: myFirstFont;
}
.navbar-right
{
   text-align: -webkit-right; 
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;
   display: none;
    z-index: 10000;
    position: fixed;
}
.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
.al
{
  margin-top: 10%; 
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #loo
  {
        margin-left: 41%;
  }
  .al
  {
        margin-top: 18%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: 3px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }

}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) { 
  .submit
  {
        margin-top: 30px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  #loo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top:18%;
  }
 #userimage {
        margin-top: 3px;
        width: 50%;
        margin-left: 2%;
  }
 
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
    
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 109%;
    margin-left: -14px;
  }

  }
   /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  .submit
  {
        margin-top: 22px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
 #loo
  {
        margin-left: 27%;
  }
  
 #userimage {
        margin-top: 3px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 110%;
    margin-left: -14px;
  }

}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
     #loo
  {
        margin-left: 27%;
  }
 
 #userimage {
        margin-top: 3px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }
  }
    </style>
</head>
<body >

	<!-- <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
                <img class="imgg" 
                     src="/img/healthheal.png">
            </a>
        </div>
       
         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                 
                   
                     <li>

                          <a href="/admin/home" style="font-size: 15px;">Home </a>

                       </li>
                       
                
            </ul>
           
        </div>

    </div>
</div> -->
<!-- Update tabel view ends -->
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\employees' title='' style='color:black'>Back</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">

	<div class="container">
			<div class="row al">
					<div class="col-sm-12" >
						<center><h3> Employee Table Details </h3></center>
						
					</div>
				
			</div>
	</div>

  <div class="container">
      <div class="row">
          <div class="col-sm-12 add" >
          
            <a href="/employees/create"  style="font-size: 15px;float: right;" ><img src="/img/add.png" class="img-responsive" alt="add"> </a>
          </div>
        
      </div>
  </div>
@include('partial.message')
<div class="container">


		<div class="container" style="    margin-top: 37px;">
 				<div class="row">
 							<div class="col-sm-12">

 										<!-- <table class="table"> -->
                        <table class="table footable" style="font-family: Raleway,sans-serif;">
    											<thead>
      													<tr>
        													<th>Id</th>
                                  <th data-hide="phone,tablet">REG ID</th>
        													<th data-hide="phone,tablet">First Name</th>
                                  <th data-hide="phone,tablet">Gender</th>
                                  <th data-hide="phone,tablet">DOJ</th>
                                  <th data-hide="phone,tablet">EmploymentType</th>
                                  <th data-hide="phone,tablet">Designation</th>
                                  <th data-hide="phone,tablet">Mobile Number</th>
                                  <th data-hide="phone,tablet">Department</th>
                                  <th data-hide="phone,tablet">Assigned Under</th>
                                  <th data-hide="phone,tablet"> </th>
      													</tr>
    											</thead>
    											<tbody>
      												 @foreach ($employees as $employee)
      													<tr>
        													<td>{{$employee->id}}</td>
                                  <td>{{$employee->Regid}}</td>
       														 <td>{{$employee->FirstName}}</td>
                                   
                                   
                                   <td>{{$employee-> Gender}}</td>
                                   
                                  
                                   <td>{{$employee-> DOJ}}</td>
                                   <td>{{$employee-> EmployementType}}</td>
                                   <td>{{$employee-> Designation}}</td>
                                   <td>{{$employee-> MobileNumber}}</td>
                                  
                                   <td>{{$employee-> Department}}</td>
                                   <td>{{$employee-> underwhom}}</td>
                                  

                                  <td id="diableemployee"><a href="enable?id={{$employee->id}}"><input type="submit" class="btn btn-default" value="Enable" id="diableemp" ></a> </td>
        													
        													<td>
        													<form action="{{'/employees/'.$employee->id}}" method="post">
{{csrf_field()}}
{{ method_field('DELETE') }}
<!-- <input type="submit" class="btn btn-default" value="Delete" > -->
</form>
        													</td>
     													 </tr>
      												@endforeach
    											</tbody>
  										</table>
 								<div style="text-align: -webkit-center;"> {{$employees->links()}} </div>

     
 							</div>
 					
 				</div>
           
  
		</div>



    </div>


</div>    
</body>

@endsection