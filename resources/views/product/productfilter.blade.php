
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
         <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td style="color:red;">{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "AssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
       <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    
    <td style="color:red;">{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "orderid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
        <td style="color:red;"><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "SKUid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
       <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td style="color:red;">{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>

    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "MobileNumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
<th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
    <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>
    </tr>
    @endforeach
  </tbody>
</table>



      @elseif ($filter1 === "OrderStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td >{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

             
@elseif ($filter1 === "OrderStatusrent")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


@elseif ($filter1 === "ProductName")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


@elseif ($filter1 === "city")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td style="color:red;">{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
@endif
