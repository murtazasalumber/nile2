@extends(($Designation=="Customer Care") ? 'layouts.customercare' : ($Designation2=="Pharmacy Manager") ? 'layouts.pharmacymanager' : 'layouts.productmanager')

@section('editclientfname',$leaddetails->fName)
@section('productid',$productid)
@section('editclientmname',$leaddetails->mName)
@section('editclientlname',$leaddetails->lName)
@section('editclientemail',$leaddetails->EmailId)
@section('editsource',$leaddetails->Source)
@section('editclientmob',$leaddetails->MobileNumber)
@section('editclientalternateno',$leaddetails->Alternatenumber)
@section('editEmergencyContact',$leaddetails->EmergencyContact)
@section('editAddress1',$addressdetails->Address1)
@section('editAddress2',$addressdetails->Address2)
@section('editCity',$addressdetails->City)
@section('editDistrict',$addressdetails->District)
@section('editState',$addressdetails->State)
@section('editPinCode',$addressdetails->PinCode)
@section('editPAddress1',$addressdetails->PAddress1)
@section('editPAddress2',$addressdetails->PAddress2)
@section('editPCity',$addressdetails->PCity)
@section('editPDistrict',$addressdetails->PDistrict)
@section('editPtate',$addressdetails->PState)
@section('editPPinCode',$addressdetails->PPinCode)
@section('editSAddress1',$addressdetails->SAddress1)
@section('editSAddress2',$addressdetails->SAddress2)
@section('editSCity',$addressdetails->SCity)
@section('editSDistrict',$addressdetails->SDistrict)
@section('editSState',$addressdetails->SState)
@section('editSPinCode',$addressdetails->SPinCode)
@section('editreference',$referencedetails)
@section('content')

<html lang="en">
<head>

<title>Health Heal</title>
         <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
       <!--   <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png"> -->
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
 <style type="text/css">
 button[disabled], html input[disabled] {
   background: white;
   cursor: default;
 }
 </style>


 <script>
 $(document).ready(function(){
$(function() {
   $('#country').on('change', function() {
     $('#phone1').val($(this).val());
   });



 $('[data-toggle="popover"]').popover({ animation:true,  html:true});
   var country = $('#country').val();
   $('#phone1').val(country);

   //  $('#sameaspermanentadress').change(function () {

   //         console.log("checked");
   // });
   $('#sameaspermanentadress').change(function () {

     if (this.checked) {
       console.log("checked");
       // $('#Address1').val("fgg");
       var Addresd1 =$('#Address1').val();
       // Adrees1
       $('#PAddress1').val(Addresd1);
       $("#PAddress1").prop('disabled', true);


       //Adress2
       var Addresd2 =$('#Address2').val();
       $('#PAddress2').val(Addresd2);
       $("#PAddress2").prop('disabled', true);

       //City

       var City =$('#City').val();
       $('#PCity').val(City);
       $("#PCity").prop('disabled', true);

       //district

       var District =$('#District').val();
       $('#PDistrict').val(District);
       $("#PDistrict").prop('disabled', true);

       //State

       var State =$('#State').val();
       $('#PState').val(State);
       $("#PState").prop('disabled', true);


       //Pincode

       var Pincode =$('#PinCode').val();
       $('#PPinCode').val(Pincode);
       $("#PPinCode").prop('disabled', true);
     }

     if(!$(this).is(":checked"))
     {
       console.log("unchecked");

       // Adrees1
       $('#PAddress1').val("");
       $("#PAddress1").prop('disabled', false);

       //Adress 2
       $('#PAddress2').val("");
       $("#PAddress2").prop('disabled', false);

       //City
       $('#PCity').val("");
       $("#PCity").prop('disabled', false);

       //District
       $('#PDistrict').val("");
       $("#PDistrict").prop('disabled', false);

       //State
       $('#PState').val("");
       $("#PState").prop('disabled', false);


       //Pincode
       $('#PPinCode').val("");
       $("#PPinCode").prop('disabled', false);

     }
   });





   $('#Esameaspermananentaddress').change(function () {

     if (this.checked) {
       // console.log("checked");
       // $('#Address1').val("fgg");
       var Addresd1 =$('#Address1').val();
       // Adrees1
       $('#EAddress1').val(Addresd1);
       $("#EAddress1").prop('disabled', true);


       //Adress2
       var Addresd2 =$('#Address2').val();
       $('#EAddress2').val(Addresd2);
       $("#EAddress2").prop('disabled', true);

       //City

       var City =$('#City').val();
       $('#ECity').val(City);
       $("#ECity").prop('disabled', true);

       //district

       var District =$('#District').val();
       $('#EDistrict').val(District);
       $("#EDistrict").prop('disabled', true);

       //State

       var State =$('#State').val();
       $('#EState').val(State);
       $("#EState").prop('disabled', true);


       //Pincode

       var Pincode =$('#PinCode').val();
       $('#EPinCode').val(Pincode);
       $("#EPinCode").prop('disabled', true);
     }

     if(!$(this).is(":checked"))
     {
       // console.log("unchecked");

       // Adrees1
       $('#EAddress1').val("");
       $("#EAddress1").prop('disabled', false);

       //Adress 2
       $('#EAddress2').val("");
       $("#EAddress2").prop('disabled', false);

       //City
       $('#ECity').val("");
       $("#ECity").prop('disabled', false);

       //District
       $('#EDistrict').val("");
       $("#EDistrict").prop('disabled', false);

       //State
       $('#EState').val("");
       $("#EState").prop('disabled', false);


       //Pincode
       $('#EPinCode').val("");
       $("#EPinCode").prop('disabled', false);

     }
   });



$('#shippingsameaspermananentaddress').change(function () {

     if (this.checked) {
       // console.log("checked");
       // $('#Address1').val("fgg");
       var Addresd1 =$('#Address1').val();
       // Adrees1
       $('#SAddress1').val(Addresd1);
       $("#SAddress1").prop('disabled', true);


       //Adress2
       var Addresd2 =$('#Address2').val();
       $('#SAddress2').val(Addresd2);
       $("#SAddress2").prop('disabled', true);

       //CitS

       var City =$('#City').val();
       $('#SCity').val(City);
       $("#SCity").prop('disabled', true);

       //district

       var District =$('#District').val();
       $('#SDistrict').val(District);
       $("#SDistrict").prop('disabled', true);

       //State

       var State =$('#State').val();
       $('#SState').val(State);
       $("#SState").prop('disabled', true);


       //Pincode

       var Pincode =$('#PinCode').val();
       $('#SPinCode').val(Pincode);
       $("#SPinCode").prop('disabled', true);
     }

     if(!$(this).is(":checked"))
     {
       // console.log("unchecked");

       // Adrees1
       $('#SAddress1').val("");
       $("#SAddress1").prop('disabled', false);

       //Adress 2
       $('#SAddress2').val("");
       $("#SAddress2").prop('disabled', false);

       //City
       $('#SCity').val("");
       $("#SCity").prop('disabled', false);

       //District
       $('#SDistrict').val("");
       $("#SDistrict").prop('disabled', false);

       //State
       $('#SState').val("");
       $("#SState").prop('disabled', false);


       //Pincode
       $('#SPinCode').val("");
       $("#SPinCode").prop('disabled', false);

     }
   });







//Alternate Phone Number
   $("#clientalternateno").keyup(function(){

     // console.log("clciked");

     var mobileno = $('#clientalternateno').val();
     var reg = /^[\+?\d[\d -]{8,12}\d$/;

     if (reg.test(mobileno) == false)
     {

           if (reg.test(mobileno) == false)
       {

             // console.log("Wrong Email");
             $("#clientalternateno").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#clientalternateno").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

}
});


$("#clientalternateno").blur(function(){
     var value = $('#clientalternateno').val();

     var reg = /^[\+?\d[\d -]{8,12}\d$/;
     var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
     if (numwithcountrycode.test(value) == true)
        {

            $("#clientalternateno").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }else
        {

                 if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#clientalternateno").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#clientalternateno").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#clientalternateno").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }


        }

   });





//EmergencyContact Phone Number
   $("#EmergencyContact").keyup(function(){

     // console.log("clciked");

     var mobileno = $('#EmergencyContact').val();
     var reg = /^[\+?\d[\d -]{8,12}\d$/;

     if (reg.test(mobileno) == false)
     {

           if (reg.test(mobileno) == false)
       {

             // console.log("Wrong Email");
             $("#EmergencyContact").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#clientalternateno").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

}
});


$("#EmergencyContact").blur(function(){
     var value = $('#clientalternateno').val();

     var reg = /^[\+?\d[\d -]{8,12}\d$/;
     var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
     if (numwithcountrycode.test(value) == true)
        {

            $("#EmergencyContact").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }else
        {

                 if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#EmergencyContact").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }


        }

   });


   //Phone Number
   $("#phone").keyup(function(){

     // console.log("clciked");

     var mobileno = $('#phone').val();
     var reg = /^[\+?\d[\d -]{8,12}\d$/;

     if (reg.test(mobileno) == false)
     {

           if (reg.test(mobileno) == false)
       {

             // console.log("Wrong Email");
             $("#phone").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#phone").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

}
});
   // $("#phone").blur(function(){
   //   var value = $('#phone').val();

   //   var reg = /^[\+?\d[\d -]{8,12}\d$/;

   //       if (reg.test(value) == false)
   //     {

   //           // console.log("Wrong Email");
   //           $("#phone").css("border-bottom", "1px solid red");
   //            $("#submitt").prop('disabled', true);
   //     }
   //      else
   //     {
   //       // console.log("Correct Email");
   //        $("#phone").css("border-bottom", "1px solid #484e51");
   //         $("#submitt").prop('disabled', false);
   //     }

   //     if(value == "")
   //     {
   //         $("#phone").css("border-bottom", "1px solid #484e51");
   //          $("#submitt").prop('disabled', false);
   //     }
   // });

$("#phone").blur(function(){
     var value = $('#phone').val();

     var reg = /^[\+?\d[\d -]{8,12}\d$/;
     var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
     if (numwithcountrycode.test(value) == true)
        {

            $("#phone").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }else
        {

                 if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#phone").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#phone").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#phone").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }


        }
       //   if (reg.test(value) == false)
       // {

       //       // console.log("Wrong Email");
       //       $("#phone").css("border-bottom", "1px solid red");
       //        $("#submitt").prop('disabled', true);
       // }
       //  else
       // {
       //   // console.log("Correct Email");
       //    $("#phone").css("border-bottom", "1px solid #484e51");
       //     $("#submitt").prop('disabled', false);
       // }

       // if(value == "")
       // {
       //     $("#phone").css("border-bottom", "1px solid #484e51");
       //      $("#submitt").prop('disabled', false);
       // }
   });

   // Pincode Validaion in Addres
 $("#PinCode").keyup(function(){

         // console.log("Pincode ");
         var reg = /^[1-9][0-9]{5}$/;
         var pincode = $('#PinCode').val();

         if (reg.test(pincode) == false)
       {

             // console.log("Wrong Email");
             $("#PinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#PinCode").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

   });

 $("#PinCode").blur(function(){
           var value = $('#PinCode').val();

        var reg = /^[1-9][0-9]{5}$/;

         if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#PinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#PinCode").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#PinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }
   });

   // Pincode Validaion in Present Addres
 $("#PPinCode").keyup(function(){

         // console.log("Pincode ");
         var reg = /^[1-9][0-9]{5}$/;
         var pincode = $('#PPinCode').val();

         if (reg.test(pincode) == false)
       {

             // console.log("Wrong Email");
             $("#PPinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#PPinCode").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

   });

 $("#PPinCode").blur(function(){
           var value = $('#PPinCode').val();

        var reg = /^[1-9][0-9]{5}$/;

         if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#PPinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#PPinCode").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#PPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }
   });

   // Pincode Validaion in Emergency Addres
 $("#EPinCode").keyup(function(){

         // console.log("Pincode ");
         var reg = /^[1-9][0-9]{5}$/;
         var pincode = $('#EEPinCode').val();

         if (reg.test(pincode) == false)
       {

             // console.log("Wrong Email");
             $("#EPinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#EPinCode").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
       }

   });

 $("#EPinCode").blur(function(){
           var value = $('#EPinCode').val();

        var reg = /^[1-9][0-9]{5}$/;

         if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#EPinCode").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
       }
        else
       {
         // console.log("Correct Email");
          $("#EPinCode").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }

       if(value == "")
       {
           $("#EPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
       }
   });


   // Client Email Id Validation Start
   $("#clientemail").keyup(function(){
     var value = $('#clientemail').val();

     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
         if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#clientemail").css("border-bottom", "1px solid red");
             $("#submitt").prop('disabled', true);

       }
       else
       {
         // console.log("Correct Email");
          $("#clientemail").css("border-bottom", "1px solid green");
          $("#submitt").prop('disabled', false);
       }

     if (reg.test(value) == false)
     {

       // console.log("Wrong Email");
       $("#clientemail").css("border-bottom", "1px solid red");
     }
     else
     {
       // console.log("Correct Email");
       $("#clientemail").css("border-bottom", "1px solid green");
     }


   });

   $("#clientemail").blur(function(){
           var value = $('#clientemail').val();

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

         if (reg.test(value) == false)
       {

             // console.log("Wrong Email");
             $("#clientemail").css("border-bottom", "1px solid red");
             $("#submitt").prop('disabled', true);
       }
       else
       {
         // console.log("Correct Email");
          $("#clientemail").css("border-bottom", "1px solid #484e51");
          $("#submitt").prop('disabled', false);
       }

         if(value == "")
       {
           $("#clientemail").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
       }
   });

   // Cleint ValidationEmail Ends




 });
$("#ProductName").blur(function(){
    var ProductName =$('#ProductName').val();
    $.get("{{ URL::to('productname') }}", { productname :ProductName},function(data){
             $('#SKUid').val(data);
         });
});

   var value=$('#reference').val();
   if(value == "Other")
   {
     $("#source").show();
   }else
   {
     $("#source").hide();
   }



   $("#reference").change(function(){
     var value= $("#reference").val();
     if(value == "Other")
     {
       $("#source").show();
     }else
     {
       $("#source").hide();
     }
   });


// Drop down
var servicetype = $('#servicetype').val();

if(servicetype == "")
{
     $("#branch").hide();
} else
{
 $("#branchalert").hide();
}


var branch = $('#branch').val();

if((servicetype == "") && (branch == ""))
{


 $("#assignedto").hide();

} else
{
 $("#assignedtoalert").hide();
}




$("#servicetype").change(function(){

   var servicetype = $('#servicetype').val();
   var branch = $('#branch').val();
   if(servicetype == "")
   {
       $("#branchalert").show();
       $("#branch").hide();
   }else if(branch == "")
   {
     $("#branchalert").hide();
     $("#branch").show();

   } else
   {
     $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
               $('#resrult').html(data);
         });
   }
});



$("#branch").change(function(){
   var servicetype = $('#servicetype').val();
   var branch = $('#branch').val();
 if(branch == "")
 {
     $("#assignedto").hide();
     $("#assignedtoalert").show();
     $("#resrult").hide();
 }else
 {


      // $.get("test.php", { name:"Donald", town:"Ducktown" });
      $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
               $('#resrult').html(data);
         });


     $("#assignedto").hide();
     $("#assignedtoalert").hide();
     $("#resrult").show();
 }




});


$("#clinetidsubmit").click(function(){
    var clientid =$('#clientid').val();
     $("#clientid").val("");
    // alert(clientid);
    $.get("{{ URL::to('clientdetails') }}", { clientid1 :clientid},function(data){
               $('#result').html(data);
         });
   });
var noofproduts="<?php echo $product_count; ?>";

if(noofproduts > 0)
{
  $("#value").val(noofproduts);
  $("#nooffiledsPharmacyDetails").val(noofproduts);
}

//console.log(noofproduts);
var count =1;
var numberr=1;
$("#addmoreinputfiled").click(function(){

    var type = "<?php echo $Type; ?>";
    // alert(type);
    noofproduts=parseInt(noofproduts, 10) + parseInt(numberr, 10);
    //console.log(noofproduts);
    $("#value").val(noofproduts);
        $.get("{{ URL::to('inputfileds') }}", { noofinputfiledsadded :noofproduts, type1:type},function(data){
               $('#moreinput').append(data+"<br/>");
         });
  });
//console.log(noofproduts);
var countPharmacyDetails=1;
if(noofproduts > 0)
{
  countPharmacyDetails=noofproduts;
}
$("#addmoreinputfiledPharmacyDetails").click(function(){

    //countPharmacyDetails = countPharmacyDetails + 1;
        countPharmacyDetails=parseInt(countPharmacyDetails, 10) + parseInt(numberr, 10);
    $("#nooffiledsPharmacyDetails").val(countPharmacyDetails);
        $.get("{{ URL::to('inputfiledsPharmacyDetails') }}", { countPharmacyDetails :countPharmacyDetails},function(data){
               $('#moreinputPharmacyDetails').append(data+"<br/>");
         });
   });
 });


 $(document).ready(function(){

 var value=$('#Type').val();
    if(value == "Sell")
   {
     $("#sell").show();
     $("#rent").hide();
   }else
   {
   if(value == "")
   {
     $("#sell").hide();
     $("#rent").hide();

   }else
   {
     $("#sell").hide();
     $("#rent").show();
   }
 }



   $("#Type").change(function(){
   var value= $("#Type").val();
   if(value == "Sell")
   {
    $("#sell").show();
     $("#rent").hide();
   }else
   {
   if(value == "")
   {
     $("#sell").hide();
     $("#rent").hide();

   }else
   {
     $("#sell").hide();
     $("#rent").show();
   }
 }
});



   {{-- product type mandatory  --}}
 $('#cautiontoentertype').css("display","none");


             var typecheck=$("#Type").val();
             var skuidcheck=$("#SKUid").val();
             var productcheck=$("#ProductName").val();

             if((skuidcheck) || (productcheck))
             {

              if(typecheck)
              {

                  $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");
              }
              else
              {
                  $('#submitt').hide();
                  $('#cautiontoentertype').css("display","block");
              }
             }
             else
             {
                $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");

             }



            $("#SKUid").blur(function(){
             var typecheck=$("#Type").val();
             var skuidcheck=$("#SKUid").val();
             var productcheck=$("#ProductName").val();

             if((skuidcheck) || (productcheck))
             {

              if(typecheck)
              {

                  $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");
              }
              else
              {
                  $('#submitt').hide();
                  $('#cautiontoentertype').css("display","block");
              }
             }
             else
             {
                $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");

             }

        });

            $("#ProductName").blur(function(){
             var typecheck=$("#Type").val();
             var skuidcheck=$("#SKUid").val();
             var productcheck=$("#ProductName").val();

             if((skuidcheck) || (productcheck))
             {

              if(typecheck)
              {

                  $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");
              }
              else
              {
                  $('#submitt').hide();
                  $('#cautiontoentertype').css("display","block");
              }
             }
             else
             {
                $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");

             }

        });

            $("#Type").blur(function(){
             var typecheck=$("#Type").val();
             var skuidcheck=$("#SKUid").val();
             var productcheck=$("#ProductName").val();


              if(typecheck)
              {

                  $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");
              }
              else
              {
                if((!skuidcheck)&&(!productcheck))
                {
                    $('#submitt').show();
                  $('#cautiontoentertype').css("display","none");
                }
                else
                {
                    $('#submitt').hide();
                  $('#cautiontoentertype').css("display","block");
                }

              }



        });



var productcount1="<?php echo $product_count; ?>";

for(i=2; i<=productcount1;i++)
{
//make type and other information mandatory for pharmacy

            $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");



//onload

    var MedName=$("#MedName"+i).val();
             var Strength=$("#Strength"+i).val();
             var MedType=$("#MedType"+i).val();
             var pQuantity=$("#pQuantity"+i).val();




     {{-- check medname     --}}
         if(MedName)
             {
                $('#cautiontoentertypepharmacymedname'+i).css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedType)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                  if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }


             }

{{-- check strength --}}
             if(Strength)
             {
                   $('#cautiontoentertypepharmacystrength'+i).css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedType)&&(MedName)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
             }

//check medtype

        if(MedType)
             {
                $('#cautiontoentertypepharmacytype'+i).css("display","none");

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedName)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
              else
             {
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
             }

// check quantity

          if(pQuantity)
             {

                $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if((MedType)&&(Strength)&&(MedName))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }

             }


{{-- alert(i); --}}

   //  onchange
        $("#MedName"+i).blur(function(){
              var MedName=$("#MedName"+i).val();
             var Strength=$("#Strength"+i).val();
             var MedType=$("#MedType"+i).val();
             var pQuantity=$("#pQuantity"+i).val();

      {{-- alert("#MedName"+i); --}}
             if(MedName)
             {
                $('#cautiontoentertypepharmacymedname'+i).css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedType)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                  if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }


             }


        });

        $("#Strength"+i).blur(function(){
              var MedName=$("#MedName"+i).val();
             var Strength=$("#Strength"+i).val();
             var MedType=$("#MedType"+i).val();
             var pQuantity=$("#pQuantity"+i).val();

             if(Strength)
             {
                   $('#cautiontoentertypepharmacystrength'+i).css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedType)&&(MedName)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
             }


        });

        $("#MedType"+i).blur(function(){
             var MedName=$("#MedName"+i).val();
             var Strength=$("#Strength"+i).val();
             var MedType=$("#MedType"+i).val();
             var pQuantity=$("#pQuantity"+i).val();

             if(MedType)
             {
                $('#cautiontoentertypepharmacytype'+i).css("display","none");

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }

              if((MedName)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
              else
             {
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
             }

        });

        $("#pQuantity"+i).blur(function(){
             var MedName=$("#MedName"+i).val();
             var Strength=$("#Strength"+i).val();
             var MedType=$("#MedType"+i).val();
             var pQuantity=$("#pQuantity"+i).val();

             if(pQuantity)
             {

                $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if((MedType)&&(Strength)&&(MedName))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname'+i).css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength'+i).css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype'+i).css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity'+i).css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype'+i).css("display","none");
            $('#cautiontoentertypepharmacystrength'+i).css("display","none");
            $('#cautiontoentertypepharmacyquantity'+i).css("display","none");
            $('#cautiontoentertypepharmacymedname'+i).css("display","none");
              }

             }


        });


}


{{-- without for loop it will checkfor 1 product --}}
//make type and other information mandatory for pharmacy

            $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");



//onload

    var MedName=$("#MedName").val();
             var Strength=$("#Strength").val();
             var MedType=$("#MedType").val();
             var pQuantity=$("#pQuantity").val();

{{--      alert(Strength); --}}


     {{-- check medname     --}}
         if(MedName)
             {
                $('#cautiontoentertypepharmacymedname').css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedType)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                  if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }


             }

{{-- check strength --}}
             if(Strength)
             {
                   $('#cautiontoentertypepharmacystrength').css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedType)&&(MedName)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }
             }

//check medtype

        if(MedType)
             {
                $('#cautiontoentertypepharmacytype').css("display","none");

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedName)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
              else
             {
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }
             }

// check quantity

          if(pQuantity)
             {

                $('#cautiontoentertypepharmacyquantity').css("display","none");
              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if((MedType)&&(Strength)&&(MedName))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }

             }




   //  onchange
        $("#MedName").blur(function(){
             var MedName=$("#MedName").val();
             var Strength=$("#Strength").val();
             var MedType=$("#MedType").val();
             var pQuantity=$("#pQuantity").val();

             if(MedName)
             {
                $('#cautiontoentertypepharmacymedname').css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedType)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                  if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }


             }


        });

        $("#Strength").blur(function(){
             var MedName=$("#MedName").val();
             var Strength=$("#Strength").val();
             var MedType=$("#MedType").val();
             var pQuantity=$("#pQuantity").val();

             if(Strength)
             {
                   $('#cautiontoentertypepharmacystrength').css("display","none");

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedType)&&(MedName)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                $('#submitt').show();
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }
             }


        });

        $("#MedType").blur(function(){
             var MedName=$("#MedName").val();
             var Strength=$("#Strength").val();
             var MedType=$("#MedType").val();
             var pQuantity=$("#pQuantity").val();

             if(MedType)
             {
                $('#cautiontoentertypepharmacytype').css("display","none");

              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }

              if((MedName)&&(Strength)&&(pQuantity))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
              else
             {
                 if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }
             }

        });

        $("#pQuantity").blur(function(){
             var MedName=$("#MedName").val();
             var Strength=$("#Strength").val();
             var MedType=$("#MedType").val();
             var pQuantity=$("#pQuantity").val();

             if(pQuantity)
             {

                $('#cautiontoentertypepharmacyquantity').css("display","none");
              if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if((MedType)&&(Strength)&&(MedName))
              {
                  $('#submitt').show();

              }
              else
              {
                  $('#submitt').hide();

              }


             }
             else
             {
                if(MedName)
              {

                  $('#cautiontoentertypepharmacymedname').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacymedname').css("display","block");
              }

              if(Strength)
              {

                  $('#cautiontoentertypepharmacystrength').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacystrength').css("display","block");
              }

              if(MedType)
              {

                  $('#cautiontoentertypepharmacytype').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacytype').css("display","block");
              }

              if(pQuantity)
              {

                  $('#cautiontoentertypepharmacyquantity').css("display","none");
              }
              else
              {
                $('#submitt').hide();
                $('#cautiontoentertypepharmacyquantity').css("display","block");
              }
               if((!MedType)&&(!Strength)&&(!pQuantity)&&(!MedName))
              {
                  $('#submitt').show();
                  $('#cautiontoentertypepharmacytype').css("display","none");
            $('#cautiontoentertypepharmacystrength').css("display","none");
            $('#cautiontoentertypepharmacyquantity').css("display","none");
            $('#cautiontoentertypepharmacymedname').css("display","none");
              }

             }


        });


   userr=0;
$("#usersetting").click(function(){
     userr=userr+1;
       if(userr % 2 == 0)
       {

          $("#usersetting").popover('hide');

       }else
       {
          $("#usersetting").popover('show');

         //console.log(sidenvavv);
       }
   });
$("#bodyy").click(function(){
     userr=userr+1;
      if(userr % 2 != 0)
      {
       userr=userr+1;
      }
   });
});
 </script>


 </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>


#cautiontoentertype
{
  display: none;
  color: red;
}
#cautiontoentertype1
{
  display: none;
  color: red;
}




#cautiontoentertypepharmacymedname
{
  display: none;
  color: red;
}

#cautiontoentertypepharmacytype
{
  display: none;
  color: red;
}
#cautiontoentertypepharmacystrength
{
  display: none;
  color: red;
}#cautiontoentertypepharmacyquantity
{
  display: none;
  color: red;
}

 #cautiontoentertypepharmacytype1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacystrength1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacyquantity1
{
  display: none;
  color: red;
}
 #cautiontoentertypepharmacymedname1
{
  display: none;
  color: red;
}



.navbar-right
{
       text-align: -webkit-right;
}
.popover.left>.arrow
{
 display: none !important;
}
.popover.left
{
 width: 150%!important;
 margin-top: 192% !important;
   margin-left: -10% !important;
}
.all
{
   margin-top: 7%;
}
.btn
{
       color: #FFF!important;
   background-color: #00C851;
   display: inline-block;
   font-weight: 400;
   text-align: center;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   position: relative;
   cursor: pointer;
   user-select: none;
   z-index: 1;
   font-size: .8rem;
   font-size: 15px;
   border-radius: 2px;
   border: 0;
   transition: .2s ease-out;
   white-space: normal!important;
}
.btn:hover
{
 outline: 0;
   background-color: #00C851;
     opacity: 0.5;
}
.navbar2
{
        margin-left: 82%;
   margin-top: -68px;
   width: 15%;
   height: 40px;

   z-index: 10000;
   position: fixed;
}

.imgg {
      margin-top: -13px;
   margin-left: -62px;
}
label {
   display: inline-block;
   max-width: 100%;
   margin-bottom: -3px;
   font-weight: 700;
}
.switch {
 position: relative;
 display: inline-block;
 width: 30px;
 height: 17px;
}
.footer
{
 margin-top: 155px;
}

#submitt
{
      margin-top: 0%;
}
.switch input {display:none;}

.slider {
 position: absolute;
 cursor: pointer;
 top: 0;
 left: 0;
 right: 0;
 bottom: 0;
 background-color: #ccc;
 -webkit-transition: .4s;
 transition: .4s;
}
.navbar2
{
 display: none;
}
.slider:before {
 position: absolute;
 content: "";
 height: 13px;
 width: 13px;
 left: 2px;
 bottom: 2px;
 background-color: white;
 -webkit-transition: .2s;
 transition: .2s;
}

input:checked + .slider {
 background-color: #009056;
}

input:focus + .slider {
 box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
 -webkit-transform: translateX(13px);
 -ms-transform: translateX(13px);
 transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
 border-radius: 34px;
}
#maintitle
{
  margin-left: -4px;
   width: 102%;
}
#firstform,#ProductDetails,#PharmacyDetails
{
 border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;
}
.slider.round:before {
 border-radius: 50%;
}

/*Galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
#logoimage
{
     margin-left: -10%;
   height: 74px;
   margin-top: -21px;
}
#loo
 {
          margin-left: 5px;
   height: 61px;
   width: 100%;
 }
 .al
 {
   margin-top: 25%;
 }
 .navbar2 {
   margin-top: 13px;
               position: fixed!important;
   display: block!important;
     margin-left: 82%;
   width: 15%;
   height: 40px;
   z-index: 10000;
   position: fixed;
     }

     #userimage {
               margin-top: -14px;
   width: 64%;
   margin-left: 2%;
 }
 .logoo
 {
       margin-left: 25%;
 }

   #maintitle
{
     margin-left: -4px;
   width: 106%;
}
#firstform,#ProductDetails,#PharmacyDetails
{
     border-radius: 11px;
   border: 1px solid #808080;
   width: 97%;
   font-family: myFirstFont;
   margin-left: -14px;
}

}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) {
 #logoimage
{
        margin-left: -8%;
   height: 77px;
   margin-top: -21px;
}
#loo
 {
          margin-left: 5px;
   height: 61px;
   width: 100%;
 }
 .al
 {
   margin-top: 25%;
 }
 .navbar2 {
   margin-top: 13px;
               position: fixed!important;
   display: block!important;
     margin-left: 82%;
   width: 15%;
   height: 40px;
   z-index: 10000;
   position: fixed;
     }

     #userimage {
                margin-top: -24px!important;
   width: 64%!important;
   margin-left: 2%!important;
 }
.navbar2 {
           display: block!important;
     }
     #userimage {
       margin-top: 6px;
       width: 50%;
       margin-left: 2%;
 }
 .logoo
 {
       margin-left: 25%;
 }
#maintitle
{
         margin-left: -4px;
   width: 105%;
}
#firstform,#ProductDetails,#PharmacyDetails
{
      border-radius: 11px;
   border: 1px solid #808080;
   width: 99%;
   font-family: myFirstFont;
   margin-left: -20px;
}
 }


  /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
 #logoimage
{
     margin-left: -16%;
   height: 74px;
   margin-top: -21px;
}
#loo
 {
          margin-left: 5px;
   height: 61px;
   width: 100%;
 }
 .al
 {
   margin-top: 29%;
 }
 .navbar2 {
   margin-top: 13px;
               position: fixed!important;
   display: block!important;
     margin-left: 82%;
   width: 15%;
   height: 40px;
   z-index: 10000;
   position: fixed;
     }
 .navbar2 {
           display: block!important;
     }
     #userimage {
          margin-top: -14px;
   width: 64%;
   margin-left: 2%;
 }
 .logoo
 {
       margin-left: 25%;
 }
#maintitle
{
        margin-left: -4px;
   width: 106%;
}
#firstform,#ProductDetails,#PharmacyDetails
{
         border-radius: 11px;
   border: 1px solid #808080;
   width: 93%;
   font-family: myFirstFont;
   margin-left: -8px;
}

}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
 #logoimage
{
     margin-left: -10%;
   height: 74px;
   margin-top: -21px;
}
#loo
 {
          margin-left: 5px;
   height: 61px;
   width: 100%;
 }
 .al
 {
   margin-top: 20%;
 }
 .navbar2 {
   margin-top: 13px;
               position: fixed!important;
   display: block!important;
     margin-left: 82%;
   width: 15%;
   height: 40px;
   z-index: 10000;
   position: fixed;
     }

     #userimage {
              margin-top: -20px;
   width: 64%;
   margin-left: 2%;
 }
 .logoo
 {
       margin-left: 25%;
 }
#maintitle
{
        margin-left: -4px;
   width: 104%;
}
#firstform,#ProductDetails,#PharmacyDetails
{

   border-radius: 11px;
   border: 1px solid #808080;
   width: 96%;
   font-family: myFirstFont;
   margin-left: -15px;
}

}
@media screen and (min-device-width : 414px)
   and (-webkit-device-pixel-ratio: 3)
   {
.navbar2 {
           display: block!important;
     }
     #userimage {
           margin-top: -14px;
   width: 64%;
   margin-left: 2%;
 }
 .logoo
 {
       margin-left: 25%;
 }
#maintitle
{
           margin-left: -4px;
   width: 103%;
}
#firstform,#ProductDetails,#PharmacyDetails
{

      border-radius: 11px;
   border: 1px solid #808080;
   width: 97%;
   font-family: myFirstFont;
   margin-left: -21px;
}

   }
@media only screen and (max-width: 1200px) {

#checkbox
{
   margin-left: -21px
}
.imgg
   {
            padding-top: 8px;
   padding-left: 39px

   }
   #loo {
   margin-left: 82px;

}
#toggle
{
 padding-left: 28px;
}
}

</style>


</head>
<body>

<div class="navbar2">
<a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
   text-align: -webkit-center;
   padding-bottom: 6px;
   margin-left: -15px;
       border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
 </div>
 <br>
 <div style='width: 148%;
   text-align: -webkit-center;
   padding-bottom: 6px;
   margin-left: -15px;
       border-bottom: 1px solid #e9ebee;'>
 <a href='\admin\home' title='' style='color:black'>Home</a>
 </div>

  <br>
 <div style='width: 148%;
   text-align: -webkit-center;
   padding-bottom: 6px;
   margin-left: -15px;
       border-bottom: 1px solid #e9ebee;'>
  <a href='{{ route('logout') }}'
                                           onclick='event.preventDefault();
                                                    document.getElementById('logout-form').submit();'>
                                           Logout
                                       </a>




 </div>">
  <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
{{ csrf_field() }}
 </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">
<!-- title -->
<div class="container all">
 <div class="row al">
   <div class="col-sm-12" id="title">
     <h2> {{substr(Route::currentRouteName(),20)}} Product Edit </h2>
   </div>
 </div>

</div>

<!-- title Ends -->

<form class="form-horizontal" action="{{route('product.update',$productid)}}" method="POST">

 {{csrf_field()}}
 {{method_field('PATCH')}}

 @section('editMethod')
 @show

 <div class="container" id="main" style="    margin-top: 34px;">

   <div class="row" id="maintitle"  >

   <div class="col-sm-12" >
     <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#firstform"  >Client Details<p style="text-align: -webkit-right;
       margin-top: -20px;
       margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
     </button>

   </div>

 </div>
</div>

<div class="container" id="main1" style="margin-top: 11px;">

 <div class="row collapse" id="firstform" >
 <!-- Modal Box Code Starts -->
   <div class="col-sm-12" style="    text-align: right;">


 <!-- Modal -->
 <div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">

       <div class="modal-body" style="text-align: center;">
        <input type="text"   id="clientid"  style="    width: 50%;" placeholder="Client Id">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="clinetidsubmit" style="margin-left: 16px;">Submit</button>
       </div>

     </div>

   </div>
 </div>
   </div>
   <div id="result">
   <!-- Modal box code ends -->
   <div class="col-sm-3" style="    margin-top: 4px;">

     <label>First Name <span style="color:Red; font-size: 20px;">*</span></label>


     <input type="text"  rows="5" name="clientfname" id="clientfname" value="@yield('editclientfname')" required>

   </div>
   <div class="col-sm-3" id="firstrow">
     <label>Middle Name</label>
     <input type="text"   rows="5" name="clientmname" id="clientmname" value="@yield('editclientmname')">

   </div>
   <div class="col-sm-3" id="firstrow">
     <label>Last Name</label>
     <input type="text"  rows="5"  name="clientlname" id="clientlname" value="@yield('editclientlname')" >

   </div>
   <div class="col-sm-3" id="firstrow">
     <label>Email ID</label>
     <input type="text"  rows="5" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

   </div>
   <div class="col-sm-2" id="secondrow" style="margin-top: 35px;">

     <label style="  margin-bottom: 5px;">Select Country Code</label><br>
     <select  id="country" name="code" >

           <option value="+91">India</option>
         @for($i=0;$i<$count;$i++)
         <option value="{{$country_codes[$i]['dial_code']}}">{{ $country_codes[$i]['name']}}</option>
         @endfor
     </select>
   </div>
   <div class="col-sm-4" style="margin-top: 26px;">


     <label>Mobile No. <span style="color:Red; font-size: 20px;">*</span></label>


         <div class="row">
             <div class="col-sm-2">
               <input type="text"  id="phone1"  rows="5" required disabled>
             </div>
             <div class="col-sm-10">
                 <input type="text"  id="phone"  rows="5" name="clientmob" value="@yield('editclientmob')" maxlength="10" required>
             </div>
         </div>
   </div>
   <div class="col-sm-3" id="secondrow">


     <label>Alternate no.</label>

     <input type="text" rows="5" name="clientalternateno" id="clientalternateno" value="@yield('editclientalternateno')">
   </div>
   <div class="col-sm-3" id="secondrow">

     <label>Emergency Contact No.</label>
     <input type="text" rows="5" name="EmergencyContact" id="EmergencyContact" value="@yield('editEmergencyContact')" maxlength="13">
   </div>

  <div class="col-sm-12"> </div>
   <div class="col-sm-3" id="secondrow" style="margin-top: 26px;">

     <label style="  margin-bottom: 5px;">Reference<span style="color:Red; font-size: 20px;">*</span></label><br>
     <select name="reference" id="reference"  required>
       <option value="@yield('editreference')">@yield('editreference')</option>
       @foreach($reference as $reference)
       <option value="{{ $reference->Reference}}">{{ $reference->Reference}}</option>
       @endforeach
     </select>
   </div>
   <div class="col-sm-4" id="secondrow">
     <div id="source">
       <label>Source</label>
       <input type="text"   rows="5" name="source" id="source" value="@yield('editsource')">
     </div>
   </div>
   <div class="col-sm-12" id="secondrow" >

     <h4> Permanent Address</h4>
   </div>


   <div class="col-sm-4" id="secondrow" >
     <label>Address Line 1</label>
     <input type="text"   rows="5" name="Address1" id="Address1" value="@yield('editAddress1')">

   </div>
   <div class="col-sm-4" id="secondrow" >
     <label>Address Line 2</label>
     <input type="text"   rows="5" name="Address2" id="Address2" value="@yield('editAddress2')">

   </div>
   <div class="col-sm-4" id="secondrow" style="margin-top: 33px;">
     <label style="  margin-bottom: 5px;">City</label><br>
     <!-- <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')"> -->

     <select  id="City"  name="City" >

         <option value="@yield('editCity')">@yield('editCity')</option>
         @for($i=0;$i<$count1;$i++)
         <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
         @endfor
         <!-- <option value="+1">US</option>
         <option value="+44 ">UK</option> -->
     </select>


 </div>

 <div class="col-sm-4" id="secondrow" >
   <label>District</label>
   <input type="text"    rows="5" name="District" id="District" value="@yield('editDistrict')">

 </div>
 <div class="col-sm-4" id="secondrow" >
   <label>State</label>
 <!--   <input type="text"    rows="5" name="State" id="State" value="@yield('editState')"> -->
    <select name="State" id="State" value="@yield('editState')">
       <option value="@yield('editState')">@yield('editState')</option>
       <option value="AndhraPradesh">Andhra Pradesh</option>
       <option value="Arunachal Pradesh">Arunachal Pradesh </option>
       <option value="Assam">Assam</option>
       <option value="Bihar">Bihar</option>
       <option value="Chhattisgarh">Chhattisgarh</option>
       <option value="Chandigarh">Chandigarh</option>
       <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
       <option value="DamanandDiu">Daman and Diu</option>
       <option value="Delhi">Delhi</option>
       <option value="Goa">Goa</option>
       <option value="Haryana">Haryana</option>
        <option value="HimachalPradesh">Himachal Pradesh</option>
         <option value="JammuandKashmir">Jammu and Kashmir</option>
          <option value="Jharkhand">Jharkhand</option>
           <option value="Karnataka">Karnataka</option>
            <option value="Kerala">Kerala</option>
             <option value="MadhyaPradesh">Madhya Pradesh</option>
              <option value="Maharashtra">Maharashtra</option>
               <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                 <option value="Mizoram">Mizoram</option>
                  <option value="Nagaland">Nagaland</option>
                   <option value="Orissa">Orissa</option>
                    <option value="Punjab">Punjab</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Rajasthan">Rajasthan</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Sikkim">Sikkim</option>
                     <option value="TamilNadu">Tamil Nadu</option>
                     <option value="Tripura">Tripura</option>
                      <option value="UttarPradesh">Uttar Pradesh</option>
                       <option value="Uttarakhand">Uttarakhand</option>
                        <option value="WestBengal">West Bengal</option>

     </select>

 </div>
 <div class="col-sm-4" id="secondrow" >
   <label>Pincode</label>
   <input type="text"    rows="5" name="PinCode" id="PinCode" value="@yield('editPinCode')" maxlength="8">

 </div>

 <div class="col-sm-12" style="    margin-top: 30px;" >
   <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PresentAddress" style="    width: 100%;
   margin-left: -1px;" >Present Address<p style="text-align: -webkit-right;
   margin-top: -20px;
   margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
 </button>
</div>
<div class="col-sm-12 collapse" id="PresentAddress">
 <div class="col-sm-4" style="    margin-top: 19px;">
   <label>Address Line 1</label>
   <input type="text"    rows="5" name="PAddress1" id="PAddress1" value="@yield('editPAddress1')">
 </div>
 <div class="col-sm-4" style="    margin-top: 19px;">
   <label>Address Line 2</label>
   <input type="text"  rows="5" name="PAddress2" id="PAddress2" value="@yield('editPAddress2')">
 </div>
 <div class="col-sm-4" style="    margin-top: 20px;">
   <label>City</label>

   <!-- <input type="text"  rows="5" name="PCity" id="PCity" value="@yield('editPCity')"> -->


<select  id="PCity"  name="PCity" >

         <option value="@yield('editPCity')">@yield('editPCity')</option>
         @for($i=0;$i<$count1;$i++)
         <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
         @endfor
         <!-- <option value="+1">US</option>
         <option value="+44 ">UK</option> -->
     </select>




</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>District</label>
 <input type="text"  rows="5" name="PDistrict" id="PDistrict" value="@yield('editPDistrict')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>State</label>
<!--   <input type="text"  rows="5" name="PState" id="PState" value="@yield('editPState')"> -->
  <select name="PState" id="PState" value="@yield('editPtate')">
       <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
       <option value="AndhraPradesh">Andhra Pradesh</option>
       <option value="Arunachal Pradesh">Arunachal Pradesh </option>
       <option value="Assam">Assam</option>
       <option value="Bihar">Bihar</option>
       <option value="Chhattisgarh">Chhattisgarh</option>
       <option value="Chandigarh">Chandigarh</option>
       <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
       <option value="DamanandDiu">Daman and Diu</option>
       <option value="Delhi">Delhi</option>
       <option value="Goa">Goa</option>
       <option value="Haryana">Haryana</option>
        <option value="HimachalPradesh">Himachal Pradesh</option>
         <option value="JammuandKashmir">Jammu and Kashmir</option>
          <option value="Jharkhand">Jharkhand</option>
           <option value="Karnataka">Karnataka</option>
            <option value="Kerala">Kerala</option>
             <option value="MadhyaPradesh">Madhya Pradesh</option>
              <option value="Maharashtra">Maharashtra</option>
               <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                 <option value="Mizoram">Mizoram</option>
                  <option value="Nagaland">Nagaland</option>
                   <option value="Orissa">Orissa</option>
                    <option value="Punjab">Punjab</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Rajasthan">Rajasthan</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Sikkim">Sikkim</option>
                     <option value="TamilNadu">Tamil Nadu</option>
                     <option value="Tripura">Tripura</option>
                      <option value="UttarPradesh">Uttar Pradesh</option>
                       <option value="Uttarakhand">Uttarakhand</option>
                        <option value="WestBengal">West Bengal</option>

     </select>
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>Pincode</label>
 <input type="text"  rows="5" name="PPinCode" id="PPinCode" value="@yield('editPPinCode')">
</div>

</div>


 <div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
<input type="checkbox" name="presentcontact" value="same" style="    margin-left: -98px;" id="sameaspermanentadress">
 <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>

</div>








 <div class="col-sm-12" style="    margin-top: 30px;" >
   <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#shippingAddress" style="    width: 100%;
   margin-left: -1px;" >Shipping Address<p style="text-align: -webkit-right;
   margin-top: -20px;
   margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
 </button>
</div>
<div class="col-sm-12 collapse" id="shippingAddress">
 <div class="col-sm-4" style="    margin-top: 19px;">
   <label>Address Line 1</label>
   <input type="text"    rows="5" name="SAddress1" id="SAddress1" value="@yield('editSAddress1')">
 </div>
 <div class="col-sm-4" style="    margin-top: 19px;">
   <label>Address Line 2</label>
   <input type="text"  rows="5" name="SAddress2" id="SAddress2" value="@yield('editSAddress2')">
 </div>
 <div class="col-sm-4" style="    margin-top: 20px;">
   <label>City</label>
   <!-- <input type="text"  rows="5" name="SCity" id="SCity" value="@yield('editSCity')"> -->

<select  id="SCity"  name="SCity" >

         <option value="@yield('editSCity')">@yield('editSCity')</option>
         @for($i=0;$i<$count1;$i++)
         <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
         @endfor
         <!-- <option value="+1">US</option>
         <option value="+44 ">UK</option> -->
     </select>


</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>District</label>
 <input type="text"  rows="5" name="SDistrict" id="SDistrict" value="@yield('editSDistrict')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>State</label>
<!--   <input type="text"  rows="5" name="SState" id="SState" value="@yield('editSState')"> -->
  <select name="SState" id="SState" value="@yield('editSState')">
       <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
       <option value="AndhraPradesh">Andhra Pradesh</option>
       <option value="Arunachal Pradesh">Arunachal Pradesh </option>
       <option value="Assam">Assam</option>
       <option value="Bihar">Bihar</option>
       <option value="Chhattisgarh">Chhattisgarh</option>
       <option value="Chandigarh">Chandigarh</option>
       <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
       <option value="DamanandDiu">Daman and Diu</option>
       <option value="Delhi">Delhi</option>
       <option value="Goa">Goa</option>
       <option value="Haryana">Haryana</option>
        <option value="HimachalPradesh">Himachal Pradesh</option>
         <option value="JammuandKashmir">Jammu and Kashmir</option>
          <option value="Jharkhand">Jharkhand</option>
           <option value="Karnataka">Karnataka</option>
            <option value="Kerala">Kerala</option>
             <option value="MadhyaPradesh">Madhya Pradesh</option>
              <option value="Maharashtra">Maharashtra</option>
               <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                 <option value="Mizoram">Mizoram</option>
                  <option value="Nagaland">Nagaland</option>
                   <option value="Orissa">Orissa</option>
                    <option value="Punjab">Punjab</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Rajasthan">Rajasthan</option>
                     <option value="Pondicherry">Pondicherry</option>
                     <option value="Sikkim">Sikkim</option>
                     <option value="TamilNadu">Tamil Nadu</option>
                     <option value="Tripura">Tripura</option>
                      <option value="UttarPradesh">Uttar Pradesh</option>
                       <option value="Uttarakhand">Uttarakhand</option>
                        <option value="WestBengal">West Bengal</option>

     </select>
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
 <label>Pincode</label>
 <input type="text"  rows="5" name="SPinCode" id="SPinCode" value="@yield('editSPinCode')">
</div>
</div>


<div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
<input type="checkbox" name="shippingcontact" value="same" style="    margin-left: -98px;" id="shippingsameaspermananentaddress">
 <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>

</div>



 </div>
</div>
</div>
@if($pdept=="Product")
<!-- Product Deatials -->
<div class="container" id="main" style="    margin-top: 23px;">

 <div class="row" id="maintitle" >

 <div class="col-sm-12" >
   <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ProductDetails"  >Product Details <p style="text-align: -webkit-right;
     margin-top: -20px;
     margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
   </button>

 </div>

</div>
</div>


<div class="container" id="main1" style="margin-top: 11px;     ">

 <div class="row collapse" id="ProductDetails" >
    <?php
      $k=1;
    ?>
     @for($i=0;$i<$product_count;$i++)
     <div class="col-sm-12">
      <h3> Product {{$k}} Details </h3>

     </div>
    <?php
     if($k =="1")
      {?>

          <div class="col-sm-3" id="firstrow">

            <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName"  list="Products" value="{{$productName_array[$i]}}">

          </div>
          <div class="col-sm-3" id="firstrow">

          <label>SKU ID</label><input type="text"  rows="5" name="SKUid" id="SKUid"  value="{{$SKUid_array[$i]}}">
          </div>

           <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

          <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>

          <select name="Type" id="Type" >
           <option value="{{$Type}}">{{$Type}}</option>

            <option value="Sell">Sell</option>
            <option value="Rent">Rent</option>
          </select>
          <p id="cautiontoentertype" style="display: block;">Please select Type </p>



        </div>
        <!-- Demo Required Starts -->
        <?php
          if($i == "0")
          {
          ?>
                 <div class="col-sm-3" id="firstrow">
                      <label>Demo Required</label>
                        <select name="DemoRequired" id="DemoRequired">
                          <option value="{{$DemoRequired}}">{{$DemoRequired}}</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>
                  </div>
          <?php
          }
          else
          {?>
             <div class="col-sm-3" id="firstrow"> </div>
          <?php
           }
          ?>
           <!-- Demo Required Ends -->
          <div class="col-sm-12"></div>
           <div class="col-sm-3" style="margin-top: 26px;">

              <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="{{$AvailabilityStatus_array[$i]}}">
          </div>

          <div class="col-sm-3" style="margin-top: 26px;">

            <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="{{$AvailabilityAddress_array[$i]}}">
          </div>

           <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
              <label>Quantity</label><input type="text"  rows="5" name="Quantity{{$k}}" id="Quantity{{$k}}" value="{{$Quantity_array[$i]}}">
          </div>
          <!-- Delivery Date starts -->
           <?php
          if($i == "0")
          {
          ?>
                 <div class="col-sm-3" id="firstrow">
                      <label>Delivery Date</label>
                      <input type="date" rows="5" name="deliverydate" id="deliverydate" value="">
                  </div>
          <?php
          }
          else
          {?>
             <div class="col-sm-3" id="firstrow"> </div>
          <?php
           }
          ?>
          <div class="col-sm-12"></div>
          <!-- Delivery Date Ends -->
          <!-- Sell code starts -->
          <?php
            if($Type=="Sell")
            {?>
                  <div id="sell">
                    <div class="col-sm-3" style="margin-top: 26px;">
                      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="{{$SellingPrice_array[$i]}}">
                  </div>

                <?php
                if($i == "0")
                {
                ?>
                 <div class="col-sm-3" style="margin-top: 27px;" id="firstrow">
                      <label>Mode of Payment</label>
                      <select name="ModeofPayment" id="ModeofPayment">
                        <option value="{{$ModeofPayment}}">{{$ModeofPayment}}</option>
                        <option value="Swipe">Swipe</option>
                        <option value="Cash">Cash</option>
                        <option value="NEFT">NEFT</option>
                        <option value="Cheque">Cheque</option>
                       </select>
                  </div>
                <?php
                }
                else
                {?>
                  <div class="col-sm-3" id="firstrow"> </div>
                <?php
                }
                ?>
        <?php
            }
          ?>
          <!-- Sell Ends -->
          <!-- Rent Code starts -->
          <?php
            if($Type=="Rent")
            {?>
                  <div id="rent">
                    <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

                    <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="{{$RentalPrice_array[$i]}}">
                  </div>
                   <div class="col-sm-3" style="margin-top: 26px;">
                      <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt" id="AdvanceAmt" value="{{$AdvanceAmt_array[$i]}}">
                    </div>
                     <div class="col-sm-3" style="margin-top: 26px;">
                        <label>Start Date</label><input type="date"  rows="5" name="StartDate" id="StartDate" value="{{$StartDate_array[$i]}}">
                    </div>
                    <div class="col-sm-3" style="margin-top: 26px;">
                      <label>End Date</label><input type="date"  rows="5" name="EndDate" id="EndDate" value="{{$EndDate_array[$i]}}">
                    </div>
                <?php
                if($i == "0")
                {
                ?>
                <div class="col-sm-12"></div>
                <div class="col-sm-3" style="margin-top: 26px;">
                  <label>Transportation Charges</label><input type="text" name="TransportCharges" id="TransportCharges" value="{{$TransportationCharges}}">
                </div>
                 <div class="col-sm-3" style="    margin-top: 27px;" id="firstrow">
                      <label>Mode of Payment</label>
                      <select name="ModeofPayment" id="ModeofPayment">
                        <option value="{{$ModeofPayment}}">{{$ModeofPayment}}</option>
                        <option value="Swipe">Swipe</option>
                        <option value="Cash">Cash</option>
                        <option value="NEFT">NEFT</option>
                        <option value="Cheque">Cheque</option>
                       </select>
                  </div>
                <?php
                }
                else
                {?>
                  <div class="col-sm-3" id="firstrow"> </div>
                <?php
                }
                ?>
        <?php
            }
          ?>
          <!-- Rent Code starts -->
      <?php
      }
      else
      {
      ?>



        <div class="col-sm-3" id="firstrow">

          <label>Product Name</label><input type="text"  rows="5" name="ProductName{{$k}}" id="ProductName{{$k}}"  list="Products" value="{{$productName_array[$i]}}">
        </div>

        <div class="col-sm-3" id="firstrow">
        <label>SKU ID</label><input type="text"  rows="5" name="SKUid{{$k}}" id="SKUid{{$k}}"  value="{{$SKUid_array[$i]}}">


        </div>

       <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

          <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>
          <select name="Type{{$k}}" id="Type{{$k}}" >
          <option value="{{$Type}}">{{$Type}}</option>
          <option value="Sell">Sell</option>
          <option value="Rent">Rent</option>
          </select>
      </div>
           <?php
          if($i == "0")
          {
          ?>
                 <div class="col-sm-3" id="firstrow">
                      <label>Demo Required</label>
                        <select name="DemoRequired" id="DemoRequired">
                          <option value="{{$DemoRequired}}">{{$DemoRequired}}</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>
                  </div>
          <?php
          }
          else
          {?>
             <div class="col-sm-3" id="firstrow"> </div>
          <?php
           }
          ?>
          <!-- Demo Required Ends -->
           <div class="col-sm-12"></div>
           <div class="col-sm-3" style="margin-top: 26px;">

              <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus{{$k}}" id="AvailabilityStatus{{$k}}" value="{{$AvailabilityStatus_array[$i]}}">
          </div>
          <div class="col-sm-3" style="margin-top: 26px;">

            <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress{{$k}}" id="AvailabilityAddress{{$k}}" value="{{$AvailabilityAddress_array[$i]}}">
          </div>
           <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
              <label>Quantity</label><input type="text"  rows="5" name="Quantity{{$k}}" id="Quantity{{$k}}" value="{{$Quantity_array[$i]}}">
          </div>

          <?php
          if($i == "0")
          {
          ?>
                 <div class="col-sm-3" id="firstrow">
                      <label>Delivery Date</label>
                      <input type="date" rows="5" name="deliverydate" id="deliverydate" value="">
                  </div>
          <?php
          }
          else
          {?>
             <div class="col-sm-3" id="firstrow"> </div>
          <?php
           }
          ?>
          <div class="col-sm-12"></div>
              <!-- Sell code starts -->
        <?php
            if($Type=="Sell")
            {?>
                  <div id="sell">
                    <div class="col-sm-3" style="margin-top: 26px;">
                      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice{{$k}}" id="SellingPrice{{$k}}" value="{{$SellingPrice_array[$i]}}">
                  </div>

                <?php
                if($i == "0")
                {
                ?>
                 <div class="col-sm-3" style="    margin-top: 27px;" id="firstrow">
                      <label>Mode of Payment</label>
                      <select name="ModeofPayment" id="ModeofPayment">
                        <option value="{{$ModeofPayment}}">{{$ModeofPayment}}</option>
                        <option value="Swipe">Swipe</option>
                        <option value="Cash">Cash</option>
                        <option value="NEFT">NEFT</option>
                        <option value="Cheque">Cheque</option>
                       </select>
                  </div>
                <?php
                }
                else
                {?>
                  <div class="col-sm-3" id="firstrow"> </div>
                <?php
                }
                ?>
        <?php
            }
          ?>
          <!-- Sell code ends -->
              <!-- Rent Code starts -->
          <?php
            if($Type=="Rent")
            {?>
                  <div id="rent">
                    <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

                    <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice{{$k}}" id="RentalPrice{{$k}}" value="{{$RentalPrice_array[$i]}}">
                  </div>
                   <div class="col-sm-3" style="margin-top: 26px;">
                      <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt{{$k}}" id="AdvanceAmt{{$k}}" value="{{$AdvanceAmt_array[$i]}}">
                    </div>
                     <div class="col-sm-3" style="margin-top: 26px;">
                        <label>Start Date</label><input type="date"  rows="5" name="StartDat{{$k}}e" id="StartDate{{$k}}" value="{{$StartDate_array[$i]}}">
                    </div>
                    <div class="col-sm-3" style="margin-top: 26px;">
                      <label>End Date</label><input type="date"  rows="5" name="EndDate{{$k}}" id="EndDate{{$k}}" value="{{$EndDate_array[$i]}}">
                    </div>
                <?php
                if($i == "0")
                {
                ?>
                  <div class="col-sm-12"></div>
                   <div class="col-sm-3" style="margin-top: 26px;">
                  <label>Transportation Charges</label><input type="text" name="TransportCharges" id="TransportCharges" value="{{$TransportationCharges}}">
                </div>
                 <div class="col-sm-3" id="firstrow">
                      <label>Mode of Payment</label>
                      <select name="ModeofPayment" id="ModeofPayment">
                        <option value="{{$ModeofPayment}}">{{$ModeofPayment}}</option>
                        <option value="Swipe">Swipe</option>
                        <option value="Cash">Cash</option>
                        <option value="NEFT">NEFT</option>
                        <option value="Cheque">Cheque</option>
                       </select>
                  </div>
                <?php
                }
                else
                {?>
                  <div class="col-sm-3" id="firstrow"> </div>
                <?php
                }
                ?>
        <?php
            }
          ?>
          <!-- Rent Code starts -->

      <?php
       }
      ?>
<!--
     <div class="col-sm-12">

     </div>

     <br>



   <div class="col-sm-3" id="firstrow">

     <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName"  list="Products" value="{{$productName_array[$i]}}">

   </div>

    <div class="col-sm-3" id="firstrow">
    <label>SKU ID</label><input type="text"  rows="5" name="SKUid" id="SKUid"  value="{{$SKUid_array[$i]}}">
</div>


   <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

     <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>
     <select name="Type" id="Type" >
       <option value="{{$Type}}">{{$Type}}</option>
       <option value="Sell">Sell</option>
       <option value="Rent">Rent</option>
     </select>
     <p id="cautiontoentertype" style="display: block;">Please select Type </p>

   </div>
      @if($i==0)
   <div class="col-sm-3" id="firstrow">

     <label>Demo Required</label>

     <select name="DemoRequired" id="DemoRequired">
       <option value="{{$DemoRequired}}">{{$DemoRequired}}</option>
       <option value="Yes">Yes</option>
       <option value="No">No</option>
     </select>

   </div>
   @endif
<div class="col-sm-12"></div>
   <div class="col-sm-3" style="margin-top: 26px;">

     <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="{{$AvailabilityStatus_array[$i]}}">
   </div>
   <div class="col-sm-3" style="margin-top: 26px;">

     <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="{{$AvailabilityAddress_array[$i]}}">
   </div>
   <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
    <label>Quantity</label><input type="text"  rows="5" name="Quantity" id="Quantity" value="{{$Quantity_array[$i]}}">
   </div>

   <div class="col-sm-12"></div>

   @if($Type=="Sell")
  <div id="sell">
   <div class="col-sm-3" style="margin-top: 26px;">

     <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="{{$SellingPrice_array[$i]}}">
   </div>


   @if($i==0)
   <div class="col-sm-3" style="margin-top: 26px;">
    <label>Mode of Payment</label>
    <select name="ModeofPayment" id="ModeofPayment">
       <option value="{{$ModeofPayment}}">{{$ModeofPayment}}</option>
       <option value="Swipe">Swipe</option>
       <option value="Cash">Cash</option>
       <option value="NEFT">NEFT</option>
       <option value="Cheque">Cheque</option>
     </select>
   </div>


  </div>
  @endif
 @endif -->
<!-- @if($Type=="Rent")
<div id="rent">
   <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

     <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="{{$RentalPrice_array[$i]}}">
   </div>



   <div class="col-sm-3" style="margin-top: 26px;">
    <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt" id="AdvanceAmt" value="{{$AdvanceAmt_array[$i]}}">
   </div>
    <div class="col-sm-3" style="margin-top: 26px;">
    <label>Start Date</label><input type="date"  rows="5" name="StartDate" id="StartDate" value="{{$StartDate_array[$i]}}">
   </div>
    <div class="col-sm-3" style="margin-top: 26px;">
    <label>End Date</label><input type="date"  rows="5" name="EndDate" id="EndDate" value="{{$EndDate_array[$i]}}">
   </div>
   <div class="col-sm-12"></div>

@if($i==0)
   <div class="col-sm-3" style="margin-top: 26px;">
       <label>Transportation Charges</label><input type="text" name="TransportCharges" id="TransportCharges" value="{{$TransportationCharges}}">
  </div>


    <div class="col-sm-3" style="margin-top: 26px;">
    <label>Mode of Payment</label>
    <select name="ModeofPaymentrent" id="ModeofPaymentrent">
       <option value="{{$ModeofPaymentrent}}">{{$ModeofPaymentrent}}</option>
       <option value="Swipe">Swipe</option>
       <option value="Cash">Cash</option>
       <option value="NEFT">NEFT</option>
       <option value="Cheque">Cheque</option>

     </select>
   </div>
   @endif
   @endif -->
   <div class="col-sm-12"></div>
   <?php $k=$k+1;?>
  @endfor

<!--     <div class="col-sm-3" style="margin-top: 26px;">

     <label>Order Status</label>
   <select name="OrderStatusrent" id="OrderStatusrent">
       <option value="@yield('editOrderStatusrent')">@yield('editOrderStatusrent')</option>
       <option value="Pending">Pending</option>
       <option value="Processing">Processing</option>
       <option value="Awaiting Pickup">Awaiting Pickup</option>
        <option value="Ready to ship">Ready to ship</option>
        <option value="Out for Delivery">Out for Delivery</option>
       <option value="Delivered">Delivered</option>
       <option value="Cancelled">Cancelled</option>
     </select>

   </div> -->

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
    <label>Overdue Amount</label><input type="text"  rows="5" name="OverdueAmt" id="OverdueAmt" value="@yield('editOverdueAmt')">
   </div> -->


<input type="hidden" name="nooffileds" id="value">
   <!-- Add inut flieds -->

<div class="col-sm-12">
 <div id="moreinput"></div>
</div>
<div class="col-sm-12" style="text-align: right;">
 <button type="button"  id="addmoreinputfiled" style="outline: none; background: white;
   border: 0px;">
 <img src="/img/Add-32.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
</div>
</div>



 </div>
</div>
@endif



@if($pdept=="Pharmacy")
<!-- Pharmacy Details -->
<div class="container" id="main" style="    margin-top: 23px;">

 <div class="row" id="maintitle" >

 <div class="col-sm-12" >
   <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PharmacyDetails"  >Pharmacy Details <p style="text-align: -webkit-right;
     margin-top: -20px;
     margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
   </button>

 </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;">

 <div class="row collapse" id="PharmacyDetails" >

    <!--  @for($i=0;$i<$product_count;$i++)


      @endfor -->
      <?php
            $n=1;
            $k=1;
          for($i=0; $i < $product_count ; $i++)
          {
              ?>




              <?php

                if($i == 0)
                {?>
                        <div class="col-sm-12">
                            <h3> Pharmacy  1 Details </h3>
                        </div>


                         <div class="col-sm-3" id="firstrow" >
                            <label>Type </label>
                            <select name="MedType" id="MedType" >
                                <option value="{{$MedType_array[$i]}}">{{$MedType_array[$i]}}</option>
                                <option value="Tablet">Tablet</option>
                                <option value="Capsule">Capsule</option>
                                <option value="Injection">Injection</option>
                                <option value="Ointment">Ointment</option>
                                <option value="Scrub">Scrub</option>
                                 <option value="Cream">Cream</option>
                                 <option value="Powder">Powder</option>
                                 <option value="Lotion">Lotion</option>
                                 <option value="Syrum">Syrum</option>
                                 <option value="Drops">Drops</option>
                                 <option value="Gel">Gel</option>
                                 <option value="Expectorant">Expectorant</option>
                                 <option value="Respules">Respules</option>
                                 <option value="Handrub">Handrub</option>
                                 <option value="Syrup">Syrup</option>
                             </select>
                             <p id="cautiontoentertypepharmacytype" style="display: block;">Please select Type </p>
                        </div>


                        <div class="col-sm-3" id="firstrow">
                          <label>Medicine Name</label><input type="text"  rows="5" name="MedName" id="MedName" value="{{$medName_array[$i]}}">
                           <p id="cautiontoentertypepharmacymedname" style="display: block;">Please Enter Medicine Name </p>
                        </div>
                        <div class="col-sm-3" id="firstrow">
                          <label>Strength</label><input type="text"  rows="5" name="Strength" id="Strength" value="{{$Strength_array[$i]}}">
                          <p id="cautiontoentertypepharmacystrength" style="display: block;">Please Enter Strength </p>
                        </div>

                        <div class="col-sm-3" id="firstrow">
                           <label>Quantity &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</label>
                           <input type="text"  rows="5" name="pQuantity" id="pQuantity" value="{{$PQuantity_array[$i]}}" style="width:50%;">
                            <input type="text" name="QuantityType" id="QuantityType" list="QuantityTypelist"  value="{{$QuantityType_array[$i]}}"   style="width:48%;">

                                <datalist id="QuantityTypelist">
                                    <option value="Strip">Strip</option>
                                    <option value="Tablets">Tablets</option>
                                </datalist>


                          <p id="cautiontoentertypepharmacyquantity" style="display: block;">Please Enter Quantity </p>
                        </div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-3" style="margin-top: 26px;">
                          <label>Availability Status</label><input type="text"  rows="5" name="pAvailabilityStatus" id="pAvailabilityStatus" value="{{$PAvailabilityStatus_array[$i]}}">
                        </div>
                        <div class="col-sm-3" style="margin-top: 26px;">
                          <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice" id="pSellingPrice" value="{{$Price_array[$i]}}">
                         </div>
                        @if($i==0)
                          <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
                            <label>Mode of payment</label>
                            <select name="PModeofpayment" id="PModeofpayment">
                             <option value="{{$PModeofpayment}}">{{$PModeofpayment}}</option>
                             <option value="Swipe">Swipe</option>
                             <option value="Cash">Cash</option>
                             <option value="NEFT">NEFT</option>
                             <option value="Cheque">Cheque</option>
                           </select>
                          </div>
                        @endif
                       <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

                         <label>Order Status</label>
                         <select name="pOrderStatus" id="pOrderStatus">
                           <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
                           <option value="Pending">Pending</option>
                           <option value="Processing">Processing</option>
                           <option value="Awaiting Pickup">Awaiting Pickup</option>
                            <option value="Ready to ship">Ready to ship</option>
                            <option value="Out for Delivery">Out for Delivery</option>
                           <option value="Delivered">Delivered</option>
                           <option value="Cancelled">Cancelled</option>
                           <option value="Order Return">Order Return</option>
                         </select>
                       </div> -->
                       <div class="col-sm-12"></div>

                <?php
                }else
                {?>
                      <div class="col-sm-12">
                          <h3> Pharmacy   {{$n}} Details </h3>
                        </div>

                        <div class="col-sm-3" id="firstrow" >
                            <label>Type </label>
                            <select name="MedType{{$n}}" id="MedType{{$n}}" >
                                <option value="{{$MedType_array[$i]}}">{{$MedType_array[$i]}}</option>
                                <option value="Tablet">Tablet</option>
                                <option value="Capsule">Capsule</option>
                                <option value="Injection">Injection</option>
                                <option value="Ointment">Ointment</option>
                                <option value="Scrub">Scrub</option>
                                 <option value="Cream">Cream</option>
                                 <option value="Powder">Powder</option>
                                 <option value="Lotion">Lotion</option>
                                 <option value="Syrum">Syrum</option>
                                 <option value="Drops">Drops</option>
                                 <option value="Gel">Gel</option>
                                 <option value="Expectorant">Expectorant</option>
                                 <option value="Respules">Respules</option>
                                 <option value="Handrub">Handrub</option>
                                 <option value="Syrup">Syrup</option>
                             </select>
                             <p id="cautiontoentertypepharmacytype{{$n}}" style="display: block;">Please select Type </p>
                        </div>

                        <div class="col-sm-3" id="firstrow">
                          <label>Medicine Name</label><input type="text"  rows="5" name="MedName{{$n}}" id="MedName{{$n}}" value="{{$medName_array[$i]}}">
                          <p id="cautiontoentertypepharmacymedname{{$n}}" style="display: block;">Please Enter Medicine Name </p>
                        </div>
                        <div class="col-sm-3" id="firstrow">
                          <label>Strength</label><input type="text"  rows="5" name="Strength{{$n}}" id="Strength{{$n}}" value="{{$Strength_array[$i]}}">
                          <p id="cautiontoentertypepharmacystrength{{$n}}" style="display: block;">Please Enter Strength </p>
                        </div>

                        <div class="col-sm-3" id="firstrow">

                          <label>Quantity &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</label><input type="text"  rows="5" name="pQuantity{{$n}}" id="pQuantity{{$n}}" value="{{$PQuantity_array[$i]}}" style="width:50%;">

                          <input type="text" name="QuantityType{{$n}}" id="QuantityType{{$n}}" list="QuantityTypelist"  value="{{$QuantityType_array[$i]}}"   style="width:48%;">

                              <datalist id="QuantityTypelist">
                                  <option value="Strip">Strip</option>
                                  <option value="Tablets">Tablets</option>
                              </datalist>
                           <p id="cautiontoentertypepharmacyquantity{{$n}}" style="display: block;">Please Enter Quantity </p>

                        </div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-3" style="margin-top: 26px;">
                          <label>Availability Status</label><input type="text"  rows="5" name="pAvailabilityStatus{{$n}}" id="pAvailabilityStatus{{$n}}" value="{{$PAvailabilityStatus_array[$i]}}">
                        </div>
                        <div class="col-sm-3" style="margin-top: 26px;">
                          <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice{{$n}}" id="pSellingPrice{{$n}}" value="{{$Price_array[$i]}}">
                         </div>
                        @if($i==0)
                          <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
                            <label>Mode of payment</label>
                            <select name="PModeofpayment{{$n}}" id="PModeofpayment{{$n}}">
                             <option value="{{$PModeofpayment}}">{{$PModeofpayment}}</option>
                             <option value="Swipe">Swipe</option>
                             <option value="Cash">Cash</option>
                             <option value="NEFT">NEFT</option>
                             <option value="Cheque">Cheque</option>
                           </select>
                          </div>
                        @endif
                       <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

                         <label>Order Status</label>
                         <select name="pOrderStatus" id="pOrderStatus">
                           <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
                           <option value="Pending">Pending</option>
                           <option value="Processing">Processing</option>
                           <option value="Awaiting Pickup">Awaiting Pickup</option>
                            <option value="Ready to ship">Ready to ship</option>
                            <option value="Out for Delivery">Out for Delivery</option>
                           <option value="Delivered">Delivered</option>
                           <option value="Cancelled">Cancelled</option>
                           <option value="Order Return">Order Return</option>
                         </select>
                       </div> -->
                        <div class="col-sm-12"></div>

                <?php
                }


                $k=$k+1;
                $n=$n+1;

          }



      ?>


<input type="hidden" name="nooffiledsPharmacyDetails" id="nooffiledsPharmacyDetails" value="">
   <!-- Add inut flieds -->

<div class="col-sm-12">
 <div id="moreinputPharmacyDetails"></div>
</div>
<div class="col-sm-12" style="text-align: right;">
 <button type="button"  id="addmoreinputfiledPharmacyDetails" style="outline: none; background: white;
   border: 0px;">
 <img src="/img/Add-32.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
</div>

 </div>

</div>
@endif
 </div>

</div>







   <div class="container" style="    margin-top: 32px;">
         <div class="row">
               <div class="col-sm-12">
                   <input type="hidden" name="value" value="{{$value}}">
                  <center>
               <button type="submit" class="btn btn-default" id="submitt" >Update</button>
                  </center>
               </div>
         </div>
   </div>
 </div>
</div>
@include('partial.errors')
</form>
</div>
</body>

@endsection
