<!-- This page acts as a version of the View Leads page -->
<script>
$(document).ready(function(){
  $('.footable').footable();



});
</script>
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
    <td>{{$lead->PAssignedTo}}</td>
  <td style="color:red;">{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td>{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td>{!! nl2br(e($lead->POrderStatus)) !!}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "PAssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
     <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
    <td><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
    <td style="color:red;">{{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td>{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td>{!! nl2br(e($lead->POrderStatus)) !!}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "orderid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
     <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
     <td style="color:red;"><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
     <td>{{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td>{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td>{!! nl2br(e($lead->POrderStatus)) !!}</td>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "MedName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
     <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
     <td><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
     <td>{{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td style="color:red;">{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td>{!! nl2br(e($lead->POrderStatus)) !!}</td>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "MobileNumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
<th><b> Order ID</b></th>
     <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
     <td>{{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td>{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td>{!! nl2br(e($lead->POrderStatus)) !!}</td>
    </tr>
    @endforeach
  </tbody>
</table>



      @elseif ($filter1 === "POrderStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
     <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet"><b>MedName</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                 <td><a href="pharmacyshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
     <td>{{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>

  <td>{!! nl2br(e($lead->MedName)) !!}</td>
  <td>{!! nl2br(e($lead->PQuantity)) !!}</td>
  <td>{!! nl2br(e($lead->Price)) !!}</td>
  <td>{!! nl2br(e($lead->PModeofpayment)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->POrderStatus)) !!}</td>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

             

<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
@endif
