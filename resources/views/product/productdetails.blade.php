@extends('layouts.productmanager')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Health Heal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
    <style type="text/css">
    @font-face {
        font-family: myFirstFont;
        src: url(/raleway/Raleway-Regular.ttf);
    }
    select
    {
        font-family: myFirstFont;
    }
    select:focus
    {
        border-bottom:1px solid #00cccc;
    }
    select
    {
        border-bottom-color:#484e51;
        border-top: 0px;
        width: 100%;
        border-left: 0px;
        border-right: 0px;
        background: none;
        outline: none;
    }
    option
    {
        background: none;
    }
    body
    {
        font-family: myFirstFont;
    }
    .clinetdetails
    {
        background: white;
        border-color: #ddd;
    }
    h3
    {
        margin-top: auto;
    }
    .panel
    {
        border-radius: 0px;
        margin-left: -15px;
        width: 105.64%;
        margin-top: 10px;
    }
    #adress
    {
        width: 78%;

        height: auto;
        display: inline-block;
        text-align: -webkit-left;
        padding-left: 13px;
    }
    .productdetails
    {
        width: 100%;

        max-height: 550px;
        overflow-y: scroll;
    }
    .productdetails1
    {
        width: 100%;

        max-height: 200px;
        overflow-y: scroll;
    }
    hr
    {
        border-top: 1px solid black;
    }

    .companydetails
    {
        text-align: justify;
        padding-top: 23px;
        width: 53%;
        margin-left: 13px;
    }
    #logoimage
    {
        margin-top: 3px;
    }
    .footer
    {
        padding-top: 10px;
        text-align: center;
    }
    </style>
</head>
<body>
    <!-- 2260, 4th 'A' cross, 1st Main Road Vijayanagar, Club Ave, Vijay nagar, Bengaluru, Karnataka 560040 -->
    <div class="container" style="        margin-top: 68px;">
        <div class="row">
            <div class="col-sm-6" style="background: #eee;">
                <div class="panel panel-default" style="margin-top: 10px;">
                    @foreach($leaddetails as $leaddetails)
                    <div class="panel-body">
                        <h3>Client Details </h3>

                        <div class="col-sm-3" style="width:50%">
                            <p style="padding-top: 10px;"> <b> Lead Id &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</b> {{$leaddetails->id}}</p>
                            <p > <b>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </b> &nbsp;{{$leaddetails->fName}}</p>


                            <p style="display: inline-block;"> <b>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> &nbsp;
                            </p>

                            <div id="address" style="display: inline-block;">
                                {{$leaddetails->Address1}} {{$leaddetails->Address2}}

                            </div><br>


                        </div>

                        <div class="col-sm-3" style="width:50%">

                            <p style="padding-top: 10px;"> <b>Email Id &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> &nbsp; {{$leaddetails->EmailId}}</p>
                            <p > <b>Mobile No.&nbsp;&nbsp;: </b> &nbsp; {{$leaddetails->MobileNumber}}</p>

                            <p> <b>Pincode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> &nbsp;
                            </p>{{$leaddetails->PinCode}}
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-6" style="width:100%">
                    <div class="col-sm-3" style="width:50%">
                        <p> <b> Order ID &emsp;: &emsp; </b>{{$orderid}}</p>
                    </div>
                    <div class="col-sm-3" style="width:50%">
                        <p style="float: right"> <b> DATE &emsp;: </b>&emsp;{{$leaddetails->created_at}}</p>
                    </div>
                </div>



                <form class="form-horizontal" action="/productstatus" method="POST">
                    {{csrf_field()}}
                    @section('editMethod')
                    @show

                    <div class="col-sm-6" style=" width:100%;   margin-top: 11px;">
                        <div class="col-sm-3" style=" width:100%;   margin-top: 11px;">
                            <b>Comments:</b> <textarea class="form-control" disabled col=30; rows=5; style="width:100%; background: white;">{{$comments}}</textarea>
                        </div>
                        <div class="col-sm-3" style=" width:100%;   margin-top: 11px;">
                            <b>Remarks:</b> <textarea class="form-control" name="comment" col=30; rows=5; style="width:100%; background: white;"></textarea>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-4" style="margin-top: 11px;">
                        <label><b style="color: #333;"> Status</b></label>
                        @if($k5=="Sell" || $k5==NULL)

                        <select name="OrderStatus" id="OrderStatus" >
                            <option value="{{$k6}}">{{$k6}}</option>
                            @if($k6=="New")
                            <option value="Processing">Processing</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k6=="Processing")
                            <option value="Awaiting Pickup">Awaiting Pickup</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k6=="Awaiting Pickup")
                            <option value="Ready to ship">Ready to ship</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k6=="Ready to ship")
                            <option value="Out for Delivery">Out for Delivery</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k6=="Out for Delivery")
                            <option value="Delivered">Delivered</option>
                            <option value="Order Return">Order Return</option>

                            @else
                            @if($k6=="Delivered")

                            @else
                            @if($k6=="Order Return")
                            <option value="Received Order Return">Received Order Return</option>
                            @else
                            @if($k6=="Cancelled")
                            <option value="Processing">Processing</option>
                            @else
                            @if($k6=="Received Order Return")

                            @endif
                            @endif
                            @endif
                            @endif
                            @endif
                            @endif

                            @endif

                            @endif
                            @endif

                        </select>

                        @else

                        <select name="OrderStatus" id="OrderStatus" >

                            <option value="{{$k9}}">{{$k9}}</option>
                            @if($k9=="New")
                            <option value="Processing">Processing</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k9=="Processing")
                            <option value="Awaiting Pickup">Awaiting Pickup</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k9=="Awaiting Pickup")
                            <option value="Ready to ship">Ready to ship</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k9=="Ready to ship")
                            <option value="Out for Delivery">Out for Delivery</option>
                            <option value="Cancelled">Cancelled</option>
                            @else
                            @if($k9=="Out for Delivery")
                            <option value="Delivered">Delivered</option>
                            <option value="Order Return">Order Return</option>

                            @else
                            @if($k9=="Delivered")

                            @else
                            @if($k9=="Order Return")
                            <option value="Received Order Return">Received Order Return</option>
                            @else
                            @if($k9=="Cancelled")
                            <option value="Processing">Processing</option>
                            @else
                            @if($k6=="Received Order Return")

                            @endif
                            @endif
                            @endif
                            @endif
                            @endif
                            @endif

                            @endif

                            @endif
                            @endif

                        </select>
                        @endif


                    </div>

                    <div class="col-sm-4" style="   margin-top: 11px;">
                        <input type="hidden" name="loginname" value="{{ Auth::guard('admin')->user()->name }}">
                        <input type="hidden" name="type" value="{{$k5}}">
                        <input type="hidden" name="id" value="{{$orderid}}">
                        <input type="hidden" name="productid" value="{{$productid}}">

                        @for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)


                        <?php  if($k5=="Rent")
                        {

                            $sum = $sum + $k15[$cnt1]*$k7[$cnt1];
                        }
                        else{
                            $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                        }  ?>
                        @endfor


                        @if($desig!="Product Manager")
                        <label><b style="color: #333;"> Assign To</b></label>
                        @if($assign==NULL)
                        <select name="assignto" id="assignto" >
                            <option ></option>
                            @foreach($executive as $e)
                            <option value="{{$e->FirstName}}">{{$e->FirstName}}</option>
                            @endforeach
                        </select>
                    </div>
                    @else
                    <select name="assignto" id="assignto" >
                        <option value="{{$assign}}">{{$assign}}</option>

                    </select>
                </div>
                @endif


                @else
                <label><b style="color: #333;"> Assign To</b></label>

                @if($officerassign==NULL)
                <select name="assignofficer" id="assignofficer">
                    <option ></option>
                    @foreach($officers as $o)
                    <option value="{{$o->FirstName}}">{{$o->FirstName}}</option>
                    @endforeach
                    @foreach($brancheadname as $brancheadname)
                    <option value="{{$brancheadname->FirstName}}">{{$brancheadname->FirstName}}</option>
                    @endforeach

                </select>
                @else
                <select name="assignofficer" id="assignofficer">
                    <option value="{{$officerassign}}">{{$officerassign}}</option>

                </select>
                @endif

            </div>


            <div class="col-sm-4"  style="margin-top: 11px;">
                <label><b style="color: #333;">Mode of Payment</b></label>


                @if($k5=="Sell" || $k5=="")
                <select name="modeofpayment" >

                    <option>{{$k10}}</option>
                    <option>Swipe</option>
                    <option>COD</option>
                    <option>Cheque</option>
                    <option>NEFT</option>

                </select>
                @else

                <select name="modeofpaymentrent" >
                    <option>{{$k8}} </option>
                    <option>Swipe</option>
                    <option>COD</option>
                    <option>Cheque</option>
                    <option>NEFT</option>
                </select>
                @endif
            </div>


            <div class="col-sm-4" style="margin-top: 24px;">
                <label><b style="color: #333;">Total Amount</b></label>

                <input type="text" name="total" style="background-color: transparent;" value="{{$FinalAmount}}">
            </div>

            @if($k5=="Rent" || $role=="Admin" || $role=="Management")
            <div class="col-sm-4" style="margin-top: 24px;">
                <label><b style="color: #333;">Transportation Charges</b></label>

                <input type="text" name="TransportCharges" style="background-color: transparent;" value="{{$TransportCharges}}">
            </div>
            @endif

            <div class="col-sm-4" style="margin-top: 24px;">
                <label><b style="color: #333;">Discount %</b></label>
                <input type="text" name="discount" style="background-color: transparent;" value="{{$discount}}">
            </div>







            @endif

            @if($k9=="Delivered" || $k6=="Delivered")


            <div class="col-sm-4"  style="margin-top: 25px;">
                <label><b style="color: #333;">Payment Status</b></label>
                <select name="cashstatus" >

                    <option>{{$cashstatus}} </option>
                    <option>No Payment</option>
                    <option>Partial Payment</option>
                    <option>Full Payment</option>
                </select>
            </div>




            <div class="col-sm-4" style="margin-top: 25px;" >

                <label><b style="color: #333;">Receipt No.</b></label>
                <input type="text" name="receipt" style="background-color: transparent;" value="{{$Receipt}}">
            </div>

            <div class="col-sm-4" style="margin-top: 25px;">

                <label><b style="color: #333;">Cheque No.</b></label>
                <input type="text" name="cheque" style="background-color: transparent;" value="{{$Cheque}}">
            </div>

            <div class="col-sm-4" style="margin-top: 25px;">

                <label><b style="color: #333;">Amount Paid</b></label>
                <input type="text" name="apaid" style="background-color: transparent;" value="{{$apaid}}">
            </div>



            @endif


            <div>
                &emsp; <button style="float:right; margin-top: 11px;" type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>






        <!-- <p style="    margin-top: -33px;
        float: right;"> <b> DUE DATE &nbsp; &nbsp; &nbsp; &nbsp;: </b>&nbsp;&nbsp;24-06-2017</p> -->
        <div class="productdetails1">
            <table class="table table-hover" style="    margin-top: 37px;">
                <thead>
                    <tr>
                        <th>SNo.</th>
                        <th>Product Name</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)

                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$k[$cnt1]}}</td>
                        <td>{{$k5}}</td>
                        <?php if($k5=="Rent")
                        {
                            ?>
                            <td>{{$k7[$cnt1]}}</td>
                            <td>{{$k15[$cnt1]}}</td>
                            <td>{{$k15[$cnt1]*$k7[$cnt1]}}</td>

                            <?php
                        }
                        else
                        {
                            ?>
                            <td>{{$k7[$cnt1]}}</td>
                            <td>{{$k4[$cnt1]}}</td>
                            <td>{{$k4[$cnt1]*$k7[$cnt1]}}</td>
                            <?php
                        }
                        ?>
                    </tr>
                    @endfor

                    <tr style="padding-top: 10px;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="background: white;"><b> TOTAL </b></td>


                        <td style="background: white"> &#x20b9; {{$FinalAmount}} </td>

                    </tr>

                    <tr style="padding-top: 10px;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="background: white;"><b> Discounted Price </b></td>


                        <td style="background: white"> &#x20b9; {{$pdiscount}} </td>

                    </tr>

                </tbody>
            </table>
        </div>

    </div>


    <div class="col-sm-6">

        <h3 style="    padding-top: 22px;">Product Details</h3>

        @if($desig=="Product Manager")
        <a href="{{route('product.edit',$productid)}}" class="btn btn-success">Edit Products</a>
        @endif


        <div class="productdetails">

            @for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            <h4> Product {{++$i}}</h4>
            <p> <b>SKU ID &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;:&emsp; </b>{{$k0[$cnt1]}}</p>
            <p> <b>Product Name &emsp;&emsp;&emsp;&emsp;&nbsp;&emsp; :&emsp; </b>{{$k[$cnt1]}}</p>
            <p> <b>Demo Required &emsp;&emsp;&emsp;&emsp;&emsp;:&emsp; </b>{{$k1}}</p>
            <p> <b>Availability Status &emsp;&emsp;&emsp;&emsp;:&emsp; </b> {{$k2[$cnt1]}}</p>
            <p> <b>Availability Address &emsp;&emsp;&emsp;:&emsp; </b> {{$k3[$cnt1]}}</p>
            <p> <b>Type &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :&emsp; </b> {{$k5}}</p>
            <p> <b>Quantity &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &nbsp;:&emsp; </b>{{$k7[$cnt1]}}</p>

            <?php if($k5=="Rent")
            {?>

                <p> <b>Rental Price&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;:&emsp; </b> {{$k15[$cnt1]}}</p>
                <p> <b>Order Status &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; :&emsp; </b> {{$k9}}</p>


                <p> <b>Mode of Payment &emsp;&emsp;&emsp;&emsp;:&emsp; </b> {{$k8}}</p>
                <p> <b>Advance Amount &emsp;&emsp;&emsp;&emsp; :&emsp; </b> {{$k11[$cnt1]}}</p>
                <p> <b>Start Date &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;:&emsp; </b>{{$k12[$cnt1]}}</p>
                <p> <b>End Date &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :&emsp; </b>{{$k13[$cnt1]}}</p>
                <p> <b>Delivery Date&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :&emsp; </b>{{$deliverydate}}</p>

                <?php
            }
            else
            {?>
                <p> <b>Selling Price &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; :&emsp; </b> {{$k4[$cnt1]}}</p>
                <p> <b> Order Status &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :&emsp; </b> {{$k6}}</p>

                <p> <b>Mode of Payment&emsp;&emsp;&emsp;&emsp;:&emsp; </b>{{$k10}}</p>
                <p> <b>Delivery Date&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :&emsp; </b>{{$deliverydate}}</p>


            <?php }?>


            <!-- <select id="Status" >
            <option value="">Value 1</option>
            <option value="">Value 2</option>
            <option value=" ">Value 3</option>
        </select> -->

        <hr>
        @endfor

    </div>




</div>
</div>
</div>

</body>
</html>
@endsection
