@extends('layouts.customercare')
@section('content')

<html lang="en">
<head>

 <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <style type="text/css">
  button[disabled], html input[disabled] {
    background: white;
    cursor: default;
  }
  </style>


  <script>
  $(document).ready(function(){
 $(function() {
    $('#country').on('change', function() {
      $('#phone1').val($(this).val());
    });



    var country = $('#country').val();
    $('#phone1').val(country);

    //  $('#sameaspermanentadress').change(function () {

    //         console.log("checked");
    // });
    $('#sameaspermanentadress').change(function () {

      if (this.checked) {
        console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#PAddress1').val(Addresd1);
        $("#PAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#PAddress2').val(Addresd2);
        $("#PAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#PCity').val(City);
        $("#PCity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#PDistrict').val(District);
        $("#PDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#PState').val(State);
        $("#PState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#PPinCode').val(Pincode);
        $("#PPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        console.log("unchecked");

        // Adrees1
        $('#PAddress1').val("");
        $("#PAddress1").prop('disabled', false);

        //Adress 2
        $('#PAddress2').val("");
        $("#PAddress2").prop('disabled', false);

        //City
        $('#PCity').val("");
        $("#PCity").prop('disabled', false);

        //District
        $('#PDistrict').val("");
        $("#PDistrict").prop('disabled', false);

        //State
        $('#PState').val("");
        $("#PState").prop('disabled', false);


        //Pincode
        $('#PPinCode').val("");
        $("#PPinCode").prop('disabled', false);

      }
    });





    $('#Esameaspermananentaddress').change(function () {

      if (this.checked) {
        // console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#EAddress1').val(Addresd1);
        $("#EAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#EAddress2').val(Addresd2);
        $("#EAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#ECity').val(City);
        $("#ECity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#EDistrict').val(District);
        $("#EDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#EState').val(State);
        $("#EState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#EPinCode').val(Pincode);
        $("#EPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        // console.log("unchecked");

        // Adrees1
        $('#EAddress1').val("");
        $("#EAddress1").prop('disabled', false);

        //Adress 2
        $('#EAddress2').val("");
        $("#EAddress2").prop('disabled', false);

        //City
        $('#ECity').val("");
        $("#ECity").prop('disabled', false);

        //District
        $('#EDistrict').val("");
        $("#EDistrict").prop('disabled', false);

        //State
        $('#EState').val("");
        $("#EState").prop('disabled', false);


        //Pincode
        $('#EPinCode').val("");
        $("#EPinCode").prop('disabled', false);

      }
    });


    $("#EmergencyContact").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#EmergencyContact').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

        // console.log("Wrong Email");
        $("#EmergencyContact").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#EmergencyContact").css("border-bottom", "1px solid green");
      }

    });


    $("#EmergencyContact").blur(function(){
      var value = $('#EmergencyContact').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(value) == false)
      {

        // console.log("Wrong Email");
        $("#EmergencyContact").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
      }

      if(value == "")
      {
        $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
      }
    });



    //Phone Number
    $("#phone").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#phone').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

}
});
    // $("#phone").blur(function(){
    //   var value = $('#phone').val();

    //   var reg = /^[\+?\d[\d -]{8,12}\d$/;

    //       if (reg.test(value) == false)
    //     {

    //           // console.log("Wrong Email");
    //           $("#phone").css("border-bottom", "1px solid red");
    //            $("#submitt").prop('disabled', true);
    //     }
    //      else
    //     {
    //       // console.log("Correct Email");
    //        $("#phone").css("border-bottom", "1px solid #484e51");
    //         $("#submitt").prop('disabled', false);
    //     }

    //     if(value == "")
    //     {
    //         $("#phone").css("border-bottom", "1px solid #484e51");
    //          $("#submitt").prop('disabled', false);
    //     }
    // });


$("#phone").blur(function(){
      var value = $('#phone').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;
      var numwithcountrycode= /^\+[1-9]{1}[0-9]{3,14}$/;
      if (numwithcountrycode.test(value) == true)
         {

             $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
         }else
         {

                  if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#phone").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }


         }
        //   if (reg.test(value) == false)
        // {

        //       // console.log("Wrong Email");
        //       $("#phone").css("border-bottom", "1px solid red");
        //        $("#submitt").prop('disabled', true);
        // }
        //  else
        // {
        //   // console.log("Correct Email");
        //    $("#phone").css("border-bottom", "1px solid #484e51");
        //     $("#submitt").prop('disabled', false);
        // }

        // if(value == "")
        // {
        //     $("#phone").css("border-bottom", "1px solid #484e51");
        //      $("#submitt").prop('disabled', false);
        // }
    });
    // Pincode Validaion in Addres
  $("#PinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PinCode").blur(function(){
            var value = $('#PinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Present Addres
  $("#PPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PPinCode").blur(function(){
            var value = $('#PPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Emergency Addres
  $("#EPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#EEPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#EPinCode").blur(function(){
            var value = $('#EPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#EPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });


    // Client Email Id Validation Start
    $("#clientemail").keyup(function(){
      var value = $('#clientemail').val();

      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);

        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
        }

      if (reg.test(value) == false)
      {

        // console.log("Wrong Email");
        $("#clientemail").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#clientemail").css("border-bottom", "1px solid green");
      }


    });

    $("#clientemail").blur(function(){
            var value = $('#clientemail').val();

         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
        }

          if(value == "")
        {
            $("#clientemail").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }
    });

    // Cleint ValidationEmail Ends




  });

    var value=$('#reference').val();
    if(value == "Other")
    {
      $("#source").show();
    }else
    {
      $("#source").hide();
    }



    $("#reference").change(function(){
      var value= $("#reference").val();
      if(value == "Other")
      {
        $("#source").show();
      }else
      {
        $("#source").hide();
      }
    });

 $("#ProductName").blur(function(){
     var ProductName =$('#ProductName').val();
     $.get("{{ URL::to('productname') }}", { productname :ProductName},function(data){
              $('#SKUid').val(data);
          });
});
// Drop down
var servicetype = $('#servicetype').val();

if(servicetype == "")
{
      $("#branch").hide();
} else
{
  $("#branchalert").hide();
}


var branch = $('#branch').val();

if((servicetype == "") && (branch == ""))
{


  $("#assignedto").hide();

} else
{
  $("#assignedtoalert").hide();
}




$("#servicetype").change(function(){

    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
    if(servicetype == "")
    {
        $("#branchalert").show();
        $("#branch").hide();
    }else if(branch == "")
    {
      $("#branchalert").hide();
      $("#branch").show();

    } else
    {
      $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });
    }
});



$("#branch").change(function(){
    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
  if(branch == "")
  {
      $("#assignedto").hide();
      $("#assignedtoalert").show();
      $("#resrult").hide();
  }else
  {


       // $.get("test.php", { name:"Donald", town:"Ducktown" });
       $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });


      $("#assignedto").hide();
      $("#assignedtoalert").hide();
      $("#resrult").show();
  }




});


$("#clinetidsubmit").click(function(){
     var clientid =$('#clientid').val();
     // alert(clientid);
     $.get("{{ URL::to('clientdetails') }}", { clientid1 :clientid},function(data){
                $('#result').html(data);
          });
    });

var count =1;
$("#addmoreinputfiled").click(function(){

     count=count + 1;
    $("#value").val(count);
         $.get("{{ URL::to('inputfileds') }}", { noofinputfiledsadded :count},function(data){
                $('#moreinput').append(data+"<br/>");
          });
    });

var countPharmacyDetails=1;
$("#addmoreinputfiledPharmacyDetails").click(function(){

     countPharmacyDetails = countPharmacyDetails + 1;

     $("#nooffiledsPharmacyDetails").val(countPharmacyDetails);
         $.get("{{ URL::to('inputfiledsPharmacyDetails') }}", { countPharmacyDetails :countPharmacyDetails},function(data){
                $('#moreinputPharmacyDetails').append(data+"<br/>");
          });
    });
  });


  $(document).ready(function(){

  var value=$('#Type').val();
     if(value == "Sell")
    {
      $("#sell").show();
      $("#rent").hide();
    }else
    {
    if(value == "")
    {
      $("#sell").hide();
      $("#rent").hide();

    }else
    {
      $("#sell").hide();
      $("#rent").show();
    }
  }



    $("#Type").change(function(){
    var value= $("#Type").val();
    if(value == "Sell")
    {
     $("#sell").show();
      $("#rent").hide();
    }else
    {
    if(value == "")
    {
      $("#sell").hide();
      $("#rent").hide();

    }else
    {
      $("#sell").hide();
      $("#rent").show();
    }
  }
});
});
  </script>


  </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>

.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}


.switch {
  position: relative;
  display: inline-block;
  width: 30px;
  height: 17px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .2s;
  transition: .2s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


</head>
<body>

  <!-- header -->
  <!-- <div class="navbar navbar-default " style="    background-color: white;
  border-color: #e7e7e7;">
  <div class="container">

  <div class="navbar-header">
  <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 8px;">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>

<a class="navbar-brand" rel="home" href="/admin/vertical" title="Buy Sell Rent Everyting">
<img class="imgg"
src=" /img/healthheal_logo.png ">
</a>
</div>

<div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
<ul class="nav navbar-nav navbar-right">
<li ><a href="/admin/vertical" style="font-size: 15px;">Home</a></li>


</ul>

</div>

</div>
</div> -->

<!-- End of header -->
<!-- title -->
<div class="container">
  <div class="row">
    <div class="col-sm-12" id="title">
      <h2> {{substr(Route::currentRouteName(),20)}} Product Lead Registration </h2>
    </div>
  </div>

</div>

<!-- title Ends -->

<form class="form-horizontal" action="/product?type=existing" method="POST">
  {{csrf_field()}}

  @section('editMethod')
  @show

  <div class="container" id="main" style="    margin-top: 34px;">

    <div class="row" id="maintitle" style="    margin-left: -4px;
    width: 102%;" >



<!-- Product Deatials -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ProductDetails"  >Product Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;     ">

  <div class="row collapse" id="ProductDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px;">
     <div class="col-sm-3" id="firstrow">
     <label>SKU ID</label><input type="text"  rows="5" name="SKUid" id="SKUid" value="@yield('editSKUid')">
</div>
    <div class="col-sm-3" id="firstrow">

      <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName" list="Products" value="@yield('editProductName')">
      <datalist id="Products">

@foreach($products as $products)
 <option value="{{ $products->Name}}" label="&#x20B9;{{ $products->Price}}">
@endforeach
  </datalist>
    </div>

    <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

      <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>
      <select name="Type" id="Type" >
        <option value="@yield('editType')">@yield('editType')</option>
        <option value="Sell">Sell</option>
        <option value="Rent">Rent</option>
      </select>

    </div>
    <div class="col-sm-3" id="firstrow">

      <label>Demo Required</label>

      <select name="DemoRequired" id="DemoRequired">
        <option value="@yield('editDemoRequired')">@yield('editDemoRequired')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>

    </div>
<div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="@yield('editAvailabilityStatus')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="@yield('editAvailabilityAddress')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Quantity</label><input type="text"  rows="5" name="Quantity" id="Quantity" value="@yield('editQuantity')">
    </div>

    <div class="col-sm-12"></div>

 <div id="sell">
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="@yield('editSellingPrice')">
    </div>




    <div class="col-sm-3" style="margin-top: 26px;">
     <label>Mode of Payment</label>
     <select name="ModeofPayment" id="ModeofPayment">
        <option value="@yield('editModeofPayment')">@yield('editModeofPayment')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div>
   <!--  <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="OrderStatus" id="OrderStatus">
        <option value="@yield('editOrderStatus')">@yield('editOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->

   </div>

<div id="rent">
    <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

      <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="@yield('editRentalPrice')">
    </div>



    <div class="col-sm-3" style="margin-top: 26px;">
     <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt" id="AdvanceAmt" value="@yield('editAdvanceAmt')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>Start Date</label><input type="date"  rows="5" name="StartDate" id="StartDate" value="@yield('editStartDate')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>End Date</label><input type="date"  rows="5" name="EndDate" id="EndDate" value="@yield('editEndDate')">
    </div>
    <div class="col-sm-12"></div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>Mode of Payment</label>
     <select name="ModeofPaymentrent" id="ModeofPaymentrent">
        <option value="@yield('editModeofPaymentrent')">@yield('editModeofPaymentrent')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cheque">Cheque</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>



      </select>
    </div>
    <!--  <div class="col-sm-3" style="margin-top: 26px;">

      <label>Order Status</label>
		<select name="OrderStatusrent" id="OrderStatusrent">
        <option value="@yield('editOrderStatusrent')">@yield('editOrderStatusrent')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
      </select>







    </div>
 -->
     <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Overdue Amount</label><input type="text"  rows="5" name="OverdueAmt" id="OverdueAmt" value="@yield('editOverdueAmt')">
    </div>
</div>

 <input type="hidden" name="nooffileds" id="value">
    <!-- Add inut flieds -->

<div class="col-sm-12">
  <div id="moreinput"></div>
</div>

<div class="col-sm-12" style="text-align: right;">
  <button type="button"  id="addmoreinputfiled" style="outline: none; background: white;
    border: 0px;">
  <img src="/img/Add-32.png" class="img-responsive" alt="add" style="    width: 45px;"></button>
</div>


  </div>
</div>





<!-- Pharmacy Details -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PharmacyDetails"  >Pharmacy Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;     ">

  <div class="row collapse" id="PharmacyDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px;">

    <div class="col-sm-3" id="firstrow">

      <label>Medicine Name</label><input type="text"  rows="5" name="MedName" id="MedName" value="@yield('editMedName')">
    </div>
     <div class="col-sm-3" id="firstrow">

      <label>Strength</label><input type="text"  rows="5" name="Strength" id="Strength" value="@yield('editStrength')">
    </div>

    <div class="col-sm-3" id="firstrow" >

      <label>Type </label>
      <select name="MedType" id="MedType" >
        <option value="@yield('editMedType')">@yield('editMedType')</option>
        <option value="Tablet">Tablet</option>
        <option value="Capsule">Capsule</option>
        <option value="Injection">Injection</option>
        <option value="Ointment">Ointment</option>
        <option value="Scrub">Scrub</option>
         <option value="Cream">Cream</option>
         <option value="Powder">Powder</option>
         <option value="Lotion">Lotion</option>
         <option value="Syrum">Syrum</option>
         <option value="Drops">Drops</option>
         <option value="Gel">Gel</option>
         <option value="Expectorant">Expectorant</option>
         <option value="Respules">Respules</option>
         <option value="Handrub">Handrub</option>
         <option value="Syrup">Syrup</option>


      </select>

    </div>
    <div class="col-sm-3" id="firstrow">
     <label>Quantity</label><input type="text"  rows="5" name="pQuantity" id="pQuantity" value="@yield('editpQuantity')">
    </div>



    <div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="pAvailabilityStatus" id="pAvailabilityStatus" value="@yield('editpAvailabilityStatus')">
    </div>




    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice" id="pSellingPrice" value="@yield('editpSellingPrice')">
    </div>

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="pOrderStatus" id="pOrderStatus">
        <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->
    <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Mode of payment</label>
      <select name="PModeofpayment" id="PModeofpayment">
        <option value="@yield('editPModeofpayment')">@yield('editPModeofpayment')</option>
        <option value="Swipe">Swipe</option>
        <option value="Cash">Cash</option>
        <option value="NEFT">NEFT</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div>




<input type="hidden" name="nooffiledsPharmacyDetails" id="nooffiledsPharmacyDetails" value="">
    <!-- Add inut flieds -->

<div class="col-sm-12">
  <div id="moreinputPharmacyDetails"></div>
</div>

<div class="col-sm-12" style="text-align: right;">
  <button type="button"  id="addmoreinputfiledPharmacyDetails" style="outline: none; background: white;
    border: 0px;">
  <img src="/img/Add-32.png" class="img-responsive" alt="add" style="    width: 45px;"></button>
</div>
  </div>

</div>

  </div>

</div>


<input type="hidden" value="{{$leadid}}" name="existing">
<input type="hidden" value="{{$leadid}}" name="addproductwithservice">
<input type="hidden" value="{{ Auth::guard('admin')->user()->name }}" name="loginname">

    <div class="container" style="    margin-top: 32px;">
          <div class="row">
                <div class="col-sm-12">
                  <center>  <button type="submit" class="btn btn-default" id="submitt" >Submit</button></center>
                </div>
          </div>
    </div>
  </div>
</div>
@include('partial.errors')
</form>

</body>

@endsection
