<!-- {{$countPharmacyDetails}} -->
<div class="col-sm-12">
<h3> Pharmacy  {{$countPharmacyDetails}} Details </h3>
</div>
<div class="col-sm-3" id="firstrow">

      <label>Medicine Name</label><input type="text"  rows="5" name="MedName{{$countPharmacyDetails}}" id="MedName{{$countPharmacyDetails}}" value="@yield('editMedName')">
    </div>
     <div class="col-sm-3" id="firstrow">

      <label>Strength</label><input type="text"  rows="5" name="Strength{{$countPharmacyDetails}}" id="Strength{{$countPharmacyDetails}}" value="@yield('editStrength')">
    </div>

    <div class="col-sm-3" id="firstrow" >

      <label>Type </label>
      <select name="MedType{{$countPharmacyDetails}}" id="MedType{{$countPharmacyDetails}}" >
        <option value="@yield('editMedType')">@yield('editMedType')</option>
        <option value="Tablet">Tablet</option>
        <option value="Capsule">Capsule</option>
        <option value="Injection">Injection</option>
        <option value="Ointment">Ointment</option>
        <option value="Scrub">Scrub</option>
         <option value="Cream">Cream</option
         <option value="Powder">Powder</option>
         <option value="Lotion">Lotion</option>
         <option value="Syrum">Syrum</option>
         <option value="Drops">Drops</option>
         <option value="Gel">Gel</option>
         <option value="Expectorant">Expectorant</option>
         <option value="Respules">Respules</option>
         <option value="Handrub">Handrub</option>
         <option value="Syrup">Syrup</option>


      </select>

    </div>
    <div class="col-sm-3" id="firstrow">
     <label>Quantity</label><input type="text"  rows="5" name="PQuantity{{$countPharmacyDetails}}" id="PQuantity{{$countPharmacyDetails}}" value="@yield('editPQuantity')">
    </div>



    <div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="PAvailabilityStatus{{$countPharmacyDetails}}" id="PAvailabilityStatus{{$countPharmacyDetails}}" value="@yield('editPAvailabilityStatus')">
    </div>




    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="pSellingPrice{{$countPharmacyDetails}}" id="pSellingPrice{{$countPharmacyDetails}}" value="@yield('editpSellingPrice')">
    </div>

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Mode of payment</label>
      <select name="PModeofpayment{{$countPharmacyDetails}}" id="PModeofpayment{{$countPharmacyDetails}}">
        <option value="@yield('editPModeofpayment')">@yield('editPModeofpayment')</option>
        <option value="Cash">Cash</option>
        <option value="COD">COD</option>
        <option value="Cheque">Cheque</option>
      </select>
    </div> -->

    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="POrderStatus{{$countPharmacyDetails}}" id="POrderStatus{{$countPharmacyDetails}}">
        <option value="@yield('editpOrderStatus')">@yield('editpOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->