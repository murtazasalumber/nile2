<!-- This page acts as a version of the View Leads page -->
<script>
$(document).ready(function(){
  $('.footable').footable();



});
</script>
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Lead ID</b></th>
      <th  data-hide="phone,tablet"><b> Created At </b></th>
      <th  data-hide="phone,tablet"><b> Assigned To </b></th>
      <th data-hide="phone,tablet" ><b> Customer Name </b></th>
      <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
      <th data-hide="phone,tablet"><b>City </b></th>
      <th data-hide="phone,tablet"><b>Source</b></th>
      <th data-hide="phone,tablet" ><b>Service Type</b></th>
      <th data-hide="phone,tablet" ><b>Lead Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td>{{$lead->id}}</td>
      <td>{{$lead->created_at}}</td>
      <td >{{$lead->AssignedTo}}</td>
      <td style="color:red;">{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
      <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
      <td>{{$lead->Branch}}</td>
      <td>{{$lead->Source}}</td>
      <td>{{$lead->ServiceType}}</td>
      <td>{{$lead->ServiceStatus}}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "AssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Lead ID</b></th>
      <th  data-hide="phone,tablet"><b> Created At </b></th>
      <th  data-hide="phone,tablet"><b> Assigned To </b></th>
      <th data-hide="phone,tablet" ><b> Customer Name </b></th>
      <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
      <th data-hide="phone,tablet"><b>City </b></th>
      <th data-hide="phone,tablet"><b>Source</b></th>
      <th data-hide="phone,tablet" ><b>Service Type</b></th>
      <th data-hide="phone,tablet" ><b>Lead Status</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td>{{$lead->id}}</td>
      <td>{{$lead->created_at}}</td>
      <td style="color:red;" >{{$lead->AssignedTo}}</td>
      <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
      <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
      <td>{{$lead->Branch}}</td>
      <td>{{$lead->Source}}</td>
      <td>{{$lead->ServiceType}}</td>
      <td>{{$lead->ServiceStatus}}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "Source")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Lead ID</b></th>
      <th  data-hide="phone,tablet"><b> Created At </b></th>
      <th  data-hide="phone,tablet"><b> Assigned To </b></th>
      <th data-hide="phone,tablet" ><b> Customer Name </b></th>
      <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
      <th data-hide="phone,tablet"><b>City </b></th>
      <th data-hide="phone,tablet"><b>Source</b></th>
      <th data-hide="phone,tablet" ><b>Service Type</b></th>
      <th data-hide="phone,tablet" ><b>Lead Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td>{{$lead->id}}</td>
      <td>{{$lead->created_at}}</td>
      <td  >{{$lead->AssignedTo}}</td>
      <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
      <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
      <td>{{$lead->Branch}}</td>
      <td style="color:red;">{{$lead->Source}}</td>
      <td>{{$lead->ServiceType}}</td>
      <td>{{$lead->ServiceStatus}}</td>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "ServiceType")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>

      <th><b> Lead ID</b></th>
      <th  data-hide="phone,tablet"><b> Created At </b></th>
      <th  data-hide="phone,tablet"><b> Assigned To </b></th>
      <th data-hide="phone,tablet" ><b> Customer Name </b></th>
      <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
      <th data-hide="phone,tablet"><b>City </b></th>
      <th data-hide="phone,tablet"><b>Source</b></th>
      <th data-hide="phone,tablet" ><b>Service Type</b></th>
      <th data-hide="phone,tablet" ><b>Lead Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td>{{$lead->id}}</td>
      <td>{{$lead->created_at}}</td>
      <td  >{{$lead->AssignedTo}}</td>
      <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
      <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
      <td>{{$lead->Branch}}</td>
      <td >{{$lead->Source}}</td>
      <td style="color:red;">{{$lead->ServiceType}}</td>
      <td>{{$lead->ServiceStatus}}</td>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "Branch")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>

      <th><b> Lead ID</b></th>
      <th  data-hide="phone,tablet"><b> Created At </b></th>
      <th  data-hide="phone,tablet"><b> Assigned To </b></th>
      <th data-hide="phone,tablet" ><b> Customer Name </b></th>
      <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
      <th data-hide="phone,tablet"><b>City </b></th>
      <th data-hide="phone,tablet"><b>Source</b></th>
      <th data-hide="phone,tablet" ><b>Service Type</b></th>
      <th data-hide="phone,tablet" ><b>Lead Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
      <td>{{$lead->id}}</td>
      <td>{{$lead->created_at}}</td>
      <td  >{{$lead->AssignedTo}}</td>
      <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
      <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
      <td style="color:red;">{{$lead->Branch}}</td>
      <td >{{$lead->Source}}</td>
      <td >{{$lead->ServiceType}}</td>
      <td>{{$lead->ServiceStatus}}</td>
    </tr>
    @endforeach
  </tbody>
</table>



      @elseif ($filter1 === "ServiceStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td>{{$lead->id}}</td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <td style="color:red;">{{$lead->ServiceStatus}}</td>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
      @elseif ($filter1 === "leads.id")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td style="color:red;">{{$lead->id}}</td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <td >{{$lead->ServiceStatus}}</td>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
               @elseif ($filter1 === "MobileNumber")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td>{{$lead->id}}</td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <td >{{$lead->ServiceStatus}}</td>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
               @elseif ($filter1 === "Alternatenumber")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td>{{$lead->id}}</td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}}</td>
                                    <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <td >{{$lead->ServiceStatus}}</td>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

                              @elseif ($filter1 === "AssignedTo")
                         <table class="table footable" style="font-family: Raleway,sans-serif;">
                                               <thead>
                                                  <tr>

                                                               <th><b> Lead ID</b></th>
                                                               <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                               <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                               <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                               <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                               <th data-hide="phone,tablet"><b>City </b></th>
                                                               <th data-hide="phone,tablet"><b>Source</b></th>
                                                               <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                               <th data-hide="phone,tablet" ><b>Lead Status</b></th>


                                                           </tr>
                                               </thead>
                                               <tbody>
                                               @foreach($data1 as $lead)
                                                 <tr>
                                                 <?php
               if(session()->has('name'))
               {
                 $name=session()->get('name');
               }else
               {
               if(isset($_GET['name'])){
                  $name=$_GET['name'];
               }else{
                  $name=NULL;
               }
               }
               ?>
                                                   <td>{{$lead->id}}</td>
                                                   <td>{{$lead->created_at}}</td>
                                                   <td  style="color:red;" >{{$lead->AssignedTo}}</td>
                                                   <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                                   <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                                   <td >{{$lead->Branch}}</td>
                                                   <td >{{$lead->Source}}</td>
                                                   <td >{{$lead->ServiceType}}</td>
                                                   <td >{{$lead->ServiceStatus}}</td>




                                                 </tr>
                                               @endforeach
                                               </tbody>
                                             </table>


      @endif


<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
