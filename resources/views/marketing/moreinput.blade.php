<!-- {{$noofinputfiledsadded}} -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var count = {{$noofinputfiledsadded}};
    var type ="Type";
    var Type= type + count;

    var sell1="sell";
    var sell=sell1+count;

    var rent1="rent";
    var rent=rent1+count;

    var value=$('#'+Type).val();
     if(value == "Sell")
    {
      $("#"+sell).show();
      $("#"+rent).hide();
    }else
    {
    if(value == "")
    {
      $("#"+sell).hide();
      $("#"+rent).hide();

    }else
    {
      $("#"+sell).hide();
      $("#"+rent).show();
    }
  }


$("#"+Type).change(function(){
    var value= $("#"+Type).val();
    if(value == "Sell")
    {
     $("#"+sell).show();
      $("#"+rent).hide();
    }else
    {
    if(value == "")
    {
      $("#"+sell).hide();
      $("#"+rent).hide();

    }else
    {
      $("#"+sell).hide();
      $("#"+rent).show();
    }
  } 
});

   
});
</script>
<div class="col-sm-12">
<h3> Product {{$noofinputfiledsadded}} Details </h3>
</div>
<div class="col-sm-3" id="firstrow">
     <label>SKU ID</label><input type="text"  rows="5" name="SKUid{{$noofinputfiledsadded}}" id="SKUid{{$noofinputfiledsadded}}" value="@yield('editSKUid')">
</div>
    <div class="col-sm-3" id="firstrow">

      <label>Product Name</label><input type="text"  rows="5" name="ProductName{{$noofinputfiledsadded}}" id="ProductName{{$noofinputfiledsadded}}" value="@yield('editProductName')">
    </div>

    <div class="col-sm-3" id="firstrow" style="margin-top: 4px;">

      <label>Type <span style="color:Red; font-size: 20px;">*</span> </label>
      <select name="Type{{$noofinputfiledsadded}}" id="Type{{$noofinputfiledsadded}}" >
        <option value="@yield('editType')">@yield('editType')</option>
        <option value="Sell">Sell</option>
        <option value="Rent">Rent</option>
      </select>

    </div>
    <div class="col-sm-3" id="firstrow">

      <label>Demo Required</label>

      <select name="DemoRequired{{$noofinputfiledsadded}}" id="DemoRequired{{$noofinputfiledsadded}}">
        <option value="@yield('editDemoRequired')">@yield('editDemoRequired')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>

    </div>
<div class="col-sm-12"></div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus{{$noofinputfiledsadded}}" id="AvailabilityStatus{{$noofinputfiledsadded}}" value="@yield('editAvailabilityStatus')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px;">

      <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress{{$noofinputfiledsadded}}" id="AvailabilityAddress{{$noofinputfiledsadded}}" value="@yield('editAvailabilityAddress')">
    </div>
    <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Quantity</label><input type="text"  rows="5" name="Quantity{{$noofinputfiledsadded}}" id="Quantity{{$noofinputfiledsadded}}" value="@yield('editQuantity')">
    </div>

    <div class="col-sm-12"></div>

 <div id="sell{{$noofinputfiledsadded}}">
    <div class="col-sm-3" style="margin-top: 26px;margin-bottom: 10px;">

      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice{{$noofinputfiledsadded}}" id="SellingPrice{{$noofinputfiledsadded}}" value="@yield('editSellingPrice')">
    </div>



    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Mode of Payment</label>
     <select name="ModeofPayment{{$noofinputfiledsadded}}" id="ModeofPayment{{$noofinputfiledsadded}}">
        <option value="@yield('editModeofPayment')">@yield('editModeofPayment')</option>
        <option value="Cash">Cash</option>
        <option value="Cheque">Cheque</option>
        <option value="COD">COD</option>
      </select>
    </div> -->
    <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">

      <label>Order Status</label>
      <select name="OrderStatus{{$noofinputfiledsadded}}" id="OrderStatus{{$noofinputfiledsadded}}">
        <option value="@yield('editOrderStatus')">@yield('editOrderStatus')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
        <option value="Order Return">Order Return</option>
      </select>
    </div> -->

   </div>

<div id="rent{{$noofinputfiledsadded}}">
    <div class="col-sm-3" style="margin-top: 26px;    margin-bottom: 13px;">

      <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice{{$noofinputfiledsadded}}" id="RentalPrice{{$noofinputfiledsadded}}" value="@yield('editRentalPrice')">
    </div>



    <div class="col-sm-3" style="margin-top: 26px;">
     <label>Advance Amount</label><input type="text"  rows="5" name="AdvanceAmt{{$noofinputfiledsadded}}" id="AdvanceAmt{{$noofinputfiledsadded}}" value="@yield('editAdvanceAmt')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>Start Date</label><input type="date"  rows="5" name="StartDate{{$noofinputfiledsadded}}" id="StartDate{{$noofinputfiledsadded}}" value="@yield('editStartDate')">
    </div>
     <div class="col-sm-3" style="margin-top: 26px;">
     <label>End Date</label><input type="date"  rows="5" name="EndDate{{$noofinputfiledsadded}}" id="EndDate{{$noofinputfiledsadded}}" value="@yield('editEndDate')">
    </div>
    <div class="col-sm-12"></div>
    <!--  <div class="col-sm-3" style="margin-top: 26px;">
     <label>Mode of Payment</label>
     <select name="ModeofPaymentrent{{$noofinputfiledsadded}}" id="ModeofPaymentrent{{$noofinputfiledsadded}}">
        <option value="@yield('editModeofPaymentrent')">@yield('editModeofPaymentrent')</option>
        <option value="Cash">Cash</option>
        <option value="Cheque">Cheque</option>

      </select>
    </div> -->
<!--      <div class="col-sm-3" style="margin-top: 26px;">

      <label>Order Status</label>
    <select name="OrderStatusrent{{$noofinputfiledsadded}}" id="OrderStatusrent{{$noofinputfiledsadded}}">
        <option value="@yield('editOrderStatusrent')">@yield('editOrderStatusrent')</option>
        <option value="Pending">Pending</option>
        <option value="Processing">Processing</option>
        <option value="Awaiting Pickup">Awaiting Pickup</option>
         <option value="Ready to ship">Ready to ship</option>
         <option value="Out for Delivery">Out for Delivery</option>
        <option value="Delivered">Delivered</option>
        <option value="Cancelled">Cancelled</option>
      </select>







    </div> -->

     <!-- <div class="col-sm-3" style="margin-top: 26px; margin-bottom: 10px;">
     <label>Overdue Amount</label><input type="text"  rows="5" name="OverdueAmt{{$noofinputfiledsadded}}" id="OverdueAmt{{$noofinputfiledsadded}}" value="@yield('editOverdueAmt')">
    </div> -->
</div>