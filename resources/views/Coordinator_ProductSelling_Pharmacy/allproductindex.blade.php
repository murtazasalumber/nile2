
@extends('layouts.Coordinator_ProductSelling_Pharmacy')

@section('content')


 <script>
$(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
            $("#keyword").keyup(function(){

                var keyword = $('#keyword').val();
                var filter = $('#filter').val();
                var status1=$('#status1').val();
                var name=$('#name').val();


                     $.ajax({
                  type: "GET",
                  url:'jatin' ,
                   data: {'keyword1' : keyword, 'status1':status1, 'name':name, 'filter1' : filter,'_token':$('input[name=_token]').val() },
                  success: function(data){
                       $('#result').html(data);
                       // alert(data);
                  }
              });
              });

             $("#filter").change(function(){

              $('#keyword').val("");
             });

});
</script>
<style type="text/css">
.footer
{
  text-align: center;
  margin-top: 34%;
}
  .imgg
{

    margin-top: -10px;
    margin-left: -98px;



    max-width: 167px;

}
body::-webkit-scrollbar
{
  display: none;
}
.footer
{
  display: none;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{

      outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
input
{
  border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
input:focus
{
  border-bottom:1px solid #00cccc;
}
select:focus
{
  border-bottom:1px solid #00cccc;
}
select
{
  border-bottom-color:#484e51;
      border-top: 0px;
    width: 100%;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}
.footer
{
  text-align: center;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
@media (max-width: 1200px)
{
 #second
{
      margin-bottom: 5%;
    margin-left: 29%;
}
#productl
{
   margin-left: 29%;
   margin-bottom: 10%;
}
}
</style>

<div class="container">
            <div class="row">

               <div class="col-sm-12 " >
                  <center><h2 style="color: #333;"> Leads</h2></center>
               </div>
            </div>

    </div>

    <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>

                        <option value="leads.id">Lead ID</option>
                        <option value="fName">Customer Name</option>
                        <!-- <option value="AssignedTo">Assigned To</option> -->
                        <option value="orderid">Order ID</option>
                        <!-- <option value="type">Product Type</option> -->
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="OrderStatus">Status</option>

                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
                      <input type="hidden"  id="name" value="{{ Auth::guard('admin')->user()->name }}">
          </div>
      </div>
</div>


<div class="container">
    <div class="row">
          <div class="col-sm-12" style="text-align: center;margin-top: 15px">


           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" data-toggle="tooltip" title="Create Product Lead" data-placement="bottom"> <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"></a>

              <?php

    if(isset($_GET['status']))
    {
    $check = $_GET['status'];
    }
    else
    {
        $check = session()->get('status');
    }
    ?>

    <input type="hidden" name="" id="status1" value="<?php echo $check; ?>">



     @if($check=="All")
     <a href="/CoordinatorproductSellingpharmacydownloadall3?status=<?php echo $check;    ?>&download=true" data-toggle="tooltip" title="Download" data-placement="bottom"><img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>
     <!-- <input type="text" value="try"> -->
     @else

     <!-- Download for count view leads -->
         <a href="/CoordinatorproductSellingpharmacydownloadall3?status=<?php echo $check;    ?>&download=true" data-toggle="tooltip" title="Download" data-placement="bottom"><img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>

     @endif


          </div>
    </div>

</div>


    <!-- This is the URL generation code for download when the User clicks on the "Download" button on View Leads, or "Individual Count" Leads -->
    <!-- If the status is present in URL, retrieve from there, else retrieve from session -->

    <br>
<table class="table footable" style="font-family: Raleway,sans-serif;">
        <thead>
        <th  data-hide="phone,tablet" style="color: #333;"><b>S.no</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>order id</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>Lead ID</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Product Type</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created At</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created By</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Name</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet"  style="color: #333;"><b>Status</b></th>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->


<?php

if(session()->has('name'))
              {
                $name=session()->get('name');
              }else
              {
              if(isset($_GET['name'])){
                 $name=$_GET['name'];
              }else{
                 $name=NULL;
              }
              }

for($i=0;$i<$count;$i++)
{

 ?>
          <tr>


<?php if($leads1[$i]['MedName']!=NULL)
{
  if($leads1[$i]['SKUid']!=NULL)
  {
    ?>
    <td><?php echo $leads1[$i]['leadid'];?></td>
    <td>Both</td>
<?php
  }
  else
  {
?>
    <td><a href="pharmacyshow?id=<?php echo $leads1[$i]['PharmacyId'];?>&name=<?php echo $name?>&type=Pharmacy"><?php echo $leads1[$i]['id'];?></a></td>

    <td><?php echo $leads1[$i]['orderid'];?></td>
    <td><?php echo $leads1[$i]['leadid'];?></td>

    <td>Pharmacy</td>

      <td><?php echo $leads1[$i]['created_at'];?></td>
    <td><?php echo $leads1[$i]['Prequestcreatedby'];?> </td>
      <td><?php echo $leads1[$i]['fName'];  echo $leads1[$i]['lName'];?></td>
      <td><?php echo $leads1[$i]['MobileNumber'];?></td>
      <td><?php echo $leads1[$i]['POrderStatus'];?></td>
 <?php
  }
}
else
{
?>
    <td><a href="fieldofficerview?id=<?php echo $leads1[$i]['orderid'];?>&name=<?php echo $name?>&type=Product"><?php echo $leads1[$i]['id'];?></a></td>

    <td><?php echo $leads1[$i]['orderid'];?></td>
    <td><?php echo $leads1[$i]['leadid'];?></td>
    <td>Product</td>
    <td><?php echo $leads1[$i]['created_at'];?></td>
      <td><?php echo $leads1[$i]['Requestcreatedby'];?></td>
      <td><?php echo $leads1[$i]['fName'];  echo $leads1[$i]['lName'];?></td>
      <td><?php echo $leads1[$i]['MobileNumber'];?></td>
<?php

  if($leads1[$i]['Type']=="Sell" || $leads1[$i]['Type']==NULL)
  {
?>
      <td><?php echo $leads1[$i]['OrderStatus'];?></td>
<?php
  }
  else
  {
?>
      <td><?php echo $leads1[$i]['OrderStatusrent'];?></td>
<?php
  }

}
}
?>






      </tr>


    </tbody>
  </table>


@endsection
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
