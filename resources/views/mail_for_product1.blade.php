Dear {{$cust_name}},<br><br>
Your order no: {{$orderid}} has been requested.<br><br>
We will send a confirmation when your order is shipped.<br><br>

Please quote this Order Id for further communications with our team.<br><br>

For more information about our Services & Products, please call us at 8880004422 or visit our website: www.healthheal.in <br><br>

Thank you,<br><br>
Health Heal
