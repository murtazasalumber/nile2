<?php 
$value= Session::all();

$value=session()->getId();
  //echo $value;
                                  
                         
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>
  
    <script>
$(document).ready(function(){
  $('.footable').footable();
   $('[data-toggle="tooltip"]').tooltip();
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();

      
          $.ajax({
        type: "GET",
        url:'allleadc' ,
         data: {'keyword1' : keyword,'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
        }
    });
    });
        $("#filter").change(function(){

              $('#keyword').val("");
             });
    
});
</script>
<style>
.footer{
  position: absolute;
  margin-top: 2%;
}
  .imgg
{
   margin-top: -13px;
    margin-left: -70px;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      
      outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
#keyword
{
      margin-left: 26px;
    width: 18%;
}
#filter
{    margin-top: 18px;
      width: 18%;
}


@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}

 @media only screen and (max-width: 1200px) {
    #loo
    {
          margin-left: 16%;
    }
    #second
    {
          margin-left: 22%;
    margin-bottom: 9%;
    } 
    #productl
     {
        margin-bottom: 9%;
          margin-left: 22%;
    }
    #but
    {
          padding-left: 22%;
    }
}

</style>
  </head>
  <body style="font-family: myFirstFont;">
   <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="loo">
            <a class="navbar-brand" rel="home" href="/admin/management" title="Buy Sell Rent Everyting">
                <img class="imgg" 
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>
       
         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right" style="text-align: right;">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
                            
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">
                                
                                   <a href="/admin/management" style="font-family: myFirstFont;"
                                            >
                                            Home
                                        </a>
                                       <!--  <a href="\version">Version Notes</a> -->
                                        <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                       

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                </ul>
                                

                            </li>
                        @endif
                    </ul>
                </div>

           
        </div>

    </div>
</div>
<!-- title -->
    <div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2> Provisional Leads</h2></center>
               </div>
            </div>
      
    </div>

    <!-- title Ends -->


{{csrf_field()}}
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>
     <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">

                         <div class="container" style="    margin-top: 27px;">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-5" id="icons">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;" data-toggle="tooltip" title="Create New Service Lead" data-placement="bottom"></a>
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" > <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;" data-toggle="tooltip" title="Create New Product Lead" data-placement="bottom"></a>



   

            
        </div>
      
    </div>
</div>
<!-- 
  <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
          <div class="col-sm-4">

              <div style="    margin-left: -69px;" id="but" >
                <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="second">
                                          Create Service Lead
                                        </a>&emsp;

                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="productl">
                                          Create Product Lead
                                        </a>
                                        </div>
          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword'>
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">
              </div>
              <div class="col-sm-2" id="buttonforcreate">
                
          </div>

          </div>


  </div>

</div>
 -->

<!-- Search Section -->
  <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
         
              
              <div class="col-sm-3">
            
          </div>
             
          </div>
        
      </div>
    
  </div>


<!-- Search Section Ends-->
<!-- Reslts -->
  <div class="container-fluid" style="margin-top: 50px;" >
    <div class="row" >
      <div class="col-sm-12" id="result">
            <table class="table footable" style="font-family: Raleway,sans-serif;">
          <thead>

              <tr>
                    <th><b> ID</b></th>
                    <th  data-hide="phone,tablet"><b> Created At </b></th>
                    <th  data-hide="phone,tablet"><b> Created By </b></th>
                    <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                    <th data-hide="phone,tablet"><b>City </b></th>
                    <th data-hide="phone,tablet"><b>Requested type</b></th>
                    <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <th data-hide="phone,tablet" ><b>Product Name</b></th>
                    <th data-hide="phone,tablet" ><b>Address</b></th>
                    
                
                </tr>
            </thead>
            <tbody>

  @foreach ($provisionals as $provisional)
<tr>
<?php 
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
  
    <!-- <td><a href="/referalpartner/create?id={{$provisional->id}}&name=<?php echo $name?>">{{$provisional->id}}</a></td> -->
   <td>{{$provisional->id}}</td>
    <td>{{$provisional->created_at}}</td>
    <td>{{$provisional->Reference}}</td>
	<td>{{$provisional->FName}} {{$provisional->LName}}</td>
	<td>{{$provisional->MobileNumber}}</td>
 	<td>{{$provisional->City}}</td>
 	<td>{{$provisional->RequestType}}</td>
 	<td>{{$provisional->ServiceType}}</td> 
  	<td>{{$provisional->ProductName}}</td> 
  	<td>{{$provisional->Address}}</td> 

     </tr>

@endforeach
 </tbody>
</table>
 <div style="text-align: -webkit-center;"> {{$provisionals->links()}} </div>

      </div>

    </div>

</div>
  @extends('layouts.footer')
  </body>
  </html>