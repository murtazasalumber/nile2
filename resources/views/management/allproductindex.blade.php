   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>
@extends('layouts.management')

@section('content')
<script type="text/javascript">
$.noConflict();
jQuery( document ).ready(function( $ ) {
    // Code that uses jQuery's $ can follow here.
    $('.footable').footable();

});
</script>
<script>
$(document).ready(function(){
    
            $("#keyword").keyup(function(){

  var keyword = $('#keyword').val();
      var filter = $('#filter').val();
      var status1=$('#status1').val();
      var name=$('#name').val();

      
           $.ajax({
        type: "GET",
        url:'allleadpa' ,
         data: {'keyword1' : keyword, 'status1':status1, 'name':name, 'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
             // alert(data);

        }
    });
    });
     
     $("#filter").change(function(){

              $('#keyword').val("");
             }); 
});
</script>
<style type="text/css">
@media (max-width: 1200px)
{
 .keyword1
  {
    width: 35%;
}
 #filter
   {
                width: 42%;
    padding-top: 19px;
    margin-bottom: 17px;
    margin-left: -30px;
   }
 }
  .imgg
{

    margin-top: -10px;
    margin-left: -67px;
       
   

    max-width: 167px;

}
.footer
{
  display: none;
}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      
      outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
input
{
  border-right: 0px;  
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px; 
    border-bottom:1px solid #484e51;
    padding-bottom: 0px; 
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
input:focus
{
  border-bottom:1px solid #00cccc;
}
select:focus
{
  border-bottom:1px solid #00cccc;
}
select
{
  border-bottom-color:#484e51;
      border-top: 0px;
    width: 100%;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
.footer
{
  text-align: center;
}
@media (max-width: 1200px)
{
 #second
{
      margin-bottom: 5%;
    margin-left: 29%;
}
#productl
{
   margin-left: 29%;
   margin-bottom: 10%;
}
}
</style>
<div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2> Product Leads</h2></center>
               </div>
            </div>
      
    </div>
     <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>

                         <option value="leads.id">Lead ID</option>
                        <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="orderid">Order ID</option>
                        <!-- <option value="type">Product Type</option> -->
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="OrderStatus">Status</option>
                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>




               <div class="container">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-5" id="icons">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;"></a>
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" > <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"></a>


  <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>

                <a href="/admindownloadall?status=<?php echo $status;    ?>&download=true"> <img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>


               <input type="hidden" name="" id="status1" value="<?php echo $status ?>">
             

            
        </div>
      
    </div>
</div>
<!-- <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
          <div class="col-sm-4" id="buttonforcreate">
                     <div style="    margin-left: -55px;">
                     <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="second">
                                          Create Service Lead
                                        </a>&emsp;

                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="productl">
                                          Create Product Lead
                                        </a>
                </div>
          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                        <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="orderid">Order ID</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword' style="    margin-top: -5px;">
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">
              </div>
              <div class="col-sm-2" id="buttonforcreate" style="    padding-left: 16px;
    margin-top: -8px;"> -->
                <!-- <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="second">
                                          Create Service Lead
                                        </a>&emsp;

                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" >
                                          Create Product Lead
                                        </a> -->
      <!--     </div>

          </div>


  </div>

</div> -->

<div id="result">
<table class="table footable" style="font-family: Raleway,sans-serif;margin-top: 4%;">
        <thead>
        <th  data-hide="phone,tablet"><b>S.no</b></th>
        <th  data-hide="phone,tablet"><b>Order ID</b></th>
        <th  data-hide="phone,tablet"><b>Lead ID</b></th>
        	<th  data-hide="phone,tablet"><b>Product Type</b></th>
        	<th  data-hide="phone,tablet"><b>Created At</b></th>
          <th  data-hide="phone,tablet"><b>Created By</b></th>
          <th  data-hide="phone,tablet"><b>Assigned To</b></th>
          <th data-hide="phone,tablet" ><b>Customer Name</b></th>
          <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet" ><b>Status</b></th>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          @foreach ($leads as $lead)
          <tr>
            <?php
            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
             <input type="hidden"  id="name" value="{{ Auth::guard('admin')->user()->name }}">


@if($lead->MedName!=NULL)
 
	@if($lead->SKUid!=NULL)

	 <td>{{$lead->leadid}}</td>
	{{-- <td>Both</td> --}}

  	@else


	<td><a href="pharmacyshow?id={{$lead->PharmacyId}}&name=<?php echo $name?>">{{$lead->id}}</a></td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
	<td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>

 

	@endif

@else


<td><a href="productshow?id={{$lead->Prodid}}&name=<?php echo $name?>">{{$lead->id}}</a></td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>
	<td>Product</td>
	<td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
  
  


            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach
    </tbody>
  </table>

<div style="text-align: -webkit-center;"> {{$leads->links()}} </div>
</div>


@endsection