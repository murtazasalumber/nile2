<?php
$value= Session::all();

$value=session()->getId();
  //echo $value;


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>

    <script>
$(document).ready(function(){
  $('.footable').footable();
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();


          $.ajax({
        type: "GET",
        url:'alllead' ,
         data: {'keyword1' : keyword,'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
        }
    });
    });


});
</script>
<style type="text/css">
   @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
    .navbar-right
    {
      text-align: right;
    }
     .imgg {
       margin-top: -14  px;
    margin-left: -62px;
}
body
{
  font-family: myFirstFont;
}
.lead
{
     width: 87%;
    margin-left: 56px;
    border-radius: 11px;
    border: 1px solid #808080;
    font-size: 14px;


}
.lead:hover
{
  box-shadow: 3px 8px 8px 2px #888888;
}
.top10
{
      margin-top: 10px;
}
.bottommargin
{
  margin-bottom: 1%;
}
.btn
{
        color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}

@media only screen and (max-width: 500px) {
   .imgg {
       margin-top: -13px;
    margin-left: -23px;
}

@media only screen and (max-width: 1200px) {
  .lead
  {
    margin-left: 27px;
  }

}

</style>
  </head>
  <body>
   <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" rel="home" href="/admin/referalpartner" title="Buy Sell Rent Everyting">
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="/admin/referalpartner" style="font-family: myFirstFont;"
                                            >
                                            Home
                                        </a>
                                        <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>
<!-- title -->
    <div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2> Leads</h2></center>
               </div>
            </div>

    </div>
    <!-- title Ends -->

    <!-- Icon Starts here -->
    <div class="container" style="    margin-top: 11px;"> 
            <div class="row">
               <div class="col-sm-12" >
                  <center><a href="/referalpartner/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;" data-toggle="tooltip" title="Create New Service Lead" data-placement="bottom"></a></center>
               </div>
            </div>

    </div>
<!-- Icon ends here -->


{{csrf_field()}}
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
<!-- Search Section -->
  
<div class="container-fluid">
  <div class="row">
<?php $i=1;?>
  @foreach ($leads as $lead)
  <?php
    if(session()->has('name'))
    {
      $name=session()->get('name');
    }else
    {
    if(isset($_GET['name'])){
       $name=$_GET['name'];
    }else{
       $name=NULL;
    }
    }
    ?>









      <div class="col-sm-12 lead top10">
              <div class="col-sm-3 top10">
                <b>Sr. No.</b> : <?php echo $i;?>
              </div>
              <div class="col-sm-3 top10">
                <b>Lead ID</b> : {{$lead->id}}
              </div>
               <div class="col-sm-3 top10">
                <b> Created At : </b>{{$lead->created_at}} 
              </div>
                <div class="col-sm-3 top10">
                <b> Customer Name :</b> {{$lead->fName}} {{$lead->mName}} {{$lead->lName}}
              </div>
                <div class="col-sm-3 top10">
                <b> City : </b>{{$lead->Branch}} 
              </div>
               <div class="col-sm-3 top10">
                <b> Customer Mobile :</b> {{$lead->Country_Code}}{{$lead->MobileNumber}} 
              </div>
                <div class="col-sm-3 top10">
                <b> Service Type :</b> {{$lead->ServiceType}} 
              </div>
              <div class="col-sm-3 top10">
                <b>Lead Status : </b>{{$lead->ServiceStatus}}
              </div>
              <div class="col-sm-3 top10">
                <b> Follow Up Date :</b> {{$lead->followupdate}}
              </div>
              <div class="col-sm-3 top10 bottommargin">
                <b> Drop Reason :</b> {{$lead->reason}}
              </div>
      </div>
       <?php $i=$i+1;?>
       @endforeach
  </div>

</div>

<!-- Search Section Ends-->
<!-- Reslts -->
 <!--  <div class="container-fluid" style="margin-top: 50px;" >
    <div class="row" >
      <div class="col-sm-12" id="result">
            <table class="table footable" style="font-family: Raleway,sans-serif; margin-top: -38px;
">
          <thead>

              <tr>
              <th><b>Sr. No.</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b> Created At </b></th>

                    <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                    <th data-hide="phone,tablet"><b>City </b></th>


                    <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>


                </tr>
            </thead>
            <tbody> -->
<!-- <?php $i=1;?>
  @foreach ($leads as $lead)
<tr>
    <?php
    if(session()->has('name'))
    {
      $name=session()->get('name');
    }else
    {
    if(isset($_GET['name'])){
       $name=$_GET['name'];
    }else{
       $name=NULL;
    }
    }
    ?>

    <td><?php echo $i;?></td>
    <td>{{$lead->id}}</td>
    <td>{{$lead->created_at}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->Branch}}</td>
  <td>{{$lead->ServiceType}}</td>
  <td>{{$lead->followupdate}}</td>
  <td>{{$lead->ServiceStatus}}</td>
  <td>{{$lead->reason}}</td>
  <?php $i=$i+1;?> -->
<!--
<td>

<form action="{{'/cc/'.$lead->id}}" method="post">
{{csrf_field()}}
{{ method_field('DELETE') }}
<input type="submit" value="Delete">

</form>

    </td> -->
     </tr>

<!-- @endforeach -->
 <!-- </tbody>
</table>
<div style="text-align: -webkit-center;"> {{$leads->appends(request()->except('page'))->links()}} </div>

      </div>

    </div>

</div> -->


  @extends('layouts.footer')




  </body>
  </html>
