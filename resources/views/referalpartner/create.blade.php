@extends('layouts.referalpartner')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}"> -->
         <link rel="shortcut icon" href="{{ asset('img/favicon-96x96.png') }}">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
<style type="text/css">
  button[disabled], html input[disabled] {
    background: white;
    cursor: default;
}
</style>
<script>
$(function() {
    $('#country').on('change', function() {
        $('#phone').val($(this).val());
    });



    var country = $('#country').val();
    $('#phone').val(country);

    //  $('#sameaspermanentadress').change(function () {

    //         console.log("checked");
    // });
      $('#sameaspermanentadress').change(function () {

        if (this.checked) {
              console.log("checked");
              // $('#Address1').val("fgg");
             var Addresd1 =$('#Address1').val();
              // Adrees1
              $('#PAddress1').val(Addresd1);
              $("#PAddress1").prop('disabled', true);


              //Adress2
              var Addresd2 =$('#Address2').val();
              $('#PAddress2').val(Addresd2);
              $("#PAddress2").prop('disabled', true);

              //City

              var City =$('#City').val();
              $('#PCity').val(City);
              $("#PCity").prop('disabled', true);

              //district

              var District =$('#District').val();
              $('#PDistrict').val(District);
              $("#PDistrict").prop('disabled', true);

              //State

              var State =$('#State').val();
              $('#PState').val(State);
              $("#PState").prop('disabled', true);


              //Pincode

               var Pincode =$('#PinCode').val();
              $('#PPinCode').val(Pincode);
              $("#PPinCode").prop('disabled', true);
          }

         if(!$(this).is(":checked"))
         {
            console.log("unchecked");

            // Adrees1
            $('#PAddress1').val("");
            $("#PAddress1").prop('disabled', false);

            //Adress 2
            $('#PAddress2').val("");
            $("#PAddress2").prop('disabled', false);

            //City
            $('#PCity').val("");
            $("#PCity").prop('disabled', false);

            //District
            $('#PDistrict').val("");
            $("#PDistrict").prop('disabled', false);

             //State
            $('#PState').val("");
            $("#PState").prop('disabled', false);


            //Pincode
            $('#PPinCode').val("");
            $("#PPinCode").prop('disabled', false);

         }
    });





        $('#Esameaspermananentaddress').change(function () {

        if (this.checked) {
              // console.log("checked");
              // $('#Address1').val("fgg");
             var Addresd1 =$('#Address1').val();
              // Adrees1
              $('#EAddress1').val(Addresd1);
              $("#EAddress1").prop('disabled', true);


              //Adress2
              var Addresd2 =$('#Address2').val();
              $('#EAddress2').val(Addresd2);
              $("#EAddress2").prop('disabled', true);

              //City

              var City =$('#City').val();
              $('#ECity').val(City);
              $("#ECity").prop('disabled', true);

              //district

              var District =$('#District').val();
              $('#EDistrict').val(District);
              $("#EDistrict").prop('disabled', true);

              //State

              var State =$('#State').val();
              $('#EState').val(State);
              $("#EState").prop('disabled', true);


              //Pincode

               var Pincode =$('#PinCode').val();
              $('#EPinCode').val(Pincode);
              $("#EPinCode").prop('disabled', true);
          }

         if(!$(this).is(":checked"))
         {
            // console.log("unchecked");

            // Adrees1
            $('#EAddress1').val("");
            $("#EAddress1").prop('disabled', false);

            //Adress 2
            $('#EAddress2').val("");
            $("#EAddress2").prop('disabled', false);

            //City
            $('#ECity').val("");
            $("#ECity").prop('disabled', false);

            //District
            $('#EDistrict').val("");
            $("#EDistrict").prop('disabled', false);

             //State
            $('#EState').val("");
            $("#EState").prop('disabled', false);


            //Pincode
            $('#EPinCode').val("");
            $("#EPinCode").prop('disabled', false);

         }
    });


       $("#EmergencyContact").keyup(function(){

            // console.log("clciked");

            var mobileno = $('#EmergencyContact').val();
            var reg = /^[\+?\d[\d -]{8,12}\d$/;

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#EmergencyContact").css("border-bottom", "1px solid red");
        }
        else
        {
          // console.log("Correct Email");
           $("#EmergencyContact").css("border-bottom", "1px solid green");
        }

        });


        $("#EmergencyContact").blur(function(){
            var value = $('#EmergencyContact').val();

         var reg = /^[\+?\d[\d -]{8,12}\d$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EmergencyContact").css("border-bottom", "1px solid red");
        }
         else
        {
          // console.log("Correct Email");
           $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
        }

        if(value == "")
        {
            $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
        }
    });



  //Phone Number
        $("#phone").keyup(function(){

            // console.log("clciked");

            var mobileno = $('#phone').val();
            var reg = /^[\+?\d[\d -]{8,12}\d$/;

            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

        });


        $("#phone").blur(function(){
            var value = $('#phone').val();

         var reg = /^[\+?\d[\d -]{8,12}\d$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#phone").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#phone").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#phone").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });


    // Pincode Validaion in Addres
  $("#PinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PinCode").blur(function(){
            var value = $('#PinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Present Addres
  $("#PPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PPinCode").blur(function(){
            var value = $('#PPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Emergency Addres
  $("#EPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#EEPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#EPinCode").blur(function(){
            var value = $('#EPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#EPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });


    // Client Email Id Validation Start
    $("#clientemail").keyup(function(){
         var value = $('#clientemail').val();

         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);

        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
        }


      });

    $("#clientemail").blur(function(){
            var value = $('#clientemail').val();

         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
        }

          if(value == "")
        {
            $("#clientemail").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }
    });

    // Cleint ValidationEmail Ends




});
</script>

<script>
$(document).ready(function(){


  var value=$('#request').val();
     if(value == "Service")
    {
      $("#type").show();
    }else
    {
      $("#type").hide();
    }
    if(value == "Product")
    {
      $("#type1").show();
    }else
    {
      $("#type1").hide();
    }



    $("#request").change(function(){
    var value= $("#request").val();
   if(value == "Service")
    {
      $("#type").show();
    }else
    {
      $("#type").hide();
    }
    if(value == "Product")
    {
      $("#type1").show();
    }else
    {
      $("#type1").hide();
    }

});

});
</script>
<style type="text/css">
  .btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
.footer
  {
    margin-top: 120px;
  }
.btn:hover
{
  outline: 0;
    background-color: #00C851;
}
  .imgg {
       margin-top: -13px;
    margin-left: -62px;
}
@media only screen and (max-width: 1200px) {

#loo
{
    margin-left: 11%;
}

}
</style>

</head>
<body>

<!-- header -->
<!-- <div class="navbar navbar-default " style="    background-color: white;
    border-color: #e7e7e7;">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 8px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" rel="home" href="/admin/vertical" title="Buy Sell Rent Everyting">
                <img class="imgg"
                     src=" /img/healthheal_logo.png ">
            </a>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="/admin/vertical" style="font-size: 15px;">Home</a></li>


            </ul>

        </div>

    </div>
</div> -->

<!-- End of header -->
<!-- title -->
    <div class="container">
            <div class="row">
               <div class="col-sm-12" id="title">
                    <h2> {{substr(Route::currentRouteName(),20)}} Lead Details </h2>
               </div>
            </div>

    </div>

    <!-- title Ends -->

<form class="form-horizontal" action="/referalpartner" method="POST">
{{csrf_field()}}
@section('editMethod')
@show

    <div class="container" id="main" style="    margin-top: 34px;">

      <div class="row" id="maintitle" style="    margin-left: -4px;
    width: 102%;" >



      </div>
    </div>

     <div class="container" id="main1" style="margin-top: 11px;">

      <div class="row" id="firstform" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">
              <div class="col-sm-4" style="    margin-top: 4px;">

                <label>First Name <span style="color:Red; font-size: 20px;">*</span></label>


                            <input type="text"  rows="5" name="FName" id="FName" value="@yield('editFName')" required>

              </div>
               <div class="col-sm-4" style="    margin-top: 15px;">

                <label>Middle Name </label>


                            <input type="text"  rows="5" name="MName" id="MName" value="@yield('editMName')">

              </div>
               <div class="col-sm-4" id="firstrow">
                   <label>Last Name</label>
                            <input type="text"  rows="5"  name="LName" id="LName" value="@yield('editLName')" >

              </div>
               <!-- <div class="col-sm-3" id="firstrow">
                   <label>Email ID</label>
                            <input type="text"  rows="5" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

              </div> -->

                <div class="col-sm-4" style="margin-top: 25px;">


                        <label>Mobile No. <span style="color:Red; font-size: 20px;">*</span></label>

                            <input type="text"  id="phone"  rows="5" name="MobileNumber" id="MobileNumber" value="@yield('editMobileNumber')" maxlength="10" required>
              </div>
                <div class="col-sm-4" style="margin-top: 35px;">


                         <label>Alternate no.</label>

                     <input type="text" rows="5" name="AlternateNumber" id="AlternateNumber" value="@yield('editAlternateNumber')">
              </div>

               <div class="col-sm-4" id="secondrow"  >
                 <label>Request Type </label>
                            <select name="RequestType" id="request"  value="@yield('editRequestType')" required>
            <option value="@yield('editRequestType')">@yield('editRequestType')</option>
            <option value="Service">Service</option>
            <option value="Product">Product</option>
          </select>

              </div>

              <div class="col-sm-4" id="type" style="margin-top: 36px;" >
                 <label>Service Type </label>

                            <select name="ServiceType" id="ServiceType"  >
                          <option value="@yield('editServiceType')">@yield('editServiceType')</option>
                          @foreach($vertical as $vertical)
                          <option value="{{ $vertical->verticaltype}}">{{ $vertical->verticaltype}}</option>
                          @endforeach
           </select>


              </div>
              <div class="col-sm-4" id="type1" style="margin-top: 36px;"  >
                 <label>Product Name </label>
                            <input type="text"   rows="5" name="ProductName" id="ProductName" value="@yield('editProductName')">

              </div>

              <div class="col-sm-4" id="secondrow" >
                 <label>Address </label>
                            <input type="text"   rows="5" name="Address" id="Address" value="@yield('editAddress')">

              </div>

               <div class="col-sm-4" id="secondrow" style="margin-top: 35px; margin-bottom: 10px;">
                 <label style="  margin-bottom: 5px;">City</label><br>
                  <!-- <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')"> -->

                  <select  id="City"  name="City" >

                      <option value="@yield('editCity')">@yield('editCity')</option>
                      @for($i=0;$i<$count1;$i++)
                      <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
                      @endfor
                      <!-- <option value="+1">US</option>
                      <option value="+44 ">UK</option> -->
                  </select>



              </div>

             </div>



 <?php $loginname=$_GET['name'];

?>

<input type="hidden" name="loginname" value="<?php echo $loginname;?>">

    <div class="container" style="    margin-top: 32px;">
          <div class="row">
                <div class="col-sm-12">
                 <!--  <center>  <button type="submit" class="btn btn-default" id="submitt" >Submit</button></center> -->

                   <center><button type="reset" class="btn btn-danger" style="    background-color: #ff3547;" >Reset</button>
              &emsp;
                <button type="submit" class="btn btn-default" id="submitt" >Submit</button>
                   </center>
                </div>
          </div>
    </div>

    @include('partial.errors')
  </form>


</body>

@endsection
