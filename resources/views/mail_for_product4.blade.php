Dear {{$cust_name}}, <br /><br />
Your order no: {{$orderid}} has been shipped. And it will reach you soon.<br /><br />

Please quote this OrderID for further communications with our team.<br /><br />

For more information about our Services & Products, please call us at 8880004422 or visit our website: www.healthheal.in<br /><br />

Thank you,<br /><br />
Health Heal
