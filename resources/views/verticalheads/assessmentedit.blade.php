<?php
$today = date("Y-m-d");
$numberofdoctors=$count_of_docs;
if($count_of_docs> 0)
{


}
?>
@section('editid', $lead->id)


@if($verc == NULL)

@section('editassigned',$verc)

@else
@section('editassigned',$verc->FirstName)

@endif



@section('editfollowupdate',$lsd->followupdate)
@section('editreason',$lsd->reason)


@section('editrequested_service',$service_request->requested_service)
@section('editServiceStatus',$service_request->ServiceStatus)


@section('editAssessor',$assessment->Assessor)
@section('editAssessDateTime',$assessment->AssessDateTime)
@section('editAssessPlace',$assessment->AssessPlace)
@section('editServiceStartDate',$assessment->ServiceStartDate)
@section('editServicePause',$assessment->ServicePause)
@section('editServiceEndDate',$assessment->ServiceEndDate)
@section('editShiftPreference',$assessment->ShiftPreference)
@section('editSpecificRequirements',$assessment->SpecificRequirements)
@section('editDaysWorked',$assessment->DaysWorked)
@section('editLongitude',$assessment->Longitude)
@section('editLatitude',$assessment->Latitude)


@section('editInspection', $abdomen->Inspection)
@section('editAusculationofBS', $abdomen->AusculationofBS)
@section('editPalpation', $abdomen->Palpation)
@section('editPercussion', $abdomen->Percussion)
@section('editIleostomy', $abdomen->Ileostomy)
@section('editColostomy', $abdomen->Colostomy)
@section('editFunctioning', $abdomen->Functioning)
@section('editShape', $abdomen->Shape)
@section('editgirth', $abdomen->girth)
@section('editchangeincolor', $abdomen->changeincolor)
@section('editBulgeType', $abdomen->BulgeType)
@section('editabdomennotess', $abdomen->abdomennotess)
@section('editnotsignificantforabdomen', $abdomen->notsignificantforabdomen)



@section('editChestPain', $circulatory->ChestPain)
@section('edithoHTNCADCHF', $circulatory->hoHTNCADCHF)
@section('editHR', $circulatory->HR)
@section('editPeripheralCyanosis', $circulatory->PeripheralCyanosis)
@section('editJugularVein', $circulatory->JugularVein)
@section('editSurgeryHistory', $circulatory->SurgeryHistory)
@section('editcirculatorynotes', $circulatory->circulatorynotes)
@section('editnotsignificantforcirculatory',$circulatory->notsignificantforcirculatory)

@section('editLanguage', $communication->Language)
@section('editAdequateforAllActivities', $communication->AdequateforAllActivities)
@section('editunableToCommunicate', $communication->unableToCommunicate)
@section('editcommunicationnotes', $communication->communicationnotes)
@section('editnotsignificantforcommunication', $communication->notsignificantforcommunication)

<?php
$languagenew= $communication->Language;

?>
@section('editUpper', $denture->Upper)
@section('editLower', $denture->Lower)
@section('editCleaning', $denture->Cleaning)
@section('editdenturenotes', $denture->denturenotes)
@section('editnotsignificantfordenture', $denture->notsignificantfordenture)


@section('editUppROM', $extremitie->UppROM)
@section('editUppMuscleStrength', $extremitie->UppMuscleStrength)
@section('editLowROM', $extremitie->LowROM)
@section('editLowMuscleStrength', $extremitie->LowMuscleStrength)

@section('editFecalType', $fecal->FecalType)
@section('editIncontinentOccasionally', $fecal->IncontinentOccasionally)
@section('editIncontinentAlways', $fecal->IncontinentAlways)
@section('editCommodechair', $fecal->Commodechair)
@section('editNeedassistanceoffecal', $fecal->Needassistanceoffecal)
@section('editTypeoffecal', $fecal->Typeoffecal)
@section('editfecalnotes', $fecal->fecalnotes)


@section('editUrinaryContinent', $genito->UrinaryContinent)
@section('editNeedassistanceofurinary', $genito->Needassistanceofurinary)
@section('editTypeofurinary', $genito->Typeofurinary)
@section('editHoPOSTTURP', $genito->HoPOSTTURP)
@section('editHoUTI', $genito->HoUTI)
@section('editCompletelyContinet', $genito->CompletelyContinet)
@section('editIncontinentUrineOccasionally', $genito->IncontinentUrineOccasionally)
@section('editIncontinentUrineNightOnly', $genito->IncontinentUrineNightOnly)
@section('editIncontinentUrineAlways', $genito->IncontinentUrineAlways)
@section('editurinarynotes', $genito->urinarynotes)
@section('editnotsignificantforurinary', $genito->notsignificantforurinary)



@section('editWeight', $generalcondition->Weight)
@section('editHeight', $generalcondition->Height)
@section('editHeight_Measured_In', $generalcondition->Height_Measured_In)
@section('editBMI', $generalcondition->BMI)


@section('editMedicalHistory', $generalcondition->MedicalHistory)



@section('editLongTerm', $memoryintact->LongTerm)
@section('editShortTerm', $memoryintact->ShortTerm)
@section('editmemoryintactnotes', $memoryintact->memoryintactnotes)
@section('editnotsignificantformemoryintact', $memoryintact->notsignificantformemoryintact)


@section('editIndependent', $mobility->Independent)
@section('editNeedAssistance', $mobility->NeedAssistance)
@section('editTypeofmobility', $mobility->Typeofmobility)
@section('editmobilitynotes', $mobility->mobilitynotes)
@section('editWalker', $mobility->Walker)
@section('editWheelChair', $mobility->WheelChair)
@section('editCrutch', $mobility->Crutch)
@section('editCane', $mobility->Cane)
@section('editChairFast', $mobility->ChairFast)
@section('editBedFast', $mobility->BedFast)
@section('editnotsignificantformobility', $mobility->notsignificantformobility)

@section('editPresentHistory', $generalhistory->PresentHistory)
@section('editPastHistory', $generalhistory->PastHistory)
@section('editFamilyHistory', $generalhistory->FamilyHistory)
@section('editMensural_OBGHistory', $generalhistory->Mensural_OBGHistory)
@section('editAllergicHistory', $generalhistory->AllergicHistory)
@section('editAllergicStatus', $generalhistory->AllergicStatus)
@section('editAllergicSeverity', $generalhistory->AllergicSeverity)


@section('editIntact', $nutrition->Intact)
@section('editType',$nutrition->Type)
@section('editnutritionnotes',$nutrition->nutritionnotes)
@section('editDiet', $nutrition->Diet)
@section('editBFTime', $nutrition->BFTime)
@section('editLunchTime', $nutrition->LunchTime)
@section('editSnacksTime', $nutrition->SnacksTime)
@section('editDinnerTime', $nutrition->DinnerTime)
@section('editTPN', $nutrition->TPN)
@section('editRTFeeding', $nutrition->RTFeeding)
@section('editRTFeeding_Measured_In', $nutrition->RTFeeding_Measured_In)
@section('editPEGFeeding', $nutrition->PEGFeeding)
@section('editPEGFeeding_Measured_In', $nutrition->PEGFeeding_Measured_In)
@section('editfeedingtype', $nutrition->feedingtype)
@section('editfrequency', $nutrition->frequency)
@section('editroutes', $nutrition->routes)
@section('editQuantityinfusion', $nutrition->Quantityinfusion)
@section('edittimeinfusion', $nutrition->timeinfusion)
@section('editinfusionratedropspermin', $nutrition->infusionratedropspermin)
@section('editdiettnotes', $nutrition->diettnotes)



@section('editPerson', $orientation->Person)
@section('editPlace', $orientation->Place)
@section('editTime', $orientation->Time)
@section('editorientationnotes', $orientation->orientationnotes)
@section('editEyeopening', $orientation->Eyeopening)
@section('editverbalresponse', $orientation->verbalresponse)
@section('editmotorresponse', $orientation->motorresponse)
@section('edittotalscoregcs', $orientation->totalscoregcs)
@section('editnotsignificantfornervoussystem', $orientation->notsignificantfornervoussystem)

<?php
$Eyeopening=$orientation->Eyeopening;
$verbalresponse=$orientation->verbalresponse;
$motorresponse=$orientation->motorresponse;
$totalscroe=$orientation->totalscoregcs;

?>

@section('editBP', $vitalsign->BP)
@section('editBP_equipment', $vitalsign->BP_equipment)
@section('editBP_taken_from', $vitalsign->BP_taken_from)
@section('editRR', $vitalsign->RR)
@section('editTemperature', $vitalsign->Temperature)
@section('editTemperatureType', $vitalsign->TemperatureType)
@section('editvitalnotes', $vitalsign->vitalnotes)

@section('editTemperature_Measured_In', $vitalsign->Temperature_Measured_In)
@section('editP1', $vitalsign->P1)
@section('editP2', $vitalsign->P2)
@section('editP3', $vitalsign->P3)
@section('editR1', $vitalsign->R1)
@section('editR2', $vitalsign->R2)
@section('editR3', $vitalsign->R3)
@section('editR4', $vitalsign->R4)
@section('editT1', $vitalsign->T1)
@section('editT2', $vitalsign->T2)
@section('editPulse', $vitalsign->Pulse)
@section('editPainScale', $vitalsign->PainScale)
@section('editQuality', $vitalsign->Quality)
@section('editSeverityScale', $vitalsign->SeverityScale)
@section('editMedicalDiagnosis', $vitalsign->MedicalDiagnosis)
@section('editq1', $vitalsign->q1)


<?php
$ServerityScale= $vitalsign->SeverityScale;

?>

@section('editSOB', $respiratory->SOB)
@section('editCough', $respiratory->Cough)
@section('editColorOfPhlegm', $respiratory->ColorOfPhlegm)
@section('editNebulization', $respiratory->Nebulization)
@section('editTracheostomy', $respiratory->Tracheostomy)
@section('editCPAP_BIPAP', $respiratory->CPAP_BIPAP)
@section('editplacementoficd', $respiratory->placementoficd)
@section('editICD', $respiratory->ICD)
@section('editnoofdays', $respiratory->noofdays)
@section('editrespiratorynotes', $respiratory->respiratorynotes)
@section('editH_OTB_Asthma_COPD', $respiratory->H_OTB_Asthma_COPD)
@section('editSPO2', $respiratory->SPO2)
@section('editcentral_cynaosis', $respiratory->central_cynaosis)
@section('$respiratory->notsignificantforrespiratory', $respiratory->notsignificantforrespiratory)
<?php
$Tracheostomy=$respiratory->Tracheostomy;
$historyofrespiratory=$respiratory->H_OTB_Asthma_COPD;
?>



@section('editPosition_Type',$position->Position_Type)
@section('editpositionnotes',$position->positionnotes)

<!-- @section('editSupine',$position->Supine)
@section('editRt_Lateral',$position->Rt_Lateral)
@section('editLt_Lateral',$position->Lt_Lateral) -->

<?php
$position_Type=$position->Position_Type;

?>


@section('editImpared', $visionhearing->Impared)
@section('editHImpared', $visionhearing->HImpared)
@section('editShortSight', $visionhearing->ShortSight)
@section('editLongSight', $visionhearing->LongSight)
@section('editWearsGlasses', $visionhearing->WearsGlasses)
@section('editHearingAids', $visionhearing->HearingAids)
@section('editvisionnotes', $visionhearing->visionnotes)
@section('edithearingnotes', $visionhearing->hearingnotes)
@section('editvisionnotsignificant', $visionhearing->notsignificantforvision)
@section('edithearingnotsignificant', $visionhearing->notsignificantforhear)

@section('editSKUid', $product->SKUid)
@section('editProductName', $product->ProductName)
@section('editDemoRequired', $product->DemoRequired)
@section('editAvailabilityStatus', $product->AvailabilityStatus)
@section('editAvailabilityAddress', $product->AvailabilityAddress)
@section('editSellingPrice', $product->SellingPrice)
@section('editRentalPrice', $product->RentalPrice)




@section('editMethod')
{{method_field('PUT')}}
@endsection

@extends('layouts.app1')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
              <!--       <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png"> -->
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
 <script src="mdtimepicker.js" type="text/javascript"></script>
  <script src="freshslider.min.js" type="text/javascript"></script>
 <script src="jquery.prettydropdowns.js" type="text/javascript"></script>
 <link rel="stylesheet" href="{{ URL::asset('freshslider.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/mdtimepicker.css') }}" />
 <link rel="stylesheet" href="{{ URL::asset('prettydropdowns.css') }}" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style type="text/css">
    .multiselect-container {
    border: 0px;
    width: 343px;
    position: absolute;
    list-style-type: none;
    margin: 0;
    padding: 0;
}
.btn-group>.btn:first-child {
           margin-top: -1px;
    box-shadow: none!important;
    outline: none;
    width: 343px;
    margin-left: 0;
    border-right: 0px;
    background: white;
    border-radius: 0px;
    color: black!important;
    border-bottom: 1px solid black;
    height: 25px;
}
.btn-group>.btn:first-child:hover
{
  color:black!important;

}
.btn-group.open .dropdown-toggle {
    box-shadow: none!important;
    cursor: pointer;
    border-left: 0px;
    border-top: 0px;
    width: 343px;
    -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
    border-right: 0px;
    background: white; 
}
.btn-group.open .dropdown-toggle:focus
{
  background: white!important;
}
.btn-group.open .dropdown-toggle:hover
{
  background: :white!important;
}
ul > li > a > label
{
  color:black;
}
ul > li > a
{
  display: none;
}
/*.multiselect-selected-text
{
  display: none;
}*/
.caret
{
  margin-top: 9px;
  float: right!important;
}
.multiselect-container>li>a>label>input[type=checkbox] {
    margin-bottom: 5px;
    margin-left: -57%;
}
</style>
    <script>
  /*! freshslider 27-09-2014 */
!function(a){a.fn.freshslider=function(b){var c=this,d="undefined"==typeof b.range?!1:b.range,e=!d,f=b.min||0,g=b.max||100,h=g-f,i=b.step||1,j=b.unit||"",k="undefined"==typeof b.enabled?!0:b.enabled,l=[0,1],m="undefined"==typeof b.text?!0:b.text,n=null;if(0>h)throw new Error;var o=function(a){return a&&"[object Function]"==Object.prototype.toString.call(a)},p=null;o(b.onchange)===!0&&(p=b.onchange);var q=""+i,r=0;q.indexOf(".")>=0&&(r=q.length-q.indexOf(".")-1),b.hasOwnProperty("value")?e?l[1]=(b.value-f)/h:b.value.length&&2==b.value.length&&(l[0]=(b.value[0]-f)/h,l[1]=(b.value[1]-f)/h):e&&(l[1]=.5),n=d?this.html("<div class='fsslider'><div class='fsfull-value'></div><div class='fssel-value'></div><div class='fscaret fss-left'></div><div class='fscaret fss-right'></div></div>").find(".fsslider"):this.html("<div class='fsslider'><div class='fsfull-value'></div><div class='fssel-value'></div><div class='fscaret'></div></div>").find(".fsslider");var s=a(this.find(".fscaret")[0]),t=e?s:a(this.find(".fscaret")[1]),u=this.find(".fssel-value"),v=function(a){return i*Math.round(a/i)},w=function(){m&&(t.text((v(l[1]*h)+f).toFixed(r)+j),e||s.text((v(l[0]*h)+f).toFixed(r)+j));var a=c.width(),b=s.outerWidth(),d=t.outerWidth(),g=a-(b+d)/2;u.css({left:l[0]*a,width:(l[1]-l[0])*a}),s.css({left:l[0]*g+b/2,"margin-left":-(b/2),"z-index":x?0:1}),t.css({left:l[1]*g+d/2,"margin-left":-(d/2),"z-index":x?1:0}),p&&(e?p(v(l[1]*h)+f):p(v(l[0]*h)+f,v(l[1]*h)+f))},x=!0,y=!1;this.mousedown(function(a){if(k){y=!0;var b=c.width(),d=s.outerWidth(),f=t.outerWidth(),g=b-(d+f)/2,h=a.target,i=h.className,j=a.pageX-c.offset().left,m=j-d/2;if(m=0>m?0:m>g?g:m,e)l[1]=m/g,x=!0;else switch(i){case"fscaret fss-left":x=!1,l[0]=m/g;break;case"fscaret fss-right":x=!0,l[1]=m/g;break;default:m<(l[0]+l[1])/2*g?(x=!1,l[0]=m/g):(x=!0,l[1]=m/g)}return w(),event.preventDefault?void event.preventDefault():!1}});var z=function(){k&&(y=!1,l[1]=v(l[1]*h)/h,e||(l[0]=v(l[0]*h)/h),w())};return a(document).mouseup(function(){y&&z()}),this.mousemove(function(a){if(k){if(y){var b=c.width(),d=s.outerWidth(),f=t.outerWidth(),g=b-(d+f)/2,h=a.target,i=(h.className,a.pageX-c.offset().left),j=i-d/2;j=0>j?0:j>g?g:j,e?(l[1]=j/g,x=!0):x?(l[1]=j/g,l[1]<l[0]&&(l[1]=l[0])):(l[0]=j/g,l[0]>l[1]&&(l[0]=l[1])),w()}return event.preventDefault?void event.preventDefault():!1}}),this.getValue=function(){return e?[l[1]*h+f]:[l[0]*h+f,l[1]*h+f]},this.setValue=function(){e?(l[1]=(arguments[0]-f)/h,w()):arguments.length>=2&&(l[0]=(arguments[0]-f)/h,l[1]=(arguments[1]-f)/h,w())},this.setEnabled=function(a){k="undefined"==typeof a?!0:a,k?n.removeClass("fsdisabled"):n.addClass("fsdisabled")},this.setEnabled(k),w(),this}}(jQuery);
</script>

<script>

/* -- DO NOT REMOVE --
 * jQuery MDTimePicker v1.0 plugin
 *
 * Author: Dionlee Uy
 * Email: dionleeuy@gmail.com
 *
 * Date: Tuesday, August 28 2017
 *
 * @requires jQuery
 * -- DO NOT REMOVE -- */
 if (typeof jQuery === 'undefined') { throw new Error('MDTimePicker: This plugin requires jQuery'); }
+function ($) {
  var MDTP_DATA = "mdtimepicker", HOUR_START_DEG = 120, MIN_START_DEG = 90, END_DEG = 360, HOUR_DEG_INCR = 30, MIN_DEG_INCR = 6,
    EX_KEYS = [9, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123];

  var Time = function (hour, minute) {
    this.hour = hour;
    this.minute = minute;

    this.format = function(format, hourPadding) {
      var that = this, is24Hour = (format.match(/h/g) || []).length > 1;

      return $.trim(format.replace(/(hh|h|mm|ss|tt|t)/g, function (e) {
        switch(e.toLowerCase()){
          case 'h':
            var hour = that.getHour(true);

            return (hourPadding && hour < 10 ? '0' + hour : hour);
          case 'hh': return (that.hour < 10 ? '0' + that.hour : that.hour);
          case 'mm': return (that.minute < 10 ? '0' + that.minute : that.minute);
          case 'ss': return '00';
          case 't': return is24Hour ? '' : that.getT().toLowerCase();
          case 'tt': return is24Hour ? '' : that.getT();
        }
      }));
    };

    this.setHour = function (value) { this.hour = value; };
    this.getHour = function (is12Hour) { return is12Hour ? this.hour === 0 || this.hour === 12 ? 12 : (this.hour % 12) : this.hour; };
    this.invert = function () {
      if (this.getT() === 'AM') this.setHour(this.getHour() + 12);
      else this.setHour(this.getHour() - 12);
    };
    this.setMinutes = function (value) { this.minute = value; }
    this.getMinutes = function (value) { return this.minute; }
    this.getT = function() { return this.hour < 12 ? 'AM' : 'PM'; };
  };

  var MDTimePicker = function (input, config) {
    var that = this;

    this.visible = false;
    this.activeView = 'hours';
    this.hTimeout = null;
    this.mTimeout = null;
    this.input = $(input);
    this.config = config;
    this.time = new Time(0, 0);
    this.selected = new Time(0,0);
    this.timepicker = {
      overlay : $('<div class="mdtimepicker hidden"></div>'),
      wrapper : $('<div class="mdtp__wrapper"></div>'),
      timeHolder : {
        wrapper: $('<section class="mdtp__time_holder"></section>'),
        hour: $('<span class="mdtp__time_h">12</span>'),
        dots: $('<span class="mdtp__timedots">:</span>'),
        minute: $('<span class="mdtp__time_m">00</span>'),
        am_pm: $('<span class="mdtp__ampm">AM</span>')
      },
      clockHolder : {
        wrapper: $('<section class="mdtp__clock_holder"></section>'),
        am: $('<span class="mdtp__am">AM</span>'),
        pm: $('<span class="mdtp__pm">PM</span>'),
        clock: {
          wrapper: $('<div class="mdtp__clock"></div>'),
          dot: $('<span class="mdtp__clock_dot"></span>'),
          hours: $('<div class="mdtp__hour_holder"></div>'),
          minutes: $('<div class="mdtp__minute_holder"></div>')
        },
        buttonsHolder : {
          wrapper: $('<div class="mdtp__buttons">'),
          btnOk : $('<span class="mdtp__button ok">Ok</span>'),
          btnCancel: $('<span class="mdtp__button cancel">Cancel</span>')
        }
      }
    };

    var picker = that.timepicker;

    that.setup(picker).appendTo('body');

    picker.clockHolder.am.click(function () { if (that.selected.getT() !== 'AM') that.setT('am'); });
    picker.clockHolder.pm.click(function () { if (that.selected.getT() !== 'PM') that.setT('pm'); });
    picker.timeHolder.hour.click(function () { if (that.activeView !== 'hours') that.switchView('hours'); });
    picker.timeHolder.minute.click(function () { if (that.activeView !== 'minutes') that.switchView('minutes'); });
    picker.clockHolder.buttonsHolder.btnOk.click(function () {
      that.setValue(that.selected);

      var formatted = that.getFormattedTime();

      that.input.trigger($.Event('timechanged', { time: formatted.time, value: formatted.value }))
        .trigger('onchange'); // for ASP.Net postback

      that.hide();
    });
    picker.clockHolder.buttonsHolder.btnCancel.click(function () { that.hide(); });

    that.input.on('keydown', function (e) {
      if (e.keyCode === 13) that.show();
      return !(EX_KEYS.indexOf(e.which) < 0 && that.config.readOnly); })
      .on('click', function () { that.show(); })
      .prop('readonly', that.config.readOnly);

    if (that.input.val() !== '') {
      var time = that.parseTime(that.input.val(), that.config.format);

      that.setValue(time);
    } else {
      var time = that.getSystemTime();

      that.time = new Time(time.hour, time.minute);
    }

    that.resetSelected();
    that.switchView(that.activeView);
  };

  MDTimePicker.prototype = {
    constructor : MDTimePicker,

    setup : function (picker) {
      if (typeof picker === 'undefined') throw new Error('Expecting a value.');

      var that = this, overlay = picker.overlay, wrapper = picker.wrapper,
        time = picker.timeHolder, clock = picker.clockHolder;

      // Setup time holder
      time.wrapper.append(time.hour)
        .append(time.dots)
        .append(time.minute)
        .append(time.am_pm)
        .appendTo(wrapper);

      // Setup hours
      for (var i = 0; i < 12; i++) {
        var value = i + 1, deg = (HOUR_START_DEG + (i * HOUR_DEG_INCR)) % END_DEG,
          hour = $('<div class="mdtp__digit rotate-' + deg + '" data-hour="' + value + '"><span>'+ value +'</span></div>');

        hour.find('span').click(function () {
          var _data = parseInt($(this).parent().data('hour')),
            _selectedT = that.selected.getT(),
            _value = (_data + (_selectedT === 'PM' || (_selectedT === 'AM' && _data === 12) ? 12 : 0)) % 24;

          that.setHour(_value);
          that.switchView('minutes');
        });

        clock.clock.hours.append(hour);
      }

      // Setup minutes
      for (var i = 0; i < 60; i++) {
        var min = i < 10 ? '0' + i : i, deg = (MIN_START_DEG + (i * MIN_DEG_INCR)) % END_DEG,
          minute = $('<div class="mdtp__digit rotate-' + deg + '" data-minute="' + i + '"></div>');

        if (i % 5 === 0) minute.addClass('marker').html('<span>' + min + '</span>');
        else minute.html('<span></span>');

        minute.find('span').click(function () {
          that.setMinute($(this).parent().data('minute'));
        });

        clock.clock.minutes.append(minute);
      }

      // Setup clock
      clock.clock.wrapper
        .append(clock.am).append(clock.pm)
        .append(clock.clock.dot)
        .append(clock.clock.hours)
        .append(clock.clock.minutes)
        .appendTo(clock.wrapper);

      // Setup buttons
      clock.buttonsHolder.wrapper.append(clock.buttonsHolder.btnCancel)
        .append(clock.buttonsHolder.btnOk)
        .appendTo(clock.wrapper);

      clock.wrapper.appendTo(wrapper);

      switch(that.config.theme) {
        case 'red':
        case 'blue':
        case 'green':
        case 'purple':
        case 'indigo':
        case 'teal':
          wrapper.attr('data-theme', that.config.theme);
        break;
        default:
          wrapper.attr('data-theme', $.fn.mdtimepicker.defaults.theme);
        break;
      }

      wrapper.appendTo(overlay);

      return overlay;
    },

    setHour : function (hour) {
      if (typeof hour === 'undefined') throw new Error('Expecting a value.');

      var that = this;

      this.selected.setHour(hour);
      this.timepicker.timeHolder.hour.text(this.selected.getHour(true));

      this.timepicker.clockHolder.clock.hours.children('div').each(function (idx, div) {
        var el = $(div), val = el.data('hour');

        el[val === that.selected.getHour(true) ? 'addClass' : 'removeClass']('active');
      });
    },

    setMinute : function (minute) {
      if (typeof minute === 'undefined') throw new Error('Expecting a value.');

      this.selected.setMinutes(minute);
      this.timepicker.timeHolder.minute.text(minute < 10 ? '0' + minute : minute);

      this.timepicker.clockHolder.clock.minutes.children('div').each(function (idx, div) {
        var el = $(div), val = el.data('minute');

        el[val === minute ? 'addClass' : 'removeClass']('active');
      });
    },

    setT : function (value) {
      if (typeof value === 'undefined') throw new Error('Expecting a value.');

      if (this.selected.getT() !== value.toUpperCase()) this.selected.invert();

      var t = this.selected.getT();

      this.timepicker.timeHolder.am_pm.text(t);
      this.timepicker.clockHolder.am[t === 'AM' ? 'addClass' : 'removeClass']('active');
      this.timepicker.clockHolder.pm[t === 'PM' ? 'addClass' : 'removeClass']('active');
    },

    setValue : function (value) {
      if (typeof value === 'undefined') throw new Error('Expecting a value.');

      var time = typeof value === 'string' ? this.parseTime(value, this.config.format) : value;

      this.time = new Time(time.hour, time.minute);

      var formatted = this.getFormattedTime();

      this.input.val(formatted.value)
        .attr('data-time', formatted.time)
        .attr('value', formatted.value);
    },

    resetSelected : function () {
      this.setHour(this.time.hour);
      this.setMinute(this.time.minute);
      this.setT(this.time.getT());
    },

    getFormattedTime : function () {
      var time = this.time.format(this.config.timeFormat, false),
        tValue = this.time.format(this.config.format, this.config.hourPadding);

      return { time: time, value: tValue };
    },

    getSystemTime : function () {
      var now = new Date();

      return new Time (now.getHours(), now.getMinutes());
    },

    parseTime : function (time, tFormat) {
      var that = this, format = typeof tFormat === 'undefined' ? that.config.format : tFormat,
                hLength = (format.match(/h/g) || []).length,
        is24Hour = hLength > 1,
        mLength = (format.match(/m/g) || []).length, tLength = (format.match(/t/g) || []).length,
        timeLength = time.length,
        fH = format.indexOf('h'), lH = format.lastIndexOf('h'),
        hour = '', min = '', t = '';

      // Parse hour
      if (that.config.hourPadding || is24Hour) {
        hour = time.substr(fH, 2);
      } else {
        var prev = format.substring(fH - 1, fH), next = format.substring(lH + 1, lH + 2);

        if (lH === format.length - 1) {
          hour = time.substring(time.indexOf(prev, fH - 1) + 1, timeLength);
        } else if (fH === 0) {
          hour = time.substring(0, time.indexOf(next, fH));
        } else {
          hour = time.substring(time.indexOf(prev, fH - 1) + 1, time.indexOf(next, fH + 1));
        }
      }

      format = format.replace(/(hh|h)/g, hour);

      var fM = format.indexOf('m'), lM = format.lastIndexOf('m'),
        fT = format.indexOf('t');

      // Parse minute
      var prevM = format.substring(fM - 1, fM), nextM = format.substring(lM + 1, lM + 2);

      if (lM === format.length - 1) {
        min = time.substring(time.indexOf(prevM, fM - 1) + 1, timeLength);
      } else if (fM === 0) {
        min = time.substring(0, 2);
      } else {
        min = time.substr(fM, 2);
      }

      // Parse t (am/pm)
      if (is24Hour) t = parseInt(hour) > 11 ? (tLength > 1 ? 'PM' : 'pm') : (tLength > 1 ? 'AM' : 'am');
      else t = time.substr(fT, 2);

      var isPm = t.toLowerCase() === 'pm',
        outTime = new Time(parseInt(hour), parseInt(min));
      if ((isPm && parseInt(hour) < 12) || (!isPm && parseInt(hour) === 12)) {
          outTime.invert();
      }

      return outTime;
    },

    switchView : function (view) {
      var that = this, picker = this.timepicker, anim_speed = 350;

      if (view !== 'hours' && view !== 'minutes') return;

      that.activeView = view;

      picker.timeHolder.hour[view === 'hours' ? 'addClass' : 'removeClass']('active');
      picker.timeHolder.minute[view === 'hours' ? 'removeClass' : 'addClass']('active');

      picker.clockHolder.clock.hours.addClass('animate');
      if (view === 'hours') picker.clockHolder.clock.hours.removeClass('hidden');

      clearTimeout(that.hTimeout);

      that.hTimeout = setTimeout(function() {
        if (view !== 'hours') picker.clockHolder.clock.hours.addClass('hidden');
        picker.clockHolder.clock.hours.removeClass('animate');
      }, view === 'hours' ? 20 : anim_speed);

      picker.clockHolder.clock.minutes.addClass('animate');
      if (view === 'minutes') picker.clockHolder.clock.minutes.removeClass('hidden');

      clearTimeout(that.mTimeout);

      that.mTimeout = setTimeout(function() {
        if (view !== 'minutes') picker.clockHolder.clock.minutes.addClass('hidden');
        picker.clockHolder.clock.minutes.removeClass('animate');
      }, view === 'minutes' ? 20 : anim_speed);
    },

    show : function () {
      var that = this;

      if (that.input.val() === '') {
        var time = that.getSystemTime();
        this.time = new Time(time.hour, time.minute);
      }

      that.resetSelected();

      $('body').attr('mdtimepicker-display', 'on');

      that.timepicker.wrapper.addClass('animate');
      that.timepicker.overlay.removeClass('hidden').addClass('animate');
      setTimeout(function() {
        that.timepicker.overlay.removeClass('animate');
        that.timepicker.wrapper.removeClass('animate');

        that.visible = true;
        that.input.blur();
      }, 10);
    },

    hide : function () {
      var that = this;

      that.timepicker.overlay.addClass('animate');
      that.timepicker.wrapper.addClass('animate');
      setTimeout(function() {
        that.switchView('hours');
        that.timepicker.overlay.addClass('hidden').removeClass('animate');
        that.timepicker.wrapper.removeClass('animate');

        $('body').removeAttr('mdtimepicker-display');

        that.visible = false;
        that.input.focus();
      }, 300);
    }
  };

  $.fn.mdtimepicker = function (config) {
    return $(this).each(function (idx, el) {
      var that = this,
        $that = $(this),
        picker = $(this).data(MDTP_DATA);
        options = $.extend({}, $.fn.mdtimepicker.defaults, $that.data(), typeof config === 'object' && config);

      if (!picker) {
        $that.data(MDTP_DATA, (picker = new MDTimePicker(that, options)));
      }
      if(typeof config === 'string') picker[config]();

      $(document).on('keydown', function (e) {
        if(e.keyCode !== 27) return;

        if (picker.visible) picker.hide();
      });
    });
  }

  $.fn.mdtimepicker.defaults = {
    timeFormat: 'hh:mm:ss.000', // format of the time value (data-time attribute)
    format: 'h:mm tt',      // format of the input value
    theme: 'blue',        // theme of the timepicker
    readOnly: true,       // determines if input is readonly
    hourPadding: false      // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
  };
}(jQuery);
  </script>
  <script type="text/javascript">
    $.noConflict();
jQuery(document).ready(function($){

    $('#Language').multiselect();
     $('#Position_Type').multiselect();


     $('#Typeofurinary').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 2,
     });
      $('#Typeoffecal').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 2,
     });
      $('#Typeofmobility').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 2,
     });
       $('#H_OTB_Asthma_COPD').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 2,
     }); $('#hoHTNCADCHF').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 2,
     });
    var languageset = '<?php echo $languagenew; ?>';
    var position_Type = '<?php echo $position_Type; ?>';
    var fecal_type = '<?php echo $fecal->Typeoffecal; ?>';
    var urinary_type = '<?php echo $genito->Typeofurinary; ?>';
    var mobility_type = '<?php echo $mobility->Typeofmobility; ?>';

    var histpryofcirculatory = '<?php echo $circulatory->hoHTNCADCHF; ?>';
    var historyofrespiratory = '<?php echo $historyofrespiratory; ?>';


    var languagearray = languageset.split(",");
    var length=languagearray.length;
    for(var i=0; i < length ; i++)
    {
        $('#Language').multiselect('select', languagearray[i]);
    }

    var position_Typearray = position_Type.split(",");
    var lengthposition=position_Typearray.length;
    for(var i=0; i < lengthposition ; i++)
    {
        $('#Position_Type').multiselect('select', position_Typearray[i]);
    }
     var fecal_typearray = fecal_type.split(",");
    var lengthposition=fecal_typearray.length;
    for(var i=0; i < lengthposition ; i++)
    {
        $('#Typeoffecal').multiselect('select', fecal_typearray[i]);
    }
     var urinary_typearray = urinary_type.split(",");
    var lengthposition=urinary_typearray.length;
    for(var i=0; i < lengthposition ; i++)
    {
        $('#Typeofurinary').multiselect('select', urinary_typearray[i]);
    }
     var mobility_typearray = mobility_type.split(",");
    var lengthposition=mobility_typearray.length;
    for(var i=0; i < lengthposition ; i++)
    {
        $('#Typeofmobility').multiselect('select', mobility_typearray[i]);
    }


    var histpryofcirculatory = histpryofcirculatory.split(",");
    var lengthhistoryof=histpryofcirculatory.length;
    for(var i=0; i < lengthhistoryof ; i++)
    {
        $('#hoHTNCADCHF').multiselect('select', histpryofcirculatory[i]);
    }
    var historyofrespiratory = historyofrespiratory.split(",");
    var lengthhistoryof=historyofrespiratory.length;
    for(var i=0; i < lengthhistoryof ; i++)
    {
        $('#H_OTB_Asthma_COPD').multiselect('select', historyofrespiratory[i]);
    }

     $("#submit").click(function(event){
         //event.preventDefault();

         //console.log(no+no2);
         $('#formcontent').hide();
         $('.footer').hide();
         $('.navbar').hide();
        $("#messagetoshow").show();
        // $("#submit12").hide();
        var eye=$("#Eyeopening").val();
        console.log(eye);
         //alert("form will be subite");
         var language =$("#Language").val();

        $('#languageschecked').val(JSON.stringify(language));

        var Typeofurinary =$("#Typeofurinary").val();
        $('#Typeofurinarychecked').val(JSON.stringify(Typeofurinary));

         var Typeoffecal=$("#Typeoffecal").val();
        $('#Typeoffecalchecked').val(JSON.stringify(Typeoffecal));

         var Typeofmobility=$("#Typeofmobility").val();
        $('#Typeofmobilitychecked').val(JSON.stringify(Typeofmobility));

          var Position_Type = $("#Position_Type").val();

        $('#Position_Typechecked').val(JSON.stringify(Position_Type));

         var Historyofcirculatory1 = $("#hoHTNCADCHF").val();
        $('#Historyofcirculatory').val(JSON.stringify(Historyofcirculatory1));

        var Historyofrespiratory1 = $("#H_OTB_Asthma_COPD").val();
        $('#Historyofrespiratory').val(JSON.stringify(Historyofrespiratory1));


    });

var value= '<?php  echo $ServerityScale;?>';


    var s2 = $("#unranged-value").freshslider({
        step: 1,
        value:value,
        min:0,
        max:10,
         onchange:function(low){
            console.log(low);
            $("#SeverityScale").val(low);
        }
    });


var gender = $('#gender').val();

if(gender == "Male")
{
  $('#Mensuration_OBG').hide();

}else
{
  $('#Mensuration_OBG').show();
}




    $("#ServiceStatus").change(function(){
    var value= $("#ServiceStatus").val();
    if(value == "Deferred")
    {
      $("#submit").prop("disabled",false);
      $("#followupdate").show();
      $("#reason").hide();

      $(".asterisk").css("display","none");
      $(".asterisktemp").css("display","none");
      $(".asteriskpulse").css("display","none");

    }else if(value == "Converted")
    {
      //  $("#AssessmentDetails").removeClass().addClass('row collapse in');
      // $("#VitalSigns").removeClass().addClass('col-sm-12 collapse in');
      

      // $('html, body').animate({
      // scrollTop: ($('#VitalSigns').offset().top)
      // },800);
      // if(($("#BP").val() != "") && ($("#Temperature").val() != "") && ($("#RR").val() != "") && ($("#Pulse").val() != ""))
      // {
      //     $("#submit").prop('disabled',false);
      // }
      // else
      // {

      // $("#submit").prop('disabled',true);
      // }

      //Nothing Significant
      //onchange to stauts conveted to check enbale or not
         $(".asterisk").css("display","block");
         $(".asterisktemp").css("display","block");
         $(".asteriskpulse").css("display","block");
      //Nervous system
          var Person= $("#Person").val();
          var ServiceStatus=$('#ServiceStatus').val();
          var Place= $("#Place").val();
          var Time= $("#Time").val();
          var orientationnotes= $("#orientationnotes").val();
          var Eyeopening= $("#Eyeopening").val();
          var verbalresponse= $("#verbalresponse").val();
          var motorresponse= $("#motorresponse").val();
          var nsnss=$(".nsnss").is(":checked");
         if((Person == "" || Person == null) && (Place == "" || Place == null)  && (Time == "" || Time == null) && (orientationnotes == "" || orientationnotes == " ") && (Eyeopening == "" || Eyeopening == null) && (verbalresponse == "" || verbalresponse == null) && (motorresponse == "" || motorresponse == null) && nsnss== false)
        {
              $("#NervousSystemm").val("0");
        }else 
        {
              $("#NervousSystemm").val("1");
        }
        //Memory Intacts
        var ShortTerm=$("#ShortTerm").val();
        var LongTerm=$("#LongTerm").val();
        var memoryintactnotes=$("#memoryintactnotes").val();
        var switchval=$(".memoryintact").is(":checked");
        if((ShortTerm == "" || ShortTerm == null) && (LongTerm == "" || LongTerm == null)&& (memoryintactnotes == "" || memoryintactnotes == " ") && (switchval == false) )
        {
             $("#MemoryIntactsNs").val("0 ");
        }else 
        {
           $("#MemoryIntactsNs").val("1");
        }
        //Communication
        var Adequate = $("#AdequateforAllActivities").val();
        var unableToCommunicate = $("#unableToCommunicate").val();
        var communicationnotes = $("#communicationnotes").val();
        var communicationhval=$(".communication").is(":checked");
        var ServiceStatus=$('#ServiceStatus').val();
         if((unableToCommunicate == "" || unableToCommunicate == null) && (Adequate == "" || Adequate == null)&& (communicationnotes == "" || communicationnotes == " ")  && communicationhval==false)
        {
           $("#CommunicationNs").val("0");
        }else 
        {
           $("#CommunicationNs").val("1");
        }
        //vision
        var vimpared=$("#vimpared").val();
        var visionnotes=$("#visionnotes").val();
        var ServiceStatus=$('#ServiceStatus').val();
         var vnss=$(".vnss").is(":checked");
         if((vimpared == "" || vimpared == null) && (visionnotes == "" || visionnotes == " ") &&(vnss == false) )
        {
           $("#Visionns").val("0");
        }else 
        {
           $("#Visionns").val("1");
        }
        //Hearing 
        var himpared=$("#himpared").val();
        var hearingnotes=$("#hearingnotes").val();
        var hnss=$(".hnss").is(":checked");
            if((himpared == "" || himpared == null) && (hearingnotes == "" || hearingnotes == " ") && (hnss == false))
        {
           $("#Hearingns").val("0");
        }else 
        {
           $("#Hearingns").val("1");
        }
        //Respiratory
        var SOB=$("#SOB").val();
        var SPO2=$("#SPO2").val();
        var respiratorynotes=$("#respiratorynotes").val();
        var Cough=$("#Cough").val();
        var Nebulization=$("#Nebulization").val();
        var Tracheostomy=$("#Tracheostomy").val();
        var CPAP_BIPAP=$("#CPAP_BIPAP").val();
        var ICD=$("#ICD").val();
        var central_cynaosis=$("#central_cynaosis").val();
        var nulll=  $("input[value='null']").is(":checked");
        var TB=  $("input[value='TB']").is(":checked");
        var Asthama=  $("input[value='Asthama']").is(":checked");
        var COPD=  $("input[value='COPD']").is(":checked");
        var Pnemonia=  $("input[value='Pnemonia']").is(":checked");
        var rnss=$(".rnss").is(":checked");
         if((SOB == "" || SOB == " " ) &&  (SPO2 == " ") && (respiratorynotes == "" || respiratorynotes == " ") && (Cough == "" || Cough == null) && (Nebulization == "" || Nebulization == null) && (Tracheostomy == "" || Tracheostomy == null) && (CPAP_BIPAP == "" || CPAP_BIPAP == null)  && (ICD == "" || ICD == null) && (central_cynaosis == "" || central_cynaosis == null) && (nulll == false) && (TB == false) && (Asthama == false) && (COPD == false) && (Pnemonia == false) &&(rnss==false))
        {
          $("#Respiratoryy").val("0");

        }else 
        {
          $("#Respiratoryy").val("1");
        }

        //Position
        var suspine=  $("input[value='Supine']").is(":checked");
        var fowlers= $("input[value='Fowlers']").is(":checked");
        var rl = $("input[value='Right_Lateral']").is(":checked");
        var ll= $("input[value='Left_Lateral']").is(":checked");
        var ServiceStatus=$('#ServiceStatus').val();
        var positionnotess= $("#positionnotes").val();
        var pnss=$(".pnss").is(":checked");
         if((suspine == false) && (fowlers ==false) && (rl==false) && (ll == false) && (positionnotess == "" || positionnotess == " ") && (pnss ==false))
        {
           $("#Positionn").val("0");

        }else 
        {
             $("#Positionn").val("1");
        }
        //Circulatory
         var ChestPain= $("#ChestPain").val();
        var PeripheralCyanosis= $("#PeripheralCyanosis").val();
         var cnss=$(".cnss").is(":checked");
        var JugularVein= $("#JugularVein").val();
        var SurgeryHistory= $("#SurgeryHistory").val();
        var circulatorynotes= $("#circulatorynotes").val();
        var DM=$("input[value='DM']").is(":checked");
        var IHD=$("input[value='IHD']").is(":checked");
        var HTN=$("input[value='HTN']").is(":checked");
        var CAD=$("input[value='CAD']").is(":checked");
        var CHF=$("input[value='CHF']").is(":checked");

        if((ChestPain == "" || ChestPain == " " ) && (PeripheralCyanosis == "" || PeripheralCyanosis == null) && (JugularVein == "" || JugularVein == null) && (SurgeryHistory == "" || SurgeryHistory == " ") && (circulatorynotes == "" || circulatorynotes == " ") && (cnss == false)&& (DM ==false) && (IHD == false) && (HTN ==false) && (CAD == false) && (CHF ==false))
        {
             $("#Circulatoryy").val("0");
        }else 
        {
             $("#Circulatoryy").val("1");
        }
        //Denture
        var Lower= $("#Lower").val();
         var dnss=$(".dnss").is(":checked");
        var Upper= $("#Upper").val();
        var Cleaning= $("#Cleaning").val();
        var denturenotes= $("#denturenotes").val();
         if((Lower == "" || Lower == null) && (Upper == "" || Upper == null) && (Cleaning == "" || Cleaning ==null) && (denturenotes == "" || denturenotes == " ") && (dnss ==false) )
        {
             $("#Dentureee").val("0");
        }else 
        {
             $("#Dentureee").val("1");
        }
        //Nutrtion

        var Type= $("#Type").val();
        var Intact= $("#Intact").val();
        var nutritionnotes= $("#nutritionnotes").val();
        var Diet= $("#Diet").val();
        var feedingtype= $("#feedingtype").val();
        var diettnotes= $("#diettnotes").val();
        var nnss=$(".nnss").is(":checked");

        if((Type == "" || Type == null) && (Intact == "" || Intact == null) && (Diet == "" || Diet == null)  && (feedingtype == "" || feedingtype == null) && (nutritionnotes == "" || nutritionnotes == " ") && (diettnotes == "" || diettnotes == " ") &&(nnss==false))
        { 
             $("#Nutritionnn").val("0");
        }else 
        {
             $("#Nutritionnn").val("1");
        }
        //Abdomen
        var Shape= $("#Shape").val();
        var Palpation= $("#Palpation").val();
        var AusculationofBS= $("#AusculationofBS").val();
        var Percussion= $("#Percussion").val();
        var Ileostomy= $("#Ileostomy").val();
        var Colostomy= $("#ColostomyFunctioning").val();
        var abdomennotess= $("#abdomennotess").val();
         var adnss=$(".adnss").is(":checked");
         if((Shape == "" || Shape == null) && (Palpation == "" || Palpation == null) && (AusculationofBS == "" || AusculationofBS == null)  && (Percussion == "" || Percussion == " " ) && (Ileostomy == "" || Ileostomy == null) && (Colostomy == "" || Colostomy == null) && (abdomennotess == "" || diettnotes == " ") && (adnss==false ))
        {
          $("#abdomenn").val("0");

        }else 
        {
          $("#abdomenn").val("1");
        }
        //Elimination
        var UrinaryContinent= $("#UrinaryContinent").val();
         var enss=$(".enss").is(":checked");
        var urinarynotes= $("#urinarynotes").val();
        var FecalType= $("#FecalType").val();
        var fecalnotes= $("#fecalnotes").val();
         if((UrinaryContinent == "" || UrinaryContinent == null) && (FecalType == "" || FecalType == null) && (urinarynotes == "" || urinarynotes == " ") && (fecalnotes == "" || fecalnotes == " ") &&(enss==false))
        {
               $("#eliminationnn").val("0");
        }else 
        {
               $("#eliminationnn").val("1");
        }
        //Mobility
        var Independentofmobility= $("#Independentofmobility").val();
         var mnss=$(".mnss").is(":checked");
        var mobilitynotes= $("#mobilitynotes").val();

       if((Independentofmobility == "" || Independentofmobility == null)   && (mobilitynotes == "" || mobilitynotes == " ") && (mnss ==false))
        {
          $("#Mobilityyy").val("0");
        }else 
        {
          $("#Mobilityyy").val("1");
        }
        var communication=$("#CommunicationNs").val();
        var MemoryIntactsNs=$("#MemoryIntactsNs").val();
        var Visionns=$("#Visionns").val();
        var Hearingns=$("#Hearingns").val();
        var Respiratoryy=$("#Respiratoryy").val();
        var Circulatoryy=$("#Circulatoryy").val();
        var Dentureee=$("#Dentureee").val();
        var Nutritionnn=$("#Nutritionnn").val();
        var abdomenn=$("#abdomenn").val();
        var eliminationnn=$("#eliminationnn").val();
        var Mobilityyy=$("#Mobilityyy").val();
        var Positionn=$("#Positionn").val();
        var NervousSystemm=$("#NervousSystemm").val();
        if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm ==1 && ($("#BP").val() != "") && ($("#Temperature").val() != "") && ($("#RR").val() != "") && ($("#Pulse").val() != ""))
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }else 
              {
                $("#submit").prop("disabled",true);
              }
              

    }
    else
    {
      $("#submit").prop("disabled",false);
      $("#followupdate").hide();
       if(value == "Dropped")
      {
        $("#reason").show();
         $("#submit").prop("disabled",false);
      }
      else
      {
         $("#reason").hide();
          $("#submit").prop("disabled",false);
      }
         $(".asterisk").css("display","none");
         $(".asterisktemp").css("display","none");
         $(".asteriskpulse").css("display","none");
    }
});

$(".mandatoryy").blur(function(){
        var serivicestatus = $("#ServiceStatus").val();
        var bp=$("#BP").val();
        var Temperature=$("#Temperature").val();
        var RR=$("#RR").val();
        var Pulse=$("#Pulse").val();
        var communication=$("#CommunicationNs").val();
        var MemoryIntactsNs=$("#MemoryIntactsNs").val();
        var Visionns=$("#Visionns").val();
        var Hearingns=$("#Hearingns").val();
        var Respiratoryy=$("#Respiratoryy").val();
        var Circulatoryy=$("#Circulatoryy").val();
        var Dentureee=$("#Dentureee").val();
        var Nutritionnn=$("#Nutritionnn").val();
        var abdomenn=$("#abdomenn").val();
        var eliminationnn=$("#eliminationnn").val();
        var Mobilityyy=$("#Mobilityyy").val();
        var Positionn=$("#Positionn").val();
        var NervousSystemm=$("#NervousSystemm").val();
   if(serivicestatus == "Converted")
   {
        if(bp != '' && RR != '' && Temperature != '' && Pulse != '' && communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm ==1  )
        {
            //console.log("nonempty");
            $("#submit").prop('disabled',false);
        }
        else
        {
              //console.log("empty1");
              $("#submit").prop('disabled',true);
        }
      }
     else
    {
         $("#submit").prop('disabled',false);
    }



  });

  var value=$('#vimpared').val();
     if(value == "Impared")
    {
      $("#visiontype").show();
    }else
    {
      $("#visiontype").hide();
    }



    $("#vimpared").change(function(){
    var value= $("#vimpared").val();
    if(value == "Impared")
    {
      $("#visiontype").show();
    }else
    {
      $("#visiontype").hide();
    }
});

    var value=$('#himpared').val();
     if(value == "Impared")
    {
      $("#heartype").show();
    }else
    {
      $("#heartype").hide();
    }



    $("#himpared").change(function(){
    var value= $("#himpared").val();
    if(value == "Impared")
    {
      $("#heartype").show();
    }else
    {
      $("#heartype").hide();
    }
});


   // BMI
    $("#Weight").blur(function(){
    var Weight =$('#Weight').val();
     var Heightinfeet =$('#Height').val();
     var Height_Measured_In =$('#Height_Measured_In').val();
     if(Weight == ""  && Heightinfeet =="" && Height_Measured_In=="")
     {
        $("#BMI").css("color","red");
        var warning ="";
        $("#BMI").attr("value", warning);

     }else if(Weight == ""  && Heightinfeet !="")
     {
        $("#BMI").css("color","red");
        var warning ="Please Enter Weight";
        $("#BMI").attr("value", warning);
     }
     else if( Weight == "" && Heightinfeet=="")
     {
          $("#BMI").css("color","red");
    var warning ="Please Enter Weight & Height";
    $("#BMI").attr("value", warning);
     }
     else if(Heightinfeet == "" && Height_Measured_In == "")
     {
      $("#BMI").css("color","red");
        var warning ="Please Enter Height & Measuring Scale.";
        $("#BMI").attr("value", warning);
     }
     else if(Height_Measured_In == "")
     {
        $("#BMI").css("color","red");
        var warning ="Please Select Measuring Scale for BMI";
        $("#BMI").attr("value", warning);
     }
     else if(Heightinfeet== "")
     {
       $("#BMI").css("color","red");
        var warning ="Please Enter Height";
        $("#BMI").attr("value", warning);
     }

     else
     {
          if($('#Height_Measured_In').val()=="ft")
        {

        var Heightincm = Heightinfeet / 0.032808;
        }
        else
        {

         var Heightincm = Heightinfeet;
        }

      var TwiceHeightincm = Heightincm * Heightincm;
      var Weightinkg =$('#Weight').val();

      var resofdiv =  Weightinkg / TwiceHeightincm;
      var res = resofdiv * 10000;

       var res=res +" kg/ (m)2 ";
       $("#BMI").css("color","#333");
      $("#BMI").attr("value", res);
     }
  });
  $("#Height").blur(function(){
     var Heightinfeet =$('#Height').val();
    var Weight =$('#Weight').val();
    var Height_Measured_In =$('#Height_Measured_In').val();
      if(Weight == ""  && Heightinfeet =="" && Height_Measured_In=="")
     {
        $("#BMI").css("color","red");
        var warning ="";
        $("#BMI").attr("value", warning);
     }else if( Heightinfeet == "" && Weight == "" )
    {
        $("#BMI").css("color","red");
      var warning ="Please Enter Weight & Height";
      $("#BMI").attr("value", warning);
    }
    else if(Heightinfeet == "" && Weight == ""  && Height_Measured_In !="")
    {

        $("#BMI").css("color","red");
      var warning ="Please Enter Weight & Height";
      $("#BMI").attr("value", warning);
    }
    else if(Height_Measured_In == "")
  {

    $("#BMI").css("color","red");
    var warning ="Please Select Measuring Scale for BMI.";
    $("#BMI").attr("value", warning);

  }
  else if(Weight == "")
  {
    $("#BMI").css("color","red");
    var warningforweight ="Please Enter Weight";
    $("#BMI").attr("value", warningforweight);
  }
  else if(Heightinfeet == "")
  {
    $("#BMI").css("color","red");
    var warningforweight ="Please Enter Height";
    $("#BMI").attr("value", warningforweight);
  }
  else
  {
      if($('#Height_Measured_In').val()=="ft")
    {

       var Heightincm = Heightinfeet / 0.032808;
    }
    else
    {

    var Heightincm = Heightinfeet;
    }

      var TwiceHeightincm = Heightincm * Heightincm;
      var Weightinkg =$('#Weight').val();

      var resofdiv =  Weightinkg / TwiceHeightincm;
      var res = resofdiv * 10000;

       var res=res +" kg/ (m)2 ";
       $("#BMI").css("color","#333");
      $("#BMI").attr("value", res);


  }




// if($('#Height_Measured_In').val()=="ft")
// {

//    var Heightincm = Heightinfeet / 0.032808;
// }
// else
// {

//    var Heightincm = Heightinfeet;
// }

//       var TwiceHeightincm = Heightincm * Heightincm;
//       var Weightinkg =$('#Weight').val();

//       var resofdiv =  Weightinkg / TwiceHeightincm;
//       var res = resofdiv * 10000;

//        var res=res +" kg/ (m)2 ";
//       $("#BMI").attr("value", res);

//        document.getElementById('Result').innerHTML=res +" kg/ m<sup>2</sup> ";
  });
// Weight

$('#Height_Measured_In').change(function(){

  var Heightinfeet =$('#Height').val();
  var Weight =$('#Weight').val();
  var Height_Measured_In =$('#Height_Measured_In').val();
  if( Height_Measured_In == "" && Weight == "" && Heightinfeet == "")
  {
      $("#BMI").css("color","red");
    var warning ="";
    $("#BMI").attr("value", warning);
  }else if(Height_Measured_In == "")
  {

    $("#BMI").css("color","red");
    var warning ="Please Select Measuring Scale.";
    $("#BMI").attr("value", warning);

  }
  else if(Weight == "")
  {
    $("#BMI").css("color","red");
    var warningforweight ="Please Enter Weight";
    $("#BMI").attr("value", warningforweight);
  }
  else if(Heightinfeet == "")
  {
    $("#BMI").css("color","red");
    var warningforweight ="Please Enter Height";
    $("#BMI").attr("value", warningforweight);

  }
  else
  {
      if($('#Height_Measured_In').val()=="ft")
    {

       var Heightincm = Heightinfeet / 0.032808;
    }
    else
    {

    var Heightincm = Heightinfeet;
    }

      var TwiceHeightincm = Heightincm * Heightincm;
      var Weightinkg =$('#Weight').val();

      var resofdiv =  Weightinkg / TwiceHeightincm;
      var res = resofdiv * 10000;

       var res=res +" kg/ (m)2 ";
       $("#BMI").css("color","#333");
      $("#BMI").attr("value", res);


  }
});
    $("#FecalType").change(function(){
        var selectedvalue=$("#FecalType").val();
      if(selectedvalue == "Continent")
      {

        $("#IncontinentOccasionallydiv").hide();
        $("#IncontinentAlwaysdiv").hide();
      }
      else
      {
        $("#IncontinentOccasionallydiv").show();
        $("#IncontinentAlwaysdiv").show();
      }

    });




       //code to make follow up date required

var statuscheck=$("#ServiceStatus").val();
             if(statuscheck=="Deferred")
              {
                $('.followupdaterequired').attr("required","required");
                var checkfollowupdate=$(".followupdaterequired").val();

                if(checkfollowupdate)
                {
                   $('#submit').show();
                    $(".cautiontoenterfollowup").css("display","none");
                }
                else
                {
                  $('#submit').hide();
                   $(".cautiontoenterfollowup").css("display","block");
                }
              }
              else
              {
                 if(statuscheck=="Dropped")
                {
                       $('.dropreasonrequired').attr("required","required");
                        var checkdropreason=$(".dropreasonrequired").val();
                        $('#showassignto').hide();

                        if(checkdropreason)
                        {
                          $('#submit').show();
                          $(".cautiontoenterdropreason").css("display","none");
                        }
                        else
                        {
                          $('#submit').hide();
                          $(".cautiontoenterdropreason").css("display","block");
                        }
                }
                else
                {

                        if(statuscheck=="Converted" || statuscheck=="In Progress")
                        {
                          $(".cautiontoenterfollowup").css("display","none");
                          $('#reason').hide();
                          $('#followupdate').hide();
                          $('#showassignto').show();
                          $('.followupdaterequired').removeAttr("required","required");

                          $(".cautiontoenterdropreason").css("display","none");
                          $('.dropreasonrequired').removeAttr("required","required");

                        }

                        $('.followupdaterequired').removeAttr("required","required");
                        $('.dropreasonrequired').removeAttr("required","required");

                         $('#submit').show();
                }
              }

    
        $("#ServiceStatus").change(function(){ 
             var statuscheck=$("#ServiceStatus").val();
             if(statuscheck=="Deferred")
              {

                $('.followupdaterequired').attr("required","required");
                var checkfollowupdate=$(".followupdaterequired").val();

                if(checkfollowupdate)
                {
                  $('#submit').show();
                }
                else
                {
                  $('#submit').hide();
                  $(".cautiontoenterfollowup").css("display","block");
                }
              }
              else
              {
                if(statuscheck=="Dropped")
                {
                       $('.dropreasonrequired').attr("required","required");
                        var checkdropreason=$(".dropreasonrequired").val();
                        $('#showassignto').hide();

                        if(checkdropreason)
                        {
                          $('#submit').show();
                        }
                        else
                        {
                          $('#submit').hide();
                          $(".cautiontoenterdropreason").css("display","block");
                        }
                }
                else
                {
                      if(statuscheck=="Converted" || statuscheck=="In Progress")
                      {
                        $(".cautiontoenterfollowup").css("display","none");
                        $(".cautiontoenterdropreason").css("display","none");
                        $('#reason').hide();
                        $('#followupdate').hide();
                        $('#showassignto').show();
                        $('.followupdaterequired').removeAttr("required","required");
                        $('.dropreasonrequired').removeAttr("required","required");
      
                      }
                       $('.followupdaterequired').removeAttr("required","required");
                       $('.dropreasonrequired').removeAttr("required","required");
                      $('#submit').show();
                }
               
              }
        });

        $(".followupdaterequired").blur(function(){ 
             var checkfollowupdate=$(".followupdaterequired").val();

              if(checkfollowupdate)
              {
                $('#submit').show();
                $(".cautiontoenterfollowup").css("display","none");
              }
              else
              {
                $('#submit').hide();
                $(".cautiontoenterfollowup").css("display","block");
              }

             
        });

        $(".dropreasonrequired").blur(function(){ 
             var checkdrop=$(".dropreasonrequired").val();

              if(checkdrop)
              {
                $('#submit').show();
                $(".cautiontoenterdropreason").css("display","none");
              }
              else
              {
                $('#submit').hide();
                $(".cautiontoenterdropreason").css("display","block");
              }

             
        });
  


});
</script>


<script>
$.noConflict();
jQuery(document).ready(function($){
   $('#BFTime').mdtimepicker();
   $('#LunchTime').mdtimepicker();
   $('#SnacksTime').mdtimepicker();
   $('#DinnerTime').mdtimepicker();




});
</script>
<script type="text/javascript">
  $.noConflict();
jQuery(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({ animation:true,  html:true});
    userr=0;
  $("#usersetting").click(function(){

      userr=userr+1;
        if(userr % 2 == 0)
        {

           $("#usersetting").popover('hide');

        }else
        {
           $("#usersetting").popover('show');

          //console.log(sidenvavv);
        }
  });



$("#bodyy").click(function(){
 userr=userr+1;
      if(userr % 2 == 0)
        {



        }else
        {
           userr=userr+1;

          //console.log(sidenvavv);
        }

});



//GCS
var eyeopningonload= "<?php echo $Eyeopening?>";
var verbalresponseonload="<?php echo $verbalresponse ?>";
var motorresponse="<?php echo $motorresponse?>";
var totalscoregcsonload="<?php echo $totalscroe;?>";

//console.log(totalscoregcsonload);
if(eyeopningonload == "" && verbalresponseonload=="" && motorresponse== "")
{

}
else
{

   if(eyeopningonload == "Spontaneously")
   {
        var valuetobeplaced="Spontaneously 4";
        $("#eyeopeningvalue").val("4");
        $("#Eyeopening").val(valuetobeplaced);
   }
   else if(eyeopningonload == "To speech")
   {
      var valuetobeplaced="To speech 3";
      $("#eyeopeningvalue").val("3");
      $("#Eyeopening").val(valuetobeplaced);
   }
   else if(eyeopningonload == "To pain")
   {
     var valuetobeplaced="To pain 2";
    $("#eyeopeningvalue").val("2");
    $("#Eyeopening").val(valuetobeplaced);
   }else if(eyeopningonload == "No response")
   {
     var valuetobeplaced="No response 1";
    $("#eyeopeningvalue").val("1");
    $("#Eyeopening").val(valuetobeplaced);
   }


   if(verbalresponseonload == "Orientated time place person")
   {
      var verbal="Orientated time place person 5";
      $("#verbalresponsevalue").val("5");
      $("#verbalresponse").val(verbal);

   }else if(verbalresponseonload == "Confused")
   {
        var verbal="Confused 4";
       $("#verbalresponsevalue").val("4");
        $("#verbalresponse").val(verbal);
   }else if(verbalresponseonload == "Inappropriate Words")
   {
        var verbal="Inappropriate Words 3";
       $("#verbalresponsevalue").val("3");
        $("#verbalresponse").val(verbal);
   }else if(verbalresponseonload == "Incomprehensible Sounds")
   {
        var verbal="Incomprehensible Sounds 2";
       $("#verbalresponsevalue").val("2");
        $("#verbalresponse").val(verbal);
   }else if(verbalresponseonload == "No responce")
   {
        var verbal="No responce 1";
       $("#verbalresponsevalue").val("1");
        $("#verbalresponse").val(verbal);
   }


   if(motorresponse == "Obey commands")
   {
      var motor="Obey commands 6";
      $("#motorresponsevalue").val("6");
      $("#motorresponse").val(motor);
   }else if(motorresponse == "moves to Localised pains")
   {
     var motor="moves to Localised pains 5";
      $("#motorresponsevalue").val("5");
      $("#motorresponse").val(motor);
   }else if(motorresponse == "flexion withdrawal from")
   {
     var motor="flexion withdrawal from 4";
      $("#motorresponsevalue").val("4");
      $("#motorresponse").val(motor);
   }else if(motorresponse == "abnormal fexion")
   {
     var motor="abnormal fexion 3";
      $("#motorresponsevalue").val("3");
      $("#motorresponse").val(motor);
   }else if(motorresponse == "abnormal edxtension")
   {
     var motor="abnormal edxtension 2";
      $("#motorresponsevalue").val("2");
      $("#motorresponse").val(motor);
   }else if(motorresponse == "No responce")
   {
     var motor="No responce 1";
      $("#motorresponsevalue").val("1");
      $("#motorresponse").val(motor);
   }

      // if(totalscoregcsonload == 15)
      // {
      //   $("#totalscoregcstoshow").css("color","#009900");
      //   $("#totalscoregcstoshow").css("font-size","16px");
      // }
      // else if(totalscoregcsonload < 15)
      // {
      //   $("#totalscoregcstoshow").css("color","red");
      //   $("#totalscoregcstoshow").css("font-size","16px");
      // }
      var Tracheostomy="<?php echo $Tracheostomy;?>";
      if(Tracheostomy == "Yes")
      {
          if( totalscoregcsonload.charAt( 0 ) === 'T' )
          totalscoregcsonload = totalscoregcsonload.slice( 1 );
          totalscoregcsonload =totalscoregcsonload;
      }

        $('#totalscoregcstoshow').attr('value',totalscoregcsonload);
        if(totalscoregcsonload >= 9 && totalscoregcsonload<=15)

      {
        $("#totalscoregcstoshow").css("color","#009900");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      else if(totalscoregcsonload >= 4 && totalscoregcsonload<=8)
      {
        $("#totalscoregcstoshow").css("color","red");
        $("#totalscoregcstoshow").css("font-size","16px");
      }else if(totalscoregcsonload <= 3)
      {
        $("#totalscoregcstoshow").css("color","orange");
        $("#totalscoregcstoshow").css("font-size","16px");
      }

}
$("#Eyeopening").change(function(){
  var value= $(this).val();
  var vaerbalresponse = $("#verbalresponsevalue").val();
  var motorresponse = $("#motorresponsevalue").val();
  var splitedresult= value.split(" ");
  var numberofeyeopening = splitedresult.pop();
  $("#eyeopeningvalue").val(numberofeyeopening);
   var eyeopeningvalue = $("#eyeopeningvalue").val();
    if(vaerbalresponse != "" && motorresponse != "")
    {
      var Result  =parseInt(eyeopeningvalue, 10) + parseInt(vaerbalresponse, 10) + parseInt(motorresponse, 10);
     if(Result >= 9 && Result<=15)
      {
        $("#totalscoregcstoshow").css("color","#009900");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      else if(Result >= 4 && Result<=8)
      {
        $("#totalscoregcstoshow").css("color","red");
        $("#totalscoregcstoshow").css("font-size","16px");
      }else if(Result <= 3)
      {
        $("#totalscoregcstoshow").css("color","orange");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      var Tracheostomy=$("#Tracheostomy").val();
      if(Tracheostomy == "Yes")
      {
        Result ="T"+Result;
      }
      $('#totalscoregcstoshow').attr('value',Result);
      // document.getElementById('totalscoregcstoshow').innerHTML=Result;
    }

});
$("#verbalresponse").change(function(){
  var value= $(this).val();
  var splitedresult= value.split(" ");
  var numberofverbalresponse = splitedresult.pop();
  $("#verbalresponsevalue").val(numberofverbalresponse);
  var eyeopeningvalue = $("#eyeopeningvalue").val();
  var motorresponse = $("#motorresponsevalue").val();
   var vaerbalresponse = $("#verbalresponsevalue").val();

   if(eyeopeningvalue != "" && motorresponse != "")
    {
      var Result  =parseInt(eyeopeningvalue, 10) + parseInt(vaerbalresponse, 10) + parseInt(motorresponse, 10);
      if(Result >= 9 && Result<=15)
      {
        $("#totalscoregcstoshow").css("color","#009900");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      else if(Result >= 4 && Result<=8)
      {
        $("#totalscoregcstoshow").css("color","red");
        $("#totalscoregcstoshow").css("font-size","16px");
      }else if(Result <= 3)
      {
        $("#totalscoregcstoshow").css("color","orange");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      var Tracheostomy=$("#Tracheostomy").val();
      if(Tracheostomy == "Yes")
      {
        Result ="T"+Result;
      }
      $('#totalscoregcstoshow').attr('value',Result);
      // document.getElementById('totalscoregcstoshow').innerHTML=Result;
    }
});
$("#motorresponse").change(function(){
  var value= $(this).val();
  var splitedresult= value.split(" ");
  var numberofmotorresponse = splitedresult.pop();
  $("#motorresponsevalue").val(numberofmotorresponse);
});
$("#motorresponse").blur(function(){
    var eyeopening = $("#eyeopeningvalue").val();
    var vaerbalresponse = $("#verbalresponsevalue").val();
    var motorresponse = $("#motorresponsevalue").val();
    // console.log(eyeopening+vaerbalresponse+motorresponse);
    var Result  =parseInt(eyeopening, 10) + parseInt(vaerbalresponse, 10) + parseInt(motorresponse, 10);
     if(Result >= 9 && Result<=15)
      {
        $("#totalscoregcstoshow").css("color","#009900");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      else if(Result >= 4 && Result<=8)
      {
        $("#totalscoregcstoshow").css("color","red");
        $("#totalscoregcstoshow").css("font-size","16px");
      }else if(Result <= 3)
      {
        $("#totalscoregcstoshow").css("color","orange");
        $("#totalscoregcstoshow").css("font-size","16px");
      }
      var Tracheostomy=$("#Tracheostomy").val();
      if(Tracheostomy == "Yes")
      {
        Result ="T"+Result;
      }
    $('#totalscoregcstoshow').attr('value',Result);
    // document.getElementById('totalscoregcstoshow').innerHTML=Result;

});

$("#Tracheostomy").change(function(){
  var value= $(this).val();
  if(value == "Yes")
  {
      var currentres=$("#totalscoregcstoshow").val();
      //console.log(currentres+"  "+"yes");
      currentres = "T"+currentres;
       $('#totalscoregcstoshow').attr('value',currentres);
  }
  else if(value == "No")
  {
      var currentres=$("#totalscoregcstoshow").val();
       //console.log(currentres+"  "+"No");
      if( currentres.charAt( 0 ) === 'T' )
      currentres = currentres.slice( 1 );
      $('#totalscoregcstoshow').attr('value',currentres);
      //console.log(currentres+"  "+"No");
  }
 });
//ICD


var valueofchange = $('#ICD').val();
  if(valueofchange == "Yes")
  {
    $("#placementoficd1").css("display","block");
  }else if(valueofchange == "No")
  {
    $("#placementoficd1").css("display","none");
  }
$("#ICD").change(function(){
  var valueofchange = $(this).val();
  if(valueofchange == "Yes")
  {
    $("#placementoficd1").css("display","block");
  }else if(valueofchange == "No")
  {
    $("#placementoficd1").css("display","none");
  }
 });
//Diet

  var valueofchange = $('#feedingtype').val();
  if(valueofchange == "Oral")
  {
    $(".eating").css("display","block");
    $("#tpnn").css("display","none");
    $("#rtfeeding").css("display","none");
    $("#pegfeeding").css("display","none");

    $("#frequency").css("display","none");
  }
  else if(valueofchange == "RT")
  {
    $("#rtfeeding").css("display","block");
    $(".eating").css("display","none");
    $("#tpnn").css("display","none");
    $("#pegfeeding").css("display","none");

    $("#frequency").css("display","block");
  }else if(valueofchange == "PEG")
  {
    $("#pegfeeding").css("display","block");
    $("#rtfeeding").css("display","none");
    $(".eating").css("display","none");
    $("#frequency").css("display","block");


    $("#tpnn").css("display","none");
  }else if(valueofchange == "TPN")
  {
     $("#frequencyofrt").css("display","none");
    $("#tpnn").css("display","block");
    $("#rtfeeding").css("display","none");
    $(".eating").css("display","none");
   $("#pegfeeding").css("display","none");
    $("#frequency").css("display","none");
  }


$("#feedingtype").change(function(){
  var valueofchange = $(this).val();
  if(valueofchange == "Oral")
  {
    $(".eating").css("display","block");
    $("#tpnn").css("display","none");
    $("#rtfeeding").css("display","none");
    $("#pegfeeding").css("display","none");

    $("#frequency").css("display","none");
  }
  else if(valueofchange == "RT")
  {
    $("#rtfeeding").css("display","block");
    $(".eating").css("display","none");
    $("#tpnn").css("display","none");
    $("#pegfeeding").css("display","none");

    $("#frequency").css("display","block");
  }else if(valueofchange == "PEG")
  {
    $("#pegfeeding").css("display","block");
    $("#rtfeeding").css("display","none");
    $(".eating").css("display","none");
    $("#frequency").css("display","block");


    $("#tpnn").css("display","none");
  }else if(valueofchange == "TPN")
  {
     $("#frequencyofrt").css("display","none");
    $("#tpnn").css("display","block");
    $("#rtfeeding").css("display","none");
    $(".eating").css("display","none");
   $("#pegfeeding").css("display","none");
    $("#frequency").css("display","none");
  }
 });

// infusion rate
$("#timeinfusion").blur(function(){
var Quantity = $("#Quantityinfusion").val();
var time =$(this).val();
if(Quantity == "")
  {
        $("#infusionratedropspermin").css("color","red");
        var warning ="Please Enter Quantity";
        $("#infusionratedropspermin").attr("value", warning);
  }
  else if(time == "")
  {
      $("#infusionratedropspermin").css("color","red");
        var warning ="Please Enter Time";
        $("#infusionratedropspermin").attr("value", warning);
  }
  else
  {
    $("#infusionratedropspermin").css("color","#333");
    var dropfactor = (Quantity * 1 )/ (time * 60);
    var dropfactorindecimal =Math.ceil(dropfactor)+"   " +"drop(s) per minute";
    $("#infusionratedropspermin").attr("value", dropfactorindecimal);
  }

  });


$("#Quantityinfusion").blur(function(){
  var Quantity = $("#Quantityinfusion").val();
  var time = $("#timeinfusion").val();

  if(Quantity == "")
  {
        $("#infusionratedropspermin").css("color","red");
        var warning ="Please Enter Quantity";
        $("#infusionratedropspermin").attr("value", warning);
  }
  else if(time == "")
  {
      $("#infusionratedropspermin").css("color","red");
        var warning ="Please Enter Time";
        $("#infusionratedropspermin").attr("value", warning);
  }
  else
  {
    $("#infusionratedropspermin").css("color","#333");
    var dropfactor = (Quantity * 1 )/ (time * 60);
    var dropfactorindecimal =Math.ceil(dropfactor)+"   "+ "drop(s) per minute";
    $("#infusionratedropspermin").attr("value", dropfactorindecimal);
  }
});
//Scrolling Top
$(".scrolll").click(function(){
    $('html, body').animate({
      scrollTop: ($('#GD').offset().top)
      },800);

});
$(".assessmentdetails").click(function(){
    $('html, body').animate({
      scrollTop: ($('#AD').offset().top)
      },800);

});

// Nebulization

$("#Nebulization").change(function(){
  var Nebulizationvalue = $(this).val();
  if(Nebulizationvalue == "Yes")
  {
    $("#NebulizationMedicineName").css("display","block");
    $(".cautiontoentermar").css("display","block");
  }
  else if(Nebulizationvalue == "No")
  {
    $("#NebulizationMedicineName").css("display","none");
    $(".cautiontoentermar").css("display","none");
  }
});


   var value= $('#UrinaryContinent').val();
  if(value == "")
  {

    $(".Needassistanceofurinary").css("display","none");
    $(".typeofurinary").css("display","none");
  }
  else
  {
    $(".Needassistanceofurinary").css("display","block");
    $(".typeofurinary").css("display","block");
  }


$("#UrinaryContinent").change(function(){
  var value= $(this).val();
  if(value == "")
  {

    $(".Needassistanceofurinary").css("display","none");
    $(".typeofurinary").css("display","none");
  }
  else
  {
    $(".Needassistanceofurinary").css("display","block");
    $(".typeofurinary").css("display","block");
  }
  });


  var value2= $("#FecalType").val();
  if(value2 == "")
  {

    $(".Needassistanceoffecal").css("display","none");
    $(".typeoffecal").css("display","none");
  }
  else
  {
    $(".Needassistanceoffecal").css("display","block");
    $(".typeoffecal").css("display","block");
  }


$("#FecalType").change(function(){
  var value= $(this).val();
  if(value == "")
  {

    $(".Needassistanceoffecal").css("display","none");
    $(".typeoffecal").css("display","none");
  }
  else
  {
    $(".Needassistanceoffecal").css("display","block");
    $(".typeoffecal").css("display","block");
  }
  });

var value3= $("#Independentofmobility").val();
  if(value3 == "")
  {

    $(".NeedAssistanceofmobility").css("display","none");
    $(".typesofmobility").css("display","none");
  }
  else if(value == "Mobile")
  {
    $(".NeedAssistanceofmobility").css("display","none");
    $(".typesofmobility").css("display","none");
  }
  else
  {
    $(".NeedAssistanceofmobility").css("display","block");
    $(".typesofmobility").css("display","block");
  }


$("#Independentofmobility").change(function(){
  var value= $(this).val();
  if(value == "")
  {

    $(".NeedAssistanceofmobility").css("display","none");
    $(".typesofmobility").css("display","none");
  }
  else if(value == "Mobile")
  {
    $(".NeedAssistanceofmobility").css("display","none");
    $(".typesofmobility").css("display","none");
  }
  else
  {
    $(".NeedAssistanceofmobility").css("display","block");
    $(".typesofmobility").css("display","block");
  }
  });
// Cough

var currentval= $("#Cough").val();
  if(currentval == "Productive")
  {
    $("#ColorOfPhlegm1").css("display","block");
  }
  else if(currentval == "Non-Productive")
  {
    $("#ColorOfPhlegm1").css("display","none");
  }
  else
  {
     $("#ColorOfPhlegm1").css("display","none");
  }

$("#Cough").change(function(){
  var currentval= $(this).val();
  if(currentval == "Productive")
  {
    $("#ColorOfPhlegm1").css("display","block");
  }
  else if(currentval == "Non-Productive")
  {
    $("#ColorOfPhlegm1").css("display","none");
  }
  else
  {
     $("#ColorOfPhlegm1").css("display","none");
  }
});

// Abdomen
  var valueofshape= $('#Shape').val();
    if(valueofshape=="")
    {
        $(".BulgeType").css("display","none");
    }
    else if(valueofshape == "Flat")
    {
      $(".BulgeType").css("display","none");
    }else if(valueofshape == "Distended")
    {
      $(".BulgeType").css("display","none");
    }else if(valueofshape == "Bulge")
    {
      $(".BulgeType").css("display","block");
    }

$("#Shape").change(function(){
  var valueofshape= $(this).val();
    if(valueofshape=="")
    {
        $(".BulgeType").css("display","none");
    }
    else if(valueofshape == "Flat")
    {
      $(".BulgeType").css("display","none");
    }else if(valueofshape == "Distended")
    {
      $(".BulgeType").css("display","none");
    }else if(valueofshape == "Bulge")
    {
      $(".BulgeType").css("display","block");
    }
  });
//onload if thier are doctors by default update doctor count
var numberofdoctors="<?php echo $numberofdoctors;?>";
//console.log(numberofdoctors);
if(numberofdoctors > 0)
{
  $("#NumberofPrescribedbydoc").val(numberofdoctors);
  var noofnewdoc=numberofdoctors;


}else
{
  var noofnewdoc=1;
}
//console.log(noofnewdoc);
var numbertoadddoctors=1;
$("#addnewdoc").click(function(){
   noofnewdoc=parseInt(noofnewdoc, 10) + parseInt(numbertoadddoctors, 10);
  //noofnewdoc =noofnewdoc +1;
      $("#NumberofPrescribedbydoc").val(noofnewdoc);
      $.get("{{ URL::to('newdoc1') }}", { noofnewdoc :noofnewdoc},function(data){
                 $('#moredoc').append(data);

          });
});
var Medicineclicks=1;
// $("#moremedicine").click(function(){
//     Medicineclicks= Medicineclicks + 1;
//     var classidtobeplaced = 1;
//         $.get("{{ URL::to('addmedicine') }}",{ classidtobeplaced :classidtobeplaced,currentvalue :Medicineclicks},function(data){
//                  $('#'+classidtobeplaced).append(data);

//           });
// });


$("#moremedicine").unbind('click').click(
  function()
    {
      Medicineclicks= Medicineclicks + 1;
     var classidtobeplaced = 1;
     $("#PrescribedbyDocfirstnomedicices").val(Medicineclicks);
      $.get("{{ URL::to('addmedicine') }}",{ classidtobeplaced :classidtobeplaced,currentvalue :Medicineclicks},function(data){
                  $('#'+classidtobeplaced).append(data);

           });
   }
);

// Nebulization Add more

var nebulizationmoremedicineclicks=1;
$("#nebulizationmoremedicine").click(function(){
    nebulizationmoremedicineclicks= nebulizationmoremedicineclicks + 1;
    $("#nebulizationnoofmedioffirstdoc").val(nebulizationmoremedicineclicks);
    var classidtobeplaced = "nebulizationdoc1";
         $.get("{{ URL::to('nebulizationaddmedicine') }}",{ classidtobeplaced :classidtobeplaced,currentvalue :nebulizationmoremedicineclicks},function(data){
                 $('#'+classidtobeplaced).append(data);

          });
});

// Add New Doctor
var nebulizationadddocclicks = 1;
$("#nebulizationaddnewdoc").click(function(){
nebulizationadddocclicks = nebulizationadddocclicks +1;
$("#NumberofNebulizationdoc").val(nebulizationadddocclicks);
     $.get("{{ URL::to('nebulozationnewdoc') }}", { noofnebulizationnewdoc :nebulizationadddocclicks},function(data){
                 $('#nebulizationmoredoc').append(data);

          });
});

// Shape
var shapevalue = $("#Shape").val();
if(shapevalue == "Flat")
{
  $(".girth").css("display","none");
}
else
{
   $(".girth").css("display","block");
}


$("#Shape").change(function(){
  var currentvalue=$(this).val();
  if(currentvalue =="Flat")
  {
    $(".girth").css("display","none");

  }
  else
  {
    $(".girth").css("display","block");
  }
  });





$(".vision").change(function(){
    var name= $(this).attr("name");

  if (!$(".vision").is(":checked")) {
    // do something if the checkbox is NOT checked
     $(".NervousSystem").prop('disabled', false);

  }
  else
  {
      var lastClass = $('#NS').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#NS").removeClass(lastClass);
      }
      $(".NervousSystem").prop('disabled', true);
      $(".NS").val(" ");
      // $("#Place").val(" ");
      // $("#Time").val(" ");
      // $("#orientationnotes").val(" ");

  }
 });
$(".respiratory").change(function(){if (!$(".respiratory").is(":checked")) {
    // do something if the checkbox is NOT checked
     $(".Respiratory").prop('disabled', false);}
  else{
      var lastClass = $('#Respiratory').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Respiratory").removeClass(lastClass);
      }
      $(".Respiratory").prop('disabled', true);}
      $(".RES").val(" ");
 });
$(".hearing").change(function(){
  if (!$(".hearing").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Hearing").prop('disabled', false);}
  else{
      var lastClass = $('#Hearing').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Hearing").removeClass(lastClass);
      }
      $(".Hearing").prop('disabled', true);}
      $(".Hearingg").val(" ");
 });
$(".circulatory").change(function(){
  if (!$(".circulatory").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Circulatory").prop('disabled', false);}
  else{
     var lastClass = $('#Circulatory').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Circulatory").removeClass(lastClass);
      }
      $(".Circulatory").prop('disabled', true);}
      $(".circulatoryy").val(" ");
 });
 $(".denture").change(function(){
  if (!$(".denture").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Denture").prop('disabled', false);}
  else{
       var lastClass = $('#Denture').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Denture").removeClass(lastClass);
      }
      $(".Denture").prop('disabled', true);}
      $(".denturee").val(" ");
 });
  $(".abdomen").change(function(){
  if (!$(".abdomen").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Abdomen").prop('disabled', false);}
  else{
      var lastClass = $('#Abdomen').attr('class').split(' ').pop();
      if(lastClass== "in"){
        $("#Abdomen").removeClass(lastClass);
      }
      $(".Abdomen").prop('disabled', true);}
      $(".abdomenn").val(" ");
 });
  $(".elimination").change(function(){
  if (!$(".elimination").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Elimination1").prop('disabled', false);}
  else{
    var lastClass = $('#Elimination').attr('class').split(' ').pop();
      if(lastClass== "in"){
        $("#Elimination").removeClass(lastClass);
      }
      $(".Elimination1").prop('disabled', true);}
       $(".eliminationn").val(" ");
 });
  $(".mobility").change(function(){
  if (!$(".mobility").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Mobility").prop('disabled', false);}
  else{
     var lastClass = $('#Mobility').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Mobility").removeClass(lastClass);
      }
      $(".Mobility").prop('disabled', true);}
      $(".mobilityy").val(" ");
 });
          $(".position").change(function(){
  if (!$(".position").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Position").prop('disabled', false);}
  else{
     var lastClass = $('#Position').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Position").removeClass(lastClass);
      }
      $(".Position").prop('disabled', true);}
      $(".Positionn").val(" ");
 });
                      $(".nutrition").change(function(){
  if (!$(".nutrition").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Nutrition").prop('disabled', false);}
  else{
     var lastClass = $('#Nutrition').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Nutrition").removeClass(lastClass);
      }
      $(".Nutrition").prop('disabled', true);}
      $(".nutritionn").val(" ");
 });

                $(".memoryintact").change(function(){
  if (!$(".memoryintact").is(":checked")) {// do something if the checkbox is NOT checked
     $(".MemoryIntact").prop('disabled', false);}
  else{
     var lastClass = $('#MemoryIntacts').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#MemoryIntacts").removeClass(lastClass);
      }
      $(".MemoryIntact").prop('disabled', true);}
      $(".MI").val(" ");
 });

                $(".communication").change(function(){
  if (!$(".communication").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Communication").prop('disabled', false);}
  else{
     var lastClass = $('#Communication').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Communication").removeClass(lastClass);
      }
      $(".Communication").prop('disabled', true);}
      $(".comm").val(" ");
 });



                $(".vision1").change(function(){
  if (!$(".vision1").is(":checked")) {// do something if the checkbox is NOT checked
     $(".Vision1").prop('disabled', false);}
  else{
     var lastClass = $('#Vision').attr('class').split(' ').pop();
      if(lastClass == "in"){
        $("#Vision").removeClass(lastClass);
      }
      $(".Vision1").prop('disabled', true);}
      $(".visionn").val(" ");
 });
//Diet Type is NBO/NPO hide Oral in Feeding type
//Onload
var Dietvalue=$("#Diet").val();

      if(Dietvalue =="NBO/NPO")
      {
        $("#oral").css("display","none");
      }
      else
      {
        $("#oral").css("display","block");
      }


//Duering Vewing
  $("#Diet").change(function(){

      var Dietvalue=$("#Diet").val();

      if(Dietvalue =="NBO/NPO")
      {
        $("#oral").css("display","none");
      }
      else
      {
        $("#oral").css("display","block");
      }
  });

var countns=0;
//Nothing Significant
$(".MI").change(function(){
    var ServiceStatus=$('#ServiceStatus').val();
    var switchval=$(".memoryintact").is(":checked");
    //console.log("changed" + "    "+switchval);
    var ShortTerm=$("#ShortTerm").val();
    var LongTerm=$("#LongTerm").val();
    var memoryintactnotes=$("#memoryintactnotes").val();
    //console.log(memoryintactnotes);
  if(ServiceStatus == "Converted")
  {


    if((ShortTerm == "" || ShortTerm == null) && (LongTerm == "" || LongTerm == null)&& (memoryintactnotes == "" || memoryintactnotes == " ") )
    {
        //console.log(" put 0");
         $("#MemoryIntactsNs").val("0");
         $("#submit").prop("disabled",true);
       
    }
    else 
    {
        
        //console.log("put 1");
       $("#MemoryIntactsNs").val("1");
       var communication=$("#CommunicationNs").val();
       var MemoryIntactsNs=$("#MemoryIntactsNs").val();
       var Visionns=$("#Visionns").val();
         var Hearingns=$("#Hearingns").val();
         var Respiratoryy=$("#Respiratoryy").val();
         var Circulatoryy=$("#Circulatoryy").val();
         var Dentureee=$("#Dentureee").val();
         var Nutritionnn=$("#Nutritionnn").val();
         var abdomenn=$("#abdomenn").val();
         var eliminationnn=$("#eliminationnn").val();
         var Mobilityyy=$("#Mobilityyy").val();
         var Positionn=$("#Positionn").val();
         var NervousSystemm=$("#NervousSystemm").val();
       if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy ==1 && Circulatoryy == 1 && Dentureee==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy==1 && Positionn==1 && NervousSystemm==1 && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
      {
        //$("#submit").prop("disabled",false);
        $("#submit").prop("disabled",false);
      }else 
      {
         $("#submit").prop("disabled",true);
      }
      
      
    }
  }else 
  {
      $("#submit").prop("disabled",false);
  }
  });

   $(".mii").change(function(){
          var switchval=$(".memoryintact").is(":checked");
        var ServiceStatus=$('#ServiceStatus').val();

        if(ServiceStatus == "Converted")
        {
          if( switchval == true)
          {
             
              $("#MemoryIntactsNs").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
                var Hearingns=$("#Hearingns").val();
                var Respiratoryy=$("#Respiratoryy").val();
                var Circulatoryy=$("#Circulatoryy").val();
                var Dentureee=$("#Dentureee").val();
                var Nutritionnn=$("#Nutritionnn").val();
                var abdomenn=$("#abdomenn").val();
                var eliminationnn=$("#eliminationnn").val();
                var Mobilityyy=$("#Mobilityyy").val();
                var Positionn=$("#Positionn").val();
                var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 &&Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                  //$("#submit").prop("disabled",false);
                  $("#submit").prop("disabled",false);
              }else 
              {
                  $("#submit").prop("disabled",true);
              }
      
          }else if( switchval == false)
          {
               $("#MemoryIntactsNs").val("0");
               $("#submit").prop("disabled",true);
          }

      }else 
      {
         $("#submit").prop("disabled",false);
      }
    });

   $(".cmnsss").change(function(){
        var Adequate = $("#AdequateforAllActivities").val();
        var unableToCommunicate = $("#unableToCommunicate").val();
        var communicationnotes = $("#communicationnotes").val();
         var communicationhval=$(".communication").is(":checked");
          var ServiceStatus=$('#ServiceStatus').val();
          var hindi=$("input[value='Hindi']").is(":checked");
          var English=$("input[value='English']").is(":checked");
          var Kannada=$("input[value='Kannada']").is(":checked");
          var Tamil=$("input[value='Tamil']").is(":checked");
          var Marathi=$("input[value='Marathi']").is(":checked");
          var Malayalam=$("input[value='Malayalam']").is(":checked");
          var Telugu=$("input[value='Telugu']").is(":checked");

      if(ServiceStatus == "Converted")
      {
        if((unableToCommunicate == "" || unableToCommunicate == null) && (Adequate == "" || Adequate == null)&& (communicationnotes == "" || communicationnotes == " ") &&(hindi ==false) && (English==false)&&(Kannada ==false)&&(Tamil==false)&&(Marathi ==false)&&(Malayalam==false)&&(Telugu==false))
        {
            //console.log(" put 0");
          


            $("#CommunicationNs").val("0");
            $("#submit").prop("disabled",true);
          
        }
        else 
        {
              $("#CommunicationNs").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn==1 && Mobilityyy == 1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                  //console.log("put 1");
                $("#submit").prop("disabled",false);
              }else 
              {
                $("#submit").prop("disabled",true);
              }
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
  });

     $(".cmns").change(function(){
          
           var communicationhval=$(".communication").is(":checked");
            var ServiceStatus=$('#ServiceStatus').val();
          if(ServiceStatus == "Converted")
        {
          if( communicationhval == true)
          {
              

                $("#CommunicationNs").val("1");
                
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
               var Hearingns=$("#Hearingns").val();
               var Respiratoryy=$("#Respiratoryy").val();
               var Circulatoryy=$("#Circulatoryy").val();
               var Dentureee=$("#Dentureee").val();
               var Nutritionnn=$("#Nutritionnn").val();
               var abdomenn=$("#abdomenn").val();
               var eliminationnn=$("#eliminationnn").val();
               var Mobilityyy=$("#Mobilityyy").val();
               var Positionn=$("#Positionn").val();
               var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }else 
              {
                $("#submit").prop("disabled",true);
              }
              
          }else if( communicationhval == false)
          {
               $("#CommunicationNs").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });

    $(".visionn").change(function(){

        var vimpared=$("#vimpared").val();
        var visionnotes=$("#visionnotes").val();
         var ServiceStatus=$('#ServiceStatus').val();
        //console.log("csdsf");
       if(ServiceStatus == "Converted")
      {
        if((vimpared == "" || vimpared == null) && (visionnotes == "" || visionnotes == " ") )
        {
            //console.log(" put 0");
       
            $("#Visionns").val("0");
            $("#submit").prop("disabled",true);
        
        }
        else 
        {
            

             //console.log("put 1");
            $("#Visionns").val("1");

              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns==1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee==1 && Nutritionnn == 1 && abdomenn==1 && eliminationnn == 1 && Mobilityyy==1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "") 
              {
          
                  $("#submit").prop("disabled",false);
              }else 
              {
                  $("#submit").prop("disabled",true);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


    $(".vnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var vnss=$(".vnss").is(":checked");
          if(ServiceStatus == "Converted")
        {
          if( vnss == true)
          {

             $("#Visionns").val("1");
            $("#visiontype").hide();
             var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn==1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
               
                $("#submit").prop("disabled",false);
              
              }else 
              {
                $("#submit").prop("disabled",true);
              }
          }else if( vnss == false)
          {
               $("#Visionns").val("0");
               $("#submit").prop("disabled",true);
          }
        }else 
      {
         $("#submit").prop("disabled",false);
      }
     });


    $(".hns").change(function(){
         var ServiceStatus=$('#ServiceStatus').val();
        var himpared=$("#himpared").val();
        var hearingnotes=$("#hearingnotes").val();
         if(ServiceStatus == "Converted")
        {
        if((himpared == "" || himpared == null) && (hearingnotes == "" || hearingnotes == " ") )
        {
            //console.log(" put 0");
       
            $("#Hearingns").val("0");
            $("#submit").prop("disabled",true);
        
        }
        else 
        {
           
             //console.log("put 1");
            $("#Hearingns").val("1");
             var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "") 
              {
                $("#submit").prop("disabled",false);
              }
            
        }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


  $(".hnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var hnss=$(".hnss").is(":checked");
        if(ServiceStatus == "Converted")
        {
          if( hnss == true)
          {
              
                $("#Hearingns").val("1");
                $("#heartype").hide();
                var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn==1 && Mobilityyy ==1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
              
              
          }else if( hnss == false)
          {
               $("#Hearingns").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });
    


    $(".rns").change(function(){
         var ServiceStatus=$('#ServiceStatus').val();
        var SOB=$("#SOB").val();
        var SPO2=$("#SPO2").val();
        var respiratorynotes=$("#respiratorynotes").val();
        var Cough=$("#Cough").val();
        var Nebulization=$("#Nebulization").val();
        var Tracheostomy=$("#Tracheostomy").val();
        var CPAP_BIPAP=$("#CPAP_BIPAP").val();
        var ICD=$("#ICD").val();
        var central_cynaosis=$("#central_cynaosis").val();
        var nulll=  $("input[value='null']").is(":checked");
        var TB=  $("input[value='TB']").is(":checked");
        var Asthama=  $("input[value='Asthama']").is(":checked");
        var COPD=  $("input[value='COPD']").is(":checked");
        var Pnemonia=  $("input[value='Pnemonia']").is(":checked");

         if(ServiceStatus == "Converted")
  {
         if((SOB == "" || SOB == " " ) &&  (SPO2 == "" || SPO2 == " ") && (respiratorynotes == "" || respiratorynotes == " ") && (Cough == "" || Cough == null) && (Nebulization == "" || Nebulization == null) && (Tracheostomy == "" || Tracheostomy == null) && (CPAP_BIPAP == "" || CPAP_BIPAP == null)  && (ICD == "" || ICD == null) && (central_cynaosis == "" || central_cynaosis == null) && (nulll == false) && (TB == false) && (Asthama == false) && (COPD == false) && (Pnemonia == false))
        {
            //console.log(" put 0");
       
           $("#Respiratoryy").val("0");
            $("#submit").prop("disabled",true);
        
        }
        else 
        {
               $("#Respiratoryy").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy ==1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn==1 && eliminationnn == 1 && Mobilityyy ==1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });

     $(".rnss").change(function(){
          //console.log("adsadsadas");
           var rnss=$(".rnss").is(":checked");
          var ServiceStatus=$('#ServiceStatus').val();
          if(ServiceStatus == "Converted")
  {
          if( rnss == true)
          {
              
                 $("#Respiratoryy").val("1");
                 $("#placementoficd1").hide();
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn==1 && eliminationnn == 1 && Mobilityyy==1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              
              }
          }else if( rnss == false)
          {
                $("#Respiratoryy").val("0");
               $("#submit").prop("disabled",true);
          }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
     });
    
     $(".cns").change(function(){
      var ServiceStatus=$('#ServiceStatus').val();
        var ChestPain= $("#ChestPain").val();
        var PeripheralCyanosis= $("#PeripheralCyanosis").val();

        var JugularVein= $("#JugularVein").val();
        var SurgeryHistory= $("#SurgeryHistory").val();
        var circulatorynotes= $("#circulatorynotes").val();
         var DM=$("input[value='DM']").is(":checked");
        var IHD=$("input[value='IHD']").is(":checked");
        var HTN=$("input[value='HTN']").is(":checked");
        var CAD=$("input[value='CAD']").is(":checked");
        var CHF=$("input[value='CHF']").is(":checked");
       if(ServiceStatus == "Converted")
  {
        if((ChestPain == "" || ChestPain == " " ) && (PeripheralCyanosis == "" || PeripheralCyanosis == null) && (JugularVein == "" || JugularVein == null) && (SurgeryHistory == "" || SurgeryHistory == " ") && (circulatorynotes == "" || circulatorynotes == " ")&& (DM ==false) && (IHD == false) && (HTN ==false) && (CAD == false) && (CHF ==false))
        {

           $("#Circulatoryy").val("0");
            $("#submit").prop("disabled",true);
        }else 
        {
               $("#Circulatoryy").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy==1&& Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });

      $(".cnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var cnss=$(".cnss").is(":checked");

  if(ServiceStatus == "Converted")
  {
          if( cnss == true)
          {
              
                 $("#Circulatoryy").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy==1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              
              }
          }else if( cnss == false)
          {
                $("#Circulatoryy").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });

         $(".dns").change(function(){
         var ServiceStatus=$('#ServiceStatus').val();
        var Lower= $("#Lower").val();

        var Upper= $("#Upper").val();
        var Cleaning= $("#Cleaning").val();
        var denturenotes= $("#denturenotes").val();

  if(ServiceStatus == "Converted")
  {
        if((Lower == "" || Lower == null) && (Upper == "" || Upper == null) && (Cleaning == "" || Cleaning ==null) && (denturenotes == "" || denturenotes == " "))
        {

           $("#Dentureee").val("0");
            $("#submit").prop("disabled",true);
        }else 
        {
               $("#Dentureee").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn ==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


           $(".dnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var dnss=$(".dnss").is(":checked");
         if(ServiceStatus == "Converted")
  {
          if( dnss == true)
          {
              
                 $("#Dentureee").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn==1 && Mobilityyy == 1 && Positionn==1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              
              }
          }else if( dnss == false)
          {
                $("#Dentureee").val("0");
               $("#submit").prop("disabled",true);
          }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
     });




         $(".nns").change(function(){
        
        var Type= $("#Type").val();
         var ServiceStatus=$('#ServiceStatus').val();
        var Intact= $("#Intact").val();
        var nutritionnotes= $("#nutritionnotes").val();
        var Diet= $("#Diet").val();
        var feedingtype= $("#feedingtype").val();
        var diettnotes= $("#diettnotes").val();

  if(ServiceStatus == "Converted")
  {
        if((Type == "" || Type == null) && (Intact == "" || Intact == null) && (Diet == "" || Diet == null)  && (feedingtype == "" || feedingtype == null) && (nutritionnotes == "" || nutritionnotes == " ") && (diettnotes == "" || diettnotes == " "))
        {

           $("#Nutritionnn").val("0");
            $("#submit").prop("disabled",true);
        }else 
        {
               $("#Nutritionnn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn==1 && Mobilityyy==1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


           $(".nnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var nnss=$(".nnss").is(":checked");


  if(ServiceStatus == "Converted")
  {
          if( nnss == true)
          {
              
              $("#Nutritionnn").val("1");
              $("#breakfast").hide();
              $("#lunchtime").hide();
              $("#sanckstime").hide();
              $("#dinnertime").hide();
              $("#rtfeeding").hide();
              $("#pegfeeding").hide();
              $("#frequency").hide();
              $("#tpnn").hide();
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if( nnss == false)
          {
                $("#Nutritionnn").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });




         $(".adns").change(function(){
        
        var Shape= $("#Shape").val();
         var ServiceStatus=$('#ServiceStatus').val();
        var Palpation= $("#Palpation").val();
        var AusculationofBS= $("#AusculationofBS").val();
        var Percussion= $("#Percussion").val();
        var Ileostomy= $("#Ileostomy").val();
        var Colostomy= $("#ColostomyFunctioning").val();
        var abdomennotess= $("#abdomennotess").val();
          if((Shape == "" || Shape ==null) && (Palpation == "" || Palpation==null) && (AusculationofBS==""|| AusculationofBS ==null)&& (Percussion == "" || Percussion== " ") && (Ileostomy =="" || Ileostomy==null) && (Colostomy == "" || Colostomy==null) && (abdomennotess=="" || abdomennotess==" ")) 
          {
            console.log("empty");
            $("#abdomenn").val("0");
               $("#submit").prop("disabled",true);
          }else 
          {
            console.log("non empty");
            $("#abdomenn").val("1");
            var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          
          }
  
    });


              $(".adnss").change(function(){
          //console.log("adsadsadas");
           var adnss=$(".adnss").is(":checked");
            var ServiceStatus=$('#ServiceStatus').val();
        if(ServiceStatus == "Converted")
  {
          if( adnss == true)
          {
              
                 $("#abdomenn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if( adnss == false)
          {
                $("#abdomenn").val("0");
               $("#submit").prop("disabled",true);
          }
        }else 
      {
         $("#submit").prop("disabled",false);
      }
     });


  $(".ens").change(function(){
           var ServiceStatus=$('#ServiceStatus').val();
        var UrinaryContinent= $("#UrinaryContinent").val();

        var urinarynotes= $("#urinarynotes").val();
        var FecalType= $("#FecalType").val();
        var fecalnotes= $("#fecalnotes").val();
  if(ServiceStatus == "Converted")
  {
       if((UrinaryContinent == "" || UrinaryContinent == null) && (FecalType == "" || FecalType == null) && (urinarynotes == "" || urinarynotes == " ") && (fecalnotes == "" || fecalnotes == " ") )
        {
          //console.log("empty");
           $("#eliminationnn").val("0");
            $("#submit").prop("disabled",true);
         }
        else 
        {
               $("#eliminationnn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy ==1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


          $(".enss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var enss=$(".enss").is(":checked");
         if(ServiceStatus == "Converted")
  {
          if( enss == true)
          {  $(".typeofurinary").css("display","none");
              $(".Needassistanceofurinary").css("display","none");
              $(".typeoffecal").css("display","none");
              $(".Needassistanceoffecal").css("display","none");
                 $("#eliminationnn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if( enss == false)
          {

          
                $("#eliminationnn").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });

        $("#Independentofmobility").blur(function(){
            
        var ServiceStatus=$('#ServiceStatus').val();
        var Independentofmobility= $("#Independentofmobility").val();

        var mobilitynotes= $("#mobilitynotes").val();
       //console.log(Independentofmobility);
  if(ServiceStatus == "Converted")
  {
       if((Independentofmobility == "" || Independentofmobility == null)   && (mobilitynotes == "" || mobilitynotes == " ") )
        {
          //console.log("empty");
           $("#Mobilityyy").val("0");
            $("#submit").prop("disabled",true);
         }
        else 
        {
               $("#Mobilityyy").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn ==1 && NervousSystemm ==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")  
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
        

           });
      $(".mns").change(function(){
         var ServiceStatus=$('#ServiceStatus').val();
        var Independentofmobility= $("#Independentofmobility").val();

        var mobilitynotes= $("#mobilitynotes").val();
       //console.log(Independentofmobility);
  if(ServiceStatus == "Converted")
  {
       if((Independentofmobility == "" || Independentofmobility == null)   && (mobilitynotes == "" || mobilitynotes == " ") )
        {
          //console.log("empty");
           $("#Mobilityyy").val("0");
            $("#submit").prop("disabled",true);
         }
        else 
        {
               $("#Mobilityyy").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn ==1 && NervousSystemm ==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "") 
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });

       $(".mnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();
           var mnss=$(".mnss").is(":checked");
        if(ServiceStatus == "Converted")
  {
          if( mnss == true)
          {
              
                 $("#Mobilityyy").val("1");
                 $(".NeedAssistanceofmobility").hide();
                 $(".typesofmobility").hide();
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm ==1 && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if( mnss == false)
          {
                $("#Mobilityyy").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     });


$(".multiselectt").change(function(){
      var suspine=  $("input[value='Supine']").is(":checked");
       var fowlers= $("input[value='Fowlers']").is(":checked");
       var rl = $("input[value='Right_Lateral']").is(":checked");
      var ll= $("input[value='Left_Lateral']").is(":checked");
       var ServiceStatus=$('#ServiceStatus').val();
      var positionnotess= $("#positionnotes").val();

  if(ServiceStatus == "Converted")
  {
      if((suspine == false) && (fowlers ==false) && (rl==false) && (ll == false) && (positionnotess == "" || positionnotess == " "))
      {
            //console.log(" disenable");
            $("#Positionn").val("0");
            $("#submit").prop("disabled",true);
      }else 
      {
              //console.log("has vale enable");
              $("#Positionn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm ==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
      }
  }else 
      {
         $("#submit").prop("disabled",false);
      }
}); 
   

       $(".pnss").change(function(){
          //console.log("adsadsadas");
           var ServiceStatus=$('#ServiceStatus').val();

           var pnss=$(".pnss").is(":checked");
        if(ServiceStatus == "Converted")
  {
          if( pnss == true)
          {
              
                 $("#Positionn").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if( pnss == false)
          {
                $("#Positionn").val("0");
               $("#submit").prop("disabled",true);
          }
      }else 
      {
         $("#submit").prop("disabled",false);
      }
     }); 


        $(".nsns").change(function(){
        
        var Person= $("#Person").val();
         var ServiceStatus=$('#ServiceStatus').val();
        var Place= $("#Place").val();
       
        var Time= $("#Time").val();
        var orientationnotes= $("#orientationnotes").val();
        var Eyeopening= $("#Eyeopening").val();
        var verbalresponse= $("#verbalresponse").val();
        var motorresponse= $("#motorresponse").val();
       if(ServiceStatus == "Converted")
  {
       if((Person == "" || Person == null) && (Place == "" || Place == null)  && (Time == "" || Time == null) && (orientationnotes == "" || orientationnotes == " ") && (Eyeopening == "" || Eyeopening == null) && (verbalresponse == "" || verbalresponse == null) && (motorresponse == "" || motorresponse == null))
        {
            //console.log("empty");
           $("#NervousSystemm").val("0");
            $("#submit").prop("disabled",true);
         }
        else 
        {
               $("#NervousSystemm").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns ==1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee ==1 && Nutritionnn == 1 && abdomenn == 1 && eliminationnn == 1 && Mobilityyy == 1 && Positionn ==1 && NervousSystemm ==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                $("#submit").prop("disabled",false);
              }
            
        }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
    });


       $(".nsnss").change(function(){
          //console.log("adsadsadas");
          var ServiceStatus=$('#ServiceStatus').val();
           var nsnss=$(".nsnss").is(":checked");
        if(ServiceStatus == "Converted")
  {
          if( nsnss == true)
          {
              
                 $("#NervousSystemm").val("1");
              var communication=$("#CommunicationNs").val();
              var MemoryIntactsNs=$("#MemoryIntactsNs").val();
              var Visionns=$("#Visionns").val();
              var Hearingns=$("#Hearingns").val();
              var Respiratoryy=$("#Respiratoryy").val();
              var Circulatoryy=$("#Circulatoryy").val();
              var Dentureee=$("#Dentureee").val();
              var Nutritionnn=$("#Nutritionnn").val();
              var abdomenn=$("#abdomenn").val();
              var eliminationnn=$("#eliminationnn").val();
              var Mobilityyy=$("#Mobilityyy").val();
              var Positionn=$("#Positionn").val();
              var NervousSystemm=$("#NervousSystemm").val();
               if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm==1  && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }
          }else if(nsnss == false)
          {
                $("#NervousSystemm").val("0");
               $("#submit").prop("disabled",true);
          }
    }else 
      {
         $("#submit").prop("disabled",false);
      }
     }); 
  
  //Onload  Service status
    var value=$('#ServiceStatus').val();
     if(value == "Deferred")
    {

      $("#followupdate").show();
      $("#reason").hide();
      $('.followupdaterequired').attr("required","required");
                
                var checkfollowupdate=$(".followupdaterequired").val();

                if(checkfollowupdate)
                {
                   $('#submit').show();
                    $(".cautiontoenterfollowup").css("display","none");
                }
                else
                {
                  
                  $('#submit').hide();
                   $(".cautiontoenterfollowup").css("display","block");
                }

                $(".asterisk").css("display","none");
                $(".asteriskpulse").css("display","none");
                $(".asterisktemp").css("display","none");
    }else if(value == "Converted")
    {
          $(".asterisk").css("display","block");
          $(".asterisktemp").css("display","block");
          $(".asteriskpulse").css("display","block");
          //Nervous system
          var Person= $("#Person").val();
          var ServiceStatus=$('#ServiceStatus').val();
          var Place= $("#Place").val();
          var Time= $("#Time").val();
          var orientationnotes= $("#orientationnotes").val();
          var Eyeopening= $("#Eyeopening").val();
          var verbalresponse= $("#verbalresponse").val();
          var motorresponse= $("#motorresponse").val();
          var nsnss=$(".nsnss").is(":checked");
         if((Person == "" || Person == null) && (Place == "" || Place == null)  && (Time == "" || Time == null) && (orientationnotes == "" || orientationnotes == " ") && (Eyeopening == "" || Eyeopening == null) && (verbalresponse == "" || verbalresponse == null) && (motorresponse == "" || motorresponse == null) && nsnss== false)
        {
              $("#NervousSystemm").val("0");
        }else 
        {
              $("#NervousSystemm").val("1");
        }
        //Memory Intacts
        var ShortTerm=$("#ShortTerm").val();
        var LongTerm=$("#LongTerm").val();
        var memoryintactnotes=$("#memoryintactnotes").val();
        var switchval=$(".memoryintact").is(":checked");
        if((ShortTerm == "" || ShortTerm == null) && (LongTerm == "" || LongTerm == null)&& (memoryintactnotes == "" || memoryintactnotes == " ") && (switchval == false) )
        {
             $("#MemoryIntactsNs").val("0 ");
        }else 
        {
           $("#MemoryIntactsNs").val("1");
        }
        //Communication
        var Adequate = $("#AdequateforAllActivities").val();
        var unableToCommunicate = $("#unableToCommunicate").val();
        var communicationnotes = $("#communicationnotes").val();
        var communicationhval=$(".communication").is(":checked");
        var ServiceStatus=$('#ServiceStatus').val();
         if((unableToCommunicate == "" || unableToCommunicate == null) && (Adequate == "" || Adequate == null)&& (communicationnotes == "" || communicationnotes == " ")  && communicationhval==false)
        {
           $("#CommunicationNs").val("0");
        }else 
        {
           $("#CommunicationNs").val("1");
        }
        //vision
        var vimpared=$("#vimpared").val();
        var visionnotes=$("#visionnotes").val();
        var ServiceStatus=$('#ServiceStatus').val();
         var vnss=$(".vnss").is(":checked");
         if((vimpared == "" || vimpared == null) && (visionnotes == "" || visionnotes == " ") &&(vnss == false) )
        {
           $("#Visionns").val("0");
        }else 
        {
           $("#Visionns").val("1");
        }
        //Hearing 
        var himpared=$("#himpared").val();
        var hearingnotes=$("#hearingnotes").val();
        var hnss=$(".hnss").is(":checked");
            if((himpared == "" || himpared == null) && (hearingnotes == "" || hearingnotes == " ") && (hnss == false))
        {
           $("#Hearingns").val("0");
        }else 
        {
           $("#Hearingns").val("1");
        }
        //Respiratory
        var SOB=$("#SOB").val();
        var SPO2=$("#SPO2").val();
        var respiratorynotes=$("#respiratorynotes").val();
        var Cough=$("#Cough").val();
        var Nebulization=$("#Nebulization").val();
        var Tracheostomy=$("#Tracheostomy").val();
        var CPAP_BIPAP=$("#CPAP_BIPAP").val();
        var ICD=$("#ICD").val();
        var central_cynaosis=$("#central_cynaosis").val();
        var nulll=  $("input[value='null']").is(":checked");
        var TB=  $("input[value='TB']").is(":checked");
        var Asthama=  $("input[value='Asthama']").is(":checked");
        var COPD=  $("input[value='COPD']").is(":checked");
        var Pnemonia=  $("input[value='Pnemonia']").is(":checked");
        var rnss=$(".rnss").is(":checked");
         if((SOB == "" || SOB == " " ) &&  (SPO2 == "" || SPO2 == " ") && (respiratorynotes == "" || respiratorynotes == " ") && (Cough == "" || Cough == null) && (Nebulization == "" || Nebulization == null) && (Tracheostomy == "" || Tracheostomy == null) && (CPAP_BIPAP == "" || CPAP_BIPAP == null)  && (ICD == "" || ICD == null) && (central_cynaosis == "" || central_cynaosis == null) && (nulll == false) && (TB == false) && (Asthama == false) && (COPD == false) && (Pnemonia == false) &&(rnss==false))
        {
          $("#Respiratoryy").val("0");

        }else 
        {
          $("#Respiratoryy").val("1");
        }

        //Position
        var suspine=  $("input[value='Supine']").is(":checked");
        var fowlers= $("input[value='Fowlers']").is(":checked");
        var rl = $("input[value='Right_Lateral']").is(":checked");
        var ll= $("input[value='Left_Lateral']").is(":checked");
        var ServiceStatus=$('#ServiceStatus').val();
        var positionnotess= $("#positionnotes").val();
        var pnss=$(".pnss").is(":checked");
         if((suspine == false) && (fowlers ==false) && (rl==false) && (ll == false) && (positionnotess == "" || positionnotess == " ") && (pnss ==false))
        {
           $("#Positionn").val("0");

        }else 
        {
             $("#Positionn").val("1");
        }
        //Circulatory
         var ChestPain= $("#ChestPain").val();
        var PeripheralCyanosis= $("#PeripheralCyanosis").val();
         var cnss=$(".cnss").is(":checked");
        var JugularVein= $("#JugularVein").val();
        var SurgeryHistory= $("#SurgeryHistory").val();
        var circulatorynotes= $("#circulatorynotes").val();

        if((ChestPain == "" || ChestPain == " " ) && (PeripheralCyanosis == "" || PeripheralCyanosis == null) && (JugularVein == "" || JugularVein == null) && (SurgeryHistory == "" || SurgeryHistory == " ") && (circulatorynotes == "" || circulatorynotes == " ") && (cnss == false))
        {
             $("#Circulatoryy").val("0");
        }else 
        {
             $("#Circulatoryy").val("1");
        }
        //Denture
        var Lower= $("#Lower").val();
         var dnss=$(".dnss").is(":checked");
        var Upper= $("#Upper").val();
        var Cleaning= $("#Cleaning").val();
        var denturenotes= $("#denturenotes").val();
         if((Lower == "" || Lower == null) && (Upper == "" || Upper == null) && (Cleaning == "" || Cleaning ==null) && (denturenotes == "" || denturenotes == " ") && (dnss ==false) )
        {
             $("#Dentureee").val("0");
        }else 
        {
             $("#Dentureee").val("1");
        }
        //Nutrtion

        var Type= $("#Type").val();
        var Intact= $("#Intact").val();
        var nutritionnotes= $("#nutritionnotes").val();
        var Diet= $("#Diet").val();
        var feedingtype= $("#feedingtype").val();
        var diettnotes= $("#diettnotes").val();
        var nnss=$(".nnss").is(":checked");

        if((Type == "" || Type == null) && (Intact == "" || Intact == null) && (Diet == "" || Diet == null)  && (feedingtype == "" || feedingtype == null) && (nutritionnotes == "" || nutritionnotes == " ") && (diettnotes == "" || diettnotes == " ") &&(nnss==false))
        { 
             $("#Nutritionnn").val("0");
        }else 
        {
             $("#Nutritionnn").val("1");
        }
        //Abdomen
        var Shape= $("#Shape").val();
        var Palpation= $("#Palpation").val();
        var AusculationofBS= $("#AusculationofBS").val();
        var Percussion= $("#Percussion").val();
        var Ileostomy= $("#Ileostomy").val();
        var Colostomy= $("#ColostomyFunctioning").val();
        var abdomennotess= $("#abdomennotess").val();
         var adnss=$(".adnss").is(":checked");
         if((Shape == "" || Shape == null) && (Palpation == "" || Palpation == null) && (AusculationofBS == "" || AusculationofBS == null)  && (Percussion == "" || Percussion == " " ) && (Ileostomy == "" || Ileostomy == null) && (Colostomy == "" || Colostomy == null) && (abdomennotess == "" || diettnotes == " ") && (adnss==false ))
        {
          $("#abdomenn").val("0");

        }else 
        {
          $("#abdomenn").val("1");
        }
        //Elimination
        var UrinaryContinent= $("#UrinaryContinent").val();
         var enss=$(".enss").is(":checked");
        var urinarynotes= $("#urinarynotes").val();
        var FecalType= $("#FecalType").val();
        var fecalnotes= $("#fecalnotes").val();
         if((UrinaryContinent == "" || UrinaryContinent == null) && (FecalType == "" || FecalType == null) && (urinarynotes == "" || urinarynotes == " ") && (fecalnotes == "" || fecalnotes == " ") &&(enss==false))
        {
               $("#eliminationnn").val("0");
        }else 
        {
               $("#eliminationnn").val("1");
        }
        //Mobility
        var Independentofmobility= $("#Independentofmobility").val();
         var mnss=$(".mnss").is(":checked");
        var mobilitynotes= $("#mobilitynotes").val();

       if((Independentofmobility == "" || Independentofmobility == null)   && (mobilitynotes == "" || mobilitynotes == " ") && (mnss ==false))
        {
          $("#Mobilityyy").val("0");
        }else 
        {
          $("#Mobilityyy").val("1");
        }
        var communication=$("#CommunicationNs").val();
        var MemoryIntactsNs=$("#MemoryIntactsNs").val();
        var Visionns=$("#Visionns").val();
        var Hearingns=$("#Hearingns").val();
        var Respiratoryy=$("#Respiratoryy").val();
        var Circulatoryy=$("#Circulatoryy").val();
        var Dentureee=$("#Dentureee").val();
        var Nutritionnn=$("#Nutritionnn").val();
        var abdomenn=$("#abdomenn").val();
        var eliminationnn=$("#eliminationnn").val();
        var Mobilityyy=$("#Mobilityyy").val();
        var Positionn=$("#Positionn").val();
        var NervousSystemm=$("#NervousSystemm").val();
        if( communication == 1 && MemoryIntactsNs == 1 && Visionns == 1 && Hearingns == 1 && Respiratoryy == 1 && Circulatoryy == 1 && Dentureee == 1 && Nutritionnn== 1 && abdomenn == 1 && eliminationnn ==1 && Mobilityyy == 1 && Positionn == 1 && NervousSystemm ==1 && $("#BP").val() != "" && $("#Temperature").val() != "" && $("#RR").val() != "" && $("#Pulse").val() != "")
              {
                //console.log("sadsad");
                $("#submit").prop("disabled",false);
              
              }else 
              {
                $("#submit").prop("disabled",true);
              }
    }
    else
    {
      $("#followupdate").hide();
      if(value == "Dropped")
      {
        $("#reason").show();
        $("#followupdate").hide();
      }
      else
      {
         $("#reason").hide();
         $("#followupdate").hide();
          $('.followupdaterequired').removeAttr("required","required");

                 $('#submit').show();
      }
    }

});
</script>

<!-- <script>

  $.noConflict();
jQuery(document).ready(function($){

$(document).ready(function(){



});
});
</script> -->
<style type="text/css">
.Needassistanceofurinary,.typeofurinary,.Needassistanceoffecal,.typeoffecal,.NeedAssistanceofmobility,.typesofmobility,.BulgeType,#ColorOfPhlegm1,.girth
{
display: none;
}
  .btn

{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
#frequency
{
  display: none;
}
 .units
  {
       float: right;
    margin-right: 5px;
    margin-top: 3px;
  }
.btn:hover
{
      opacity: 0.5;
}
.maintitle
{
     margin-left: -4px;
    width: 102%;
}
.footer
  {
    margin-top: 70px;
  }
.btn:hover
{
  outline: 0;
    background-color: #00C851;
}
  .imgg {
       margin-top: -13px;
    margin-left: -80px;
}
#alignn
{
      margin-top: 24%;
}
.top10
{
  margin-top: 2%;
}
.loo
{
      margin-top: -1px;
    margin-left: 11px;
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;

    z-index: 10000;
    position: fixed;
}
.popover.bottom
{
  top:49px!important;
  width: 408% !important;
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
}
.lead
{
  font-size: 13px;
}
.firstname
{
  margin-top: -7px;
}
 #NebulizationMedicineName
  {
    display: none;
  }
.showw1,#AssessmentDetails
{
     border-radius: 11px;
    border: 1px solid #808080;
    width: 102.3%;
    font-family: myFirstFont;
    margin-left: -29px;
}
#Prescribedby,.doc
{
  margin-bottom: 15px;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  .navbar
  {
    position: fixed;
  }
  .showw1
  {
        margin-left: -25px;
    width: 105%;
  }
  .firstname
{
      padding-top: 15px;
}
  .lead
  {
    margin-top: 40px!important;
  }
 .loo
  {
        margin-left: 41%;
    margin-top:-5px;
  }
  .al
  {
    margin-top: 20%;
  }
  .navbar2 {
    margin-top: 13px;
                position: fixed!important;
    display: block!important;
      margin-left: 82%;
    width: 15%;
    height: 40px;
    z-index: 10000;
    position: fixed;
      }

      #userimage {
               margin-top: -9%;
    width: 61%;
    margin-left: 2%;
  }


    #maintitle{
              width: 113%;
            margin-left: -13px;}

            #VitalSigns,#Orientation,#MemoryIntacts,#Communication,#Vision,#Hearing,#Respiratory,#Position,#Circulatory,#Denture,#Nutrition,#Diet,#Abdomen,#Elimination,#Mobility
            {
                  margin-top: 43px;
            }
.custom
{
      width: 103%!important;
}
.switch
{
  margin-left: -37%;
    z-index: 1000000;
    position: absolute;
}
#notsignificant
{    margin-right: -15px!important;
    background: white!important;
}
}

/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) {
.navbar
   {
    position: fixed;
  }
  .loo
  {
        margin-left: 41%;
    margin-top:-5px;
  }
  .showw1
  {
        margin-left: -25px;
    width: 105%;
  }
#userimage {
               margin-top: -9%;
    width: 61%;
    margin-left: 2%;
  }
    .firstname
{
      padding-top: 15px;
}
  .lead
  {
    margin-top: 40px!important;
  }
  #loo
  {
        margin-left: 41%;
    margin-top: 13px;
  }
  #main1 {
    padding-left: 7%;
    margin-left: 5%;
    width: 93%;
}
#notsignificant
{    margin-right: -15px!important;
    background: white!important;
}
  .al
  {
    margin-top: 20%;
  }
  .navbar2 {
    margin-top: 13px;
                position: fixed!important;
    display: block!important;
      margin-left: 82%;
    width: 15%;
    height: 40px;
    z-index: 10000;
    position: fixed;
      }


 #maintitle
{
   width: 111%;      margin-left: -20px;
}
#firstform,#ServiceDetails,#ServiceDetails1
{
    margin-left: -36px;
    width: 105%;
}
        #VitalSigns,#Orientation,#MemoryIntacts,#Communication,#Vision,#Hearing,#Respiratory,#Position,#Circulatory,#Denture,#Nutrition,#Diet,#Abdomen,#Elimination,#Mobility
            {
                  margin-top: 43px;
            }
.custom
{
      width: 103%!important;
}
.switch
{
  margin-left: -37%;
    z-index: 1000000;
    position: absolute;
}
}

/*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  .navbar
   {
    position: fixed;
  }
  .loo
  {
        margin-left: 41%;
    margin-top:-5px;
  }
  .showw1
  {
        margin-left: -25px;
    width: 105%;
  }
#userimage {
               margin-top: -9%;
    width: 61%;
    margin-left: 2%;
  }
    .firstname
{
      padding-top: 15px;
}
  .lead
  {
    margin-top: 40px!important;
  }
  #main1 {
    padding-left: 7%;
    margin-left: 10%;
    width: 90%;
}
  #loo
  {
        margin-left: 41%;
    margin-top: 13px;
  }
  .al
  {
    margin-top: 28%;
  }
  .navbar2 {
    margin-top: 13px;
                position: fixed!important;
    display: block!important;
      margin-left: 82%;
    width: 15%;
    height: 40px;
    z-index: 10000;
    position: fixed;
      }
  .navbar2 {
            display: block!important;
      }
     .custom
     {
      width: 107%;
    margin-left: -33px;
     }
 #maintitle
{
       width: 112%;
    margin-left: -10px;
}
#firstform,#ServiceDetails,#ServiceDetails1
{
    margin-left: -14px;
    width: 100%;
}
        #VitalSigns,#Orientation,#MemoryIntacts,#Communication,#Vision,#Hearing,#Respiratory,#Position,#Circulatory,#Denture,#Nutrition,#Diet,#Abdomen,#Elimination,#Mobility
            {
                  margin-top: 43px;
            }
.custom
{
      width: 107%!important;
}
.switch
{
  margin-left: -37%;
    z-index: 1000000;
    position: absolute;
}
#notsignificant
{    margin-right: -15px!important;
    background: white!important;
}

}


/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  .custom
     {
      width: 107%;
    margin-left: -33px;
     }
  .navbar
   {
    position: fixed;
  }
  .loo
  {
        margin-left: 41%;
    margin-top:-5px;
  }
  .showw1
  {
        margin-left: -25px;
    width: 105%;
  }
#userimage {
               margin-top: -9%;
    width: 61%;
    margin-left: 2%;
  }
    .firstname
{
      padding-top: 15px;
}
  .lead
  {
    margin-top: 40px!important;
  }
  #main1 {
    padding-left: 7%;
    margin-left: 8%;
    width: 90%;
}

  .al
  {
    margin-top: 20%;
  }
  .navbar2 {
    margin-top: 13px;
                position: fixed!important;
    display: block!important;
      margin-left: 82%;
    width: 15%;
    height: 40px;
    z-index: 10000;
    position: fixed;
      }
  .navbar2 {
            display: block!important;
      }

         #maintitle
{
       width: 108%;
    margin-left: -10px;
}
#firstform,#ServiceDetails,#ServiceDetails1
{
    margin-left: -22px;
    width: 100%;
}
        #VitalSigns,#Orientation,#MemoryIntacts,#Communication,#Vision,#Hearing,#Respiratory,#Position,#Circulatory,#Denture,#Nutrition,#Diet,#Abdomen,#Elimination,#Mobility
            {
                  margin-top: 43px;
            }
.custom
{
      width: 106%!important;
}
.switch
{
  margin-left: -37%;
    z-index: 1000000;
    position: absolute;
}
#notsignificant
{    margin-right: -15px!important;
    background: white!important;
}
  }
/*iPad:*/
@media screen and (device-aspect-ratio: 3/4) {
          #maintitle
{
           width: 102%;
    margin-left: -10px;
}
#firstform,#ServiceDetails,#ServiceDetails1
{
        margin-left: -76px;
    width: 111%;
}
.head
{
  display: none;
}

}
#placementoficd1
{
  display: none;
}
.head
{
          margin-top: -73px;
    height: 40px;
    z-index: 10000;
    position: absolute;
    width: 100%;
}
.popover.bottom
{
  min-width:30%;
  margin-left: 20%;
}
.popover.bottom>.arrow
{
  display: none !important;
}
.navbar2
{
  display: none;
}

.alignment
{
  margin-top: 6px;
}
#SpecificRequirements
{
    margin-top: 9px;
}
#DaysWorked
{
  margin-top: 5px;
}
.totalscoregcs
{
      margin-top: 17px;
    margin-bottom: 22px;
    min-height: 66px;

}
#breakfast,#lunchtime,#sanckstime,#dinnertime,#tpnn,#rtfeeding,#pegfeeding
{
  display: none;
}
.asterisk
{
     margin-top: -20px;
    position: absolute;
    color: red;
    font-size: 10px;
    display: none;
    margin-left: 22px;
}
.asterisktemp
{
     margin-top: -20px;
    position: absolute;
    color: red;
    font-size: 10px;
    display: none;
    margin-left: 89px;
}
.asteriskpulse
{
     margin-top: -20px;
    position: absolute;
    color: red;
    font-size: 10px;
    display: none;
    margin-left: 39px;
}
</style>
<style>
.switch {
    top: 14px;
    width: 37px;
    height: 19px;
    position: relative;
    display: inline-block;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
     position: absolute;
    content: "";
    height: 13px;
    width: 13px;
    left: 2px;
    bottom: 3px;
    background-color: white;
    -webkit-transition: .2s;
    transition: .2s;
}

input:checked + .slider {
  background-color: #009056;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
#notsignificant
{    background: #f2f2f2;
    display: inline-flex;
    float: right;
    margin-right: 0px;
    min-height: 51px;
    min-width: 203px;
}
.text
{
  margin-left: 13px;
      margin-top: 15px;
}
#overflowcss
{
      max-width: 97%;
    height: 45px;
    overflow: overlay;
}

#overflowcss:::-webkit-scrollbar-track
{
  background: red;
  width: 120px;  /* for vertical scrollbars */
  height: 10px; /* for horizontal scrollbars */
}
#overflowcss::-webkit-scrollbar-thumb
{
  background: green;



}
#central_cynaosis
{
      margin-top: 6px;
}
#ChestPain,#PeripheralCyanosis
{
      margin-top: 3px;
}
.cautiontoentermar
{
  display: none;
  color: green;
}
.cautiontoenterfollowup
{
  display: none;
  color: red;
}
.cautiontoenterdropreason
{
  display: none;
  color: red;
}
</style>
</head>
<body >
    <div id="formcontent">
        <!-- header -->
        <!-- <div class="navbar navbar-default " style="    background-color: white;
        border-color: #e7e7e7;">
        <div class="container">

        <div class="navbar-header">
        <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 8px;">
        <span class="sr-only">Toggle navigation</span>
        <span cl<div id="formcontent"> ass="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button> -->




    <!-- End of header -->
    <!-- title -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="title">
                <h2> {{substr(Route::currentRouteName(),9)}} Care Assessment</h2>
            </div>
        </div>

    </div>

    <!-- title Ends -->
    <form class="form-horizontal" action="/vh/@yield('editid')" method="POST">
        {{csrf_field()}}
        @section('editMethod')
        @show
        <!-- Lead details start -->
        <div class="container" id="main" style="    margin-top: 34px;">

            <div class="row" id="maintitle" style="    margin-left: -4px;
            width: 102%;" >

            <div class="col-sm-12" >
                <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#firstform"  >Lead Details<p style="text-align: -webkit-right;
                    margin-top: -20px;
                    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
                </button>

            </div>

        </div>
    </div>

    <div class="container" id="main1" style="margin-top: 11px;">

        <div class="row collapse in" id="firstform" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">
            @foreach ($leaddata as $lead)


            <div class="col-sm-12" style="margin-top: 7px;">
                <a href="{{'/Verticalhead/'.$lead->id.'/edit'}}"   style="float: right;"><img src="/img/edit.png" class="img-responsive" alt="oops! Not found"></a>
            </div>


            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Lead ID : </b>{{$lead->id}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Created At : </b>{{$lead->created_at}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Created By : </b>{{$lead->createdby}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">

            </div>
            <div class="col-sm-12"></div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Client First Name : </b>{{$lead->fName}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Client Middle Name : </b>{{$lead->mName}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Client Last Name : </b>{{$lead->lName}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Client Mobile Number : </b>{{$lead->Country_Code}}{{$lead->MobileNumber}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Email Id:</b>{{$lead->EmailId}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Source :</b>{{$lead->Source}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Service Type : </b>{{$lead->ServiceType}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Lead Type : </b>{{$lead->LeadType}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Service Status : </b>{{$lead->ServiceStatus}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Alternate number:</b> {{$lead->Alternatenumber}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Patient Name:</b> {{$lead->PtfName}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Age:</b> {{$lead->age}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Gender:</b> {{$lead->Gender}}
                <input type="hidden" name="gender" id="gender" value="{{$lead->Gender}}">
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Relationship:</b> {{$lead->Relationship}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Aadhar number:</b> {{$lead->AadharNum}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>General Condition:</b> {{$lead->GeneralCondition}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Branch:</b> {{$lead->Branch}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Requested Date:</b> {{$lead->RequestDateTime}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Assigned to:</b> {{$lead->AssignedTo}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Quoted Price:</b> &#8377; {{$lead->QuotedPrice}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Expected Price:</b> &#8377; {{$lead->ExpectedPrice}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Service Status:</b> {{$lead->ServiceStatus}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Gender Prefered:</b> {{$lead->PreferedGender}}
            </div>
            <div class="col-sm-3" style="    margin-top: 11px;">
                <b>Prefered Languages:</b> {{$lead->PreferedLanguage}}
            </div>
            <div class="col-sm-12" style="margin-top: 11px">
                <b>Address:</b> &nbsp;&nbsp;{{$lead->Address1}} {{$lead->Address2}} {{$lead->City}} {{$lead->District}} {{$lead->State}} {{$lead->PinCode}}
            </div>
            <div class="col-sm-12" style="    margin-top: 11px;">
                <!-- {!! nl2br(e($lead->Remarks)) !!} -->
                <b>Remarks:</b> <textarea class="form-control" disabled col=30; rows=5; style="width:100%; background: white;">{{$lead->Remarks}}</textarea>

            </div>
            <div class="col-sm-12"></div>
            <!-- <button><a href="{{'/cc/'.$lead->id.'/edit'}}">Edit &emsp;</a></button> -->
            <div class="col-sm-3" style="    margin-top: 11px;">
                <label style="color: #333"><b>Lead Status </b> </label>
                <select name="ServiceStatus" id="ServiceStatus"  >
                    <option value="{{$lead->ServiceStatus}}">{{$lead->ServiceStatus}}</option>
                    @if($lead->ServiceStatus=="New")
                    <option value="In Progress">In Progress</option>
                    <option value="Deferred">Deferred</option>
                    <option value="Dropped">Dropped</option>
                    <option value="Converted">Converted</option>
                    @else
                    @if($lead->ServiceStatus=="In Progress")
                    <option value="Deferred">Deferred</option>
                    <option value="Dropped">Dropped</option>
                    <option value="Converted">Converted</option>
                    @else
                    @if($lead->ServiceStatus=="Deferred")
                    <option value="In Progress">In Progress</option>
                    <option value="Dropped">Dropped</option>
                    @else
                    @if($lead->ServiceStatus=="Dropped")
                    <option value="In Progress">In Progress</option>
                    @else
                    @if($lead->ServiceStatus=="Converted")
                    <option value="Deferred">Deferred</option>
                    @endif
                    @endif

                    @endif

                    @endif
                    @endif
                    <!-- @foreach($service1 as $service1)

                    <option value="{{ $service1->status}}">{{ $service1->status}}</option>
                    @endforeach -->
                </select>
            </div>
            <div class="col-sm-3" id="followupdate" style="      margin-bottom: 11px;  margin-top: 11px;">
                <label style="color: #333">Follow Up Date </label>

                <input type="date" class="followupdaterequired" name="followupdate" id="followupdate" value="@yield('editfollowupdate')" style="width: 92%;">
                <p class="cautiontoenterfollowup" style="display: block;">Please Enter Follow up Date </p>

            </div>

            <div class="col-sm-3" id="reason" style="      margin-bottom: 11px;  margin-top: 11px;">
                <label style="color: #333">Reason for Lead Drop </label>

                <input type="text"  name="reason" id="reason" class="dropreasonrequired" value="@yield('editreason')" style="width: 92%;">
                 <p class="cautiontoenterdropreason" style="display: block;">Please enter reason for dropping </p>

            </div>
            <div class="col-sm-3" style="      margin-bottom: 11px;  margin-top: 11px;">
                <label style="color: #333">Service Requested </label>
                <select name="requested_service" id="requested_service"  value="@yield('editrequested_service')">
                    <option value="@yield('editrequested_service')">@yield('editrequested_service')</option>
                    @if($service_request->requested_service!=NULL)
                    <option></option>
                    @endif

                    @foreach($service as $service)
                    <option value="{{ $service->servicetype}}">{{ $service->servicetype}}</option>
                    @endforeach
                </select>
            </div>
            @if($verc == NULL)
            <div id="showassignto" class="col-sm-3" style="margin-bottom: 11px;    margin-top: 11px;">
                <label style="color: #333">Assign To </label>
                <select name="assigned" id="assigned"  >

                    <option value="@yield('editassigned')">@yield('editassigned')</option>

                    @foreach($emp as $emp)
                    <option value="{{ $emp->FirstName }}">{{ $emp->FirstName }}</option>
                    @endforeach
                </select>
            </div>
            @else

            <div class="col-sm-3" style="margin-bottom: 11px;    margin-top: 11px;">
                <b>Assigned To: </b>{{ $verc->FirstName }}
            </div>
            @endif
            <div class="col-sm-3"><br>
            </div>
            <div class="col-sm-12"><br>
            </div>

            <div class="col-sm-6" style="margin-bottom: 11px;    margin-top: 11px;">
                <label><b style="color: #333;"> Comments</b></label>
                <textarea class="form-control" rows="5" cols="20" name="comment" id="comment" ></textarea>
            </div>



            @endforeach
        </div>
    </div>
    <!-- End of Lead Details -->
    <!-- General Details Start -->
    <div class="container" id="main" style="    margin-top: 34px;">

        <div class="row" id="maintitle" style="    margin-left: -4px;
        width: 102%;" >

        <div class="col-sm-12" >
            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#GeneralDetails"  >General Details<p style="text-align: -webkit-right;
                margin-top: -20px;
                margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
            </button>

        </div>

    </div>
</div>
<div class="container" id="main1" style="margin-top: 11px;">

    <div class="row collapse" id="GeneralDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">

        <div class="col-sm-3" style="margin-top: 15px;">
            <label>Assessor</label><input type="text"  rows="5" name="Assessor" id="Assessor" value="@yield('editAssessor')">
        </div>
        <div class="col-sm-3" style="margin-top: 14px;">
            <label>Assessment Date</label><input type="date"  rows="5" name="AssessDateTime" id="AssessDateTime" value="@yield('editAssessDateTime')">
        </div>
        <div class="col-sm-3" style="margin-top: 15px;">
            <label>Assessment Place</label><input type="text"  rows="5" name="AssessPlace" id="AssessPlace" value="@yield('editAssessPlace')">
        </div>
        <div class="col-sm-3" style="margin-top: 14px;">
            <label>Service Start Date</label><input type="date"  rows="5" name="ServiceStartDate" id="ServiceStartDate" value="@yield('editServiceStartDate')">
        </div>
        <div class="col-sm-3" style="margin-top: 23px;">
            <label>Service Pause</label><input type="date"  rows="5" name="ServicePause" id="ServicePause" value="@yield('editServicePause')">
        </div>
        <div class="col-sm-3" style="margin-top: 22px;">
            <label>Service End Date</label><input type="date"  rows="5" name="ServiceEndDate" id="ServiceEndDate" value="@yield('editServiceEndDate')">
        </div>
        <div class="col-sm-3" style="margin-top: 23px;">
            <label>Shift Preference</label>
            <select name="SpecificRequirements" id="SpecificRequirements"  value="@yield('editSpecificRequirements')">
                <option value="@yield('editSpecificRequirements')">@yield('editSpecificRequirements')</option>
                @foreach($shift as $shift)
                <option value="{{ $shift->shiftrequired}}">{{ $shift->shiftrequired}}</option>
                @endforeach
            </select>

        </div>
        <div class="col-sm-3" style="margin-top: 23px;">
            <label>Days Worked</label><input type="text"  rows="5" name="DaysWorked" id="DaysWorked" value="@yield('editDaysWorked')">
        </div>
        <div class="col-sm-12"></div>
        <div class="col-sm-3" style="margin-top: 23px;">
            <label>Latitude</label><input type="text"  rows="5" name="Latitude" id="Latitude" value="@yield('editLatitude')">
        </div>
        <div class="col-sm-3" style="    margin-bottom: 11px;margin-top: 23px;">
            <label>Longitude</label><input type="text"  rows="5" name="Longitude" id="Longitude" value="@yield('editLongitude')">
        </div>
    </div>
</div>
<!-- End of General Details -->
 <!-- Assessment Details Begins -->
          <div class="container" id="main" style="    margin-top: 34px;">

      <div class="row" id="maintitle" style="  " >

        <div class="col-sm-12" >
          <button type="button" class="btnCustom btnCustom-default custom assessmentdetails" data-toggle="collapse" data-target="#AssessmentDetails" id="AD" >Assessment Details<p style="text-align: -webkit-right;
           margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
            </button>

        </div>

      </div>
    </div>
    <div class="container" id="main1" style="margin-top: 11px;">

      <div class="row collapse showw1" id="AssessmentDetails" style="">

            <div class="col-sm-6" style="margin-top: 18px;">
                      <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Medical Diagnosis</label>
                           <textarea class="form-control" rows="5" cols="20" name="MedicalDiagnosis" id="MedicalDiagnosis" value="@yield('editMedicalDiagnosis')">@yield('editMedicalDiagnosis')</textarea>
                </div>
            </div>
             <div class="col-sm-6" style="margin-top: 18px;">
                    <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Chief reasons for seeking home healthcare</label>
                           <textarea class="form-control" rows="5" cols="20" name="q1" id="q1" value="@yield('editq1')">@yield('editq1')</textarea>
                </div>
            </div>
            <div class="col-sm-12" style="margin-top:16px;    margin-bottom: 13px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#GeneralHistory" style="    margin-left: 1px; width: 100%;" >General History<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

            </div>
            <div class="col-sm-12 collapse" id="GeneralHistory">
                   <div class="col-sm-4" style="margin-top: 15px;">
                       <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Present History</label>
                            <textarea class="form-control" rows="5" cols="20" name="PresentHistory" id="PresentHistory" value="@yield('editPresentHistory')">@yield('editPresentHistory')</textarea>
                           </div>
                      </div>


                  <div class="col-sm-4" style="margin-top: 15px;">
                        <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Past History </label>
                            <textarea class="form-control" rows="5" cols="20" name="PastHistory" id="PastHistory" value="@yield('editPastHistory')">@yield('editPastHistory')</textarea>
                           </div>
                    </div>

                   <div class="col-sm-4" style="margin-top: 15px;">
                           <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Family History</label>
                            <textarea class="form-control" rows="5" cols="20" name="FamilyHistory" id="FamilyHistory" value="@yield('editFamilyHistory')">@yield('editFamilyHistory')</textarea>
                           </div>

                  </div>
                  <div class="col-sm-4" style="margin-top: 15px;" id="Mensuration_OBG">
                       <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Menstruation & OBG History</label>
                           <textarea class="form-control" rows="5" cols="20" name="Mensural_OBGHistory" id="Mensural_OBGHistory" value="@yield('editMensural_OBGHistory')">@yield('editMensural_OBGHistory')</textarea>
                           </div>
                      </div>


                  <div class="col-sm-4" style="margin-top: 15px;">
                        <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Allergic History</label>
                            <textarea class="form-control" rows="5" cols="20" name="AllergicHistory" id="AllergicHistory" value="@yield('editAllergicHistory')">@yield('editAllergicHistory')</textarea>
                           </div>
                    </div>
                   <div class="col-sm-4" style="margin-top: 15px;">
                           <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Allergic Status</label>
                           <textarea class="form-control" rows="5" cols="20" name="AllergicStatus" id="AllergicStatus" value="@yield('editAllergicStatus')">@yield('editAllergicStatus')</textarea>
                           </div>

                  </div>
                   <div class="col-sm-4" style="margin-top: 15px;">
                           <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Allergic Severity </label>
                           <textarea class="form-control" rows="5" cols="20" name="AllergicSeverity" id="AllergicSeverity" value="@yield('editAllergicSeverity')">@yield('editAllergicSeverity')</textarea>
                           </div>

                  </div>
                  <div class="col-sm-4" style="margin-top: 15px;">
                           <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Medical History</label>
                            <textarea class="form-control" rows="5" cols="20" name="MedicalHistory" id="MedicalHistory" value="@yield('editMedicalHistory')">@yield('editMedicalHistory')</textarea>
                           </div>

                  </div>

            </div>
             <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#VitalSigns" style="    margin-left: 1px; width: 100%;" >Vital Signs<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                      {{--       <div id="notsignificant">
                                <label class="switch">
                                  <input type="checkbox" >
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Siginificant  </b></h5>
                            </div> --}}



            </div>
            <div class="col-sm-12 collapse" id="VitalSigns">
                   <div class="col-sm-3" style="margin-top: 15px;">
                      <!--  <div class="form-group" style="    width: 87%;margin-left: 0px;">
                            <label >BP</label>
                            <input type="text"  rows="5" name="BP" id="BP" value="@yield('editBP')" style="width:92%;">

                            <div id="BPM" style="float: right;    margin-right: -37px;">mm/Hg</div>
                           </div> -->

                           <label>BP <span class="asterisk">&#10033;</span> &emsp;&emsp;&emsp;&emsp;</label>

                            <input class="mandatoryy" type="text" name="BP" id="BP" value="@yield('editBP')" style="width:76%;" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>
                            <div class="units" id="BPM">mmHg</div>
<!--
                             <select name="BP_Measured_In" id="BP_Measured_In"  value="@yield('editBP_Measured_In')"   style="width:21%;">
                                  <option value="@yield('editBP_Measured_In')">@yield('editBP_Measured_In')</option>
                                  <option value="mm">mm</option>
                                  <option value="Hg">Hg</option>

                                </select> -->


                      </div>


                  <div class="col-sm-3" style="margin-top: 15px;">

                            <label >BP Measured By </label>
                            <select name="BP_equipment" id="BP_equipment"  value="@yield('editBP_equipment')">
                              <option value="@yield('editBP_equipment')">@yield('editBP_equipment')</option>
                              <option value="Electronic Device">Electronic Device</option>
                              <option value="Manually">Manually</option>
                            </select>
                    </div>

                   <div class="col-sm-3" style="margin-top: 15px;">

                            <label >Taken from</label>
                            <select name="BP_taken_from" id="BP_taken_from"  value="@yield('editBP_taken_from')">
                            <option value="@yield('editBP_taken_from')">@yield('editBP_taken_from')</option>
                            <option value="Left Arm">Left Arm</option>
                            <option value="Right Arm">Right Arm</option>
                          </select>

                  </div>
                  <div class="col-sm-3" style="margin-top: 15px;">

                            <label >RR <span class="asterisk">&#10033;</span></label><br>
                           <input class="mandatoryy" type="text"  rows="5" name="RR" id="RR" value="@yield('editRR')" style="width:86%;">
                           <div id="BPM" style="float: right;    margin-right: -12px;">bpm</div>
                      </div>

                 <div class="col-sm-12"></div>
                  <div class="col-sm-3" style="margin-top: 15px;">

                           <!--  <label >Temperature</label>
                            <input type="text"  rows="5" name="Temperature" id="Temperature" value="@yield('editTemperature')" style="width:84%;">
                            <div id="BPM" style="float: right;            margin-right: 5px;;">c &deg;/f &deg;</div> -->


                            <label >Temperature<span class="asterisktemp">&#10033;</span></label>
                            <input class="mandatoryy"   type="text"  rows="5" name="Temperature" id="Temperature" value="@yield('editTemperature')" style="width:76%;" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>

                            <select name="Temperature_Measured_In" id="Temperature_Measured_In"  value="@yield('editTemperature_Measured_In')"   style="width:21%;">
                                  <option value="@yield('editTemperature_Measured_In')">@yield('editTemperature_Measured_In')</option>
                                  <option value="C &deg;">C &deg;</option>
                                  <option value="F &deg;">F&deg;</option>

                                </select>


                    </div>
                   <div class="col-sm-3" style="margin-top: 15px;">

                            <label >Temperature Type</label>
                            <select name="TemperatureType" id="TemperatureType"  value="@yield('editTemperatureType')">
                            <option value="@yield('editTemperatureType')">@yield('editTemperatureType')</option>
                                  <option value="Armpit">Armpit</option>
                                  <option value="Oral">Oral</option>
                                  <option value="Groin">Groin</option>
                                  <option value="Rectal">Rectal</option>
                                  <option value="Axilla">Axilla</option>

                                </select>

                  </div>
                   <div class="col-sm-3" style="margin-top: 15px;">

                            <label >Pulse  <span class="asteriskpulse">&#10033;</span></label>
                            <input class="mandatoryy" type="text"  rows="5" cols="20" name="Pulse" id="Pulse" value="@yield('editPulse')" style="width:88%;">
                           <div id="BPM" style="float: right;    margin-right: -19px;">bpm</div>

                  </div>
                  <div class="col-sm-12">
                  </div>
                   <div class="col-sm-3" style="margin-top: 15px;">
                        <label >Weight</label>
                                      <input type="text" c rows="5" name="Weight" id="Weight" value="@yield('editWeight')" style="width: 87%;" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>

                                     <div id="BPM" style="float: right;    margin-right: 6px;">kg</div>
                      </div>


                  <div class="col-sm-3" style="margin-top: 15px;">
                        <label >Height  &emsp;&emsp;</label>

                                   <input type="text"  rows="5" name="Height" id="Height" value="@yield('editHeight')" style="width:77%;">

                                   <select name="Height_Measured_In" id="Height_Measured_In"  value="@yield('editHeight_Measured_In')"   style="width:21%;" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>
                                  <option value="@yield('editHeight_Measured_In')">@yield('editHeight_Measured_In')</option>
                                  <option value="cm">cm</option>
                                  <option value="ft">ft</option>

                                </select>
                    </div>

                     <div class="col-sm-4" style="margin-top: 15px;" >

                   <b> <p>BMI  </p>  </b>

                   <input type="text"  rows="5" name="BMI" id="BMI" value="@yield('editBMI')" style="border-bottom: 0px;    background: white;" readonly>

                  </div>


                  <div class="col-sm-4" style="margin-top: 18px;">
                    <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Other Observations</label>
                           <textarea class="form-control" rows="5" cols="20" name="vitalnotes" id="vitalnotes" value="">@yield('editvitalnotes')</textarea>
                </div>
            </div>


                 <div class="col-sm-12" style="margin-top: 14px;">
                      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Pain" style="    margin-left: 1px; width: 100%;" >Pain<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                 </div>
                 <div class="col-sm-12 collapse" id="Pain" style="margin-top: 11px;">
                    <div class="col-sm-4">
                            <label >Pain Scale </label>
                            <select name="PainScale" id="PainScale" value="@yield('editPainScale')">
                          <option value="@yield('editPainScale')">@yield('editPainScale')</option>
                                <option value="Face Reading">Face Reading</option>
                                <option value="Numeric Reading">Numeric Reading </option>
                              </select>
                    </div>
                    <div class="col-sm-4">
                          <label >Quality  </label>
                            <select name="Quality" id="Quality"  value="@yield('editQuality')">
                            <option value="@yield('editQuality')">@yield('editQuality')</option>
                                  <option value="Sharp">Sharp</option>
                                  <option value="Dull">Dull</option>
                                  <option value="Stabbing">Stabbing</option>
                                  <option value="Burning">Burning</option>
                                  <option value="Cruciating">Cruciating</option>
                                </select>

                    </div>
                    <div class="col-sm-4">
                          <label >Severity Scale  </label>
                    <!--   <select name="SeverityScale" id="SeverityScale"  value="@yield('editSeverityScale')">
                        <option value="@yield('editSeverityScale')">@yield('editSeverityScale')</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                            </select> -->
                             <div id="unranged-value" style="width: 333px;margin-top: 4px;margin-left: 3px;"></div>
                            <input type="hidden" name="SeverityScale" id="SeverityScale" value="">
                    </div>
                 </div>
                <div class="col-sm-12" style="margin-top: 14px;">
                      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Provocation" style="    margin-left: 1px; width: 100%;" >Provocation/Alleviation  <p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                 </div>
                <div class="col-sm-12 collapse" id="Provocation" style="margin-top: 11px;">
                    <div class="col-sm-4">
                            <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >What causes the pain? </label>
                           <textarea class="form-control" rows="5" cols="20" name="P1" id="P1" value="@yield('editP1')">@yield('editP1')</textarea>
                           </div>
                    </div>
                    <div class="col-sm-4">
                          <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >What makes it better?</label>
                           <textarea class="form-control" rows="5" cols="20" name="P2" id="P2" value="@yield('editP2')">@yield('editP2')</textarea>
                           </div>
                    </div>
                    <div class="col-sm-4">
                          <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >What makes it Worse? </label>
                           <textarea class="form-control" rows="5" cols="20" name="P3" id="P3" value="@yield('editP3')">@yield('editP3')</textarea>
                         </div>
                    </div>
                </div>
                    <div class="col-sm-12" style="margin-top: 14px;">
                        <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Region" style="    margin-left: 1px; width: 100%;" >Region<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                   </div>
                  <div class="col-sm-12 collapse" id="Region" style="margin-top: 11px;">
                    <div class="col-sm-4">
                            <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Where does the pain radiates to? </label>
                           <textarea class="form-control" rows="5" cols="20" name="R1" id="R1" value="@yield('editR1')">@yield('editR1')</textarea>
                           </div>
                    </div>
                    <div class="col-sm-4">
                          <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Is it in one place?</label>
                          <textarea class="form-control" rows="5" cols="20" name="R2" id="R2" value="@yield('editR2')">@yield('editR2')</textarea>
                          </div>
                    </div>
                    {{-- <div class="col-sm-4">
                          <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Does it go anywhere else? </label>
                           <textarea class="form-control" rows="5" cols="20" name="R3" id="R3" value="@yield('editR3')">@yield('editR3')</textarea>
                           </div>
                    </div> --}}
                    <div class="col-sm-4">
                          <div class="form-group" style="width: 101%;margin-left: 0px;">
                            <label >Did it start elsewhere and now localised to one spot? </label>
                           <textarea  class="form-control" rows="5" cols="20" name="R4" id="R4" value="@yield('editR4')">@yield('editR4')</textarea>
                          </div>
                 </div>
              </div>
               <div class="col-sm-12" style="margin-top: 14px;">
                      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Timing" style="    margin-left: 1px; width: 100%;" >Timing<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                 </div>
                 <div class="col-sm-12 collapse" id="Timing" style="margin-top: 11px;">
                    <div class="col-sm-6">
                           <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Time pain started? </label>
                           <textarea class="form-control" rows="5" cols="20" name="T1" id="T1" value="@yield('editT1')">@yield('editT1')</textarea>
                          </div>
                    </div>
                     <div class="col-sm-6">
                         <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >How long did it last?  </label>
                          <textarea class="form-control" rows="5" cols="20" name="T2" id="T2" value="@yield('editT2')">@yield('editT2')</textarea>
                          </div>
                    </div>
                 </div>
            </div>

            <div class="col-sm-12" style="margin-top: 15px;">
                             @if($orientation->notsignificantfornervoussystem == "true")
                            <button type="button" class="btnCustom btnCustom-default NervousSystem  custom" data-toggle="collapse" data-target="#NS" style="    margin-left: 1px; width: 82%;" id="" disabled>Nervous System<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                             @else
                              <button type="button" class="btnCustom btnCustom-default NervousSystem  custom" data-toggle="collapse" data-target="#NS" style="    margin-left: 1px; width: 82%;" id="" >Nervous System<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                              @endif
                              <div id="notsignificant">
                                <label class="switch">

                                  @if($orientation->notsignificantfornervoussystem == "true")
                                    <input type="checkbox" class="vision nsnss" name="notsignificantfornervoussystem" value="true" checked>
                                  @else

                                    <input type="checkbox" class="vision nsnss"  name="notsignificantfornervoussystem" value="true">

                                  @endif
                                <span class="slider round"></span>
                                 </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>






            </div>
            <div class="col-sm-12 collapse" id="NS">
               <div class="col-sm-12" style="margin-top: 15px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Orientation" style="    margin-left: 1px; width: 100%;" >Orientation<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
              </div>
            <div class="col-sm-12 collapse" id="Orientation">
                  <div class="col-sm-4" style="margin-top: 10px;">
                        <label>Person</label>
                        <select class="NS nsns" name="Person" id="Person"  value="@yield('editPerson')">
                                          <option value="@yield('editPerson')">@yield('editPerson')</option>
                                              <option value="No">No</option>
                                              <option value="Yes">Yes</option>
                                          </select>

                  </div>
                  <div class="col-sm-4" style="margin-top: 10px;">
                        <label>Place</label>
                        <select name="Place" class="NS nsns" id="Place"  value="@yield('editPlace')">
                                          <option value="@yield('editPlace')">@yield('editPlace')</option>
                                              <option value="No">No</option>
                                              <option value="Yes">Yes</option>
                                          </select>
                  </div>
                  <div class="col-sm-4" style="margin-top: 10px;">
                        <label>Time</label>
                        <select name="Time" class="NS nsns" id="Time"  value="@yield('editTime')">
                                          <option value="@yield('editTime')">@yield('editTime')</option>
                                              <option value="No">No</option>
                                              <option value="Yes">Yes</option>
                                          </select>
                  </div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                    <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Other Observations</label>
                           <textarea class="form-control nsns NS" rows="5" cols="20" name="orientationnotes" id="orientationnotes" value="">@yield('editorientationnotes')</textarea>
                </div>
              </div>
            </div>
                   <div class="col-sm-12" style="margin-top: 14px;">
                      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#gcs" style="    margin-left: 1px; width: 100%;">Glasgow Coma Scale(GCS)
                      <p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                 </div>
                 <div class="col-sm-12 collapse" id="gcs" style="margin-top: 11px;">
                    <div class="col-sm-4">
                            <label >Eye Opening </label>
                            <select class="NS nsns" name="Eyeopening" id="Eyeopening">
                      <!--     <option value="@yield('editEyeopening')" >@yield('editEyeopening')</option> -->
                                <option value="Spontaneously 4">Spontaneously</option>
                                <option value="To speech 3">To speech </option>
                                <option value="To pain 2">To pain</option>
                                <option value="No response 1">No response </option>
                              </select>
                              <input type="hidden" id="eyeopeningvalue" value="">
                    </div>
                     <div class="col-sm-4">
                            <label >Verbal Response </label>
                            <select class="NS nsns" name="verbalresponse" id="verbalresponse">
                        <!--   <option value="@yield('editverbalresponse')">@yield('editverbalresponse')</option> -->
                                <option value="Orientated time place person 5">Orientated to time place and person</option>
                                <option value="Confused 4">Confused</option>
                                <option value="Inappropriate Words 3">Inappropriate Words</option>
                                <option value="Incomprehensible Sounds 2">Incomprehensible Sounds</option>
                                <option value="No responce 1">No responce</option>
                              </select>
                              <input type="hidden" id="verbalresponsevalue" value="">
                    </div>
                     <div class="col-sm-4">
                            <label >Motor Response </label>
                            <select class="NS nsns" name="motorresponse" id="motorresponse">
                         <!--  <option value="@yield('editmotorresponse')">@yield('editmotorresponse')</option> -->


                                <option value="Obey commands 6">Obey commands</option>
                                <option value="moves to Localised pain 5">Moves to Localised Pain</option>
                                <option value="flexion withdrawal from 4">Flexion Withdrawal From </option>
                                <option value="abnormal fexion 3">Abnormal Flexion (Discorticate)</option>
                                <option value="abnormal edxtension 2">Abnormal Extension (Decerebrate)</option>
                                <option value="No responce 1">No responce</option>
                              </select>
                              <input type="hidden" id="motorresponsevalue" value="">
                    </div>
                    <div class="col-sm-4 totalscoregcs">
                            <label >Total Score </label>

                            <input type="text" value="" id="totalscoregcstoshow" name="totalscoregcs"  readonly style="border-bottom: 0px;">
                    </div>
                  </div>
            </div>
            <div class="col-sm-12" style="    margin-top: 16px;">
                          @if($memoryintact->notsignificantformemoryintact == "true")
                            <button type="button" class="btnCustom MemoryIntact btnCustom-default custom" data-toggle="collapse" data-target="#MemoryIntacts" style="    margin-left: 1px; width: 82%;"  disabled>Memory Intacts<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  " ><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                          @else
                            <button type="button" class="btnCustom MemoryIntact btnCustom-default custom" data-toggle="collapse" data-target="#MemoryIntacts" style="    margin-left: 1px; width: 82%;" >Memory Intacts<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                          @endif
                             <div id="notsignificant">
                                <label class="switch">

                                  @if($memoryintact->notsignificantformemoryintact == "true")
                                    <input type="checkbox" class="memoryintact mii" name="notsignificantformemoryintact" value="true" checked>
                                  @else

                                    <input type="checkbox" class="memoryintact mii"  name="notsignificantformemoryintact" value="true">

                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="MemoryIntacts">
                <div class="col-sm-6" style="    margin-top: 16px;">

                              <div class="col-sm-6" style="margin-top: 11px;">
                                           <label >Short Term Type</label>
                                          <select name="ShortTerm" id="ShortTerm" class="MI" value="@yield('editShortTerm')">
                                          <option value="@yield('editShortTerm')">@yield('editShortTerm')</option>
                                          @if('editShortTerm' == NULL)
                                          <option value="N/A" selected="selected">N/A</option>
                                          @else
                                          <option value="N/A">N/A</option>
                                          @endif
                                          <option value="Intact">Intact</option>
                                          <option value="Impaired">Impaired</option>
                                          </select>

                              </div>
                              <div class="col-sm-6" style="margin-top: 11px;">
                                           <label >Long Term Type</label>
                                          <select name="LongTerm" class="MI" id="LongTerm"  value="@yield('editLongTerm')">

                                            <option value="@yield('editLongTerm')">@yield('editLongTerm')</option>

                                          @if('editLongTerm' == NULL)
                                          <option value="N/A" selected="selected">N/A</option>
                                          @else
                                          <option value="N/A">N/A</option>
                                          @endif

                                          <option value="Intact">Intact</option>
                                          <option value="Impaired">Impaired</option>
                                          </select>

                              </div>
                          </div>
                          <div class="col-sm-12">
</div>                               <div class="col-sm-6" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control MI" rows="5" cols="20" name="memoryintactnotes" id="memoryintactnotes" value="">@yield('editmemoryintactnotes')</textarea>
                                </div>
                              </div>





            </div>
             <div class="col-sm-12" style="margin-top:16px;">
                        @if($communication->notsignificantforcommunication == "true")
                            <button type="button" class="btnCustom Communication btnCustom-default custom" data-toggle="collapse" data-target="#Communication" style="    margin-left: 1px; width: 82%;"  disabled>Communication<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @else
                            <button type="button" class="btnCustom Communication btnCustom-default custom" data-toggle="collapse" data-target="#Communication" style="    margin-left: 1px; width: 82%;" >Communication<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                        @endif
                             <div id="notsignificant">
                                <label class="switch">
                                    @if($communication->notsignificantforcommunication == "true")
                                    <input type="checkbox" class="communication cmns" name="notsignificantforcommunication" value="true" checked>
                                  @else

                                    <input type="checkbox" class="communication cmns"  name="notsignificantforcommunication" value="true">

                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Communication">
                  <div class="col-sm-4" style="margin-top: 10px;">
                                    <label >Language</label>

                                     <select id="Language" class="comm cmnsss" name="Language" multiple="Language">

                                      @foreach($language as $language)
                                      <option class="cmnsss" value="{{ $language->Languages}}">{{ $language->Languages}}</option>
                                      @endforeach
                                    </select>
                  </div>
                  <div class="col-sm-4" style="margin-top: 10px;">
                        <label >Adequate for All Activities</label>
                                    <select class="comm cmnsss" name="AdequateforAllActivities" id="AdequateforAllActivities"  value="@yield('editAdequateforAllActivities')">
                                      <option value="@yield('editAdequateforAllActivities')">@yield('editAdequateforAllActivities')</option>
                                      <option value="No">No</option>
                                      <option value="Yes">Yes</option>
                                    </select>
                  </div>
                  <div class="col-sm-4" style="margin-top: 10px;">
                        <label >Able to Communicate?</label>
                                    <select class="comm cmnsss" name="unableToCommunicate" id="unableToCommunicate"  value="@yield('editunableToCommunicate')">
                                      <option value="@yield('editunableToCommunicate')">@yield('editunableToCommunicate')</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                      </select>
                  </div>
                  <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control comm cmnsss" rows="5" cols="20" name="communicationnotes" id="communicationnotes" value="">@yield('editcommunicationnotes')</textarea>
                                </div>
                            </div>

            </div>
             <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom Vision1 btnCustom-default custom " data-toggle="collapse" data-target="#Vision" style="    margin-left: 1px; width: 82%;" >Vision<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                             <div id="notsignificant">
                                <label class="switch">

                                  @if($visionhearing->notsignificantforvision == "true")
                                    <input type="checkbox" class="vision1 vnss" name="notsignificantforvision" value="true" checked>
                                  @else

                                    <input type="checkbox" class="vision1 vnss"  name="notsignificantforvision" value="true">

                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Vision">
                  <div class="col-sm-3" style="margin-top: 10px;">
                                    <label >Type</label>
                                    <select name="Impared"  class="visionn" id="vimpared"  value="@yield('editImpared')">
                                      <option value="@yield('editImpared')">@yield('editImpared')</option>

                                       @if('editImpared' == NULL)
                                          <option value="N/A" selected="selected">N/A</option>
                                          @else
                                          <option value="N/A">N/A</option>
                                          @endif

                                          <option value="Impared">Impared</option>
                                          <option value="Intact">Intact</option>
                                        </select>
                   </div>








                 <div id="visiontype">
                  <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Sight Type</label>
                                    <select name="ShortSight" class="visionn" id="ShortSight"  value="@yield('editShortSight')">
                                      <option value="@yield('editShortSight')">@yield('editShortSight')</option>
                                          <option value="Short-Sight">Short-Sight</option>
                                          <option value="Long-Sight">Long-Sight</option>
                                        </select>
                  </div>
                  <!--  <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Long-Sight</label>
                                   <select name="LongSight" id="LongSight"  value="@yield('editLongSight')">
                                        <option value="@yield('editLongSight')">@yield('editLongSight')</option>
                                            <option value="No">NO</option>
                                            <option value="YES">YES</option>
                                          </select>
                  </div> -->
                  <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Wears Glasses</label>
                                    <select name="WearsGlasses" class="visionn" id="WearsGlasses"  value="@yield('editWearsGlasses')">
                                    <option value="@yield('editWearsGlasses')">@yield('editWearsGlasses')</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                      </select>
                  </div>

              </div>
              <div class="col-sm-12"></div>
               <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control visionn" class="visionn" rows="5" cols="20" name="visionnotes" id="visionnotes" value="">@yield('editvisionnotes')</textarea>
                                </div>
                            </div>
            </div>
            <div class="col-sm-12" style="margin-top:16px;">
                        @if($visionhearing->notsignificantforhear == "true")
                            <button type="button" class="btnCustom Hearing btnCustom-default custom" data-toggle="collapse" data-target="#Hearing" style="    margin-left: 1px; width: 82%;" disabled >Hearing <p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @else
                               <button type="button" class="btnCustom Hearing btnCustom-default custom" data-toggle="collapse" data-target="#Hearing" style="    margin-left: 1px; width: 82%;" >Hearing <p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                        @endif
                             <div id="notsignificant">
                                <label class="switch">





                                  @if($visionhearing->notsignificantforhear == "true")
                                    <input type="checkbox" class="hearing hnss" name="hearingnotsignificant" value="true" checked>
                                  @else
                                    <input type="checkbox" class="hearing hnss" name="hearingnotsignificant" value="true">
                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
             <div class="col-sm-12 collapse" id="Hearing">
                  <div class="col-sm-3" style="margin-top: 10px;">
                                    <label >Type</label>
                                    <select name="HImpared" class="Hearingg hns" id="himpared"  value="@yield('editHImpared')">
                                      <option value="@yield('editHImpared')">@yield('editHImpared')</option>
                                    <!--    @if('editHImpared' == NULL)
                                          <option value="N/A" selected="selected">N/A</option>
                                          @else
                                          <option value="N/A">N/A</option>
                                          @endif -->

                                          <option value="Impared">Impared</option>
                                          <option value="Intact">Intact</option>
                                        </select>
                  </div>
                  <div class="col-sm-3" style="margin-top: 10px;" id="heartype">
                        <label >Hearing Aids</label>
                                    <select name="HearingAids" class="Hearingg" id="HearingAids"  value="@yield('editHearingAids')">
                                      <option value="@yield('editHearingAids')">@yield('editHearingAids')</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                        </select>
                  </div>
 <div class="col-sm-12"></div>
                  <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control Hearingg hns"  rows="5" cols="20" name="hearingnotes" id="hearingnotes" value="">@yield('edithearingnotes')</textarea>
                                </div>
                            </div>
            </div>
              <div class="col-sm-12" style="margin-top:16px;">
                        @if($respiratory->notsignificantforrespiratory == "true")
                            <button type="button" class="btnCustom Respiratory btnCustom-default custom" data-toggle="collapse" data-target="#Respiratory" style="    margin-left: 1px; width: 82%;" disabled>Respiratory<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                         @else

                             <button type="button" class="btnCustom Respiratory btnCustom-default custom" data-toggle="collapse" data-target="#Respiratory" style="    margin-left: 1px; width: 82%;" >Respiratory<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                         @endif
                             <div id="notsignificant">
                                <label class="switch">
                                   @if($respiratory->notsignificantforrespiratory == "true")
                                    <input type="checkbox" class="respiratory rnss" name="notsignificantforrespiratory" value="true" checked>
                                  @else
                                    <input type="checkbox" class="respiratory rnss" name="notsignificantforrespiratory" value="true">
                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Respiratory">
                  <div class="col-sm-3" style="margin-top: 10px;">
                                       <label>SOB</label><input class="RES rns" type="text"  rows="5" name="SOB" id="SOB" value="@yield('editSOB')">
                  </div>
                  <div class="col-sm-3" style="margin-top: 10px;">

                                    <label>SPO2</label><input type="text" class="RES rns" rows="5" name="SPO2" id="SPO2" value="@yield('editSPO2')" style="width: 88%;" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    <div id="BPM" style="float: right;    margin-right: 6px;">%</div>
                  </div>
                 <!--  <div class="col-sm-3" style="margin-top: 10px;">
                                    <label >H/O TB/Asthma/COPD</label>
                                    <input type="text"  rows="5" name="H_OTB_Asthma_COPD" id="H_OTB_Asthma_COPD" value="@yield('editH_OTB_Asthma_COPD')">
                  </div>
 -->

                     <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Cough</label>
                                    <select name="Cough" class="RES rns" id="Cough"  value="@yield('editCough')">

                                      <option value="@yield('editCough')">@yield('editCough')</option>

                                    @if('editCough'==NULL)
                                    <option value="N/A" selected="selected">N/A</option>
                                    @else
                                    <option value="N/A" >N/A</option>
                                    @endif
                                      <option value="Productive">Productive</option>
                                      <option value="Non-Productive">Non-Productive</option>
                                      <option value="Productive (Dry)">Productive (Dry)</option>
                                      <option value="Non-Productive (Dry)">Non-Productive (Dry)</option>
                                    </select>
                  </div>

                   <div class="col-sm-3" style="margin-top: 10px;" id="ColorOfPhlegm1">
                        <label >Color Of Phlegm</label>
                                   <!--  <input type="text"  rows="5" name="ColorOfPhlegm" id="ColorOfPhlegm" value="@yield('editColorOfPhlegm')"> -->
                                    <select name="ColorOfPhlegm" class="RES" id="ColorOfPhlegm"  value="@yield('editColorOfPhlegm')">
                                      <option value="@yield('editColorOfPhlegm')">@yield('editColorOfPhlegm')</option>
                                            <option value="Yellowish">Yellowish</option>
                                            <option value="Greenish">Greenish</option>
                                            <option value="Blood Tinged">Blood Tinged</option>
                                            <option value="Smoky">Smoky</option>
                                          </select>
                  </div>
                  <div class="col-md-12"></div>
                   <div class="col-sm-3" style="margin-top: 19px;    min-height: 85px;">
                        <label >Nebulization Prescribed </label>
                                    <select name="Nebulization" class="RES rns" id="Nebulization"  value="@yield('editNebulization')">
                                      <option value="@yield('editNebulization')">@yield('editNebulization')</option>
                                            <option value="No">No</option>
                                            <option value="Yes">Yes</option>
                                          </select>
                                        <p class="cautiontoentermar">Please Enter MAR (Prescribed by) </p>
                  </div>
                <!--   <div class="col-sm-3" style="margin-top: 19px;" id="NebulizationMedicineName">
                        <label >Medicine Name </label>
                                    <select name="NebulizationMedicine" id="NebulizationMedicine"  value="@yield('NebulizationMedicine')">
                                      <option value="@yield('NebulizationMedicine')">@yield('NebulizationMedicine')</option>
                                            <option value="Ipravent respules">Ipravent respules</option>
                                            <option value="Duolin respules">Duolin respules</option>
                                            <option value="Levolin respules">Levolin respules</option>
                                            <option value="Budesal respules">Budesal respules</option>
                                            <option value="Pulmicort respules">Pulmicort respules</option>
                                          </select>
                  </div> -->
                   <div class="col-sm-3" style="margin-top: 19px;">
                        <label >Tracheostomy</label>
                                    <select name="Tracheostomy" class="RES rns" id="Tracheostomy"  value="@yield('editTracheostomy')">
                                    <option value="@yield('editTracheostomy')">@yield('editTracheostomy')</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                              </select>
                  </div>
                  <div class="col-sm-3" style="margin-top: 19px;">
                        <label >Ventilation Mode</label>
                                    <select name="CPAP_BIPAP" class="RES rns" id="CPAP_BIPAP"  value="@yield('editCPAP_BIPAP')">
                                    <option value="@yield('editCPAP_BIPAP')">@yield('editCPAP_BIPAP')</option>
                                    <option value="CMV">CMV</option>
                                    <option value="SIMV">SIMV</option>
                                    <option value="BiPAP">BIPAP</option>
                                    <option value="CPAP">CPAP</option>
                                  </select>

                  </div>
                    <div class="col-sm-3" style="margin-top: 19px;">
                        <label >ICD</label>
                                    <select name="ICD" class="RES rns" id="ICD"  value="@yield('editICD')">
                                    <option value="@yield('editICD')">@yield('editICD')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>

                  </div>
                  <div class="col-md-12"></div>
                  <div id="placementoficd1">
                     <div class="col-sm-6" style="margin-top: -16px;    margin-bottom: 36px;" >
                        <label >Placement of ICD</label>
                                    <select name="placementoficd" class="RES" id="placementoficd"  value="@yield('editplacementoficd')">
                                    <option value="@yield('editplacementoficd')">@yield('editplacementoficd')</option>
                                   <option value="Right fifth Intercostal and The Midaxillary Line">Right fifth Intercostal and The Midaxillary Line </option>
                                    <option value="Left fifth Intercostal and The Midaxillary Line">Left fifth Intercostal and The Midaxillary Line</option>
                                  </select>

                  </div>
                  <div class="col-sm-3" style="margin-top: -16px;">
                                       <label>Number of Days</label><input type="text" class="RES"  rows="5" name="noofdays" id="noofdays" value="@yield('editnoofdays')" style="width: 87%;">
                                       <div id="BPM" style="float: right;    margin-right: 6px;">situ</div>
                  </div>
                </div>
                <div class="col-md-12"></div>
                  <div class="col-sm-3" style="margin-top: -18px;">
                        <label >Central Cynaosis</label>
                                    <select name="central_cynaosis" class="RES rns" id="central_cynaosis" value="@yield('editcentral_cynaosis')">
                                    <option value="@yield('editcentral_cynaosis')">@yield('editcentral_cynaosis')</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                        </select>

                  </div>
                  <div class="col-sm-3" style="margin-top: -18px;">
                                    <label >History of</label>
                                  <!--   <input type="text"  rows="5" name="H_OTB_Asthma_COPD" id="H_OTB_Asthma_COPD" value="@yield('editH_OTB_Asthma_COPD')"> -->
                                <!--   <select name="H_OTB_Asthma_COPD" id="H_OTB_Asthma_COPD" >
                                      <option value="@yield('editH_OTB_Asthma_COPD')">@yield('editH_OTB_Asthma_COPD')</option>
                                      <option value="TB">TB</option>
                                      <option value="Asthama">Asthama</option>
                                      <option value="COPD">COPD</option>
                                      <option value="Pnemonia">Pnemonia</option>
                                    </select> -->
                                    <select id="H_OTB_Asthma_COPD" class="RES" name="H_OTB_Asthma_COPD" multiple="multiple">

                                      <option class="rns" value="@yield('editH_OTB_Asthma_COPD')">@yield('editH_OTB_Asthma_COPD')</option>
                                      <option class="rns" value="TB">TB</option>
                                      <option class="rns" value="Asthama">Asthama</option>
                                      <option class="rns" value="COPD">COPD</option>
                                      <option class="rns" value="Pnemonia">Pnemonia</option>

                                    </select>
                  </div>
                   <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control RES rns" rows="5" cols="20" name="respiratorynotes" id="respiratorynotes">@yield('editrespiratorynotes')</textarea>
                                </div>
                    </div>
            </div>
             <div class="col-sm-12" style="margin-top:16px;">
                        @if($position->notsignificantforposition == "true")
                            <button type="button" class="btnCustom Position btnCustom-default custom" data-toggle="collapse" data-target="#Position" style="    margin-left: 1px; width:82%;" disabled>Position<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                         @else
                          <button type="button" class="btnCustom Position btnCustom-default custom" data-toggle="collapse" data-target="#Position" style="    margin-left: 1px; width:82%;" >Position<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>


                         @endif
                            <div id="notsignificant">
                                <label class="switch">
                                   @if($position->notsignificantforposition == "true")
                                    <input type="checkbox" class="position pnss" name="notsignificantforposition" value="true" checked>
                                  @else
                                    <input type="checkbox" class="position pnss" name="notsignificantforposition" value="true">
                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Position">
                  <div class="col-sm-3" style="margin-top: 10px;">
                                    <label >Position Type</label>
                              <!--      <select name="Fowler" id="Fowler"  value="@yield('editFowler')">
                                    <option value="@yield('editFowler')">@yield('editFowler')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                      </select> -->
                                        <select id="Position_Type" class="Positionn" name="Position_Type" multiple="multiple">

                                     <option class="multiselectt" value="Fowlers">Fowlers</option>
                                      <option class="multiselectt" value="Supine">Supine</option>
                                      <option class="multiselectt" value="Right_Lateral">Right_Lateral</option>
                                      <option class="multiselectt" value="Left_Lateral">Left_Lateral</option>

                                    </select>

                   </div>

                    <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control Positionn multiselectt" rows="5" cols="20" name="positionnotes" id="positionnotes" value="">@yield('editpositionnotes')</textarea>
                                </div>
                            </div>
</div>
                 <!--  <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Supine</label>
                                    <select name="Supine" id="Supine"  value="@yield('editSupine')">
                                    <option value="@yield('editSupine')">@yield('editSupine')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>
                  </div>
                   <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Rt_Lateral</label>
                                   <select name="Rt_Lateral" id="Rt_Lateral"  value="@yield('editRt_Lateral')">
                                    <option value="@yield('editRt_Lateral')">@yield('editRt_Lateral')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>
                  </div>
                  <div class="col-sm-3" style="margin-top: 10px;">
                        <label >Lt_Lateral</label>
                                     <select name="Lt_Lateral" id="Lt_Lateral"  value="@yield('editLt_Lateral')">
                                      <option value="@yield('editLt_Lateral')">@yield('editLt_Lateral')</option>
                                      <option value="No">No</option>
                                      <option value="Yes">Yes</option>
                                    </select>
                  </div> -->


            <div class="col-sm-12" style="margin-top:16px;">
                         @if($circulatory->notsignificantforcirculatory == "true")
                            <button type="button" class="btnCustom Circulatory btnCustom-default custom" data-toggle="collapse" data-target="#Circulatory" style="    margin-left: 1px; width: 82%;" disabled>Circulatory<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                         @else
                               <button type="button" class="btnCustom Circulatory btnCustom-default custom" data-toggle="collapse" data-target="#Circulatory" style="    margin-left: 1px; width: 82%;" >Circulatory<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>


                         @endif
                             <div id="notsignificant">
                                <label class="switch">
                                   @if($circulatory->notsignificantforcirculatory == "true")
                                    <input type="checkbox" class="circulatory cnss" name="notsignificantforcirculatory" value="true" checked>
                                  @else
                                    <input type="checkbox" class="circulatory cnss" name="notsignificantforcirculatory" value="true">
                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Circulatory">
                  <div class="col-sm-4" style="margin-top: 15px;">
                                    <label >Chest Pain</label>
                                   <input type="text" class="circulatoryy cns" rows="5" name="ChestPain" id="ChestPain" value="@yield('editChestPain')" >
                   </div>

                  <div class="col-sm-4" style="margin-top: 15px;">
                        <label >History of</label>
                                   <!--  <input type="text" rows="5" name="hoHTNCADCHF" id="hoHTNCADCHF" value="@yield('edithoHTNCADCHF')"> -->
                                    <!--  <select name="hoHTNCADCHF" id="hoHTNCADCHF" value="@yield('edithoHTNCADCHF')">
                                      <option value="@yield('edithoHTNCADCHF')">@yield('edithoHTNCADCHF')</option>
                                      <option value="DM">DM</option>
                                      <option value="IHD">IHD</option>
                                      <option value="HTN">HTN</option>
                                      <option value="CAD">CAD</option>
                                      <option value="CHF">CHF</option>
                                    </select> -->
                                       <select id="hoHTNCADCHF" class="circulatoryy" name="hoHTNCADCHF" multiple="multiple">

                                         {{-- <option value="@yield('edithoHTNCADCHF')">@yield('edithoHTNCADCHF')</option> --}}
                                      <option class="cns" value="DM">DM</option>
                                      <option class="cns" value="IHD">IHD</option>
                                      <option class="cns" value="HTN">HTN</option>
                                      <option class="cns" value="CAD">CAD</option>
                                      <option class="cns" value="CHF">CHF</option>

                                    </select>
                  </div>
                 <!--   <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Heart Rate</label>
                                   <input type="text" rows="5" name="HR" id="HR" value="@yield('editHR')" style="    width: 91%;">
                                   <div id="BPM" style="float: right;">BPM</div>
                  </div> -->
                  <div class="col-sm-4" style="margin-top: 18px;">
                        <label >Peripheral Cyanosis</label>
                                      <select class="circulatoryy cns" name="PeripheralCyanosis" id="PeripheralCyanosis" value="@yield('editPeripheralCyanosis')">
                                      <option value="@yield('editPeripheralCyanosis')">@yield('editPeripheralCyanosis')</option>
                                      <option value="No">No</option>
                                      <option value="Yes">Yes</option>
                                    </select>
                  </div>
                  <div class="col-md-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                        <label >Jugular Vein</label>
                                      <select class="circulatoryy cns" name="JugularVein" id="JugularVein" value="@yield('editJugularVein')">
                                      <option value="@yield('editJugularVein')">@yield('editJugularVein')</option>
                                      <option value="Flat">Flat</option>
                                      <option value="Distended">Distended</option>
                                    </select>
                  </div>
                  <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                       <div class="form-group" style="width: 99%;margin-left: 0px;">
                            <label >Surgery History</label>
                           <textarea class="form-control circulatoryy cns" rows="5" cols="20" name="SurgeryHistory" id="SurgeryHistory" value="@yield('editSurgeryHistory')">@yield('editSurgeryHistory')</textarea>
                           </div>
                  </div>
                     <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control circulatoryy cns" rows="5" cols="20" name="circulatorynotes" id="circulatorynotes" >@yield('editcirculatorynotes')</textarea>
                                </div>
                            </div>
            </div>

            <div class="col-sm-12" style="margin-top:16px;">
                        @if($denture->notsignificantfordenture == "true")
                            <button type="button" class="btnCustom Denture btnCustom-default custom" data-toggle="collapse" data-target="#Denture" style="    margin-left: 1px; width: 82%;" disabled >Denture<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @else
                            <button type="button" class="btnCustom Denture btnCustom-default custom" data-toggle="collapse" data-target="#Denture" style="    margin-left: 1px; width: 82%;" >Denture<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @endif
                         <div id="notsignificant">

                                <label class="switch">

                                   @if($denture->notsignificantfordenture == "true")
                                    <input type="checkbox" class="denture dnss" name="notsignificantfordenture" value="true" checked>
                                  @else
                                    <input type="checkbox" class="denture dnss" name="notsignificantfordenture" value="true">
                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Denture">
                  <div class="col-sm-4" style="margin-top: 15px;">
                                    <label >Upper</label>
                                   <select name="Upper" id="Upper"  class="denturee dns" value="@yield('editUpper')">
                                  <option value="@yield('editUpper')">@yield('editUpper')</option>
                                  <option value="No">No</option>
                                  <option value="Yes">Yes</option>
                                </select>
                   </div>

                  <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Lower</label>
                                    <select name="Lower" id="Lower" class="denturee dns" value="@yield('editLower')">
                                      <option value="@yield('editLower')">@yield('editLower')</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                        </select>
                  </div>
                   <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Need assistance in cleaning</label>
                                   <select name="Cleaning" class="denturee dns" id="Cleaning"  value="@yield('editCleaning')">
                                  <option value="@yield('editCleaning')">@yield('editCleaning')</option>
                                  <option value="No">No</option>
                                  <option value="Yes">Yes</option>
                                </select>
                  </div>

                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control denturee dns" rows="5" cols="20" name="denturenotes" id="denturenotes">@yield('editdenturenotes')</textarea>
                                </div>
                            </div>
            </div>

            <div class="col-sm-12" style="margin-top:16px;">
                      @if($nutrition->notsignificantfornutrition == "true")
                            <button type="button" class="btnCustom Nutrition btnCustom-default custom" data-toggle="collapse" data-target="#Nutrition" style="    margin-left: 1px; width: 82%;" disabled>Nutrition<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                      @else
                           <button type="button" class="btnCustom Nutrition btnCustom-default custom" data-toggle="collapse" data-target="#Nutrition" style="    margin-left: 1px; width: 82%;" >Nutrition<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                      @endif
                         <div id="notsignificant">
                                <label class="switch">

                                   @if($nutrition->notsignificantfornutrition == "true")
                                    <input type="checkbox"  class="nutrition nnss" name="notsignificantfornutrition" value="true" checked>
                                  @else
                                    <input type="checkbox"   class="nutrition nnss" name="notsignificantfornutrition" value="true">
                                  @endif

                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Nutrition">


                  <div class="col-sm-6" style="margin-top: 15px;">
                        <label >Type</label>
                                    <select name="Type" class="nutritionn nns" id="Type"  value="@yield('editType')">
                                  <option value="@yield('editType')">@yield('editType')</option>
                                      <option value="Veg">Veg</option>
                                      <option value="Non-Veg">Non-Veg</option>
                                    </select>
                  </div>

                  <div class="col-sm-6" style="margin-top: 15px;">
                                    <label >Adequacy</label>
                                   <select name="Intact" class="nutritionn nns" id="Intact"  value="@yield('editIntact')">
                                  <option value="@yield('editIntact')">@yield('editIntact')</option>
                                      <option value="Adequate">Adequate</option>
                                      <option value="Inadequate">Inadequate</option>
                                    </select>
                   </div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control nutritionn nns" rows="5" cols="20" name="nutritionnotes" id="nutritionnotes" >@yield('editnutritionnotes')</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Diet1" style="    margin-left: 1px; width:100%;" >Diet<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                          <!-- <div id="notsignificant">
                                <label class="switch">
                                  <input type="checkbox" >
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>  -->
            </div>
            <div class="col-sm-12 collapse" id="Diet1">
                        <div class="col-sm-3" style="margin-top: 15px;">
                                    <label >Diet Type</label>
                                   <select class="nutritionn nns" name="Diet" id="Diet"  >
                                    <option value="@yield('editDiet')">@yield('editDiet')</option>
                                    <option value="NBO/NPO">NBO/NPO</option>
                                        <option value="Bland">Bland</option>
                                        <option value="Minced">Minced</option>
                                        <option value="Liquid">Liquid</option>
                                        <option value="Diabetic">Diabetic</option>
                                        <option value="Salt Restricted">Salt Restricted</option>
                                        <option value="Protein Restricted">Protein Restricted</option>
                                        <option value="Fat Free">Low Fat</option>
                                        <option value="High Carbohydrate">High Carbohydrate</option>
                                        <option value="High Protein">High Protein</option>

                                    </select>
                        </div>
                          <div class="col-sm-3" style="margin-top: 15px;">
                                    <label >Feeding Type</label>
                                   <select class="nutritionn nns" name="feedingtype" id="feedingtype"  value="@yield('editfeedingtype')">
                                    <option value="@yield('editfeedingtype')">@yield('editfeedingtype')</option>
                                    <option value="Oral" id="oral">Oral</option>
                                        <option value="RT">RT</option>
                                        <option value="PEG">PEG</option>
                                        <option value="TPN">TPN</option>


                                    </select>
                        </div>

                  <div class="col-sm-3 eating" style="margin-top: 15px;" id="breakfast">
                        <label >Break Fast Time</label>
                                <!--     <input type="text"  rows="5" name="BFTime" id="BFTime" value="@yield('editBFTime')" style="    width: 91%;"> -->
                                  <!--   <div id="BPM" style="float: right;">AM</div> -->
                                  <input  class="nutritionn" type="text" id="BFTime" name="BFTime"  value="@yield('editBFTime')" style="    width: 91%;"/>
                  </div>
                   <div class="col-sm-3 eating" style="margin-top: 15px;" id="lunchtime">
                        <label >Lunch Time</label>
                                   <!--  -->
                                   <!--  <div id="BPM" style="float: right;">PM</div> -->
                                   <input class="nutritionn" type="text" id="LunchTime" name="LunchTime"  value="@yield('editLunchTime')" style="    width: 91%;"/>
                  </div>
                    <div class="col-sm-3 eating" style="margin-top: 15px;" id="sanckstime">
                        <label >Snacks Time</label>
                                   <!-- <input type="text"  rows="5" name="SnacksTime" id="SnacksTime" value="@yield('editSnacksTime')" style="    width: 91%;"> -->
                                   <!--  <div id="BPM" style="float: right;">PM</div> -->
                                    <input type="text" class="nutritionn" id="SnacksTime" name="SnacksTime"  value="@yield('editSnacksTime')" style="    width: 91%;"/>
                  </div>
                   <div class="col-sm-3 eating" style="margin-top: 19px;" id="dinnertime">
                                    <label >Dinner Time</label>
                                  <!--  <input type="text"  rows="5" name="DinnerTime" id="DinnerTime" value="@yield('editDinnerTime')" style="    width: 91%;"> -->
                                <!--     <div id="BPM" style="float: right;">PM</div> -->
                                <input type="text" class="nutritionn" id="DinnerTime" name="DinnerTime"  value="@yield('editDinnerTime')" style="    width: 91%;"/>
                    </div>
                     <div class="col-sm-3" style="margin-top: 14px;" id="rtfeeding">
                        <label >RT Feeding</label>
                                   <input type="text" class="nutritionn"  rows="5" name="RTFeeding" id="RTFeeding" value="@yield('editRTFeeding')" style="    width: 72%;">
                                  <!--  <select name="RTFeeding_Measured_In" id="RTFeeding_Measured_In"  value="@yield('editRTFeeding_Measured_In')" style="width: 18%;">
                                              <option value="@yield('editRTFeeding_Measured_In')">@yield('editRTFeeding_Measured_In')</option>
                                              <option value="ml">ml</option>
                                              <option value="hr">hr</option>
                                          </select> -->
                                    <div id="BPM" style="float: right;    margin-right: 27px;">ml</div>
                  </div>

                    <div class="col-sm-3" style="margin-top: 14px;" id="pegfeeding">
                        <label >PEG Feeding</label>
                                  <input type="text" class="nutritionn" rows="5" name="PEGFeeding" id="PEGFeeding" value="@yield('editPEGFeeding')" style="    width: 72%;">
                              <!--      <select name="PEGFeeding_Measured_In" id="PEGFeeding_Measured_In"  value="@yield('editPEGFeeding_Measured_In')" style="width: 18%;">
                                              <option value="@yield('editPEGFeeding_Measured_In')">@yield('editPEGFeeding_Measured_In')</option>
                                              <option value="ml">ml</option>
                                              <option value="hr">hr</option>
                                          </select> -->
                                          <div id="BPM" style="float: right;    margin-right: 27px;">ml</div>
                  </div>

                    <div class="col-sm-3" style="margin-top: 14px;" id="frequency">
                        <label >Frequency</label>

                                   <select name="frequency" class="nutritionn" id="frequency1"  value="" stye="width: 18%;">
                                              <option value="@yield('editfrequency')">@yield('editfrequency')</option>
                                              <option value="30 min">30 min</option>
                                              <option value="1 hr"> 1 hr</option>
                                              <option value="2 hr"> 2 hr</option>
                                              <option value="3 hr"> 3 hr</option>
                                              <option value="4 hr"> 4 hr</option>
                                              <option value="6 hr"> 6 hr</option>
                                          </select>

                  </div>

                  <div id="tpnn">
                  <div class="col-sm-3" style="margin-top: 14px;" >
                        <label >TPN Type</label>
                                   <!--  <input type="text"  rows="5" name="TPN" id="TPN" value="@yield('editTPN')"> --> <select class="nutritionn" name="TPN" id="TPN"  value="@yield('editTPN')">
                                    <option value="@yield('editTPN')">@yield('editTPN')</option>
                                    <option value="low concentration">Low Concentration</option>
                                        <option value="high concentration">High Concentration</option>



                                    </select>
                  </div>
                   <div class="col-sm-3" style="margin-top: 14px;" >
                        <label >Routes</label>
                                   <!--  <input type="text"  rows="5" name="TPN" id="TPN" value="@yield('editTPN')"> -->
                                   <select name="routes" class="nutritionn" id="routes"  value="@yield('editroutes')">
                                    <option value="@yield('editroutes')">@yield('editroutes')</option>
                                    <option value="Central line">Central line</option>
                                        <option value="Peripheral line">Peripheral line</option>



                                    </select>
                  </div>
                  <div class="col-md-12"></div>
                    <div class="col-sm-3" style="margin-top: 19px;" >
                        <label >Quantity</label><br>
                                   <input type="text"  class="nutritionn" rows="5" name="Quantityinfusion" id="Quantityinfusion" value="@yield('editQuantityinfusion')" style="    width: 88%;" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                  <!--  <select name="RTFeeding_Measured_In" id="RTFeeding_Measured_In"  value="@yield('editRTFeeding_Measured_In')" style="width: 18%;">
                                              <option value="@yield('editRTFeeding_Measured_In')">@yield('editRTFeeding_Measured_In')</option>
                                              <option value="ml">ml</option>
                                              <option value="hr">hr</option>
                                          </select> -->
                                    <div id="BPM" style="float: right;    margin-right: 7px;">ml</div>
                  </div>
                      <div class="col-sm-3" style="margin-top: 19px;" >
                        <label >Time</label><br>
                                   <input type="text" class="nutritionn"  rows="5" name="timeinfusion" id="timeinfusion" value="@yield('edittimeinfusion')" style="    width: 88%;" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="2">
                                  <!--  <select name="RTFeeding_Measured_In" id="RTFeeding_Measured_In"  value="@yield('editRTFeeding_Measured_In')" style="width: 18%;">
                                              <option value="@yield('editRTFeeding_Measured_In')">@yield('editRTFeeding_Measured_In')</option>
                                              <option value="ml">ml</option>
                                              <option value="hr">hr</option>
                                          </select> -->
                                    <div id="BPM" style="float: right;    margin-right: 7px;">hr</div>
                  </div>
                   <div class="col-sm-3" style="margin-top: 19px;" >
                        <label >Infusion Rate</label><br>
                                   <input type="text"  rows="5"  class="nutritionn"name="infusionratedropspermin" id="infusionratedropspermin" value="@yield('editinfusionratedropspermin')" style="    width: 84%;border-bottom:0px;" readonly>
                                  <!--  <select name="RTFeeding_Measured_In" id="RTFeeding_Measured_In"  value="@yield('editRTFeeding_Measured_In')" style="width: 18%;">
                                              <option value="@yield('editRTFeeding_Measured_In')">@yield('editRTFeeding_Measured_In')</option>
                                              <option value="ml">ml</option>
                                              <option value="hr">hr</option>
                                          </select> -->

                  </div>


                </div>
                 <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control nutritionn nns" rows="5" cols="20" name="diettnotes" id="diettnotes" >@yield('editdiettnotes')</textarea>
                                </div>
                            </div>
            </div>
            </div>

             <div class="col-sm-12" style="margin-top:16px;">
                        @if($abdomen->notsignificantforabdomen == "true")
                            <button type="button" class="btnCustom Abdomen btnCustom-default custom" data-toggle="collapse" data-target="#Abdomen" style="    margin-left: 1px; width: 82%;" disabled>Abdomen<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @else
                            <button type="button" class="btnCustom Abdomen btnCustom-default custom" data-toggle="collapse" data-target="#Abdomen" style="    margin-left: 1px; width: 82%;" >Abdomen<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                        @endif
                             <div id="notsignificant">
                                <label class="switch">
                                   @if($abdomen->notsignificantforabdomen == "true")
                                    <input type="checkbox" class="abdomen adnss" name="notsignificantforabdomen" value="true" checked>
                                  @else
                                    <input type="checkbox" class="abdomen adnss" name="notsignificantforabdomen" value="true">
                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
             <div class="col-sm-12 collapse" id="Abdomen">
                  <!-- <div class="col-sm-4" style="margin-top: 15px;">
                                    <label >Inspection</label>
                                   <input type="text"  rows="5" name="Inspection" id="Inspection" value="@yield('editInspection')">
                   </div> -->
                <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#Inspection" style="    margin-left: 1px; width: 100%;" >Inspection<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                  </div>
                              <div class="col-sm-12 collapse" id="Inspection">
                        <div class="col-sm-4" style="margin-top: 15px;">
                           <label >Shape </label>
                        <select class="abdomenn adns" name="Shape" id="Shape"  >
                           <option value="@yield('editShape')">@yield('editShape')</option>
                          <option value="Flat">Flat</option>
                          <option value="Distended">Distended</option>
                          <option value="Bulge">Bulge</option>
                        </select>
                        </div>
                          <div class="col-sm-3 girth" style="margin-top: 15px;">
                                    <label >Girth</label><br>
                                   <input type="text"  rows="5" name="girth" id="girth" value="@yield('editgirth')" style="width: 88%;">
                                   <p style="float: right;">cm</p>
                   </div>


                     <div class="col-sm-4 changeincolor" style="margin-top: 15px;">
                           <label > Change in Color </label>
                        <select name="changeincolor" class="abdomenn" id="changeincolor"  value="">
                           <option value="@yield('editchangeincolor')">@yield('editchangeincolor')</option>
                          <option value="Brusing / Purple Around Umbilical ">Brusing / Purple Around Umbilical </option>
                          <option value="Brusing / Purple Around Flange">Brusing / Purple Around Flange</option>

                        </select>
                    </div>
                     <div class="col-sm-12"></div>
                     <div class="col-sm-4 BulgeType" style="margin-top: 15px;">
                           <label >Bulge Type </label>
                        <select name="BulgeType" class="abdomenn" id="BulgeType"  value="">
                           <option value="@yield('editBulgeType')">@yield('editBulgeType')</option>
                          <option value="Symmetry ">Symmetry </option>
                          <option value="Asymmetry">Asymmetry</option>

                        </select>
                    </div>

                  </div>
                     <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Palpation</label>
                                   <!--  <input type="text"  rows="5" name="Palpation" id="Palpation" value="@yield('editPalpation')"> -->
                                     <select name="Palpation" class="abdomenn adns" id="Palpation"  value="@yield('editPalpation')">
                                           <option value="@yield('editPalpation')">@yield('editPalpation')</option>
                                          <option value="Soft to Touch">Soft to Touch</option>
                                          <option value="Hard to Touch">Hard to Touch</option>
                                          <option value="Tender">Tender</option>
                                        </select>
                  </div>
                  <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Ausculation of Bowel Sound</label>
                        <!-- $title = app()->view->getSections()['title']; -->
                                    <select class="abdomenn adns" name="AusculationofBS" id="AusculationofBS"  >
                                     <option value="@yield('editAusculationofBS')">@yield('editAusculationofBS')</option>

                                    <option value="Absent" >Absent</option>
                                    <option value="Present">Present</option>
                                  </select>
                  </div>

                  <div class="col-sm-4" style="margin-top: 15px;">
                                    <label >Percussion</label>
                                   <input type="text"  rows="5" class="abdomenn adns" name="Percussion" id="Percussion" value="@yield('editPercussion')">
                                   <!--  <select name="Percussion" id="Percussion"  value="@yield('editPercussion')">
                                           <option value="@yield('editPercussion')">@yield('editPercussion')</option>
                                          <option value="No">Value 1</option>
                                          <option value="Yes">Value 2</option>
                                        </select> -->
                   </div>

                  <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Ileostomy</label>

                                   <select name="Ileostomy" class="abdomenn adns" id="Ileostomy"  value="@yield('editIleostomy')">
                                           <option value="@yield('editIleostomy')">@yield('editIleostomy')</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                        </select>
                  </div>
                   <div class="col-sm-4" style="margin-top: 15px;">
                   <label >Colostomy Functioning </label>
                        <select name="Colostomy" class="abdomenn adns" id="ColostomyFunctioning"  value="@yield('editColostomy')">
                           <option value="@yield('editColostomy')">@yield('editColostomy')</option>
                          <option value="No">No</option>
                          <option value="Yes">Yes</option>
                        </select>

                  </div>
                   <div class="col-sm-12"></div>
                          <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control abdomenn adns" rows="5" cols="20" name="abdomennotess" id="abdomennotess">@yield('editabdomennotess')</textarea>
                                </div>
                            </div>



            </div>

             <div class="col-sm-12" style="margin-top:16px;">
                          @if($genito->notsignificantforurinary == "true")
                            <button type="button" class="btnCustom Elimination1 btnCustom-default custom" data-toggle="collapse" data-target="#Elimination" style="    margin-left: 1px; width: 82%;" disabled >Elimination<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                        @else

                          <button type="button" class="btnCustom Elimination1 btnCustom-default custom" data-toggle="collapse" data-target="#Elimination" style="    margin-left: 1px; width: 82%;" >Elimination<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>

                        @endif
                             <div id="notsignificant">
                                <label class="switch">
                                  @if($genito->notsignificantforurinary == "true")
                                    <input type="checkbox" class="elimination enss" name="notsignificantforurinary" value="true" checked>
                                  @else
                                    <input type="checkbox" class="elimination enss" name="notsignificantforurinary" value="true"> 
                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
            <div class="col-sm-12 collapse" id="Elimination">


             <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#UrinaryContinence" style="    margin-left: 1px; width: 100%;" >Urinary Continence<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
            </div>
             <div class="col-sm-12 collapse" id="UrinaryContinence">
                   <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Urinary Continence</label>
                                    <select class="eliminationn ens" name="UrinaryContinent"  id="UrinaryContinent"  value="@yield('editUrinaryContinent')">
                                    <option value="@yield('editUrinaryContinent')">@yield('editUrinaryContinent')</option>
                                    <option value="Completely Continet">Completely Continet</option>
                                    <option value="Incontinent Urine Occasionally">Incontinent Urine Occasionally</option>
                                    <option value="Incontinent Urine Night Only">Incontinent Urine Night Only</option>
                                    <option value="Incontinent Urine Always">Incontinent Urine Always</option>
                                  </select>
                  </div>

                   <div class="col-sm-4 typeofurinary" style="margin-top: 10px;">
                                    <label >Type</label>
                              <!--      <select name="Fowler" id="Fowler"  value="@yield('editFowler')">
                                    <option value="@yield('editFowler')">@yield('editFowler')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                      </select> -->
                                        <select id="Typeofurinary" class="eliminationn" name="Typeofurinary" multiple="multiple">

                                     <option value="Independent">Independent</option>
                                    <option value="Urinal">Urinal</option>
                                    <option value="Bedpan">Bedpan</option>
                                    <option value="BathroomRoutine">BathroomRoutine</option>
                                    <option value="Daiper/Breifs">Daiper/Breifs</option>
                                     <option value="Condom Catheter">Condom Catheter</option>
                                  <option value="Catheter">Catheter</option>
                                  <option value="On Dialysis">On Dialysis</option>
                                    </select>

                   </div>
                    <div class="col-sm-4 Needassistanceofurinary" style="margin-top: 15px;" >
                        <label >Need Assistance</label>
                                    <select name="Needassistanceofurinary" class="eliminationn" id="Needassistanceofurinary"  value="">
                                    <option value="@yield('editNeedassistanceofurinary')">@yield('editNeedassistanceofurinary')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                    </select>
                  </div>
                   <div class="col-sm-12"></div>
                   <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control eliminationn ens" rows="5" cols="20" name="urinarynotes" id="urinarynotes">@yield('editurinarynotes')</textarea>
                                </div>
                            </div>
                  <!-- <div class="col-sm-4" style="margin-top: 15px;">
                                    <label >Completely Continet</label>
                                   <select name="CompletelyContinet" id="CompletelyContinet"  value="@yield('editCompletelyContinet')">
                                    <option value="@yield('editCompletelyContinet')">@yield('editCompletelyContinet')</option>
                                    <option value="Indepentdent">Indepentdent</option>
                                    <option value="Urinal/Bedpan">Urinal/Bedpan</option>
                                    <option value="BathroomRoutine">BathroomRoutine</option>
                                  </select>
                   </div>

                  <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Incontinent Urine Occasionally</label>

                                   <select name="IncontinentUrineOccasionally" id="IncontinentUrineOccasionally"  value="@yield('editIncontinentUrineOccasionally')">
                                    <option value="@yield('editIncontinentUrineOccasionally')">@yield('editIncontinentUrineOccasionally')</option>
                                    <option value="Assistance required">Assistance required</option>
                                    <option value="Daiper/Breifs">Daiper/Breifs</option>
                                  </select>
                  </div>
                   <div class="col-sm-4" style="margin-top: 15px;">
                   <label >Incontinent Urine Night Only </label>
                        <select name="IncontinentUrineOccasionally" id="IncontinentUrineOccasionally"  value="@yield('editIncontinentUrineOccasionally')">
                        <option value="@yield('editIncontinentUrineOccasionally')">@yield('editIncontinentUrineOccasionally')</option>
                        <option value="Assistance required">Assistance required</option>
                        <option value="Daiper/Breifs">Daiper/Breifs</option>
                      </select>

                  </div>

                    <div class="col-sm-4" style="margin-top: 15px;">
                   <label >Incontinent Urine Always </label>
                        <select name="IncontinentUrineAlways" id="IncontinentUrineAlways"  value="@yield('editIncontinentUrineAlways')">
                        <option value="@yield('editIncontinentUrineAlways')">@yield('editIncontinentUrineAlways')</option>
                        <option value="Condom Catheter">Condom Catheter</option>
                        <option value="Catheter">Catheter</option>
                      </select>
                  </div> -->

            </div>

            <div class="col-sm-12" style="margin-top:16px;">
                            <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#FecalContinent" style="    margin-left: 1px; width: 100%;" >Fecal Continence<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
            </div>
             <div class="col-sm-12 collapse" id="FecalContinent">
                   <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Fecal Continence</label>
                                     <select name="FecalType" id="FecalType" class="eliminationn ens" value="@yield('editFecalType')">
                                    <option value="@yield('editFecalType')">@yield('editFecalType')</option>
                                    <option value="Continent">Continent</option>
                                    <option value="Incontinent">Incontinent</option>
                                  </select>
                  </div>

                   <div class="col-sm-4 typeoffecal" style="margin-top: 10px;">
                                    <label >Type </label>
                              <!--      <select name="Fowler" id="Fowler"  value="@yield('editFowler')">
                                    <option value="@yield('editFowler')">@yield('editFowler')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                      </select> -->
                                        <select id="Typeoffecal" class="eliminationn" name="Typeoffecal" multiple="multiple">

                                     <option value="Independent">Independent</option>
                                    <option value="Urinal">Urinal</option>
                                    <option value="Bedpan">Bedpan</option>
                                    <option value="BathroomRoutine">BathroomRoutine</option>
                                    <option value="Daiper/Breifs">Daiper/Breifs</option>
                                     <option value="Condom Catheter">Condom Catheter</option>
                                  <option value="Catheter">Catheter</option>
                                  <option value="Incontinent Occasionally">Incontinent Occasionally</option>
                                  <option value="Incontinent Always">Incontinent Always</option>
                                  <option value="Commode chair">Commode chair</option>
                                   <option value="On Dialysis">On Dialysis</option>
                                    </select>

                   </div>
                    <div class="col-sm-4 Needassistanceoffecal" style="margin-top: 15px;" >
                        <label >Need Assistance</label>
                                    <select name="Needassistanceoffecal" class="eliminationn"id="Needassistanceoffecal"  value="">
                                    <option value="@yield('editNeedassistanceoffecal')">@yield('editNeedassistanceoffecal')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                    </select>
                  </div>
                   <div class="col-sm-12"></div>
                     <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control eliminationn ens" rows="5" cols="20" name="fecalnotes" id="fecalnotes">@yield('editfecalnotes')</textarea>
                                </div>
                            </div>
                 <!--  <div class="col-sm-3" style="margin-top: 15px;" id="IncontinentOccasionallydiv">
                                    <label >Incontinent Occasionally</label>
                                   <select name="IncontinentOccasionally" id="IncontinentOccasionally value="@yield('editIncontinentOccasionally')">
                                  <option value="@yield('editIncontinentOccasionally')">@yield('editIncontinentOccasionally')</option>
                                  <option value="No">No</option>
                                  <option value="Yes">Yes</option>
                                </select>
                   </div>

                  <div class="col-sm-3" style="margin-top: 15px;" id="IncontinentAlwaysdiv">
                        <label >Incontinent Always</label>

                                   <select name="IncontinentAlways" id="IncontinentAlways value="@yield('editIncontinentAlways')">
                                    <option value="@yield('editIncontinentAlways')">@yield('editIncontinentAlways')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>

                                  </select>
                  </div>
                   <div class="col-sm-3" style="margin-top: 15px;">
                   <label >Commode chair </label>
                        <select name="Commodechair" id="Commodechair value="@yield('editCommodechair')">
                          <option value="@yield('editCommodechair')">@yield('editCommodechair')</option>
                          <option value="No">No</option>
                          <option value="Yes">Yes</option>
                        </select>

                  </div> -->
            </div>
          </div>
            <div class="col-sm-12" style="margin-top:16px;">
                            @if($mobility->notsignificantformobility == "true")
                            <button type="button" class="btnCustom Mobility btnCustom-default custom" data-toggle="collapse" data-target="#Mobility" style="    margin-left: 1px; width: 82%;"  disabled=>Mobility<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                            @else
                               <button type="button" class="btnCustom Mobility btnCustom-default custom" data-toggle="collapse" data-target="#Mobility" style="    margin-left: 1px; width: 82%;"  >Mobility<p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                            @endif
                             <div id="notsignificant">
                                <label class="switch">
                                 @if($mobility->notsignificantformobility == "true")
                                    <input type="checkbox" class="mobility mnss" name="notsignificantformobility" value="true" checked>
                                  @else
                                    <input type="checkbox" class="mobility mnss" name="notsignificantformobility" value="true">
                                  @endif
                                  <span class="slider round"></span>
                                </label>
                                <h5 class="text"> <b> Nothing Significant  </b></h5>
                            </div>
            </div>
             <div class="col-sm-12 collapse" id="Mobility">
                   <div class="col-sm-4" style="margin-top: 15px;">
                        <label >Mobility type</label>
                                     <select  name="Independent" class="mobilityy   mns" id="Independentofmobility"  value="@yield('editIndependent')">
                                    <option value="@yield('editIndependent')">@yield('editIndependent')</option>
                                        <option value="Mobile">Mobile</option>
                                        <option value="Partial Mobile">Partial Mobile</option>
                                        <option value="Immobile">Immobile</option>
                                      </select>
                      </div>


                  <div class="col-sm-4 NeedAssistanceofmobility" style="margin-top: 15px;">
                        <label >Physical Assistance</label>

                                    <select name="NeedAssistance" class="mobilityy" id="NeedAssistanceofmobility"  value="@yield('editNeedAssistance')">
                                    <option value="@yield('editNeedAssistance')">@yield('editNeedAssistance')</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                      </select>
                 </div>
                   <div class="col-sm-4 typesofmobility" style="margin-top: 10px;">
                                    <label >Assistive Devices </label>
                              <!--      <select name="Fowler" id="Fowler"  value="@yield('editFowler')">
                                    <option value="@yield('editFowler')">@yield('editFowler')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                      </select> -->
                                        <select id="Typeofmobility" class="mobilityy" name="Typeofmobility" multiple="multiple">

                                     <option value="Wheel Chair">Wheel Chair</option>
                                    <option value="Walker">Walker</option>
                                    <option value="Crutch">Crutch</option>
                                    <option value="Cane">Cane</option>
                                     <option value="Chair Fast">Chair Fast</option>

                                  <option value="Bed Fast">Bed Fast</option>

                                    </select>

                   </div>
                   <div class="col-sm-12"></div>
                    <div class="col-sm-4" style="margin-top: 18px;">
                                 <div class="form-group" style="width: 99%;margin-left: 0px;">
                                 <label >Other Observations</label>
                                 <textarea class="form-control mobilityy mns" rows="5" cols="20" name="mobilitynotes" id="mobilitynotes">@yield('editmobilitynotes')</textarea>
                                </div>
                            </div>
                <!--   <div class="col-sm-3" style="margin-top: 15px;">
                    <label >Walker </label>
                        <select name="Walker" id="Walker"  value="@yield('editWalker')">
                        <option value="@yield('editWalker')">@yield('editWalker')</option>
                            <option value="No">No</option>
                            <option value="Yes">Yes</option>
                          </select>

                  </div>
                   <div class="col-sm-3" style="margin-top: 15px;">
                        <label >WheelChair</label>
                                     <select name="WheelChair" id="WheelChair"  value="@yield('editWheelChair')">
                                    <option value="@yield('editWheelChair')">@yield('editWheelChair')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>
                      </div>
                  <div class="col-sm-3" style="margin-top: 15px;">
                                    <label >Crutch</label>
                                   <select name="Crutch" id="Crutch"value="@yield('editCrutch')">
                                    <option value="@yield('editCrutch')">@yield('editCrutch')</option>
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>
                   </div>

                  <div class="col-sm-3" style="margin-top: 15px;">
                        <label >Cane </label>

                                     <select name="Cane" id="Cane" value="@yield('editCane')">
                                      <option value="@yield('editCane')">@yield('editCane')</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                        </select>
                                              </div>
                   <div class="col-sm-3" style="margin-top: 15px;">
                   <label >Chair Fast</label>
                        <select name="ChairFast" id="ChairFast" value="@yield('editChairFast')">
                          <option value="@yield('editChairFast')">@yield('editChairFast')</option>
                              <option value="No">No</option>
                              <option value="Yes">Yes</option>
                            </select>

                  </div>
                   <div class="col-sm-3" style="margin-top: 15px;">
                   <label >Bed Fast</label>
                        <select name="BedFast" id="BedFast"  value="@yield('editBedFast')">
                          <option value="@yield('editBedFast')">@yield('editBedFast')</option>
                              <option value="No">No</option>
                              <option value="Yes">Yes</option>
                            </select>

                  </div> -->
            </div>
            <div class="col-sm-12" style="margin-top: 14px;">
                      <button type="button" class="btnCustom btnCustom-default custom doc" data-toggle="collapse" data-target="#Prescribedby" style="    margin-left: 1px; width: 100%;" >Prescription <p style="text-align: -webkit-right; margin-top: -20px; margin-right: 16px;  "><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">  </p> </button>
                 </div>
                 <div class="col-sm-12 collapse" id="Prescribedby" style="margin-top: 11px;">

             <?php
             if($count_of_docs > 0)
             {
                    $m=0;
                    $z=0;
                    $j=1;

                  for($i=0; $i < $count_of_docs ; $i++)

                  {
                        $m=$medicine_count[$i];
                    ?>
                    <input type="hidden" class="PrescribedbyDocfirstnomedicices" name="PrescribedbyDocfirstnomedicices" id="PrescribedbyDocfirstnomedicices" value="<?php echo $m;?>">
                    <input type="hidden" class="2" id="Prescribed<?php echo $j;?>" name="Prescribed<?php echo $j;?>" value="<?php echo $m;?>">
                      <div class="col-sm-12" style="border:1px solid green;margin-bottom: 10px;">
                           <h4 style="    margin-left: 13px;"> <b> Doctor <?php echo $j;?> Details </b></h4>
                           <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Doctor Name</label>
                             <input type="text"  rows="5" name="doc<?php echo $j;?>" id="doc<?php echo $j;?>" value="<?php echo $Prescribedby_value_unique[$i]?>" style="    margin-top: 8px;">
                            </div>
                          <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Registration Id</label>
                             <input type="text"  rows="5" name="reg<?php echo $j;?>" id="reg<?php echo $j;?>" value="<?php echo $RegistrationNo_value_unique[$i];?>" style="    margin-top: 8px;" >
                          </div>
                          <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Prescription Date</label>
                             <input type="date"  rows="5" name="date<?php echo $j;?>" id="" value="<?php echo $Prescriptiondate_value_unique[$i];?>" style="       margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                    </div>
                            <?php
                              for($n=1 ; $n <=$m ; $n++)
                              {?>

                                     <div class="col-md-12">
                                       <h4 style=" margin-top: 10px;   color: #636b6f;"> <b> Medicine <?php echo $n;?> Details</b> </h4>
                                    </div>
                                     <div class="col-sm-4" style="    margin-top: 12px;">
                                      <label >Medication Form</label>
                                          <!--  <input type="text"  rows="5" name="doc1med1form" id="" value="" style="    margin-top: 8px;"> -->
                                      <select name="doc<?php echo $j;?>med<?php echo $n;?>form" id="doc1med<?php echo $n;?>form" >
                                <option value="<?php echo $MedicationForm_value[$z];?>"><?php echo $MedicationForm_value[$z];?></option>
                                <option value="Tablet">Tablet</option>
                                <option value="Capsule">Capsule</option>
                                <option value="Injection">Injection</option>
                                <option value="Ointment">Ointment</option>
                                <option value="Scrub">Scrub</option>
                                 <option value="Cream">Cream</option>
                                <option value="Powder">Powder</option>
                                 <option value="Lotion">Lotion</option>
                              <!--    <option value="Syrum">Syrum</option> -->
                                 <option value="Drops">Drops</option>
                                 <option value="Gel">Gel</option>
                                 <option value="Expectorant">Expectorant</option>
                                <option value="Respules">Respules</option>
                                 <option value="Handrub">Handrub</option>
                                 <option value="Syrup">Syrup</option>


                                  </select>
                                   </div>
                                     <div class="col-sm-4">
                                      <label >Medication</label>
                                      <input type="text"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>medication" id="" value="<?php echo $Medication_value[$z];?>" style="    margin-top: 8px;margin-bottom: 10px;" >
                                    </div>
                                   <div class="col-sm-4" style="    margin-top: 8px;">
                                    <label >Route</label>
                                    <input type="text"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>route" id="" value="<?php echo $Route_value[$z];?>" style="    margin-top: -7px;margin-bottom: 10px;" >
                                  </div>
                                  <div class="col-sm-12"></div>
                                  <div class="col-sm-4">
                                    <label >Frequency</label>
                                    <input type="text"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>Frquency" id="" value="<?php echo $Frequency_value[$z];?>" style="       margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                                  </div>

                                  <div class="col-sm-4">
                                    <label >Dosage</label>
                                    <input type="text"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>dosage" id="" value="<?php echo $Dosage_value[$z];?>" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                                  </div>
                                  <div class="col-sm-4">
                                    <label >Prescription Validity(End Date)</label>
                                  <input type="date"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>validtill" id="" value="<?php echo $ValidTill_value[$z];?>" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                                  </div>
                                  <div class="col-sm-4">
                                    <label >AC/PC</label>
                                    <input type="text"  rows="5" name="doc<?php echo $j;?>med<?php echo $n;?>ACPC" id="" value="<?php echo $ACPC_value[$z];?>" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                                  </div>

                                  <div class="col-sm-12"></div>
                              <?php
                                $z=$z+1;
                              }

                            ?>
                        </div>



                  <?php
                    $j=$j+1;
                  }


              }
             else
             {?>



                  <div class="col-sm-12" style="border:1px solid green;">
                        <h4 style="    margin-left: 13px;"> <b> Doctor 1 Details </b></h4>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Doctor Name</label>
                             <input type="text"  rows="5" name="doc1" id="" value="" style="    margin-top: 8px;">
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Registration Id</label>
                             <input type="text"  rows="5" name="reg1" id="" value="" style="    margin-top: 8px;" >
                    </div>
                    <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Prescription Date</label>
                             <input type="date"  rows="5" name="date1" id="" value="" style="       margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                    </div>

                    <div class="col-md-12">
                        <h4 style=" margin-top: 10px;   color: #636b6f;"> <b> Medicine 1 Details</b> </h4>
                    </div>
                    <div class="col-sm-4" style="margin-top: 10px;">
                            <label >Medication Form</label>
                            <!--  <input type="text"  rows="5" name="doc1med1form" id="" value="" style="    margin-top: 8px;"> -->
                              <select name="doc1med1form" id="doc1med1form" >
                                <option value="@yield('editMedType')" style="    height: 29px;">@yield('editMedType')</option>
                                <option value="Tablet">Tablet</option>
                                <option value="Capsule">Capsule</option>
                                <option value="Injection">Injection</option>
                                <option value="Ointment">Ointment</option>
                                <option value="Scrub">Scrub</option>
                                 <option value="Cream">Cream</option>
                                <option value="Powder">Powder</option>
                                 <option value="Lotion">Lotion</option>
                              <!--    <option value="Syrum">Syrum</option> -->
                                 <option value="Drops">Drops</option>
                                 <option value="Gel">Gel</option>
                                 <option value="Expectorant">Expectorant</option>
                                <option value="Respules">Respules</option>
                                 <option value="Handrub">Handrub</option>
                                 <option value="Syrup">Syrup</option>


                              </select>
                    </div>
                    <div class="col-sm-4">
                            <label >Medication</label>
                             <input type="text"  rows="5" name="doc1med1medication" id="" value="" style="    margin-top: 8px;margin-bottom: 10px;" >
                    </div>

                     <div class="col-sm-4" style="    margin-top: 8px;">
                            <label >Route</label>
                             <input type="text"  rows="5" name="doc1med1route" id="" value="" style="    margin-top: -7px;margin-bottom: 10px;" >
                    </div>
                    <div class="col-sm-12"></div>
                   <div class="col-sm-4">
                            <label >Frequency</label>
                             <input type="text"  rows="5" name="doc1med1Frquency" id="" value="" style="       margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                    </div>
                     <div class="col-sm-4">
                            <label >Dosage</label>
                             <input type="text"  rows="5" name="doc1med1dosage" id="" value="" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                    </div>

                     <div class="col-sm-4">
                            <label >Prescription Validity(End Date)</label>
                             <input type="date"  rows="5" name="doc1med1validtill" id="" value="" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                    </div>
                    <div class="col-sm-4">
                         <label >AC/PC</label>
                             <input type="text"  rows="5" name="doc1med1ACPC" id="" value="" style="        margin-top: 10px;
    margin-bottom: 10px;
    height: 20px;" >
                        <!-- <select name="doc1med1ACPC" id=""  value="">
                                    <option value=""></option>
                                    <option value="AC">AC</option>
                                    <option value="PC ">PC </option>
                                      </select>  -->
                    </div>

                     <div id="1"> </div>
                     <div class="col-sm-12" style="text-align: right;">


                      <button type="button" data-toggle="tooltip" title="Add New Medicine for Doctor 1"  id="moremedicine" style="outline: none; background: white;border: 0px;">
                         <img src="/img/medicine.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
                         <input type="hidden" name="PrescribedbyDocfirstnomedicices" id="PrescribedbyDocfirstnomedicices" value="1">
                    </div>
                    <div class="col-sm-12"></div>

                    </div>
                    <div class="col-sm-12"></div>
          <?php
        }
        ?>
                    <div id="moredoc"> </div>
                    <div class="col-sm-12" style="text-align: left;">
                      <button type="button"  data-toggle="tooltip" title="Add New Doctor" id="addnewdoc" style="outline: none; background: white;border: 0px;">
                         <img src="/img/doctor.png" class="img-responsive" alt="add" style="        width: 45px;"></button>
                    </div>
                  </div>



       <!-- Product Deatials -->
<!--  <div class="container" id="main" style="    margin-top: 34px;">

      <div class="row" id="maintitle" style="    margin-left: -4px;
    width: 102%;" >

        <div class="col-sm-12" >
          <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ProductDeatials"  >Product Details<p style="text-align: -webkit-right;
           margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
            </button>

        </div>

      </div>
    </div>
    <div class="container" id="main1" style="margin-top: 11px;">

      <div class="row collapse" id="ProductDeatials" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">
 -->
      <!--  <label>SKU ID<input type="text" class="form-control" rows="5" name="SKUid" id="SKUid" value="@yield('editSKUid')"></label>&emsp; -->
<!--
        <div class="col-sm-4" style="    margin-top: 18px;">

              <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName" value="@yield('editProductName')">
          </div>
          <div class="col-sm-4" style="    margin-top: 18px;">

              <label>Demo Required</label><input type="text"  rows="5" name="DemoRequired" id="DemoRequired" value="@yield('editDemoRequired')">
          </div>
          <div class="col-sm-4" style="    margin-top: 18px;">

              <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="@yield('editAvailabilityStatus')">
          </div>
           <div class="col-sm-4" style="margin-top: 26px;">

              <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="@yield('editAvailabilityAddress')">
          </div>
           <div class="col-sm-4" style="margin-top: 26px;">

              <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="@yield('editSellingPrice')">
          </div>
           <div class="col-sm-4" style="margin-top: 26px;    margin-bottom: 13px;">

              <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="@yield('editRentalPrice')">
          </div>
      </div>
    </div> -->
<!-- Product details End -->
 <?php $leadid=$_GET['id'];
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
<input type="hidden" name="leadid" value="<?php echo $leadid;?>">
<input type="hidden" name="sname" value="<?php echo $name;?>">

  </fieldset>

</div>


</div>

<div class="container" style="    margin-top: 32px;" >
        <div class="row">
                  <div class="col-sm-12">
                      <center>
                          <a href="/history?id=<?php echo $leadid; ?>" class="btn btn-default" style="    background-color: #ff3547;">History</a>
                          &emsp;
                     <button type="submit" class="btn btn-default" id="submit">Update</button>
                   </center>
            </div>
                <input type="hidden" id="languageschecked" name="languageschecked" value="">
                <input type="hidden" id="Typeofurinarychecked" name="Typeofurinarychecked" value="">
                <input type="hidden" id="Typeoffecalchecked" name="Typeoffecalchecked" value="">
                <input type="hidden" id="Typeofmobilitychecked" name="Typeofmobilitychecked" value="">
                <input type="hidden" id="Position_Typechecked" name="Position_Typechecked" value="">
                <input type="hidden" id="Historyofcirculatory" name="Historyofcirculatory" value="">
                <input type="hidden" id="Historyofrespiratory" name="Historyofrespiratory" value="">
                <input type="hidden" id="NumberofPrescribedbydoc" name="NumberofPrescribedbydoc" value="1">
                <input type="hidden" id="NumberofNebulizationdoc" name="NumberofNebulizationdoc" value="1">
                <input type="hidden" class="MemoryIntactsNs" name="MemoryIntactsNs" id="MemoryIntactsNs" value="">
                <input type="hidden" name="CommunicationNs" id="CommunicationNs" value="0">
                <input type="hidden" name="Visionns" id="Visionns" value="0">
                <input type="hidden" name="Hearingns" id="Hearingns" value="0">
                <input type="hidden" name="Respiratoryy" id="Respiratoryy" value="0">
                <input type="hidden" name="Circulatoryy" id="Circulatoryy" value="0">
                <input type="hidden" name="Dentureee" id="Dentureee" value="0">
                <input type="hidden" name="Nutritionnn" id="Nutritionnn" value="0">
                <input type="hidden" name="abdomenn" id="abdomenn" value="0">
                <input type="hidden" name="eliminationnn" id="eliminationnn" value="0">
                <input type="hidden" name="Mobilityyy" id="Mobilityyy" value="0">
                <input type="hidden" name="Positionn" id="Positionn" value="0">
                <input type="hidden" name="NervousSystemm" id="NervousSystemm" value="0">
               
         

              </div>
        </div>
  </div>


   </form>
 </div>
 </div>
  </body>
    <style type="text/css">
      #messagetoshow
      {
        display: none;
      }
    </style>
 <div id="messagetoshow">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center> <h4 id="alignn"> Please Wait....Dont press back button. Otherwise your data will not be saved. </h4></center>
                </div>
            </div>
        </div>
  </div>
  </html>

@endsection
