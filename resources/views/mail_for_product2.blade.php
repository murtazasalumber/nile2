Dear {{$pm_name}} ,<br /><br />
We have received a new request.<br /><br />

Order Details:<br /><br />
ID: {{$orderid}}<br /><br />
Name of the Customer: {{$cust_name}}<br /><br />
Contact Number: {{$cust_mob}}<br /><br />

Order created by: {{$prod_creator}}<br /><br />

Thank You,<br /><br />
Health Heal
