Dear {{$cust_name}}, <br><br>
Your order no: {{$orderid}} has been returned back. This may be due to your cancellation or non-availability. <br><br>

For more information about our Services & Products, please call us at 8880004422 or visit our website: www.healthheal.in <br><br>

Looking forward to serving you better in future. <br><br>
Thank you,  <br><br>
Health Heal
