<!-- This page is generated when we click on the "View Leads" button after loggin in with the Vertical Head -->

<?php
$value= Session::all();

$value=session()->getId();
  //echo $value;


?>
<!DOCTYPE html>
<html lang="en">
<head>
 <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>

    <script>
$(document).ready(function(){
  $('.footable').footable();
   $('[data-toggle="tooltip"]').tooltip();
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();
      var status1 = $('#status1').val();

          $.ajax({
        type: "GET",
        url:'alllead' ,
         data: {'keyword1' : keyword,'status1':status1,'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
        }
    });
    });

 $("#filter").change(function(){

              $('#keyword').val("");
             });
});
</script>
<style type="text/css">
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}
  .imgg
{
   margin-top: -13px;

    margin-left: -80px;
}
@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
.navbar-right
{
  text-align: right;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
.footer
{
  margin-top: 2%;
}
@media screen and (max-height: 1200px) {
    .loo
    {
          margin-left: 18%;
    }



}



@media (max-width: 1200px)
{
  .identy
  {
    padding-left: 7%;
  }
 #buttonforcreate
  {
      margin-top: 10%;
  }
   #imagess
   {
    text-align: center;
   }
   #productleadparent
   {
              margin-top: 12px;
    margin-left: 36%;
   }
   #filter
   {
        width: 44%;

   }
 .keyword1
    {
    margin-right: 50px;
    }

   #seacrh
   {
    margin-bottom: 37px;
   }
   #download1
   {
    margin-top: 5%;
    margin-left: 24%;
    width: 53%;
   }
   #second
    {
              margin-bottom: 22px;
    margin-left: 36%;
    }
    #productl
    {
          margin-bottom: 16px;
    margin-left: 26%;
    }

}

</style>
  </head>
  <body style="font-family: myFirstFont;">
   <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
              <div class="loo">
            <a class="navbar-brand" rel="home" href="/admin/Vertical_PharmacyManager">
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                        <!-- this route is for showing the login form -->
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="/admin/Vertical_PharmacyManager"  style="font-family: myFirstFont;"
                                            >
                                            Home
                                        </a>
                                        <a href="\version">Version Notes</a>
                                        <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>
<!-- title -->
    <div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2> Leads</h2></center>
               </div>
            </div>

    </div>

    <!-- title Ends -->


{{csrf_field()}}
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
 <div class="container">
      <div class="row" >
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>
                              <option value="leads.id">Lead ID</option>
                         <option value="fName">Customer Name</option>
                        <option value="createdby">Created By</option>
                        <option value="Source">Lead Source</option>
                         <!-- <option value="ServiceType">Service Type</option> -->
                         <!-- <option value="Branch">City</option> -->
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="Alternatenumber">Alternate Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>
<?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status1" value="<?php echo $status ?>">

               <div class="container">
    <div class="row" id="imagess">
        <div class="col-sm-3 col-sm-offset-5" id="icons">

            <a href="/Verticalhead/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;"></a>
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" > <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"></a>



             <?php

if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?>
  <input type="hidden" name="" id="status1" value="<?php echo $check; ?>">

<!-- Download for coordinator view Leads -->
@if($check=="All")
<a href="/verticalpharmacydownloadall?status=<?php echo $check;    ?>&download=true"><!-- <input type="button" name="download" value="Download" class="btn btn-primary" id="download" style="background: #4abde8"> --> <img src="/img/download1.png" class="img-responsive" alt="add" id="download" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>
<!-- <input type="text" value="try"> -->
@else

<!-- Download for count view leads -->
    <a href="/verticalpharmacydownload?status=<?php echo $check;    ?>&download=true"><!-- <input type="button" name="download" value="Download" id="download" class="btn btn-primary"  style="background: #4abde8"> --><img src="/img/download1.png" class="img-responsive" alt="add" id="download" name="download" style="     margin-left: 18px;    height:35px;  display: -webkit-inline-box;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>

@endif


        </div>

    </div>
</div>
<!-- Search Section -->
<!--   <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
          <div style="    margin-left: -69px;">
          <div class="col-sm-4" id="buttonforcreate">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="second" >
                                          Create Service Lead
                                        </a>&emsp;
                  <div id="productleadparent" style="    display: -webkit-inline-box;">
                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="procductl">
                                          Create Product Lead
                                        </a>
                                        </div>
                                        </div>

          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="createdby">Created By</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword'>
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status1" value="<?php echo $status; ?>">
              </div>
              <div class="col-sm-2" > -->
                    <!-- Search Section Ends-->
<!-- <?php


if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?> -->

<!-- Download for coordinator view Leads -->
<!-- @if($check=="All")
<a href="/verticaldownloadall?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" value="Download" id="download" style="background: #4abde8" class="btn btn-primary" ></a> -->
<!-- <input type="text" value="try"> -->
<!-- @else -->

<!-- Download for count view leads -->
   <!--  <a href="/verticaldownload?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" value="Download" style="margin-top: 2%;" class="btn btn-primary" ></a>

@endif -->

        <!--   </div>

          </div>

      </div>

  </div>
 -->



<!-- Reslts -->
  <div class="container-fluid" style="margin-top: 50px;" >
    <div class="row" >
      <div class="col-sm-12" id="result">
            <table class="table footable" style="font-family: Raleway,sans-serif;">
          <thead>

              <tr>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b> Created At </b></th>
                    <th  data-hide="phone,tablet"><b> Created By </b></th>
                    <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                   <!--  <th data-hide="phone,tablet"><b>City </b></th> -->
                    <th data-hide="phone,tablet"><b>Source</b></th>
                    <!-- <th data-hide="phone,tablet" ><b>Service Type</b></th> -->
                    <th data-hide="phone,tablet" ><b>Assigned To</b></th>
 <?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All"  || $status==NULL)
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>


                </tr>
            </thead>
            <tbody>

            <?php
                $result=count($leads );
                  if($result <=4)
                  {
                    ?>

                      <style type="text/css">

                      .footer
                      {
                        margin-top: 20%;
                      }
                    </style>
  @foreach ($leads as $lead)
<tr>
<?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
    <td><a class="identy" href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
 <!--  <td>{{$lead->Branch}}</td> -->
  <td>{{$lead->Source}}</td>
  <!-- <td>{{$lead->ServiceType}}</td> -->
<td>{{$lead->ename}}</td>
<?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All"  || $status==NULL)
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>


<!--
<td>

<form action="{{'/cc/'.$lead->id}}" method="post">
{{csrf_field()}}
{{ method_field('DELETE') }}
<input type="submit" value="Delete">

</form>

    </td> -->
     </tr>

@endforeach
                <?php
              }
              else
              {?>



  @foreach ($leads as $lead)
<tr>
    <?php
    if(session()->has('name'))
    {
      $name=session()->get('name');
    }else
    {
    if(isset($_GET['name'])){
       $name=$_GET['name'];
    }else{
       $name=NULL;
    }
    }
    ?>
    <td><a class="identy" href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->createdby}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
 <!--  <td>{{$lead->Branch}}</td> -->
  {{-- <td>{{$lead->EmailId}}</td> --}}
  <td>{{$lead->Source}}</td>
  <!-- <td>{{$lead->ServiceType}}</td> -->
<td>{{$lead->ename}}</td>
<?php
                     if(isset($_GET['status'])){
                        $status=$_GET['status'];
                    }
                    else
                    {
                       $status=NULL;
                     }

                     if($status=="All"  || $status==NULL)
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>

<!--
<td>

<form action="{{'/cc/'.$lead->id}}" method="post">
{{csrf_field()}}
{{ method_field('DELETE') }}
<input type="submit" value="Delete">

</form>

    </td> -->
     </tr>

@endforeach
<?php }?>
 </tbody>
</table>
<div style="text-align: -webkit-center;"> {{$leads->appends(request()->except('page'))->links()}} </div>

      </div>

    </div>

</div>
 @extends('layouts.footer')
  </body>
  </html>
