Dear {{$name}}, <br><br>
Thank you for contacting Health Heal. <br>
Our Care Team will get in touch with you, shortly.<br>
Your reference ID is {{$leadid}}. Please quote this ID for further communications.<br><br>

For more information about our Services & Products, please visit our website: www.healthheal.in<br><br>

Thank you,<br>
Health Heal