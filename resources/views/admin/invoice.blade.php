@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Inovice </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
select
{
  font-family: myFirstFont;
}
select:focus
{
  border-bottom:1px solid #00cccc;
}
select
{
  border-bottom-color:#484e51;
      border-top: 0px;
       width: 31%;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
body
{
  font-family: myFirstFont;
}
.clinetdetails
{
  background: white;
      border-color: #ddd;
}
h3
{
  margin-top: auto;
}
.panel
{
  border-radius: 0px;
    margin-left: -15px;
    width: 105.64%;
    margin-top: 10px;
}
#adress
{
  width: 78%;
  
  height: auto;
      display: inline-block;
          text-align: -webkit-left;
  padding-left: 13px;
}
.productdetails
{
     width: 100%;
  
    max-height: 550px;
    overflow-y: scroll;
}
.productdetails1
{
     width: 100%;
  
    max-height: 200px;
    overflow-y: scroll;
}
hr
{
  border-top: 1px solid black;
}

.companydetails
{
  text-align: justify;
    padding-top: 23px;
    width: 53%;
      margin-left: 13px;
}
#logoimage
{
  margin-top: 3px;
}
.footer
{
  padding-top: 10px;
  text-align: center;
}
</style>
</head>
<body>
 <!-- 2260, 4th 'A' cross, 1st Main Road Vijayanagar, Club Ave, Vijay nagar, Bengaluru, Karnataka 560040 -->
<div class="container" style="        margin-top: 68px;">
    <div class="row">
          <div class="col-sm-6" style="background: #eee;">
               <div class="panel panel-default" style="margin-top: 10px;">
    
                  <div class="panel-body">
                    <h3>Client Details</h3>
                    <p style="    padding-top: 10px;"> <b> Lead Id &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</b> 5606</p>
                    <p > <b>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : </b> &nbsp; Sunil Kumar B V</p>

                    <p> <b>Email Id &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> &nbsp; sunilkuamrbv7@gmail.com</p>
                    <p> <b>Mobile No.&nbsp;&nbsp;: </b> &nbsp; +918892800730</p>
                    
                    <p style="display: inline-block;"> <b>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<br><br> </b> &nbsp;
                      </p>
                      <div id="adress" style="display: inline-block;"> 
                        2260, 4th 'A' cross, 1st Main Road Vijayanagar, Club Ave, Vijay nagar, Bengaluru, Karnataka 560040
                    
                      </div>

                  </div>
              </div>
              <p> <b> BILL NUMBER &nbsp;: </b>&nbsp;&nbsp;234589</p>
              <p style="padding-top: 25px;"> <b> DATE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: </b>&nbsp;&nbsp;24-06-2017</p>
              <p style="    margin-top: -33px;
    float: right;"> <b> DUE DATE &nbsp; &nbsp; &nbsp; &nbsp;: </b>&nbsp;&nbsp;24-06-2017</p>
               <div class="productdetails1">
                       <table class="table table-hover" style="    margin-top: 37px;">
    <thead>
      <tr>
        <th>Id</th>
        <th>Description</th>
        <th>Quantity</th>
        <th>Price</th>
        
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1234</td>
        <td>some description</td>
        <td>500gm</td>
        <td>&#x20b9; 500</td>
        
        <td>&#x20b9; 2000 </td>
      </tr>
       <tr>
        <td>1234</td>
        <td>some description</td>
        <td>500gm</td>
        <td>&#x20b9; 500</td>
        
        <td>&#x20b9; 2000 </td>
      </tr>
       <tr>
        <td>1234</td>
        <td>some description</td>
        <td>500gm</td>
        <td>&#x20b9; 500</td>
        
        <td>&#x20b9; 2000 </td>
      </tr>
       <tr>
        <td>1234</td>
        <td>some description</td>
        <td>500gm</td>
        <td>&#x20b9; 500</td>
        
        <td>&#x20b9; 2000 </td>
      </tr>
       <tr>
        <td>1234</td>
        <td>some description</td>
        <td>500gm</td>
        <td>&#x20b9; 500</td>
        
        <td>&#x20b9; 2000 </td>
      </tr>

        <tr style="padding-top: 10px;">
       <td></td>
       <td></td>
       <td></td>
        <td style="background: white;"><b> TOTAL </b></td>
        
        <td style="background: white"> &#x20b9; 2000 </td>
      </tr>
      
    </tbody>
  </table>
               </div>
          
          </div>


          <div class="col-sm-6">
              
           


            


              <h3 style="    padding-top: 22px;">Product Details</h3>
                   <div class="productdetails">
                  

              <h4> Product 1</h4>
              <p> <b>SKUID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</b>12345789</p>
              <p> <b>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b>Product 1 </p>
              <p> <b>Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b> Some Category</p>
              <p> <b>Quanity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b> 250gm</p>
              <p> <b>Availability Status :&nbsp;&nbsp;</b> Available</p>
              <p> <b>Mode of Payment&nbsp;:&nbsp;&nbsp;</b> <select  id="Status" >

        <option value="">Value 1</option>
        <option value="">Value 2</option>
        <option value=" ">Value 3</option>
      </select></p>
              <p style="padding-top: 10px;"> <b>Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</b>  <select  id="Status" >

        <option value="">Value 1</option>
        <option value="">Value 2</option>
        <option value=" ">Value 3</option>
      </select></p>
              <hr>
               <h4> Product 2</h4>
              <p> <b>SKUID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</b>12345789</p>
              <p> <b>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b>Product 1 </p>
              <p> <b>Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b> Some Category</p>
              <p> <b>Quanity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </b> 250gm</p>
              <p> <b>Availability Status :&nbsp;&nbsp;</b> Available</p>
              <p> <b>Mode of Payment&nbsp;:&nbsp;&nbsp;</b> <select  id="Status" >

        <option value="">Value 1</option>
        <option value="">Value 2</option>
        <option value=" ">Value 3</option>
      </select></p>
              <p style="padding-top: 10px;"> <b>Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</b>  <select  id="Status" >

        <option value="">Value 1</option>
        <option value="">Value 2</option>
        <option value=" ">Value 3</option>
      </select></p>
            </div>

              
          </div>
    </div>
</div>

</body>
</html>
@endsection