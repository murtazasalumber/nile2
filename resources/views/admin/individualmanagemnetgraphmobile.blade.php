<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" } ],
        ["New Lead", {{$newcountforall}}, "#b87333"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deferred", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

      ]);







      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "",
        width: 250,
        height: 300,
        bar: {groupWidth: "30%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
      chart.draw(view, options);
  }
  </script>

<body>
	
		 <div class="col-sm-12 " >
                    <div class="panel panel-default" style="width: 112%;
    margin-left: -16px;">

                    <div class="panel-body" style="height: auto;text-align: center;">
                      
                             <div id="columnchart_values1" ></div>

                    </div>
                </div>
            </div>

</body>