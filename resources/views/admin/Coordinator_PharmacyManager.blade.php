<!-- This is dashboard for Coordinator -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    if (screen.width <= 800) {
        $('.footer1').show();
        $('.footer').hide();
        $('#mobile').show();

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
        // mobile
    }else
    {


        $('#mySidenav').show();
        $('#main').show();
    }
     $("#filter").click(function(){

                $('#myModal').modal('show');



        });

    $("#search").click(function(){



        var branch =  $('#Branch').val();
        var vertical=   $('#Vertical').val();
        var from=  $('#FromDate').val();
        var to= $('#ToDate').val();

        if(from == 0 && to == 0)
        // alert("branch and vertical");
        {
            alert("Nothing Seleted");
        }
        else
        {

            $.get("{{ URL::to('searchfiltercoordinator') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to } ,function(data){
                $('#main').html(data);
            });

        }

    });
});


</script>
<style type="text/css">
@media screen and (max-width: 380px) {
    #mobile
    {
        margin-top: 20%;
    }
    .navbar-header {
        padding-left: 82px;
    }
}
@media screen and (max-width: 412px) {


}
@media screen and (max-width: 1200px) {


    #mobile
    {
        margin-top: 19%;
    }
    #loo {
        text-align: -webkit-center;
    }
}

#mySidenav,#main
{
    display: none;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -62px;
}
#mobile
{
    display: none;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.btn
{
    color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
    outline: 0;
    background-color: #00C851;
    opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
    background: white;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: -28px !important;
}
.cssmenu,
.cssmenu ul,
.cssmenu ul li,
.cssmenu ul li a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.cssmenu:after,.#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
.cssmenu {
  width: auto;

  font-family: Raleway, sans-serif;
  line-height: 1;
}
.cssmenu ul {
  background: #ffffff;
}
.cssmenu > ul > li {
  float: left;
}
.cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
.cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
.cssmenu.align-right > ul > li {
  float: right;
}
.cssmenu.align-right > ul > li > a {
  margin-right: 0;
  margin-left: -4px;
}
.cssmenu > ul > li > a {
        margin-top: 23px;
  z-index: 2;
  padding: 18px 25px 12px 25px;
  font-size: 15px;
  font-weight: 400;
  text-decoration: none;
  color: #444444;
  -webkit-transition: all .2s ease;
  -moz-transition: all .2s ease;
  -ms-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  margin-right: -4px;
}
.cssmenu > ul > li.active > a,
.cssmenu > ul > li:hover > a,
#cssmenu > ul > li > a:hover {
. color: #ffffff;
}
.cssmenu > ul > li > a:after {
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: -1;
  width: 100%;
  height: 120%;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  content: "";
  -webkit-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  -webkit-transform: perspective(5px) rotateX(2deg);
  -webkit-transform-origin: bottom;
  -moz-transform: perspective(5px) rotateX(2deg);
  -moz-transform-origin: bottom;
  transform: perspective(5px) rotateX(2deg);
  transform-origin: bottom;
}
.cssmenu > ul > li.active > a:after,
.cssmenu > ul > li:hover > a:after,
.cssmenu > ul > li > a:hover:after {
  background: #47c9af;
}

.footer {
    margin-top: -23px;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    background-color: #efefef;
    text-align: center;
}
.panel-body
{
    text-align: center;
    height: 147px;
}



.sidenav {

    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}
.footer1
{
    display: none;
}
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover, .offcanvas a:focus{

}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

#main {
    transition: margin-left .5s;
    padding: 16px;
}

#tabmenu
{
      width: 104%;
    margin-left: -17px;
    height: 69px;
    margin-top: 17px;
    border-bottom: 3px solid #47c9af;
}


.custom
{
    float: left;
    height: 100%;
    background: #30a5ff;
    width: 50%;
    padding-top: 10px;
    text-align: -webkit-center;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
@media screen and (max-width: 380px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 20%;
    }
    #branch1
    {
        margin-left: -17px;
    }
    #donutchartmobile
    {
        margin-left: -54px;
    }
}
@media screen and (min-width: 412px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 15%;
    }
    #greetings
    {
        padding-top: 1px;
    }

}
@media screen and (min-width: 1200px) {
    #logoimage {
        margin-left: -69px;
    }
}
#mySidenav,#main
{
    display: none;
}


</style>
<style>


.sidenav2 {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align:center;
}

.sidenav2 a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;

}

.sidenav2 a:hover{
    color: #f1f1f1;
}

.sidenav2 .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
a
{
    text-decoration: none !important;
}

@media screen and (max-height: 450px) {
    .sidenav2 {padding-top: 15px;}
    .sidenav2 a {font-size: 18px;}
}

</style>

@extends('layouts.app')

@section('content')

<div id="mySidenav" class="sidenav" style="height: 653px;    -webkit-transition:none !important;
-moz-transition:none !important;
-o-transition:none !important;
transition:none !important;          background-color: white">
<!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
<ul class="nav" id="side-menu" style="    margin-top: -2px;">


    <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
        <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

        <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
            width: 15px;
            float: left;
            margin-left: -2px;display: none;" id="close"> </a>

        </li>

        <li style="    height: 55px;    ">
            <a href="/cc/create?name={{ Auth::user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="     padding-top: 4px; margin-top: -7px;
                width: 22px;       margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>



            </li>

            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="  margin-top: -7px; padding-top: 4px; width: 22px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>
            </li>

             <li style="    height: 55px;    ">
                        <a href="/vh?name={{ Auth::guard('admin')->user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



                        </li>

           <li style="    height: 55px;    ">
                        <a href="/product?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Order</p></a>



                        </li>


         
            </ul>
        </div>





        <div id="main" style="margin-top: 6%;background: #eee; min-height: 550px;">


            <div class="container-fluid" style="    margin-top: 0px;">
                <div class="row">

                               <div class="col-sm-10 col-sm-offset-1" style="margin-top:6%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;height: 180px;">

                    <div class="panel-body">
                        <h3 style="color:  #33cc33;    margin-top: 20px;"> Welcome !!! </h3>
                        <p> You are logged in as Coordinator and Pharmacy Manager. </p>

                        <div class="tab cssmenu" id="tabmenu">


                                                     <ul > 
  <!-- changes here to make simply -->
  <li class="active"><a data-toggle="tab" href="#home" >Service Details</a></li>
    <li><a data-toggle="tab" href="#menu1" >Pharmacy Details</a></li>
    <!-- <li><a data-toggle="tab" href="#menu2" >Rental Product Details</a></li> -->
    <!-- <li><a data-toggle="tab" href="#menu3" >Pharmacy Details</a></li> -->
  
                                         
  </ul>

  

                                        </div>
                     
                    </div>
                </div>
            </div>

                <div class="col-sm-12">
                </div>

<div class="col-md-12 tab-content" style="margin-top: 0px">
                                            <div id="home" class="tab-pane fade in active">

                <div class="col-sm-2 col-sm-offset-2" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=In Progress">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  New Leads </h5>
                                <h3 style="color:#337ab7 ;margin-top: -8px;"> {{$assignedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f"> Converted Leads  </h5>
                                <h3 style=" margin-top: -8px;color: #33cc33">  {{$convertedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                <h3 style=" color:#990000;margin-top: -8px;">{{$deferredcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color: #ff1a1a;margin-top: -8px;"> {{$droppedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>










            </div>
            <!-- Welcome Message starts -->
  


                                            <!-- Product details counts -->


                    <div id="menu1" class="tab-pane fade">
                            <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=New&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/New Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  New  </h5>
                        <h3 style="color:#337ab7; margin-top: -8px;">{{$productnewcount}} </h3>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Processing&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/processing.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Processing   </h5>
                        <h3 style="color:orange; margin-top: -8px;"> {{$productprocessingcount}} </h3>
                    </div>
                </a>
            </div>
        </div>


        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Awaiting%20Pickup&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/awaiting pickup.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Awaiting Pickup </h5>
                        <h3 style="color:purple; margin-top: -8px;">{{$productawaitingpickupcount}} </h3>
                    </div>
                </a>
            </div>
        </div>



        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Ready%20To%20Ship&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/Ready To ship.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Ready To Ship </h5>
                        <h3 style="color:yellow; margin-top: -8px;">{{$productreadytoshipcount}} </h3>
                    </div>
                </a>
            </div>
        </div>



          <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Out%20for%20Delivery&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/out for delivery.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Out for Delivery </h5>
                        <h3 style="color:pink; margin-top: -8px;">{{$productoutfordeliverycount}} </h3>
                    </div>
                </a>
            </div>
        </div>



         <div class="col-sm-2 col-sm-offset-3" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Delivered&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/delivered.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Delivered </h5>
                        <h3 style="color:#33cc33; margin-top: -8px;">{{$productdeliveredcount}} </h3>
                    </div>
                </a>
            </div>
        </div>

         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Cancelled&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/cancelled.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Cancelled </h5>
                        <h3 style="color:#ff1a1a; margin-top: -8px;">{{$productcanceledcount}} </h3>
                    </div>
                </a>
            </div>
        </div>
         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Order%20Return&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$productorderreturncount}} </h3>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/cphmproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Received%20Order%20Return&type=Sell">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Received Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$productrecievedorderreturncount}} </h3>
                    </div>
                </a>
            </div>
        </div>

     </div>
     <!-- product sell end -->



   </div>


<div class="col-sm-12">
                <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

                    Copyright © Health Heal - 2017

                    <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
                </div>


            </div>
    <script>

    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $("#close").show();

    function openNav() {
        document.getElementById("mySidenav").style.width = "230px";
        document.getElementById("main").style.marginLeft = "200px";
        $(".pa").show();

        $("#open").hide();
        $("#close").show();


    }

    function closeNav() {
        $("#open").show();
        $("#close").hide();
        document.getElementById("mySidenav").style.width = "50";
        document.getElementById("main").style.marginLeft= "50";
        $(".pa").hide();
    }
    </script>
    <!-- Footer  -->


</div>
</div>










</div>


<style type="text/css">
.popover.bottom>.arrow
{
    display: none;
}
.popover {
    top: 60px !important;
    left: 4% !important;
}
.navbar-toggle
{
    display: none;
}
.popover.bottom {
    margin-top: 35px;
}
</style>


<div id="mobile" >
    <!--  <span style="    margin-left: 10px;
    margin-top: -16%;
    font-size: 30px;
    cursor: pointer;
    position: fixed;
    z-index: 10000;" onclick="openNav2()" >&#9776; </span> -->
    <a href="#" data-toggle="popover" title="" data-placement="bottom" data-trigger="focus" data-content="
    <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
    border-bottom: 1px solid #e9ebee;'>
    <a href='/cc/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/vh?name={{ Auth::user()->name }}' title='' style='color:black'>View Leads</a>
</div>

"><span style="     margin-top: -50px;
margin-left: 13px;
font-size: 30px;
cursor: pointer;
position: fixed;
z-index: 10000;"  id="sidemenu"><img class="img-responsive" src="/img/dashboard.png" alt="User" style="">  </span></a>

<div class="col-sm-12" style="    text-align: -webkit-right;">
    <button type="button" style="    margin-top: -50px;
    z-index: 100000000;
    outline: none;
    background: white;
    border: none;
    position: absolute;
    margin-left: -35px;" data-toggle="collapse" data-target="#demo"><img class="img-responsive" src="/img/user _dashboard.png" alt="User" style=";" style=""></button>


    <div id="demo" class="collapse" >
        <div style="border-bottom: 1px solid #e9ebee;
        padding-top: 8px;
        margin-left: -29px;
        background: white;
        padding-right: 43px;
        width: 117%;
        text-align: right;">
        {{ Auth::guard('admin')->user()->name }}
    </div>

    <div style="border-bottom: 1px solid #e9ebee;
    padding-top: 8px;
    margin-left: -29px;
    background: white;
    margin-bottom: 13px;
    width: 117%;text-align: right;">
    <a href="\change-password" style="    padding-right: 18px;">User Settings</a>
    <a href="{{ route('logout') }}" style="    padding-right: 18px;"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"><br>
    Logout
</a>
</div>

</div>
</div>

<div class="col-sm-2 " style="margin-top: 10px;    ">
    <div class="panel panel-default">
        <a href="#count">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <h3 style="color: #33cc33;">
                    Welcome !!!</h3><p>You are logged in as Coordinator and Product Manager.</p>

                          <div class="tab cssmenu" id="tabmenu">


                                                     <ul > 
  <!-- changes here to make simply -->
  <li class="active"><a data-toggle="tab" href="#home" >Service Details</a></li>
    <li><a data-toggle="tab" href="#menu1" >Selling Product Details</a></li>
    <li><a data-toggle="tab" href="#menu2" >Rental Product Details</a></li>
    <!-- <li><a data-toggle="tab" href="#menu3" >Pharmacy Details</a></li> -->
  
                                         
  </ul>

  

                                        </div>
                     

                </div>
            </a>
        </div>
    </div>

    <div id="mySidenav2" class="sidenav2" style="margin-top: 20%;background: white;">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">&times;</a>
        <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Service Lead</a>
        <a href="/product/create?name={{ Auth::user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Product Lead</a>

        <a href="/vh?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">View Leads</a>

    </div>

    <!--
    <script>
    function openNav2() {
    document.getElementById("mySidenav2").style.width = "100%";
}

function closeNav2() {
document.getElementById("mySidenav2").style.width = "0";
}
</script> -->
<div class="col-sm-2 col-sm-offset-2" style="margin-top: 10px;">
    <div class="panel panel-default">
        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=In Progress">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  New Leads </h5>
                <h3 style="color:#337ab7 ;margin-top: -8px;"> {{$assignedcount}} </h3>
            </div>
        </a>
    </div>
</div>

<div class="col-sm-2 " style="margin-top: 10px;">
    <div class="panel panel-default">
        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f"> Converted Leads  </h5>
                <h3 style=" margin-top: -8px;color: #33cc33">  {{$convertedcount}} </h3>
            </div>
        </a>
    </div>
</div>


<div class="col-sm-2 " style="margin-top: 10px;">
    <div class="panel panel-default">
        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                <h3 style=" color:#990000;margin-top: -8px;">{{$deferredcount}} </h3>
            </div>
        </a>
    </div>
</div>



<div class="col-sm-2 " style="margin-top: 10px;">
    <div class="panel panel-default">
        <a href="/coordinatorpharmacymanagerassignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                <h3 style="color: #ff1a1a;margin-top: -8px;"> {{$droppedcount}} </h3>
            </div>
        </a>
    </div>
</div>







</div>



<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({html:true});


});
</script>
