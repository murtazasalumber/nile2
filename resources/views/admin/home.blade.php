<!-- This is the dashboard of the admin -->
 <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

<!-- including the bootstrap datepicker file here -->
<script src="/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$.noConflict();
jQuery( document ).ready(function( $ ) {
    // Code that uses jQuery's $ can follow here.

    // This function is helping display the "dates" in the graph filters
    $('#FromDate').datepicker({
        format: "yyyy-mm-dd",

    });
    $('#ToDate').datepicker({
        format: "yyyy-mm-dd"
    });

    // On change the dates will be hidden
   

    $('#ToDate').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
      $('#FromDate').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    

 
 
});
</script>
<script>
$(document).ready(function(){
    // $('#myModal').modal('show');

    // $.get("{{ URL::to('search') }}",  function(data){
    //           $('#main').html(data);
    //     });

     $('.counter-count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

$('[data-toggle="tooltip"]').tooltip();
    $("#filter").click(function(){

        $('#myModal').modal('show');



    });

  



    $("#search").click(function(){ 

        var branch =  $('#Branch').val();
        var vertical=   $('#Vertical').val();
        var from=  $('#FromDate').val();
        var to= $('#ToDate').val();

        $('#Branch').prop('selectedIndex',0);
        $('#Vertical').prop('selectedIndex',0);

        $("#FromDate").val("");
        $("#ToDate").val("");


        if(branch == "All" && vertical == "All" && from == 0 && to == 0)

        {


            $('#PleaseCauation').modal('show');

        }else if(from != 0 && to == 0 || from == 0 && to != 0 )
        {


            $('#PleaseDatebothCauation').modal('show');
        }
        else
        {
            if(from!=0 && to !=0)
            {
                if( (new Date(from) < new Date(to)))
                {


                    $.get("{{ URL::to('searchfilteradmin') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
                        $('#main').html(data);
                    });

                }else
                {


                    $('#PleaseDateboth2Cauation').modal('show');
                }

            }else
            {



                $.get("{{ URL::to('searchfilteradmin') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
                    $('#main').html(data);
                });

            }
        }


    });



});
</script>
<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ["Element", "Leads amount", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deferred", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
    var view = new google.visualization.DataView(data);


    var options = {
        title: "",
        width: 800,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "25%"},
        legend: { position: "none" },
    };




    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}
</script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  {{$pbsnewcount}},   {{$ppsnewcount}},{{$phsnewcount}},{{$pcsnewcount}},{{$phdsnewcount}}],
        ['Processing', {{$pbsprocessingcount}},   {{$ppsprocessingcount }},{{$phsprocessingcount}},{{$pcsprocessingcount}},{{$phdsprocessingcount}}],
        ['Await', {{$pbsawaitingpickupcount }},  {{ $ppsawaitingpickuppount }},{{$phsawaitingpickuppount}},{{$pcsawaitingpickupcount}},{{$phdsawaitingpickuppount}}],
        ['Ready to ship', {{$pbsreadytoshipcount }},  {{ $ppsreadytoshippount }},{{$phsreadytoshippount}},{{$pcsreadytoshipcount}},{{$phdsreadytoshippount}}],
        ['Out', {{$pbsoutfordeliverycount}},   {{    $ppsoutfordeliverycount}},{{$phsoutfordeliverycount}},{{$pcsoutfordeliverycount}},{{$phdsoutfordeliverycount}}],
        ['Delivered', {{ $pbsdeliveredcount  }},   {{$ppsdeliveredcount}},{{$phsdeliveredcount}},{{$pcsdeliveredcount}},{{$phdsreadytoshippount}}],
        ['Canceled', {{$pbscanceledcount}},   {{$ppscanceledcount}},{{$phscanceledcount}},{{$pcscanceledcount}},{{$phdscanceledcount}}],
        ['Order Return', {{$pbscanceledcount}},  {{$ppsorderreturncount}},{{$phsorderreturncount}},{{$pcsorderreturncount}},{{$phdsorderreturncount}}],
        ['Rec Return', {{$pbsrecievedorderreturncount}},  {{$ppsrecievedorderreturncount}},{{$phsrecievedorderreturncount}},{{$pcsrecievedorderreturncount}},{{$phdsrecievedorderreturncount}}]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },

          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('seeling_product_line_chart'));

    chart.draw(data, options);
}
</script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  {{$pbrnewcount}},   {{$pprnewcount}},{{$phrnewcount}},{{$pcrnewcount}},{{$phdrnewcount}}],
        ['Processing', {{$pbrprocessingcount}},   {{$pprprocessingcount }},{{$phrprocessingcount}},{{$pcrprocessingcount}},{{$phdrprocessingcount}}],
        ['Await', {{$pbrawaitingpickupcount }},  {{ $pprawaitingpickuppount }},{{$phrawaitingpickuppount}},{{$pcrawaitingpickupcount}},{{$phdrawaitingpickuppount}}],
        ['Ready To ship', {{$pbrreadytoshipcount }},  {{ $pprreadytoshippount }},{{$phrreadytoshippount}},{{$pcrreadytoshipcount}},{{$phdrreadytoshippount}}],
        ['Out', {{$pbroutfordeliverycount}},   {{    $pproutfordeliverycount}},{{$phroutfordeliverycount}},{{$pcroutfordeliverycount}},{{$phdroutfordeliverycount}}],
        ['Delivered',  {{$pbroutfordeliverycount}},   {{    $pproutfordeliverycount}},{{$phroutfordeliverycount}},{{$pcroutfordeliverycount}},{{$phdroutfordeliverycount}}],
        ['Canceled', {{$pbrcanceledcount}},   {{$pprcanceledcount}},{{$phrcanceledcount}},{{$pcrcanceledcount}},{{$phdrcanceledcount}}],
        ['Order Return', {{$pbrcanceledcount}},  {{$pprorderreturncount}},{{$phrorderreturncount}},{{$pcrorderreturncount}},{{$phdrorderreturncount}}],
        ['Rec Return', {{$pbrrecievedorderreturncount}},  {{$pprrecievedorderreturncount}},{{$phrrecievedorderreturncount}},{{$pcrrecievedorderreturncount}},{{$phdrrecievedorderreturncount}}]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },

          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('Rental_line_chart'));

    chart.draw(data, options);
}
</script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  {{$pharmacybnewcount}},   {{$pharmacypnewcount}},{{$pharmacyhnewcount}},{{$pharmacycnewcount}},{{$pharmacyhdnewcount}}],
        ['Processing', {{$pharmacybprocessingcount}},   {{$pharmacypprocessingcount}},{{$pharmacyhprocessingcount}},{{$pharmacycprocessingcount}},{{$pharmacyhdprocessingcount}}],
        ['Await', {{$pharmacybawaitingpickupcount}},   {{$pharmacypawaitingpickupcount}},{{$pharmacyhawaitingpickupcount}},{{$pharmacycawaitingpickupcount}},{{$pharmacyhdawaitingpickupcount}}],
        ['Ready To ship', {{$pharmacybreadytoshipcount }},  {{ $pharmacypreadytoshipcount }},{{$pharmacyhreadytoshipcount}},{{$pharmacycreadytoshipcount}},{{$pharmacyhdreadytoshipcount}}],
        ['Out', {{$pharmacyboutfordeliverycount}},   {{$pharmacypoutfordeliverycount}},{{$pharmacyhoutfordeliverycount}},{{$pharmacycoutfordeliverycount}},{{$pharmacyhdoutfordeliverycount}}],
        ['Delivered',  {{$pharmacybdeliveredcount}},   {{$pharmacypdeliveredcount}},{{$pharmacyhdeliveredcount}},{{$pharmacycdeliveredcount}},{{$pharmacyhddeliveredcount}}],
        ['Canceled', {{$pharmacybcanceledcount}},   {{$pharmacypcanceledcount}},{{$pharmacyhcanceledcount}},{{$pharmacyccanceledcount}},{{$pharmacyhdcanceledcount}}],
        ['Order Return', {{$pharmacyborderreturncount}},   {{$pharmacyporderreturncount}},{{$pharmacyhorderreturncount}},{{$pharmacycorderreturncount}},{{$pharmacyhdorderreturncount}}],
        ['Rec Return', {{$pharmacybrecievedorderreturncount}},   {{$pharmacyprecievedorderreturncount}},{{$pharmacyhrecievedorderreturncount}},{{$pharmacycrecievedorderreturncount}},{{$pharmacyhdrecievedorderreturncount}}]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },

          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('Pharmacy_chart'));

    chart.draw(data, options);
}
</script>
<!-- <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New", {{$newcountforall}}, "#337ab7"],
        ["Processing ", {{ $inprogresscountforall}}, "orange"],
        ["Awaiting Pickup", {{ $convertedcountforall}}, "purple"],
        ["Ready To Ship", {{ $deferredcountforall }}, "yellow"],
        ["Out for Delivery", {{$droppedcountforall}}, "pink"],
        ["Delivered", {{$droppedcountforall}}, "#33cc33"],
        ["Cancelled", {{$droppedcountforall}}, "#ff1a1a"],
        ["Order Return", {{$droppedcountforall}}, "#990000"],
    ]);
    var view = new google.visualization.DataView(data);


    var options = {
        title: "",
        width: 800,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "25%"},
        legend: { position: "none" },
    };




    var chart = new google.visualization.ColumnChart(document.getElementById("Product1"));
    chart.draw(view, options);
}
</script> -->

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
       var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New", {{$newcount}}, "#337ab7"],
        ["Processing", {{ $processingcount}}, "orange"],
        ["Pickuping", {{ $awaitingpickupcount}}, "purple"],
        ["Shiping", {{ $readytoshipcount }}, "yellow"],
        ["Deliverying", {{$outfordeliverycount}}, "pink"],
        ["Delivered", {{$deliveredcount}}, "#33cc33"],
        ["Cancelled", {{$canceledcount}}, "#ff1a1a"],
        ["Returned", {{$orderreturncount}}, "#990000"],
    ]);

      var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 810,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "35%"},
        legend: { position: "none" },
    };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
      chart.draw(view, options);
  }
  </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
       var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
       ["New", {{$newcountrent}}, "#337ab7"],
        ["Processing", {{ $processingcountrent}}, "orange"],
        ["Pickuping", {{ $awaitingpickupcountrent}}, "purple"],
        ["Shiping", {{ $readytoshipcountrent }}, "yellow"],
        ["Deliverying", {{$outfordeliverycountrent}}, "pink"],
        ["Delivered", {{$deliveredcountrent}}, "#33cc33"],
        ["Cancelled", {{$canceledcountrent}}, "#ff1a1a"],
        ["Returned", {{$orderreturncountrent}}, "#990000"],
    ]);

      var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 810,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "35%"},
        legend: { position: "none" },
    };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values2"));
      chart.draw(view, options);
  }
  </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
       var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New", {{$pnewcount}}, "#337ab7"],
        ["Processing", {{ $pprocessingcount}}, "orange"],
        ["Pickuping", {{ $pawaitingpickupcount}}, "purple"],
        ["Shiping", {{ $preadytoshipcount }}, "yellow"],
        ["Deliverying", {{$poutfordeliverycount}}, "pink"],
        ["Delivered", {{$pdeliveredcount}}, "#33cc33"],
        ["Cancelled", {{$pcanceledcount}}, "#ff1a1a"],
        ["Returned", {{$porderreturncount}}, "#990000"],
    ]);

      var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 810,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "35%"},
        legend: { position: "none" },
    };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values3"));
      chart.draw(view, options);
  }
  </script>
<!-- Pie chart starts
-->
<?php

$j= 0;

// Depending on the number of verticals sent we are displaying the graphs here
for($i=0; $i< $totalcount; $i++)
{
    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',   {{$statuscounts[$j][0]}}  ],
            ['In Progress Leads', {{$statuscounts[$j][1]}}],
            ['Converted Leads', {{$statuscounts[$j][2]}}],
            ['Dropped Leads', {{$statuscounts[$j][3]}} ],
            ['Deferred Leads', {{$statuscounts[$j][4]}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 300,
            height: 300,
            colors: ['#337ab7','orange','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j; ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>
<!-- Pie chart ends Here -->


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([


            ['Task', 'Rating'],
            ['New', {{$newcount}} ],
            ['Processing', {{ $processingcount}}],
            ['pickup', {{ $awaitingpickupcount}} ],
            ['shipping',{{ $readytoshipcount }}],
            ['Deliverying', {{$outfordeliverycount}} ],
            ['Delivered', {{$deliveredcount}} ],
            ['Cancelled', {{$canceledcount}} ],
            ['Returned', {{$orderreturncount}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 300,
            height: 300,
            colors: ['#337ab7','orange','purple','yellow','pink','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('product2'));
        chart.draw(data, options);

    }

    </script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New', {{$pnewcount}} ],
            ['Processing', {{ $pprocessingcount}}],
            ['pickup', {{ $pawaitingpickupcount}} ],
            ['shipping',{{ $preadytoshipcount }}],
            ['Deliverying', {{$poutfordeliverycount}} ],
            ['Delivered', {{$pdeliveredcount}} ],
            ['Cancelled', {{$pcanceledcount}} ],
            ['Returned', {{$porderreturncount}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 300,
            height: 300,
            colors: ['#337ab7','orange','purple','yellow','pink','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('product4'));
        chart.draw(data, options);

    }

    </script>



    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
             ['New', {{$newcountrent}} ],
            ['Processing', {{ $processingcountrent}}],
            ['pickup', {{ $awaitingpickupcountrent}} ],
            ['shipping',{{ $readytoshipcountrent }}],
            ['Deliverying', {{$outfordeliverycountrent}} ],
            ['Delivered', {{$deliveredcountrent}} ],
            ['Cancelled', {{$canceledcountrent}} ],
            ['Returned', {{$orderreturncountrent}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 300,
            height: 300,
            colors: ['#337ab7','orange','purple','yellow','pink','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('product3'));
        chart.draw(data, options);

    }

    </script>
<style type="text/css">

body::-webkit-scrollbar
{
    display: none;
}
#myModal
{
    display: none;
}
.btn
{
    color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.nav-tabs {

    border-bottom: 1px solid #ddd;
}
.nav-tabs>li {
    float: left;
    margin-bottom: -15px!important;
}
.btn:hover
{
    outline: 0;
    background-color: #00C851;
    opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: -28px !important;
}
.cssmenu,
.cssmenu ul,
.cssmenu ul li,
.cssmenu ul li a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.cssmenu:after,.#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
.cssmenu {
  width: auto;

  font-family: Raleway, sans-serif;
  line-height: 1;
}
.cssmenu ul {
  background: #ffffff;
}
.cssmenu > ul > li {
  float: left;
}
.cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
.cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
.cssmenu.align-right > ul > li {
  float: right;
}
.cssmenu.align-right > ul > li > a {
  margin-right: 0;
  margin-left: -4px;
}
.cssmenu > ul > li > a {
        margin-top: 23px;
  z-index: 2;
  padding: 18px 25px 12px 25px;
  font-size: 15px;
  font-weight: 400;
  text-decoration: none;
  color: #444444;
  -webkit-transition: all .2s ease;
  -moz-transition: all .2s ease;
  -ms-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  margin-right: -4px;
}
.cssmenu > ul > li.active > a,
.cssmenu > ul > li:hover > a,
#cssmenu > ul > li > a:hover {
. color: #ffffff;
}
.cssmenu > ul > li > a:after {
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: -1;
  width: 100%;
  height: 120%;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  content: "";
  -webkit-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  -webkit-transform: perspective(5px) rotateX(2deg);
  -webkit-transform-origin: bottom;
  -moz-transform: perspective(5px) rotateX(2deg);
  -moz-transform-origin: bottom;
  transform: perspective(5px) rotateX(2deg);
  transform-origin: bottom;
}
.cssmenu > ul > li.active > a:after,
.cssmenu > ul > li:hover > a:after,
.cssmenu > ul > li > a:hover:after {
  background: #47c9af;
}
body
{
    overflow-y: scroll;
    overflow-x: hidden;
    font-family: Raleway,sans-serif;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}


.footer {
    margin-top: 10%;
    display: none;
}
.panel-body
{
    text-align: center;
    height: 147px;
}


.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #555;



    cursor: default;
}
#tabmenu
{
      width: 104%;
    margin-left: -17px;
    height: 69px;
    margin-top: 17px;
    border-bottom: 3px solid #47c9af;
}
.custom {
    color: #333;
    padding-left: 17px;
    padding-bottom: 9px;
    padding-top: 11px;
    width: 102%;
    background: #f2f2f2;
    font-family: Raleway,sans-serif;
    outline: none;
    margin-left: -28px;
    text-align: -webkit-left;
    border: 0px;
    font-size: 18px;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -70px;
}

.sidenav a:hover, .offcanvas a:focus{

}
a
{
    text-decoration: none !important;
}
input
{
    border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
select
{
    background: white;
}.navbar-default .navbar-toggle{
    display: none!important;
}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
#FromDate
{
    width: 106%;
    margin-left: 11px;
}
#ToDate
{
    margin-left: -16px;
    width: 103%
}
.branch
{
    text-align: left;
}
#main {

    transition: margin-left .5s;
    padding: 16px;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}

}
@media screen and (max-width: 380px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 20%;
    }
    #branch1
    {
        margin-left: -17px;
    }
    #donutchartmobile
    {
        margin-left: -54px;
    }
}
@media screen and (min-width: 412px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 15%;
    }
    #greetings
    {
        padding-top: 1px;
    }

}
@media screen and (min-width: 1200px) {
    #logoimage {
        margin-left: -69px;
    }
}
#mySidenav,#main
{
    display: none;
}

</style>
<script>
$(document).ready(function(){

    if (screen.width <= 800) {
        $('#mySidenav').hide();
        $('#main').hide();
       
        
        $.get("{{ URL::to('homemobile') }}",function(data){
            $('#mobile').html(data);
        });

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

    }
    else
    {
        $('#mySidenav').show();
        $('#main').show();

    }

});
</script>
<style type="text/css">
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>


@extends('layouts.app')

@section('content')

<div id="mySidenav" class="sidenav" style="height: 700px;      -webkit-transition:none !important;
-moz-transition:none !important;
-o-transition:none !important;
transition:none !important;       background-color: white">
<!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
<ul class="nav" id="side-menu" style="    margin-top: -2px;">


    <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
        <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

        <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
            width: 15px;
            float: left;
            margin-left: -2px;display: none;" id="close"> </a>

        </li>

        <li style="    height: 55px;    ">
            <a href="/cc/create?name={{ Auth::user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 4px; margin-top: -7px;
                width: 22px;    margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>

                </li>

                <li style="    height: 55px;    ">
                    <!-- This goes to the "index" function of the ccController
                        We are also sending a status "All" , so that we can verify during "Download as CSV" whether
                        we want to download "View Leads" records or "Individual Counts" records
                 -->
                    <!-- Route used is :: Route::resource('cc','ccController'); -->

                    <a href="/cc?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                        width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



                    </li>
                    <li style="    height: 55px;    ">
                        <a href="/product?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Order</p></a>



                        </li>
                        <li style="    height: 55px;    ">
                            <a href="/provisionalview?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;    margin-top: -74px;"> <img src="/img/provisional.png" class="img-responsive" alt="add" style=" margin-top: -7px;
                                width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Provisional Leads</p></a>



                            </li>
                            <li style="    height: 55px;    ">
                                <a href="/log" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;    margin-top: -89px"> <img src="/img/logs.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                                    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Logs</p></a>



                                </li>
                                <li style="    height: 55px;    ">
                                    <a href="/updatetables" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -104px;"> <img src="/img/tables.png" class="img-responsive" alt="add" style=" margin-top: -7px;
                                        width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Update Tables</p></a>



                                    </li>
                                    <li style="    height: 55px;    ">
                                        <a href="#" style="  border-bottom: 1px solid #bfbfbf; outline: none; height: 40px;margin-top: -119px;" id="filter"> <img src="/img/filter.png" class="img-responsive" alt="add" style=" margin-top: -7px;
                                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Filters</p></a>



                                        </li>
                                    </ul>
                                </div>

<div class="counter">
                                <div id="main" style="margin-top: -9%;background: #eee">


                                    <div class="container-fluid" style="    margin-top: -43px;">
                                        <div class="row">



                                            <div class="col-sm-10 col-sm-offset-1" style="margin-top:5%;    ">
                                                <div class="panel panel-default" style="margin-right: 6px;
                                                margin-left: -4px;height: 180px;">

                                                <div class="panel-body">

                                                    <h3 style="color:  #33cc33;    margin-top: 20px;"> Welcome !!! </h3>
                                                    <p> You are logged in as Admin. </p>
                                                      <div class="tab cssmenu" id="tabmenu">


                                                     <ul >
  <!-- changes here to make simply -->
  <li class="active"><a data-toggle="tab" href="#home" >Service</a></li>
    <li><a data-toggle="tab" href="#menu1" >Product Sale</a></li>
    <li><a data-toggle="tab" href="#menu3" >Product Rental</a></li>
    <li><a data-toggle="tab" href="#menu2" >Pharmacy </a></li>


  </ul>



                                        </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- Tabs and Pills start here -->
<!--
                                        <div class="col-md-10 col-md-offset-1 tab" id="tabmenu">

                                                     <ul class="nav nav-tabs" style="    width: 99%;">

  <li class="active"><a data-toggle="tab" href="#home">Service Details</a></li>
    <li><a data-toggle="tab" href="#menu1">Product Detials</a></li>
    <li><a data-toggle="tab" href="#menu2">Pharmacy Details</a></li>

  </ul>

                                        </div> -->

                                        <div class="col-md-12 tab-content" style="margin-top: 0px">
                                            <div id="home" class="tab-pane fade in active">
                                             <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
                                            <div class="panel panel-default">

                                                <!-- This link is allowing the user to View the counts for the status "New". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/provisionalview?name={{ Auth::user()->name }}">
                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/provisional.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f"> Provisional Leads </h5>
                                                        <h3 class="counter-count" style="color:#337ab7; margin-top: -8px;">  {{$provisionalcount}}</h3>
                                                     <!--      <p class="counter-count">79</p> -->
                                                    </div>

                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-sm-2" style="margin-top: 10px;">
                                            <div class="panel panel-default">
                                                <!-- This link is allowing the user to View the counts for the status "In Progress". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/adminassign?name={{ Auth::user()->name }}&status=New">
                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f">  New Leads </h5>
                                                        <h3  class="counter-count" style="color:orange; margin-top: -8px;"> {{ $newcountforall}} </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-2" style="margin-top: 10px;">
                                            <div class="panel panel-default">
                                                <!-- This link is allowing the user to View the counts for the status "In Progress". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/adminassign?name={{ Auth::user()->name }}&status=In%20Progress">
                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                                        <h3  class="counter-count" style="color:orange; margin-top: -8px;"> {{ $inprogresscountforall}} </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                      {{--    <div class="col-sm-2" style="margin-top: 10px;">
                                            <div class="panel panel-default">
                                                <!-- This link is allowing the user to View the counts for the status "In Progress". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/assessmentdone?name={{ Auth::user()->name }}">
                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f">  Assessment Done </h5>
                                                        <h3  class="counter-count" style="color:orange; margin-top: -8px;"> {{ $allservice_count}} </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div> --}}




                                        <div class="col-sm-2" style="margin-top: 10px;">
                                            <div class="panel panel-default">
                                                <!-- This link is allowing the user to View the counts for the status "Converted". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/adminassign?name={{ Auth::user()->name }}&status=Converted">

                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                                        <h3  class="counter-count" style="color:#33cc33; margin-top: -8px;"> {{ $convertedcountforall}} </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-2 " style="margin-top: 10px;">
                                            <div class="panel panel-default">
                                                <!-- This link is allowing the user to View the counts for the status "Deferred". Route for this
                                                Route::get('adminassign','AdminController@assigned'); -->
                                                <a href="/adminassign?name={{ Auth::user()->name }}&status=Deferred">
                                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                                        <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                                        <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                                        <h3  class="counter-count" style="color:#990000; margin-top: -8px;"> {{ $deferredcountforall }} </h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>



                                        <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
                                            <div class="panel panel-default" style="    width: 101%;
                                            margin-left: -5px;">
                                            <!-- This link is allowing the user to View the counts for the status "Dropped". Route for this
                                            Route::get('adminassign','AdminController@assigned'); -->
                                            <a href="/adminassign?name={{ Auth::user()->name }}&status=Dropped">
                                                <div class="panel-body" style="    text-align: -webkit-center;">
                                                    <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                                    <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                                    <h3  class="counter-count" style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}} </h3>
                                                </div>
                                            </a>
                                        </div>
                                    </div>





                                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:1%;    ">
                                        <div class="panel panel-default" style="margin-right: 6px;
                                        margin-left: -4px;">

                                        <div class="panel-body" style="height: auto;">
                                            <h3> Overall Status </h3>
                                            <div id="columnchart_values" ></div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-10   col-sm-offset-1" style="     margin-top: -7px;  margin-left: 88px;">
                                    <?php

                                    $j= 0;
                                    // Depending on the number of verticals we are displaying their names
                                    for($i=0; $i< $totalcount; $i++)
                                    {

                                        ?>

                                        <div class="col-sm-6 " style="margin-top: 3%;">
                                            <div class="panel panel-default" style="margin-right: 6px;
                                            margin-left: -4px;">
                                            <?php
                                            if($servicetype_name[$j]==" " || $servicetype_name[$j]=="All")
                                            {
                                             $servicetype_name[$j] = "Branch Head";
                                            }
                                            ?>
                                            <div class="panel-body" style="height: auto;">
                                                <h4> {{$servicetype_name[$j]}} : {{$city_name[$j]}} </h4>
                                                <div id="<?php echo $j; ?>" style="   margin-left: -11px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $j=$j+1;

                                }

                                ?>

                            </div>
                                            </div>




                                            <!-- Product details counts -->


                    <div id="menu1" class="tab-pane fade">
                             <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=New&type=Selling" data-toggle="tooltip" title="&#8377; {{$newsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/New Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  New  </h5>
                        <h3  class="counter-count" style="color:#337ab7; margin-top: -8px;">{{$newcount}}</h3>
                      <!--   <h3 style="color:#337ab7; margin-top: -8px;">{{$newsum}}</h3>  -->
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Processing&type=Selling" data-toggle="tooltip" title="&#8377; {{$processingsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/processing.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Processing   </h5>
                        <h3 style="color:orange; margin-top: -8px;"> {{$processingcount}} </h3>
                      <!--   <h3 style="color:orange; margin-top: -8px;"> {{$processingsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>


        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Awaiting%20Pickup&type=Selling" data-toggle="tooltip" title="&#8377; {{$awaitingpickupsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/awaiting pickup.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Awaiting Pickup </h5>
                        <h3 style="color:purple; margin-top: -8px;">{{$awaitingpickupcount}}</h3>
                      <!--   <h3 style="color:purple; margin-top: -8px;">{{$awaitingpickupsum}}</h3> -->
                    </div>
                </a>
            </div>
        </div>



        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Ready%20To%20Ship&type=Selling" data-toggle="tooltip" title="&#8377; {{$readytoshipsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/Ready To ship.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Ready To Ship </h5>
                        <h3 style="color:yellow; margin-top: -8px;">{{$readytoshipcount}} </h3>
                       <!--  <h3 style="color:yellow; margin-top: -8px;">{{$readytoshipsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



          <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Out%20for%20Delivery&type=Selling" data-toggle="tooltip" title="&#8377; {{$outfordeliverysum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/out for delivery.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Out for Delivery </h5>
                        <h3 style="color:pink; margin-top: -8px;">{{$outfordeliverycount}} </h3>
                       <!--  <h3 style="color:pink; margin-top: -8px;">{{$outfordeliverysum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



         <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Delivered&type=Selling"  data-toggle="tooltip" title="&#8377; {{$deliveredsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/delivered.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Delivered </h5>
                        <h3 style="color:#33cc33; margin-top: -8px;">{{$deliveredcount}} </h3>
                       <!--  <h3 style="color:#33cc33; margin-top: -8px;">{{$deliveredsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Cancelled&type=Selling" data-toggle="tooltip" title="&#8377; {{$canceledsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/cancelled.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Cancelled </h5>
                        <h3 style="color:#ff1a1a; margin-top: -8px;">{{$canceledcount}} </h3>
                      <!--   <h3 style="color:#ff1a1a; margin-top: -8px;">{{$canceledsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>
         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Order%20Return&type=Selling" data-toggle="tooltip" title="&#8377; {{$orderreturnsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$orderreturncount}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$orderreturnsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Received%20Order%20Return&type=Selling" data-toggle="tooltip" title="&#8377; {{$recievedorderreturnsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">Received Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$recievedorderreturncount}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$orderreturnsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>










                                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:1%;    ">
                                        <div class="panel panel-default" style="    width: 106%;
    margin-right: 6px;
    margin-left: -25px;">

                                        <div class="panel-body" style="       min-height: 612px;">
                                            <h3> Overall Status </h3>
                                          <div id="seeling_product_line_chart" style="     margin-left: -15px;   margin-top: -14px;"> </div>
                                           <div class="col-sm-12" style="text-align: center;">
                                <div class="col-sm-2" style="float: left;margin-left: 21%;">
                                    <div style="width: 12px; height: 12px;border: solid 7px #33cc00;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Bangalore</p>
                                </div>
                                 <div class="col-sm-2" style="float: left;margin-left: -31px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #3399ff;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Pune</p>
                                </div>
                                <div class="col-sm-2" style="float: left; margin-left: -66px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ffa64d;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Hyderabad </p>
                                </div>
                                <div class="col-sm-2" style="float: left;margin-left: -27px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #996633;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Chennai</p>
                                </div>

                                <div class="col-sm-3" style="float: left;margin-left: -38px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ff6600;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Hubballi-Dharwad</p>
                                </div>
                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-10   col-sm-offset-1" style="     margin-top: -7px;  margin-left: 88px;">
                                     <div class="col-sm-6 " style="margin-top: 3%;">
                                            <div class="panel panel-default" style="margin-right: 6px;
                                            margin-left: -4px;">

                                            <div class="panel-body" style="height: auto;">
                                                <h4> Branch </h4>
                                                <div id="product2" style="   margin-left: -11px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div> -->

     </div>


<!-- Rental Product details  -->



                    <div id="menu3" class="tab-pane fade">
                             <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=New&type=Rent">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$newrentsum}}" data-placement="bottom">
                        <img src="/img/New Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  New  </h5>
                        <h3 style="color:#337ab7; margin-top: -8px;">{{$newcountrent}}</h3>
                       <!--  <h3 style="color:#337ab7; margin-top: -8px;">{{$newrentsum}}</h3> -->
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Processing&type=Rent">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$processingrentsum}}" data-placement="bottom">
                        <img src="/img/processing.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Processing   </h5>
                        <h3 style="color:orange; margin-top: -8px;"> {{$processingcountrent}} </h3>
                       <!--  <h3 style="color:orange; margin-top: -8px;"> {{$processingrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Awaiting%20Pickup&type=Rent" data-toggle="tooltip" title="&#8377; {{$awaitingpickuprentsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/awaiting pickup.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Awaiting Pickup </h5>
                        <h3 style="color:purple; margin-top: -8px;">{{$awaitingpickupcountrent}}</h3>
                       <!--  <h3 style="color:purple; margin-top: -8px;">{{$awaitingpickuprentsum}}</h3> -->
                    </div>
                </a>
            </div>
        </div>



        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Ready%20To%20Ship&type=Rent" data-toggle="tooltip" title="&#8377; {{$readytoshiprentsum}} " data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/Ready To ship.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Ready To Ship </h5>
                        <h3 style="color:yellow; margin-top: -8px;">{{$readytoshipcountrent}} </h3>
                     <!--    <h3 style="color:yellow; margin-top: -8px;">{{$readytoshiprentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



          <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Out%20for%20Delivery&type=Rent" data-toggle="tooltip" title="&#8377; {{$outfordeliveryrentsum}} " data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/out for delivery.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Out for Delivery </h5>
                        <h3 style="color:pink; margin-top: -8px;">{{$outfordeliverycountrent}} </h3>
                       <!--  <h3 style="color:pink; margin-top: -8px;">{{$outfordeliveryrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



         <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Delivered&type=Rent" data-toggle="tooltip" title="&#8377; {{$deliveredrentsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/delivered.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Delivered </h5>
                        <h3 style="color:#33cc33; margin-top: -8px;">{{$deliveredcountrent}} </h3>
                       <!--  <h3 style="color:#33cc33; margin-top: -8px;">{{$deliveredrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Cancelled&type=Rent">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$canceledrentsum}}" data-placement="bottom">
                        <img src="/img/cancelled.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Cancelled </h5>
                        <h3 style="color:#ff1a1a; margin-top: -8px;">{{$canceledcountrent}} </h3>
                     <!--    <h3 style="color:#ff1a1a; margin-top: -8px;">{{$canceledrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>
         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Order%20Return&type=Rent" data-toggle="tooltip" title="&#8377; {{$orderreturnrentsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$orderreturncountrent}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$orderreturnrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Received%20Order%20Return&type=Rent" data-toggle="tooltip" title="&#8377; {{$recievedorderreturnrentsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Received Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$recievedorderreturncountrent}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$orderreturnrentsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>










                                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:1%;    ">

                                            <div class="panel panel-default" style="    width: 106%;
    margin-right: 6px;
    margin-left: -25px;">

                                        <div class="panel-body" style="       min-height: 612px;">
                                            <h3> Overall Status </h3>
                                           <div id="Rental_line_chart" style="     margin-left: -15px;   margin-top: -14px;"> </div>
                                           <div class="col-sm-12" style="text-align: center;">
                                <div class="col-sm-2" style="float: left;margin-left: 21%;">
                                    <div style="width: 12px; height: 12px;border: solid 7px #33cc00;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Bangalore</p>
                                </div>
                                 <div class="col-sm-2" style="float: left;margin-left: -31px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #3399ff;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Pune</p>
                                </div>
                                <div class="col-sm-2" style="float: left; margin-left: -66px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ffa64d;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Hyderabad </p>
                                </div>
                                <div class="col-sm-2" style="float: left;margin-left: -27px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #996633;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Chennai</p>
                                </div>

                                <div class="col-sm-3" style="float: left;margin-left: -38px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ff6600;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Hubballi-Dharwad</p>
                                </div>
                            </div>

                                        </div>
                                    </div>
                                </div>
                               <!--  <div class="col-sm-10   col-sm-offset-1" style="     margin-top: -7px;  margin-left: 88px;">
                                     <div class="col-sm-6 " style="margin-top: 3%;">
                                            <div class="panel panel-default" style="margin-right: 6px;
                                            margin-left: -4px;">

                                            <div class="panel-body" style="height: auto;">
                                                <h4> Branch </h4>
                                                <div id="product3" style="   margin-left: -11px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div> -->

     </div>





     <!-- Pharmacy details count -->




                       <div id="menu2" class="tab-pane fade">
                             <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=New&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$pnewsum}}" data-placement="bottom">
                        <img src="/img/New Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  New  </h5>
                        <h3 style="color:#337ab7; margin-top: -8px;">{{$pnewcount}}</h3>
                       <!--  <h3 style="color:#337ab7; margin-top: -8px;">{{$pnewsum}}</h3> -->
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Processing&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$pprocessingsum}}" data-placement="bottom">
                        <img src="/img/processing.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Processing   </h5>
                        <h3 style="color:orange; margin-top: -8px;"> {{$pprocessingcount}} </h3>
                      <!--   <h3 style="color:orange; margin-top: -8px;"> {{$pprocessingsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>


        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Awaiting%20Pickup&type=Pharmacy" data-toggle="tooltip" title="&#8377; {{$pawaitingpickupsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/awaiting pickup.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Awaiting Pickup </h5>
                        <h3 style="color:purple; margin-top: -8px;">{{$pawaitingpickupcount}}</h3>
                      <!--   <h3 style="color:purple; margin-top: -8px;">{{$pawaitingpickupsum}}</h3> -->
                    </div>
                </a>
            </div>
        </div>



        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Ready%20To%20Ship&type=Pharmacy" data-toggle="tooltip" title="&#8377; {{$preadytoshipsum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/Ready To ship.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Ready To Ship </h5>
                        <h3 style="color:yellow; margin-top: -8px;">{{$preadytoshipcount}} </h3>
                       <!--  <h3 style="color:yellow; margin-top: -8px;">{{$preadytoshipsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



          <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Out%20for%20Delivery&type=Pharmacy" data-toggle="tooltip" title="&#8377; {{$poutfordeliverysum}}" data-placement="bottom">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/out for delivery.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Out for Delivery </h5>
                        <h3 style="color:pink; margin-top: -8px;">{{$poutfordeliverycount}} </h3>
                  <!--       <h3 style="color:pink; margin-top: -8px;">{{$poutfordeliverysum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



         <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Delivered&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;"  data-toggle="tooltip" title="&#8377; {{$pdeliveredsum}}" data-placement="bottom">
                        <img src="/img/delivered.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Delivered </h5>
                        <h3 style="color:#33cc33; margin-top: -8px;">{{$pdeliveredcount}} </h3>
                       <!--  <h3 style="color:#33cc33; margin-top: -8px;">{{$pdeliveredsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Cancelled&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$pcanceledsum}}" data-placement="bottom">
                        <img src="/img/cancelled.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Cancelled </h5>
                        <h3 style="color:#ff1a1a; margin-top: -8px;">{{$pcanceledcount}} </h3>
                      <!--   <h3 style="color:#ff1a1a; margin-top: -8px;">{{$pcanceledsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>
         <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Order%20Return&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$porderreturnsum}}" data-placement="bottom">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$porderreturncount}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$porderreturnsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-2 " style="margin-top: 10px;">
            <div class="panel panel-default">
                <a href="/adminpharmacyassigned?name={{ Auth::guard('admin')->user()->name }}&status=Received%20Order%20Return&type=Pharmacy">
                    <div class="panel-body" style="    text-align: -webkit-center;" data-toggle="tooltip" title="&#8377; {{$precievedorderreturnsum}}" data-placement="bottom">
                        <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f"> Received Order Return </h5>
                        <h3 style="color:#990000; margin-top: -8px;">{{$precievedorderreturncount}} </h3>
                       <!--  <h3 style="color:#990000; margin-top: -8px;">{{$porderreturnsum}} </h3> -->
                    </div>
                </a>
            </div>
        </div>



                                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:1%;    ">
                                     <div class="panel panel-default" style="    width: 106%;
    margin-right: 6px;
    margin-left: -25px;">

                                        <div class="panel-body" style="       min-height: 612px;">
                                            <h3> Overall Status </h3>
                                           <div id="Pharmacy_chart" style="     margin-left: -15px;   margin-top: -14px;"> </div>
                                           <div class="col-sm-12" style="text-align: center;">
                                <div class="col-sm-2" style="float: left;margin-left: 21%;">
                                    <div style="width: 12px; height: 12px;border: solid 7px #33cc00;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Bangalore</p>
                                </div>
                                 <div class="col-sm-2" style="float: left;margin-left: -31px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #3399ff;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Pune</p>
                                </div>
                                <div class="col-sm-2" style="float: left; margin-left: -66px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ffa64d;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Hyderabad </p>
                                </div>
                                <div class="col-sm-2" style="float: left;margin-left: -27px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #996633;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Chennai</p>
                                </div>

                                <div class="col-sm-3" style="float: left;margin-left: -38px">
                                    <div style="width: 12px; height: 12px;border: solid 7px #ff6600;float: left;">
                                    </div>
                                    <p style="    padding-left: 7px;margin-top: -3px; float: left;">Hubballi-Dharwad</p>
                                </div>
                            </div>

                                        </div>
                                    </div>
                                </div>
                           <!--      <div class="col-sm-10   col-sm-offset-1" style="     margin-top: -7px;  margin-left: 88px;">
                                     <div class="col-sm-6 " style="margin-top: 3%;">
                                            <div class="panel panel-default" style="margin-right: 6px;
                                            margin-left: -4px;">

                                            <div class="panel-body" style="height: auto;">
                                                <h4> Branch </h4>
                                                <div id="product4" style="   margin-left: -11px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
 -->
     </div>
                                        </div>
                </div>
            </div>




            <div class="col-sm-12">
                <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

                    Copyright © Health Heal - 2017

                    <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
                </div>


            </div>


            <script>

            document.getElementById("mySidenav").style.width = "230px";
            document.getElementById("main").style.marginLeft = "200px";
            $("#close").show();

            function openNav() {
                document.getElementById("mySidenav").style.width = "230px";
                document.getElementById("main").style.marginLeft = "200px";
                $(".pa").show();

                $("#open").hide();
                $("#close").show();


            }

            function closeNav() {
                $("#open").show();
                $("#close").hide();
                document.getElementById("mySidenav").style.width = "50";
                document.getElementById("main").style.marginLeft= "50";
                $(".pa").hide();
            }
            </script>
            <!-- Footer  -->

            @endsection

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body" style="min-height: 238px;">
                            <div class="col-sm-6">
                                <center> <p> <b> Branch : </b> </p>
                                    <select name="Branch"  id="Branch" value="" style="width: 80%;" >
                                        <option value="All">All</option>
                                        @foreach($data as $city)
                                        <option value="{{ $city->name}}">{{ $city->name}}</option>
                                        @endforeach
                                    </select>
                                </center>
                            </div>
                            <div class="col-sm-6">
                                <center> <p> <b> Vertical: </b> </p>
                                    <select name="Vertical"  id="Vertical" value="" style="width: 80%;" >
                                        <option value="All">All</option>
                                        <option value="NursingServices">Nursing Services</option>
                                        <option value="PersonalSupportiveCare">Personal Supportive Care</option>
                                        <option value="Mathrutvam-Baby Care">Mathrutvam - Baby Care</option>
                                        <option value="Physiotherapy-Home">Physiotherapy - Home</option>
                                    </select>
                                </center>
                            </div>
                            <div class="col-sm-12">
                                <br>
                                <center> <p> <b> TimeLine: </b> </p>  </center>

                                <div class="col-sm-5">
                                    <!--   <input type="date" name="FromDate" id="FromDate" class="custominput"> -->
                                    <!--               <input

                                    class="datepicker custominput"
                                    name="date"
                                    type="text"
                                    name="FromDate" id="FromDate"
                                    > -->
                                    <input  type="text" placeholder="From Date"  id="FromDate" onkeypress=""/>


                                    <div id="container"></div>
                                </div>
                                <div class="col-sm-2" ><center style="    padding-top: 9px;"> To </center></div>
                                <div class="col-sm-5">
                                    <!--     <input type="date" name="ToDate" id="ToDate" class="custominput" style="width: 89%;"> -->

                                    <!--                           <input type="date" class="datepicker custominput"
                                    name="ToDate" id="ToDate"  style="width: 89%;"
                                    > -->
                                    <input  type="text" placeholder="To Date"  id="ToDate" onkeypress="return false"/>
                                </div>
                            </div>

                            <div class="col-sm-12" style="    text-align: -webkit-center;
                            padding-top: 5px;">
                            <br>
                            <button type="reset" class="btn btn-default" style="background: #00C851;    margin-right: 16px;" id="search" data-dismiss="modal">Apply Filter</button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="PleaseCauation" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="    width: 40%;">

                    <div class="modal-body" style="height: auto;     text-align: -webkit-center;">


                        <p> Nothing Selected </p>

                    </div>

                </div>

            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="PleaseDatebothCauation" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="    width: 40%;">

                    <div class="modal-body" style="height: auto;     text-align: -webkit-center;">


                        <p> Please Select the both Dates</p>

                    </div>

                </div>

            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="PleaseDateboth2Cauation" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="    width: 60%;">

                    <div class="modal-body" style="height: auto;     text-align: -webkit-center;">


                        <p> From date always should be less than To date</p>

                    </div>

                </div>

            </div>
        </div>
    </div>
        <div id="mobile" > </div>
