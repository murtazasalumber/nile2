

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){



    if (screen.width <= 800) {

        $('#mobile').show();
        window.location.replace("{{ URL::to('homemobilevertical') }}");

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

    }
    else
    {
        $('#mySidenav').show();
        $('#main').show();

    }

    //         $(function(){ // jQuery dom ready event
    //     if (window.location.href.toLowerCase().indexOf("loaded") < 0) {
    //         window.location = window.location.href + '?loaded=1'
    //     }
    // });
        $("#filter").click(function(){

                $('#myModal').modal('show');



        });
    $("#search").click(function(){
              


            var branch =  $('#Branch').val();
          var vertical=   $('#Vertical').val();
          var from=  $('#FromDate').val();
          var to= $('#ToDate').val();

          if(from == 0 && to == 0)
            // alert("branch and vertical");
        {
            alert("Nothing Seleted");
        }
        else 
        {
                $.get("{{ URL::to('searchfiltervertical') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to } ,function(data){
                $('#main').html(data);
          });
          
        }

        });
});
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
     var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcount}}, "#337ab7"],
        ["In Progress", {{$inprogresscount}}, "orange"],
        ["Converted", {{$convertedcount}}, "#33cc33"],
        ["Deferred", {{ $deferredcount }}, "#990000"],
        ["Dropped", {{$droppedcount}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 810,
        height: 400,
annotations: {
    alwaysOutside: false
},
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>

<?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{
    ?>

    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',    {{$statuscounts[$j][0]}} ],
            ['Converted Leads',  {{ $statuscounts[$j][1] }}],
            ['Deferred Leads', {{ $statuscounts[$j][2] }} ],
            ['Dropped Leads', {{ $statuscounts[$j][3] }}  ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend: 'none',
             colors: ['#337ab7', '#33cc33', '#990000', '#ff1a1a'],
            width: 380,
            height: 350,
            title: '',
            pieHole: 0.4,

            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>


<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<style type="text/css">

body::-webkit-scrollbar
{
    display: none;
}
.canvasjs-chart-credit
{
    display: none;
}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.btn
{
        color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
    background: white;
}

body
{
    overflow-y: scroll;
    overflow-x: hidden;
    font-family: Raleway,sans-serif;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}
#mySidenav, #main ,#mobile
{
    display: none;
}
.footer {
    display: none;
}
.panel-body
{
    text-align: center;
    height: 147px;
}
</style>
<style>


.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -66px;
}

.sidenav a:hover, .offcanvas a:focus{

}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

#main {
    transition: margin-left .5s;
    padding: 16px;
}
a
{
    text-decoration: none !important;
}

.custom
{
    float: left;
    height: 100%;
    background: #30a5ff;
    width: 50%;
    padding-top: 10px;
    text-align: -webkit-center;
}
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}

</style>
@extends('layouts.app')

@section('content')



<div id="mySidenav" class="sidenav" style="height: 653px;     -webkit-transition:none !important;
  -moz-transition:none !important;
  -o-transition:none !important;
  transition:none !important;         background-color: white">
    <!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
    <ul class="nav" id="side-menu" style="    margin-top: -2px;">


        <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
            <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

            <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
                width: 15px;
                float: left;
                margin-left: -2px;display: none;" id="close"> </a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/Verticalhead/create?name={{ Auth::guard('admin')->user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 4px;  margin-top: -9px;
    width: 22px;    margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>

            </li>
            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="   margin-top: -9px;
    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>
            </li>


            <li style="    height: 55px;    ">
                        <a href="/vh?name={{ Auth::guard('admin')->user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



                        </li>

           <li style="    height: 55px;    ">
                        <a href="/product?name={{ Auth::guard('admin')->user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Order</p></a>



                        </li>


        </ul>
    </div>
    <div id="main" style="margin-top: 4%;background: #eee">


        <div class="container-fluid" style="    margin-top: -23px;">
            <div class="row">



            <div class="col-sm-10 col-sm-offset-1" style="margin-top:6%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;height: 180px;">

                    <div class="panel-body">
                        <h3 style="color:  #33cc33;    margin-top: 36px;"> Welcome !!! </h3>
                        <p> You are logged in as Vertical. </p>
                     
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
            </div>



            <!--  Count New  code -->

         <!--    <div class="col-sm-3">
                     <div class="panel panel-default">
                    <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/speech-bubble.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;">  In Progress Leads </h5>
                            <h3 style="color:#5cb85c; margin-top: -8px;"> {{$inprogresscount}}</h3>
                        </div>
                    </a>
                </div>
            </div> -->
            <div class="col-sm-2  col-sm-offset-1 " style="margin-top: 10px;    ">
                <div class="panel panel-default">

                    <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcount}}</h3>
                        </div>

                    </a>
                </div>
            </div>





            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                            <h3 style="color: orange; margin-top: -8px;"> {{$inprogresscount}}</h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=Converted">

                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                            <h3 style=";color:#33cc33; margin-top: -8px;">{{$convertedcount}}</h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-2  " style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f ">  Deferred Leads </h5>
                            <h3 style=" color:#990000; margin-top: -8px;">{{$deferredcount}}</h3>
                        </div>
                    </a>
                </div>
            </div>



            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default" style="    width: 101%;
                margin-left: -5px;">
                <a href="/verticalassign?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                    <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                        <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                        <h3 style="color:#ff1a1a; margin-top: -8px;">{{$droppedcount}} </h3>
                    </div>
                </a>
            </div>
        </div>


        <div class="col-sm-10 col-sm-offset-1 " style="margin-top:2%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;">

                    <div class="panel-body" style="height: auto;text-align: center;">
                        <h3> Overall Status </h3>
                        <div id="columnchart_values" ></div>

                    </div>
                </div>
            </div>


        <div class="col-sm-10 col-sm-offset-1" style="    margin-left: 82px;">



                        <?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{

    ?>

   

  <?php 
    if($i=="1")
    {?>
        
             <div class="col-sm-6" style="    margin-top: -24px;">
        <div class="panel panel-default" style="    min-height: 441px;">

            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
               
                
                <?php 
                    if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                        {?>
                     <h3> {{$User[$i]}}</h3>
                    <p> No count </p>
                    <?php } else
                    {?> <h3> {{$User[$i]}}</h3>
                        <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                   <?php }?>
            </div>
        </div>

    </div>
<?php
   }else if($i=="2")
   {?>

            <div class="col-sm-6" style="    margin-top: -4px;" >
        <div class="panel panel-default" style="    min-height: 441px;">

            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
               
                
                <?php 
                    if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                        {?>
                     <h3> {{$User[$i]}}</h3>
                    <p> No count </p>
                    <?php } else
                    {?> <h3> {{$User[$i]}}</h3>
                        <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                   <?php }?>
            </div>
        </div>

    </div>


    <?php

   }else
   {?>

        <div class="col-sm-6" style="    margin-top: -4px;">
        <div class="panel panel-default" style="    min-height: 441px;">

            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
               
                
                <?php 
                    if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                        {?>
                     <h3> {{$User[$i]}}</h3>
                    <p> No count </p>
                    <?php } else
                    {?> <h3> {{$User[$i]}}</h3>
                        <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                   <?php }?>
            </div>
        </div>

    </div>
    <?php
   }




$j=$j+1;

}

?>


        </div>

<!-- <?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{

    ?>

    <div class="col-sm-4" style="margin-top: 2%;">
        <div class="panel panel-default">

            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
                <h3> {{$User[$i]}}</h3>
                <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
            </div>
        </div>

    </div>





<?php
$j=$j+1;

}

?> -->


</div>

</div>


<div class="col-sm-12">

    <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

        Copyright © Health Heal - 2017

        <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
    </div>
</div>




<script>

document.getElementById("mySidenav").style.width = "230px";
document.getElementById("main").style.marginLeft = "200px";
$("#close").show();

function openNav() {
    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $(".pa").show();

    $("#open").hide();
    $("#close").show();


}

function closeNav() {
    $("#open").show();
    $("#close").hide();
    document.getElementById("mySidenav").style.width = "50";
    document.getElementById("main").style.marginLeft= "50";
    $(".pa").hide();
}
</script>
<!-- Footer  -->
</div>



@endsection
<div id="mobile" style="    margin-top: 15%;">
    @extends('admin.mobilevertical')
</div>

