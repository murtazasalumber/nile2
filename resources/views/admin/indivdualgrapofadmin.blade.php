
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      
var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deffered", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);
     

      var options = {
        title: "",
        width: 250,
        height:250,
annotations: {
    alwaysOutside: false
},
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
      chart.draw(view, options);
  }

  $(window).resize(function(){
  drawChart();
  
});
  </script>

<body>
	
		 <div class="col-sm-12 " >
                    <div class="panel panel-default" style="    width: 114%;
    margin-left: -8%;">

                    <div class="panel-body" style="height: auto;text-align: -webkit-center;">
                      
                             <div id="columnchart_values1" ></div>

                    </div>
                </div>
            </div>

</body>