<!-- This is the view page when the user clicks on View Leads button when logged in through admin || comments by jatin -->
<?php
$value= Session::all();

$value=session()->getId();
  //echo $value;


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Adding bootstrap cdn for css here -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <!-- Adding jquery library here -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Adding bootstrap cdn for javascript here -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- Including the css file here -->
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />

  <!-- Including the css file here by another way -->
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />

  <!-- Including the javascript file here -->
<script src="js/footable.js" type="text/javascript"></script>


<style type="text/css">
  .imgg
{
   margin-top: -13px;
    margin-left: -80px;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
.footer
{
  margin-top: 2%;
}
@media screen and (max-height: 1200px) {
    #loo
    {
          margin-left: 13%;
    }
}
@media (max-width: 1200px)
{
 #buttonforcreate
  {
      margin-top: 10%;
  }
   #second
   {
    margin-bottom: 18px;
   }
}
hr
{
      border-top: 1px solid #000;
}
@font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
</style>
  </head>
  <body style="font-family: myFirstFont;">
   <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- This link on being clicked will take to the admin's login page -->
            <div id="loo">
            <a class="navbar-brand" rel="home" href="/admin/home" >
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right" style="text-align: right;">
                        <!-- Authentication Links -->

                        <!-- if the admin is not logged in show the login button-->
                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

                        <!-- if the admin is loggedin show a dropdown menu with the person's name , home and logout button     -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="/admin/home"  style="font-family: myFirstFont;"
                                            >
                                            Home
                                        </a>
                                        <!-- to ensure the correct person is logged out passing a csrf token alogn with route -->
                                        <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                       <center> <h3> Version Release Notes </h3> </center>
                    </div>
                </div>
            </div>


               <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 3.2 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <td style="text-align: center;">86</td>
                                        <td>PSC and NS new requirements</td>
                                        <td>Changes in <b> Vitals tab</b>:<br>
BP values in mmHg , Weight in kg and Respiration &Pulse in bpm.<br>
Changes in <b> Orientation</b>:<br>
GCS values inclusion  and include "Not Applicable" as a default value for some fields.<br>
Changes in <b>Respiratory</b>:<br>
Drop down to H/O field, color of Phlegm.
Added new field Placement of ICD and no of days, dropdown for ventilation mode, new tab for a prescription.
Changes in <b>diet</b> tab based on the dropdown selection, respective field are displayed.Added Quantity, Time , Infusion Rate, new fields and calculation for the same.

 </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">87</td>
                                        <td>New Requirements </td>
                                        <td>Addition of allergies field to mother and baby details tab, immunization based on the DOB, Added Denver Scale and GCS.If immunization is missed system will reschedule within buffer period. </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">88</td>
                                        <td>Nothing Significant  </td>
                                        <td>Nothing Significant if there doesn't exist any other values.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">89</td>
                                        <td>Remainder</td>
                                        <td>Send SMS and Email to verticals, co-ordinator, and manager periodically until the case is dropped or converted. </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">90</td>
                                        <td>Validation   </td>
                                        <td>Validation for Create and Update form for all fields.</td>
                                      </tr>
                                     <tr>
                                        <td style="text-align: center;">91</td>
                                        <td>Product Edit  </td>
                                        <td>Update the entered product details.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">92</td>
                                        <td>History</td>
                                        <td>Complete logs for a particular lead.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">93</td>
                                        <td>SMS to Feild executive</td>
                                        <td>SMS to Feild executive when status is ready to ship.</td>
                                      </tr>
                                        <tr>
                                        <td style="text-align: center;">94</td>
                                        <td>Mandatory field in assessment forms</td>
                                        <td>To ensure assignment has done properly few fields were made mandatory.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">95</td>
                                        <td>Dashboard changes</td>
                                        <td>Addition one more tab for provisional lead count.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">96</td>
                                        <td>Enable/Disable SMS and Email</td>
                                        <td>Added feature for SMS and Email enable and disable.</td>
                                      </tr>
                                     
                                     
                                     
                                     
                                     
                                    
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>
               <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 3.1 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <td style="text-align: center;">80</td>
                                        <td>Dashboard Changes</td>
                                        <td>Removed duplicate graphs for vertical head and branch head if they belong to same department and city. <br>
Modified the counts accordingly.
 </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">81</td>
                                        <td>Logic to check for existing leads during creation </td>
                                        <td>Displaying all leads that have been created with the mobile number entered during creation of new lead.<br>Checking after submit button is clicked on submission of new lead whether any lead with same department and city already exist.<br>In case the existing lead is in “Dropped” stage checking if one month has passed from the creation of that lead. If not, then changing the status of that lead to “New”.<br>If none of the above case holds true or the user still clicks on “YES”, create a new lead </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">82</td>
                                        <td>Restrict URLs  </td>
                                        <td>82  Restrict URLs In case any person tries to type routes in URL, ensuring that any privilege that has been given to the admin or management is not accessible to any person of any other designation. </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">83</td>
                                        <td>Token Mismatch  </td>
                                        <td>Anytime a new tab is opened and the user is logged out from the first tab or any other tab redirecting them in the new tab to the login page instead of giving token mismatch. </td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">84</td>
                                        <td>Referal partner and branch head mobile views   </td>
                                        <td>Dashboard given for referral partner and branch head for mobile. </td>
                                      </tr>
                                     <tr>
                                        <td style="text-align: center;">85</td>
                                        <td>SMS and mail on change of vertical head  </td>
                                        <td>While editing a lead if it is wrongly assigned to particular service type and itis then assigned to a new service type then send an SMS and mail to the newly assigned vertical head. <br>Also allow the newly assigned vertical head to freshly assign a coordinator. </td>
                                      </tr>
                                     
                                     
                                     
                                     
                                     
                                    
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 3.0 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">66</td>
                                        <td>Feature for already existing email in DB  </td>
                                        <td>During updating for employees if an emailid already exists in the table and we try to update the same emailid for another employee show an error notification.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">67</td>
                                        <td>Unique REG</td>
                                        <td>No duplicate REG value allowed for any employee.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">68</td>
                                        <td>Privilege change</td>
                                        <td>Disabled co-ordinator privilege for selection of vertical head in lead edit form.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">69</td>
                                        <td>User changing service </td>
                                        <td>If user wants to change the service type of an existing lead they can do so now.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">70</td>
                                        <td>UI related issues w.r.t Assessment form</td>
                                        <td><b> Vital Signs:</b> In General History: If patient is Male Mensural & OBG history must be hidden
                                          <hr>
                                              <b>Diet: </b>Field Validation for Breakfast, lunch, snack and dinner, insert timer
                                              <hr>
                                                Position<br>
                                                Fowler's instead of fowler<br>
                                                Right lateral instead of rt_lateral<br>
                                                Multiselect check box option is included<br><br><br>

                                            <b>Nutrition</b><br>
                                              Type and Adequacy, change the sequence
                                            <hr>
                                               <b> Pain</b><br>
                                            1. Eliminated "scale" from Face Reading and Numeric Reading<br>
                                            2. Severity instead of 1 to 10 values implemented movable scale<br>
                                            <hr>
                                            <b>Communication</b><br>
                                            1. Included others in language dropdown and provided provision to enter another language only in case of "others"<br>
                                            2. Language select is a multi-select.<br>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">71</td>
                                        <td>Mail and SMS for Products and Pharmacy</td>
                                        <td>Sending a mail and SMS while creating a lead, assigning to someone and changing the status to different peoples and different scenarios.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">72</td>
                                        <td>Mail for comments updating.</td>
                                        <td>Sending a mail to vertical head when comment is updated by anyone.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">73</td>
                                        <td>Added Multi role functionality.  </td>
                                        <td>1. Added the selection of designation, department and reporting to for all employees by management and admin<br>
2. Created Dashboards, added Create Service Leads, Create Product leads, View Service and Product Leads, Download as CSV, Search filter functionalities for combination of roles depending on requirement
.</td>
                                      </tr>
                                     <tr>
                                        <td style="text-align: center;">74</td>
                                        <td>Branch Head graphs  </td>
                                        <td>Included Branch head pie charts along with Vertical Heads on the admin and management dashboards. </td>
                                      </tr>
                                     
                                      <tr>
                                        <td style="text-align: center;">75</td>
                                        <td>Product and Pharmacy Graphs   </td>
                                        <td>Product and Pharmacy Graphs Added Product Selling, rental and Pharmacy graphs and counts for the corresponding dashboards. </td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">76</td>
                                        <td>Country codes and Indian cities  </td>
                                        <td>Added country codes for mobile numbers and dropdown for Indian cities for all dashboards for creation as well as updating. </td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">77</td>
                                        <td>Follow updates and drop reason  </td>
                                        <td>Added follow updates input when the status is changed to “Deferred” and added drop reason input when status is changed to “Dropped”. </td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">78</td>
                                        <td>Transportation Charges  </td>
                                        <td>Added Transportation Charges when Product is selected as “Rent” and calculating the sum and showing as total amount. </td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">79</td>
                                        <td>Multi-select for Designation, Department and Reporting as well as Languages  </td>
                                        <td>1. Added Multi-select option with restriction to “3” when the employees are being created or updated for assigning designations, department and Reporting to <br>
2. Added Multi-select for Languages during Assessment form.
 </td>
                                      </tr>
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>
             <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 2.3 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">56</td>
                                        <td>UI changes </td>
                                        <td>Added 3 clickable buttons (Selling product Details, Rental product details and pharmacy details) to give Admin and Management the privilege to check the counts and statuses of each product and pharmacy.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">57</td>
                                        <td>Download CSV and Search Filter</td>
                                        <td>Added Download as CSV and Search filter functionality for all the 3 tabs for each status. Also added “Leadid” as a search filter along with others.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">58</td>
                                        <td>Log functionality</td>
                                        <td>Added Log functionality for Update Tables tabs and for products and pharmacy updation and creation statuses.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">59</td>
                                        <td>Product Name and SKUid search </td>
                                        <td>When the user types one letter of the Product name, he/she will be able to see all products having that character and select the appropriate product. As soon as the product name is selected, the corresponding SKUid will be displayed in the field as well.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">60</td>
                                        <td>Solved Update Employee name and Login error</td>
                                        <td>If the Management changes the name of the employee, the changes will be made in both the “employees” and the “admin” table so that Login can happen properly without any error.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">61</td>
                                        <td>Display Service types to admin and management in pie chart.</td>
                                        <td>Instead of showing the Names of vertical heads , show the servicetype and the branch on the top of the pie chart.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">62</td>
                                        <td>AssignedTo column in Verticals View page.</td>
                                        <td>Added an “AssignedTo” column on the View Leads page for Vertical Head.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">63</td>
                                        <td>Tooltip for button/icons.  </td>
                                        <td>Added tooltips on buttons and icons to make them understandable.</td>
                                      </tr>
                                     <tr>
                                        <td style="text-align: center;">64</td>
                                        <td>Marketing Manager View  </td>
                                        <td>Can create all types of leads. Can view all service and provisional leads but can only view product leads created by them. </td>
                                      </tr>
                                     
                                      <tr>
                                        <td style="text-align: center;">65</td>
                                        <td>Product Leads view   </td>
                                        <td>Coordinators and Vertical Heads can view only the product leads created by them. </td>
                                      </tr>
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>

  <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 2.2 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">44</td>
                                        <td>Graph filters </td>
                                        <td>Added graph filters for Mgmt and admin.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">45</td>
                                        <td>Change users</td>
                                        <td>Added provitional changes to users for different categories.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">46</td>
                                        <td>Version updates</td>
                                        <td>Added version updates.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">47</td>
                                        <td>.csv format</td>
                                        <td>Can download all leads in .csv format.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">48</td>
                                        <td>Alternate number sms.</td>
                                        <td>Added feature to send sms to alternate mobile number also.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">49</td>
                                        <td>Changes related to product department.</td>
                                        <td>Assisging co-ordinator , vertical assign and changing status feature added to Product department.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">50</td>
                                        <td>Add version notes to application.</td>
                                        <td>Added a feature in dash board to display the version details.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">51</td>
                                        <td>Dashboard changes.  </td>
                                        <td>Modification in graphs and resolved issues raised in Mantis.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">52</td>
                                        <td>Popover addition.</td>
                                        <td>Resolved issues raised in Mantis.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">53</td>
                                        <td>Model Popup.</td>
                                        <td>Confirmation for deleting a row from DB for admin and management table view.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">54</td>
                                        <td>Edit button.</td>
                                        <td>Eliminated external edit button from table view, same functionality added to their unique id.[onclick navigates edit view]</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">55</td>
                                        <td>Updated view.</td>
                                        <td>Updated web and mobile view for product manager, pharmacy manager, feild executive, feild officer as per client requirements.</td>
                                      </tr>
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>

  <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 2.1 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">39</td>
                                        <td>Mobile view issues</td>
                                        <td>Resolved mobile view issues.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">40</td>
                                        <td>Backend issues</td>
                                        <td>Resolved issues related backend database and backend controller code.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">41</td>
                                        <td>Page redirection errors</td>
                                        <td>Solved issues with page redirection.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">42</td>
                                        <td>Filters for Service Leads</td>
                                        <td>Added filters for service leads.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">43</td>
                                        <td>Mantis issues.</td>
                                        <td>Resolved issues raised in Mantis.</td>
                                      </tr>
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 2.0 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">33</td>
                                        <td>Store Hippo</td>
                                        <td>Extracted product data from store hippo api , created schema and transfered all data from store hippo to our own database.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">34</td>
                                        <td>Add Product</td>
                                        <td>New feature to add products seperately.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">35</td>
                                        <td>Add Service</td>
                                        <td>While adding product can include service required.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">36</td>
                                        <td>Dashboard View</td>
                                        <td>User dashboard can view data statics in form of graphs and charts.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">37</td>
                                        <td>Pharmacy</td>
                                        <td>Pharmacy data is pulled from pharmacy api in .csv format and schema is created and data is replicated to our own server.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">38</td>
                                        <td>Views</td>
                                        <td>Two views created namely for Product and Pharmacy Manager.<br> They can create new product lead and view the same. </td>
                                      </tr>
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>


             <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.5 </b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">28</td>
                                        <td>SMS functionality</td>
                                        <td>Send sms to clients, verticals, co-ordinators, referal partners when a new lead is created.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">29</td>
                                        <td>Name Conflicts</td>
                                        <td>Multiple name conflict fixed, issue w.r.t name , same person name entered as full, partial and with initials while creating different leads.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">30</td>
                                        <td>Form view</td>
                                        <td>Lead creation form view changes and added validation for mobile number.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">31</td>
                                        <td>Sorting functionality</td>
                                        <td>Sorting was implemented as per their Department and designation.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">32</td>
                                        <td>Lead Conflict</td>
                                        <td>Lead and service schema with same lead id had different data, which lead to error while fetching few data. Rectified and eliminated duplicate values.</td>
                                      </tr>
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>




             <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.4</b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">23</td>
                                        <td>Provisional Leads Creation</td>
                                        <td>Provisional leads creation for referal partner.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">24</td>
                                        <td>Provisional Leads View</td>
                                        <td>Customer care can view provisional leads and create the actual lead from it, admin and management can view but cannot create a lead.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">25</td>
                                        <td>Resolved Conflicts</td>
                                        <td>Resolved duplicate leadid in databases and fixed offset error in server.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">26</td>
                                        <td>Bug Fixing</td>
                                        <td>Fixed bugs  that are rised in bug tracker like adding filters to leads view</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">27</td>
                                        <td>Pages view</td>
                                        <td>Changed the footer , added new form view.</td>
                                      </tr>
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>



             <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.3</b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">19</td>
                                        <td>Automatic login</td>
                                        <td>Automatic login for referal partner. <br> If referal partner is created by admin automatically login credentials is granted w.r.t emailid provided by them.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">20</td>
                                        <td>Emp login</td>
                                        <td>For new employee id is created by admin, automatically login credentials is granted w.r.t designation and emailid provided by healthheal.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">21</td>
                                        <td>User settings</td>
                                        <td>All users can change their password by authenticating with old password.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">22</td>
                                        <td>Bug Fixing</td>
                                        <td>Fixed bugs  that are rised in bug tracker like adding filters to leads view</td>
                                      </tr>
                                       
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>




             <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.2</b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">16</td>
                                        <td>Email</td>
                                        <td>When a new lead is created emails are sent to verticalhead and client</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">17</td>
                                        <td>Validations</td>
                                        <td>Validation for mobile number, emailid, pincode, emergency contact, reference, referencetype, assissgnto, branch.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">18</td>
                                        <td>Comment Box</td>
                                        <td>Comment box to write comments w.r.t leads.</td>
                                      </tr>
                                      
                                       
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>



                 <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.1</b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;">14</td>
                                        <td>Forgot Password</td>
                                        <td>If a user forgets password, through forgot password, link is sent to his email and can change it</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">15</td>
                                        <td>Login to referal partner</td>
                                        <td>Different referal partner can log in and view leads created or refered by them.</td>
                                      </tr>
                                    
                                      
                                       
                                     
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>



                 <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 2%;">

                    <h4><b>  Release Notes 1.0</b></h4>
                                <table class="table table-bordered" style="margin-top: 2%;">
                                    <thead>
                                      <tr>
                                        <th style="width: 8%;text-align: -webkit-center;">Sl</th>
                                        <th style="    text-align: -webkit-center;width: 23%;">Features of LMS</th>
                                        <th style="text-align: -webkit-center;">Description</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td style="text-align: center;    vertical-align: middle;" rowspan="7">1</td>
                                        <td rowspan="7" style="    vertical-align: middle;"> Login</td>
                                        <td >If a user forgets password, through forgot password, link is sent to his email and can change it</td>
                                      </tr>
                                    
                                       <tr>
                                     
                                       
                                        <td >Admin login with all credentials to view, modify , add fields to DB.</td>
                                      </tr>
                                     
                                      <tr>
                                        
                                        <td >Management login to create, view all the leads, Update values to Tables, such as new referrer etc..,</td>
                                      </tr>
                                     <tr>
                                        
                                        <td >Vertical login to view their respective leads, assign to their team of coordinators, and assign to other verticals.</td>
                                      </tr>
                                    
                                     
                                       
                                      <tr>
                                        
                                        <td >Coordinator Login to create / view their respective leads. </td>
                                      </tr>
                                      <tr>
                                        
                                        <td >Lead Provider / Referrer Login  to view the leads referred by them. They can be individuals or Institutions. </td>
                                      </tr>
                                       <tr>
                                        
                                        <td >Customer Care Login  to create and assign lead to respective verticals, Provision to update the details. </td>
                                      </tr>

                                       <tr>
                                        <td style="text-align: center;">2</td>
                                        <td>Lead Registration</td>
                                        <td>Registration Form allows users to create a new lead with related details after successful login. Provisioned for all the users except Lead Provider.</td>
                                      </tr>
                                        <tr>
                                        <td style="text-align: center;">3</td>
                                        <td>Lead Assessment</td>
                                        <td>Post Registration the Assessment is carried out for the Lead. Based on Vertical type different Forms are provisioned to update the assessment details.</td>
                                      </tr>
                                        <tr>
                                        <td style="text-align: center;">4</td>
                                        <td>Forgot Password </td>
                                        <td>Provision for the user to request the password, if forgotten. An email is received to the mentioned email id.</td>
                                      </tr>
                                       <tr>
                                        <td style="text-align: center;">5</td>
                                        <td>Landing Page </td>
                                        <td>Navigate to New and View lead.<br>For User Type Admin, Provision to Update the  Tables and view logs.</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align: center;">6</td>
                                        <td>New Lead </td>
                                        <td>New lead will be created with Client details, Service Required For, Service Details, Product and assigning the right coordinator.</td>
                                      </tr>
                                      
                                       <tr>
                                        <td style="text-align: center;">7</td>
                                        <td>View Lead </td>
                                        <td>Allows yo view lead details and create an assessment form w.r.t service details such as Nursing, Personal Supportive Care , Mathruthvam and Physiotherapy. <br>After create can edit few details</td>
                                      </tr>

                                       <tr>
                                        <td style="text-align: center;">8</td>
                                        <td>Update fields </td>
                                        <td>
                                        Editing all fields except Lead Id & creation date & Time.
                                        </td>
                                        <tr>
                                        <td style="text-align: center;">9</td>
                                        <td>Activity Logging </td>
                                        <td>Each and every activity performed on the Database are logged along with user login and log out.</td>
                                      </tr></td>
                                      </tr>


                                       <tr>
                                        <td style="text-align: center;">10</td>
                                        <td>Activity Log view</td>
                                        <td>View for the Admin user to see the activities carried out till date by all the Users. Provision to filter by LeadID & User.</td>
                                      </tr></td>
                                      </tr>
                                      
                                      <tr>
                                        <td style="text-align: center;">11</td>
                                        <td>Emails</td>
                                        <td>After lead creation Email is sent to lead creator, referer, VerticalHead and Customer.<br>
                                        After lead Assignment Email sent to vertical head to Co-ordinator, incase of vertical change sent for different user/VerticalHead.<br>
                                        After Assesment Email is sent to verticalhead and customer.</td>
                                      </tr>


                                      <tr>
                                        <td style="text-align: center;">12</td>
                                        <td>Data Migration</td>
                                        <td>Old data is migrated and synced to new app, hence user can view , modify existing data.</td>
                                      </tr>


                                       <tr>
                                        <td style="text-align: center;">13</td>
                                        <td>Log Out</td>
                                        <td>Logging out of Session for all the users.</td>
                                      </tr>
                                    </tbody>
                                  </table>
                    </div>
                </div>
            </div>
<!-- 
   @extends('layouts.footer') -->
  </body>
  </html>
