<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
 var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcount}}, "#337ab7"],
        ["In Progress", {{$inprogresscount}}, "orange"],
        ["Converted", {{$convertedcount}}, "#33cc33"],
        ["Deferred", {{ $deferredcount }}, "#990000"],
        ["Dropped", {{$droppedcount}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);
     

      var options = {
        title: "",
        width: 280,
        height: 400,
annotations: {
    alwaysOutside: false
},
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };

      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>

<boady>
 <div class="col-sm-12" style="margin-top:6%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;">

                    <div class="panel-body" style="height: auto;text-align: center;">
                        <div id="columnchart_values" ></div>

                    </div>
                </div>
            </div>
</boady>