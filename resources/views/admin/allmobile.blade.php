 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Bangalore', 'Personal Suportive Care'],
          ['New Lead',  {{$newcountforall}}],
          ['In Progress',  {{$inprogresscountforall}}],
          ['Converted',  {{$convertedcountforall}}],
          ['Deferred',  {{$deferredcountforall}}],
		      ['Dropped',  {{$droppedcountforall}}]
        ]);

        var options = {
          title: '',
          curveType: 'none',
		  legend : { position:"none"},
          legend: { position: 'bottom' },
		      colors: ['#337ab7', 'orange', '#33cc33','#990000','#ff1a1a'],
           pointSize: 20,
          pointShape: 'square'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
<div class="container">

<div class="row">
			<div class="col-sm-12 " style="margin-top: 10px;">
                        <div class="panel panel-default">

                                @if($designation=="Management")
                                <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @else
                                    <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                        @endif
                                <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                        </div>

                    </a>
                </div>

                </div>

                <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{$inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> {{$convertedcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12 " >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> {{$deferredcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-12" >
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        @if($designation=="Management")
                        <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                            @else
                            <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch={{$branch}}&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @endif                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}} </h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-12">
                		<div class="panel panel-default" style="margin-right: 6px;
                    margin-left: -4px;text-align:center;">

                    <div class="panel-body" style="height: auto;">
                    <h3> {{$vertical}} of {{$branch}} Report </h3>
                       <div id="curve_chart" style="width: 250px; height: 250px"></div>
                    </div>
                </div>
                </div>


</div>
</div>