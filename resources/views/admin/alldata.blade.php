
<!-- <style type="text/css">
  body
{
    overflow-y: scroll;
  overflow-x: scroll;
  font-family: Raleway,sans-serif;
}
</style> -->
<script>
$(document).ready(function(){
  $('.footable').footable();



});

</script>


<div style="overflow-x: scroll;">
  @if(count($data1) > 0)



      @if ($filter1 === "fName")
           <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td >{{$lead->AssignedTo}}</td>
                                    <td style="color:red;">{{$lead->fName}} {{$lead->mName}} {{$lead->lName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td>{{$lead->Branch}}</td>
                                    <td>{{$lead->Source}}</td>
                                    <td>{{$lead->ServiceType}}</td>
                                 <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "AssignedTo")
            <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td style="color:red;" >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td>{{$lead->Branch}}</td>
                                    <td>{{$lead->Source}}</td>
                                    <td>{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "Source")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td>{{$lead->Branch}}</td>
                                    <td style="color:red;">{{$lead->Source}}</td>
                                    <td>{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "ServiceType")
            <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td>{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td style="color:red;">{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "Branch")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td style="color:red;">{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "leads.id")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td style="color:red;"> <a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>" style="color:red;">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

      @elseif ($filter1 === "ServiceStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td style="color:red;">{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>




                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

                @elseif ($filter1 === "MobileNumber")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>
                                                

                                              

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
                                    <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>
                                    



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
                @elseif ($filter1 === "Alternatenumber")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>

                                                <th><b> Lead ID</b></th>
                                                <th  data-hide="phone,tablet"><b> Created At </b></th>
                                                <th  data-hide="phone,tablet"><b> Assigned To </b></th>
                                                <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                                                <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                                                <th data-hide="phone,tablet"><b>City </b></th>
                                                <th data-hide="phone,tablet"><b>Source</b></th>
                                                <th data-hide="phone,tablet" ><b>Service Type</b></th>
                                                <?php


                     if($status1=="All")
                     {
                    ?>

                    <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <th data-hide="phone,tablet" ><b>Follow Up Date</b></th>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <th data-hide="phone,tablet" ><b>Drop Reason</b></th>
                    <?php
                  }
                    ?>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
                                    <td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name;?>">{{$lead->id}}</a></td>
                                    <td>{{$lead->created_at}}</td>
                                    <td  >{{$lead->AssignedTo}}</td>
                                    <td >{{$lead->fName}}</td>
                                    <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
                                    <td >{{$lead->Branch}}</td>
                                    <td >{{$lead->Source}}</td>
                                    <td >{{$lead->ServiceType}}</td>
                                    <?php


                     if($status1=="All")
                     {
                    ?>

                        <td>{{$lead->followupdate}}</td>
                        <td>{{$lead->ServiceStatus}}</td>
                        <td>{{$lead->reason}}</td>
                  <?php
                  }
                  if($status1=="Deferred")
                     {
                    ?>

                      <td>{{$lead->followupdate}}</td>

                <?php
                  }
                  if($status1=="Dropped")
                     {
                    ?>
                    <td>{{$lead->reason}}</td>
                    <?php
                  }
                    ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


      @endif

  @else
          <p> No result Found </p>
  @endif


</div>
