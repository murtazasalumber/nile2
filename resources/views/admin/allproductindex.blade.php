  <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
@extends('layouts.admin')

@section('content')

 <!-- Adding bootstrap cdn for css here -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

  <!-- Adding jquery library here -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Adding bootstrap cdn for javascript here -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- Including the css file here -->
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />

  <!-- Including the css file here by another way -->
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />

  <!-- Including the javascript file here -->
<script src="js/footable.js" type="text/javascript"></script>
<script>
     $.noConflict();
jQuery(document).ready(function($){
$(document).ready(function(){
// $( "<p>Test</p>" ).prependTo( "#linkk" );

  $('.footable').footable();
   $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({ animation:true,  html:true}); 
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {
         
           $("#usersetting").popover('hide');
          
        }else
        {
           $("#usersetting").popover('show');
          
          //console.log(sidenvavv);
        }
    }); 
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();
      var status1=$('#status1').val();
      var name=$('#name').val();


           $.ajax({
        type: "GET",
        url:'allleadpa' ,
         data: {'keyword1' : keyword, 'status1':status1, 'name':name, 'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
             // alert(data);
        }
    });
    });
             $("#filter").change(function(){

              $('#keyword').val("");
             });

});
});
</script>
<style type="text/css">
  .imgg
{

    margin-top: -10px;
    margin-left: -98px;



    max-width: 167px;

}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
    font-family: myFirstFont;
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;
   display: none;
    z-index: 10000;
    position: fixed;
}
body::-webkit-scrollbar
{
  display: none;
}
.footer
{
  display: none;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{

      outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.footable.breakpoint > tbody > tr > td > span.footable-toggle {
    float: right;
    display: inline-block;
    font-family: 'footable';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    -webkit-font-smoothing: antialiased;
    padding-right: 5px;
    font-size: 14px;
    color: #888888;
}

input
{
  border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
input:focus
{
  border-bottom:1px solid #00cccc;
}
select:focus
{
  border-bottom:1px solid #00cccc;
}
select
{
  border-bottom-color:#484e51;
      border-top: 0px;
    width: 100%;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
.al
{
  margin-top: 8%;
}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}
.footer
{
  text-align: center;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  #loo
  {
        margin-top: 6px!important;
    margin-left: 45%!important;
  }
   .al
  {
        margin-top: 19%;
  }
  .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .filter
  {
          margin-top: 14px;
    width: 62%
  }
  .keyword1
  {
        margin-top: 7px;
    width: 63%;
    margin-left: 4px;
  }

  #icons
  {
        margin-left: 26%;
    margin-top: 27px;
  }
}

/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) { 
#loo
  {
        margin-top: 6px!important;
    margin-left: 45%!important;
  }
.al
  {
        margin-top: 19%;
  }
  .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .filter
  {
          margin-top: 14px;
    width: 62%
  }
  .keyword1
  {
        margin-top: 7px;
    width: 63%;
    margin-left: 4px;
  }

  #icons
  {
        margin-left: 26%;
    margin-top: 27px;
  }


 }
 /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  #loo
  {
        margin-top: 6px!important;
    margin-left: 45%!important;
  }
.al
  {
        margin-top: 19%;
  }
  .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .filter
  {
          margin-top: 14px;
    width: 62%
  }
  .keyword1
  {
        margin-top: 7px;
    width: 63%;
    margin-left: 4px;
  }

  #icons
  {
        margin-left: 26%;
    margin-top: 27px;
  }

}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  #loo
  {
        margin-top: 6px!important;
    margin-left: 45%!important;
  }
  .al
  {
        margin-top: 19%;
  }
  .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: 6px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .filter
  {
          margin-top: 14px;
    width: 62%
  }
  .keyword1
  {
        margin-top: 7px;
    width: 63%;
    margin-left: 4px;
  }

  #icons
  {
        margin-left: 26%;
    margin-top: 27px;
  }

}
/*iPad:*/
@media screen and (device-aspect-ratio: 3/4) {

.filter
  {
          margin-top: 14px;
    width: 62%
  }
  .keyword1
  {
        margin-top: 7px;
    width: 63%;
    margin-left: 4px;
  }
  
}
@media (max-width: 1200px)
{
 #second
{
      margin-bottom: 5%;
    margin-left: 29%;
}
#productl
{
   margin-left: 29%;
   margin-bottom: 10%;
}
}
</style>
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\admin\home' title='' style='color:black'>Home</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">
<div class="container">
            <div class="row al">
               <div class="col-sm-12"  >
                  <center> <h2 style="color:#333"> Product Leads</h2></center>
               </div>
            </div>

    </div>
    <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >
<!-- 
                               <option>Leads by</option> -->

                        <option value="leads.id">Lead ID</option>
                        <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="orderid">Order ID</option>
                        <!-- <option value="type">Product Type</option> -->
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="OrderStatus">Status</option>

                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-5" id="icons">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" data-toggle="tooltip" title="Create Service Lead" data-placement="bottom">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;"></a>
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" data-toggle="tooltip" title="Create Product Lead" data-placement="bottom"> <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"></a>



             <?php

if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?>

 <a href="/admindownloadall?status=<?php echo $check;    ?>&download=true"> <img src="/img/download1.png" class="img-responsive" alt="add" id="download1" name="download" style="     margin-left: 18px;   display: -webkit-inline-box;    height: 35px;" data-toggle="tooltip" title="Download" data-placement="bottom"></a>
  <input type="hidden" name="" id="status1" value="<?php echo $check; ?>">




        </div>

    </div>
</div>
<!-- Search Section Ends-->





<!-- <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
          <div class="col-sm-4">
                <div style="    margin-left: -55px;">
                     <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="second">
                                          Create Service Lead
                                        </a>&emsp;

                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;" id="productl">
                                          Create Product Lead
                                        </a>
                </div>
          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="orderid">Order ID</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword' style="    margin-top: -5px;">
               <?php
                if(isset($_GET['status']))
                {
                    $check = $_GET['status'];
                }
                else
                {
                    $check = session()->get('status');
                }
               ?>
                   <input type="hidden"  id="status1" value="<?php echo $check ?>">



              </div>
              <div class="col-sm-2" id="buttonforcreate" style="    padding-left: 16px;
    margin-top: -8px;">

          </div>

          </div>


  </div>

</div> -->
<div id="result">
<table class="table footable" style="font-family: Raleway,sans-serif;margin-top: 4%;">
        <thead>
        <th   style="color: #333;"><b>S.no</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>Order ID</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>Lead ID</b></th>
        	<th  data-hide="phone,tablet" style="color: #333;"><b>Product Type</b></th>
        	<th  data-hide="phone,tablet" style="color: #333;"><b>Created At</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created By</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Assigned To</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Name</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet" style="color: #333;"><b>Status</b></th>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          @foreach ($leads as $lead)
          
            <?php

            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
              <input type="hidden"  id="name" value="{{ Auth::guard('admin')->user()->name }}">



@if($lead->MedName!=NULL)

	


<tr>
	 <td><a href="pharmacyshow?id={{$lead->PharmacyId}}&name=<?php echo $name?>" id="linkk">{{$lead->id}}</a></td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
	<td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>

</tr>
	@endif

@if($lead->SKUid!=NULL || $lead->ProductName!=NULL)

<tr>
    <td><a href="productshow?id={{$lead->Prodid}}&name=<?php echo $name?>" id="linkk">{{$lead->id}}</a></td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

	<td>Product</td>
	<td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif
</tr>

@endif




            <!--

            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
             
           </tr>

         @endforeach
  
    </tbody>
  </table>
<div style="text-align: -webkit-center;"> {{$leads->links()}} </div>
</div>

<div class="footer1" style="   text-align: center; border-top: 1px solid #e7e7e7;background: white; ">

  Copyright © Health Heal - 2017

  <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
</div>
</div>
@endsection

@extends('layouts.footer')
