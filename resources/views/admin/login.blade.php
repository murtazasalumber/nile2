<!-- This is the view for the Login Page. It also has the Health Heal Logo on it -->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style type="text/css">
  body::-webkit-scrollbar
  {
    display: none;
  }
  #alignn
{
      margin-top: 24%;
}

  .btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
.footer
  {
    margin-top: 70px;
  }
.btn:hover
{
  outline: 0;
    background-color: #00C851;
}
  body
  {
    overflow-y: scroll;
    overflow-x: hidden;

  }
  @-webkit-keyframes autofill {
    to {
      color: #666;
      background: transparent;
    }
  }

  input:-webkit-autofill {
    -webkit-animation-name: autofill;
    -webkit-animation-fill-mode: both;
  }


  input:focus
  {
    border-bottom:1px solid #00cccc;
  }

  @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
  }
  #title
  {
    font-family: myFirstFont;
  }


  .panel-body {
    height: 316px;
    padding: 15px;
  }

  .imgg
  {
    padding-bottom: 12px;
    margin-left: -19px;
    max-width: 167px;
    margin-top: -12px;
  }

  input
  {
    border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
  }
  label
  {
    color: #777;
  }


  </style>
  <script type="text/javascript">
    $(document).ready(function(){

$('#message').hide();
inpersonate_hidden_value=document.getElementById("inpersonate_hidden_value");
// alert(inpersonate_hidden_value.value);
value = inpersonate_hidden_value.value;
// alert(value);
if(!value)
{

  $('#message').hide();
    // alert(value);
}
else
{
$('#message').show();
$('#form').hide();
  $("#email").attr("value", "{{$email}}");
  $("#password").attr("value", "{{$password}}");
  $('#btnSignIn').click();
}
 });

  </script>
</head>
<body>
<div id="form">
  <div class="container-fluid">
    <div class="row" style="border-bottom: 1px solid #e7e7e7;">

      <div class="col-sm-12" style="text-align: center;    padding-top: 14px;">

        <!-- Shows the logo on the top -->
        <img class="imgg" src=" /img/healthheal_logo.png ">
      </div>
    </div>

  </div>


  <div class="container" style="    margin-top: 40px;">
    <div class="row">
      <div class="col-sm-3"></div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading" id="title">User Login</div>
          <div class="panel-body">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">

              <!-- As soon as the user clicks on the login button , the 'admin.login' route for show login form will be called -->
              <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login') }}" style="    margin-top: 18px;">
                <!-- A csrf field needs to be passed for session status -->
                {{ csrf_field() }}



                <!-- Showing validation errors along with display of form -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>E-Mail Address</label>
                  <input type="text"  name="email" value="{{ old('email') }}" id="email" required autofocus>

                  <div style="width: 100%;    margin-top: 10px;min-height: 30px; " >
                    @if ($errors->has('email'))

                    <strong style="color: #a94442">{{ $errors->first('email') }}</strong>


                    @endif
                  </div>
                </div>

                <!-- Showing validation errors along with display of form -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-top: -5px">
                  <label>Password</label>
                  <input id="password" type="password" name="password" value="" required>
                  <div style="width: 100%;    margin-top: 10px;min-height: 30px; " >
                    @if ($errors->has('password'))

                    <strong style="color: #a94442">{{ $errors->first('password') }}</strong>


                    @endif
                  </div>
                </div>
                <!--  <div style="height: auto;
                margin-top: -41px;
                margin-left: -12px">
                <div class="box" style="display: -webkit-inline-box;">
                <input style="margin-top: -18px;" type='checkbox'  value='valuable' id="thing" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                <p style="    margin-top: -2px;margin-left: 14px;color:#757575;"> Remember Me</p>
              </div>
            </div> -->

<input type="hidden" name="token" value="{{$token}}" id="inpersonate_hidden_value">
<input type="hidden" name="logged_in_user" value="{{$logged_in_user}}">
            <div style="margin-top: 21px;   margin-left: -15px;   width: 108%;">
              <p style="margin-top: 12px;"> <button type="submit" class="btn btn-default" id="btnSignIn">Login</button>

                <!-- On clicking on the Forgot password link , the below mentioned route should show the Forgot Password form -->
                <a href="{{ route('admin.password.request') }}" style="margin-left: 18px;">Forgot Your Password?</a>
              </p>
            </div>
          </form>
        </div>
        <div class="col-sm-2"> </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-3"></div>
</div>
</div>
<div id="message">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center> <h4 id="alignn"> Please Wait.... You're about to be Redirected !! </h4></center>
                </div>
            </div>
        </div>
  </div>
</body>
</html>
