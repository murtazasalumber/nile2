<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
<script type="text/javascript">

$(document).ready(function(){
    if (screen.width <= 800) {
        $('.footer1').show();
        $('.footer').hide();

        $('#mobile').show();

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
        // mobile
    }else
    {


        $('#mySidenav').show();
        $('#main').show();
    }

    //counter 
      $('.counter-count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
});


</script>
<style type="text/css">

@media screen and (max-width: 1200px) {
    #logoimage
    {
        margin-left: -6%;
        margin-top: -1%;
    }
    #loo {
    text-align: -webkit-center;
}

    #mobile
    {
        margin-top: 19%;
    }
}
#mySidenav,#main
{
    display: none;
}

#mobile
{
    display: none;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}

.footer {
        top: -22px;
    position: relative;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    background-color: #efefef;
    text-align: center;
}
.panel-body
{
    text-align: center;
    height: 147px;
}



.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}
.footer1
{
    display: none;
}
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover, .offcanvas a:focus{

}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

#main {
    transition: margin-left .5s;
    padding: 16px;
}


.custom
{
    float: left;
    height: 100%;
    background: #30a5ff;
    width: 50%;
    padding-top: 10px;
    text-align: -webkit-center;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}





.sidenav2 {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align:center;
}

.sidenav2 a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;

}

.sidenav2 a:hover{
    color: #f1f1f1;
}

.sidenav2 .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
.imgg
{

    margin-top: 1px;
    margin-left: -62px;



    max-width: 167px;

}
p
{
        color: #636b6f
}
a
{
    text-decoration: none !important;
}

.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  margin-top: 112% !important;
    margin-left: -371% !important;
}

</style>

@extends('layouts.app')

@section('content')

<div id="mySidenav" class="sidenav" style="height: 653px;  -webkit-transition:none !important;
  -moz-transition:none !important;
  -o-transition:none !important;
  transition:none !important;           background-color: white">
    <!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
    <ul class="nav" id="side-menu" style="    margin-top: -2px;">


        <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
            <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

            <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
                width: 15px;
                float: left;
                margin-left: -2px;display: none;" id="close"> </a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/cc/create?name={{ Auth::user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/new lead.png" class="img-responsive" alt="add" style="     padding-top: 4px;     margin-top: -7px;
    width: 22px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>



            </li>

            <li style="    height: 55px;  ">
                <a href="/product/create?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="     margin-top: -9px;
    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>
            </li>


                <li style="    height: 55px;   ">
                <a href="/cc?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="      margin-top: -9px;
    width: 22px;padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>
            </li>

            <li style="    height: 55px;  ;  ">
                <a href="/product?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="      margin-top: -9px;
    width: 22px;padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Product Orders</p></a>
            </li>


 <li style="    height: 55px;  ;  ">
                <a href="/provisionalview?name={{Auth::guard('admin')->user()->name}}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -74px;"> <img src="/img/provisional.png" class="img-responsive" alt="add" style="      margin-top: -9px;
    width: 22px;padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Provisional Leads</p></a>
            </li>

        </ul>
    </div>





    <div id="main" style="margin-top: 2%;background: #eee; min-height: 550px;">


        <div class="container-fluid" style="    ">
            <div class="row">



                <!--  <div class="col-sm-10">


                <p>  <h3 style="color: #33cc33; margin-top: -10px;   display: inline-block;margin-right: 12px;">
                Welcome</h3>You are logged in as Coordinator!</p>
            </div> -->


            <div class="col-sm-10 col-sm-offset-1" style="margin-top:15px;    ">
                <div class="panel panel-default" style="    margin-top: 7px;margin-right: 6px;
    margin-left: -4px;
    height: 180px;">

                    <div class="panel-body">
                        <h3 style="color:  #33cc33;    margin-top: 36px;"> Welcome !!!</h3>
                        <p> You are logged in as Customer Care. </p>

                    </div>
                </div>
            </div>

            <div class="col-sm-12">
            </div>


            <div class="col-sm-2 " style="margin-top: 10px;    margin-left: 84px;">
                <div class="panel panel-default">
                    <a href="/provisionalview?name={{Auth::guard('admin')->user()->name}}">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/provisional.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> Provisional Leads </h5>
                            <h3 class="counter-count" style="color:#337ab7; margin-top: -8px;">{{$provisionalcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>



<div class="col-sm-2" style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 class="counter-count" style="color: orange; margin-top: -8px;"> {{$newcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-sm-2" style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                            <h3 class="counter-count" style="color: orange; margin-top: -8px;"> {{$inprogresscount}} </h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-2" style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px; color: #636b6f">  Converted Leads </h5>
                            <h3 class="counter-count" style=" color:#33cc33;margin-top: -8px;"> {{$convertedcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>




            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                            <h3 class="counter-count" style=" color:#990000;margin-top: -8px;"> {{$deferredcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-2 " style="margin-top: 10px; margin-left: 84px;">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                            <h3 class="counter-count" style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>

















        </div>
        <!-- Welcome Message starts -->
    </div>



</div>



<script>

document.getElementById("mySidenav").style.width = "230px";
document.getElementById("main").style.marginLeft = "200px";
$("#close").show();

function openNav() {
    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $(".pa").show();

    $("#open").hide();
    $("#close").show();


}

function closeNav() {
    $("#open").show();
    $("#close").hide();
    document.getElementById("mySidenav").style.width = "50";
    document.getElementById("main").style.marginLeft= "50";
    $(".pa").hide();
}
</script>
<!-- Footer  -->
<style type="text/css">
    .popover.bottom>.arrow
    {
        display: none;
    }
.popover {
   top: -2% !important;
    left: 4% !important;
}
.navbar-toggle
{
    display: none;
}
</style>


</div>
</div>





@extends('layouts.footer')
@endsection



</div>




<div id="mobile" >
<style type="text/css">
    .userdashbord
    {
            margin-top: -57px;
    z-index: 100000000;
    outline: none;
    background: white;
    border: none;
    position: fixed;
    margin-left: -35px;
    }
    .sidemenu1
    {
            margin-top: -54px;
    margin-left: 2px;
    font-size: 30px;
    cursor: pointer;
    position: fixed;
    z-index: 10000;
    }
    .ipad
    {
        display: none;
    }
  
    /*Media Query starts*/

/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px)  and (orientation: portrait)
{

#logoimage
{
        margin-left: -38%;
}
.navbar2
{
    z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -48px;
}
.sidemenu1
{
    margin-top: -47px;
}
}
@media screen and (device-width: 640px) and (device-height:360px)  and (orientation: landscape)
{
    .sidemenu1
    {
            margin-top: -103px;
    }
    .navbar2
    {   
            z-index: 100000000;
            position: fixed;
            margin-left: 88%;
            margin-top: -101px;
    }

    
}

/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) and (orientation: portrait){ 
    #logoimage
    {
        margin-left: -38%;
    }
    .navbar2
    {
        z-index: 100000000;
        position: fixed;
        margin-left: 83%;
        margin-top: -57px;
    }
    .sidemenu1
    {
        margin-top: -57px;
    }
}
@media screen and (device-width: 732px) and (device-height:412px)  and (orientation: landscape)
{
    .sidemenu1
    {
            margin-top: -115px;
    }
    .navbar2
    {   
            z-index: 100000000;
            position: fixed;
            margin-left: 88%;
            margin-top: -115px;
    }

    
}
/*Query for iphone 5*/
@media (min-device-width: 320px) and (max-device-height: 568px) and (orientation: portrait){ 
    #logoimage
    {
        margin-left: -38%;
    }
    .navbar2
    {
        z-index: 100000000;
        position: fixed;
        margin-left: 83%;
        margin-top: -48px;
    }
    .sidemenu1
    {
        margin-top: -47px;
    }
    .welcomemessage
    {
        margin-top: 27%!important;
    }
}
@media (min-device-width: 568px) and (max-device-height: 320px) and (orientation: landscape){ 
    .sidemenu1
    {
            margin-top: -86px;
    }
    .navbar2
    {   
            z-index: 100000000;
            position: fixed;
            margin-left: 88%;
            margin-top: -86px;
    }


}
/*Query for iphone 6*/
@media (min-device-width: 375px) and (max-device-height: 667px) and (orientation: portrait){ 
    #logoimage
    {
        margin-left: -38%;
    }
    .navbar2
    {
        z-index: 100000000;
        position: fixed;
        margin-left: 83%;
        margin-top: -57px;
    }
    .sidemenu1
    {
        margin-top: -57px;
    }
}
@media (min-device-width: 667px) and (max-device-height: 375px) and (orientation: landscape){ 
.sidemenu1
    {
            margin-top: -107px;
    }
    .navbar2
    {   
            z-index: 100000000;
            position: fixed;
            margin-left: 88%;
            margin-top: -110px;
    }
}
/*Iphone 6+*/
@media (min-device-width:736px) and (max-device-height: 414px) and (orientation: landscape){ 
.sidemenu1
    {
            margin-top: -107px;
    }
    .navbar2
    {   
            z-index: 100000000;
            position: fixed;
            margin-left: 88%;
            margin-top: -110px;
    }
}
/*Iphone X*/
@media (min-device-width: 375px) and (max-device-height: 812px) and (orientation: portrait){ 
     #logoimage
    {
        margin-left: -38%;
    }
    .navbar2
    {
        z-index: 100000000;
        position: fixed;
        margin-left: 83%;
        margin-top: -57px;
    }
    .sidemenu1
    {
        margin-top: -57px;
    }
}
/*Ipad*/
@media (min-device-width: 768px) and (max-device-height: 1024px) and (orientation: portrait){ 
    #logoimage
    {
        margin-left: 180px;
    margin-top: 5px;
    }
    .navbar2
    {
        display: none;
    }
    .sidemenu1
    {
        display: block;
            margin-top: -116px;
    }
    .footer1
    {
        margin-top: 41%!important;
        position: relative;

    }
    .ipad,.ipadp
    {
        width: 225%;
    }
    .ipad1
    {
        width: 225%;
        margin-left: 134%;
    }
    .ipad2
    {    width: 225%;
        margin-left: 264%;
    }
    .ipad3
    {    width: 225%;
        margin-left: 264%;
    }
    .ipad
    {
        display: block;
    }
     .ipad4
    {    width: 225%;
       margin-left: -133%;
    }
     .ipad5
    {    width: 225%;
       margin-left: 260%;
    }
     .ipad6
    {    width: 225%;
       margin-left: -1%;
    }
}
/*Nexus 7 (Tab)*/
@media (min-device-width: 600px) and (max-device-height: 960px) and (orientation: portrait){ 

    .navbar2
    {
        z-index: 100000000;
        position: fixed;
        margin-left: 83%;
        margin-top: -91px;
    }
    .sidemenu1
    {
        margin-top: -94px;
    }
   

}
@media (min-device-width: 960px) and (max-device-height: 600px) and (orientation: landscape){ 
    .sidemenu1
    {
            margin-top: -151px;
    }
    .navbar2
    {   
            display: none;
    }
    .tabb
    {
            margin-left: -152px;
    }
    .tabbb
    {
            margin-left: 10%;
    }
     #logoimage
    {
        margin-left: 93%;
    }
    #welcome1
    {
            margin-top: -78px;
    }
    .footer1
    {
        margin-top: 113px!important;
    }
}
</style>
    <div class="col-sm-12" ">




        <div id="mySidenav2" class="sidenav2" style="margin-top: 20%;background: white;">


        </div>

      <a href="#" data-toggle="popover" data-trigger="focus" title="" data-placement="bottom" data-content="
<div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/cc/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
  </div>
  <br>
  <div style='width: 121%;
      margin-top: -12px;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
</div>
<br>
 <div style='width: 121%;
     margin-top: -12px;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<a href='/cc?name={{ Auth::user()->name }}' title='' style='color:black'>View Service Leads</a>
</div>
<br>
 <div style='width: 121%;
     margin-top: -12px;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<a href='/product?name={{ Auth::user()->name }}' title='' style='color:black'>View Product Leads</a>
</div>
<br>
 <div style='width: 121%;
     margin-top: -12px;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<a href='/provisionalview?name=Padmashree' title='' style='color:black'>View Provisional Lead</a>
</div>
  "><span class="sidemenu1" id="sidemenu"><img class="img-responsive" src="/img/clipboard.png" alt="User" style=""> </span></a>




<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\change-password' title='' style='color:black'>User Settings</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" > </a>
</div>
    
        <div class="col-sm-12" id="welcome1">

            <div class="panel panel-default welcomemessage">
             
                <div class="panel-body ">
                    <h3 style="color: #33cc33;">  Welcome !!! </h3>
                    <p>  You are logged in as customer Care. </p>
           
                </div>
            </div>

        </div>

       <!--  <script>
        function openNav2() {
            document.getElementById("mySidenav2").style.width = "100%";
        }

        function closeNav2() {
            document.getElementById("mySidenav2").style.width = "0";
        }
        </script> -->
      <!--  <div class="col-sm-2 " style="margin-top: 10px;    ">
                <div class="panel panel-default">
                    <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/New Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">{{$newcount}} </h3>
                        </div>
                    </a>
                </div>
            </div> -->

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({html:true});


});
</script>


       <div class="col-sm-2 tabb" style="margin-top: 10px;    ">
                <div class="panel panel-default ipadp">
                    
                        <div class="panel-body" style="    text-align: -webkit-center;">
                       
                            <img src="/img/provisional.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                             <a href="/provisionalview?name={{Auth::guard('admin')->user()->name}}">
                            <h5 style="padding-top: 10px;color: #636b6f"> Provisional Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">{{$provisionalcount}} </h3>  </a>
                        </div>
                  
                </div>
            </div>

        

        <div class="col-sm-2 tabbb" style="margin-top: 10px;">
                <div class="panel panel-default ipad1">
                    
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                            <h5 style="padding-top: 10px;color: #636b6f">  New Leads </h5>
                            <h3 style="color: orange; margin-top: -8px;"> {{$newcount}} </h3>
                              </a>
                        </div>
                  
                </div>
            </div>

              

            <div class="col-sm-2" style="margin-top: 10px;">
                <div class="panel panel-default ipad2">
                   
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                             <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                            <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                            <h3 style="color: orange; margin-top: -8px;"> {{$inprogresscount}} </h3>
                             </a>
                        </div>
                   
                </div>
            </div>
            <div class="col-sm-12 ipad"> </div>
            <div class="col-sm-2" style="margin-top: 10px;">
                <div class="panel panel-default ipad3">
                    
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
                            <h5 style="padding-top: 10px; color: #636b6f">  Converted Leads </h5>
                            <h3 style=" color:#33cc33;margin-top: -8px;"> {{$convertedcount}} </h3>
                             </a>
                        </div>
                   
                </div>
            </div>




            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default ipad4">
                    
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                            <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                            <h3 style=" color:#990000;margin-top: -8px;"> {{$deferredcount}} </h3>
                             </a>
                        </div>
                   
                </div>
            </div>

            <div class="col-sm-2 " style="margin-top: 10px;">
                <div class="panel panel-default ipad5">
                    
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                            <a href="/ccassign?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                            <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                            <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcount}} </h3>
                            </a>
                        </div>
                    
                </div>
            </div>








    </div>



    <div class="col-sm-12">
        <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

            Copyright © Health Heal - 2017

            <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
        </div>
    </div>



</div>
