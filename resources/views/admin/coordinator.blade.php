<!-- This is dashboard for Coordinator -->
<meta name="theme-color" content="#999999" />
    <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    if (screen.width <= 800) {
        $('.footer1').show();
        $('.footer').hide();
        $('#mobile').show();

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
        // mobile
    }else
    {


        $('#mySidenav').show();
        $('#main').show();
         $('#mobile').hide();
    }
     $("#filter").click(function(){

                $('#myModal').modal('show');



        });

    $("#search").click(function(){



        var branch =  $('#Branch').val();
        var vertical=   $('#Vertical').val();
        var from=  $('#FromDate').val();
        var to= $('#ToDate').val();

        if(from == 0 && to == 0)
        // alert("branch and vertical");
        {
            alert("Nothing Seleted");
        }
        else
        {

            $.get("{{ URL::to('searchfiltercoordinator') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to } ,function(data){
                $('#main').html(data);
            });

        }

    });
    $('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

});


</script>
<style type="text/css">
@media screen and (max-width: 380px) {
    #mobile
    {
        margin-top: 20%;
    }
    .navbar-header {
        padding-left: 82px;
    }
}
@media screen and (max-width: 412px) {


}
@media screen and (max-width: 1200px) {


    #mobile
    {
        margin-top: 19%;
    }
    #loo {
        text-align: -webkit-center;
    }
}

#mySidenav,#main
{
    display: none;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -62px;
}
#mobile
{
    display: none;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.btn
{
    color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
    outline: 0;
    background-color: #00C851;
    opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
    background: white;
}

.footer {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    background-color: #efefef;
    text-align: center;
}
.panel-body
{
    text-align: center;
    height: 147px;
}



.sidenav {

    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}
.footer1
{
    display: none;
}
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover, .offcanvas a:focus{

}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

#main {
    transition: margin-left .5s;
    padding: 16px;
}


.custom
{
    float: left;
    height: 100%;
    background: #30a5ff;
    width: 50%;
    padding-top: 10px;
    text-align: -webkit-center;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}


</style>
<style>


.sidenav2 {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align:center;
}

.sidenav2 a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;

}

.sidenav2 a:hover{
    color: #f1f1f1;
}

.sidenav2 .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
a
{
    text-decoration: none !important;
}

@media screen and (max-height: 450px) {
    .sidenav2 {padding-top: 15px;}
    .sidenav2 a {font-size: 18px;}
}
</style>

@extends('layouts.app')

@section('content')

<div id="mySidenav" class="sidenav" style="height: 653px;    -webkit-transition:none !important;
-moz-transition:none !important;
-o-transition:none !important;
transition:none !important;          background-color: white">
<!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
<ul class="nav" id="side-menu" style="    margin-top: -2px;">


    <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
        <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

        <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
            width: 15px;
            float: left;
            margin-left: -2px;display: none;" id="close"> </a>

        </li>

        <li style="    height: 55px;    ">
            <a href="/cc/create?name={{ Auth::user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="     padding-top: 4px; margin-top: -7px;
                width: 22px;       margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>



            </li>

            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="  margin-top: -7px; padding-top: 4px; width: 22px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>
            </li>

             <li style="    height: 55px;    ">
                        <a href="/vh?name={{ Auth::guard('admin')->user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



                        </li>

           <li style="    height: 55px;    ">
                        <a href="/product?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Order</p></a>



                        </li>



            </ul>
        </div>





        <div id="main" style="margin-top: 6%;background: #eee; min-height: 550px;">


            <div class="container-fluid" style="    margin-top: 5px;">
                <div class="row">

                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:15px;    ">
                        <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;
                        height: 180px;">

                        <div class="panel-body">
                            <h3 style="color:  #33cc33;    margin-top: 36px;"> Welcome !!!</h3>
                            <p> You are logged in as Coordinator. </p>

                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                </div>


                <div class="col-sm-2 col-sm-offset-2" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=In Progress">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                <h3 style="color:#337ab7 ;margin-top: -8px;"> {{$assignedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f"> Converted Leads  </h5>
                                <h3 style=" margin-top: -8px;color: #33cc33">  {{$convertedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                <h3 style=" color:#990000;margin-top: -8px;">{{$deferredcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color: #ff1a1a;margin-top: -8px;"> {{$droppedcount}} </h3>
                            </div>
                        </a>
                    </div>
                </div>










            </div>
            <!-- Welcome Message starts -->
        </div>



    </div>



    <script>

    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $("#close").show();

    function openNav() {
        document.getElementById("mySidenav").style.width = "230px";
        document.getElementById("main").style.marginLeft = "200px";
        $(".pa").show();

        $("#open").hide();
        $("#close").show();


    }

    function closeNav() {
        $("#open").show();
        $("#close").hide();
        document.getElementById("mySidenav").style.width = "50";
        document.getElementById("main").style.marginLeft= "50";
        $(".pa").hide();
    }
    </script>
    <!-- Footer  -->


</div>
</div>





@extends('layouts.footer')
@endsection



</div>

<style type="text/css">
.popover.bottom>.arrow,.popover.left>.arrow
{
    display: none;
}
.popover {
    top: 60px !important;
    left: 4% !important;
}
.navbar-toggle
{
    display: none;
}
.popover.bottom {
    margin-top: 30px;
}
.navbar2
{
    display: none;
}
.popover.left
  {
    top:39px!important;
    width: 408% !important;
    left: -180%!important;
  }

  #logoimage
    {
            margin-left: -138px!important   ;
    }
    .safarii,.sidebarr
    {
        display: none;
    }
 /*Media Query starts*/

/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px)  and (orientation: portrait)
{
    .normalsidebar
    {
        display: block;
    }
    .sidebarr
    {
        display: none;
    }

      .safarii
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    .navbar2
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -48px;
    }
    .sidemenuimage
    {
            width: 119%!important;
    margin-top: 4px;
    }
}
@media screen and (device-width: 640px) and (device-height:360px)  and (orientation: landscape)
{
     .normalsidebar
    {
        display: block;
    }
    .sidebarr
    {
        display: none;
    }
      .safarii
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    .navbar2
    {
            display: block;
    z-index: 100000000;
    position: fixed;
    margin-left: 90%;
    margin-top: -95px;
    }
    .sidemenuimage
    {
                width: 119%!important;
    margin-top: -45px;
    }
    .Welcomemessage
    {
            margin-top: -52px;

    }
}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) and (orientation: portrait){ 
     .normalsidebar
    {
        display: block;
    }
    .sidebarr
    {
        display: none;
    }
      .safarii
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    .navbar2
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -54px;
    }
    .sidemenuimage
    {
            width: 119%!important;
    margin-top: 0px;
    }

}
@media screen and (device-width: 732px) and (device-height:412px)  and (orientation: landscape)
{
     .normalsidebar
    {
        display: block;
    }
    .sidebarr
    {
        display: none;
    }
      .safarii
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
     .navbar2
    {
            display: block;
    z-index: 100000000;
    position: fixed;
    margin-left: 90%;
    margin-top: -107px;
    }
    .sidemenuimage
    {
                width: 119%!important;
    margin-top: -55px;
    }
    .Welcomemessage
    {
            margin-top: -52px;

    }
}
/*Query for iphone 5*/
@media (min-device-width: 320px) and (max-device-height: 568px) and (orientation: portrait){ 
    .navbar2
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
      .safarii
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    .normalsidebar
    {
        display: none;
    }
    .sidebarr
    {
        display: block;
    }
    .sidemenuimage
    {
                margin-left: 13px;
    width: 79%!important;
    margin-top: 13px
    }
    #loo
    {
        margin-left: 33px;
    }
}
@media (min-device-width: 568px) and (max-device-height: 320px) and (orientation: landscape){ 
    .navbar2
    {
            display: none;
    z-index: 100000000;
    position: fixed;
    margin-left: 90%;
    margin-top: -82px;
    }
      .safarii
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    .normalsidebar
    {
        display: none;
    }
     .sidebarr
    {
        display: block;
    }
    .sidemenuimage
    {
                width: 119%!important;
    margin-top: -28px;
    }
    .Welcomemessage
    {
            margin-top: -42px;

    }

}
/*Query for iphone 6*/
@media (min-device-width: 375px) and (max-device-height: 667px) and (orientation: portrait){ 
     .navbar2
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -46px;
    }
    .normalsidebar
    {
        display: none;
    }
     .sidebarr
    {
        display: block;
    }
    .sidemenuimage
    {
                margin-left: 13px;
    width: 79%!important;
    margin-top: 0px
    }
      .safarii
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -46px;
    }
}

@media (min-device-width: 667px) and (max-device-height: 375px) and (orientation: landscape){
         .navbar2
    {
            display: none;
    z-index: 100000000;
    position: fixed;
    margin-left: 90%;
    margin-top: -99px;
    }
    .normalsidebar
    {
        display: none;
    }
     .sidebarr
    {
        display: block;
    }
      .safarii
    {
        display: block;
    z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -99px;
    }
    .sidemenuimage
    {
                    width: 119%!important;
    margin-top: -47px;
    }
    .Welcomemessage
    {
            margin-top: -42px;

    }
}
/*Iphone 6+*/
@media (min-device-width:736px) and (max-device-height: 414px) and (orientation: landscape){ 
    .normalsidebar
    {
        display: none;
    }
     .sidebarr
    {
        display: block;
    }
     .navbar2
    {
            display: none;
    z-index: 100000000;
    position: fixed;
    margin-left: 90%;
    margin-top: -111px;
    }
      .safarii
    {
       display: block;
    z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -111px;
    }
    .sidemenuimage
    {
                    width: 119%!important;
    margin-top: -60px;
    }
    .Welcomemessage
    {
            margin-top: -42px;

    }
}
/*Iphone X*/
@media (min-device-width: 375px) and (max-device-height: 812px) and (orientation: portrait){ 
    .normalsidebar
    {
        display: none;
    }
     .sidebarr
    {
        display: block;
    }
      .navbar2
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -46px;
    }
      .safarii
    {
       display: block;
    z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -48px;
    }
    .sidemenuimage
    {
                margin-left: 13px;
    width: 79%!important;
    margin-top: 0px
    }
}
/*Ipad*/
@media (min-device-width: 768px) and (max-device-height: 1024px) and (orientation: portrait){
    .normalsidebar
    {
        display: none;
    }
  .safarii
    {
        display: none;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -36px;
    }
    #loo,#logoimage
    {
            
    
    
               margin-left: 1055%;
    }
    .sidemenuimage
    {
            margin-top: -63px;
    }
    .Welcomemessage
    {
        width: 593%!important;
        margin-left: 89%!important;
            margin-top: -53px;
    }
    .leads
    {
            margin-top: -30px!important;
    }
    .IPL
    {
            margin-top: 171%;
            width: 125%!important;
    }
    .CL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 7px;
    }
    .DL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 13px;
    }
    .Drl
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 23px;
    }
    .IPLC
    {
            margin-left: -2%!important;
    }
    .footer1
    {
            margin-top: 382px!important;
    }
}
/*Ipad*/
@media (min-device-width: 1024px) and (max-device-height: 768px) and (orientation: landscape){
    
       #loo
    {
            margin-left: 289%;
    }
    .sidemenuimage
    {
            margin-top: -113px;
    }
    .Welcomemessage
    {
        width: 593%!important;
        margin-left: 89%!important;
            margin-top: -98px;
    }
    .leads
    {
            margin-top: -152px!important;
    }
    .IPL
    {
            margin-top: 171%;
            width: 125%!important;
    }
    .CL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 23px;
    }
    .DL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 44px;
    }
    .Drl
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 82px;
    }
    .IPLC
    {
            margin-left: -2%!important;
    }
    .footer1
    {
            margin-top: 282px!important;
    }
}
/*Nexus 7 (Tab)*/
@media (min-device-width: 600px) and (max-device-height: 960px) and (orientation: portrait){ 

      .navbar2
    {
        display: block;
        z-index: 100000000;
    position: fixed;
    margin-left: 83%;
    margin-top: -86px;
    }
    .sidemenuimage
    {
                margin-left: 13px;
    width: 79%!important;
    margin-top: -36px
    }
     .Welcomemessage
    {
            margin-top: -40px;
    }
}
@media (min-device-width: 960px) and (max-device-height: 600px) and (orientation: landscape){ 
      #loo
    {
            margin-left: 188%;
    }
    .sidemenuimage
    {
               margin-top: -101px;
    margin-left: 44px;
    }
    .Welcomemessage
    {
            width: 607%!important;
    margin-left: 37%!important;
    margin-top: -98px;
    }
    .leads
    {
                margin-top: -135px!important;
    }
    .IPL
    {
            margin-top: 171%;
            width: 125%!important;
    }
    .CL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 23px;
    }
    .DL
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 44px;
    }
    .Drl
    {
         margin-top: 171%;
            width: 125%!important;
                margin-left: 82px;
    }
    .IPLC
    {
                margin-left: -8%!important;
    }
    .footer1
    {
            margin-top: 112px!important;
    }

}
</style>


<div id="mobile" >
    <!--  <span style="    margin-left: 10px;
    margin-top: -16%;
    font-size: 30px;
    cursor: pointer;
    position: fixed;
    z-index: 10000;" onclick="openNav2()" >&#9776; </span> -->
    <a href="#" data-toggle="popover" class="normalsidebar" title="" data-placement="bottom" data-trigger="focus" data-content="
    <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
    border-bottom: 1px solid #e9ebee;'>
    <a href='/cc/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/vh?name={{ Auth::user()->name }}' title='' style='color:black'>View Leads</a>
</div>

"><span style="     margin-top: -50px;
margin-left: 13px;
font-size: 30px;
cursor: pointer;
position: fixed;
z-index: 10000;"  id="sidemenu"><img class="img-responsive sidemenuimage" src="/img/clipboard.png" alt="User" style="">  </span></a>




<a href="#" data-toggle="popover" class="sidebarr" title="" data-placement="bottom"  data-content="
    <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
    border-bottom: 1px solid #e9ebee;'>
    <a href='/cc/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
</div>
<br>
<div style='width: 121%;
margin-top: -12px;
text-align: -webkit-center;
padding-bottom: 6px;
margin-left: -15px;
border-bottom: 1px solid #e9ebee;'>
<a href='/vh?name={{ Auth::user()->name }}' title='' style='color:black'>View Leads</a>
</div>

"><span style="     margin-top: -50px;
margin-left: 13px;
font-size: 30px;
cursor: pointer;
position: fixed;
z-index: 10000;"  id="sidemenu"><img class="img-responsive sidemenuimage" src="/img/clipboard.png" alt="User" style="">  </span></a>


<div class="col-sm-12" style="    text-align: -webkit-right;">
    
    <div id="demo" class="collapse" >
        <div style="border-bottom: 1px solid #e9ebee;
        padding-top: 8px;
        margin-left: -29px;
        background: white;
        padding-right: 43px;
        width: 117%;
        text-align: right;">
        {{ Auth::guard('admin')->user()->name }}
    </div>

    <div style="border-bottom: 1px solid #e9ebee;
    padding-top: 8px;
    margin-left: -29px;
    background: white;
    margin-bottom: 13px;
    width: 117%;text-align: right;">
    <a href="\change-password" style="    padding-right: 18px;">User Settings</a>
    <a href="{{ route('logout') }}" style="    padding-right: 18px;"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"><br>
    Logout
</a>
</div>

</div>
    
</div>
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\change-password' title='' style='color:black'>User Settings</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" > </a>
</div>
<div class="navbar2 safarii">
 <a href="javascript:void(0);" class="saffrii" data-toggle="popover" title="" id="usersetting"  data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\change-password' title='' style='color:black'>User Settings</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>


                                      

  </div>">
   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" > </a>
</div>
<div id="bodyyyyy">
<div class="col-sm-2 " style="margin-top: 10px;    ">
    <div class="panel panel-default Welcomemessage">
   
            <div class="panel-body" style="    text-align: -webkit-center;">
                <h3 style="color: #33cc33;">
                    Welcome !!!</h3><p>You are logged in as Coordinator.</p>
                </div>
         
        </div>
    </div>

    <div id="mySidenav2" class="sidenav2" style="margin-top: 20%;background: white;">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">&times;</a>
        <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Service Lead</a>
        <a href="/product/create?name={{ Auth::user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Product Lead</a>

        <a href="/vh?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">View Leads</a>

    </div>

    <!--
    <script>
    function openNav2() {
    document.getElementById("mySidenav2").style.width = "100%";
}

function closeNav2() {
document.getElementById("mySidenav2").style.width = "0";
}
</script> -->
<div class="col-sm-2 col-sm-offset-2 IPLC leads" style="margin-top: 10px;">
    <div class="panel panel-default IPL">
        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=In Progress">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                <h3 style="color:#337ab7 ;margin-top: -8px;"> {{$assignedcount}} </h3>
            </div>
        </a>
    </div>
</div>

<div class="col-sm-2 leads" style="margin-top: 10px;">
    <div class="panel panel-default CL">
        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Converted">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f"> Converted Leads  </h5>
                <h3 style=" margin-top: -8px;color: #33cc33">  {{$convertedcount}} </h3>
            </div>
        </a>
    </div>
</div>


<div class="col-sm-2 leads" style="margin-top: 10px;">
    <div class="panel panel-default DL">
        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                <h3 style=" color:#990000;margin-top: -8px;">{{$deferredcount}} </h3>
            </div>
        </a>
    </div>
</div>



<div class="col-sm-2 leads" style="margin-top: 10px;">
    <div class="panel panel-default DrL">
        <a href="/assignedstatus?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
            <div class="panel-body" style="    text-align: -webkit-center;">
                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                <h3 style="color: #ff1a1a;margin-top: -8px;"> {{$droppedcount}} </h3>
            </div>
        </a>
    </div>
</div>







</div>



<div class="col-sm-12">
    <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

        Copyright © Health Heal - 2017

            <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
        </div>

    </div>
</div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({html:true});


});
</script>
