
 
@extends('layouts.app1')

@section('content')
 <!-- Styles -->
    <link href="{{ asset('css/forms.css') }}" rel="stylesheet">
    <style type="text/css">
      @media only screen and (max-width: 1200px) {
    #password,#password-confirm
    {
          margin-left: 34px;
    width: 73%;
    }
    #lable
    {
          margin-left: 32px;
    }
    #sub
    {
          margin-left: 66px;
    }
    .panel-default
    {
          margin-left: 10px;
    }
   
  }
  .footer{
    position: absolute;
  }
.imgg
{
   margin-top: 0px;
    margin-left: -80px;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.footer
{
  margin-top: 2%;
}
@media screen and (max-height: 1200px) {
    #loo
    {
          margin-left: 19%;
    }
    #ss
{
  padding-top: 44px;
  margin-top: 0%;
}
}
    </style>
<div class="container">
  <div class="row" id="ss">
    <div class="col-md-8 col-md-offset-2" style="    " >
      <div class="panel panel-default" style="    width: 93%;
    height: 380px;">

        <!-- Change password view code starts here || by jatin -->
        <div class="panel-heading" style="  text-align: center;  background-color: #f5f5f5;
    border-color: #ddd;
">Change Password </div>
        <div class="panel-body">

          <!-- Depending on the session set in the UpdateController display the appropriate message -->
          @if (Session::has('success'))
          <div class="alert alert-success">{!! Session::get('success') !!}</div>
          @endif
          @if (Session::has('failure'))
          <div class="alert alert-danger">{!! Session::get('failure') !!}</div>
          @endif
          
          <form action="change-password" method="post" role="form" class="form-horizontal">

        
            {{csrf_field()}}
<input type="hidden" name="name" value="{{ Auth::guard('admin')->user()->name }}" >


            <div class="form-group{{ $errors->has('old') ? ' has-error' : '' }}">

                  <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                          <label for="password" id="lable">Old Password</label>
                          <input id="password" type="password"  name="old" >
                           @if ($errors->has('old'))
                            <span class="help-block">
                              <strong>{{ $errors->first('old') }}</strong>
                           </span>
                            @endif
                      </div>
            
                  </div>
              <!-- <label for="password" class="col-md-4 control-label">Old Password</label>

              <div class="col-md-6">
                <input id="password" type="password"  name="old">

                @if ($errors->has('old'))
                <span class="help-block">
                  <strong>{{ $errors->first('old') }}</strong>
                </span>
                @endif
 -->

              </div>
         <br>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                  <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                          <label for="password" id="lable">New Password</label>
                          <input id="password" type="password"  name="password" >
                           @if ($errors->has('password'))
                            <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                          </span>
                          @endif
                      </div>
            
                  </div>
              <!-- <label for="password" class="col-md-4 control-label">New Password</label>

              <div class="col-md-6">
                <input id="password" type="password"  name="password">

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif -->
              </div>
            </div>
<br>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

               <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                          <label for="password-confirm" id="lable">Confirm Password</label>
                          <input id="password-confirm" type="password"  name="password_confirmation" >
                           @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                          </span>
                          @endif
                      </div>
            
                  </div>
              <!-- <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

              <div class="col-md-6">
                <input id="password-confirm" type="password"  name="password_confirmation">

                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
              </div> -->
            </div>
<br>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-5">
                <button type="submit" class="btn btn-default " id="sub">Submit</button>
              </div>
            </div>
          </form>
        </div>

        <!-- Change password view code ends here || by jatin -->

      </div>
    </div>
  </div>
</div>

@endsection
