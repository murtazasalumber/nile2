@extends('layouts.app')

@section('content')
<style type="text/css">
    input
{
  border-right: 0px;  
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px; 
    border-bottom:1px solid #484e51;
    padding-bottom: 0px; 
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
@media only screen and (max-width: 1200px) {
    #email
    {
            margin-left: 26px;
    width: 82%;
    }
    #lable
    {
            margin-left: 22px;
    }
   .btn-default
   {
        margin-left: 30px;
   }
  }
  .footer{
    margin-top: 14%;
    padding-top: 2%;
   text-align: center;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 15%;">
            <div class="panel panel-default" >
                <div class="panel-heading" style="    background-color: #f5f5f5;
    border-color: #ddd;
">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <!--   <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                         <!--    <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif -->

                                    <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                          <label for="email" id="lable">E-Mail Address</label>
                          <input id="email" type="email"  name="email" value="{{ old('email') }}" required >
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif 
                    <!--   </div> -->
            
                  </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-default">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
