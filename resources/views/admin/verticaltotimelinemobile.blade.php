<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['{{$vertical}}', 'New Lead', 'In Progress', 'Converted', 'Deferred', 'Dropped'],
            ['Beng', {{$newcountforbng}},{{$inprogresscountforbng}},{{$convertedcountforbng}},{{$deferredcountforbng}},{{$droppedcountforbng}}],
            ['Chen', {{$newcountforchen}},{{$inprogresscountforchen}},{{$convertedcountforchen}},{{$deferredcountforchen}},{{$droppedcountforchen}}],
            ['Hyd', {{$newcountforhyd}},{{$inprogresscountforhyd}},{{$convertedcountforhyd}},{{$deferredcountforhyd}},{{$droppedcountforhyd}}],
            ['Pun', {{$newcountforpune}},{{$inprogresscountforpune}},{{$convertedcountforpune}},{{$deferredcountforpune}},{{$droppedcountforpune}}],
            ['Hu-Dh', {{$newcountforhub}},{{$inprogresscountforhub}},{{$convertedcountforhub}},{{$deferredcountforhub}},{{$droppedcountforhub}}]
        ]);

        var options = {
            chart: {
                title: '',
                subtitle: '',
            },
            colors: ['#337ab7', 'orange', '#33cc33','#990000','#ff1a1a'],
            width: 280,
            height: 200,
            legend : { position:"none"}
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
    </script>
<div class="contianer">
		<div class="row">
					 <div class="col-sm-12 " style="margin-top: 10px;">
                <div class="panel panel-default">

                    @if($designation=="Management")
                    <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                        @else
                        <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                            @endif                        <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                                <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                            </div>

                        </a>
                    </div>

                </div>

                <div class="col-sm-12" >
                    <div class="panel panel-default">
                        @if($designation=="Management")
                        <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                            @else
                            <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @endif                              <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{$inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif
                                    <div class="panel-body" style="    text-align: -webkit-center;">
                                        <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                        <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                        <h3 style="color:#33cc33; margin-top: -8px;"> {{$convertedcountforall}} </h3>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-12 " >
                            <div class="panel panel-default">
                                @if($designation=="Management")
                                <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @else
                                    <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                        @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                            <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                            <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                            <h3 style="color:#990000; margin-top: -8px;"> {{$deferredcountforall}} </h3>
                                        </div>
                                    </a>
                                </div>
                            </div>



                            <div class="col-sm-12" >
                                <div class="panel panel-default" style="    width: 101%;
                                margin-left: -5px;">
                                @if($designation=="Management")
                                <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                    @else
                                    <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical={{$vertical}}&from={{$Fromdate}}&to={{$Todate}}">
                                        @endif                             <div class="panel-body" style="    text-align: -webkit-center;">
                                            <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                            <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                            <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}}
                                            </h3>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            	<div class="col-sm-12">
                            			<div class="panel panel-default" style="margin-right: 6px;
                                margin-left: -4px;text-align:center;">

                                <div class="panel-body" style="height: auto;">
                                    <h3> {{$vertical}} Report from to </h3>
                                    <br><br>
                                    <div id="columnchart_material" style="padding-left: 25px;" ></div>
                                    <br>
                                    	<div class="col-sm-12">
                			<div style="width: 12px; height: 12px;border: solid 7px #337ab7;float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;"> New</p>
                		</div>
                		<div class="col-sm-12">
                		
                         <div style="width: 12px; height: 12px;border: solid 7px orange; margin-left: 20%;
;float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left; ">  In Progress</p>
                		</div>
                		<div class="col-sm-12"></div>
                		<div class="col-sm-12"></div>
                		<br><br>
                		<div class="col-sm-12" style="margin-top: -13px;">
                		
                            <div style="width: 12px; height: 12px;border: solid 7px #33cc33;margin-left: 0%; float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;">Converted</p>
                		</div>
                		<div class="col-sm-12">
                		
                            <div style="width: 12px; height: 12px;border: solid 7px #990000;float: left; margin-left: 5%;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Deferred</p>
                		</div>
                		<div class="col-sm-12"></div>
                		<div class="col-sm-12"></div>
                		<br><br>
                		<div class="col-sm-12" style="margin-top: -13px;">
                					<div style="width: 12px; height: 12px;border: solid 7px #ff1a1a;float: left;">
                        </div>

                        <p style="    padding-left: 7px;margin-top: -3px; float: left;">Dropped</p>
                		</div>

                                    </div>
                                    </div>

                            	</div>
		</div>

</div>