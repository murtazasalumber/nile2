



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
      <script type="text/javascript">
$.noConflict();
jQuery( document ).ready(function( $ ) {
    // Code that uses jQuery's $ can follow here.

    // This function is helping display the "dates" in the graph filters
    $('#FromDate').datepicker({
        format: "yyyy-mm-dd"
    });
    $('#ToDate').datepicker({
        format: "yyyy-mm-dd"
    });

    // On change the dates will be hidden
    $('#FromDate').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    $('#ToDate').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){

                        $("#filter").click(function(){

                $('#myModal').modal('show');
                     });


$("#search").click(function(){
               var branch =  $('#Branch').val();
          var vertical=   $('#Vertical').val();
          var from=  $('#FromDate').val();
          var to= $('#ToDate').val();
 $('#Branch').prop('selectedIndex',0);
             $('#Vertical').prop('selectedIndex',0);
         
            $("#FromDate").val(""); 
            $("#ToDate").val(""); 



          

        if(branch == "All" && vertical == "All" && from == 0 && to == 0)
            
            {

                             alert("Nothing Seleted");
            }else if(from != 0 && to == 0 || from == 0 && to != 0 )
            {
                alert("Please select both Date");
            }
            else 
            {
                if(from!=0 && to !=0)
                {
                   if( (new Date(from) < new Date(to)))
                    {
                            $.get("{{ URL::to('searchfiltermanagement') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
                $('#main').html(data);
          });

                    }else 
                    {
                            alert("From Date should always be less than To Date");
                    } 
                   
                }else 
                {
                         $.get("{{ URL::to('searchfiltermanagement') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
                $('#main').html(data);
          });
                }
            }
    });





    if (screen.width <= 800) {
        $('#mySidenav').hide();
        $('#main').hide();
        $.get("{{ URL::to('managementmobile') }}",function(data){
            $('#mobile').html(data);
        });

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

    }
    else
    {
        $('#mySidenav').show();
        $('#main').show();
    }

});
</script>
 <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

 var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deferred", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 830,
        height: 400,
annotations: {
    alwaysOutside: false
},
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };




        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
    </script>
  <!-- Pie chart starts
-->
<?php

$j= 0;
for($i=0; $i< $verticals_count; $i++)
{
    ?>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',   {{$statuscounts[$j][0]}}  ],
            ['In Progress Leads', {{$statuscounts[$j][1]}}],
            ['Converted Leads', {{$statuscounts[$j][2]}}],
            ['Dropped Leads', {{$statuscounts[$j][3]}} ],
            ['Deferred Leads', {{$statuscounts[$j][4]}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 280,
            height: 350,
              colors: ['#337ab7','orange','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j; ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  100,   200,300,400,500],
        ['Processing', 100,   200,300,400,500],
        ['Await', 100,   200,300,400,500],
        ['Out', 100,   200,300,400,500],
        ['Delivered',  100,   200,300,400,500],
        ['Canceled', 100,   200,300,400,500],
        ['Order Return', 100,   200,300,400,500],
        ['Rec Return', 100,   200,300,400,500]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },
           
          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('seeling_product_line_chart'));

    chart.draw(data, options);
}
</script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  100,   200,300,400,500],
        ['Processing', 100,   200,300,400,500],
        ['Await', 100,   200,300,400,500],
        ['Out', 100,   200,300,400,500],
        ['Delivered',  100,   200,300,400,500],
        ['Canceled', 100,   200,300,400,500],
        ['Order Return', 100,   200,300,400,500],
        ['Rec Return', 100,   200,300,400,500]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },
           
          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('Rental_line_chart'));

    chart.draw(data, options);
}
</script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Bengaluru','Pune','Hyderabad','Chennai','Hubballi-Dharwad'],
        ['New',  100,   200,300,400,500],
        ['Processing', 100,   200,300,400,500],
        ['Await', 100,   200,300,400,500],
        ['Out', 100,   200,300,400,500],
        ['Delivered',  100,   200,300,400,500],
        ['Canceled', 100,   200,300,400,500],
        ['Order Return', 100,   200,300,400,500],
        ['Rec Return', 100,   200,300,400,500]
    ]);

    var options = {
        title: '',
        curveType: 'none',
        width: 940,
        height: 500,
        pointSize: 10,
        pointShape: 'square',
        series: {
            0: { color: '#33cc00' },
            1: { color: '#3399ff' },
            2: { color: '#ffa64d' },
            3: { color: '#996633' },
            4: { color: '#ff6600' },
           
          },
        legend: { position: 'none' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('Pharmacy_chart'));

    chart.draw(data, options);
}
</script>

<!-- Pie chart ends Here -->
<style type="text/css">

body::-webkit-scrollbar
{
    display: none;
}
#myModal
{
    display: none;
}
.btn
{
        color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
}
body
{
    overflow-y: scroll;
    overflow-x: hidden;
    font-family: Raleway,sans-serif;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}


.footer {
    margin-top: 10%;
    display: none;
}
.panel-body
{
    text-align: center;
    height: 147px;
}


.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}
.custom {
    color: #333;
    padding-left: 17px;
    padding-bottom: 9px;
    padding-top: 11px;
    width: 102%;
    background: #f2f2f2;
    font-family: Raleway,sans-serif;
    outline: none;
    margin-left: -28px;
    text-align: -webkit-left;
    border: 0px;
    font-size: 18px;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -70px;
}

.sidenav a:hover, .offcanvas a:focus{

}
a
{
    text-decoration: none !important;
}
input
{
  border-right: 0px;  
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px; 
    border-bottom:1px solid #484e51;
    padding-bottom: 0px; 
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
select
{
background: white;
}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
.picker__holder
{
    padding-top: 10px;
        margin-top: 43%;
    width: 60%;
    height: 320px;
    margin-left: 20%;
    overflow-y: hidden;

}

.picker__wrap
{
    margin-top: -14%;
}
.branch
{
    text-align: left;
}
#main {

    transition: margin-left .5s;
    padding: 16px;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}

}
@media screen and (max-width: 380px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 20%;
    }
    #branch1
    {
        margin-left: -17px;
    }
    #donutchartmobile
    {
        margin-left: -54px;
    }
}
@media screen and (min-width: 412px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 15%;
    }
    #greetings
    {
        padding-top: 1px;
    }

}
@media screen and (min-width: 1200px) {
    #logoimage {
        margin-left: -69px;
    }
}
#mySidenav,#main
{
    display: none;
}

</style>

<style type="text/css">
    @media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>


@extends('layouts.app')

@section('content')

<div id="mySidenav" class="sidenav" style="height: 653px;      -webkit-transition:none !important;
  -moz-transition:none !important;
  -o-transition:none !important;
  transition:none !important;       background-color: white">
    <!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
    <ul class="nav" id="side-menu" style="    margin-top: -2px;">


        <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
            <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

            <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
                width: 15px;
                float: left;
                margin-left: -2px;display: none;" id="close"> </a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/cc/create?name={{ Auth::user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 4px; margin-top: -7px;
    width: 22px;    margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="  margin-top: -7px;
    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Lead</p></a>

            </li>

            <li style="    height: 55px;    ">
                <a href="/cc?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



            </li>
            <li style="    height: 55px;    ">
                <a href="/product?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Leads</p></a>



            </li>
            <li style="    height: 55px;    ">
                <a href="/provisionalview?name={{ Auth::user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;    margin-top: -74px;"> <img src="/img/provisional.png" class="img-responsive" alt="add" style=" margin-top: -7px;
    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Provisional Leads</p></a>



            </li>
            <li style="    height: 55px;    ">
                <a href="/log" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;    margin-top: -89px"> <img src="/img/logs.png" class="img-responsive" alt="add" style="  margin-top: -7px;
    width: 22px;  padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Logs</p></a>



            </li>
            <li style="    height: 55px;    ">
                <a href="/updatetables" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -105px;"> <img src="/img/tables.png" class="img-responsive" alt="add" style=" margin-top: -7px;
    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Update Tables</p></a>



            </li>
                  <li style="    height: 55px;    ">
                <a href="#" style="  border-bottom: 1px solid #bfbfbf; outline: none; height: 40px;margin-top: -120px;" id="filter"> <img src="/img/filter.png" class="img-responsive" alt="add" style=" margin-top: -7px;
    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Filters</p></a>



            </li>
        </ul>
    </div>


    <div id="main" style="margin-top: -11%;background: #eee; margin-bottom: 20px;    padding-top: 50px;">


        <div class="container-fluid" style="    margin-top: -43px;">
            <div class="row">



            <div class="col-sm-10 col-sm-offset-1" style="margin-top:6%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                        margin-left: -4px;height: 180px;">

                    <div class="panel-body">
                        <h3 style="color:  #33cc33;    margin-top: 36px;"> Welcome !!! </h3>
                        <p> You are logged in as Management. </p>

                    </div>
                </div>
            </div>

                <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
                        <div class="panel panel-default">

                    <a href="/managementassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                        </div>

                    </a>
                </div>

                </div>

                <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/managementassign?name={{ Auth::user()->name }}&status=In%20Progress">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{ $inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/managementassign?name={{ Auth::user()->name }}&status=Converted">

                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> {{ $convertedcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-2 " style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/managementassign?name={{ Auth::user()->name }}&status=Deferred">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> {{ $deferredcountforall }} </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        <a href="/managementassign?name={{ Auth::user()->name }}&status=Dropped">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}} </h3>
                            </div>
                        </a>
                    </div>
                </div>





                <div class="col-sm-10 col-sm-offset-1" style="margin-top:3%;    ">
                    <div class="panel panel-default" style="margin-right: 6px;
                    margin-left: -4px;">

                    <div class="panel-body" style="height: auto;">
                    <h3> Overall Status </h3>
                        <div id="columnchart_values" ></div>

                    </div>
                </div>
            </div>

 <div class="col-sm-10 col-sm-offset-1" style="    margin-left: 88px;">
            <?php

            $j= 0;
            for($i=0; $i< $verticals_count; $i++)
            {

                ?>

            <div class="col-sm-6 " style="margin-top: 3%;">
                <div class="panel panel-default" style="margin-right: 6px;
                margin-left: -4px;">

                <div class="panel-body" style="height: auto;">
                    <h3> {{$Vertical_names[$i]}} </h3>
                    <div id="<?php echo $j; ?>" style="   margin-left: -11px;"></div>
                </div>
            </div>
        </div>

        <?php
        $j=$j+1;

        }

        ?>

</div>
    </div>
</div>
</div>
</div>




<div class="col-sm-12">
    <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

        Copyright © Health Heal - 2017

        <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
    </div>


</div>


<script>

document.getElementById("mySidenav").style.width = "230px";
document.getElementById("main").style.marginLeft = "200px";
$("#close").show();

function openNav() {
    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $(".pa").show();

    $("#open").hide();
    $("#close").show();


}

function closeNav() {
    $("#open").show();
    $("#close").hide();
    document.getElementById("mySidenav").style.width = "50";
    document.getElementById("main").style.marginLeft= "50";
    $(".pa").hide();
}
</script>
<!-- Footer  -->
@endsection
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-body" style="min-height: 238px;">
                 <div class="col-sm-6">
                    <center> <p> <b> Branch : </b> </p>
                     <select name="Branch"  id="Branch" value="" style="width: 80%;" >
                         <option value="All">All</option>
                                 @foreach($data as $city)
                              <option value="{{ $city->name}}">{{ $city->name}}</option>
                              @endforeach
                            </select>
                            </center>
                </div>
                <div class="col-sm-6">
                            <center> <p> <b> Vertical: </b> </p>
                            <select name="Vertical"  id="Vertical" value="" style="width: 80%;" >
                                    <option value="All">All</option>
                                    <option value="NursingServices">Nursing Services</option>
                                    <option value="PersonalSupportiveCare">Personal Supportive Care</option>
                                    <option value="Mathrutvam-Baby Care">Mathrutvam - Baby Care</option>
                                    <option value="Physiotherapy-Home">Physiotherapy - Home</option>
                            </select>
                            </center>
                </div>
                <div class="col-sm-12">
                <br>
                            <center> <p> <b> TimeLine: </b> </p>  </center>

                            <div class="col-sm-5">
                                  <!--   <input type="date" name="FromDate" id="FromDate" class="custominput"> -->
                            <!--         <input
                   
                    class="datepicker custominput"
                    name="date"
                    type="date"
                    name="FromDate" id="FromDate"
                     >
            -->
             <input  type="text" placeholder="From Date"  id="FromDate" onkeypress=""/>

        <div id="container"></div>
                            </div>
                            <div class="col-sm-2" ><center style="    padding-top: 9px;"> To </center></div>
                            <div class="col-sm-5">
                                <!--     <input type="date" name="ToDate" id="ToDate" class="custominput" style="width: 89%;"> -->

                                              <input type="date" class="datepicker custominput"
                    name="ToDate" id="ToDate"  style="width: 89%;"
                     >  
                            </div>
                </div>

                 <div class="col-sm-12" style="text-align: -webkit-right;">
                <br>
                           <button type="reset" class="btn btn-default" style="background: #00C851;    margin-right: 16px;" id="search" data-dismiss="modal">Apply Filter</button>
                </div>
        </div>

      </div>

    </div>
  </div>


<div id="mobile" > </div>

