<!-- This is the intermediary code for log view -->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token1" content="{{ csrf_token() }}"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>
   <script>
$(document).ready(function(){
  $('.footable').footable();
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();

        $.ajax({
        type: "GET",
        url:'alllogs' ,
       data: {'keyword1' : keyword,'filter1' : filter,'_token1':$('input[name=_token]').val() },
        success: function(data){
              $('#result').html(data);
        }
    });
    });


});
</script>

  </head>
  <body>
   <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" rel="home" href="/admin/home" title="Buy Sell Rent Everyting">
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="/admin/management"
                                            >
                                            Home
                                        </a>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>


        </div>

    </div>
</div>
<!-- title -->
  <div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2> Logs</h2></center>
               </div>
            </div>

    </div>
    {{csrf_field()}}
 <input type="hidden" name="_token1" value="{{ csrf_token() }}">
<!-- Search Section -->
  <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
          <div class="col-sm-3">

          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Logs by</option>

                         <option value="name">Employee Name</option>
                        <option value="activity">Actions</option>

                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword'>
              </div>
              <div class="col-sm-3">

          </div>

          </div>

      </div>

  </div>


<div class="container" style="    margin-top: 47px;">
      <div class="row">
            <div class="col-sm-12" id="result">
                      <table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
      <tr>
        <th><strong>Created At</strong></th>
        <th data-hide="phone,tablet"><strong>Lead Id</strong></th>
        <th data-hide="phone,tablet"><strong>Employee Id</strong></th>
         <th data-hide="phone,tablet"><strong>Employee Name</strong></th>
          <th data-hide="phone,tablet"><strong>Actions</strong></th>
          <th data-hide="phone,tablet"><strong>Field Updated</strong></th>
          <th data-hide="phone,tablet"><strong>Value Updated</strong></th>
      </tr>
    </thead>
    <tbody>
    @foreach($log as $logs)
      <tr>
        <td>{{$logs->activity_time}}</td>
        <td>{{$logs->lead_Id}}</td>
        <td>{{$logs->emp_Id}}</td>
        <td>{{$logs->name}}</td>
        <td>{{$logs->activity}}</td>
         <td>{{$logs->field_updated}}</td>
          <td>{{$logs->value_updated}}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
  <div style="text-align: -webkit-center;"> {{$log->links()}} </div>
            </div>

      </div>
</div>
  @extends('layouts.footer')
