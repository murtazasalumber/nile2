<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

 var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{$inprogresscountforall}}, "orange"],
        ["Converted",{{$convertedcountforall}}, "#33cc33"],
        ["Deferred", {{$deferredcountforall}}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);


      var options = {
        title: "",
        width: 250,
        height: 300,
annotations: {
    alwaysOutside: false
},
		colors: ['#337ab7', 'orange', '#33cc33','#990000','#ff1a1a'],
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };




        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
    </script> -->
      <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Personal Supportive Care', 'Mathrutvam - Baby Care','Nursing Services','Physiotherapy - Home'],
          ['Bengaluru',  {{$bpsc}},   {{$bm}},{{$bns}},{{$bphy}}],
          ['Pune', {{$ppsc}},   {{$pm}},{{$pns}},{{$pphy}}],
          ['Hyderabad', {{$hpsc}},   {{$hm}},{{$hns}},{{$hphy}}],

          ['Chennai', {{$cpsc}},   {{$cm}},{{$cns}},{{$cphy}}],
          ['Hubballi-Dharwad',  {{$hdpsc}},   {{$hdm}},{{$hdns}},{{$hdphy}}]
        ]);

        var options = {
          title: '',
          curveType: 'none',
          width: 300,
        height: 300,
         pointSize: 10,
          pointShape: 'square',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
<div class="container">
	<div class="row">
				<div class="col-sm-12 " style="margin-top: 10px;">
                        <div class="panel panel-default">

                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                        </div>

                    </a>
                </div>


                </div>

                <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{$inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> {{$convertedcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12 " >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                    @endif                               <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> {{$deferredcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-12" >
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        @if($designation=="Management")
                        <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                            @else
                            <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical=All&from={{$Fromdate}}&to={{$Todate}}">
                                @endif                           <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}}</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-12">
                		<div class="panel panel-default" style="
                   ">

                    <div class="panel-body" style="height: auto;">
                    <h3> Overall  Status </h3>
                    <br>
                    <br>
                      <center>    <div id="curve_chart" ></div></center>
                    </div>
                </div>

                </div>	

	</div>

</div>