
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Branches', 'New ', 'Inprogress', 'Converted','Deferred','Drooped'],
          ['Ban', {{$newcountforbng}},{{$inprogresscountforbng}},{{$convertedcountforbng}},{{$deferredcountforbng}},{{$droppedcountforbng}}],
          ['Pune', {{$newcountforpune}},{{$inprogresscountforpune}},{{$convertedcountforpune}},{{$deferredcountforpune}},{{$droppedcountforpune}}],
          ['Hyd', {{$newcountforhyd}},{{$inprogresscountforhyd}},{{$convertedcountforhyd}},{{$deferredcountforhyd}},{{$droppedcountforhyd}}],
          ['Che', {{$newcountforchen}},{{$inprogresscountforchen}},{{$convertedcountforchen}},{{$deferredcountforchen}},{{$droppedcountforchen}}],
           ['Hu-Dh', {{$newcountforhub}},{{$inprogresscountforhub}},{{$convertedcountforhub}},{{$deferredcountforhub}},{{$droppedcountforhub}}]
        ]);

        var options = {
          chart: {
            title: '',
            subtitle: '',
          },
           legend: {
                    position: 'none'
          },
          colors: ['#337ab7', 'orange', '#33cc33','#990000','#ff1a1a'],
    animation:
           {
               "startup": true,
               duration: 10000,
               easing: 'inAndOut'
           },

        //    "displayExactValues": true,
        // "vAxis": {
        //     "title": "Sales unit",
        //     "gridlines": {
        //         "count": 10
        //     }
        // },
        "hAxis": {
            "title": ""
        }

        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
<div class="container">

		<div class="row">
						 <div class="col-sm-12" style="margin-top: 4%;">
                        <div class="panel panel-default">

                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=New&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                    @endif                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                        </div>

                    </a>
                </div>




                </div>

                <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{$inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-12" >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Converted&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                    @endif
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> {{$convertedcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12 " >
                        <div class="panel panel-default">
                            @if($designation=="Management")
                            <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                @else
                                <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Deferred&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                    @endif                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> {{$deferredcountforall}}</h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-12" >
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        @if($designation=="Management")
                        <a href="/managementfiltervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                            @else
                            <a href="/filtervalueshow?name={{ Auth::guard('admin')->user()->name }}&status=Dropped&branch=All&vertical={{$vertical}}&from=NULL&to=NULL">
                                @endif                           <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}}</h3>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-sm-12">
                				<div class="panel panel-default" style="
                   ">

                    <div class="panel-body" style="height: auto;">
                    <h3>Vertical Status-{{$vertical}}  </h3>
                    <br>
                    <br>
                      <center> <div id="columnchart_material" style="width: 250px; height: 200px;"></div></center>
                      <br>
                		<div class="col-sm-12">
                			<div style="width: 12px; height: 12px;border: solid 7px #337ab7;float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;"> New</p>
                		</div>
                		<div class="col-sm-12">
                		
                         <div style="width: 12px; height: 12px;border: solid 7px orange; margin-left: 20%;
;float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left; ">  In Progress</p>
                		</div>
                		<div class="col-sm-12"></div>
                		<div class="col-sm-12"></div>
                		<br><br>
                		<div class="col-sm-12" style="margin-top: -13px;">
                		
                            <div style="width: 12px; height: 12px;border: solid 7px #33cc33;margin-left: 0%; float: left;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;">Converted</p>
                		</div>
                		<div class="col-sm-12">
                		
                            <div style="width: 12px; height: 12px;border: solid 7px #990000;float: left; margin-left: 5%;">
                        </div>
                        <p style="    padding-left: 7px;margin-top: -3px; float: left;"> Deferred</p>
                		</div>
                		<div class="col-sm-12"></div>
                		<div class="col-sm-12"></div>
                		<br><br>
                		<div class="col-sm-12" style="margin-top: -13px;">
                					<div style="width: 12px; height: 12px;border: solid 7px #ff1a1a;float: left;">
                        </div>

                        <p style="    padding-left: 7px;margin-top: -3px; float: left;">Dropped</p>
                		</div>
                      </div>
                      </div>
                </div>
		</div>
</div>