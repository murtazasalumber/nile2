<?php

$j= 0;
for($i=0; $i< $verticals_count; $i++)
{
    ?>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',   {{$statuscounts[$j][0]}}  ],
            ['In Progress Leads', {{$statuscounts[$j][1]}}],
            ['Converted Leads', {{$statuscounts[$j][2]}}],
            ['Dropped Leads', {{$statuscounts[$j][3]}} ],
            ['Deferred Leads', {{$statuscounts[$j][4]}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 250,
            height: 350,
            colors: ['#337ab7','orange','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>
<!-- Pie chart ends Here -->



<body>
	
            <?php

            $j= 0;
            for($i=0; $i< $verticals_count; $i++)
            {

                ?>

            <div class="col-sm-12 " style="margin-top: 3%;">
                <div class="panel panel-default graph1" id="s" >

                <div class="panel-body" style="height: auto;text-align: center;">
                    <h3> {{$Vertical_names[$i]}} </h3>
                    <div id="<?php echo $j; ?>" style="margin-left: -6%;"></div>
                </div>
            </div>
        </div>

        <?php
        $j=$j+1;

        }

        ?>


</body>