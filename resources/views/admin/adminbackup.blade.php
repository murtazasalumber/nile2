<style type="text/css">


.panel-body
 {
        border-radius: 24px;

    padding: 15px;

}

.footer {

  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
}
.panel-body
{
    text-align: center;
    height: 300px;
}
.btn-default {
    width: 60%;
    color: #636b6f;
    background-color: #fff;
    border-color: #ccc;
}
.footer
{
  position: absolute;
}


</style>
@extends('layouts.app')

@section('content')



<div class="container-fluid">
    <div class="row">
        <!-- Provisions starts -->
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body" >

                  <!-- This goes to the customer care -> create route and send the logged in user's name in the session -->
                     <a href="/cc/create?name={{ Auth::user()->name }} " class="btn btn-default" >New Lead</a>
                     <br>
                    <br>
                    <!-- This goes to the customer care screen and send the logged in user's name in the session -->
                     <a href="/cc?name={{ Auth::user()->name }} " class="btn btn-default" >View Leads</a>
                      <br>
                     <br>
                     <a href="/provisionalview?name={{ Auth::user()->name }} " class="btn btn-default" style="margin-left: -2px; text-align: center;">View Provisional Leads</a>   
                     <br>
                 <br>
                     <a href="/log" class="btn btn-default" style="margin-left: -4px;">View Logs</a>
                     <br>
                 <br>
                     <a href="/updatetables" class="btn btn-default" style="margin-left: -4px;">Update the Tables</a>

                </div>
            </div>
        </div>
        <!-- Provisions Ends -->

        <!-- Welcome Message starts -->
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-body" style="text-align: center;">
                  <h3>Welcome  </h3>
                  <br>
                  <p> You are logged in as Admin!1 </p>
                </div>
            </div>
        </div>
          <!-- Welcome Message starts -->
    </div>
</div>

@endsection

