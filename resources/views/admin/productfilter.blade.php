<!-- This page acts as a version of the View Leads page -->
<script>
$(document).ready(function(){
  $('.footable').footable();



});
</script>
<!-- If some data is retrieved from the filter function in the ccController -->
@if(count($data1) > 0)

@if ($filter1 === "fName")
<table class="table footable" style="font-family: Raleway,sans-serif;margin-top: 4%;">
        <thead>
        <th  data-hide="phone,tablet" style="color: #333;"><b>S.no</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>order id</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>Lead ID</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Product Type</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created At</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created By</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Assigned To</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Name</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet" style="color: #333;"><b>Status</b></th>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          @foreach ($leads as $lead)
          <tr>
            <?php
            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
     

@if($lead->MedName!=NULL)

  @if($lead->SKUid!=NULL)

   
  <td>Both</td>

    @else

 
   <td><a href="pharmacyshow?id={{$lead->PharmacyId}}&name=<?php echo $name?>">{{$lead->id}}</a></td>
   <td>{{$lead->orderid}}</td>
  <td>{{$lead->leadid}}</td>
  <td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
 <td> {{$lead->PAssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

  @endif

@else

    <td><a href="productshow?id={{$lead->Prodid}}&name=<?php echo $name?>">{{$lead->id}}</a></td>

<td>{{$lead->orderid}}</td>
<td>{{$lead->leadid}}</td>

  <td>Product</td>
  <td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}{{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
  
  


            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach
    </tbody>
  </table>

@elseif ($filter1 === "AssignedTo")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
       <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    
    <td style="color:red;">{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>
    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "orderid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
        <td style="color:red;"><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>




    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "SKUid")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
      <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
       <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td style="color:red;">{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>

    </tr>
    @endforeach
  </tbody>
</table>

@elseif ($filter1 === "MobileNumber")
<table class="table footable" style="font-family: Raleway,sans-serif;">
  <thead>
    <tr>
<th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $lead)
    <tr>
      <?php
      if(session()->has('name'))
      {
        $name=session()->get('name');
      }else
      {
        if(isset($_GET['name'])){
          $name=$_GET['name'];
        }else{
          $name=NULL;
        }
      }
      ?>
    <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td style="color:red;">{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>
    </tr>
    @endforeach
  </tbody>
</table>



      @elseif ($filter1 === "OrderStatus")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td >{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>

             
@elseif ($filter1 === "OrderStatusrent")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


@elseif ($filter1 === "ProductName")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td style="color:red;">{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


@elseif ($filter1 === "city")
          <table class="table footable" style="font-family: Raleway,sans-serif;">
                                <thead>
                                   <tr>
                                              <th><b> Order ID</b></th>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b>Created At</b></th>
                    <th  data-hide="phone,tablet"><b>Created By</b></th>
                    <th  data-hide="phone,tablet"><b>Assigned To</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                    <th data-hide="phone,tablet" ><b>City</b></th>
                    <th data-hide="phone,tablet" ><b>SKU Id</b></th>
                    <th data-hide="phone,tablet"><b>Product Name</b></th>
                    <th data-hide="phone,tablet"><b>Quantity</b></th>
                    <th data-hide="phone,tablet"><b>Type</b></th>
                    <th data-hide="phone,tablet"><b>Price</b></th>
                    <th data-hide="phone,tablet" ><b>Payment Mode</b></th>
                    <th data-hide="phone,tablet" ><b>Status</b></th>


                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($data1 as $lead)
                                  <tr>
                                  <?php
if(session()->has('name'))
{
  $name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
     <td><a href="productshow?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->orderid}}</a></td>
    <td>{{$lead->leadid}}</td>
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->Requestcreatedby}}</td>
    <td>{{$lead->AssignedTo}}</td>
  <td>{{$lead->fName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td style="color:red;">{{$lead->City}}</td>



  <td>{!! nl2br(e($lead->SKUid)) !!}</td>
  <td>{!! nl2br(e($lead->ProductName)) !!}</td>
  <td>{!! nl2br(e($lead->Quantity)) !!}</td>
  <td>{!! nl2br(e($lead->Type)) !!}</td>
<?php
  if($lead->Type=="Rent")
  {
?>
  <td>{!! nl2br(e($lead->RentalPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPaymentrent)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatusrent)) !!}</td>
<?php }
else
{?>

  <td>{!! nl2br(e($lead->SellingPrice)) !!}</td>
  <td>{!! nl2br(e($lead->ModeofPayment)) !!}</td>
  <td>{!! nl2br(e($lead->OrderStatus)) !!}</td>
  <?php
  } ?>



                                  </tr>
                                @endforeach
                                </tbody>
                              </table>


<!-- If no data is retrieved from the filter function in the ccController -->
@else
<p> No result Found </p>
@endif
@endif
