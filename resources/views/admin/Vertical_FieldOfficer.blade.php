
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

<!-- including the bootstrap datepicker file here -->
<script src="/js/bootstrap-datepicker.js" type="text/javascript"></script>


<script>
$(document).ready(function(){



    if (screen.width <= 800) {

        $('#mobile').show();
        window.location.replace("{{ URL::to('homemobilevertical') }}");

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

    }
    else
    {
        $('#mySidenav').show();
        $('#main').show();

    }

    //         $(function(){ // jQuery dom ready event
    //     if (window.location.href.toLowerCase().indexOf("loaded") < 0) {
    //         window.location = window.location.href + '?loaded=1'
    //     }
    // });
    $("#filter").click(function(){

        $('#myModal').modal('show');



    });
    $("#search").click(function(){



        var branch =  $('#Branch').val();
        var vertical=   $('#Vertical').val();
        var from=  $('#FromDate').val();
        var to= $('#ToDate').val();

        if(from == 0 && to == 0)
        // alert("branch and vertical");
        {
            alert("Nothing Seleted");
        }
        else
        {
            $.get("{{ URL::to('searchfiltervertical') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to } ,function(data){
                $('#main').html(data);
            });

        }

    });
});
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcount}}, "#337ab7"],
        ["In Progress", {{$inprogresscount}}, "orange"],
        ["Converted", {{$convertedcount}}, "#33cc33"],
        ["Deferred", {{ $deferredcount }}, "#990000"],
        ["Dropped", {{$droppedcount}}, "#ff1a1a"],

    ]);
    var view = new google.visualization.DataView(data);


    var options = {
        title: "",
        width: 810,
        height: 400,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}
</script>

<?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{
    ?>

    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',    {{$statuscounts[$j][0]}} ],
            ['Converted Leads',  {{ $statuscounts[$j][1] }}],
            ['Deferred Leads', {{ $statuscounts[$j][2] }} ],
            ['Dropped Leads', {{ $statuscounts[$j][3] }}  ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend: 'none',
            colors: ['#337ab7', '#33cc33', '#990000', '#ff1a1a'],
            width: 380,
            height: 350,
            title: '',
            pieHole: 0.4,

            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>


<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<style type="text/css">

body::-webkit-scrollbar
{
    display: none;
}
#myModal
{
    display: none;
}
.btn
{
    color: #FFF!important;
    background-color: #4285F4;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.nav-tabs {

    border-bottom: 1px solid #ddd;
}
.nav-tabs>li {
    float: left;
    margin-bottom: -15px!important;
}
.btn:hover
{
    outline: 0;
    background-color: #00C851;
    opacity: 0.5;
}
select
{
    border-bottom-color:#484e51;
    border-top: 0px;
    width: 60%;
    border-left: 0px;
    border-right: 0px;
    background: #eee;
    outline: none;
}
.tooltip.bottom .tooltip-arrow {

    display: none;
}
.tooltip.bottom {
    padding: 5px 0;
    margin-top: -28px !important;
}
.cssmenu,
.cssmenu ul,
.cssmenu ul li,
.cssmenu ul li a {
    margin: 0;
    padding: 0;
    border: 0;
    list-style: none;
    line-height: 1;
    display: block;
    position: relative;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.cssmenu:after,.#cssmenu > ul:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
}
.cssmenu {
    width: auto;

    font-family: Raleway, sans-serif;
    line-height: 1;
}
.cssmenu ul {
    background: #ffffff;
}
.cssmenu > ul > li {
    float: left;
}
.cssmenu.align-center > ul {
    font-size: 0;
    text-align: center;
}
.cssmenu.align-center > ul > li {
    display: inline-block;
    float: none;
}
.cssmenu.align-right > ul > li {
    float: right;
}
.cssmenu.align-right > ul > li > a {
    margin-right: 0;
    margin-left: -4px;
}
.cssmenu > ul > li > a {
    margin-top: 23px;
    z-index: 2;
    padding: 18px 25px 12px 25px;
    font-size: 15px;
    font-weight: 400;
    text-decoration: none;
    color: #444444;
    -webkit-transition: all .2s ease;
    -moz-transition: all .2s ease;
    -ms-transition: all .2s ease;
    -o-transition: all .2s ease;
    transition: all .2s ease;
    margin-right: -4px;
}
.cssmenu > ul > li.active > a,
.cssmenu > ul > li:hover > a,
#cssmenu > ul > li > a:hover {
    . color: #ffffff;
}
.cssmenu > ul > li > a:after {
    position: absolute;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: -1;
    width: 100%;
    height: 120%;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    content: "";
    -webkit-transition: all .2s ease;
    -o-transition: all .2s ease;
    transition: all .2s ease;
    -webkit-transform: perspective(5px) rotateX(2deg);
    -webkit-transform-origin: bottom;
    -moz-transform: perspective(5px) rotateX(2deg);
    -moz-transform-origin: bottom;
    transform: perspective(5px) rotateX(2deg);
    transform-origin: bottom;
}
.cssmenu > ul > li.active > a:after,
.cssmenu > ul > li:hover > a:after,
.cssmenu > ul > li > a:hover:after {
    background: #47c9af;
}
body
{
    overflow-y: scroll;
    overflow-x: hidden;
    font-family: Raleway,sans-serif;
}
.panel-body
{
    border-radius: 24px;

    padding: 15px;

}


.footer {
    margin-top: 10%;
    display: none;
}
.panel-body
{
    text-align: center;
    height: 147px;
}


.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #555;



    cursor: default;
}
#tabmenu
{
    width: 104%;
    margin-left: -17px;
    height: 69px;
    margin-top: 17px;
    border-bottom: 3px solid #47c9af;
}
.custom {
    color: #333;
    padding-left: 17px;
    padding-bottom: 9px;
    padding-top: 11px;
    width: 102%;
    background: #f2f2f2;
    font-family: Raleway,sans-serif;
    outline: none;
    margin-left: -28px;
    text-align: -webkit-left;
    border: 0px;
    font-size: 18px;
}
#logoimage
{
    margin-top: 1px;
    margin-left: -70px;
}

.sidenav a:hover, .offcanvas a:focus{

}
a
{
    text-decoration: none !important;
}
input
{
    border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
select
{
    background: white;
}
.custominput
{
    width: 103%;
    margin-left: 14px;
    outline: none;
    border-top: 0px;
    border-bottom: 1px solid;
    border-right: 0px;
    border-left: 0px
}
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
#FromDate
{
    width: 106%;
    margin-left: 11px;
}
#ToDate
{
    margin-left: -16px;
    width: 103%
}
.branch
{
    text-align: left;
}
#main {

    transition: margin-left .5s;
    padding: 16px;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}

}
@media screen and (max-width: 380px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 20%;
    }
    #branch1
    {
        margin-left: -17px;
    }
    #donutchartmobile
    {
        margin-left: -54px;
    }
}
@media screen and (min-width: 412px) {
    #logoimage {
        margin-left: -69px;
    }
    #mobile
    {
        margin-top: 15%;
    }
    #greetings
    {
        padding-top: 1px;
    }

}
@media screen and (min-width: 1200px) {
    #logoimage {
        margin-left: -69px;
    }
}
#mySidenav,#main
{
    display: none;
}

</style>
<script>
$(document).ready(function(){

    if (screen.width <= 800) {
        $('#mySidenav').hide();
        $('#main').hide();
        $.get("{{ URL::to('homemobile') }}",function(data){
            $('#mobile').html(data);
        });

    }else if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

    }
    else
    {
        $('#mySidenav').show();
        $('#main').show();

    }

});
</script>
<style type="text/css">
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>


@extends('layouts.app')

@section('content')



<div id="mySidenav" class="sidenav" style="height: 653px;     -webkit-transition:none !important;
-moz-transition:none !important;
-o-transition:none !important;
transition:none !important;         background-color: white">
<!--  <a href="javascript:void(0)" class="" onclick="closeNav()"><img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 17px;"></a> -->
<ul class="nav" id="side-menu" style="    margin-top: -2px;">


    <li style="    height: 51px;    border-bottom: 1px solid #bfbfbf;" id="close_open">
        <a href="javascript:void(0)" onclick="openNav()" style="    height: 36px;"> <img src="/img/keyboard-right-arrow-button.png" class="img-responsive" alt="add" style="    width: 15px; padding-top: 4px; display: none;    margin-right: 23px;  float: left;" id="open"> </a>

        <a href="javascript:void(0)" onclick="closeNav()" style="    height: 36px;"> <img src="/img/left-arrow.png" class="img-responsive" alt="add" style="     margin-top: -32px;
            width: 15px;
            float: left;
            margin-left: -2px;display: none;" id="close"> </a>

        </li>

        <li style="    height: 55px;    ">
            <a href="/Verticalhead/create?name={{ Auth::guard('admin')->user()->name }}" style=" border-bottom: 1px solid #bfbfbf;   height: 40px;    margin-top: -14px;"> <img src="/img/new lead.png" class="img-responsive" alt="add" style="    padding-top: 4px;  margin-top: -9px;
                width: 22px;    margin-right: 23px;  float: left;"> <p class="pa" style="font-size: 14px;color: black; margin-top: -3px;">Create Service Lead</p></a>

            </li>
            <li style="    height: 55px;    ">
                <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -29px;"> <img src="/img/product.png" class="img-responsive" alt="add" style="   margin-top: -9px;
                    width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">Create Product Order</p></a>
                </li>


                <li style="    height: 55px;    ">
                    <a href="/vh?name={{ Auth::guard('admin')->user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -44px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                        width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View Service Leads</p></a>



                    </li>

                    <li style="    height: 55px;    ">
                        <a href="/product?name={{ Auth::user()->name }}&status=All" style="  border-bottom: 1px solid #bfbfbf;  height: 40px;margin-top: -59px;"> <img src="/img/view leads.png" class="img-responsive" alt="add" style="  margin-top: -7px;
                            width: 22px; padding-top: 4px;     margin-right: 23px;  float: left;"> <p class="pa" style="margin-top: -3px;font-size: 14px;color: black;">View  Product Order</p></a>



                        </li>


                    </ul>
                </div>
                <div id="main" style="margin-top: -9%;background: #eee">


                    <div class="container-fluid" style="    margin-top: -43px">
                        <div class="row">



                            <div class="col-sm-10 col-sm-offset-1" style="margin-top:6%;    ">
                                <div class="panel panel-default" style="margin-right: 6px;
                                margin-left: -4px;height: 180px;">

                                <div class="panel-body">
                                    <h3 style="color:  #33cc33;    margin-top: 20px;"> Welcome !!! </h3>
                                    <p> You are logged in as Vertical Head and Field Officer. </p>

                                    <div class="tab cssmenu" id="tabmenu">


                                        <ul >
                                            <!-- changes here to make simply -->
                                            <li class="active"><a data-toggle="tab" href="#home" >Service Details</a></li>
                                            <li><a data-toggle="tab" href="#menu1" >Product Details</a></li>
                                            <!--     <li><a data-toggle="tab" href="#menu3" >Rental Product Details</a></li>
                                            <li><a data-toggle="tab" href="#menu2" >Pharmacy Details</a></li> -->


                                        </ul>



                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                        </div>



                        <!--  Count New  code -->

                        <!--    <div class="col-sm-3">
                        <div class="panel panel-default">
                        <a href="/verticalproductsellingassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                        <img src="/img/speech-bubble.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                        <h5 style="padding-top: 10px;">  In Progress Leads </h5>
                        <h3 style="color:#5cb85c; margin-top: -8px;"> {{$inprogresscount}}</h3>
                    </div>
                </a>
            </div>
        </div> -->

        <div class="col-md-12 tab-content" style="margin-top: 0px">
            <div id="home" class="tab-pane fade in active">

                <div class="col-sm-2  col-sm-offset-1 " style="margin-top: 10px;    ">
                    <div class="panel panel-default">

                        <a href="/verticalfieldofficerassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                                <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcount}}</h3>
                            </div>

                        </a>
                    </div>
                </div>





                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/verticalfieldofficerassign?name={{ Auth::guard('admin')->user()->name }}&status=In%20Progress">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/inprogress.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                <h3 style="color: orange; margin-top: -8px;"> {{$inprogresscount}}</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/verticalfieldofficerassign?name={{ Auth::guard('admin')->user()->name }}&status=Converted">

                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/converted.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                <h3 style=";color:#33cc33; margin-top: -8px;">{{$convertedcount}}</h3>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-2  " style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <a href="/verticalfieldofficerassign?name={{ Auth::guard('admin')->user()->name }}&status=Deferred">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/deffered.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f ">  Deferred Leads </h5>
                                <h3 style=" color:#990000; margin-top: -8px;">{{$deferredcount}}</h3>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-sm-2 " style="margin-top: 10px;">
                    <div class="panel panel-default" style="    width: 101%;
                    margin-left: -5px;">
                    <a href="/verticalfieldofficerassign?name={{ Auth::guard('admin')->user()->name }}&status=Dropped">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                            <h3 style="color:#ff1a1a; margin-top: -8px;">{{$droppedcount}} </h3>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-sm-10 col-sm-offset-1 " style="margin-top:2%;    ">
                <div class="panel panel-default" style="margin-right: 6px;
                margin-left: -4px;">

                <div class="panel-body" style="height: auto;text-align: center;">
                    <h3> Overall Status </h3>
                    <div id="columnchart_values" ></div>

                </div>
            </div>
        </div>


        <div class="col-sm-10 col-sm-offset-1" style="    margin-left: 82px;">



            <?php

            $j= 0;
            for($i=0; $i< $coords_under_vert_count; $i++)
            {

                ?>



                <?php
                if($i=="1")
                {?>

                    <div class="col-sm-6" style="    margin-top: -24px;">
                        <div class="panel panel-default" style="    min-height: 441px;">

                            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">


                                <?php
                                if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                                {?>
                                    <h3> {{$User[$i]}}</h3>
                                    <p> No count </p>
                                <?php } else
                                {?> <h3> {{$User[$i]}}</h3>
                                <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                            <?php }?>
                        </div>
                    </div>

                </div>
                <?php
            }else if($i=="2")
            {?>

                <div class="col-sm-6" style="    margin-top: -4px;" >
                    <div class="panel panel-default" style="    min-height: 441px;">

                        <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">


                            <?php
                            if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                            {?>
                                <h3> {{$User[$i]}}</h3>
                                <p> No count </p>
                            <?php } else
                            {?> <h3> {{$User[$i]}}</h3>
                            <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                        <?php }?>
                    </div>
                </div>

            </div>


            <?php

        }else
        {?>

            <div class="col-sm-6" style="    margin-top: -4px;">
                <div class="panel panel-default" style="    min-height: 441px;">

                    <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">


                        <?php
                        if($statuscounts[$j][0] == "0" && $statuscounts[$j][1]== "0" &&  $statuscounts[$j][2]== "0" && $statuscounts[$j][3]== "0" )
                        {?>
                            <h3> {{$User[$i]}}</h3>
                            <p> No count </p>
                        <?php } else
                        {?> <h3> {{$User[$i]}}</h3>
                        <div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
                    <?php }?>
                </div>
            </div>

        </div>
        <?php
    }




    $j=$j+1;

}

?>


</div>

<!-- <?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{

?>

<div class="col-sm-4" style="margin-top: 2%;">
<div class="panel panel-default">

<div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
<h3> {{$User[$i]}}</h3>
<div id="<?php echo $j; ?>" style="margin-left: -15px;"></div>
</div>
</div>

</div>





<?php
$j=$j+1;

}

?> -->


</div>




<!-- Product details counts -->


<div id="menu1" class="tab-pane fade">

    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Processing">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/processing.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f"> Processing   </h5>
                    <h3 style="color:orange; margin-top: -8px;"> {{$processingcount}} </h3>
                </div>
            </a>
        </div>
    </div>


    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Awaiting%20Pickup">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/awaiting pickup.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Awaiting Pickup </h5>
                    <h3 style="color:purple; margin-top: -8px;">{{$awaitingpickupcount}} </h3>
                </div>
            </a>
        </div>
    </div>



    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Ready%20To%20Ship">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/Ready To ship.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Ready To Ship </h5>
                    <h3 style="color:yellow; margin-top: -8px;">{{$readytoshipcount}} </h3>
                </div>
            </a>
        </div>
    </div>



    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Out%20for%20Delivery">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/out for delivery.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Out for Delivery </h5>
                    <h3 style="color:pink; margin-top: -8px;">{{$outfordeliverycount}} </h3>
                </div>
            </a>
        </div>
    </div>



    <div class="col-sm-2 col-sm-offset-3" style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Delivered">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/delivered.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Delivered </h5>
                    <h3 style="color:#33cc33; margin-top: -8px;">{{$deliveredcount}} </h3>
                </div>
            </a>
        </div>
    </div>

    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Cancelled">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/cancelled.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Cancelled </h5>
                    <h3 style="color:#ff1a1a; margin-top: -8px;">{{$canceledcount}} </h3>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-2 " style="margin-top: 10px;">
        <div class="panel panel-default">
            <a href="/verticalfieldofficerproductassign?name={{ Auth::guard('admin')->user()->name }}&status=Order%20Return">
                <div class="panel-body" style="    text-align: -webkit-center;">
                    <img src="/img/order return.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                    <h5 style="padding-top: 10px;color: #636b6f">  Order Return </h5>
                    <h3 style="color:#990000; margin-top: -8px;">{{$orderreturncount}} </h3>
                </div>
            </a>
        </div>
    </div>


</div>
<!-- product sell end -->

</div>


<div class="col-sm-12">
    <div class="footer1" style="    margin-top: 30px;background: white;text-align: center; ">

        Copyright © Health Heal - 2017

        <p style="float: right;    margin-top: 0px;    margin-right: 6%;"> Ver : v 3.2</p>
    </div>


</div>



<script>

document.getElementById("mySidenav").style.width = "230px";
document.getElementById("main").style.marginLeft = "200px";
$("#close").show();

function openNav() {
    document.getElementById("mySidenav").style.width = "230px";
    document.getElementById("main").style.marginLeft = "200px";
    $(".pa").show();

    $("#open").hide();
    $("#close").show();


}

function closeNav() {
    $("#open").show();
    $("#close").hide();
    document.getElementById("mySidenav").style.width = "50";
    document.getElementById("main").style.marginLeft= "50";
    $(".pa").hide();
}
</script>
<!-- Footer  -->
</div>



@endsection
<div id="mobile" style="    margin-top: 15%;">
    @extends('admin.mobilevertical')
</div>
