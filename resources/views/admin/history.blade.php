@extends('layouts.app1')

@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token1" content="{{ csrf_token() }}"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
<link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
<script src="js/footable.js" type="text/javascript"></script>
   <script>
$(document).ready(function(){
  $('.footable').footable();
  $('[data-toggle="popover"]').popover({ animation:true,  html:true});
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {

           $("#usersetting").popover('hide');

        }else
        {
           $("#usersetting").popover('show');

          //console.log(sidenvavv);
        }
    });
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();

        $.ajax({
        type: "GET",
        url:'alllogs' ,
       data: {'keyword1' : keyword,'filter1' : filter,'_token1':$('input[name=_token]').val() },
        success: function(data){
              $('#result').html(data);
        }
    });
    });


});
</script>
<style type="text/css">
.imgg
{
 margin-top: -13px;
  margin-left: -80px;
}
.btn
{
      color: #FFF!important;
  background-color: #00C851;
  display: inline-block;
  font-weight: 400;
  text-align: center;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
  position: relative;
  cursor: pointer;
  user-select: none;
  z-index: 1;
  font-size: .8rem;
  font-size: 15px;
  border-radius: 2px;
  border: 0;
  transition: .2s ease-out;
  white-space: normal!important;
}
.popover.left>.arrow
{
display: none !important;
}
.popover.left
{
width: 150%!important;
margin-top: 192% !important;
  margin-left: -10% !important;
  font-family: myFirstFont;
}

.navbar2
{
       margin-left: 82%;
  margin-top: -68px;
  width: 15%;
  height: 40px;
 display: none;
  z-index: 10000;
  position: fixed;
}
.btn:hover
{
    opacity: 0.5;
}
.footable.breakpoint > tbody > tr > td > span.footable-toggle {
  float: right;
  display: inline-block;
  font-family: 'footable';
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  -webkit-font-smoothing: antialiased;
  padding-right: 5px;
  font-size: 14px;
  color: #888888;
}
.footer
{
margin-top: 2%;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
 .al
{
      margin-top: 19%;
}
.navbar2 {
          display: block!important;
              margin-top: 14px!important;
              font-family: myFirstFont;
    }
    #userimage {
      margin-top: 6px;
      width: 50%;
      margin-left: 2%;
}
.navbar
{
      position: fixed;
}
#filter
{
      width: 70%;
    margin-left: 14%;
}
#keyword
{
    width: 70%;
  margin-left: 14%;
  margin-top: 15px;
}
}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) {
 .al
{
      margin-top: 19%;
}
.navbar2 {
          display: block!important;
              margin-top: 14px!important;
              font-family: myFirstFont;
    }
    #userimage {
      margin-top: 6px;
      width: 50%;
      margin-left: 2%;
}
.navbar
{
      position: fixed;
}
#filter
{
      width: 70%;
    margin-left: 14%;
}
#keyword
{
    width: 70%;
  margin-left: 14%;
  margin-top: 15px;
}

}
/*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
 .al
{
      margin-top: 19%;
}
.navbar2 {
          display: block!important;
              margin-top: 14px!important;
              font-family: myFirstFont;
    }
    #userimage {
      margin-top: 6px;
      width: 50%;
      margin-left: 2%;
}
.navbar
{
      position: fixed;
}
#filter
{
      width: 70%;
    margin-left: 14%;
}
#keyword
{
    width: 70%;
  margin-left: 14%;
  margin-top: 15px;
}


}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
 .al
{
      margin-top: 19%;
}
.navbar2 {
          display: block!important;
              margin-top: 14px!important;
              font-family: myFirstFont;
    }
    #userimage {
      margin-top: 6px;
      width: 50%;
      margin-left: 2%;
}
.navbar
{
      position: fixed;
}
#filter
{
      width: 70%;
    margin-left: 14%;
}
#keyword
{
    width: 70%;
  margin-left: 14%;
  margin-top: 15px;
}


}
@media screen and (max-height: 1200px) {
  #loo
  {
        margin-left: 36%;
  }
}
@font-face {
  font-family: myFirstFont;
  src: url(/raleway/Raleway-Regular.ttf);
}
.panel
{
  width:60%;
}
.Maindetails
{
  margin-top: 15px;
}
.empname
{
  font-size: 19px;
}
</style>
  </head>

<div id="bodyy">
<!-- title -->
  <div class="container">
            <div class="row al">
               <div class="col-sm-12" >
                  <center> <h2> Lead History</h2></center>
               </div>
            </div>

    </div>
    {{csrf_field()}}
 <input type="hidden" name="_token1" value="{{ csrf_token() }}">


<!-- <div class="col-sm-12 Maindetails" >
  <div class="col-sm-2">
     
  </div>
  <div class="col-sm-3">
      <div class="panel panel-default">
            <div class="panel-body" style="    text-align: -webkit-center;">
                  <img src="/img/provisional.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                      <h5 style="padding-top: 10px;color: #636b6f"> Lead Id </h5>
                       <h3 class="counter-count" style="color:#33cc33; margin-top: -8px;"> {{$leadid}} </h3>
            </div>
       </div>
  </div>
  <div class="col-sm-3">
      
       <div class="panel panel-default">
            <div class="panel-body" style="    text-align: -webkit-center;">
                  <img src="/img/employee-id.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                      <h5 style="padding-top: 10px;color: #636b6f"> Employee Id</h5>
                       <h3 class="counter-count" style="color:#990000; margin-top: -8px;"> {{$id}} </h3>
            </div>
       </div>
  </div>
  <div class="col-sm-3">
     <!--  <b style="color:#337ab7;"> Employee Name </b> : {{$logged_in_user}} 
       <div class="panel panel-default">
            <div class="panel-body" style="    text-align: -webkit-center;">
                  <img src="/img/employee-name.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                      <h5 style="padding-top: 10px;color: #636b6f"> Employee Name</h5>
                       <h4 class="counter-count empname" style="color:#337ab7; margin-top: -8px;"> {{$logged_in_user}} </h4>
            </div>
       </div>
  </div>
</div> -->
<div class="container" style="    margin-top: 47px;">
      <div class="row">
            <div class="col-sm-12" id="result">
                      <table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
      <tr>
        <th><strong>Created At</strong></th>
        <th data-hide="phone,tablet"><strong>Lead Id</strong></th>
        <th data-hide="phone,tablet"><strong>Employee Id</strong></th>
         <th data-hide="phone,tablet"><strong>Employee Name</strong></th>
          <th data-hide="phone,tablet"><strong>Actions</strong></th>
          <th data-hide="phone,tablet"><strong>Field Updated</strong></th>
          <th data-hide="phone,tablet"><strong>Value Updated</strong></th>
      </tr>
    </thead>
    <tbody>
    @foreach($log as $logs)
      <tr>
        <td>{{$logs->activity_time}}</td>
        <td>{{$logs->lead_Id}}</td>
        <td>{{$logs->emp_Id}}</td>
        <td>{{$logs->name}}</td>
        <td>{{$logs->activity}}</td>
         <td>{{$logs->field_updated}}</td>
          <td>{{$logs->value_updated}}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
  <div style="text-align: -webkit-center;"> {{$log->links()}} </div>
            </div>

      </div>
</div>
  {{-- @extends('layouts.footer') --}}
</div>
@endsection
