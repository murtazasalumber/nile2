<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{
    ?>

    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',    {{$statuscounts[$j][0]}} ],
            ['Converted Leads',  {{ $statuscounts[$j][1] }}],
            ['Deferred Leads', {{ $statuscounts[$j][2] }} ],
            ['Dropped Leads', {{ $statuscounts[$j][3] }}  ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
             legend: { position: "none" },
            width:280,
            height: 350,
            title: '',
             colors: ['#337ab7', '#33cc33', '#990000', '#ff1a1a'],
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };



        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>


<body>
<?php

$j= 0;
for($i=0; $i< $coords_under_vert_count; $i++)
{

    ?>

    <div class="col-sm-6" style="margin-top: 2%;text-align: center;">
        <div class="panel panel-default" >

            <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
                <h3> {{$User[$i]}}</h3>
                <div id="<?php echo $j; ?>" style="margin-left: -11px;"></div>
            </div>
        </div>

    </div>

    <!-- <div class="col-sm-6">
    <div class="panel panel-default">

    <div class="panel-body" style=" height: auto;   text-align: -webkit-center;">
    <h3> User 2</h3>
    <div id="1" style="   margin-left: -11px;"></div>
</div>
</div>

</div> -->



<?php
$j=$j+1;

}

?>
</body>