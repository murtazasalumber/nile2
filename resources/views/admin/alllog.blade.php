

<!--
@foreach($data as $da)

{{$da->name}}
@endforeach -->

<!-- If some data is present -->
@if( count($data) > 0)

<!-- Code for Name selection present here || by jatin-->

@if($filter === "name")
		 <table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
      <tr>
        <th><b> Created At</b></th>
        <th data-hide="phone,tablet"><b>Lead Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Name</b></th>
         <th data-hide="phone,tablet"><b>Actions</b></th>

         	<th data-hide="phone,tablet"><b>Field Updated</b></th>
         	<th data-hide="phone,tablet"><b>Value Updated</b></th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $da)

      <tr>
             <td>{{$da->activity_time}}</td>
        <td>{{$da->lead_Id}}</td>
        <td>{{$da->emp_Id}}</td>
        <td style="color: red;">{{$da->name}}</td>
        <td>{{$da->activity}}</td>
         <td>{{$da->field_updated}}</td>
          <td>{{$da->value_updated}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

	<!-- Code for Activity selection present here || by jatin-->

@elseif($filter === "activity")
	 <table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
      <tr>
        <th><b> Created At</b></th>
        <th data-hide="phone,tablet"><b>Lead Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Name</b></th>
         <th data-hide="phone,tablet"><b>Actions</b></th>

         	<th data-hide="phone,tablet"><b>Field Updated</b></th>
         	<th data-hide="phone,tablet"><b>Value Updated</b></th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $da)

      <tr>
             <td>{{$da->activity_time}}</td>
        <td>{{$da->lead_Id}}</td>
        <td>{{$da->emp_Id}}</td>
        <td>{{$da->name}}</td>
        <td style="color: red;">{{$da->activity}}</td>
         <td>{{$da->field_updated}}</td>
          <td>{{$da->value_updated}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

<!-- Code for Lead Id selection present here || by jatin-->
@elseif($filter === "lead_Id")
	 <table class="table footable" style="font-family: Raleway,sans-serif;">
    <thead>
      <tr>
        <th><b> Created At</b></th>
        <th data-hide="phone,tablet"><b>Lead Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Id</b></th>
        <th data-hide="phone,tablet"><b>Employee Name</b></th>
         <th data-hide="phone,tablet"><b>Actions</b></th>

         	<th data-hide="phone,tablet"><b>Field Updated</b></th>
         	<th data-hide="phone,tablet"><b>Value Updated</b></th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $da)

      <tr>
             <td>{{$da->activity_time}}</td>
        <td style="color: red;">{{$da->lead_Id}}</td>
        <td>{{$da->emp_Id}}</td>
        <td>{{$da->name}}</td>
        <td>{{$da->activity}}</td>
         <td>{{$da->field_updated}}</td>
          <td>{{$da->value_updated}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

@endif

@else
		<p> No Result Found</p>

@endif
