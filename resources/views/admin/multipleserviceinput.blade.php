<?php
$today = date("Y-m-d");
?>


 <script type="text/javascript">
    $(document).ready(function(){


    

         $("#branch{{$i}}").hide();
      $("#servicetype{{$i}}").change(function(){
          var servicetype = $('#servicetype{{$i}}').val();
          var branch = $('#branch{{$i}}').val();
          var i=<?php echo $i;?>;

          if(servicetype == ""){
            $("#branchalert{{$i}}").show();
            $("#branch{{$i}}").hide();
          }
          else if(branch == "")
          {
             $("#branchalert{{$i}}").hide();
            $("#branch{{$i}}").show();
          }else{
             $.get("{{ URL::to('testing1') }}", { servicetype :servicetype, branch :branch,i:i}, function(data){
                $('#resrult{{$i}}').html(data);
             });
            $("#assignedto{{$i}}").hide();
            $("#assignedtoalert{{$i}}").hide();
            $("#resrult{{$i}}").show();

          }
      });
      $("#branch{{$i}}").change(function(){
           var servicetype = $('#servicetype{{$i}}').val();
          var branch = $('#branch{{$i}}').val();
          var i=<?php echo $i;?>;
          if(branch == ""){
              $("#branchalert{{$i}}").show();
              $("#branch{{$i}}").hide();
          }else
          {

              $.get("{{ URL::to('testing1') }}", { servicetype :servicetype, branch :branch,i:i}, function(data){
                $('#resrult{{$i}}').html(data);

             });
            $("#assignedto{{$i}}").hide();
            $("#assignedtoalert{{$i}}").hide();
            $("#resrult{{$i}}").show();

          }
        });


   $("#submitt").click(function(event){
      // event.preventDefault();
      var assignto{{$i}} = $("#s{{$i}}").val();
      $("#assigntonew{{$i}}").val(assignto{{$i}});
      
  var prefferedlang{{$i}}=$("#preferedlanguage{{$i}}").val();
      $('#Preferredlanguages{{$i}}').val(JSON.stringify(prefferedlang{{$i}}))

    })
 

  });
  </script>



      
              <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 12px;">
                            <label>Service Type <span style="color:Red; font-size: 20px;">*</span></label>
                            <select name="servicetype{{$i}}" id="servicetype{{$i}}" required>
                              <option value="@yield('editservicetype')">@yield('editservicetype')</option>
                            @foreach($vertical as $vertical)
                            <option value="{{ $vertical->verticaltype}}">{{ $vertical->verticaltype}}</option>
                            @endforeach
                           </select>

          </div>


          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 10px;">
                            <label>Branch  <span style="color:Red; font-size: 20px;">*</span></label>
                            <select  id="branchalert{{$i}}" >
                            <option value="notseleced"> --Please Select the Servcie Type --</option>
                             </select>


                            <select name="branch{{$i}}" id="branch{{$i}}" required>
                            <option value="@yield('editbranch')">@yield('editbranch')</option>
                              @foreach($branch as $branch)
                              <option value="{{$branch->name}}">{{ $branch->name}}</option>
                              @endforeach
                             </select>

          </div>


           <div class="col-sm-3" style="    margin-top: 10px;" >

              <label>Assigned To <span style="color:Red; font-size: 20px;">*</span></label>

         {{--                   <select  id="assignedto" >
                            <option value="@yield('editassignedto')">@yield('editassignedto')</option>
            @foreach($emp as $emp1)
            <option value="{{ $emp1->FirstName}}">{{ $emp1->FirstName}}  {{ $emp1->Designation}}</option>
            @endforeach
           </select> --}}

           <select  id="assignedtoalert{{$i}}" >
                            <option value="notselecedassign')"> -- Please Select the Branch --</option>

           </select>
           <div id="resrult{{$i}}">

           </div>
          </div>




          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 21px;">
                            <label>General Condition </label>
                            <select name="GeneralCondition{{$i}}" id="GeneralCondition" >
                            <option value="@yield('editGeneralCondition')">@yield('editGeneralCondition')</option>
                              @foreach($condition as $condition)
                              <option class="rest" value="{{ $condition->conditiontypes}}">{{ $condition->conditiontypes}}</option>
                              @endforeach
                              <option class="mathrutvam" value="Pre-term">Pre-term</option>
                              <option class="mathrutvam" value="Normal">Normal</option>
                            </select>

          </div>



          <div class="col-sm-12"> </div>

          <div class="col-sm-3" style="    margin-top: 12px;">



              <label>Required On</label><input type="date"  rows="5" name="requesteddate{{$i}}" id="requesteddate" value="@yield('editRequestDateTime')" min="<?php echo $today; ?>">

          </div>


         <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Gender </label>
                            <select name="preferedgender{{$i}}" id="preferedgender" >
                            <option value="@yield('editpreferedgender')">@yield('editpreferedgender')</option>
            @foreach($gender1 as $gender1)
            <option value="{{ $gender1->gendertypes}}">{{ $gender1->gendertypes}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-6" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Language  </label><br>
                            <select name="preferedlanguage{{$i}}" id="preferedlanguage{{$i}}"  multiple="multiple">
                           <!--  <option value="@yield('editpreferedlanguage')">@yield('editpreferedlanguage')</option> -->
                            @foreach($language as $language)
            <option value="{{ $language->Languages}}">{{ $language->Languages}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-12"></div>
          <div class="col-sm-3 quotedp" style="    margin-top: 17px;">
              <label>Quoted Price </label>
              <input type="text"  rows="5" name="quotedprice{{$i}}" id="quotedprice" value="@yield('editquotedprice')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
              <p class="indiancurrency"><b>  INR </b></p>
          </div>

          <div class="col-sm-3" style="    margin-top: 17px;">
              <label>Expected Price </label><input type="text"  rows="5" name="expectedprice{{$i}}" id="expectedprice" value="@yield('editexpectedprice')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
               <p class="indiancurrency"><b>  INR </b></p>
          </div>

        


          <div class="col-sm-12"></div>
          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 18px;">
                           <div class="form-group" style="    width: 99%;
    margin-left: 0px;">
                            <label >Remarks</label>
                            <textarea class="form-control" rows="5" col="20" name="remarks{{$i}}" id="remarks" value="@yield('editremarks')">@yield('editremarks')</textarea>
                          </div>
          </div>


      <div class="col-sm-12"></div>

      <input type="text" name="assigntonew{{$i}}" id="assigntonew{{$i}}" value="">