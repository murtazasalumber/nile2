<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script>
$(document).ready(function(){



   

     //  $.get("{{ URL::to('managementverticalgraph') }}" ,function(data){
     //            $('#result1').html(data);
     //      });


    //   $("#search1").click(function(){
    //            var branch =  $('#Branch1').val();
    //       var vertical=   $('#Vertical1').val();
    //       var from=  $('#FromDate1').val();
    //       var to= $('#ToDate1').val();


         
          
    //             if(branch == "All" && vertical == "All" && from == 0 && to == 0)
            
    //         {

    //                          alert("Nothing Seleted");
    //         }else if(from != 0 && to == 0 || from == 0 && to != 0 )
    //         {
    //             alert("Please select both Date");
    //         }else 
    //         {
                
    //               if(from!=0 && to !=0)
    //             {

    //                 if( (new Date(from) < new Date(to)))
    //                 {
    //                       $.get("{{ URL::to('searchfiltermanagementmobile') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
    //             $('#main1').html(data);
    //               });  

    //                 }else 
    //                 {
    //                         alert("From Date should always be less than To Date");
    //                 } 
    //             } else 
    //             {
    //                       $.get("{{ URL::to('searchfiltermanagementmobile') }}",{ Branch: branch,Vertial : vertical,FromDate : from, ToDate: to }  ,function(data){
    //             $('#main1').html(data);
    //               });  
    //             }
    //         }
        
    // });
    

    });
</script>
<style type="text/css">
 .navbar1
{
   margin-left: 10px;
    margin-top: -76px;
    width: 12%;
    height: 50px;
  
    z-index: 1000000;
    position: fixed;
}
.navbar2
{
      margin-left: 81%;
    margin-top: -76px;
    width: 15%;
    height: 40px;
   
    z-index: 10000;
    position: fixed;
}
.popover.bottom
{
  top:66px!important;
  width: 408% !important;
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  margin-top: 216% !important;
    margin-left: -71% !important;
}

body {
    font-family: "Lato", sans-serif;
}
.footer
{
  display: none;
}
.sidenav1 {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align:center;
}

.sidenav1 a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;

}

.sidenav1 a:hover{
    color: #f1f1f1;
}

.sidenav1 .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav1 {padding-top: 15px;}
  .sidenav1 a {font-size: 18px;}
}
@media screen and (max-height:1200px) {
  #loo
  {
           text-align: center;
  }
  


}
@media only screen and (min-device-width : 360px)
{
   #s {
       width: 114%;
    margin-left: -15px;
}
}
.popover.bottom>.arrow
    {
        display: none;
    }
.popover {
   top: -40px !important;
    left: 4% !important;
}
.navbar-toggle
{
    display: none;
}
</style>
<?php

$j= 0;

// Depending on the number of verticals sent we are displaying the graphs here
for($i=0; $i< $verticals_count; $i++)
{
    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([

            ['Task', 'Rating'],
            ['New Leads',   {{$statuscounts[$j][0]}}  ],
            ['In Progress Leads', {{$statuscounts[$j][1]}}],
            ['Converted Leads', {{$statuscounts[$j][2]}}],
            ['Dropped Leads', {{$statuscounts[$j][3]}} ],
            ['Deferred Leads', {{$statuscounts[$j][4]}} ]
        ]);

        var options = {
            pieSliceText: 'value',
            chartArea: {
                right: 0,   // set this to adjust the legend width
                left: 60,     // set this eventually, to adjust the left margin
            },
            legend:'none',
            width: 280,
            height: 350,
            colors: ['#337ab7','orange','#33cc33','#ff1a1a','#990000'],
            title: '',
            pieHole: 0.4,
            sliceVisibilityThreshold:0
        };

        var chart = new google.visualization.PieChart(document.getElementById('<?php echo $j ?>'));
        chart.draw(data, options);

    }
    <?php $j= $j+1; ?>
    </script>

    <?php
}
?>
<!-- Service Leads overall satus -->
<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deferred", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
    var view = new google.visualization.DataView(data);


    var options = {
        title: "",
        width: 320,
        height: 300,
        annotations: {
            alwaysOutside: false
        },
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
    };




    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_valuesmobile"));
    chart.draw(view, options);
}
</script>

<!--   <div class="col-sm-12" style="margin-top: 20%;"> -->
  <!-- <span style="    font-size: 30px;
    cursor: pointer;
    position: fixed;
    z-index: 100000;
    margin-top: -16%;" onclick="openNav()">&#9776; </span>
    <a href="#" data-toggle="popover" title="" data-placement="bottom" data-trigger="focus" data-content="
<div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/cc/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
  </div>
  <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
  </div>

   <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/cc?name={{ Auth::user()->name }}' title='' style='color:black'>View Service Leads</a>
  </div>

    <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/product?name={{ Auth::user()->name }}' title='' style='color:black'>View Product Leads</a>
  </div>

   <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/provisionalview?name={{ Auth::user()->name }}' title='' style='color:black'>Provisional Leads</a>
  </div>

   <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/log' title='' style='color:black'>Logs</a>
  </div>
   <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/updatetables' title='' style='color:black'>Update Tables</a>
  </div>
  "><span style="     margin-top: -75px;
        margin-left: 2px;
        font-size: 30px;
        cursor: pointer;
        position: fixed;
        z-index: 10000;"  id="sidemenu"><img class="img-responsive" src="/img/dashboard.png" alt="User" style=""> </span></a>
         <div class="col-sm-12" style="    text-align: -webkit-right;">
<button type="button" style="    margin-top: -75px;
    z-index: 100000000;
    outline: none;
    background: white;
    border: none;
    position: absolute;
    margin-left: -35px;" data-toggle="collapse" data-target="#demo"><img class="img-responsive" src="/img/user _dashboard.png" alt="User" style=";" style=""></button> -->


 <!--  <div id="demo" class="collapse" >
           <div style="border-bottom: 1px solid #e9ebee;
    padding-top: 8px;
    margin-left: -29px;
    background: white;

    width: 117%;
    text-align: center;">
                {{ Auth::guard('admin')->user()->name }}
           </div>   

            <div style="border-bottom: 1px solid #e9ebee;
    padding-top: 8px;
    margin-left: -29px;
    background: white;
        margin-bottom: 13px;
    width: 117%;text-align: right;">
                 <a href="\change-password" style="    padding-right: 18px;">User Settings</a>
                                        <a href="{{ route('logout') }}" style="    padding-right: 18px;" 
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><br>
                                            Logout
                                        </a>
           </div>

  </div>
</div> -->

      
<div class="navbar1"> 
 <a href="javascript:void(0)" data-toggle="popover" title="" aria-expanded="false" data-placement="bottom" data-content="<div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/Verticalhead/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Service Lead</a>
  </div>
  <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/product/create?name={{ Auth::user()->name }}' title='' style='color:black'>Create Product Lead</a>
  </div>

   <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/cc?name={{ Auth::user()->name }}&status=All' title='' style='color:black'>View Service Leads</a>
  </div>

    <br>
  <div style='width: 121%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='/product?name={{ Auth::user()->name }}&status=All' title='' style='color:black'>View Product Leads</a>
  </div>


"><img class="img-responsive" src="/img/dashboard.png" alt="User" style="    margin-top: 32px;"> </a>

<!--  <a href='#' title='' style='color:black' data-toggle='modal' data-target='#myModal1' id='filters'>
  <img src="/img/filter.png" class="img-responsive" alt="add" style="    margin-top: -30px;
    margin-left: 86%;"> </a> -->
</div>
<div class="navbar2">
 <a href="javascript:void(0)" data-toggle="popover" title="" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\change-password' title='' style='color:black'>User Settings</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='{{ route('logout') }}' title='' style='color:black'>Logout</a>
  <a href='{{ route('logout') }}'  
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();''Logout
                                        </a>
  </div>"><img class="img-responsive" src="/img/user _dashboard.png" alt="User" style="    padding-left: 15px;
    padding-top: 34px;"></a>
</div>
  <div id="mySidenav1" class="sidenav" style="background: white;margin-top: 17%;text-align: center;">
  <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Service  Lead</a>
   <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Create Product  Lead</a>
    <a href="/cc?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">View Service Leads</a>
  <a href="/cc?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">View Product Leads</a>
  <a href="/provisionalview?name={{ Auth::guard('admin')->user()->name }}" style="border-bottom: 1px solid #bfbfbf;">Provisional Leads</a>
  <a href="/log" style="border-bottom: 1px solid #bfbfbf;">Logs</a>
  <a href="/updatetables" style="border-bottom: 1px solid #bfbfbf;">Update the Tables</a> -->

</div>



<!-- <script>
function openNav() {
    document.getElementById("mySidenav1").style.width = "100%";
}

function closeNav() {
    document.getElementById("mySidenav1").style.width = "0";
}
</script> -->
<div id="main1">
<div style="width: 100%;height: 5px;"></div>
  <div class="col-sm-12">

             <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                           
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                        <h3 style="color: #33cc33;">
               Welcome !!!</h3> <p> You are logged in as Branch Head.</p>
                                </div>
                            
                        </div>
                    </div>


     <div class="col-sm-2 col-sm-offset-1" style="margin-top: 10px;">
                        <div class="panel panel-default">

                    <a href="/BranchHeadassign?name={{ Auth::guard('admin')->user()->name }}&status=New">
                        <div class="panel-body" style="    text-align: -webkit-center;">
                            <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                            <h5 style="padding-top: 10px;color: #636b6f"> New Leads </h5>
                            <h3 style="color:#337ab7; margin-top: -8px;">  {{$newcountforall}}</h3>
                        </div>

                    </a>
                </div>

                </div>

                <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/BranchHeadassign?name={{ Auth::user()->name }}&status=In%20Progress">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/Progress Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  In Progress Leads </h5>
                                    <h3 style="color:orange; margin-top: -8px;"> {{ $inprogresscountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/BranchHeadassign?name={{ Auth::user()->name }}&status=Converted">

                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/Converted Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Converted Leads </h5>
                                    <h3 style="color:#33cc33; margin-top: -8px;"> {{ $convertedcountforall}} </h3>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-2 " style="margin-top: 10px;">
                        <div class="panel panel-default">
                            <a href="/BranchHeadassign?name={{ Auth::user()->name }}&status=Deferred">
                                <div class="panel-body" style="    text-align: -webkit-center;">
                                    <img src="/img/Deffered Lead.png" class="img-responsive" alt="add" style="    padding-top: 8px;width: 34px;">
                                    <h5 style="padding-top: 10px;color: #636b6f">  Deferred Leads </h5>
                                    <h3 style="color:#990000; margin-top: -8px;"> {{ $deferredcountforall }} </h3>
                                </div>
                            </a>
                        </div>
                    </div>



                    <div class="col-sm-2" style="margin-top: 10px;">
                        <div class="panel panel-default" style="    width: 101%;
                        margin-left: -5px;">
                        <a href="/BranchHeadassign?name={{ Auth::user()->name }}&status=Dropped">
                            <div class="panel-body" style="    text-align: -webkit-center;">
                                <img src="/img/Dropped.png" class="img-responsive" alt="add" style="    padding-top: 8px; width: 34px;">
                                <h5 style="padding-top: 10px;color: #636b6f">  Dropped Leads </h5>
                                <h3 style="color:#ff1a1a; margin-top: -8px;"> {{$droppedcountforall}} </h3>
                            </div>
                        </a>
                    </div>
                </div>
          
      	


                                    <div class="col-sm-10 col-sm-offset-1" style="margin-top:1%;    ">
                                        <div class="panel panel-default" style="margin-right: 6px;
                                        margin-left: -4px;">

                                        <div class="panel-body" style="height: auto;">
                                            <h3> Overall Status </h3>
                                            <div id="columnchart_valuesmobile"></div>

                                        </div>
                                    </div>
                                </div>

        
							 
                                    <?php

                                    $j= 0;
                                    // Depending on the number of verticals we are displaying their names
                                    for($i=0; $i< $verticals_count; $i++)
                                    {

                                        ?>

                                        <div class="col-sm-6 " style="margin-top: 3%;">
                                            <div class="panel panel-default" >

                                            <div class="panel-body" style="height: auto;">
                                                <h4> {{$servicetype_name[$j]}} : {{$city_name[$j]}} </h4>
                                                <div id="<?php echo $j; ?>" style="   margin-left: -11%;"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $j=$j+1;

                                }

                                ?>

                            </div>
                                           
      
   
<div class="col-sm-12" id="result"></div>
<div class="col-sm-12" id="result1"></div>
        
    </div>

  </div>

<!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" id="modalpopup" style="margin-top: 30%;">

        <div class="modal-body" style="min-height: 238px;">
                 <div class="col-sm-6">
                    <center> <p> <b> Branch : </b> </p>
                     <select name="Branch"  id="Branch1" value="" style="width: 80%;" >
                         <option value="All">All</option>
                                 @foreach($data as $city)
                              <option value="{{ $city->name}}">{{ $city->name}}</option>
                              @endforeach
                            </select>
                            </center>
                </div>
                <div class="col-sm-6" style="margin-top: 4%;">
                            <center> <p> <b> Vertical: </b> </p>
                            <select name="Vertical"  id="Vertical1" value="" style="width: 80%;" >
                                    <option value="All">All</option>
                                    <option value="NursingServices">Nursing Services</option>
                                    <option value="PersonalSupportiveCare">Personal Supportive Care</option>
                                    <option value="Mathrutvam-Baby Care">Mathrutvam - Baby Care</option>
                                    <option value="Physiotherapy-Home">Physiotherapy - Home</option>
                            </select>
                            </center>
                </div>
                <div class="col-sm-12">
                <br>
                            <center> <p> <b> TimeLine: </b> </p>  </center>

                            <div class="col-sm-5">
                                    <input type="date" name="FromDate" id="FromDate1" class="custominput" style="width: 90%;">
                          
                            </div>
                            <div class="col-sm-2" ><center style="    padding-top: 9px;"> To </center></div>
                            <div class="col-sm-5">
                                    <input type="date" name="ToDate" id="ToDate1" class="custominput" style="width: 89%;">
                            </div>
                </div>

                 <div class="col-sm-12" style="text-align: -webkit-right;">
                <br>
                           <button type="button" class="btn btn-default" style="background: #00C851;    margin-right: 16px;" id="search1" data-dismiss="modal">Apply Filter</button>
                </div>
        </div>

      </div>

    </div>
  </div>


  <script type="text/javascript">
  $('[data-toggle="popover"]').popover({html:true}); 

</script>

