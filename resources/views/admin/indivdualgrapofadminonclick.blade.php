
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      
var data = google.visualization.arrayToDataTable([
        ["Element", "Leads", { role: "style" },],
        ["New Lead", {{$newcountforall}}, "#337ab7"],
        ["In Progress", {{ $inprogresscountforall}}, "orange"],
        ["Converted", {{ $convertedcountforall}}, "#33cc33"],
        ["Deffered", {{ $deferredcountforall }}, "#990000"],
        ["Dropped", {{$droppedcountforall}}, "#ff1a1a"],

    ]);
var view = new google.visualization.DataView(data);
     

      var options = {
        title: "",
        width: 250,
        height:250,
annotations: {
    alwaysOutside: false
},
        bar: {groupWidth: "55%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values45"));
      chart.draw(view, options);
  }

  $(window).resize(function(){
  drawChart();
  
});
  </script>
<style type="text/css">
  
.column
{
   width: 100%;
    margin-left: -1%;
}
</style>
<body>
	
		 <div class="col-sm-12 " >
                    <div class="panel panel-default column" >

                    <div class="panel-body" style="height: auto;text-align: -webkit-center;">
                      <h3>Overall Status</h3>
                             <div id="columnchart_values45" ></div>

                    </div>
                </div>
            </div>

</body>