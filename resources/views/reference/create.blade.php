@extends('layout.app')

@section('Reference', $Reference)
@section('ReferalType', $ReferalType)
@section('ContactPerson', $ContactPerson)
@section('ReferenceAddress', $ReferenceAddress)
@section('ReferenceContact', $ReferenceContact)
@section('ReferenceEmail', $ReferenceEmail)

@section('body')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
     <link rel="shortcut icon" href="{{{ asset('img/favicon-96x96.png') }}}">
  
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Scripts -->
  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
  ]) !!};

  </script>

  <!-- JS Code for adding pop up comment starts here || by jatin -->
  <script>

  $(document).ready(function()
  {

    var value1=$('#ReferalType').val();
    if(value1 == "Institute")
    {
     $("#remark1").show();
    }else
      {
        $("#remark1").hide();
      }

    $("#ReferalType").change(function()
    {
      var value= $("#ReferalType").val();

      if(value == "Institute")
      {
        $("#remark1").show();
      }else
      {
        $("#remark1").hide();
      }
    });
     $('[data-toggle="popover"]').popover({ animation:true,  html:true});
     userr=0;
 $("#usersetting").click(function(){
      userr=userr+1;
        if(userr % 2 == 0)
        {

           $("#usersetting").popover('hide');

        }else
        {
           $("#usersetting").popover('show');

          //console.log(sidenvavv);
        }
    });
 $("#bodyy").click(function(){
      userr=userr+1;
       if(userr % 2 != 0)
       {
        userr=userr+1;
       }
    });
  });


  </script>

  <!-- JS Code for adding pop up comment ends here || by jatin -->
  <style type="text/css">
  body
  {
    background:white;
  }
  @media only screen and (max-width: 1200px) {
    .imgg
    {
      padding-left: 2px;

    }
    .navbar-header
    {
      padding-left: 17px;
    }
    #logo
    {
      margin-left: 36%;
    }
    .navbar3
    {

    }
  }
  body::-webkit-scrollbar
  {
    display: none;
  }

  body
  {
    overflow-y: scroll;
    overflow-x: hidden;
    font-family: Raleway,sans-serif;
  }

  @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
  }

  .imgg
{
   margin-top: -13px;
    margin-left: -70px;
    max-width: 167px;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
  .navbar {
    position: relative;
    min-height: 64px;
    margin-bottom: 20px;
    border: 1px solid transparent;
  }

  .navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
  }
  li
  {
    font-family: myFirstFont;
    font-size: 14px;
    color: #777;
    padding-top: 14px;
    padding-bottom: 14px;
    line-height: 22px;
  }
  input
  {
    border-right: 0px;
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px;
    border-bottom:1px solid #484e51;
    padding-bottom: 0px;
    width:100%;
    font-family: myFirstFont;
    outline: none;
  }
  label
  {
    color: #777;
  }

select
{
   border-bottom-color:#484e51;
      border-top: 0px;
   font-family: myFirstFont;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
option
{
    font-family: myFirstFont;
}

.footer {

  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
  color: #636b6f;
}
.navbar2
{
  background: white;
     height: 37px;
}

  </style>

  <style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    right: 0;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
        margin-left: -40px;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}


.navbar-right
{
   text-align: -webkit-right;
}
.popover.left>.arrow
{
  display: none !important;
}
.popover.left
{
  width: 150%!important;
  margin-top: 192% !important;
    margin-left: -10% !important;
    font-family: myFirstFont;
}
.navbar-right
{
   text-align: -webkit-right;
}
.navbar2
{
         margin-left: 82%;
    margin-top: -68px;
    width: 15%;
    height: 40px;
   display: none;
    z-index: 10000;
    position: fixed;
}
.navbar-default {
    border-bottom: 1px solid #e7e7e7;
    background-color: white;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3)
{
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
        margin-top: 77px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top: 9%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
      #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }

}
/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) {
  .submit
  {
        margin-top: 30px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
       margin-top: 77px;
  }
  #logo
  {
        margin-left: 27%;
  }
  .al
  {
        margin-top: 9%;
  }
 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }

 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }

  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 109%;
    margin-left: -14px;
  }

  }
   /*iPhone 5:*/
@media screen and (device-aspect-ratio: 40/71) {
  .submit
  {
        margin-top: 22px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
       margin-top: 77px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
 #logo
  {
        margin-left: 27%;
  }

 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
 .panel
  {
        width: 110%;
    margin-left: -14px;
  }

}
/*iphone6*/
@media screen and (device-aspect-ratio: 375/667) {
  .submit
  {
        margin-top: 25px!important;
  }
  .accepting
  {
    width: 95%;
    margin-left: 7px;
       margin-top: 77px;
  }
  .al
  {
        margin-top: 19%;
  }
 .navbar2 {
            display: block!important;
                margin-top: 14px!important;
                font-family: myFirstFont;
      }
     #logo
  {
        margin-left: 27%;
  }

 #userimage {
        margin-top: -15px;
        width: 50%;
        margin-left: 2%;
  }
  .navbar
  {
        position: fixed;
  }
  .panel
  {
        width: 109%;
    margin-left: -14px;
  }
  }
</style>
</head>


<br>
<div class="navbar navbar-default navbar-fixed-top" style="background-color: white;height: 77px;">
    <div class="container">

        <div class="navbar-header">
            <!-- <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <div id="logo">
            <a class="navbar-brand" rel="home" href="/admin/home">
                <img class="imgg"
                     src="/img/healthheal_logo.png">
            </a>
            </div>
        </div>

         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (!Auth::guard('admin')->check())
                            <li><a href="\admin">Login</a></li>


                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-content">

                                   <a href="\references">Back</a>

                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </ul>


                            </li>
                        @endif
                    </ul>
                </div>

    </div>
</div>

<!-- Update tabel view ends -->
<div class="navbar2">
 <a href="javascript:void(0);" data-toggle="popover" title="" id="usersetting" data-trigger="focus" data-placement="left" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\references' title='' style='color:black'>Back</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>




  </div>">

   <form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"> </a>
</div>
<div id="bodyy">

@include('partial.message')
<div class="container" style="margin-top: 13px;">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 accepting">
      <div class="panel panel-default">
        <div class="panel-heading" style=" text-align: center;   background-color: #f5f5f5;
    border-color: #ddd;">
          <h4 style="color:#636b6f;">   Add Referal Partner </h4>

        </div>
        <div class="panel-body">
          <form class="form-horizontal" action="/references" method="POST">
            {{csrf_field()}}
            @section('editMethod')
            @show
            <fieldset>
              <div class="col-sm-8 col-sm-offset-2">

                <label>Name</label>
                <input type="text"  rows="5" name="Reference" id="Reference" value="@yield('Reference')" required><br><br><br>

                <!-- Code for adding pop up comment starts here || by jatin -->
                <!-- When the user selects Type as 'Individual' the normal form is shown but when 'Institute' is selected show the comment input textbox -->
                 <label >Type </label>
                <br>
                <select name="ReferalType" id="ReferalType"  value="@yield('ReferalType')" style="width:100%;">
                  <option value="@yield('ReferalType')">@yield('ReferalType')</option>
                  <option value="Individual">Individual</option>
                  <option value="Institute">Institute</option>
                </select>
               <br><br><br>
                <div id="remark1">

                  <label >Contact Person</label>
                  <input type="text"  rows="5" name="ContactPerson"  value="@yield('ContactPerson')"><br><br><br>

                </div>
                  <!-- Code for adding pop up comment ends here || by jatin -->

                <label>Address</label>
                <input type="text"  rows="5" name="ReferenceAddress" id="ReferenceAddress" value="@yield('ReferenceAddress')" ><br><br><br>
                <label>Contact</label>
                <input type="text"  rows="5" name="ReferenceContact" id="ReferenceContact" value="@yield('ReferenceContact')" ><br><br><br>
                <label>Email</label>
                <input type="text"  rows="5" name="ReferenceEmail" id="ReferenceEmail" value="@yield('ReferenceEmail')" ><br>
               <!--  <button type="submit" class="btn btn-default" style="        margin-top: 30px;">Submit</button> -->

<!--
                <button type="reset" class="btn btn-default" style="float: left;margin-top: 9%;  background-color: #ff3547;" >Reset</button>
                                <button type="submit" class="btn btn-default" style="float: right;  background: #00e25b;      margin-top: 30px;">Submit</button> -->
                                 <button type="reset" class="btn btn-default" style="float: left;margin-top: 9%;  background-color: #ff3547;margin-left: 24%;" >Reset</button>&emsp;
                                <button type="submit" class="btn btn-default submit" style="  background: #00e25b;      margin-top: 30px;">Submit</button>
              </fieldset>
            </form>

          </div>

        </div>
      </div>

    </div>

  </div>
  </div>


  @include('partial.errors')
  @endsection
