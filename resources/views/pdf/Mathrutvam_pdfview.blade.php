<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Health Heal</title>


    <style media="screen">

    .imgg {
        margin-top: -13px;
        margin-left: -80px;
    }

    .border
    {
        border: 1px solid black;
    }
    </style>
</head>
<body>

    <div>
        <img class="imgg"
        src="/img/healthheal_logo.png">
    </div>

    <div id="title">
        <h1 style="text-align:center;"> Assessment Form</h2>
        </div>

        <br>
        @foreach($leaddata as $lead)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Lead ID : </b> {{$lead->id}}
                <b >Created At : </b> {{$lead->created_at}}
                <b >Created By : </b> {{$lead->createdby}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Client First Name : </b> {{$lead->fName}}
                <b>Client Middle Name : </b> {{$lead->mName}}
                <b>Client Last Name : </b> {{$lead->lName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Client Mobile Number : </b> {{$lead->Country_Code}}{{$lead->MobileNumber}}
                <b>Email Id:</b> {{$lead->EmailId}}
                <b>Source :</b> {{$lead->Source}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Type : </b> {{$lead->ServiceType}}
                <b>Lead Type : </b> {{$lead->LeadType}}
                <b>Service Status : </b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Alternate number:</b> {{$lead->Alternatenumber}}
                <b>Patient Name:</b> {{$lead->PtfName}}
                <b>Age:</b> {{$lead->PtfName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="    margin-top: 11px;">
                <b>Gender:</b> {{$lead->Gender}}
                <b>Relationship:</b> {{$lead->Relationship}}
                <b>Aadhar number:</b> {{$lead->AadharNum}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>General Condition:</b> {{$lead->GeneralCondition}}
                <b>Branch:</b>  {{$lead->Branch}}
                <b>Requested Date:</b> {{$lead->RequestDateTime}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Assigned to:</b>  {{$lead->AssignedTo}}
                <b>Quoted Price:</b> {{$lead->QuotedPrice}}
                <b>Expected Price:</b> {{$lead->ExpectedPrice}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Status:</b> {{$lead->ServiceStatus}}
                <b>Gender Prefered:</b>  {{$lead->PreferedGender}}
                <b>Prefered Languages:</b>  {{$lead->PreferedLanguage}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px">
                <b>Address:</b> &nbsp;&nbsp; {{$lead->Address1}} {{$lead->Address2}} {{$lead->City}} {{$lead->District}} {{$lead->State}} {{$lead->PinCode}}
                <b>Remarks:</b> {{$lead->Remarks}}
                <b>Lead Status:</b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
        </div>
        @endforeach

        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">General Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($assessmentdata as $assessment)
        <div>
            <div  style="margin-top: 11px;">
                <b>Assessor:</b> {{$assessment->Assessor}}
                <b>Assessment Date:</b> {{$assessment->AssessDateTime}}
                <b>Assessment Place:</b> {{$assessment->AssessPlace}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Service Start Date:</b> {{$assessment->ServiceStartDate}}
                <b>Service Pause:</b> {{$assessment->ServicePause}}
                <b>Service End Date:</b> {{$assessment->ServiceEndDate}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="margin-top: 11px;">
                <b>Shift Preference:</b> {{$assessment->ShiftPreference}}
                <b>Specific Requirements:</b>{{$assessment->SpecificRequirements}}
                <b>Days Worked:</b> {{$assessment->DaysWorked}}
                <b>Latitude:</b> {{$assessment->Latitude}}
                <b>Longitude:</b> {{$assessment->Longitude}}
            </div>
        </div>

        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">Mother Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($motherdata as $mother)
        <div >
            <div  style="margin-top: 11px; ">
                <b>First Name : </b> {{$mother->FName}}
                <b >Last Name : </b> {{$mother->MName}}
                <b >Last Name : </b> {{$mother->LName}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Medications : </b> {{$mother->Medications}}
                <b>Medical Diagnosis : </b> {{$mother->MedicalDiagnosis}}
                <b>Delivery Place : </b> {{$mother->DeliveryPlace}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Due Date : </b> {{$mother->DueDate}}
                <b>Delivery Type:</b> {{$mother->DeliveryType}}
                <b>No. of Babies :</b> {{$mother->NoOfBabies}}
                <b>No. of attender required :</b> {{$mother->NoOfAttenderRequired}}
            </div>
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">Baby Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($mathrutvamdata as $mathrutvam)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Child DOB : </b> {{$mathrutvam->ChildDOB}}
                <b >Immunization Updated : </b> {{$mathrutvam->ImmunizationUpdated}}
                <b >Birth Weight : </b> {{$mathrutvam->BirthWeight}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Discharge Weight : </b> {{$mathrutvam->DischargeWeight}}
                <b>Child Medical Diagnosis : </b> {{$mathrutvam->ChildMedicalDiagnosis}}
                <b>Child Medications : </b> {{$mathrutvam->ChildMedications}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Feeding Formula : </b> {{$mathrutvam->FeedingFormula}}
                <b>Feeding Issue:</b> {{$mathrutvam->FeedingIssue}}
                <b>Feeding Issue :</b> {{$mathrutvam->FeedingType}}
                <b>Feeding Established :</b> {{$mathrutvam->FeedingEstablished}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Medical Conditions : </b> {{$mathrutvam->FeedingFormula}}
                <b>Length:</b> {{$mathrutvam->Length}}
                <b>Head Circumference :</b> {{$mathrutvam->HeadCircumference}}
                <b>Sleeping Pattern :</b> {{$mathrutvam->SleepingPattern}}
            </div>
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">


        <label><h2 style="text-align:center;">Memory Intacts </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($memoryintactsdata as $memoryintact)
        <div >
            <label ><b>Short Term Type </b> </label> {{$memoryintact->LongTerm}}
            <label ><b>Long Term Type </b> </label> {{$memoryintact->ShortTerm}}

        </div>
        @endforeach

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Respiratory </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($respiratorydata as $respiratory)
        <div >
            <div  style="margin-top: 11px; ">
                <b>SOB : </b> {{$respiratory->SOB}}
                <b >SPO2 : </b> {{$respiratory->SPO2}}
                <b >H/O TB/Asthma/COPD : </b> {{$respiratory->H_OTB_Asthma_COPD}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Cough : </b> {{$respiratory->Cough}}
                <b>Color of Phlegm : </b> {{$respiratory->ColorOfPhlegm}}
                <b>Nebulization : </b> {{$respiratory->Nebulization}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Tracheostomy : </b> {{$respiratory->Tracheostomy}}
                <b>CPAP_BIPAP:</b> {{$respiratory->CPAP_BIPAP}}
                <b>ICD :</b> {{$respiratory->ICD}}
                <b>Central Cynaosis :</b> {{$respiratory->central_cynaosis}}
            </div>
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;">Vital Signs</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;">Vital Signs</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($vitalsignsdata as $vitalsign)

        <div  style="margin-top: 15px;">

            <label><b>BP (in mm/Hg):</b></label> {{$vitalsign->BP}}
            <label ><b>Measured by:  </b></label>  {{$vitalsign->BP_equipment}}
            <label ><b>Taken from: </b></label> {{$vitalsign->BP_taken_from}}

        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 15px;">

            <label ><b>RR (per minute):</b></label> {{$vitalsign->RR}}
            <label><b>Temperature (in C/F): </b></label> {{$vitalsign->Temperature}}
            <label><b>TemperatureType:</b></label>  {{$vitalsign->TemperatureType}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 15px;">

            <label ><b>Pulse (in minutes): </b> </label> {{$vitalsign->Pulse}}
            @endforeach


            <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

            <label><h2 style="text-align:center;">Nutrition </h2></label>
            <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

            @foreach($nutritiondata as $nutrition)
            <div >
                <label ><b>Type: </b> </label> {{$nutrition->Type}}
                <label ><b>Adequacy: </b> </label> {{$nutrition->Intact}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            @endforeach

            <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

            <label><h2 style="text-align:center;">Diet </h2></label>
            <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

            @foreach($nutritiondata as $nutrition)

            <div >
                <label ><b>Diet Type: </b> </label> {{ $nutrition->Diet}}
                <label ><b>Break Fast Time: </b> </label> {{$nutrition->BFTime}}
                <label ><b>Lunch Time: </b> </label> {{$nutrition->LunchTime}}
            </div>

            <div class="">
                <br>
            </div>

            <div >
                <label ><b>Snacks Time: </b> </label> {{$nutrition->SnacksTime}}
                <label ><b>Dinner Time: </b> </label> {{$nutrition->DinnerTime}}
                <label ><b>TPN: </b> </label> {{$nutrition->TPN}}
            </div>

            <div class="">
                <br>
            </div>

            <div >
                <label ><b>RT Feeding (in ml): </b> </label>{{$nutrition->RTFeeding}}
                <label ><b>PEG Feeding (in ml): </b> </label> {{$nutrition->PEGFeeding}}
            </div>
            @endforeach
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>


        </body>
        </html>
