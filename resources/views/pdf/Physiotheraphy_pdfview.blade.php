<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Health Heal</title>


    <style media="screen">

    .imgg {
        margin-top: -13px;
        margin-left: -80px;
    }

    .border
    {
        border: 1px solid black;
    }
    </style>
</head>
<body>

    <div>
        <img class="imgg"
        src="/img/healthheal_logo.png">
    </div>

    <div id="title">
        <h1 style="text-align:center;"> Assessment Form</h2>
        </div>

        <br>
        @foreach($leaddata as $lead)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Lead ID : </b> {{$lead->id}}
                <b >Created At : </b> {{$lead->created_at}}
                <b >Created By : </b> {{$lead->createdby}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Client First Name : </b> {{$lead->fName}}
                <b>Client Middle Name : </b> {{$lead->mName}}
                <b>Client Last Name : </b> {{$lead->lName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Client Mobile Number : </b> {{$lead->Country_Code}}{{$lead->MobileNumber}}
                <b>Email Id:</b> {{$lead->EmailId}}
                <b>Source :</b> {{$lead->Source}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Type : </b> {{$lead->ServiceType}}
                <b>Lead Type : </b> {{$lead->LeadType}}
                <b>Service Status : </b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Alternate number:</b> {{$lead->Alternatenumber}}
                <b>Patient Name:</b> {{$lead->PtfName}}
                <b>Age:</b> {{$lead->PtfName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="    margin-top: 11px;">
                <b>Gender:</b> {{$lead->Gender}}
                <b>Relationship:</b> {{$lead->Relationship}}
                <b>Aadhar number:</b> {{$lead->AadharNum}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>General Condition:</b> {{$lead->GeneralCondition}}
                <b>Branch:</b>  {{$lead->Branch}}
                <b>Requested Date:</b> {{$lead->RequestDateTime}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Assigned to:</b>  {{$lead->AssignedTo}}
                <b>Quoted Price:</b> {{$lead->QuotedPrice}}
                <b>Expected Price:</b> {{$lead->ExpectedPrice}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Status:</b> {{$lead->ServiceStatus}}
                <b>Gender Prefered:</b>  {{$lead->PreferedGender}}
                <b>Prefered Languages:</b>  {{$lead->PreferedLanguage}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px">
                <b>Address:</b> &nbsp;&nbsp; {{$lead->Address1}} {{$lead->Address2}} {{$lead->City}} {{$lead->District}} {{$lead->State}} {{$lead->PinCode}}
                <b>Remarks:</b> {{$lead->Remarks}}
                <b>Lead Status:</b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
        </div>
        @endforeach

        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">General Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($assessmentdata as $assessment)
        <div>
            <div  style="margin-top: 11px;">
                <b>Assessor:</b> {{$assessment->Assessor}}
                <b>Assessment Date:</b> {{$assessment->AssessDateTime}}
                <b>Assessment Place:</b> {{$assessment->AssessPlace}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Service Start Date:</b> {{$assessment->ServiceStartDate}}
                <b>Service Pause:</b> {{$assessment->ServicePause}}
                <b>Service End Date:</b> {{$assessment->ServiceEndDate}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="margin-top: 11px;">
                <b>Shift Preference:</b> {{$assessment->ShiftPreference}}
                <b>Specific Requirements:</b>{{$assessment->SpecificRequirements}}
                <b>Days Worked:</b> {{$assessment->DaysWorked}}
                <b>Latitude:</b> {{$assessment->Latitude}}
                <b>Longitude:</b> {{$assessment->Longitude}}
            </div>
        </div>

        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">Physiotherapy Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($physiotheraphydata as $physiotheraphy)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Chief Complains : </b> {{$physiotheraphy->chiefcomplains}}
                <b >History of Presenting Complaints : </b> {{$physiotheraphy->ho_pc}}
                <b >Any previous history/surgical/physiotherapy treatment : </b> {{$physiotheraphy->previousmedical}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Occupational History : </b> {{$physiotheraphy->occupationalH}}
                <b>Environment History : </b> {{$physiotheraphy->envhistory}}
                <b>Physiotheraphy Type : </b> {{$physiotheraphy->PhysiotheraphyType}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Metal Implant : </b> {{$physiotheraphy->MetalImplant}}
                <b>Hypertension:</b> {{$physiotheraphy->Hypertension}}
                <b>Medications :</b> {{$physiotheraphy->Medications}}
                <b>Pregnant or Breast Feeding:</b> {{$physiotheraphy->PregnantOrBreastFeeding}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Diabetes : </b> {{$physiotheraphy->Diabetes}}
                <b>Chronic Infection:</b> {{$physiotheraphy->ChronicInfection}}
                <b>Heart Disease :</b> {{$physiotheraphy->HeartDisease}}
                <b>Epilepsy:</b> {{$physiotheraphy->Epilepsy}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Past/Present Surgery Details : </b> {{$physiotheraphy->SurgeryUndergone}}
                <b>Assessment Date:</b> {{$physiotheraphy->AssesmentDate}}
                <b>Short Term Goal :</b> {{$physiotheraphy->ShortTermGoal}}
                <b>Long Term Goal:</b> {{$physiotheraphy->LongTermGoal}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Medical Diagnosis : </b> {{$physiotheraphy->MedicalDisgnosis}}
                <b>Physiotherapeutic Diagnosis:</b> {{$physiotheraphy->PhysiotherapeuticDiagnosis}}
                <b>Affected Area :</b> {{$physiotheraphy->AffectedArea}}

                @if($physiotheraphy->AffectedArea != NULL)
                <b>Comment :</b> {{$physiotheraphy->affectedq}}
                @endif
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Pain Pattern : </b> {{$physiotheraphy->PainPattern}}
                @if($physiotheraphy->PainPattern != NULL)
                <b>Comment :</b> {{$physiotheraphy->painq}}
                @endif

                <b>Examination Report:</b> {{$physiotheraphy->ExaminationReport}}
                @if($physiotheraphy->ExaminationReport != NULL)
                <b>Comment :</b> {{$physiotheraphy->examinationq}}
                @endif

                <b>Lab Or Radiological Report :</b> {{$physiotheraphy->LabOrRadiologicalReport}}

                @if($physiotheraphy->LabOrRadiologicalReport != NULL)
                <b>Comment :</b> {{$physiotheraphy->labq}}
                @endif
            </div>

        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">Baby Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($physioreportdata as $physioreport)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Problem Identified : </b> {{$physioreport->ProblemIdentified}}
                <b >Treatment : </b> {{$physioreport->Treatment}}
            </div>

        </div>
        @endforeach

        </body>
        </html>
