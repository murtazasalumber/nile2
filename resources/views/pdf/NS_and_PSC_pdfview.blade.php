<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Health Heal</title>


    <style media="screen">

    .imgg {
        margin-top: -13px;
        margin-left: -80px;
    }

    .border
    {
        border: 1px solid black;
    }
    </style>
</head>
<body>



    <div>
        <img class="imgg"
        src="/img/healthheal_logo.png">
    </div>

    <div id="title">
        <h1 style="text-align:center;"> Assessment Form</h2>
        </div>

        <br>
        @foreach($leaddata as $lead)
        <div >
            <div  style="margin-top: 11px; ">
                <b>Lead ID : </b> {{$lead->id}}
                <b >Created At : </b> {{$lead->created_at}}
                <b >Created By : </b> {{$lead->createdby}}
            </div>

            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px;">
                <b>Client First Name : </b> {{$lead->fName}}
                <b>Client Middle Name : </b> {{$lead->mName}}
                <b>Client Last Name : </b> {{$lead->lName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Client Mobile Number : </b> {{$lead->Country_Code}}{{$lead->MobileNumber}}
                <b>Email Id:</b> {{$lead->EmailId}}
                <b>Source :</b> {{$lead->Source}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Type : </b> {{$lead->ServiceType}}
                <b>Lead Type : </b> {{$lead->LeadType}}
                <b>Service Status : </b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Alternate number:</b> {{$lead->Alternatenumber}}
                <b>Patient Name:</b> {{$lead->PtfName}}
                <b>Age:</b> {{$lead->PtfName}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>

            <div  style="    margin-top: 11px;">
                <b>Gender:</b> {{$lead->Gender}}
                <b>Relationship:</b> {{$lead->Relationship}}
                <b>Aadhar number:</b> {{$lead->AadharNum}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>General Condition:</b> {{$lead->GeneralCondition}}
                <b>Branch:</b>  {{$lead->Branch}}
                <b>Requested Date:</b> {{$lead->RequestDateTime}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Assigned to:</b>  {{$lead->AssignedTo}}
                <b>Quoted Price:</b> {{$lead->QuotedPrice}}
                <b>Expected Price:</b> {{$lead->ExpectedPrice}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="    margin-top: 11px;">
                <b>Service Status:</b> {{$lead->ServiceStatus}}
                <b>Gender Prefered:</b>  {{$lead->PreferedGender}}
                <b>Prefered Languages:</b>  {{$lead->PreferedLanguage}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
            <div  style="margin-top: 11px">
                <b>Address:</b> &nbsp;&nbsp; {{$lead->Address1}} {{$lead->Address2}} {{$lead->City}} {{$lead->District}} {{$lead->State}} {{$lead->PinCode}}
                <b>Remarks:</b> {{$lead->Remarks}}
                <b>Lead Status:</b> {{$lead->ServiceStatus}}
            </div>
            <div  style="margin-top: 11px;">
                <br>
            </div>
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        @foreach($assessmentdata as $assessment)
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label for=""><h2 style="text-align:center">General Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div  style="margin-top: 11px;">
            <b>Assessor:</b> {{$assessment->Assessor}}
            <b>Assessment Date:</b> {{$assessment->AssessDateTime}}
            <b>Assessment Place:</b> {{$assessment->AssessPlace}}
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <b>Service Start Date:</b> {{$assessment->ServiceStartDate}}
            <b>Service Pause:</b> {{$assessment->ServicePause}}
            <b>Service End Date:</b> {{$assessment->ServiceEndDate}}
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <b>Shift Preference:</b> {{$assessment->ShiftPreference}}
            <b>Specific Requirements:</b>{{$assessment->SpecificRequirements}}
            <b>Days Worked:</b> {{$assessment->DaysWorked}}
            <b>Latitude:</b> {{$assessment->Latitude}}
            <b>Longitude:</b> {{$assessment->Longitude}}
        </div>

        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;">Assessment Details</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($vitalsignsdata as $vitalsign)
        <div  style="margin-top: 18px;">
            <b >Medical Diagnosis:</b> {{$vitalsign->MedicalDiagnosis}}
            <b >Chief reasons for seeking home healthcare:</b>{{ $vitalsign->q1 }}
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;">Vital Signs</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div  style="margin-top: 15px;">

            <label><b>BP (in mm/Hg):</b></label> {{$vitalsign->BP}}
            <label ><b>Measured by:  </b></label>  {{$vitalsign->BP_equipment}}
            <label ><b>Taken from: </b></label> {{$vitalsign->BP_taken_from}}

        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 15px;">

            <label ><b>RR (per minute):</b></label> {{$vitalsign->RR}}
            <label><b>Temperature (in C/F): </b></label> {{$vitalsign->Temperature}}
            <label><b>TemperatureType:</b></label>  {{$vitalsign->TemperatureType}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 15px;">

            <label ><b>Pulse (in minutes): </b> </label> {{$vitalsign->Pulse}}
            @endforeach

            @foreach($generalconditionsdata as $generalcondition)
            <label ><b>Weight (in kg): </b></label> {{$generalcondition->Weight}}
            <label ><b>Height (cms/ft): </b></label> {{$generalcondition->Height}}
            <b> <p>BMI:  </p>  </b>
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;"><b>Pain: </b></h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($vitalsignsdata as $vitalsign)
        <div >
            <label ><b>Pain Scale: </b> </label> {{$vitalsign->PainScale}}
            <label > <b>Quality: </b>  </label> {{$vitalsign->Quality}}
            <label > <b>Severity Scale: </b>  </label> {{$vitalsign->SeverityScale}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center">Provocation/ Alleviation </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div >
            <label ><b>Cause of pain: </b> </label> {{$vitalsign->P1}}
            <label > <b>Methods of alleviation: </b>  </label> {{$vitalsign->P2}}
            <label > <b>What makes it worse: </b>  </label> {{$vitalsign->P3}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">
        <label><h2 style="text-align:center;">Region </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div >
            <label ><b>Place of origin for the pain: </b> </label> {{ $vitalsign->R1}}
            <label > <b>Number of places the pain exists: </b>  </label> {{$vitalsign->R2}}
            <label > <b>Does it go anywhere else? </b>  </label> {{$vitalsign->R3}}
            <label > <b>Did it start elsewhere and now localised to one spot? </b>  </label> {{$vitalsign->R4}}
        </div>


        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Timing </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div >
            <label ><b>Time pain started? </b> </label> {{ $vitalsign->T1}}
            <label ><b>How long it started? </b> </label> {{$vitalsign->T2}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>
        @endforeach

        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Orientation </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($orientationsdata as $orientation)
        <div >
            <label ><b>Person </b> </label> {{$orientation->Person}}
            <label ><b>Place </b> </label> {{$orientation->Place}}
            <label ><b>Time </b> </label> {{$orientation->Time}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        @endforeach
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">


        <label><h2 style="text-align:center;">Memory Intacts </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($memoryintactsdata as $memoryintact)
        <div >
            <label ><b>Short Term Type </b> </label> {{$memoryintact->LongTerm}}
            <label ><b>Long Term Type </b> </label> {{$memoryintact->ShortTerm}}

        </div>
        @endforeach

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Communication </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($communicationsdata as $communication)
        <div >
            <label ><b>Language </b> </label> {{$communication->Language}}
            <label ><b>Adequate for all activities </b> </label> {{$communication->AdequateforAllActivities}}
            <label ><b>Unable to Communicate</b> </label> {{$communication->unableToCommunicate}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Vision</h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($visionhearingsdata as $visionhearing)
        <div >
            @if($visionhearing->Impared == "Intact")
            <label ><b>Type: Intact</b> </label>
            @else
            <label ><b>Type:</b> </label> {{$visionhearing->Impared}}
            @if($visionhearing->ShortSight ==NULL)
            <label ><b>Sight Type: </b> </label> {{$visionhearing->LongSight}}
            @else
            <label ><b>Sight Type: </b> </label> {{$visionhearing->ShortSight}}
            <label ><b>Wears Glasses: </b> </label> {{$visionhearing->WearsGlasses}}
            @endif
        </div>
        @endif

        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Hearing </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($visionhearingsdata as $visionhearing)
        <div >
            @if($visionhearing->HImpared == "Intact")
            <label ><b>Type:</b> </label> {{$visionhearing->HImpared}}
            @else
            <label ><b>Type:</b> </label> {{$visionhearing->HImpared}}
            <label ><b>Hearing Aids:</b> </label> {{$visionhearing->HearingAids}}
            @endif
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($respiratorydata as $respiratory)
        <label><h2 style="text-align:center;">Respiratory </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div >
            <label ><b>SOB: </b> </label> {{$respiratory->SOB}}
            <label ><b>SPO2: </b> </label> {{$respiratory->SPO2}}
            <label ><b> H/O TB/Asthma/COPD: </b> </label> {{$respiratory->H_OTB_Asthma_COPD}}
        </div>

        <div>
            <br>
        </div>

        <div >
            <label ><b>Cough: </b> </label> {{$respiratory->Cough}}
            <label ><b>Color of Phlegm: </b> </label> {{$respiratory->ColorOfPhlegm}}
            <label ><b> Nebulization: </b> </label> {{$respiratory->Nebulization}}
        </div>

        <div>
            <br>
        </div>

        <div >
            <label ><b>Tracheostomy: </b> </label> {{$respiratory->Tracheostomy}}
            <label ><b>CPAP / BIPAP: </b> </label> {{$respiratory->CPAP_BIPAP}}
            <label ><b> ICD: </b> </label> {{$respiratory->ICD}}
            <label ><b> Central Cynaosis: </b> </label>{{$respiratory->central_cynaosis}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Position </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($positiondata as $position)
        <div >
            <label ><b>Position Type: </b> </label>{{ $position->Position_Type}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Circulatory </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($circulatorydata as $circulatory)
        <div >
            <label ><b>Chest Pain: </b> </label> {{$circulatory->ChestPain}}
            <label ><b>H/O DM/IHD/HTN/CAD/CHF: </b> </label> {{$circulatory->hoHTNCADCHF}}
            <label ><b>Peripheral Cyanosis: </b> </label>  {{$circulatory->PeripheralCyanosis}}
        </div>

        <div class="">
            <br>
        </div>

        <div >
            <label ><b>Jugular Vein: </b> </label>{{$circulatory->JugularVein}}
            <label ><b>Surgical history: </b> </label> {{$circulatory->SurgeryHistory}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Denture </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($denturesdata as $denture)
        <div >
            <label ><b>Upper: </b> </label> {{$denture->Upper}}
            <label ><b>Lower: </b> </label> {{$denture->Lower}}
            <label ><b>Need assistance in cleaning: </b> </label> {{$denture->Cleaning}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Nutrition </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($nutritiondata as $nutrition)
        <div >
            <label ><b>Type: </b> </label> {{$nutrition->Type}}
            <label ><b>Adequacy: </b> </label> {{$nutrition->Intact}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Diet </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <div >
            <label ><b>Diet Type: </b> </label> {{ $nutrition->Diet}}
            <label ><b>Break Fast Time: </b> </label> {{$nutrition->BFTime}}
            <label ><b>Lunch Time: </b> </label> {{$nutrition->LunchTime}}
        </div>

        <div class="">
            <br>
        </div>

        <div >
            <label ><b>Snacks Time: </b> </label> {{$nutrition->SnacksTime}}
            <label ><b>Dinner Time: </b> </label> {{$nutrition->DinnerTime}}
            <label ><b>TPN: </b> </label> {{$nutrition->TPN}}
        </div>

        <div class="">
            <br>
        </div>

        <div >
            <label ><b>RT Feeding (in ml): </b> </label>{{$nutrition->RTFeeding}}
            <label ><b>PEG Feeding (in ml): </b> </label> {{$nutrition->PEGFeeding}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Abdomen </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($abdomendata as $abdomen)
        <div >
            <label ><b>Inspection: </b> </label> {{$abdomen->Inspection}}
            <label ><b>Ausculation of Bowel Sound: </b> </label> {{$abdomen->AusculationofBS}}
            <label ><b>Palpation: </b> </label> {{$abdomen->Palpation}}
        </div>

        <div class="">
            <br>
        </div>

        <div >
            <label ><b>Percussion: </b> </label> {{$abdomen->Percussion}}
            <label ><b>Illeostomy: </b> </label> {{$abdomen->Ileostomy}}
            <label ><b>Colostomy: </b> </label> {{$abdomen->Colostomy}}
            <label ><b>Functioning: </b> </label> {{$abdomen->Functioning}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Urinary Continence </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($genitosdata as $genito)
        <div >
            <label ><b>Urinary Continent: </b> </label> {{$genito->UrinaryContinent}}
            <label ><b>Completely Continent: </b> </label> {{$genito->CompletelyContinet}}
            <label ><b>Incontinent Urine Occasionally: </b> </label> {{$genito->IncontinentUrineOccasionally}}
        </div>

        <div class="">
            <br>
        </div>

        <div >
            <label ><b>Incontinent Urine Night Only: </b> </label> {{$genito->IncontinentUrineNightOnly}}
            <label ><b>Incontinent Urine Always: </b> </label> {{$genito->IncontinentUrineAlways}}
        </div>

        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Fecal Continent </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($fecaldata as $fecal)
        <div >
            @if($fecal->FecalType == "Continent")
            <label ><b>Type: </b> </label> {{$fecal->FecalType}}
            <label ><b>Commode chair: </b> </label> {{$fecal->Commodechair}}
            @else
            <label ><b>Type: </b> </label> {{$fecal->FecalType}}
            <label ><b>Incontinent Occasionally: </b> </label> {{$fecal->IncontinentOccasionally}}
            <label ><b>Incontinent Always: </b> </label> {{$fecal->IncontinentAlways}}
            <label ><b>Commode chair: </b> </label> {{$fecal->Commodechair}}
            @endif
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">Mobility Type </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($mobilitydata as $mobility)
        <div >
            <label ><b>Mobility Type: </b> </label> {{$mobility->Independent}}
            <label ><b>Need Assistance: </b> </label> {{$mobility->NeedAssistance}}
            <label ><b>Walker: </b> </label> {{$mobility->Walker}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div >
            <label ><b>Wheelchair: </b> </label> {{$mobility->WheelChair}}
            <label ><b>Crutch: </b> </label> {{$mobility->Crutch}}
            <label ><b>Cane: </b> </label> {{$mobility->Cane}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div >
            <label ><b>Chair Fast: </b> </label> {{$mobility->ChairFast}}
            <label ><b>Bed Fast: </b> </label> {{$mobility->BedFast}}
        </div>
        @endforeach
        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>


        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        <label><h2 style="text-align:center;">General History </h2></label>
        <hr style="color:black; background-color:black; height: 2px; margin-top:-2px;">

        @foreach($generalhistorydata as $generalhistory)
        <div >
            <label ><b>Present History: </b> </label> {{$generalhistory->PresentHistory}}
            <label ><b>Past History: </b> </label> {{$generalhistory->PastHistory}}
            <label ><b>Family History: </b> </label> {{$generalhistory->FamilyHistory}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div >
            @foreach($leaddata as $lead)
            @if($lead->Gender == "Female")
            <label ><b>Menstruation & OBG History: </b> </label> {{$generalhistory->Mensural_OBGHistory}}
            @endif
            @endforeach
            <label ><b>Allergic History: </b> </label> {{$generalhistory->AllergicHistory}}
            <label ><b>Allergic Status: </b> </label> {{$generalhistory->AllergicStatus}}
        </div>

        <div  style="margin-top: 11px;">
            <br>
        </div>

        <div >
            <label ><b>Allergic Severity: </b> </label> {{$generalhistory->AllergicSeverity}}

            @foreach($generalconditionsdata as $generalcondition)
            <label ><b>Medical History: </b> </label> {{$generalcondition->MedicalHistory}}
            @endforeach
        </div>
        @endforeach
    </body>
    </html>
