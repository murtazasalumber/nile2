
@extends('layouts.coordinator')

@section('content')
<script>
$(document).ready(function(){
  
            $("#keyword").keyup(function(){

      var keyword = $('#keyword').val();
      var filter = $('#filter').val();

      
          $.ajax({
        type: "GET",
        url:'allleadc' ,
         data: {'keyword1' : keyword,'filter1' : filter,'_token':$('input[name=_token]').val() },
        success: function(data){
             $('#result').html(data);
        }
    });
    });
     $("#filter").change(function(){

              $('#keyword').val("");
             });  
    
});
</script>
<style type="text/css">
  .imgg
{

    margin-top: -10px;
    margin-left: -60px;
       
   

    max-width: 167px;

}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}
body::-webkit-scrollbar 
{
  display: none;
}
  .btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      
      outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
input
{
  border-right: 0px;  
    margin-top: -11px;
    border-top: 0px;
    border-left: 0px; 
    border-bottom:1px solid #484e51;
    padding-bottom: 0px; 
    width:100%;
    font-family: myFirstFont;
    outline: none;
}
input:focus
{
  border-bottom:1px solid #00cccc;
}
select:focus
{
  border-bottom:1px solid #00cccc;
}
select
{
  border-bottom-color:#484e51;
      border-top: 0px;
    width: 100%;
    border-left: 0px;
    border-right: 0px;
    background: white;
    outline: none;
}
.footer
{
  text-align: center;
  display: none;
}

@media (max-width: 1200px)
{
#loo
{
  margin-left: 37%;
}
.imggg
{
  text-align: center;
}
#filter
   {  
        width: 44%;
        padding-top: 19px;
    margin-bottom: 17px;
   }
.keyword1
    {
    margin-right: 50px;
    }
 #buttonforcreate
  {
      margin-top: 10%;
  }
   
   #productleadparent
   {
              margin-top: 12px;
    margin-left: 36%;
   }
  
 
   #seacrh
   {
    margin-bottom: 37px;
   }
   #download
   {
    margin-top: 5%;
    margin-left: 24%;
    width: 53%;
   }
   #second
    {
              margin-bottom: 22px;
    margin-left: 36%;
    }
    #productl
    {
          margin-bottom: 16px;
    margin-left: 26%;
    }
}

.navbar2
{
  display: none;
}
  .popover.bottom>.arrow
    {
        display: none;
    }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
.footable.breakpoint > tbody > tr > td > span.footable-toggle {
    float: right;
    display: inline-block;
    font-family: 'footable';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    -webkit-font-smoothing: antialiased;
    padding-right: 5px;
    font-size: 14px;
    color: #888888;
}
/*Query for galaxy*/
@media screen and (device-width: 360px) and (device-height: 640px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)
{
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }
}
@media screen and (device-width: 640px) and (device-height:360px)  and (orientation: landscape)
{
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }
}

/*Query for Nexus*/
@media (min-device-width: 412px) and (max-device-width: 420px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }


}
@media screen and (device-width: 732px) and (device-height:412px)  and (orientation: landscape)
{
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

  }
  /*Query for iphone 5*/
@media (min-device-width: 320px) and (max-device-height: 568px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }


}
@media (min-device-width: 568px) and (max-device-height: 320px) and (orientation: landscape){ 
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }
}
/*Query for iphone 6*/
@media (min-device-width: 375px) and (max-device-height: 667px) and (orientation: portrait){ 
.navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }

}
@media (min-device-width: 667px) and (max-device-height: 375px) and (orientation: landscape){ 
   .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

}
/*Iphone 6+*/
@media (min-device-width:736px) and (max-device-height: 414px) and (orientation: landscape){
  .title
  {
        margin-top: 9%;
  }
    .navbar2
  {
        z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 20px;
  }
  .navbar
  {
    position: fixed;
  }
  .filter
  {
        margin-left: 7%;
    width: 34%!important;
  }
  .keyword1
  {
        width: 29%;
  }

}
/*Iphone X*/
@media (min-device-width: 375px) and (max-device-height: 812px) and (orientation: portrait){ 
.navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }

}
/*Ipad*/
@media (min-device-width: 768px) and (max-device-height: 1024px) and (orientation: portrait){ 
  .filter
  {
    margin-left: 7%;
    width: 29%!important;
  }
}
/*Nexus 7 (Tab)*/
@media (min-device-width: 600px) and (max-device-height: 960px) and (orientation: portrait){ 
  .navbar2
  {
    z-index: 10000;
    display: block;
    position: fixed;
    margin-left: 83%;
    margin-top: 24px;
  }
.popover.bottom
  {
    top:49px!important;
    width: 408% !important;
    left: -220%!important;
  }
  .navbar
  {
    position: fixed;
  }
  .title
  {
        margin-top: 19%;
  }
  .filter
  {
    margin-left: 7%;
    width: 70%!important;
  }
  .keyword1
  {
    margin-left: 19%;
    width: 70%!important;
    margin-top: 9px;
        margin-bottom: 15px;
  }
}
@media (min-device-width: 960px) and (max-device-height: 600px) and (orientation: landscape){ 
  .filter
  {
    margin-left: 7%;
    width: 29%!important;
  }
}
</style>
  <div class="container">
            <div class="row">
               <div class="col-sm-12" >
                  <center> <h2 style="color: #333;"> Product Leads</h2></center>
               </div>
            </div>
      
    </div>
     <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <center> <select class="filter" id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select> <input type="text"  class="keyword1" placeholder='Search' id='keyword'></center>
          </div>
      </div>
</div>

   <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">
             <div class="container">
    <div class="row imggg" >
        <div class="col-sm-3 col-sm-offset-5" id="icons">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}">  <img src="/img/NewCreateLead.png" class="img-responsive" alt="add" style="    display: -webkit-inline-box;"></a>
           <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" > <img src="/img/product.png" class="img-responsive" alt="add" style="    margin-left: 14px;    display: -webkit-inline-box;"></a>



             

            
        </div>
      
    </div>
</div>
<!-- <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
         
           <div style="    margin-left: -69px;">
          <div class="col-sm-4" id="buttonforcreate">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="second" >
                                          Create Service Lead
                                        </a>&emsp;
                  <div id="productleadparent" style="    display: -webkit-inline-box;">
                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="procductl">
                                          Create Product Lead
                                        </a>
                                        </div>
                                        </div>
          </div> 
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="text"  placeholder='Search' id='keyword'>
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">
              </div>
              <div class="col-sm-2" >
              
          </div>

          </div>


  </div>

</div> -->

<table class="table footable" style="font-family: Raleway,sans-serif;margin-top: 4%;">
        <thead>
        <th  data-hide="phone,tablet" style="color: #333;"><b>S.no</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>order id</b></th>
        <th  data-hide="phone,tablet" style="color: #333;"><b>Lead ID</b></th>
        	<th  data-hide="phone,tablet" style="color: #333;"><b>Product Type</b></th>
        	<th  data-hide="phone,tablet" style="color: #333;"><b>Created At</b></th>
          <th  data-hide="phone,tablet" style="color: #333;"><b>Created By</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Name</b></th>
          <th data-hide="phone,tablet"  style="color: #333;"><b>Customer Mobile</b></th>

          <th data-hide="phone,tablet" style="color: #333;"><b>Status</b></th>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          @foreach ($leads as $lead)
          <tr>
            <?php
            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
       <td>{{$lead->id}}</td>
<td>{{$lead->orderid}}</td>

@if($lead->MedName!=NULL)

	@if($lead->SKUid!=NULL)

	 <td>{{$lead->leadid}}</td>
	<td>Both</td>

  	@else


	<td>{{$lead->leadid}}</td>
	<td>Pharmacy</td>

    <td>{{$lead->created_at}}</td>
 <td> {{$lead->Prequestcreatedby}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
  <td>{{$lead->POrderStatus}}</td>
 

	@endif

@else


<td>{{$lead->leadid}}</td>
	<td>Product</td>
	<td>{{$lead->created_at}}</td>
  <td>{{$lead->Requestcreatedby}}</td>
  <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>
  <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>

@if($lead->Type=="Sell" || $lead->Type==NULL)
  <td>{{$lead->OrderStatus}}</td>
@else
  <td>{{$lead->OrderStatusrent}}</td>
@endif


@endif
  
  


            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach
    </tbody>
  </table>
<div style="text-align: -webkit-center;"> {{$leads->links()}} </div>



@endsection