<html>
<head>



  
  <script>
$(document).ready(function(){

  $('.footable').footable();
    
});
</script>
</head>
<body>

@if (count($data) > 0)
   




	@if ($filter1 === "City")
   						<div class="container-fluid" id="result" >
<div class="row">
	<div class="col-sm-12">
				<table class="table footable" style="font-family: Raleway,sans-serif;">
    			<thead>
     					<tr>
        						<th> <b> Leads ID </b></th>
        						<th  data-hide="phone,tablet" ><b>Created by</b></th>
        						<th data-hide="phone,tablet" ><b>Customer Name</b></th>
        						<th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
        						<th data-hide="phone,tablet" ><b>City</b></th>
        						<th data-hide="phone,tablet"><b>Vertical</b></th>
        						<th data-hide="phone,tablet"><b>Coordinator</b></th>
        						<th data-hide="phone,tablet" ><b>Lead Priority</b></th>
        						<th data-hide="phone,tablet" ><b>Lead Status</b></th>
        						<th data-hide="phone,tablet" ><b>Lead Referance</b></th>
        						
      					</tr>
    				</thead>
    				<tbody>

      						@foreach($data as $lead)

									 <tr>
        									<td>{{$lead->LeadiD}}</td>
        									<td>{{$lead->Createdby}}</td>
        									<td>{{$lead->CustomerName}}</td>
        									<td>{{$lead->CustomerMobile}}</td>
        									<td style="color:red;">{{$lead->City}}</td>
        									<td>{{$lead->Vertical}}</td>
        									<td>{{$lead->Coordinator}}</td>
        									<td>{{$lead->LeadPriority}}</td>
        									<td>{{$lead->LeadStatus}}</td>
        									<td>{{$lead->LeadReferance}}</td>
        								
      									</tr>
							@endforeach
      
   						 </tbody>
  				</table>
	</div>
</div>
</div>



	@elseif ($filter1 === "LeadPriority")
    			<div class="container-fluid" id="result">
<div class="row">
	<div class="col-sm-12">
			<table class="table footable" style="font-family: Raleway,sans-serif;">
                <thead>
                        <tr>
                                <th> <b> Leads ID </b></th>
                                <th  data-hide="phone,tablet" ><b>Created by</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                <th data-hide="phone,tablet" ><b>City</b></th>
                                <th data-hide="phone,tablet"><b>Vertical</b></th>
                                <th data-hide="phone,tablet"><b>Coordinator</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Priority</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Referance</b></th>
                                
                        </tr>
                    </thead>
    				<tbody>

      						@foreach($data as $lead)

									 <tr>
        									<td>{{$lead->LeadiD}}</td>
        									<td>{{$lead->Createdby}}</td>
        									<td>{{$lead->CustomerName}}</td>
        									<td>{{$lead->CustomerMobile}}</td>
        									<td >{{$lead->City}}</td>
        									<td>{{$lead->Vertical}}</td>
        									<td>{{$lead->Coordinator}}</td>
        									<td style="color:red;">{{$lead->LeadPriority}}</td>
        									<td>{{$lead->LeadStatus}}</td>
        									<td>{{$lead->LeadReferance}}</td>
        								
      									</tr>
							@endforeach
      
   						 </tbody>
  				</table>
	</div>
</div>
</div>



    @elseif ($filter1 === "Vertical")
    		<div class="container-fluid" id="result">
<div class="row">
	<div class="col-sm-12">
				<table class="table footable" style="font-family: Raleway,sans-serif;">
                <thead>
                        <tr>
                                <th> <b> Leads ID </b></th>
                                <th  data-hide="phone,tablet"><b>Created by</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                <th data-hide="phone,tablet" ><b>City</b></th>
                                <th data-hide="phone,tablet"><b>Vertical</b></th>
                                <th data-hide="phone,tablet"><b>Coordinator</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Priority</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Referance</b></th>
                                
                        </tr>
                    </thead>
    				<tbody>

      						@foreach($data as $lead)

									 <tr>
        									<td>{{$lead->LeadiD}}</td>
        									<td>{{$lead->Createdby}}</td>
        									<td>{{$lead->CustomerName}}</td>
        									<td>{{$lead->CustomerMobile}}</td>
        									<td >{{$lead->City}}</td>
        									<td style="color:red;">{{$lead->Vertical}}</td>
        									<td>{{$lead->Coordinator}}</td>
        									<td >{{$lead->LeadPriority}}</td>
        									<td>{{$lead->LeadStatus}}</td>
        									<td>{{$lead->LeadReferance}}</td>
        								
      									</tr>
							@endforeach
      
   						 </tbody>
  				</table>
	</div>
</div>
</div>


    
    @elseif ($filter1 === "LeadReferance")
    		<div class="container-fluid" id="result">
<div class="row">
	<div class="col-sm-12">
				<table class="table footable" style="font-family: Raleway,sans-serif;">
                <thead>
                        <tr>
                                <th> <b> Leads ID </b></th>
                                <th  data-hide="phone,tablet" ><b>Created by</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                <th data-hide="phone,tablet" ><b>City</b></th>
                                <th data-hide="phone,tablet"><b>Vertical</b></th>
                                <th data-hide="phone,tablet"><b>Coordinator</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Priority</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Referance</b></th>
                                
                        </tr>
                    </thead>
    				<tbody>

      						@foreach($data as $lead)

									 <tr>
        									<td>{{$lead->LeadiD}}</td>
        									<td>{{$lead->Createdby}}</td>
        									<td>{{$lead->CustomerName}}</td>
        									<td>{{$lead->CustomerMobile}}</td>
        									<td >{{$lead->City}}</td>
        									<td >{{$lead->Vertical}}</td>
        									<td>{{$lead->Coordinator}}</td>
        									<td >{{$lead->LeadPriority}}</td>
        									<td>{{$lead->LeadStatus}}</td>
        									<td style="color:red;">{{$lead->LeadReferance}}</td>
        								
      									</tr>
							@endforeach
      
   						 </tbody>
  				</table>
	</div>
</div>
</div>
    @elseif ($filter1 === "CustomerName")
   		<div class="container-fluid" id="result">
<div class="row">
	<div class="col-sm-12">
				<table class="table footable" style="font-family: Raleway,sans-serif;">
                <thead>
                        <tr>
                                <th> <b> Leads ID </b></th>
                                <th  data-hide="phone,tablet" ><b>Created by</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Name</b></th>
                                <th data-hide="phone,tablet" ><b>Customer Mobile</b></th>
                                <th data-hide="phone,tablet" ><b>City</b></th>
                                <th data-hide="phone,tablet"><b>Vertical</b></th>
                                <th data-hide="phone,tablet"><b>Coordinator</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Priority</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                                <th data-hide="phone,tablet" ><b>Lead Referance</b></th>
                                
                        </tr>
                    </thead>
    				<tbody>

      						@foreach($data as $lead)

									 <tr>
        									<td>{{$lead->LeadiD}}</td>
        									<td>{{$lead->Createdby}}</td>
        									<td style="color:red;">{{$lead->CustomerName}}</td>
        									<td>{{$lead->CustomerMobile}}</td>
        									<td >{{$lead->City}}</td>
        									<td >{{$lead->Vertical}}</td>
        									<td>{{$lead->Coordinator}}</td>
        									<td >{{$lead->LeadPriority}}</td>
        									<td>{{$lead->LeadStatus}}</td>
        									<td >{{$lead->LeadReferance}}</td>
        								
      									</tr>
							@endforeach
      
   						 </tbody>
  				</table>
	</div>
</div>
</div>

			
	@endif




@else
<p> Not Found </p>
@endif
</body>
</html>