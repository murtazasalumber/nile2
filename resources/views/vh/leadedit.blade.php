@section('editid', $lead->id)


@section('editreference',$lead->reference)
@section('editclientfname',$lead->fName)
@section('editclientmname',$lead->mName)
@section('editclientlname',$lead->lName)
@section('editclientemail',$lead->EmailId)
@section('editsource',$lead->Source)
@section('editcountryname',$lead->Country_Name)
@section('editcountrycode',$lead->Country_Code)
@section('editMobileNumber',$lead->MobileNumber)
@section('editclientalternateno',$lead->Alternatenumber)
@section('editEmergencyContact',$lead->EmergencyContact)
@section('editassesmentreq',$lead->AssesmentReq)



@section('editAddress1',$address->Address1)
@section('editAddress2',$address->Address2)
@section('editCity',$address->City)
@section('editDistrict',$address->District)
@section('editState',$address->State)
@section('editPinCode',$address->PinCode)
@section('editPAddress1',$address->PAddress1)
@section('editPAddress2',$address->PAddress2)
@section('editPCity',$address->PCity)
@section('editPDistrict',$address->PDistrict)
@section('editPState',$address->PState)
@section('editPPinCode',$address->PPinCode)
@section('editEAddress1',$address->EAddress1)
@section('editEAddress2',$address->EAddress2)
@section('editECity',$address->ECity)
@section('editEDistrict',$address->EDistrict)
@section('editEState',$address->EState)
@section('editEPinCode',$address->EPinCode)




@section('editpatientfname',$personneldetail->PtfName)
@section('editpatientmname',$personneldetail->PtmName)
@section('editpatientlname',$personneldetail->PtlName)
@section('editage',$personneldetail->age)
@section('editgender',$personneldetail->Gender)
@section('editrelationship',$personneldetail->Relationship)
@section('editOccupation',$personneldetail->Occupation)
@section('editaadhar',$personneldetail->AadharNum)
@section('editAlternateUHIDType',$personneldetail->AlternateUHIDType)
@section('editAlternateUHIDNumber',$personneldetail->AlternateUHIDNumber)
@section('editPTAwareofDisease',$personneldetail->PTAwareofDisease)
@section('editaddressid',$personneldetail->Addressid)




@section('editservicetype',$service->ServiceType)
@section('editGeneralCondition',$service->GeneralCondition)
@section('editleadtype',$service->LeadType)
@section('editbranch',$service->Branch)
@section('editRequestDateTime',$service->RequestDateTime)
@section('editassignedto',$service->AssignedTo)
@section('editquotedprice',$service->QuotedPrice)
@section('editexpectedprice',$service->ExpectedPrice)
@section('editservicestatus',$service->ServiceStatus)
@section('editpreferedgender',$service->PreferedGender)
@section('editpreferedlanguage',$service->PreferedLanguage)
@section('editremarks',$service->Remarks)


@section('editSKUid',$product->SKUid)
@section('editProductName',$product->ProductName)
@section('editDemoRequired',$product->DemoRequied)
@section('editAvailabilityStatus',$product->AvailabilityStatus)
@section('editAvailabilityAddress',$product->AvailabilityAddress)
@section('editSellingPrice',$product->SellingPrice)
@section('editRentalPrice',$product->RentalPrice)

@if($vert==NULL)
@section('editassigned',$vert)
@else
@section('editassigned',$vert->FirstName)
@endif

@section('editMethod')
{{method_field('PUT')}}
@endsection




@extends('layouts.app1')
@section('content')

<html lang="en">
<head>
<title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

  <meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <style type="text/css">
  button[disabled], html input[disabled] {
    background: white;
    cursor: default;
  }
  </style>


  <script>
  $(document).ready(function(){
 $(function() {
    $('#country').on('change', function() {
      $('#phone1').val($(this).val());
    });



    var country = $('#country').val();
    $('#phone1').val(country);

    //  $('#sameaspermanentadress').change(function () {

    //         console.log("checked");
    // });
    $('#sameaspermanentadress').change(function () {

      if (this.checked) {
        console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#PAddress1').val(Addresd1);
        $("#PAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#PAddress2').val(Addresd2);
        $("#PAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#PCity').val(City);
        $("#PCity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#PDistrict').val(District);
        $("#PDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#PState').val(State);
        $("#PState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#PPinCode').val(Pincode);
        $("#PPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        console.log("unchecked");

        // Adrees1
        $('#PAddress1').val("");
        $("#PAddress1").prop('disabled', false);

        //Adress 2
        $('#PAddress2').val("");
        $("#PAddress2").prop('disabled', false);

        //City
        $('#PCity').val("");
        $("#PCity").prop('disabled', false);

        //District
        $('#PDistrict').val("");
        $("#PDistrict").prop('disabled', false);

        //State
        $('#PState').val("");
        $("#PState").prop('disabled', false);


        //Pincode
        $('#PPinCode').val("");
        $("#PPinCode").prop('disabled', false);

      }
    });





    $('#Esameaspermananentaddress').change(function () {

      if (this.checked) {
        // console.log("checked");
        // $('#Address1').val("fgg");
        var Addresd1 =$('#Address1').val();
        // Adrees1
        $('#EAddress1').val(Addresd1);
        $("#EAddress1").prop('disabled', true);


        //Adress2
        var Addresd2 =$('#Address2').val();
        $('#EAddress2').val(Addresd2);
        $("#EAddress2").prop('disabled', true);

        //City

        var City =$('#City').val();
        $('#ECity').val(City);
        $("#ECity").prop('disabled', true);

        //district

        var District =$('#District').val();
        $('#EDistrict').val(District);
        $("#EDistrict").prop('disabled', true);

        //State

        var State =$('#State').val();
        $('#EState').val(State);
        $("#EState").prop('disabled', true);


        //Pincode

        var Pincode =$('#PinCode').val();
        $('#EPinCode').val(Pincode);
        $("#EPinCode").prop('disabled', true);
      }

      if(!$(this).is(":checked"))
      {
        // console.log("unchecked");

        // Adrees1
        $('#EAddress1').val("");
        $("#EAddress1").prop('disabled', false);

        //Adress 2
        $('#EAddress2').val("");
        $("#EAddress2").prop('disabled', false);

        //City
        $('#ECity').val("");
        $("#ECity").prop('disabled', false);

        //District
        $('#EDistrict').val("");
        $("#EDistrict").prop('disabled', false);

        //State
        $('#EState').val("");
        $("#EState").prop('disabled', false);


        //Pincode
        $('#EPinCode').val("");
        $("#EPinCode").prop('disabled', false);

      }
    });


    $("#EmergencyContact").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#EmergencyContact').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(mobileno) == false)
      {

        // console.log("Wrong Email");
        $("#EmergencyContact").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#EmergencyContact").css("border-bottom", "1px solid green");
      }

    });


    $("#EmergencyContact").blur(function(){
      var value = $('#EmergencyContact').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;

      if (reg.test(value) == false)
      {

        // console.log("Wrong Email");
        $("#EmergencyContact").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
      }

      if(value == "")
      {
        $("#EmergencyContact").css("border-bottom", "1px solid #484e51");
      }
    });



       //Phone Number
    $("#clientmob").keyup(function(){

      // console.log("clciked");

      var mobileno = $('#clientmob').val();
      var reg = /^[\+?\d[\d -]{8,12}\d$/;



            if (reg.test(mobileno) == false)
        {

              // console.log("Wrong Email");
              $("#clientmob").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientmob").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }


});
     $("#clientmob").blur(function(){
      var value = $('#clientmob').val();

      var reg = /^[\+?\d[\d -]{8,12}\d$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientmob").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else if (reg.test(value) == true)
        {
          console.log("Correct Email");
           $("#clientmob").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);

        }

        if(value == "")
        {
            $("#clientmob").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
     });


    // Pincode Validaion in Addres
  $("#PinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PinCode").blur(function(){
            var value = $('#PinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Present Addres
  $("#PPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#PPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#PPinCode").blur(function(){
            var value = $('#PPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#PPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#PPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#PPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });

    // Pincode Validaion in Emergency Addres
  $("#EPinCode").keyup(function(){

          // console.log("Pincode ");
          var reg = /^[1-9][0-9]{5}$/;
          var pincode = $('#EEPinCode').val();

          if (reg.test(pincode) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid green");
            $("#submitt").prop('disabled', false);
        }

    });

  $("#EPinCode").blur(function(){
            var value = $('#EPinCode').val();

         var reg = /^[1-9][0-9]{5}$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#EPinCode").css("border-bottom", "1px solid red");
               $("#submitt").prop('disabled', true);
        }
         else
        {
          // console.log("Correct Email");
           $("#EPinCode").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }

        if(value == "")
        {
            $("#EPinCode").css("border-bottom", "1px solid #484e51");
             $("#submitt").prop('disabled', false);
        }
    });


    // Client Email Id Validation Start
    $("#clientemail").keyup(function(){
      var value = $('#clientemail').val();

      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);

        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid green");
           $("#submitt").prop('disabled', false);
        }

      if (reg.test(value) == false)
      {

        // console.log("Wrong Email");
        $("#clientemail").css("border-bottom", "1px solid red");
      }
      else
      {
        // console.log("Correct Email");
        $("#clientemail").css("border-bottom", "1px solid green");
      }


    });

    $("#clientemail").blur(function(){
            var value = $('#clientemail').val();

         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (reg.test(value) == false)
        {

              // console.log("Wrong Email");
              $("#clientemail").css("border-bottom", "1px solid red");
              $("#submitt").prop('disabled', true);
        }
        else
        {
          // console.log("Correct Email");
           $("#clientemail").css("border-bottom", "1px solid #484e51");
           $("#submitt").prop('disabled', false);
        }

          if(value == "")
        {
            $("#clientemail").css("border-bottom", "1px solid #484e51");
            $("#submitt").prop('disabled', false);
        }
    });

    // Cleint ValidationEmail Ends




  });

    var value=$('#reference').val();
    if(value == "Other")
    {
      $("#source").show();
    }else
    {
      $("#source").hide();
    }



    $("#reference").change(function(){
      var value= $("#reference").val();
      if(value == "Other")
      {
        $("#source").show();
      }else
      {
        $("#source").hide();
      }
    });


// Drop down
var servicetype = $('#servicetype').val();

if(servicetype == "")
{
      $("#branch").hide();
} else
{
  $("#branchalert").hide();
}


var branch = $('#branch').val();

if((servicetype == "") && (branch == ""))
{


  $("#assignedto").hide();

} else
{
  $("#assignedtoalert").hide();
}




$("#servicetype").change(function(){

    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
    if(servicetype == "")
    {
        $("#branchalert").show();
        $("#branch").hide();
    }else if(branch == "")
    {
      $("#branchalert").hide();
      $("#branch").show();

    } else
    {
      $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });
    }
});



$("#branch").change(function(){
    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
  if(branch == "")
  {
      $("#assignedto").hide();
      $("#assignedtoalert").show();
      $("#resrult").hide();
  }else
  {


       // $.get("test.php", { name:"Donald", town:"Ducktown" });
       $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });


      $("#assignedto").hide();
      $("#assignedtoalert").hide();
      $("#resrult").show();
  }


});

$("#branch").ready(function(){
    var servicetype = $('#servicetype').val();
    var branch = $('#branch').val();
  if(branch == "")
  {
      $("#assignedto").hide();
      $("#assignedtoalert").show();
      $("#resrult").hide();
  }else
  {


       // $.get("test.php", { name:"Donald", town:"Ducktown" });
       $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
          });


      $("#assignedto").hide();
      $("#assignedtoalert").hide();
      $("#resrult").show();
  }


});

  });
  </script>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 30px;
  height: 17px;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .2s;
  transition: .2s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.btn:hover
{
      opacity: 0.5;
}
</style>
</head>
<body>

  <!-- header -->
  <!-- <div class="navbar navbar-default " style="    background-color: white;
  border-color: #e7e7e7;">
  <div class="container">

  <div class="navbar-header">
  <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 8px;">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>

<a class="navbar-brand" rel="home" href="/admin/vertical" title="Buy Sell Rent Everyting">
<img class="imgg"
src=" /img/healthheal_logo.png ">
</a>
</div>

<div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
<ul class="nav navbar-nav navbar-right">
<li ><a href="/admin/vertical" style="font-size: 15px;">Home</a></li>


</ul>

</div>

</div>
</div> -->

<!-- End of header -->
<!-- title -->
<div class="container">
  <div class="row">
    <div class="col-sm-12" id="title">
      <h2> {{substr(Route::currentRouteName(),20)}} Update Lead</h2>
    </div>
  </div>

</div>

<!-- title Ends -->
<div class="text-center">
@if(Session::has('warning'))
<div class="alert alert-danger" role="alert">
  <strong>Warning:</strong> {{Session::get('warning')}}
</div>

@endif
</div>

<form class="form-horizontal" action="/cc/@yield('editid')" method="POST">
  {{csrf_field()}}

  @section('editMethod')
  @show

  <div class="container" id="main" style="    margin-top: 34px;">

    <div class="row" id="maintitle" style="    margin-left: -4px;
    width: 102%;" >

    <div class="col-sm-12" >
      <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#firstform"  >Client Details<p style="text-align: -webkit-right;
        margin-top: -20px;
        margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
      </button>

    </div>

  </div>
</div>

<div class="container" id="main1" style="margin-top: 11px;">

  <div class="row collapse" id="firstform" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;    margin-left: -22px;">
    <div class="col-sm-3" style="    margin-top: 4px;">

      <label>First Name <span style="color:Red; font-size: 20px;">*</span></label>


      <input type="text"  rows="5" name="clientfname" id="clientfname" value="@yield('editclientfname')" required>

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Middle Name</label>
      <input type="text"   rows="5" name="clientmname" id="clientmname" value="@yield('editclientmname')">

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Last Name</label>
      <input type="text"  rows="5"  name="clientlname" id="clientlname" value="@yield('editclientlname')" >

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Email ID</label>
      <input type="text"  rows="5" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

    </div>
    <div class="col-sm-2" id="secondrow" style="margin-top: 35px;">

      <label style="  margin-bottom: 5px;">Select Country Code</label><br>
      <select  id="country"  name="code" >

               <option value="@yield('editcountrycode')">@yield('editcountryname')</option>
                @for($i=0;$i<$count;$i++)
                <option value="{{$country_codes[$i]['dial_code']}}" >{{ $country_codes[$i]['name']}}</option>
                @endfor
                <!-- <option value="+1">US</option>
                <option value="+44 ">UK</option> -->
            </select>
    </div>
    <div class="col-sm-4" style="margin-top: 26px;">


      <label>Mobile No. <span style="color:Red; font-size: 20px;">*</span></label>

       <div class="row">
              <div class="col-sm-2">
                <input type="text"  id="phone1"  rows="5" required disabled>
              </div>
              <div class="col-sm-10">
                  <input type="text"  id="clientmob"  rows="5" name="clientmob" value="@yield('editMobileNumber')" maxlength="10" required>
              </div>
          </div>

    </div>
    <div class="col-sm-3" id="secondrow">


      <label>Alternate no.</label>

      <input type="text" rows="5" name="clientalternateno" id="clientalternateno" value="@yield('editclientalternateno')">
    </div>
    <div class="col-sm-3" id="secondrow">

      <label>Emergency Contact No.</label>
      <input type="text" rows="5" name="EmergencyContact" id="EmergencyContact" value="@yield('editEmergencyContact')" maxlength="13">
    </div>

    <div class="col-sm-4" id="secondrow" style="margin-top: 35px;">

      <label style="  margin-bottom: 5px;"> Is Assessment Required?  </label><br>
      <select name="assesmentreq" id="assesmentreq">
        <option value="@yield('editassesmentreq')">@yield('editassesmentreq')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>
    </div>
    <div class="col-sm-4" id="secondrow" style="margin-top: 26px;">

      <label style="  margin-bottom: 5px;">Reference<span style="color:Red; font-size: 20px;">*</span></label><br>
      <select name="reference" id="reference"  required>
        <option value="@yield('editsource')">@yield('editsource')</option>
        @foreach($reference as $reference)
        <option value="{{ $reference->Reference}}">{{ $reference->Reference}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-4" id="secondrow">
      <div id="source">
        <label>Source</label>
        <input type="text"   rows="5" name="source" id="source" value="@yield('editsource')">
      </div>
    </div>
    <div class="col-sm-12" id="secondrow" >

      <h4> Permanent Address</h4>
    </div>


    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 1</label>
      <input type="text"   rows="5" name="Address1" id="Address1" value="@yield('editAddress1')">

    </div>
    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 2</label>
      <input type="text"   rows="5" name="Address2" id="Address2" value="@yield('editAddress2')">

    </div>
    <div class="col-sm-4" id="secondrow" style="margin-top: 35px;">
      <label style="  margin-bottom: 5px;">City</label><br>
      <!-- <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')"> -->

      <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')">


      <!-- <select name="City" id="City" >
      <option value="@yield('editCity')">@yield('editCity')</option>
      @foreach($city as $city)
      <option value="{{ $city->name}}">{{ $city->name}}</option>
      @endforeach
    </select> -->

  </div>

  <div class="col-sm-4" id="secondrow" >
    <label>District</label>
    <select  id="City"  name="City" >

        <option value="@yield('editCity')">@yield('editCity')</option>
        @for($i=0;$i<$count1;$i++)
        <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
        @endfor
        <!-- <option value="+1">US</option>
        <option value="+44 ">UK</option> -->
    </select>


  </div>
  <div class="col-sm-4" id="secondrow" >
    <label>State</label>
    <input type="text"    rows="5" name="State" id="State" value="@yield('editState')">

  </div>
  <div class="col-sm-4" id="secondrow" >
    <label>Pincode</label>
    <input type="text"    rows="5" name="PinCode" id="PinCode" value="@yield('editPinCode')" maxlength="8">

  </div>

  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PresentAddress" style="    width: 100%;
    margin-left: -1px;" >Present Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
  </button>
</div>
<div class="col-sm-12 collapse" id="PresentAddress">
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 1</label>
    <input type="text"    rows="5" name="PAddress1" id="PAddress1" value="@yield('editPAddress1')">
  </div>
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 2</label>
    <input type="text"  rows="5" name="PAddress2" id="PAddress2" value="@yield('editPAddress2')">
  </div>
  <div class="col-sm-4" style="    margin-top: 20px;">
    <label>City</label>

    <!-- <input type="text"  rows="5" name="PCity" id="PCity" value="@yield('editPCity')"> -->

    <input type="text"  rows="5" name="PCity" id="PCity" value="@yield('editPCity')">




    <!--  <select name="PCity" id="PCity" >
    <option value="@yield('editPCity')">@yield('editPCity')</option>
    @foreach($pcity as $pcity)
    <option value="{{ $pcity->name}}">{{ $pcity->name}}</option>
    @endforeach
  </select> -->


</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>District</label>
  <select  id="PDistrict"  name="PDistrict" >

      <option value="@yield('editPDistrict')">@yield('editPDistrict')</option>
      @for($i=0;$i<$count1;$i++)
      <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
      @endfor
      <!-- <option value="+1">US</option>
      <option value="+44 ">UK</option> -->
  </select>

</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>State</label>
  <input type="text"  rows="5" name="PState" id="PState" value="@yield('editPState')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>Pincode</label>
  <input type="text"  rows="5" name="PPinCode" id="PPinCode" value="@yield('editPPinCode')">
</div>

</div>

<div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
  <input type="checkbox" name="presentcontact" value="same" style="    margin-left: -98px;" id="sameaspermanentadress">
  <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>
</div>



  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#EmergencyAddress" style="    width: 100%;
    margin-left: -1px;" >Emergency Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
  </button>
</div>
<div class="col-sm-12 collapse" id="EmergencyAddress">
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 1</label>
    <input type="text"    rows="5" name="EAddress1" id="EAddress1" value="@yield('editEAddress1')">
  </div>
  <div class="col-sm-4" style="    margin-top: 19px;">
    <label>Address Line 2</label>
    <input type="text"  rows="5" name="EAddress2" id="EAddress2" value="@yield('editEAddress2')">
  </div>
  <div class="col-sm-4" style="    margin-top: 20px;">
    <label>City</label>
    <!-- <input type="text"  rows="5" name="ECity" id="ECity" value="@yield('editECity')"> -->

    <input type="text"  rows="5" name="ECity" id="ECity" value="@yield('editECity')">


    <!--  <select name="ECity" id="ECity" >
    <option value="@yield('editECity')">@yield('editECity')</option>
    @foreach($ecity as $ecity)
    <option value="{{ $ecity->name}}">{{ $ecity->name}}</option>
    @endforeach
  </select>
-->
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>District</label>
  <select  id="EDistrict"  name="EDistrict" >

      <option value="@yield('editEDistrict')">@yield('editEDistrict')</option>
      @for($i=0;$i<$count1;$i++)
      <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
      @endfor
      <!-- <option value="+1">US</option>
      <option value="+44 ">UK</option> -->
  </select>

</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>State</label>
  <input type="text"  rows="5" name="EState" id="EState" value="@yield('editEState')">
</div>
<div class="col-sm-4" style="    margin-top: 28px;">
  <label>Pincode</label>
  <input type="text"  rows="5" name="EPinCode" id="EPinCode" value="@yield('editEPinCode')">
</div>
</div>


<div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
<div style=" margin-top: 14px;" id="checkbox">
<label class="switch">
  <input type="checkbox" name="emergencycontact" value="same" style="    margin-left: -98px;" id="Esameaspermananentaddress">
  <span class="slider round"></span>

</label>&emsp;
<label>Same as Permanent Address</label>

</div>
</div>



</div>
</div>
<!-- Servcie for -->

<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ServiceDetails"  >Service Required For<p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;        ">

  <div class="row collapse" id="ServiceDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;margin-left: -22px;">
    <div class="col-sm-3" id="firstrow">

      <label> First Name</label><input type="text"  rows="5" name="patientfname" id="patientfname" value="@yield('editpatientfname')">

    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Middle Name</label><input type="text"  rows="5" name="patientmname" id="patientmname" value="@yield('editpatientmname')">
    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Last Name</label><input type="text"  rows="5" name="patientlname" id="patientlname" value="@yield('editpatientlname')">
    </div>
    <div class="col-sm-3" id="firstrow">
      <label>Age </label><input type="text"  rows="5" name="age" id="age" value="@yield('editage')">
    </div>
    <div class="col-sm-3" style="    margin-top: 29px;">
      <label>Gender</label>
      <select name="gender" id="gender" >
        <option value ="@yield('editgender')">@yield('editgender')</option>
        @foreach($gender as $gender)
        <option value="{{ $gender->gendertypes}}">{{ $gender->gendertypes}}</option>
        @endforeach
      </select>

    </div>
    <div class="col-sm-3" style="    margin-top: 29px;">
      <label>Relationship</label>
      <select name="relationship" id="relationship" >
        <option value="@yield('editrelationship')">@yield('editrelationship')</option>
        @foreach($relation as $relation)
        <option value="{{ $relation->relationshiptype}}">{{ $relation->relationshiptype}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3" style="    margin-top: 28px;">
      <label>Occupation </label><input type="text"  rows="5" name="Occupation" id="Occupation" value="@yield('editOccupation')">
    </div>
    <div class="col-sm-3" style="    margin-top: 28px;">
      <label>Aadhar Number </label><input type="text"  rows="5" name="aadhar" id="aadhar" value="@yield('editaadhar')">
    </div>
    <div class="col-sm-3" style="    margin-top: 33px;">
      <label>Alternate UHID Type </label>
      <select name="AlternateUHIDType" id="AlternateUHIDType"  value="@yield('editAlternateUHIDType')">
        <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
        <option value="PAN Card">PAN Card</option>
        <option value="Driving Licence">Driving Licence </option>
        <option value="Passport">Passport</option>
        <option value="Bank Passbook">Bank Passbook</option>
        <option value="Voter ID">Voter ID</option>
        <option value="Cellphone Postpaid Bill">Cellphone Postpaid Bill</option>
        <option value="Water Bill">Water Bill</option>
        <option value="Electricity Bill">Electricity Bill</option>
        <option value="Telephone Bill">Telephone Bill</option>
        <option value="Gas Consumer Number">Gas Consumer Number</option>
        <option value="Employee ID issued by defence">Employee ID issued by defence</option>

      </select>

    </div>
    <div class="col-sm-3" style="    margin-top: 33px;">
      <label>Alternate UHID Number </label><input type="text"  rows="5" name="AlternateUHIDNumber" id="AlternateUHIDNumber" value="@yield('editAlternateUHIDNumber')">
    </div>
    <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 34px;">
      <label>Is Patient aware of Disease / Diagnosis? </label>
      <select name="PTAwareofDisease" id="PTAwareofDisease"  value="@yield('editPTAwareofDisease')">
        <option value="@yield('editPTAwareofDisease')">@yield('editPTAwareofDisease')</option>
        <option value="No">No</option>
        <option value="Yes">Yes</option>
      </select>


        </div>

      </div>
    </div>


      </div>

<!-- End of Service for -->
<!-- service details starts -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;" >

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ServiceDetails1"  >Service Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;       ">

      <div class="row collapse" id="ServiceDetails1" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont;margin-left: -22px; ">
              <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 12px;">
                            <label>Service Type <span style="color:Red; font-size: 20px;">*</span></label>
                            <select name="servicetype" id="servicetype" required>
                              <option value="@yield('editservicetype')">@yield('editservicetype')</option>
                            @foreach($vertical as $vertical)
                            <option value="{{ $vertical->verticaltype}}">{{ $vertical->verticaltype}}</option>
                            @endforeach
                           </select>

          </div>


          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 10px;">
                            <label>Branch  <span style="color:Red; font-size: 20px;">*</span></label>
                            <select  id="branchalert" >
                            <option value="notseleced"> --Please Select the Servcie Type --</option>
                             </select>


                            <select name="branch" id="branch" required>
                            <option value="@yield('editbranch')">@yield('editbranch')</option>
                              @foreach($branch as $branch)
                              <option value="{{ $branch->name}}">{{ $branch->name}}</option>
                              @endforeach
                             </select>

          </div>

          <div class="col-sm-3" style="    margin-top: 10px;" >

              <label>Assigned To <span style="color:Red; font-size: 20px;">*</span></label>

                           <select name="assignedto" id="assignedto" >
                            <option value="@yield('editassignedto')">@yield('editassignedto')</option>
            @foreach($emp as $emp1)
            <option value="{{ $emp1->FirstName}}">{{ $emp1->FirstName}}  {{ $emp1->Designation}}</option>
            @endforeach
           </select>

           <select  id="assignedtoalert" >
                            <option value="notselecedassign')"> -- Please Select the Branch --</option>

           </select>
           <div id="resrult">

           </div>
          </div>

          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 21px;">
                            <label>General Condition </label>
                            <select name="GeneralCondition" id="GeneralCondition" >
                            <option value="@yield('editGeneralCondition')">@yield('editGeneralCondition')</option>
                              @foreach($condition as $condition)
                              <option value="{{ $condition->conditiontypes}}">{{ $condition->conditiontypes}}</option>
                              @endforeach
                            </select>

          </div>

          <div class="col-sm-12"> </div>
          <div class="col-sm-3" style="    margin-top: 12px;">



              <label>Required On</label><input type="date"  rows="5" name="requesteddate" id="requesteddate" value="@yield('editRequestDateTime')">

          </div>







         <input type="hidden" name="servicestatus" id="servicestatus" value="New">
         <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Gender </label>
                            <select name="preferedgender" id="preferedgender" >
                            <option value="@yield('editpreferedgender')">@yield('editpreferedgender')</option>
            @foreach($gender1 as $gender1)
            <option value="{{ $gender1->gendertypes}}">{{ $gender1->gendertypes}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Language  </label>
                            <select name="preferedlanguage" id="preferedlanguage" >
                            <option value="@yield('editpreferedlanguage')">@yield('editpreferedlanguage')</option>
                            @foreach($language as $language)
            <option value="{{ $language->Languages}}">{{ $language->Languages}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-12"></div>
          <div class="col-sm-3" style="    margin-top: 17px;">
              <label>Quoted Price </label><input type="text"  rows="5" name="quotedprice" id="quotedprice" value="@yield('editquotedprice')">
          </div>

          <div class="col-sm-3" style="    margin-top: 17px;">
              <label>Expected Price </label><input type="text"  rows="5" name="expectedprice" id="expectedprice" value="@yield('editexpectedprice')">
          </div>
          <div class="col-sm-12"></div>
          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 18px;">
                           <div class="form-group" style="    width: 99%;
    margin-left: 0px;">
                            <label >Remarks</label>
                            <textarea class="form-control" rows="5" col="20" name="remarks" id="remarks" value="@yield('editremarks')">@yield('editremarks')</textarea>
                          </div>
          </div>
      </div>
      </div>


<!-- End of service details -->
<!-- Product Deatials -->
<!-- <div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="    margin-left: -4px;
  width: 102%;">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ProductDetails"  >Product Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;     ">

  <div class="row collapse" id="ProductDetails" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px;">
 -->    <!--  <label>SKU ID<input type="text" class="form-control" rows="5" name="SKUid" id="SKUid" value="@yield('editSKUid')"></label>&emsp; -->
<!--
    <div class="col-sm-4" id="firstrow">

      <label>Product Name</label><input type="text"  rows="5" name="ProductName" id="ProductName" value="@yield('editProductName')">
    </div>
    <div class="col-sm-4" id="firstrow">

      <label>Demo Required</label>

      <select name="DemoRequired" id="DemoRequired">
        <option value="@yield('editDemoRequired')">@yield('editDemoRequired')</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </select>

    </div>
    <div class="col-sm-4" id="firstrow">

      <label>Availability Status</label><input type="text"  rows="5" name="AvailabilityStatus" id="AvailabilityStatus" value="@yield('editAvailabilityStatus')">
    </div>
    <div class="col-sm-4" style="margin-top: 26px;">

      <label>Availability Address</label><input type="text"  rows="5" name="AvailabilityAddress" id="AvailabilityAddress" value="@yield('editAvailabilityAddress')">
    </div>
    <div class="col-sm-4" style="margin-top: 26px;">

      <label>Selling Price</label><input type="text"  rows="5" name="SellingPrice" id="SellingPrice" value="@yield('editSellingPrice')">
    </div>
    <div class="col-sm-4" style="margin-top: 26px;    margin-bottom: 13px;">

      <label>Rental Price</label><input type="text"  rows="5" name="RentalPrice" id="RentalPrice" value="@yield('editRentalPrice')">
    </div>
  </div>
</div> -->
<!-- Product details End -->
<!-- Assing Lead To -->
  <div class="container" id="main" style="    margin-top: 23px;">

<div class="row" id="maintitle" style="    margin-left: -4px;
width: 102%;">

<div class="col-sm-12" >
<button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#AssignLeadTo"  >Assign to Coordinator <p style="text-align: -webkit-right;
margin-top: -20px;
margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
</button>

</div>


</div>
</div>
<div class="container" id="main1" style="margin-top: 11px;">

<div class="row collapse" id="AssignLeadTo" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px; ">
<div class="col-sm-4" style="    margin-top: 13px;
margin-bottom: 14px;">
<label>Assign To </label>
<select name="assigned" id="assigned"  value="@yield('editassigned')">
<option value="@yield('editassigned')">@yield('editassigned')</option>
@foreach($emp12 as $emp)

<option value="{{ $emp->FirstName}}">{{ $emp->FirstName}}</option>
@endforeach
</select>

</div>

</div>
</div>
<!-- Assign Lead To Ends -->

<input type="hidden" name="loginname" value="{{ Auth::guard('admin')->user()->name }}">
<input type="hidden" name="addproduct" value="{{$checkbox}}">


    <div class="container" style="    margin-top: 32px;">
          <div class="row">
                <div class="col-sm-12">
                  <center>  <button type="submit" class="btn btn-default" id="submitt" >Update</button></center>
                </div>
          </div>
    </div>
  </div>
</div>
@include('partial.errors')
</form>

</body>

@endsection
