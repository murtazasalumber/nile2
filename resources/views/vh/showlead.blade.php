  <!-- This page is displayed when the View Leads button is clicked by the customer care-->

<?php
$value= Session::all();

$value=session()->getId();
//echo $value;


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Heal</title>
          <!-- <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}"> -->
          <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />
  <link href="css/footable.core.css" rel="stylesheet" type="text/css" />
  <link href="css/footable.metro.css" rel="stylesheet" type="text/css" />
  <script src="js/footable.js" type="text/javascript"></script>

  <script>
  $(document).ready(function(){
    $('.footable').footable();
  $('[data-toggle="tooltip"]').tooltip(); 
    $("#keyword").keyup(function(){

      // take the value of the selected and input by user
      var keyword = $('#keyword').val();
      var filter = $('#filter').val();
      var status = $('#status').val();


      $.ajax({
        type: "GET",
        // this is the route
        url:'allleadc' ,
        data: {'keyword1' : keyword , 'status1':status , 'filter1' : filter,'_token':$('input[name=_token]').val() },
        // This is coming from alldata view page
        success: function(data){
          $('#result').html(data);
        }
      });
    });

//when the user moves from one filter to another, then "empty" the keyword field
$("#filter").change(function(){

              $('#keyword').val("");
             });

  });
  </script>
  <style type="text/css">
  @font-face {
    font-family: myFirstFont;
    src: url(/raleway/Raleway-Regular.ttf);
}
    .footer {
  font-family: myFirstFont;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
  color: #636b6f;
}
.btn:hover
{
  outline: 0;
    background-color: #00C851;
      opacity: 0.5;
}
.imgg {
       margin-top: -14px;
    margin-left: -62px;
}
body
{
  font-family: myFirstFont;
}
.btn
{
        color: #FFF!important;
    background-color: #00C851;
    display: inline-block;
    font-weight: 400;
    text-align: center;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    position: relative;
    cursor: pointer;
    user-select: none;
    z-index: 1;
    font-size: .8rem;
    font-size: 15px;
    border-radius: 2px;
    border: 0;
    transition: .2s ease-out;
    white-space: normal!important;
}
.tooltip.bottom .tooltip-arrow {

  display: none;
  }
.tooltip.bottom {
    padding: 5px 0;
    margin-top: 10px;
}
@media screen and (max-height:1200px) {




}
.filter
{
  width: 21%;
  margin-bottom: 20px;
  margin-top: 10px;
}
.keyword1
{
      margin-left: 36px;
    width: 21%;
}

@media (max-width: 1200px)
{

  .identy
  {
    padding-left: 7%!important;
  }
#filter
   {
        width: 44%;
        padding-top: 19px;
    margin-bottom: 17px;
   }
.keyword1
    {
    margin-right: 50px;
    }
#loo
{
  margin-left: 37%;
}
 #buttonforcreate
  {
      margin-top: 10%;
  }

   #productleadparent
   {
              margin-top: 12px;
    margin-left: 36%;
   }
  .imggg
  {
    text-align: center;
  }
   #filter
   {
        padding-top: 19px;
    margin-bottom: 17px;
   }
   #seacrh
   {
    margin-bottom: 37px;
   }
   #download
   {
    margin-top: 5%;
    margin-left: 24%;
    width: 53%;
   }
   #second
    {
              margin-bottom: 22px;
    margin-left: 36%;
    }
    #productl
    {
          margin-bottom: 16px;
    margin-left: 26%;
    }
}

</style>
</head>
<body style="font-family: myFirstFont;">
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

      <div class="navbar-header">
        <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div id="loo">
        <a class="navbar-brand" rel="home" href="/admin/{{$layout}}" >
          <img class="imgg"
          src="/img/healthheal_logo.png">
        </a></div>
      </div>

      <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav navbar-right" style="text-align: right;">
          <!-- Authentication Links -->

          <!-- if the admin is not logged in show the login button-->
          @if (!Auth::guard('admin')->check())
          <li><a href="\admin">Login</a></li>
          <!-- <li><a href="{{ route('register') }}">Register</a></li> -->

          @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
              {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-content">

              <a href="/admin/{{$layout}}" style="font-family: myFirstFont;"
              >
              Home
            </a>
            <a href="\version">Version Notes</a>
            <a href="{{ route('logout') }}" style="font-family: myFirstFont;"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Logout
          </a>


          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>

        </ul>


      </li>
      @endif
    </ul>
  </div>


</div>

</div>
</div>
<!-- title -->
<div class="container">
  <div class="row">
    <div class="col-sm-12" >
      <center> <h2>Existing Leads</h2></center>
    </div>
  </div>

</div>

<!-- title Ends -->

<!-- sending a hidden value  -->

{{csrf_field()}}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!-- Search Section -->

<div style="margin-left: 5%;margin-top: 40px;">


<div>Please check if the information entered by you matches with the information given below.<br></div>

<div style="margin-top: 15px;">Do you want to create a new lead<br><br>


  <form class="form-horizontal" action="/Verticalhead" method="POST">
  {{csrf_field()}}

  @section('editMethod')
  @show

<button type="submit" class="btn btn-default" id="Yes" >Yes</button> &emsp;

<a href="/admin/{{$layout}}"><input type="button"  class="btn btn-danger" style="    background-color: #ff3547;width: 50px;
    margin-top: 0px;" value="No" ></a>
             




<input type="hidden"  rows="5" name="clientfname" id="clientfname" value="{{$fName}}" >

      <input type="hidden"   rows="5" name="clientmname" id="clientmname" value="{{$mName}}">

      <input type="hidden"  rows="5"  name="clientlname" id="clientlname" value="{{$lName}}" >

      <input type="hidden"  rows="5" rows="5" name="clientemail" id="clientemail" value="{{$EmailId}}">
      <input type="hidden"  rows="5" rows="5" name="code" id="code" value="{{$Country_Code}}">
         
      <input type="hidden"  id="phone"  rows="5" name="clientmob" value="{{$MobileNumber}}">
             

      <input type="hidden" rows="5" name="clientalternateno" id="clientalternateno" value="{{$Alternatenumber}}">
  
      <input type="hidden" rows="5" name="EmergencyContact" id="EmergencyContact" value="{{$EmergencyContact}}">
      
      <input type="hidden" rows="5" name="assesmentreq" id="assesmentreq" value="{{$AssesmentReq}}">
      <input type="hidden" rows="5" name="reference" id="reference" value="{{$Referenceid}}">
 
      <input type="hidden"   rows="5" name="source" id="source" value="{{$Source}}">
  
      <input type="hidden"   rows="5" name="Address1" id="Address1" value="{{$Address1}}">

      <input type="hidden"   rows="5" name="Address2" id="Address2" value="{{$Address2}}">
      <input type="hidden"   rows="5" name="City" id="City" value="{{$City}}">

 
    <input type="hidden"    rows="5" name="District" id="District" value="{{$District}}">
    <input type="hidden"    rows="5" name="State" id="State" value="{{$State}}">
  
    <input type="hidden"    rows="5" name="PinCode" id="PinCode" value="{{$PinCode}}">

    <input type="hidden"    rows="5" name="PAddress1" id="PAddress1" value="{{$PAddress1}}">
  
    <input type="hidden"  rows="5" name="PAddress2" id="PAddress2" value="{{$PAddress2}}">
    <input type="hidden"  rows="5" name="PCity" id="PCity" value="{{$PCity}}">
  
  
  <input type="hidden"  rows="5" name="PDistrict" id="PDistrict" value="{{$PDistrict}}">
  <input type="hidden"  rows="5" name="PState" id="PState" value="{{$PState}}">


  <input type="hidden"  rows="5" name="PPinCode" id="PPinCode" value="{{$PPinCode}}">

  <input type="hidden" name="presentcontact" value="{{$same1}}">
  
    <input type="hidden"    rows="5" name="EAddress1" id="EAddress1" value="{{$EAddress1}}">
 
    <input type="hidden"  rows="5" name="EAddress2" id="EAddress2" value="{{$EAddress2}}">
    <input type="hidden"  rows="5" name="ECity" id="ECity" value="{{$ECity}}">
   
  <input type="hidden"  rows="5" name="EDistrict" id="EDistrict" value="{{$EDistrict}}">
  <input type="hidden"  rows="5" name="EState" id="EState" value="{{$EState}}">


  <input type="hidden"  rows="5" name="EPinCode" id="EPinCode" value="{{$EPinCode}}">

  <input type="hidden" name="emergencycontact" value="{{$same}}">
  
  <input type="hidden"  rows="5" name="patientfname" id="patientfname" value="{{$PtfName}}">

<input type="hidden"  rows="5" name="patientmname" id="patientmname" value="{{$PtmName}}">
   
   <input type="hidden"  rows="5" name="patientlname" id="patientlname" value="{{$PtlName}}">
   
   <input type="hidden"  rows="5" name="age" id="age" value="{{$age}}">
   <input type="hidden"  rows="5" name="gender" id="gender" value="{{$Gender}}">
   <input type="hidden"  rows="5" name="relationship" id="relationship" value="{{$Relationship}}">
  
    
    <input type="hidden"  rows="5" name="Occupation" id="Occupation" value="{{$Occupation}}">
    
    <input type="hidden"  rows="5" name="aadhar" id="aadhar" value="{{$AadharNum}}">
    <input type="hidden"  rows="5" name="AlternateUHIDType" id="AlternateUHIDType" value="{{$AlternateUHIDType}}">
      
    <input type="hidden"  rows="5" name="AlternateUHIDNumber" id="AlternateUHIDNumber" value="{{$AlternateUHIDNumber}}">
    <input type="hidden"  rows="5" name="PTAwareofDisease" id="PTAwareofDisease" value="{{$PTAwareofDisease}}">

    <input type="hidden"  rows="5" name="servicetype" id="servicetype" value="{{$ServiceType}}">
    <input type="hidden"  rows="5" name="branch" id="branch" value="{{$Branch}}">
    <input type="hidden"  rows="5" name="assignedto" id="assignedto" value="{{$AssignedTo}}">
    <input type="hidden"  rows="5" name="GeneralCondition" id="GeneralCondition" value="{{$GeneralCondition}}">
    
<input type="hidden"  rows="5" name="requesteddate" id="requesteddate" value="{{$RequestDateTime}}">
<input type="hidden"  rows="5" name="preferedgender" id="preferedgender" value="{{$PreferedGender}}">
<input type="hidden"  rows="5" name="preferedlanguage" id="preferedlanguage" value="{{$PreferedLanguage}}">

<input type="hidden"  rows="5" name="quotedprice" id="quotedprice" value="{{$QuotedPrice}}">

<input type="hidden"  rows="5" name="expectedprice" id="expectedprice" value="{{$ExpectedPrice}}">
<input type="hidden"  rows="5" name="remarks" id="remarks" value="{{$Remarks}}">
<input type="hidden"  rows="5" name="addproduct" id="addproduct" value="{{$checkbox}}">
<input type="hidden"  rows="5" name="Existing" id="existing" value="notexisting">

<input type="hidden" name="loginname" value="{{$logged_in_user}}">

<input type="hidden" name="servicestatus" id="servicestatus" value="New">
<input type="hidden" name="assigned" id="assigned" value="{{$coordinator_assigned}}">


@include('partial.errors')
</form>
</div>
</div>
<!--   <div class="container">
      <div class="row">
          <div class="col-sm-12" style="    margin-top: 23px;">
           <div style="    margin-left: -69px;">
          <div class="col-sm-4" id="buttonforcreate">
            <a href="/cc/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="second" >
                                          Create Service Lead
                                        </a>&emsp;
                  <div id="productleadparent" style="    display: -webkit-inline-box;">
                 <a href="/product/create?name={{ Auth::guard('admin')->user()->name }}" class="btn btn-info" style="    margin-top: -10px;    background-color: #00C851;" id="procductl">
                                          Create Product Lead
                                        </a>
                                        </div>
                                        </div>
          </div>
              <div class="col-sm-3">
                      <select  id="filter"  >

                               <option>Leads by</option>

                         <option value="fName">Customer Name</option>
                        <option value="AssignedTo">Assigned To</option>
                        <option value="Source">Lead Source</option>
                         <option value="ServiceType">Service Type</option>
                         <option value="Branch">City</option>
                         <option value="MobileNumber">Mobile Number</option>
                         <option value="ServiceStatus">Status</option>
                      </select>
              </div>
              <div class="col-sm-3">
               <input type="hidden"  placeholder='Search' id='keyword'>
               <?php
                if(isset($_GET['status']))
                {
                    $status = $_GET['status'];
                }
                else
                {
                    $status = NULL;
                }
               ?>
               <input type="hidden" name="" id="status" value="<?php echo $status ?>">
              </div>
              <div class="col-sm-2" >
            <?php

if(isset($_GET['status']))
{
$check = $_GET['status'];
}
else
{
    $check = session()->get('status');
}
 ?>
 -->
<!-- Download for product manager view Leads -->
<!-- @if($check=="All")
<a href="/customercaredownloadall?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" id="download" value="Download" class="btn btn-primary" style="background: #4abde8"></a> -->
<!-- <input type="text" value="try"> -->
<!-- @else -->

<!-- Download for count view leads -->
  <!--   <a href="/customercaredownload?status=<?php echo $check;    ?>&download=true"><input type="button" name="download" value="Download" id="download" class="btn btn-primary" style="background: #4abde8"></a>

@endif
          </div>

          </div>


  </div>

</div> -->


<!-- Search Section Ends-->


<!-- This is the URL generation code for download when the User clicks on the "Download" button on View Leads, or "Individual Count" Leads -->
<!-- If the status is present in URL, retrieve from there, else retrieve from session -->


<!-- Reslts -->
<div class="container-fluid" style="margin-top: 50px;" >
  <div class="row" >

    <!-- Overwrite whatever result was received from alldate.blade.php -->
    <div class="col-sm-12" id="result">
      <table class="table footable" style="font-family: Raleway,sans-serif;">
        <thead>
          <!-- This is for displaying the view data headings -->
          <tr>
            <th><b> Lead ID</b></th>
            <th  data-hide="phone,tablet"><b> Created At </b></th>
            <th  data-hide="phone,tablet"><b> Assigned To </b></th>
            <th data-hide="phone,tablet" ><b> Customer Name </b></th>
            <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
            <th data-hide="phone,tablet"><b>City </b></th>
            <!-- <th data-hide="phone,tablet"><b>Email ID</b></th> -->
            <!-- <th data-hide="phone,tablet"><b>Source</b></th> -->
            <th data-hide="phone,tablet" ><b>Service Type</b></th>
            <!-- <th data-hide="phone,tablet" ><b>Follow Up Date</b></th> -->
            <th data-hide="phone,tablet" ><b>Lead Status</b></th>
            <!-- <th data-hide="phone,tablet" ><b>Drop Reason</b></th> -->


          </tr>
        </thead>
        <tbody>
          <!-- This is for displaying the actual data on the page -->

          <?php
                $result = count($leads);

                if($result <= 4)
                {?>

                    <style type="text/css">

                      .footer
                      {
                        margin-top: 20%;
                      }
                    </style>

                    @foreach ($leads as $lead)
          <tr>
            <?php
            if(session()->has('name'))
            {
              $name=session()->get('name');
            }else
            {
              if(isset($_GET['name'])){
                $name=$_GET['name'];
              }else{
                $name=NULL;
              }
            }
            ?>
            <td class="identy">{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td>{{$lead->AssignedTo}}</td>
            <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>

            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>

            <td>{{$lead->ServiceType}}</td>
        
            <td>{{$lead->ServiceStatus}}</td>
   

            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach

               <?php }
                else
                {
            ?>


          @foreach ($leads as $lead)
          <tr>
              <?php
              if(session()->has('name'))
              {
                $name=session()->get('name');
              }else
              {
              if(isset($_GET['name'])){
                 $name=$_GET['name'];
              }else{
                 $name=NULL;
              }
              }
              ?>
            <td class="identy">{{$lead->id}}</td>
            <td>{{$lead->created_at}}</td>
            <td>{{$lead->AssignedTo}}</td>
            <td>{{$lead->fName}} {{$lead->mName}} {{$lead->lName}}</td>

            <td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
            <td>{{$lead->Branch}}</td>

         
            <td>{{$lead->ServiceType}}</td>
           
            <td>{{$lead->ServiceStatus}}</td>
           

            <!--
            <td>

            <form action="{{'/cc/'.$lead->id}}" method="post">
            {{csrf_field()}}
            {{ method_field('DELETE') }}
            <input type="submit" value="Delete">

          </form>

        </td> -->
      </tr>

      @endforeach
      <?php } ?>
    </tbody>
  </table>

  <!-- This is to ensure that the pagination links have parameters that were passed using "GET" for all pages -->
<!-- <div style="text-align: -webkit-center;"> {{$leads->appends(request()->except('page'))->links()}} </div> -->

</div>

</div>

</div>
@extends('layouts.footer')
</body>
</html>








      

 