<!-- This view page is the Lead registration form for the Management -->
<?php
$today = date("Y-m-d");

?>
@extends('layouts.app1')
@section('content')
  <head>
    <link href="{{ URL::asset('css/createlead.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/createlead.js') }}"></script>
    <title>Health Heal</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon-96x96.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/forms.css') }}" />

      <script type="text/javascript">
        $.noConflict();
jQuery(document).ready(function($){

$('#preferedlanguage').multiselect({
            allSelectedText: 'All',
            numberDisplayed: 1,
     });

   });
      </script>
  <script type="text/javascript">
    $(document).ready(function(){

         $("#branch").hide();
      $("#servicetype").change(function(){
          var servicetype = $('#servicetype').val();
          var branch = $('#branch').val();
          if(servicetype == ""){
            $("#branchalert").show();
            $("#branch").hide();
          }else if(branch == "")
          {
             $("#branchalert").hide();
            $("#branch").show();
          }else{
             $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);
             });
            $("#assignedto").hide();
            $("#assignedtoalert").hide();
            $("#resrult").show();
          }
      });
      $("#branch").change(function(){
           var servicetype = $('#servicetype').val();
          var branch = $('#branch').val();
          if(branch == ""){
              $("#branchalert").show();
              $("#branch").hide();
          }else
          {

              $.get("{{ URL::to('testing') }}", { servicetype :servicetype, branch :branch}, function(data){
                $('#resrult').html(data);

             });
            $("#assignedto").hide();
            $("#assignedtoalert").hide();
            $("#resrult").show();

          }
        });
userr=0;
  $("#usersetting").click(function(){

      userr=userr+1;
        if(userr % 2 == 0)
        {

           $("#usersetting").popover('hide');

        }else
        {
           $("#usersetting").popover('show');

          //console.log(sidenvavv);
        }
  });
  $("#bodyy").click(function(){
  // alert("clicked");
      userr=userr+1;
      if(userr % 2 == 0)
        {

        }else
        {
           userr=userr+1;

          //console.log(sidenvavv);
        }

});

   $("#submitt").click(function(event){
      // event.preventDefault();
      var assignto = $("#s").val();
      $("#assigntonew").val(assignto);
      var prefferedlang=$("#preferedlanguage").val();
      $('#Preferredlanguages').val(JSON.stringify(prefferedlang))


    })
  });
  </script>
  </head>
  <body>
    <div class="navbar2">
        <a href="javascript:void(0);" data-toggle="popover" id="usersetting" data-trigger="focus" data-placement="left" title="" data-content="<div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
<p>{{ Auth::guard('admin')->user()->name }}</p>
  </div>
  <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
  <a href='\admin\home' title='' style='color:black'>Home</a>
  </div>

   <br>
  <div style='width: 148%;
    text-align: -webkit-center;
    padding-bottom: 6px;
    margin-left: -15px;
        border-bottom: 1px solid #e9ebee;'>
   <a href='{{ route('logout') }}'
                                            onclick='event.preventDefault();
                                                     document.getElementById('logout-form').submit();'>
                                            Logout
                                        </a>




  </div>"><form id='logout-form' action='{{ route('logout') }}' method='POST' style='display: none;'>
 {{ csrf_field() }}
  </form><img class="img-responsive userdashboard" src="/img/user _dashboard.png" alt="User" id="userimage"></a>
    </div>
    <div id="bodyy">
    <div class="container header">
          <div class="row">
                 <div class="col-md-12">

                    <h2> {{substr(Route::currentRouteName(),20)}} Lead Registration </h2>
                </div>
          </div>
    </div>


    <form class="form-horizontal" action="/Verticalhead" method="POST">
    {{csrf_field()}}
    @section('editMethod')
    @show

        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <button type="button" class="btnCustom btnCustom-default cd custom titlebutton" data-toggle="collapse" data-target="#clientDetails"  >Client Details<p class="firstitle"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found"></p>
              </button>
            </div>
          </div>
        </div>

      <div class="container">
        <div class="row collapse descriptionshow" id="clientDetails">
          <div class="col-sm-3 firstnamecd" style="   ">
              <label>First Name <span style="color:Red; font-size: 20px;">*</span></label>
              <input type="text"  class="charonly" rows="5" name="clientfname" id="clientfname" value="@yield('editclientfname')" required>

          </div>
          <div class="col-sm-3 middlenamecd" id="firstrow">
            <label>Middle Name</label>
            <input type="text"   rows="5" class="charonly" name="clientmname" id="clientmname" value="@yield('editclientmname')">

          </div>
          <div class="col-sm-3 lastnamecd" id="firstrow">
              <label>Last Name</label>
              <input type="text"  rows="5"  class="charonly" name="clientlname" id="clientlname" value="@yield('editclientlname')" >

          </div>
          <div class="col-sm-3 emailidcd" id="firstrow">
            <label>Email ID</label>
            <input type="text"  rows="5" class="mailid" rows="5" name="clientemail" id="clientemail" value="@yield('editclientemail')">

          </div>
          <div class="col-md-12"></div>
          <div class="col-sm-2 topalign secondrow" id="secondrow">
            <label style="  margin-bottom: 5px;">Select Country Code</label><br>
            <select  id="country"  name="code" >
                <option value="+91" name="country_name">India</option>
                @for($i=0;$i<$count;$i++)
                <option value="{{$country_codes[$i]['dial_code']}}" >{{ $country_codes[$i]['name']}}</option>
                @endfor
                <!-- <option value="+1">US</option>
                <option value="+44 ">UK</option> -->
            </select>
          </div>
        <div class="col-sm-4 mobile secondrow" style="margin-top: 26px;">
          <label>Mobile No. <span style="color:Red; font-size: 20px;">*</span></label>
          <div class="row">
              <div class="col-sm-2">
                <input type="text"  id="phone1"  rows="5" required disabled>
              </div>
              <div class="col-sm-10 mobilenumcd">
                  <input type="text"  id="phone"  class="mobileno" rows="5" name="clientmob" value="@yield('editclientmob')" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>

              </div>
          </div>
        </div>
        <div class="col-sm-3 alternatemobilecd topalign secondrow" id="secondrow">
          <label>Alternate Number</label>
          <input type="text" rows="5" class="mobileno" name="clientalternateno" id="clientalternateno" value="@yield('editclientalternateno')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10">

       </div>
      <div class="col-sm-3 emergencymobilecd topalign secondrow" id="secondrow">
       <label>Emergency Contact Number</label>
        <input type="text" rows="5" class="mobileno" name="EmergencyContact" id="EmergencyContact" value="@yield('editEmergencyContact')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10">

      </div>
      <div class="col-sm-12"></div>
      <div class="col-sm-4 thrid" id="secondrow" style="margin-top: 35px;">
        <label style="  margin-bottom: 5px;"> Is Assessment Required? </label><br>
        <select name="assesmentreq" id="assesmentreq">
          <option value="@yield('editassesmentreq')">@yield('editassesmentreq')</option>
          <option value="Yes">Yes</option>
          <option value="No">No</option>
        </select>
      </div>
      <div class="col-sm-4 thridsecond ref" id="secondrow">
        <label style="  margin-bottom: 5px;">Reference<span style="color:Red; font-size: 20px;">*</span></label><br>
        <select name="reference" id="reference"  required>
          <option value="@yield('editreference')">@yield('editreference')</option>
        @foreach($reference as $reference)
          <option value="{{ $reference->Reference}}">{{ $reference->Reference}}</option>
        @endforeach
        </select>
    </div>
    <div class="col-sm-4 thridsecond1" id="secondrow">
      <div id="source">
        <label>Source</label>
        <input type="text"   rows="5" name="source" id="source" value="@yield('editsource')">
      </div>
    </div>
    <div class="col-sm-12" id="secondrow" >
      <h4> <b> Permanent Address </b></h4>
    </div>
    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 1</label>
      <input type="text"   rows="5" name="Address1" id="Address1" value="@yield('editAddress1')">
    </div>
    <div class="col-sm-4" id="secondrow" >
      <label>Address Line 2</label>
      <input type="text"   rows="5" name="Address2" id="Address2" value="@yield('editAddress2')">
    </div>
    <div class="col-sm-4" id="secondrow" style="margin-top: 35px;">
      <label style="  margin-bottom: 5px;">City</label><br>
      <!-- <input type="text"    rows="5" name="City" id="City" value="@yield('editCity')"> -->
      <input type="text"  class="charonly"  rows="5" name="City" id="City" value="@yield('editCity')">

      <!-- <select name="City" id="City" >
      <option value="@yield('editCity')">@yield('editCity')</option>
      @foreach($city as $city)
      <option value="{{ $city->name}}">{{ $city->name}}</option>
      @endforeach
    </select> -->
    </div>
    <div class="col-sm-4 districtpa" id="secondrow" >
      <label>District</label>
      <select  id="District"  name="District" >
          <option value="@yield('editDistrict')">@yield('editDistrict')</option>
          @for($i=0;$i<$count1;$i++)
          <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
          @endfor
          <!-- <option value="+1">US</option>
          <option value="+44 ">UK</option> -->
      </select>
    </div>
  <div class="col-sm-4" id="secondrow" >
    <label>State</label>
  <!--   <input type="text"    rows="5" name="State" id="State" value="@yield('editState')"> -->
     <select name="State" id="State" value="@yield('editState')">
        <option value="@yield('editState')">@yield('editState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
        <option value="Chhattisgarh">Chhattisgarh</option>
        <option value="Chandigarh">Chandigarh</option>
        <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
        <option value="DamanandDiu">Daman and Diu</option>
        <option value="Delhi">Delhi</option>
        <option value="Goa">Goa</option>
        <option value="Haryana">Haryana</option>
        <option value="HimachalPradesh">Himachal Pradesh</option>
        <option value="JammuandKashmir">Jammu and Kashmir</option>
        <option value="Jharkhand">Jharkhand</option>
        <option value="Karnataka">Karnataka</option>
        <option value="Kerala">Kerala</option>
        <option value="MadhyaPradesh">Madhya Pradesh</option>
        <option value="Maharashtra">Maharashtra</option>
        <option value="Manipur">Manipur</option>
        <option value="Meghalaya">Meghalaya</option>
        <option value="Mizoram">Mizoram</option>
        <option value="Nagaland">Nagaland</option>
        <option value="Orissa">Orissa</option>
        <option value="Punjab">Punjab</option>
        <option value="Pondicherry">Pondicherry</option>
        <option value="Rajasthan">Rajasthan</option>
        <option value="Pondicherry">Pondicherry</option>
        <option value="Sikkim">Sikkim</option>
        <option value="TamilNadu">Tamil Nadu</option>
        <option value="Tripura">Tripura</option>
        <option value="UttarPradesh">Uttar Pradesh</option>
        <option value="Uttarakhand">Uttarakhand</option>
        <option value="WestBengal">West Bengal</option>
     </select>
   </div>
  <div class="col-sm-4 pincodeepaa pincodee" id="secondrow" >
    <label>Pincode</label>
    <input type="text" class="numeric"   rows="5" name="PinCode" id="PinCode" value="@yield('editPinCode')" maxlength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
  </div>
  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#PresentAddress" style="    width: 100%;
    margin-left: -1px;" >Present Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
   </button>
  </div>
  <div class="col-sm-12 collapse" id="PresentAddress">
    <div class="col-sm-4" style="    margin-top: 19px;">
      <label>Address Line 1</label>
      <input type="text"    rows="5" name="PAddress1" id="PAddress1" value="@yield('editPAddress1')">
    </div>
    <div class="col-sm-4" style="    margin-top: 19px;">
      <label>Address Line 2</label>
      <input type="text"  rows="5" name="PAddress2" id="PAddress2" value="@yield('editPAddress2')">
    </div>
    <div class="col-sm-4" style="    margin-top: 20px;">
      <label>City</label>

    <!-- <input type="text"  rows="5" name="PCity" id="PCity" value="@yield('editPCity')"> -->

    <input type="text"  rows="5" class="charonly"  name="PCity" id="PCity" value="@yield('editPCity')">



    <!--  <select name="PCity" id="PCity" >
    <option value="@yield('editPCity')">@yield('editPCity')</option>
    @foreach($pcity as $pcity)
    <option value="{{ $pcity->name}}">{{ $pcity->name}}</option>
    @endforeach
  </select> -->
  </div>
  <div class="col-sm-4 distirctpea" style="    margin-top: 28px;">
    <label>District</label>
    <select  id="PDistrict"  name="PDistrict" >

        <option value="@yield('editPDistrict')">@yield('editPDistrict')</option>
        @for($i=0;$i<$count1;$i++)
        <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
        @endfor
        <!-- <option value="+1">US</option>
        <option value="+44 ">UK</option> -->
    </select>

  </div>
  <div class="col-sm-4" style="    margin-top: 28px;">
    <label>State</label>
<!--   <input type="text"  rows="5" name="PState" id="PState" value="@yield('editPState')"> -->
   <select name="PState" id="PState" value="@yield('editPState')">
        <option value="@yield('editPState')">@yield('editPState')</option>
        <option value="AndhraPradesh">Andhra Pradesh</option>
        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
        <option value="Assam">Assam</option>
        <option value="Bihar">Bihar</option>
        <option value="Chhattisgarh">Chhattisgarh</option>
        <option value="Chandigarh">Chandigarh</option>
        <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
        <option value="DamanandDiu">Daman and Diu</option>
        <option value="Delhi">Delhi</option>
        <option value="Goa">Goa</option>
        <option value="Haryana">Haryana</option>
        <option value="HimachalPradesh">Himachal Pradesh</option>
        <option value="JammuandKashmir">Jammu and Kashmir</option>
        <option value="Jharkhand">Jharkhand</option>
        <option value="Karnataka">Karnataka</option>
        <option value="Kerala">Kerala</option>
        <option value="MadhyaPradesh">Madhya Pradesh</option>
        <option value="Maharashtra">Maharashtra</option>
        <option value="Manipur">Manipur</option>
        <option value="Meghalaya">Meghalaya</option>
        <option value="Mizoram">Mizoram</option>
        <option value="Nagaland">Nagaland</option>
        <option value="Orissa">Orissa</option>
        <option value="Punjab">Punjab</option>
        <option value="Pondicherry">Pondicherry</option>
        <option value="Rajasthan">Rajasthan</option>
        <option value="Pondicherry">Pondicherry</option>
        <option value="Sikkim">Sikkim</option>
        <option value="TamilNadu">Tamil Nadu</option>
        <option value="Tripura">Tripura</option>
        <option value="UttarPradesh">Uttar Pradesh</option>
        <option value="Uttarakhand">Uttarakhand</option>
        <option value="WestBengal">West Bengal</option>
    </select>
  </div>
  <div class="col-sm-4 pincodeprea pincodee" style="    margin-top: 28px;">
    <label>Pincode</label>
    <input type="text"  rows="5" class="numeric"  name="PPinCode" id="PPinCode" value="@yield('editPPinCode')" maxlength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

  </div>

</div>

    <div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
      <div style=" margin-top: 14px;" id="checkbox">
        <label class="switch">
        <input type="checkbox" name="presentcontact" value="same" style="    margin-left: -98px;" id="sameaspermanentadress">
        <span class="slider round"></span>
        </label>&emsp;
        <label>Same as Permanent Address</label>
      </div>
    </div>


  <div class="col-sm-12" style="    margin-top: 30px;" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#EmergencyAddress" style="    width: 100%;
    margin-left: -1px;" >Emergency Address<p style="text-align: -webkit-right;
    margin-top: -20px;
    margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>
  </div>
  <div class="col-sm-12 collapse" id="EmergencyAddress">
    <div class="col-sm-4" style="    margin-top: 19px;">
      <label>Address Line 1</label>
      <input type="text"    rows="5" name="EAddress1" id="EAddress1" value="@yield('editEAddress1')">
    </div>
    <div class="col-sm-4" style="    margin-top: 19px;">
      <label>Address Line 2</label>
      <input type="text"  rows="5" name="EAddress2" id="EAddress2" value="@yield('editEAddress2')">
    </div>
    <div class="col-sm-4" style="    margin-top: 20px;">
      <label>City</label>
      <!-- <input type="text"  rows="5" name="ECity" id="ECity" value="@yield('editECity')"> -->
      <input type="text" class="charonly" rows="5" name="ECity" id="ECity" value="@yield('editECity')">
      <!--  <select name="ECity" id="ECity" >
        <option value="@yield('editECity')">@yield('editECity')</option>
        @foreach($ecity as $ecity)
        <option value="{{ $ecity->name}}">{{ $ecity->name}}</option>
          @endforeach
       </select>
      -->
    </div>
    <div class="col-sm-4 distrea" style="    margin-top: 28px;">
      <label>District</label>
      <select  id="EDistrict"  name="EDistrict" >
        <option value="@yield('editEDistrict')">@yield('editEDistrict')</option>
        @for($i=0;$i<$count1;$i++)
        <option value="{{$indian_cities[$i]['name']}}">{{ $indian_cities[$i]['name']}}</option>
        @endfor
        <!-- <option value="+1">US</option>
        <option value="+44 ">UK</option> -->
      </select>
    </div>
    <div class="col-sm-4" style="    margin-top: 28px;">
      <label>State</label>
<!--   <input type="text"  rows="5" name="EState" id="EState" value="@yield('editEState')"> -->
        <select name="PState" id="EState" value="@yield('editEState')">
            <option value="@yield('editPState')">@yield('editEState')</option>
            <option value="AndhraPradesh">Andhra Pradesh</option>
            <option value="Arunachal Pradesh">Arunachal Pradesh </option>
            <option value="Assam">Assam</option>
            <option value="Bihar">Bihar</option>
            <option value="Chhattisgarh">Chhattisgarh</option>
            <option value="Chandigarh">Chandigarh</option>
            <option value="DadraandNagarHaveli">Dadra and Nagar Haveli</option>
            <option value="DamanandDiu">Daman and Diu</option>
            <option value="Delhi">Delhi</option>
            <option value="Goa">Goa</option>
            <option value="Haryana">Haryana</option>
            <option value="HimachalPradesh">Himachal Pradesh</option>
            <option value="JammuandKashmir">Jammu and Kashmir</option>
            <option value="Jharkhand">Jharkhand</option>
            <option value="Karnataka">Karnataka</option>
            <option value="Kerala">Kerala</option>
            <option value="MadhyaPradesh">Madhya Pradesh</option>
            <option value="Maharashtra">Maharashtra</option>
            <option value="Manipur">Manipur</option>
            <option value="Meghalaya">Meghalaya</option>
            <option value="Mizoram">Mizoram</option>
            <option value="Nagaland">Nagaland</option>
            <option value="Orissa">Orissa</option>
            <option value="Punjab">Punjab</option>
            <option value="Pondicherry">Pondicherry</option>
            <option value="Rajasthan">Rajasthan</option>
            <option value="Pondicherry">Pondicherry</option>
            <option value="Sikkim">Sikkim</option>
            <option value="TamilNadu">Tamil Nadu</option>
            <option value="Tripura">Tripura</option>
            <option value="UttarPradesh">Uttar Pradesh</option>
            <option value="Uttarakhand">Uttarakhand</option>
            <option value="WestBengal">West Bengal</option>
        </select>
      </div>
    <div class="col-sm-4 pincodeema pincodee" style="    margin-top: 28px;">
        <label>Pincode</label>
        <input type="text"  rows="5" class="numeric"  name="EPinCode" id="EPinCode" value="@yield('editEPinCode')" maxlength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

    </div>
  </div>


  <div class="col-sm-12 col-sm-offset-1" style="margin-left: 20px;" >
    <div style=" margin-top: 14px;" id="checkbox">
      <label class="switch">
        <input type="checkbox" name="emergencycontact" value="same" style="    margin-left: -98px;" id="Esameaspermananentaddress">
        <span class="slider round"></span>
         </label>&emsp;
        <label>Same as Permanent Address</label>
    </div>
  </div>
  </div>
</div>


<!-- service details starts -->
<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" >

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ServiceDetails1"  >Service Details <p style="text-align: -webkit-right;
      margin-top: -20px;
      margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;       ">

      <div class="row collapse descriptionshow" id="ServiceDetails1">
              <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 12px;">
                            <label>Service Type <span style="color:Red; font-size: 20px;">*</span></label>
                            <select name="servicetype" id="servicetype" required>
                              <option value="@yield('editservicetype')">@yield('editservicetype')</option>
                            @foreach($vertical as $vertical)
                            <option value="{{ $vertical->verticaltype}}">{{ $vertical->verticaltype}}</option>
                            @endforeach
                           </select>

          </div>


          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 10px;">
                            <label>Branch  <span style="color:Red; font-size: 20px;">*</span></label>
                            <select  id="branchalert" >
                            <option value="notseleced"> --Please Select the Servcie Type --</option>
                             </select>


                            <select name="branch" id="branch" required>
                            <option value="@yield('editbranch')">@yield('editbranch')</option>
                              @foreach($branch as $branch)
                              <option value="{{$branch->name}}">{{ $branch->name}}</option>
                              @endforeach
                             </select>

          </div>


           <div class="col-sm-3" style="    margin-top: 10px;" >

              <label>Assigned To <span style="color:Red; font-size: 20px;">*</span></label>

         {{--                   <select  id="assignedto" >
                            <option value="@yield('editassignedto')">@yield('editassignedto')</option>
            @foreach($emp as $emp1)
            <option value="{{ $emp1->FirstName}}">{{ $emp1->FirstName}}  {{ $emp1->Designation}}</option>
            @endforeach
           </select> --}}

           <select  id="assignedtoalert" >
                            <option value="notselecedassign')"> -- Please Select the Branch --</option>

           </select>
           <div id="resrult">

           </div>
          </div>




          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 21px;">
                            <label>General Condition </label>
                            <select name="GeneralCondition" id="GeneralCondition" >
                            <option value="@yield('editGeneralCondition')">@yield('editGeneralCondition')</option>
                              @foreach($condition as $condition)
                              <option value="{{ $condition->conditiontypes}}">{{ $condition->conditiontypes}}</option>
                              @endforeach
                            </select>

          </div>



          <div class="col-sm-12"> </div>

          <div class="col-sm-3" style="    margin-top: 12px;">



              <label>Required On</label><input type="date"  rows="5" name="requesteddate" id="requesteddate" value="@yield('editRequestDateTime')" min="<?php echo $today; ?>">

          </div>








         <input type="hidden" name="servicestatus" id="servicestatus" value="New">
         <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Gender </label>
                            <select name="preferedgender" id="preferedgender" >
                            <option value="@yield('editpreferedgender')">@yield('editpreferedgender')</option>
            @foreach($gender1 as $gender1)
            <option value="{{ $gender1->gendertypes}}">{{ $gender1->gendertypes}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-6" style="     margin-bottom: 13px;   margin-top: 17px;">
                            <label>Preferred Language  </label><br>
                            <select name="preferedlanguage" id="preferedlanguage"  multiple="multiple">
                           <!--  <option value="@yield('editpreferedlanguage')">@yield('editpreferedlanguage')</option> -->
                            @foreach($language as $language)
            <option value="{{ $language->Languages}}">{{ $language->Languages}}</option>
            @endforeach
           </select>
          </div>
          <div class="col-sm-12"></div>
          <div class="col-sm-3 quotedp" style="    margin-top: 17px;">
              <label>Quoted Price </label>
              <input type="text"  rows="5" name="quotedprice" id="quotedprice" value="@yield('editquotedprice')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
              <p class="indiancurrency"><b>  INR </b></p>
          </div>

          <div class="col-sm-3" style="    margin-top: 17px;">
              <label>Expected Price </label><input type="text"  rows="5" name="expectedprice" id="expectedprice" value="@yield('editexpectedprice')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
               <p class="indiancurrency"><b>  INR </b></p>
          </div>
          <div class="col-sm-12"></div>
          <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 18px;">
                           <div class="form-group" style="    width: 99%;
    margin-left: 0px;">
                            <label >Remarks</label>
                            <textarea class="form-control" rows="5" col="20" name="remarks" id="remarks" value="@yield('editremarks')">@yield('editremarks')</textarea>
                          </div>
          </div>
      </div>
      </div>

<!-- Servcie for -->


<div class="container" id="main" style="    margin-top: 23px;">

  <div class="row" id="maintitle" style="  ">

  <div class="col-sm-12" >
    <button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#ServiceDetails"  >Service Required For<p class="firstitle"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found"></p>
    </button>

  </div>

</div>
</div>

<div class="container" id="main1" style="margin-top: 11px;        ">

  <div class="row collapse descriptionshow" id="ServiceDetails">
    <div class="col-sm-3 fnamepatient" id="firstrow">
      <label> First Name</label>
      <input type="text" class="charonly" rows="5" name="patientfname" id="patientfname" value="@yield('editpatientfname')">

    </div>
    <div class="col-sm-3 mnamepatient" id="firstrow">
      <label>Middle Name</label>
      <input type="text"  class="charonly" rows="5" name="patientmname" id="patientmname" value="@yield('editpatientmname')">

    </div>
    <div class="col-sm-3 lastnamep" id="firstrow">
      <label>Last Name</label>
      <input type="text" class="charonly" rows="5" name="patientlname" id="patientlname" value="@yield('editpatientlname')">

    </div>
    <div class="col-sm-3 agee" id="firstrow">

      <label>Age </label><input type="number" class="agepatient" rows="5" name="age" id="age" value="@yield('editage')" maxlength="3"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' min=1 max=150>
    </div>
    <div class="col-sm-3 topalign" style="    margin-top: 29px;">
      <label>Gender</label>
      <select name="gender" id="gender" >
        <option value ="@yield('editgender')">@yield('editgender')</option>
        @foreach($gender as $gender)
        <option value="{{ $gender->gendertypes}}">{{ $gender->gendertypes}}</option>
        @endforeach
      </select>

    </div>
    <div class="col-sm-3 topalign align" style="    margin-top: 29px;">
      <label>Relationship</label>
      <select name="relationship" id="relationship" >
        <option value="@yield('editrelationship')">@yield('editrelationship')</option>
        @foreach($relation as $relation)
        <option value="{{ $relation->relationshiptype}}">{{ $relation->relationshiptype}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3 topalign align" style="    margin-top: 28px;">
      <label>Occupation </label>
      <input type="text"  rows="5" name="Occupation" id="Occupation" value="@yield('editOccupation')">
    </div>
    <div class="col-sm-3 topalign align" style="    margin-top: 28px;">
      <label>Aadhar Number </label>
      <input type="text"  rows="5" name="aadhar" id="aadhar" value="@yield('editaadhar')" maxlength="14">
    </div>
    <div class="col-sm-3" style="    margin-top: 33px;">
      <label>Alternate UHID Type </label>
      <select name="AlternateUHIDType" id="AlternateUHIDType"  value="@yield('editAlternateUHIDType')">
        <option value="@yield('editAlternateUHIDType')">@yield('editAlternateUHIDType')</option>
        <option value="PAN Card">PAN Card</option>
        <option value="Driving Licence">Driving Licence </option>
        <option value="Passport">Passport</option>
        <option value="Bank Passbook">Bank Passbook</option>
        <option value="Voter ID">Voter ID</option>
        <option value="Cellphone Postpaid Bill">Cellphone Postpaid Bill</option>
        <option value="Water Bill">Water Bill</option>
        <option value="Electricity Bill">Electricity Bill</option>
        <option value="Telephone Bill">Telephone Bill</option>
        <option value="Gas Consumer Number">Gas Consumer Number</option>
        <option value="Employee ID issued by defence">Employee ID issued by defence</option>

      </select>

    </div>
    <div class="col-sm-3" style="    margin-top: 33px;">
      <label>Alternate UHID Number </label><input type="text"  rows="5" name="AlternateUHIDNumber" id="AlternateUHIDNumber" value="@yield('editAlternateUHIDNumber')">
    </div>
    <div class="col-sm-3" style="     margin-bottom: 13px;   margin-top: 34px;">
      <label>Is Patient aware of Disease / Diagnosis?</label>
      <select name="PTAwareofDisease" id="PTAwareofDisease"  value="@yield('editPTAwareofDisease')">
        <option value="@yield('editPTAwareofDisease')">@yield('editPTAwareofDisease')</option>
        <option value="No">No</option>
        <option value="Yes">Yes</option>
      </select>


        </div>

      </div>
    </div>


      </div>
<!-- Assing Lead To -->
  <div class="container" id="main" style="    margin-top: 23px;">

<div class="row" id="maintitle" style="    margin-left: -4px;
width: 102%;">

<div class="col-sm-12" >
<button type="button" class="btnCustom btnCustom-default custom" data-toggle="collapse" data-target="#AssignLeadTo"  >Assign to Coordinator <p style="text-align: -webkit-right;
margin-top: -20px;
margin-right: 16px;"><img src="/img/drop-down-arrow.png" class="img-responsive" alt="oops! Not found">          </p>
</button>

</div>


</div>
</div>
<div class="container" id="main1" style="margin-top: 11px;">

<div class="row collapse" id="AssignLeadTo" style=" border-radius: 11px; border: 1px solid #808080; width: 103%; font-family: myFirstFont; margin-left: -22px; ">
<div class="col-sm-4" style="    margin-top: 13px;
margin-bottom: 14px;">
<label>Assign To </label>
<select name="assigned" id="assigned"  value="@yield('editassign')">
<option value="@yield('editassign')">@yield('editassign')</option>
@foreach($emp as $emp)

<option value="{{ $emp->FirstName}}">{{ $emp->FirstName}}</option>
@endforeach
</select>

</div>

</div>
</div>
<!-- Assign Lead To Ends -->


<!-- checkbox to add service details with product -->
<div style="margin-left: 3%; margin-top: 20px;" id="toggle">
<label class="switch">
  <input type="checkbox" name="addproduct" value="add" style="" id="addproduct">
  <span class="slider round"></span>

</label>&emsp;
<label>Add Products along with it..</label>

</div>

<!-- end -->



<?php $loginname=$_GET['name'];

?>

<input type="hidden" name="loginname" value="<?php echo $loginname;?>">
<input type="hidden"  rows="5" name="existing" id="existing" value="existing">
<input type="hidden" name="assigntonew" id="assigntonew" value="">
<input type="hidden" name="Preferredlanguages" id="Preferredlanguages" value="">

    <div class="container" style="    margin-top: 10px;">
          <div class="row">
                <div class="col-sm-12">
                 <!--  <center>  <button type="submit" class="btn btn-default" id="submitt" >Submit</button></center> -->


                   <center><button type="reset" class="btn btn-danger" style="    background-color: #ff3547;" >Reset</button>
              &emsp;
                <button type="submit" class="btn btn-default" id="submitt" >Submit</button>
                   </center>
                </div>
          </div>
    </div>
  </div>
</div>
@include('partial.errors')
</form>

</body>

@endsection
