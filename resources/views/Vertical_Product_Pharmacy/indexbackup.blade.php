@extends('layouts.Vertical_Product_Pharmacy')

@section('title','leads')
@section('content')


<!-- <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-top: 20px;    margin-right: 17px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" rel="home" href="/admin/vertical" title="Health Heal">
                <img class="imgg" 
                     src="img/healthheal_logo.png">
            </a>
        </div>
       
         <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="/admin/vertical" style="font-size: 15px;">Home</a></li>
                
                
            </ul>
           
        </div>

    </div>
</div> -->




<div class="container">
  <div class="row" style="    margin-top: -17px;">
    <div class="col-md-12" style="text-align: center;">
      <h2>Leads </h2>
      
    </div>

  </div>
</div>

{{csrf_field()}}
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="container">
  <div class="row">
    <div class="col-sm-4">
        <div class="styled-select" > 

            <select id="filter" >
                  <option>Leads by</option>
                   
                         
                        <option value="leads.lName">l name</option>
                      
                        <option value="leads.fName">first Name</option>

             </select>

          </div>
         </div>
    <div class="col-sm-4">
    <input type='text' placeholder='Search' id='keyword' />
    </div>
    <div class="col-sm-4">
     </div>
    </div>
  </div>
</div>





<div class="container-fluid" style="margin-top: 50px;" >
<div class="row" >
  <div class="col-sm-12" id="result">
        <table class="table footable" style="font-family: Raleway,sans-serif;">
          <thead>

              <tr>
                    <th><b> Lead ID</b></th>
                    <th  data-hide="phone,tablet"><b> Created At </b></th>
                    <th  data-hide="phone,tablet"><b> Created By </b></th>
                    <th data-hide="phone,tablet" ><b> Customer Name </b></th>
                    <th data-hide="phone,tablet" ><b>Customer Mobile </b></th>
                    <th data-hide="phone,tablet"><b>City </b></th>
                    <th data-hide="phone,tablet"><b>Email ID</b></th>
                    <th data-hide="phone,tablet"><b>Source</b></th>
                    <th data-hide="phone,tablet" ><b>Service Type</b></th>
                    <th data-hide="phone,tablet" ><b>Lead Status</b></th>
                    
                
                </tr>
            </thead>
            <tbody>

  @foreach ($leads as $lead)
<tr>
<?php 
if(session()->has('name'))
{
	$name=session()->get('name');
}else
{
if(isset($_GET['name'])){
   $name=$_GET['name'];
}else{
   $name=NULL;
}
}
?>
  	<td><a href="/vh/create?id={{$lead->id}}&name=<?php echo $name?>">{{$lead->id}}</a></td>
  	<td>{{$lead->created_at}}</td>
  	<td>{{$lead->createdby}}</td>
	<td>{{$lead->fName}}</td>
	<td>{{$lead->Country_Code}}{{$lead->MobileNumber}}</td>
	<td>{{$lead->City}}</td>
	<td>{{$lead->EmailId}}</td>
	<td>{{$lead->Source}}</td>
	<td>{{$lead->ServiceType}}</td>
	<td>{{$lead->ServiceStatus}}</td>
	
<!-- 
<td>

<form action="{{'/cc/'.$lead->id}}" method="post">
{{csrf_field()}}
{{ method_field('DELETE') }}
<input type="submit" value="Delete">

</form>

    </td> -->
   <!--  </tr> -->

@endforeach
 </tbody>
          </table>
          <div style="text-align: -webkit-center;"> {{$leads->links()}} </div>
  </div>
</div>
</div>

@endsection

 <script src="{{ asset('js/app.js') }}"></script>

@section('body')

@endsection