<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assessment extends Model
{
  //for getting the column names of a particular table
  public function getTableColumns()
  {
    return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
  }
}
