<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

        ],



        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'customercare' => \App\Http\Middleware\CustomercareMiddleware::class,
        'coordinator' => \App\Http\Middleware\CoordinatorMiddleware::class,
        'superuser' => \App\Http\Middleware\Super_userMiddleware::class,
        'management' => \App\Http\Middleware\ManagementMiddleware::class,
        'careprovider' => \App\Http\Middleware\Care_providerMiddleware::class,
        'referalpartner' => \App\Http\Middleware\ReferalpartnerMiddleware::class,
        'vertical' => \App\Http\Middleware\VerticalMiddleware::class,
        'productmanager' => \App\Http\Middleware\productmanagerMiddleware::class,
        'pharmacymanager' => \App\Http\Middleware\pharmacymanagerMiddleware::class,
        'fieldofficer' => \App\Http\Middleware\fieldofficerMiddleware::class,
        'fieldexecutive' => \App\Http\Middleware\fieldexecutiveMiddleware::class,
        'marketing' => \App\Http\Middleware\marketingMiddleware::class,
        'admin' => \App\Http\Middleware\AdminMiddleware::class,
        'admin' => \App\Http\Middleware\AdminMiddleware::class,
        'branch_head' => \App\Http\Middleware\Branch_HeadMiddleware::class,
        'coordinator_fieldexecutive' => \App\Http\Middleware\Coordinator_FieldExecutiveMiddleware::class,
        'coordinator_fieldofficer' => \App\Http\Middleware\Coordinator_FieldOfficerMiddleware::class,
        'coordinator_pharmacymanager' => \App\Http\Middleware\Coordinator_PharmacyManagerMiddleware::class,
        'coordinator_product_pharmacy' => \App\Http\Middleware\Coordinator_Product_PharmacyMiddleware::class,
        'coordinator_productmanager' => \App\Http\Middleware\Coordinator_ProductManagerMiddleware::class,
        'coordinator_productrental_pharmacy' => \App\Http\Middleware\Coordinator_ProductRental_PharmacyMiddleware::class,
        'coordinator_productrental' => \App\Http\Middleware\Coordinator_ProductRentalMiddleware::class,
        'coordinator_productselling_pharmacy' => \App\Http\Middleware\Coordinator_ProductSelling_PharmacyMiddleware::class,
        'coordinator_productselling' => \App\Http\Middleware\Coordinator_ProductSellingMiddleware::class,
        'vertical_fieldexecutive' => \App\Http\Middleware\Vertical_FieldExecutiveMiddleware::class,
        'vertical_fieldofficer' => \App\Http\Middleware\Vertical_FieldOfficerMiddleware::class,
        'vertical_pharmacymanager' => \App\Http\Middleware\Vertical_PharmacyManagerMiddleware::class,
        'vertical_product_pharmacy' => \App\Http\Middleware\Vertical_Product_PharmacyMiddleware::class,
        'vertical_productmanager' => \App\Http\Middleware\Vertical_ProductManagerMiddleware::class,
        'vertical_productrental_pharmacy' => \App\Http\Middleware\Vertical_ProductRental_PharmacyMiddleware::class,
        'vertical_productrental' => \App\Http\Middleware\Vertical_ProductRentalMiddleware::class,
        'vertical_productselling_pharmacy' => \App\Http\Middleware\Vertical_ProductSelling_PharmacyMiddleware::class,
        'vertical_productselling' => \App\Http\Middleware\Vertical_ProductSellingMiddleware::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
}
