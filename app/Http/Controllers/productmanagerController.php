<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Auth;
use DB;

class productmanagerController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('productmanager');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');

$name = Auth::guard('admin')->user()->name;
        $designation="productmanager";

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";


         $status00 = "New";
        $status11 = "Processing";
        $status22  = "Awaiting Pickup";
        $status33  = "Out for Delivery";
        $status44  = "Ready to ship";
        $status55  = "Order Return";
        $status66  = "Cancelled";
        $status77  = "Delivered";
        $status88  = "Received Order Return";

          $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
        
        // dd($check);

        if($check=="Product - Selling")
        {
            $newcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status0)
            ->orderBy('id', 'DESC')
            ->count();

            $processingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status1)
            ->orderBy('id', 'DESC')
            ->count();

            $awaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status2)
            ->orderBy('id', 'DESC')
            ->count();

            $outfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status3)
            ->orderBy('id', 'DESC')
            ->count();

            $readytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status4)
            ->orderBy('id', 'DESC')
            ->count();

            $orderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status5)
            ->orderBy('id', 'DESC')
            ->count();

            $recievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status8)
            ->orderBy('id', 'DESC')
            ->count();

            $canceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status6)
            ->orderBy('id', 'DESC')
            ->count();

            $deliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status7)
            ->orderBy('id', 'DESC')
            ->count();
        }
        else
        {
            if($check=="Product")
            {
                // product manager dashboard count

                  $productnewcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status00)
            
            ->orderBy('id', 'DESC')
            ->count();
            // dd($productnewcount);

            $productprocessingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status11)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productawaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status22)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productoutfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status33)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productreadytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status44)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status55)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productrecievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status88)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productcanceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status66)
            
            ->orderBy('id', 'DESC')
            ->count();

            $productdeliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status77)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productnewcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status00)
            
            ->orderBy('id', 'DESC')
            ->count();

            $productprocessingcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status11)
            
            ->orderBy('id', 'DESC')
            ->count();

            $productawaitingpickupcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status22)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productoutfordeliverycountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status33)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productreadytoshipcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status44)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productorderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status55)
            
            ->orderBy('id', 'DESC')
            ->count();


            $productrecievedorderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status88)
            
            ->orderBy('id', 'DESC')
            ->count();

            $productcanceledcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status66)
            
            ->orderBy('id', 'DESC')
            ->count();



            $productdeliveredcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status77)
            
            ->orderBy('id', 'DESC')
            ->count();


            session()->put('name',$name);
        return view('admin.productmanagerall',compact('productnewcount','productprocessingcount','productawaitingpickupcount','productoutfordeliverycount','productreadytoshipcount','productorderreturncount','productrecievedorderreturncount','productcanceledcount','productdeliveredcount','productnewcountrent','productprocessingcountrent','productawaitingpickupcountrent','productoutfordeliverycountrent','productreadytoshipcountrent','productorderreturncountrent','productrecievedorderreturncountrent','productcanceledcountrent','productdeliveredcountrent'));

        // product count end

                //product manager end
            }
            else
            {

            $newcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status0)
            ->orderBy('id', 'DESC')
            ->count();

            $processingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status1)
            ->orderBy('id', 'DESC')
            ->count();

            $awaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status2)
            ->orderBy('id', 'DESC')
            ->count();

            $outfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status3)
            ->orderBy('id', 'DESC')
            ->count();

            $readytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status4)
            ->orderBy('id', 'DESC')
            ->count();

            $orderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status5)
            ->orderBy('id', 'DESC')
            ->count();

            $recievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status8)
            ->orderBy('id', 'DESC')
            ->count();

            $canceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status6)
            ->orderBy('id', 'DESC')
            ->count();

            $deliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status7)
            ->orderBy('id', 'DESC')
            ->count();
            }
        }

        return view('admin.productmanager',compact('newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation','recievedorderreturncount'));
    }

    public function assigned()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');

        if($check=="Product - Selling")
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        else
        {

        if($check=="Product")
        {
            if(isset($_GET['type']))
            {
            $type=$_GET['type'];
            }
            else
            {
                 $type=NULL;
            }

            if($type=="Sell")
             {

             $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
            }
            else
            {

             $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
            // dd($leads);
            }
        }
        else
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        }
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation2;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department2;


                if($check=="Product - Selling")
                {

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('products.Type','Sell')
                    ->orwhere('products.Type','')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    if($check=="Product")
                    {

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                     }
                     else
                    {
                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('products.Type','Rent')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                    }
                }


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Source', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                if($check=="Product - Selling")
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatus',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    if($check=="Product")
                    {
                        // dd($status);
                        if(isset($_GET['type']))
                        {
                        $type=$_GET['type'];
                        }
                        else
                        {
                             $type=NULL;
                        }

                         if($type=="Sell")
                        {


                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatus',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                        }
                        else
                        {
                            $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatusrent',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                        }
                    }
                    else
                    {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatusrent',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                    }
                }

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Source', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        session()->put('name',$logged_in_user);

        return view('product.productindex',compact('leads'));
    }
}
