<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use \PDF;

class TestPdfController extends Controller
{
    public function index1()
    {
        return view('pdf/pdfview1');
    }

    public function index(Request $request)
    {
        // dd($info);
        if($request->has('download'))
        {
            $leadid = 3454;
            $pdf = PDF::loadView('pdf/pdfview');
            return $pdf->download("Assessment_Form_for_$leadid.pdf");
        }

        return view('pdf/pdfview');
    }
}
