<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Log;
use App\lead;
use App\personneldetail;
use App\service;
use App\address;
use App\language;
use App\product;
use App\enquiryproduct;
use App\Activity;
use App\shiftrequired;
use Session;
use Illuminate\Contracts\Auth\Guard;
use Mail;
use App\pharmacy;
use App\prodlead;
use App\order;
use Auth;
use Illuminate\Support\Facades\Response;


use App\Http\Requests;


class productController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {
            if (session()->has('name'))
            {

                $name=session()->get('name');
            }
            else{
                $name=Auth::guard('admin')->user()->name;
            }

            $d=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $d2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $d3=DB::table('employees')->where('FirstName',$name)->value('Designation3');
            $eid=DB::table('employees')->where('FirstName',$name)->value('id');
            $log_id=DB::table('employees')->where('FirstName',$name)->value('id');

            $product= product::paginate(50);

            $p=DB::table('products')->get();


            $checkid=DB::table('admins')->where('name',$name)->value('id');
            $check=DB::table('role_admins')->where('admin_id',$checkid)->value('role_id');
            $check1=DB::table('roles')->where('id',$check)->value('name');

            $city=DB::table('employees')->where('FirstName',$name)->value('city');
            // dd($city);
            // dd($check);
            // dd($check1);

            if($check1=="Management")

            {
                $leads=DB::table('leads')

                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);
                // dd($leads);


                session()->put('name',$name);
                return view('management.allproductindex',compact('product','name','d','p','leads'));
            }
            else{
                if($check1=="admin")

                {
                    $leads=DB::table('leads')

                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);

                    // dd($leads);

                    session()->put('name',$name);
                    return view('admin.allproductindex',compact('product','name','d','p','leads'));
                }

            }

            if($check1=="Product Manager")
            {

                $pcheck=DB::table('employees')->where('FirstName',$name)->value('Department2');

                if($pcheck=="Product - Selling")
                {
                    $leads=DB::table('leads')
                    ->select('addresses.*','leads.*','prodleads.*','products.*')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('products.Type',"Sell")
                    ->orderBy('products.id', 'DESC')
                    ->paginate(50);

                    session()->put('name',$name);
                    return view('product.productindex',compact('product','name','d2','p','leads'));
                }
                else
                {
                    if($pcheck=="Product")
                    {
                        $leads=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'leads.id', '=', 'products.leadid')
                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                        ->orderBy('products.id', 'DESC')
                        ->paginate(50);

                        session()->put('name',$name);
                        return view('product.productindex',compact('product','name','d2','p','leads'));
                    }
                    else
                    {
                        $leads=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'leads.id', '=', 'products.leadid')
                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                        ->where('products.Type',"Rent")
                        ->orderBy('products.id', 'DESC')
                        ->paginate(50);

                        session()->put('name',$name);
                        return view('product.productindex',compact('product','name','d2','p','leads'));
                    }
                }




            }
            else
            {
                if($check1=="Pharmacy Manager")
                {
                    $leads=DB::table('leads')
                    ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                    ->orderBy('pharmacies.id', 'DESC')
                    ->paginate(50);

                    session()->put('name',$name);
                    return view('product.pharmacyindex',compact('product','name','d2','p','leads'));
                }
                else
                {
                    if($check1=="Field Officer")

                    {
                        $leads=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->where('AssignedTo',$name)
                        ->orwhere('PAssignedTo',$name)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);

                        $desig = DB::table('employees')->where('FirstName',$name)->value('Designation3');


                        session()->put('name',$name);
                        return view('fieldexecutive.index',compact('product','name','d3','p','leads','desig'));

                        // ->where('products.OrderStatus','<>','New')
                        //       ->orwhere('products.OrderStatusrent','<>','New')
                        //       ->orwhere('pharmacies.pOrderStatus','<>','New')





                    }
                    else
                    {
                        if($check1=="Field Executive")

                        {
                            $leads=DB::table('leads')

                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('prodleads.empid','=',$eid)
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(50);

                            $desig = DB::table('employees')->where('FirstName',$name)->value('Designation3');
                            session()->put('name',$name);
                            return view('fieldexecutive.index',compact('product','name','d3','p','leads','desig'));
                        }
                        else{
                            if($check1=="Marketing")

                            {

                                $leads=DB::table('leads')

                                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                ->orderBy('prodleads.id', 'DESC')
                                ->paginate(50);

                                session()->put('name',$name);
                                return view('marketing.productindex',compact('product','name','d','p','leads'));

                            }
                            else
                            {

                                if($check1=="vertical")

                                {


                                    $leads=DB::table('leads')

                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                    ->where('Requestcreatedby',$name)
                                    ->orwhere('Prequestcreatedby',$name)
                                    ->orderBy('prodleads.id', 'DESC')
                                    ->paginate(50);

                                    session()->put('name',$name);
                                    return view('verticalheads.productindex',compact('product','name','d','p','leads'));

                                }
                                else
                                {
                                    if($check1=="Coordinator")

                                    {

                                        $leads=DB::table('leads')

                                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                        ->where('Requestcreatedby',$name)
                                        ->orwhere('Prequestcreatedby',$name)
                                        ->orderBy('prodleads.id', 'DESC')
                                        ->paginate(50);

                                        session()->put('name',$name);
                                        return view('co.productindex',compact('product','name','d','p','leads'));

                                    }
                                    else
                                    {
                                        if($check1=="Branch Head")

                                        {
                                            $leads=DB::table('leads')

                                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                            ->where('addresses.City',$city)
                                            ->orderBy('prodleads.id', 'DESC')
                                            ->paginate(50);


                                            session()->put('name',$name);
                                            return view('BranchHead.allproductindex',compact('product','name','d','p','leads'));
                                        }
                                        else
                                        {
                                            if($check1=="Vertical_ProductManager")
                                            {


                                                $leads=DB::table('leads')
                                                ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                ->join('products', 'leads.id', '=', 'products.leadid')
                                                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                ->where('City',$city)
                                                ->orderBy('products.id', 'DESC')
                                                ->paginate(50);

                                                session()->put('name',$name);
                                                return view('Vertical_ProductManager.productindex',compact('product','name','d2','p','leads'));

                                            }
                                            else
                                            {

                                                if($check1=="Vertical_PharmacyManager")
                                                {

                                                    $leads=DB::table('leads')
                                                    ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                                                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                                                    ->where('City',$city)
                                                    ->orderBy('pharmacies.id', 'DESC')
                                                    ->paginate(50);

                                                    session()->put('name',$name);
                                                    return view('Vertical_PharmacyManager.pharmacyindex',compact('product','name','d2','p','leads'));

                                                }
                                                else
                                                {
                                                    if($check1=="Vertical_ProductSelling")
                                                    {

                                                        $leads=DB::table('leads')
                                                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                        ->join('products', 'leads.id', '=', 'products.leadid')
                                                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                        ->where('products.Type',"Sell")
                                                        ->where('City',$city)
                                                        ->orderBy('products.id', 'DESC')
                                                        ->paginate(50);

                                                        session()->put('name',$name);
                                                        return view('Vertical_ProductSelling.productindex',compact('product','name','d2','p','leads'));

                                                    }
                                                    else
                                                    {
                                                        if($check1=="Vertical_ProductRental")
                                                        {

                                                            $leads=DB::table('leads')
                                                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                            ->join('products', 'leads.id', '=', 'products.leadid')
                                                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                            ->where('products.Type',"Rent")
                                                            ->where('City',$city)
                                                            ->orderBy('products.id', 'DESC')
                                                            ->paginate(50);

                                                            session()->put('name',$name);
                                                            return view('Vertical_ProductRental.productindex',compact('product','name','d2','p','leads'));

                                                        }
                                                        else
                                                        {
                                                            if($check1=="Vertical_Product_Pharmacy")
                                                            {
                                                                $leads=DB::table('leads')

                                                                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                ->where('addresses.City',$city)
                                                                ->orderBy('prodleads.id', 'DESC')
                                                                ->paginate(50);


                                                                session()->put('name',$name);
                                                                return view('Vertical_Product_Pharmacy.allproductindex',compact('product','name','d','p','leads'));
                                                            }
                                                            else
                                                            {
                                                                if($check1=="Vertical_ProductSelling_Pharmacy")
                                                                {

                                                                    //         $leads=DB::table('leads')

                                                                    // ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                    // ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                    // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                    // ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                    // ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                    // ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                    // ->where('addresses.City',$city)
                                                                    // ->orwhere('products.Type','Sell')
                                                                    // ->orderBy('prodleads.id', 'DESC')
                                                                    // ->paginate(50);


                                                                    $leads1= DB::table('leads')
                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                    ->where('City',$city)
                                                                    ->where('MedName','!=',NULL)
                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                    ->get();

                                                                    // dd($leads1);

                                                                    $leads1 = json_decode($leads1,true);


                                                                    $leads3= DB::table('leads')
                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                    ->where('City',$city)
                                                                    ->where('products.Type',"Sell")
                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                    ->get();

                                                                    $leads3 = json_decode($leads3,true);

                                                                    $leads_all = array_merge($leads1,$leads3);

                                                                    //function for sorting array into descending order
                                                                    function make_comparer()
                                                                    {
                                                                        $leads_all = func_get_args();
                                                                        $comparer = function($first, $second) use ($leads_all) {
                                                                            // Do we have anything to compare?
                                                                            while(!empty($leads_all)) {
                                                                                // What will we compare now?
                                                                                $criterion = array_shift($leads_all);

                                                                                // Used to reverse the sort order by multiplying
                                                                                // 1 = ascending, -1 = descending
                                                                                $sortOrder = -1;
                                                                                if (is_array($criterion)) {
                                                                                    $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                    $criterion = $criterion[0];
                                                                                }

                                                                                // Do the actual comparison
                                                                                if ($first[$criterion] < $second[$criterion]) {
                                                                                    return -1 * $sortOrder;
                                                                                }
                                                                                else if ($first[$criterion] > $second[$criterion]) {
                                                                                    return 1 * $sortOrder;
                                                                                }

                                                                            }

                                                                            // Nothing more to compare with, so $first == $second
                                                                            return 0;
                                                                        };

                                                                        return $comparer;
                                                                    }

                                                                    usort($leads_all, make_comparer('id'));


                                                                    $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

                                                                    // dd($leads1);
                                                                    $count = count($leads1);





                                                                    session()->put('name',$name);
                                                                    return view('Vertical_ProductSelling_Pharmacy.allproductindex',compact('product','name','d','p','leads','count','leads1'));

                                                                }
                                                                else
                                                                {
                                                                    if($check1=="Vertical_ProductRental_Pharmacy")
                                                                    {
                                                                        // $leads=DB::table('leads')
                                                                        //  ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                        //  ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                        //  ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                        //  ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                        //  ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                        //  ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                        //  ->where('addresses.City',$city)
                                                                        //  ->where('products.Type',"Rent")
                                                                        //  ->orderBy('prodleads.id', 'DESC')
                                                                        //  ->paginate(50);


                                                                        $leads1= DB::table('leads')
                                                                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                        ->where('City',$city)
                                                                        ->where('MedName','!=',NULL)
                                                                        ->orderBy('prodleads.id', 'DESC')
                                                                        ->get();

                                                                        // dd($leads1);

                                                                        $leads1 = json_decode($leads1,true);


                                                                        $leads3= DB::table('leads')
                                                                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                        ->where('City',$city)
                                                                        ->where('products.Type',"Rent")
                                                                        ->orderBy('prodleads.id', 'DESC')
                                                                        ->get();

                                                                        $leads3 = json_decode($leads3,true);

                                                                        $leads_all = array_merge($leads1,$leads3);

                                                                        //function for sorting array into descending order
                                                                        function make_comparer()
                                                                        {
                                                                            $leads_all = func_get_args();
                                                                            $comparer = function($first, $second) use ($leads_all) {
                                                                                // Do we have anything to compare?
                                                                                while(!empty($leads_all)) {
                                                                                    // What will we compare now?
                                                                                    $criterion = array_shift($leads_all);

                                                                                    // Used to reverse the sort order by multiplying
                                                                                    // 1 = ascending, -1 = descending
                                                                                    $sortOrder = -1;
                                                                                    if (is_array($criterion)) {
                                                                                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                        $criterion = $criterion[0];
                                                                                    }

                                                                                    // Do the actual comparison
                                                                                    if ($first[$criterion] < $second[$criterion]) {
                                                                                        return -1 * $sortOrder;
                                                                                    }
                                                                                    else if ($first[$criterion] > $second[$criterion]) {
                                                                                        return 1 * $sortOrder;
                                                                                    }

                                                                                }

                                                                                // Nothing more to compare with, so $first == $second
                                                                                return 0;
                                                                            };

                                                                            return $comparer;
                                                                        }

                                                                        usort($leads_all, make_comparer('id'));


                                                                        $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

                                                                        // dd($leads1);
                                                                        $count = count($leads1);




                                                                        session()->put('name',$name);
                                                                        return view('Vertical_ProductRental_Pharmacy.allproductindex',compact('product','name','d','p','leads','count','leads1'));

                                                                    }
                                                                    else
                                                                    {
                                                                        if($check1=="Coordinator_ProductManager")
                                                                        {
                                                                            $leads=DB::table('leads')
                                                                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                            ->join('products', 'leads.id', '=', 'products.leadid')
                                                                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                                            ->where('addresses.City',$city)
                                                                            ->orderBy('products.id', 'DESC')
                                                                            ->paginate(50);

                                                                            session()->put('name',$name);
                                                                            return view('Coordinator_ProductManager.productindex',compact('product','name','d2','p','leads'));


                                                                        }
                                                                        else
                                                                        {
                                                                            if($check1=="Coordinator_PharmacyManager")
                                                                            {
                                                                                $leads=DB::table('leads')
                                                                                ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                                                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                                                                                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                                                                                ->where('addresses.City',$city)
                                                                                ->orderBy('pharmacies.id', 'DESC')
                                                                                ->paginate(50);

                                                                                session()->put('name',$name);
                                                                                return view('Coordinator_PharmacyManager.pharmacyindex',compact('product','name','d2','p','leads'));

                                                                            }
                                                                            else
                                                                            {
                                                                                if($check1=="Coordinator_ProductSelling")
                                                                                {

                                                                                    $leads=DB::table('leads')
                                                                                    ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                    ->join('products', 'leads.id', '=', 'products.leadid')
                                                                                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                                                    ->where('addresses.City',$city)
                                                                                    ->where('products.Type',"Sell")
                                                                                    ->orderBy('products.id', 'DESC')
                                                                                    ->paginate(50);

                                                                                    session()->put('name',$name);
                                                                                    return view('Coordinator_ProductSelling.productindex',compact('product','name','d2','p','leads'));

                                                                                }
                                                                                else
                                                                                {
                                                                                    if($check1=="Coordinator_ProductRental")
                                                                                    {
                                                                                        $leads=DB::table('leads')
                                                                                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                                                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                        ->join('products', 'leads.id', '=', 'products.leadid')
                                                                                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                                                                                        ->where('addresses.City',$city)
                                                                                        ->where('products.Type',"Rent")
                                                                                        ->orderBy('products.id', 'DESC')
                                                                                        ->paginate(50);

                                                                                        session()->put('name',$name);
                                                                                        return view('Coordinator_ProductRental.productindex',compact('product','name','d2','p','leads'));

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if($check1=="Coordinator_Product_Pharmacy")
                                                                                        {
                                                                                            $leads=DB::table('leads')

                                                                                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                            ->where('addresses.City',$city)
                                                                                            ->orderBy('prodleads.id', 'DESC')
                                                                                            ->paginate(50);


                                                                                            session()->put('name',$name);
                                                                                            return view('Coordinator_Product_Pharmacy.allproductindex',compact('product','name','d','p','leads'));

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if($check1=="Coordinator_ProductSelling_Pharmacy")
                                                                                            {


                                                                                                // $leads=DB::table('leads')

                                                                                                //  ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                //  ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                //  ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                //  ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                //  ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                //  ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                //  ->where('addresses.City',$city)
                                                                                                //  ->where('products.Type',"Sell")
                                                                                                //  ->orderBy('prodleads.id', 'DESC')
                                                                                                //  ->paginate(50);

                                                                                                $leads1= DB::table('leads')
                                                                                                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                ->where('City',$city)
                                                                                                ->where('MedName','!=',NULL)
                                                                                                ->orderBy('prodleads.id', 'DESC')
                                                                                                ->get();

                                                                                                // dd($leads1);

                                                                                                $leads1 = json_decode($leads1,true);


                                                                                                $leads3= DB::table('leads')
                                                                                                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                ->where('City',$city)
                                                                                                ->where('products.Type',"Sell")
                                                                                                ->orderBy('prodleads.id', 'DESC')
                                                                                                ->get();

                                                                                                $leads3 = json_decode($leads3,true);

                                                                                                $leads_all = array_merge($leads1,$leads3);

                                                                                                //function for sorting array into descending order
                                                                                                function make_comparer()
                                                                                                {
                                                                                                    $leads_all = func_get_args();
                                                                                                    $comparer = function($first, $second) use ($leads_all) {
                                                                                                        // Do we have anything to compare?
                                                                                                        while(!empty($leads_all)) {
                                                                                                            // What will we compare now?
                                                                                                            $criterion = array_shift($leads_all);

                                                                                                            // Used to reverse the sort order by multiplying
                                                                                                            // 1 = ascending, -1 = descending
                                                                                                            $sortOrder = -1;
                                                                                                            if (is_array($criterion)) {
                                                                                                                $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                                                $criterion = $criterion[0];
                                                                                                            }

                                                                                                            // Do the actual comparison
                                                                                                            if ($first[$criterion] < $second[$criterion]) {
                                                                                                                return -1 * $sortOrder;
                                                                                                            }
                                                                                                            else if ($first[$criterion] > $second[$criterion]) {
                                                                                                                return 1 * $sortOrder;
                                                                                                            }

                                                                                                        }

                                                                                                        // Nothing more to compare with, so $first == $second
                                                                                                        return 0;
                                                                                                    };

                                                                                                    return $comparer;
                                                                                                }

                                                                                                usort($leads_all, make_comparer('id'));


                                                                                                $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

                                                                                                // dd($leads1);
                                                                                                $count = count($leads1);


                                                                                                session()->put('name',$name);
                                                                                                return view('Coordinator_ProductSelling_Pharmacy.allproductindex',compact('product','name','d','p','leads','count','leads1'));

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if($check1=="Coordinator_ProductRental_Pharmacy")
                                                                                                {

                                                                                                    // $leads=DB::table('leads')

                                                                                                    //  ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                    //  ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                    //  ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                    //  ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                    //  ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                    //  ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                    //  ->where('addresses.City',$city)
                                                                                                    //  ->where('products.Type',"Rent")
                                                                                                    //  ->orderBy('prodleads.id', 'DESC')
                                                                                                    //  ->paginate(50);


                                                                                                    $leads1= DB::table('leads')
                                                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                    ->where('City',$city)
                                                                                                    ->where('MedName','!=',NULL)
                                                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                                                    ->get();

                                                                                                    // dd($leads1);

                                                                                                    $leads1 = json_decode($leads1,true);


                                                                                                    $leads3= DB::table('leads')
                                                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                    ->where('City',$city)
                                                                                                    ->where('products.Type',"Rent")
                                                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                                                    ->get();

                                                                                                    $leads3 = json_decode($leads3,true);

                                                                                                    $leads_all = array_merge($leads1,$leads3);

                                                                                                    //function for sorting array into descending order
                                                                                                    function make_comparer()
                                                                                                    {
                                                                                                        $leads_all = func_get_args();
                                                                                                        $comparer = function($first, $second) use ($leads_all) {
                                                                                                            // Do we have anything to compare?
                                                                                                            while(!empty($leads_all)) {
                                                                                                                // What will we compare now?
                                                                                                                $criterion = array_shift($leads_all);

                                                                                                                // Used to reverse the sort order by multiplying
                                                                                                                // 1 = ascending, -1 = descending
                                                                                                                $sortOrder = -1;
                                                                                                                if (is_array($criterion)) {
                                                                                                                    $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                                                    $criterion = $criterion[0];
                                                                                                                }

                                                                                                                // Do the actual comparison
                                                                                                                if ($first[$criterion] < $second[$criterion]) {
                                                                                                                    return -1 * $sortOrder;
                                                                                                                }
                                                                                                                else if ($first[$criterion] > $second[$criterion]) {
                                                                                                                    return 1 * $sortOrder;
                                                                                                                }

                                                                                                            }

                                                                                                            // Nothing more to compare with, so $first == $second
                                                                                                            return 0;
                                                                                                        };

                                                                                                        return $comparer;
                                                                                                    }

                                                                                                    usort($leads_all, make_comparer('id'));


                                                                                                    $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

                                                                                                    // dd($leads1);
                                                                                                    $count = count($leads1);


                                                                                                    session()->put('name',$name);
                                                                                                    return view('Coordinator_ProductRental_Pharmacy.allproductindex',compact('product','name','d','p','leads','count','leads1'));
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if($check1=="Vertical_FieldOfficer")
                                                                                                    {

                                                                                                        // $leads=DB::table('leads')
                                                                                                        // ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                        // ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                        // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                        // ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                        // ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                        // ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                        // ->where('AssignedTo',$name)
                                                                                                        // ->orwhere('PAssignedTo',$name)
                                                                                                        // ->orderBy('prodleads.id', 'DESC')
                                                                                                        // ->paginate(50);

                                                                                                        $leads1= DB::table('leads')
                                                                                                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                        ->where('products.AssignedTo',$name)
                                                                                                        ->orderBy('prodleads.id', 'DESC')
                                                                                                        ->get();

                                                                                                        $leads1 = json_decode($leads1,true);

                                                                                                        // dd($leads1);


                                                                                                        $leads2= DB::table('leads')
                                                                                                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                        ->where('pharmacies.PAssignedTo',$name)
                                                                                                        ->orderBy('prodleads.id', 'DESC')
                                                                                                        ->get();

                                                                                                        $leads2 = json_decode($leads2,true);

                                                                                                        $leads_all = array_merge($leads1,$leads2);

                                                                                                        //function for sorting array into descending order
                                                                                                        function make_comparer()
                                                                                                        {
                                                                                                            $leads_all = func_get_args();
                                                                                                            $comparer = function($first, $second) use ($leads_all) {
                                                                                                                // Do we have anything to compare?
                                                                                                                while(!empty($leads_all)) {
                                                                                                                    // What will we compare now?
                                                                                                                    $criterion = array_shift($leads_all);

                                                                                                                    // Used to reverse the sort order by multiplying
                                                                                                                    // 1 = ascending, -1 = descending
                                                                                                                    $sortOrder = -1;
                                                                                                                    if (is_array($criterion)) {
                                                                                                                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                                                        $criterion = $criterion[0];
                                                                                                                    }

                                                                                                                    // Do the actual comparison
                                                                                                                    if ($first[$criterion] < $second[$criterion]) {
                                                                                                                        return -1 * $sortOrder;
                                                                                                                    }
                                                                                                                    else if ($first[$criterion] > $second[$criterion]) {
                                                                                                                        return 1 * $sortOrder;
                                                                                                                    }

                                                                                                                }

                                                                                                                // Nothing more to compare with, so $first == $second
                                                                                                                return 0;
                                                                                                            };

                                                                                                            return $comparer;
                                                                                                        }

                                                                                                        usort($leads_all, make_comparer('id'));

                                                                                                        $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));

                                                                                                        $count = count($leads1);


                                                                                                        $desig = DB::table('employees')->where('FirstName',$name)->value('Designation3');


                                                                                                        session()->put('name',$name);
                                                                                                        return view('Vertical_FieldOfficer.productindex',compact('product','name','d3','p','leads','desig','count','leads1','d'));

                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if($check1=="Vertical_FieldExecutive")
                                                                                                        {

                                                                                                            $leads = DB::table('leads')
                                                                                                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                            // ->orwhere('OrderStatusrent',$status)
                                                                                                            // ->orwhere('pharmacies.pOrderStatus',$status)
                                                                                                            ->where('prodleads.empid',$log_id)
                                                                                                            ->orderBy('prodleads.id', 'DESC')
                                                                                                            ->get();

                                                                                                            $leads = json_decode($leads,true);


                                                                                                            $leads_all = $leads;

                                                                                                            //function for sorting array into descending order
                                                                                                            function make_comparer()
                                                                                                            {
                                                                                                                $leads_all = func_get_args();
                                                                                                                $comparer = function($first, $second) use ($leads_all) {
                                                                                                                    // Do we have anything to compare?
                                                                                                                    while(!empty($leads_all)) {
                                                                                                                        // What will we compare now?
                                                                                                                        $criterion = array_shift($leads_all);

                                                                                                                        // Used to reverse the sort order by multiplying
                                                                                                                        // 1 = ascending, -1 = descending
                                                                                                                        $sortOrder = -1;
                                                                                                                        if (is_array($criterion)) {
                                                                                                                            $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                                                                                                                            $criterion = $criterion[0];
                                                                                                                        }

                                                                                                                        // Do the actual comparison
                                                                                                                        if ($first[$criterion] < $second[$criterion]) {
                                                                                                                            return -1 * $sortOrder;
                                                                                                                        }
                                                                                                                        else if ($first[$criterion] > $second[$criterion]) {
                                                                                                                            return 1 * $sortOrder;
                                                                                                                        }

                                                                                                                    }

                                                                                                                    // Nothing more to compare with, so $first == $second
                                                                                                                    return 0;
                                                                                                                };

                                                                                                                return $comparer;
                                                                                                            }

                                                                                                            usort($leads_all, make_comparer('id'));


                                                                                                            $leads1 = $leads_all;
                                                                                                            $count = count($leads1);
                                                                                                            // dd($count);
                                                                                                            // dd($leads_all);

                                                                                                            $desig = DB::table('employees')->where('FirstName',$name)->value('Designation3');
                                                                                                            session()->put('name',$name);
                                                                                                            return view('Vertical_FieldExecutive.productindex',compact('product','name','d3','p','leads','desig','count','leads1','d'));

                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if($check1=="Coordinator_FieldOfficer")
                                                                                                            {

                                                                                                                $leads=DB::table('leads')
                                                                                                                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                                ->where('AssignedTo',$name)
                                                                                                                ->orwhere('PAssignedTo',$name)
                                                                                                                ->orderBy('prodleads.id', 'DESC')
                                                                                                                ->paginate(50);


                                                                                                                $desig = DB::table('employees')->where('FirstName',$name)->value('Designation3');


                                                                                                                session()->put('name',$name);
                                                                                                                return view('Coordinator_FieldOfficer.allproductindex',compact('product','name','d3','p','leads','desig','leads1','count'));

                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if($check1=="Coordinator_FieldExecutive")
                                                                                                                {

                                                                                                                    $leads=DB::table('leads')

                                                                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                                    ->where('prodleads.empid','=',$eid)
                                                                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                                                                    ->paginate(50);

                                                                                                                    $desig = DB::table('employees')->where('FirstName',$name)->value('Designation2');
                                                                                                                    session()->put('name',$name);
                                                                                                                    return view('Coordinator_FieldExecutive.allproductindex',compact('product','name','d3','p','leads','desig'));

                                                                                                                }
                                                                                                                else
                                                                                                                {


                                                                                                                    $leads=DB::table('leads')

                                                                                                                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                                                                                                                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                                                                                                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                                                                                                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                                                                                                                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                                                                                                                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                                                                                                                    ->orderBy('prodleads.id', 'DESC')
                                                                                                                    ->paginate(50);

                                                                                                                    session()->put('name',$name);
                                                                                                                    return view('product.index',compact('product','name','d','p','leads'));



                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }


    // search filter for leads for all who can see the products leads


    public function filter(Request $request)
    {
        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;
        //to ensure that all tables are not fetched -- only for the logged in user


        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        // dd($desig);

        if($desig=="Admin")
        {


            if($keyword1 == "")
            {

                return view('admin.allproductindex');
            }

            if($filter1=="AssignedTo")
            {
                if($status1=="All")
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                    ->orWhere('PAssignedTo', 'like',   $keyword1 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);
                }
                else
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->where('OrderStatus',$status1)
                    ->orwhere('OrderStatusrent',$status1)
                    ->orwhere('pOrderStatus',$status1)
                    ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                    ->orWhere('PAssignedTo', 'like',   $keyword1 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);
                }

                return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
            }


            if($status1=="All")
            {
                $data1=DB::table('leads')
                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);
            }
            else
            {
                $data1=DB::table('leads')
                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatus',$status1)
                ->orwhere('OrderStatusrent',$status1)
                ->orwhere('pOrderStatus',$status1)
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);

            }

            return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
        }
        else
        {

            if($desig2=="Product Manager")
            {

                if($keyword1 == "")
                {

                    return view('product.productindex');
                }



                $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');

                if($pcheck=="Product - Selling")
                {

                    if($status1=="All")
                    {

                        $data1=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'leads.id', '=', 'products.leadid')
                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                        ->where('products.Type',"Sell")
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('products.id', 'DESC')
                        ->paginate(50);
                    }
                    else
                    {

                        $data1=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','products.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'leads.id', '=', 'products.leadid')
                        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                        ->where('products.Type',"Sell")
                        ->where('OrderStatus',$status1)
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('products.id', 'DESC')
                        ->paginate(50);
                    }

                    return view('product.productfilter',compact('data1','keyword1','filter1'));
                }
                else
                {
                    if($pcheck=="Product")
                    {

                        if($status1=="All")
                        {

                            $data1=DB::table('leads')
                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'leads.id', '=', 'products.leadid')
                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')

                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->orderBy('products.id', 'DESC')
                            ->paginate(50);
                        }
                        else
                        {

                            $data1=DB::table('leads')
                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'leads.id', '=', 'products.leadid')
                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')

                            ->where('OrderStatus',$status1)
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->orderBy('products.id', 'DESC')
                            ->paginate(50);
                        }

                        return view('product.productfilter',compact('data1','keyword1','filter1'));
                    }
                    else
                    {

                        if($filter1=="OrderStatus")
                        {
                            $filter1="OrderStatusrent";
                        }
                        if($status1=="All")
                        {
                            $data1=DB::table('leads')
                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'leads.id', '=', 'products.leadid')
                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                            ->where('products.Type',"Rent")
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->orderBy('products.id', 'DESC')
                            ->paginate(50);
                        }
                        else
                        {
                            $data1=DB::table('leads')
                            ->select('addresses.*','leads.*','prodleads.*','products.*')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'leads.id', '=', 'products.leadid')
                            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                            ->where('products.Type',"Rent")
                            ->where('OrderStatusrent',$status1)
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->orderBy('products.id', 'DESC')
                            ->paginate(50);
                        }

                        return view('product.productfilter',compact('data1','keyword1','filter1'));
                    }
                }



            }
            else
            {
                if($desig2=="Pharmacy Manager")
                {

                    if($keyword1 == "")
                    {

                        return view('pharmacy.pharmacyindex');
                    }

                    // dd($filter1);
                    if($status1=="All")
                    {
                        $data1=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('pharmacies.id', 'DESC')
                        ->paginate(50);
                    }
                    else
                    {
                        $data1=DB::table('leads')
                        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                        ->where('pOrderStatus',$status1)
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('pharmacies.id', 'DESC')
                        ->paginate(50);
                    }

                    // dd($leads);
                    return view('product.pharmacyfilter',compact('data1','keyword1','filter1'));







                }
                else
                {

                    //join query to retrieve all the search filter values
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('cc.alldata',compact('data1','keyword1','filter1'));

                }
            }
        }





        // //join query to retrieve all the search filter values
        // $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
        // ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        // ->join('services', 'leads.id', '=', 'services.LeadId')
        // ->Where($filter1, 'like',   $keyword1 . '%')
        // ->orderBy('leads.id', 'DESC')->paginate(200);

        // return view('cc.alldata',compact('data1','keyword1','filter1'));





        // search filter end

    }









    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {

            $name=$_GET['name'];
            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp1=DB::table('employees')->where('Designation2','Vertical Head')->get();
            $products=DB::table('productdetails')->get();


            $empid=DB::table('employees')->where('FirstName',$name)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            // dd($eid);
            $Designation2=DB::table('employees')->where('id',$eid)->value('Department2');
            $Designation=DB::table('employees')->where('id',$eid)->value('Designation');
            $Department=DB::table('employees')->where('id',$eid)->value('Department');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);


            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            // if($Designation=="Branch Head")
            // {
            //     return view('BranchHead.productlead',compact('indian_cities','count1','count','country_codes','reference','gender','city','pcity','ecity','branch','status','leadtype','condition','language','relation','language','vertical','emp','emp1','Designation2','Designation','products'));
            // }




            // if($Designation=="Marketing")
            // {

            //     return view('marketing.productlead',compact('indian_cities','count1','count','country_codes','reference','gender','city','pcity','ecity','branch','status','leadtype','condition','language','relation','language','vertical','emp','emp1','Designation2','Designation','products'));
            // }


            session()->put('name',$name);
            return view('cc.productlead',compact('indian_cities','count1','count','country_codes','reference','gender','city','pcity','ecity','branch','status','leadtype','condition','language','relation','language','vertical','emp','emp1','Designation2','Designation','products'));

        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        /* fetch who has created this lead */


        $name1= $request->loginname;

        //if any existing client is entered as the "Client Id", then pass it into this variable else pass "NULL"
        $existingclient=$request->existing;

        //pass the above value into another variable to avoid confusion
        $leadid=$existingclient;


        /* Code for storing the details entered in the form starts here */
        //Whatever reference is selected by the user , it's id needs to be retrieved and passed into the Lead reference id column
        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=Null;

        //retrieve the employee id of the logged in user
        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        //fetching product details
        $c=$request->SKUid;
        $c2=$request->ProductName;
        $c1=$request->MedName;



        // Checking if the lead is created from provisional lead then change the value in provisional leads table

        if($request->provisionallead=="true")
        {
            $pid=$request->pid;
            if($pid!=NULL)
            {
                DB::table('provisionalleads')->where('id',$pid)->update(['active'=>'0']);
            }

        }



        //checking if not existing client than create a new lead
        // if the existing client is not entered by the user, then it is "NULL" by default
        //In such a case, create a new lead altogether
        if($existingclient==NULL)
        {
            if($c!=NULL || $c1!=NULL || $c2!=NULL)

            {
                $lead = new lead;
                // $this->validate($request,[
                //         'Languages'=>'required',
                //     ]);

                //If the source field is not filled, then take the reference id of the selected Reference
                //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
                if($request->source == Null)
                {
                    //take out the value in the "reference" field, and assign the same value to "source" as well
                    $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                    $source=$refer;
                }
                //if the Other option is selected, then whatever is filled in the source field
                //pass that to the "Source" field
                else
                {
                    $source=$request->source;
                }

                //fetch all the corresponding fields
                $lead->fName=$request->clientfname;
                $lead->mName=$request->clientmname;
                $lead->lName=$request->clientlname;
                $lead->EmailId=$request->clientemail;
                $lead->Source=$source;
                $lead->MobileNumber=$request->clientmob;
                $lead->Alternatenumber=$request->clientalternateno;
                $lead->EmergencyContact =$request->EmergencyContact;
                $lead->AssesmentReq=$request->assesmentreq;
                $lead->createdby=$request->loginname;
                $lead->Referenceid=$ref;
                $lead->empid=$empid;
                $lead->save();

                $leadid=DB::table('leads')->max('id');

                $address = new address;

                //fetch and store all the address details
                $address->Address1=$request->Address1;
                $address->Address2=$request->Address2;
                $address->City=$request->City;
                $address->District=$request->District;
                $address->State=$request->State;
                $address->PinCode=$request->PinCode;


                $same1=$request->presentcontact;

                if($same1=='same')
                {
                    $address->PAddress1=$request->Address1;
                    $address->PAddress2=$request->Address2;
                    $address->PCity=$request->City;
                    $address->PDistrict=$request->District;
                    $address->PState=$request->State;
                    $address->PPinCode=$request->PinCode;
                }

                else
                {
                    $address->PAddress1=$request->PAddress1;
                    $address->PAddress2=$request->PAddress2;
                    $address->PCity=$request->PCity;
                    $address->PDistrict=$request->PDistrict;
                    $address->PState=$request->PState;
                    $address->PPinCode=$request->PPinCode;
                }

                $same=$request->emergencycontact;
                if($same=='same')
                {
                    $address->EAddress1=$request->Address1;
                    $address->EAddress2=$request->Address2;
                    $address->ECity=$request->City;
                    $address->EDistrict=$request->District;
                    $address->EState=$request->State;
                    $address->EPinCode=$request->PinCode;
                }

                else
                {
                    $address->EAddress1=$request->EAddress1;
                    $address->EAddress2=$request->EAddress2;
                    $address->ECity=$request->ECity;
                    $address->EDistrict=$request->EDistrict;
                    $address->EState=$request->EState;
                    $address->EPinCode=$request->EPinCode;
                }

                $same2=$request->shippingcontact;
                if($same2=='same')
                {
                    $address->SAddress1=$request->Address1;
                    $address->SAddress2=$request->Address2;
                    $address->SCity=$request->City;
                    $address->SDistrict=$request->District;
                    $address->SState=$request->State;
                    $address->SPinCode=$request->PinCode;
                }

                else
                {
                    $address->SAddress1=$request->SAddress1;
                    $address->SAddress2=$request->SAddress2;
                    $address->SCity=$request->SCity;
                    $address->SDistrict=$request->SDistrict;
                    $address->SState=$request->SState;
                    $address->SPinCode=$request->SPinCode;
                }


                $address->leadid=$leadid;
                $address->empid=$empid;
                $address->save();
                $addressid=DB::table('addresses')->max('id');

                // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"


                //retrieving the session id of the person who is logged in
                $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                //retrieving the username of the presently logged in user
                $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                // retrieving the employee id of the presently logged in user
                $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

                //retrieving the log id of the person who logged in just now
                $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "New Lead Created ";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->lead_id = $leadid;
                $activity->save();
            }
        }
        else
        {
            // dd($existingclient);
            $l=DB::table('leads')->where('id',$existingclient)->get();
            $l1=json_decode($l);
            $fName= $l1[0]->fName;
            $mName= $l1[0]->mName;
            $lName= $l1[0]->lName;
            $EmailId= $l1[0]->EmailId;
            $Source= $l1[0]->Source;
            $MobileNumber= $l1[0]->MobileNumber;
            $Alternatenumber= $l1[0]->Alternatenumber;
            $EmergencyContact= $l1[0]->EmergencyContact;
            $AssesmentReq= $l1[0]->AssesmentReq;
            $Referenceid= $l1[0]->Referenceid;


            $addproductwithservice=$request->addproductwithservice;

            // dd($addproductwithservice);


            if($c!=NULL || $c1!=NULL || $c2!=NULL)
            {
                // || $mName!=$request->clientmname || $lName!=$request->clientlname || $EmailId!=$request->clientemail || $MobileNumber!=$request->clientmob || $Referenceid!=$request->reference

                if($addproductwithservice==NULL)
                {
                    if($fName!=$request->clientfname || $MobileNumber!=$request->clientmob || $EmailId!=$request->clientemail || $Referenceid!=$ref)
                    {

                        $lead = new lead;
                        // $this->validate($request,[
                        //         'Languages'=>'required',
                        //     ]);

                        //If the source field is not filled, then take the reference id of the selected Reference
                        if($request->source == Null)
                        {
                            $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                            $source=$refer;
                        }
                        //if the Other option is selected, then whatever is filled in the source field
                        else
                        {
                            $source=$request->source;
                        }

                        //fetch all the corresponding fields
                        $lead->fName=$request->clientfname;
                        $lead->mName=$request->clientmname;
                        $lead->lName=$request->clientlname;
                        $lead->EmailId=$request->clientemail;
                        $lead->Source=$source;
                        $lead->MobileNumber=$request->clientmob;
                        $lead->Alternatenumber=$request->clientalternateno;
                        $lead->EmergencyContact =$request->EmergencyContact;
                        $lead->AssesmentReq=$request->assesmentreq;
                        $lead->createdby=$request->loginname;
                        $lead->Referenceid=$ref;
                        $lead->empid=$empid;
                        $lead->save();

                        $leadid=DB::table('leads')->max('id');

                        $address = new address;

                        //fetch and store all the address details
                        $address->Address1=$request->Address1;
                        $address->Address2=$request->Address2;
                        $address->City=$request->City;
                        $address->District=$request->District;
                        $address->State=$request->State;
                        $address->PinCode=$request->PinCode;


                        $same1=$request->presentcontact;

                        if($same1=='same')
                        {
                            $address->PAddress1=$request->Address1;
                            $address->PAddress2=$request->Address2;
                            $address->PCity=$request->City;
                            $address->PDistrict=$request->District;
                            $address->PState=$request->State;
                            $address->PPinCode=$request->PinCode;
                        }

                        else
                        {
                            $address->PAddress1=$request->PAddress1;
                            $address->PAddress2=$request->PAddress2;
                            $address->PCity=$request->PCity;
                            $address->PDistrict=$request->PDistrict;
                            $address->PState=$request->PState;
                            $address->PPinCode=$request->PPinCode;
                        }

                        $same=$request->emergencycontact;
                        if($same=='same')
                        {
                            $address->EAddress1=$request->Address1;
                            $address->EAddress2=$request->Address2;
                            $address->ECity=$request->City;
                            $address->EDistrict=$request->District;
                            $address->EState=$request->State;
                            $address->EPinCode=$request->PinCode;
                        }

                        else
                        {
                            $address->EAddress1=$request->EAddress1;
                            $address->EAddress2=$request->EAddress2;
                            $address->ECity=$request->ECity;
                            $address->EDistrict=$request->EDistrict;
                            $address->EState=$request->EState;
                            $address->EPinCode=$request->EPinCode;
                        }

                        $same2=$request->shippingcontact;
                        if($same2=='same')
                        {
                            $address->SAddress1=$request->Address1;
                            $address->SAddress2=$request->Address2;
                            $address->SCity=$request->City;
                            $address->SDistrict=$request->District;
                            $address->SState=$request->State;
                            $address->SPinCode=$request->PinCode;
                        }

                        else
                        {
                            $address->SAddress1=$request->SAddress1;
                            $address->SAddress2=$request->SAddress2;
                            $address->SCity=$request->SCity;
                            $address->SDistrict=$request->SDistrict;
                            $address->SState=$request->SState;
                            $address->SPinCode=$request->SPinCode;
                        }


                        $address->leadid=$leadid;
                        $address->empid=$empid;
                        $address->save();
                        $addressid=DB::table('addresses')->max('id');

                    }
                }






                $a=DB::table('addresses')->where('leadid',$existingclient)->get();
                $a1=json_decode($a);
                $PAddress1= $a[0]->PAddress1;
                $PAddress2= $a[0]->PAddress2;
                $PCity= $a[0]->PCity;
                $PDistrict= $a[0]->PDistrict;
                $PState= $a[0]->PState;
                $PPinCode= $a[0]->PPinCode;
                $EAddress1= $a[0]->EAddress1;
                $EAddress2= $a[0]->EAddress2;
                $ECity= $a[0]->ECity;
                $EDistrict= $a[0]->EDistrict;
                $EState= $a[0]->EState;
                $EPinCode= $a[0]->EPinCode;
                $SAddress1= $a[0]->SAddress1;
                $SAddress2= $a[0]->SAddress2;
                $SCity= $a[0]->SCity;
                $SDistrict= $a[0]->SDistrict;
                $SState= $a[0]->SState;
                $SPinCode= $a[0]->SPinCode;

                if($PAddress1!=$request->PAddress1)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PAddress1' => $request->PAddress1]);

                }
                if($PAddress2!=$request->PAddress2)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PAddress2' => $request->PAddress2]);
                }
                if($PCity!=$request->PCity)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PCity' => $request->PCity]);
                }
                if($PDistrict!=$request->PDistrict)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PDistrict' => $request->PDistrict]);
                }
                if($PState!=$request->PState)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PState' => $request->PState]);
                }
                if($PPinCode!=$request->PPinCode)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['PPinCode' => $request->PPinCode]);
                }
                if($EAddress1!=$request->EAddress1)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['EAddress1' => $request->EAddress1]);
                }
                if($EAddress2!=$request->EAddress2)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['EAddress2' => $request->EAddress2]);
                }
                if($ECity!=$request->ECity)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['ECity' => $request->ECity]);
                }
                if($EDistrict!=$request->EDistrict)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['EDistrict' => $request->EDistrict]);
                }
                if($EState!=$request->EState)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['EState' => $request->EState]);
                }
                if($EPinCode!=$request->EPinCode)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['EPinCode' => $request->EPinCode]);
                }
                if($SAddress1!=$request->SAddress1)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SAddress1' => $request->SAddress1]);
                }
                if($SAddress2!=$request->SAddress2)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SAddress2' => $request->SAddress2]);
                }
                if($SCity!=$request->SCity)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SCity' => $request->SCity]);
                }
                if($SDistrict!=$request->SDistrict)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SDistrict' => $request->SDistrict]);
                }
                if($SState!=$request->SState)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SState' => $request->SState]);
                }
                if($SPinCode!=$request->SPinCode)
                {
                    DB::table('addresses')->where('leadid', $existingclient)->update(['SPinCode' => $request->SPinCode]);
                }


            }

        }

        //assigning default value so that by default it will point to NULL values
        $productid="1";
        $pharmacyid="1";


        // checking if product id or pharmacy id is null don't enter the details to DB
        $c=$request->SKUid;
        $c2=$request->ProductName;
        $c1=$request->MedName;
        // dd($c,$c2,$c1);

        //check if any of the detail is present enter the id in order which is common for the complete one session
        if($c!=NULL || $c1!=NULL || $c2!=NULL)
        {
            $order=new order;
            $order->leadid=$leadid;
            $order->save();
            $orderid=DB::table('orders')->max('id');

            // SMS sent to customer for product or pharmacy or (product and pharmacy) starts here

            // dd($request->clientfname);
            // dd($request->clientmob);
            // dd($request->clientalternateno);

            $cust_name = $request->clientfname;
            $cust_mob = $request->clientmob;
            $cust_altmob = $request->clientalternateno;

            if($cust_mob==NULL)
            {
                $cust_mob=DB::table('leads')->where('id',$addproductwithservice)->value('MobileNumber');
            }
            $active_state = DB::table('sms_email_filter')->value('active_state');

            if($cust_mob!=NULL && $active_state==0)
            {
                $message=
                "Your Order no: ".$orderid." has been requested & confirmed.
                We will send a confirmation when your order is shipped.
                Contact: 8880004422
                Visit: www.healthheal.in";


                //for extracting the date for generating reports
                $time = time();
                $date = date('m/d/Y',$time);

                //actual sending of the message
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                // dd(curl_exec($ch));
                //expecting the report 'OK' here
                $report = substr($curl_scraped_page,0,2);

                //extracting the job no of the message sent if it is at all
                $jobno = trim(substr($curl_scraped_page,3));


                curl_close($ch);

                //url for retrieving the report
                $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                $ch1 =curl_init($report1);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page1 =curl_exec($ch1);
                // $report = substr($curl_scraped_page1,0,2);
                curl_close($ch1);

                //retrieving the invalid mobile no text part out of the report
                $invalidmno = substr($curl_scraped_page1,0,11);

                // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                // if($invalidmno == "Invalid Mno")
                // {
                //
                //     $checkbox=$request->addproduct;
                //
                //     session()->put('name',$who);
                //     Session::flash('warning','The Mobile number is invalid');
                //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                // }

                $i=0;

                //sending a message again if not sent first time

                //while the report generated doesn't have the message OK in it
                while($report!="OK" || $i<=1)
                {
                    //even if i<1 trigger this only in cases where report is not = OK
                    if($report!="OK")
                    {
                        $message=
                        "Your Order no: ".$orderid." has been requested & confirmed.
                        We will send a confirmation when your order is shipped.
                        Contact: 8880004422
                        Visit: www.healthheal.in";

                        $time = time();
                        $date = date('d/m/Y',$time);
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }

            // SMS for customer alternate contact number

            $active_state = DB::table('sms_email_filter')->value('active_state');

            if($cust_altmob!=NULL && $active_state==0)
            {
                $message=
                "Your Order no: ".$orderid." has been requested & confirmed.
                We will send a confirmation when your order is shipped.
                Contact: 8880004422
                Visit: www.healthheal.in";

                //for extracting the date for generating reports
                $time = time();
                $date = date('m/d/Y',$time);

                //actual sending of the message
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                // dd(curl_exec($ch));
                //expecting the report 'OK' here
                $report = substr($curl_scraped_page,0,2);

                //extracting the job no of the message sent if it is at all
                $jobno = trim(substr($curl_scraped_page,3));


                curl_close($ch);

                //url for retrieving the report
                $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                $ch1 =curl_init($report1);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page1 =curl_exec($ch1);
                // $report = substr($curl_scraped_page1,0,2);
                curl_close($ch1);

                //retrieving the invalid mobile no text part out of the report
                $invalidmno = substr($curl_scraped_page1,0,11);

                // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                // if($invalidmno == "Invalid Mno")
                // {
                //
                //     $checkbox=$request->addproduct;
                //
                //     session()->put('name',$who);
                //     Session::flash('warning','The Mobile number is invalid');
                //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                // }

                $i=0;

                //sending a message again if not sent first time

                //while the report generated doesn't have the message OK in it
                while($report!="OK" || $i<=1)
                {
                    //even if i<1 trigger this only in cases where report is not = OK
                    if($report!="OK")
                    {
                        $message=
                        "Your Order no: ".$orderid." has been requested & confirmed.
                        We will send a confirmation when your order is shipped.
                        Contact: 8880004422
                        Visit: www.healthheal.in";

                        $time = time();
                        $date = date('d/m/Y',$time);
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }


            //SMS sent to the Coordinator / Vertical Head if existing lead starts here

            //checking whether the lead id was existing or created newly
            $exist_lead = DB::table('services')->where('Leadid', $leadid)->value('Leadid');

            // extracting the name of the vertical head that lead has been assigned to
            $assigned_to_vname = DB::table('services')->where('Leadid', $leadid)->value('AssignedTo');

            //extracting the status of that lead
            $service_status = DB::table('services')->where('Leadid', $leadid)->value('ServiceStatus');

            //extracting the employee id of the coordinator to whom the lead has been assigned if at all
            $assigned_to_cid = DB::table('verticalcoordinations')->where('leadid', $leadid)->value('empid');


            if($exist_lead!=NULL)
            {
                if($service_status=="Converted" || $service_status=="In Progress" || $service_status=="New")
                {
                    //send SMS to Coordinators
                    if($assigned_to_cid!=NULL)
                    {
                        $assigned_to_cname = DB::table('employees')->where('id',$assigned_to_cid)->value('FirstName');

                        $coord_name = $assigned_to_cname;
                        $coord_mob = DB::table('employees')->where('FirstName',$coord_name)->value('MobileNumber');
                        $coord_altmob = DB::table('employees')->where('FirstName',$coord_name)->value('AlternateNumber');


                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($coord_mob!=NULL && $active_state==0)
                        {
                            $message=
                            "Dear ".$coord_name.",
                            Order no: ".$orderid." has been requested.

                            Thank you.";


                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                            //url for retrieving the report
                            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                            $ch1 =curl_init($report1);
                            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page1 =curl_exec($ch1);
                            // $report = substr($curl_scraped_page1,0,2);
                            curl_close($ch1);

                            //retrieving the invalid mobile no text part out of the report
                            $invalidmno = substr($curl_scraped_page1,0,11);

                            // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                            // if($invalidmno == "Invalid Mno")
                            // {
                            //
                            //     $checkbox=$request->addproduct;
                            //
                            //     session()->put('name',$who);
                            //     Session::flash('warning','The Mobile number is invalid');
                            //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                            // }

                            $i=0;

                            //sending a message again if not sent first time

                            //while the report generated doesn't have the message OK in it
                            while($report!="OK" || $i<=1)
                            {
                                //even if i<1 trigger this only in cases where report is not = OK
                                if($report!="OK")
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    Order no: ".$orderid." has been requested.

                                    Thank you.";

                                    $time = time();
                                    $date = date('d/m/Y',$time);
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);
                                    curl_close($ch);
                                }
                                $i++;
                            }
                        }

                        // SMS for Coordinator alternate contact number

                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($coord_altmob!=NULL && $active_state==0)
                        {
                            $message=
                            "Dear ".$coord_name.",
                            Order no: ".$orderid." has been requested.

                            Thank you.";

                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                            //url for retrieving the report
                            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                            $ch1 =curl_init($report1);
                            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page1 =curl_exec($ch1);
                            // $report = substr($curl_scraped_page1,0,2);
                            curl_close($ch1);

                            //retrieving the invalid mobile no text part out of the report
                            $invalidmno = substr($curl_scraped_page1,0,11);

                            // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                            // if($invalidmno == "Invalid Mno")
                            // {
                            //
                            //     $checkbox=$request->addproduct;
                            //
                            //     session()->put('name',$who);
                            //     Session::flash('warning','The Mobile number is invalid');
                            //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                            // }

                            $i=0;

                            //sending a message again if not sent first time

                            //while the report generated doesn't have the message OK in it
                            while($report!="OK" || $i<=1)
                            {
                                //even if i<1 trigger this only in cases where report is not = OK
                                if($report!="OK")
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    Order no: ".$orderid." has been requested.

                                    Thank you.";

                                    $time = time();
                                    $date = date('d/m/Y',$time);
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);
                                    curl_close($ch);
                                }
                                $i++;
                            }
                        }
                    }

                    //send SMS to Vertical Head
                    else
                    {
                        $vertical_name = $assigned_to_vname;
                        $v_mob = DB::table('employees')->where('FirstName',$vertical_name)->value('MobileNumber');
                        $v_altmob = DB::table('employees')->where('FirstName',$vertical_name)->value('AlternateNumber');

                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($v_mob!=NULL && $active_state==0)
                        {
                            $message=
                            "Dear ".$vertical_name.",
                            Order no: ".$orderid." has been requested.

                            Thank you.";


                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                            //url for retrieving the report
                            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                            $ch1 =curl_init($report1);
                            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page1 =curl_exec($ch1);
                            // $report = substr($curl_scraped_page1,0,2);
                            curl_close($ch1);

                            //retrieving the invalid mobile no text part out of the report
                            $invalidmno = substr($curl_scraped_page1,0,11);

                            // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                            // if($invalidmno == "Invalid Mno")
                            // {
                            //
                            //     $checkbox=$request->addproduct;
                            //
                            //     session()->put('name',$who);
                            //     Session::flash('warning','The Mobile number is invalid');
                            //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                            // }

                            $i=0;

                            //sending a message again if not sent first time

                            //while the report generated doesn't have the message OK in it
                            while($report!="OK" || $i<=1)
                            {
                                //even if i<1 trigger this only in cases where report is not = OK
                                if($report!="OK")
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    Order no: ".$orderid." has been requested.

                                    Thank you.";

                                    $time = time();
                                    $date = date('d/m/Y',$time);
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);
                                    curl_close($ch);
                                }
                                $i++;
                            }
                        }

                        // SMS for vertical alternate contact number

                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($v_altmob!=NULL && $active_state==0)
                        {
                            $message=
                            "Dear ".$vertical_name.",
                            Order no: ".$orderid." has been requested.

                            Thank you.";

                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                            //url for retrieving the report
                            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                            $ch1 =curl_init($report1);
                            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page1 =curl_exec($ch1);
                            // $report = substr($curl_scraped_page1,0,2);
                            curl_close($ch1);

                            //retrieving the invalid mobile no text part out of the report
                            $invalidmno = substr($curl_scraped_page1,0,11);

                            // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                            // if($invalidmno == "Invalid Mno")
                            // {
                            //
                            //     $checkbox=$request->addproduct;
                            //
                            //     session()->put('name',$who);
                            //     Session::flash('warning','The Mobile number is invalid');
                            //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                            // }

                            $i=0;

                            //sending a message again if not sent first time

                            //while the report generated doesn't have the message OK in it
                            while($report!="OK" || $i<=1)
                            {
                                //even if i<1 trigger this only in cases where report is not = OK
                                if($report!="OK")
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    Order no: ".$orderid." has been requested.

                                    Thank you.";

                                    $time = time();
                                    $date = date('d/m/Y',$time);
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);
                                    curl_close($ch);
                                }
                                $i++;
                            }
                        }
                    }
                }
            }


            //SMS sent to the Coordinator/Vertical head to whom it is assigned ends here


            //Sending Mail to the Customer when the Product Lead is created starts here

            // $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];
            //
            // $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];


            //finding the name and email id of the customer
            $customer_name = $request->clientfname;
            $customer_email = $request->clientemail;
            $customer_contact = $request->clientmob;

            $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_email'=>$customer_email];

            $active_state = DB::table('sms_email_filter')->value('active_state');


            if($customer_email!=NULL && $active_state==0)
            {
                Mail::send('mail_for_product1', $prod_data, function($message)use($prod_data) {
                    $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                    ('[Happy to Care] Health Heal Order Requested');
                    $message->from('care@healthheal.in','Healthheal');
                });
            }
            //Sending Mail to the Customer when the Product Lead is created ends here

            //Send Mail to the Product / Pharmacy Manager when the Lead is Created starts here

            //finding the name and email id of the Product/Pharmacy Manager

            $count_name = 0;
            if($request->Type=="Sell"  || $request->Type==NULL || $request->Type=="")
            {
                // dd($request->Type);
                $pm_desig = "Product Manager";
                $pm_dept = "Product - Selling";

                $pm_name = DB::table('employees')->select('FirstName')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->get();
                $pm_name = json_decode($pm_name,true);
                $count_name = count($pm_name);
                // dd($count_name);

                for($i=0;$i<$count_name;$i++)
                {
                    $pm_mail = DB::table('employees')->where('FirstName',$pm_name[$i]['FirstName'])->value('Email_id');
                    $pm_email[] = $pm_mail;
                }
                // dd($pm_mail);

            }
            else if($request->Type=="Rent")
            {

                $pm_desig = "Product Manager";
                $pm_dept = "Product - Rental";

                $pm_name = DB::table('employees')->select('FirstName')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->get();
                $pm_name = json_decode($pm_name,true);
                $count_name = count($pm_name);
                // dd($count_name);

                for($i=0;$i<$count_name;$i++)
                {
                    $pm_mail = DB::table('employees')->where('FirstName',$pm_name[$i]['FirstName'])->value('Email_id');
                    // dd($pm_mail);
                    $pm_email[] = $pm_mail;
                }
                // dd($pm_email);


            }

            for($i=0;$i<$count_name;$i++)
            {
                if($pm_email[$i]!=NULL && $active_state==0)
                {
                    // dd($pm_email);
                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_email'=>$customer_email,'cust_mob'=>$customer_contact,'pm_name'=>$pm_name,'pm_email'=>$pm_email,'prod_creator'=>$name1];


                    Mail::send('mail_for_product2', $prod_data, function($message)use($prod_data) {
                        $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                        ("Order Id [".$prod_data['orderid']."] Created!!!");
                        $message->from('care@healthheal.in','Healthheal');
                    });
                }
            }

            //Send Mail to the Product / Pharmacy Manager when the Lead is Created ends here
        }


        if($c!=NULL || $c2!=NULL)
        {


            $OrderStatus="New";
            $OrderStatusrent="New";

            if($request->Type=="Sell" || $request->Type==NULL || $request->Type=="")
            {

                $OrderStatusrent=NULL;
            }

            if($request->Type=="Rent")
            {

                $OrderStatus=NULL;
            }


            $products = new product;
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;
            $products->Type=$request->Type;
            $products->OrderStatus=$OrderStatus;
            $products->Quantity=$request->Quantity;
            $products->deliverydate=$request->deliverydate;
            $products->ModeofPaymentrent=$request->ModeofPaymentrent;
            $products->OrderStatusrent=$OrderStatusrent;
            $products->ModeofPayment=$request->ModeofPayment;
            $products->AdvanceAmt=$request->AdvanceAmt;
            $products->StartDate=$request->StartDate;
            $products->EndDate=$request->EndDate;
            $products->TransportCharges=$request->TransportCharges;
            $products->OverdueAmt=$request->OverdueAmt;
            $products->Requestcreatedby=$name1;
            $products->Leadid=$leadid;
            $products->save();
            $productid=DB::table('products')->max('id');


            $prodlead=new prodlead;
            $prodlead->leadid=$leadid;
            $prodlead->Prodid=$productid;
            $prodlead->PharmacyId=$pharmacyid;
            $prodlead->orderid=$orderid;
            $prodlead->save();

            // /*
            // SMS to Product Managers when Product is added -- sms going to Product Managers only starts here
            // */
            // if($request->Type=="Sell"  || $request->Type==NULL || $request->Type=="")
            // {
            //     $pm_desig = "Product Manager";
            //     $pm_dept = "Product - Selling";
            //
            //     $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');
            //
            //     $pm_contact = DB::table('employees')->where('FirstName',$pm_name)->value('MobileNumber');
            //     $pm_altcontact = DB::table('employees')->where('FirstName',$pm_name)->value('AlternateNumber');
            //
            //     // dd($pm_altcontact);
            //     if($pm_contact!=NULL)
            //     {
            //         $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //         //for extracting the date for generating reports
            //         $time = time();
            //         $date = date('m/d/Y',$time);
            //
            //         //actual sending of the message
            //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //         $ch =curl_init($url);
            //
            //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page =curl_exec($ch);
            //         // dd(curl_exec($ch));
            //         //expecting the report 'OK' here
            //         $report = substr($curl_scraped_page,0,2);
            //
            //         //extracting the job no of the message sent if it is at all
            //         $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //         curl_close($ch);
            //
            //         //url for retrieving the report
            //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //         $ch1 =curl_init($report1);
            //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page1 =curl_exec($ch1);
            //         // $report = substr($curl_scraped_page1,0,2);
            //         curl_close($ch1);
            //
            //         //retrieving the invalid mobile no text part out of the report
            //         $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //         // if($invalidmno == "Invalid Mno")
            //         // {
            //         //
            //         //     $checkbox=$request->addproduct;
            //         //
            //         //     session()->put('name',$who);
            //         //     Session::flash('warning','The Mobile number is invalid');
            //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //         // }
            //
            //         $i=0;
            //
            //         //sending a message again if not sent first time
            //
            //         //while the report generated doesn't have the message OK in it
            //         while($report!="OK" || $i<=1)
            //         {
            //             //even if i<1 trigger this only in cases where report is not = OK
            //             if($report!="OK")
            //             {
            //                 $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //                 $time = time();
            //                 $date = date('d/m/Y',$time);
            //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //                 $ch =curl_init($url);
            //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //                 $curl_scraped_page =curl_exec($ch);
            //                 $report = substr($curl_scraped_page,0,2);
            //                 curl_close($ch);
            //             }
            //             $i++;
            //         }
            //     }
            //
            //     // SMS for PM alternate contact number
            //
            //     if($pm_altcontact!=NULL)
            //     {
            //         $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //         //for extracting the date for generating reports
            //         $time = time();
            //         $date = date('m/d/Y',$time);
            //
            //         //actual sending of the message
            //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //         $ch =curl_init($url);
            //
            //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page =curl_exec($ch);
            //         // dd(curl_exec($ch));
            //         //expecting the report 'OK' here
            //         $report = substr($curl_scraped_page,0,2);
            //
            //         //extracting the job no of the message sent if it is at all
            //         $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //         curl_close($ch);
            //
            //         //url for retrieving the report
            //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //         $ch1 =curl_init($report1);
            //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page1 =curl_exec($ch1);
            //         // $report = substr($curl_scraped_page1,0,2);
            //         curl_close($ch1);
            //
            //         //retrieving the invalid mobile no text part out of the report
            //         $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //         // if($invalidmno == "Invalid Mno")
            //         // {
            //         //
            //         //     $checkbox=$request->addproduct;
            //         //
            //         //     session()->put('name',$who);
            //         //     Session::flash('warning','The Mobile number is invalid');
            //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //         // }
            //
            //         $i=0;
            //
            //         //sending a message again if not sent first time
            //
            //         //while the report generated doesn't have the message OK in it
            //         while($report!="OK" || $i<=1)
            //         {
            //             //even if i<1 trigger this only in cases where report is not = OK
            //             if($report!="OK")
            //             {
            //                 $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //                 $time = time();
            //                 $date = date('d/m/Y',$time);
            //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //                 $ch =curl_init($url);
            //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //                 $curl_scraped_page =curl_exec($ch);
            //                 $report = substr($curl_scraped_page,0,2);
            //                 curl_close($ch);
            //             }
            //             $i++;
            //         }
            //     }
            //
            // }
            // else if($request->Type=="Rent")
            // {
            //     $pm_desig = "Product Manager";
            //     $pm_dept = "Product - Rental";
            //
            //     $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');
            //
            //     // dd($pm_name);
            //     $pm_contact = DB::table('employees')->where('FirstName',$pm_name)->value('MobileNumber');
            //     $pm_altcontact = DB::table('employees')->where('FirstName',$pm_name)->value('AlternateNumber');
            //
            //     if($pm_contact!=NULL)
            //     {
            //         $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //         //for extracting the date for generating reports
            //         $time = time();
            //         $date = date('m/d/Y',$time);
            //
            //         //actual sending of the message
            //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //         $ch =curl_init($url);
            //
            //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page =curl_exec($ch);
            //         // dd(curl_exec($ch));
            //         //expecting the report 'OK' here
            //         $report = substr($curl_scraped_page,0,2);
            //
            //         //extracting the job no of the message sent if it is at all
            //         $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //         curl_close($ch);
            //
            //         //url for retrieving the report
            //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //         $ch1 =curl_init($report1);
            //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page1 =curl_exec($ch1);
            //         // $report = substr($curl_scraped_page1,0,2);
            //         curl_close($ch1);
            //
            //         //retrieving the invalid mobile no text part out of the report
            //         $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //         // if($invalidmno == "Invalid Mno")
            //         // {
            //         //
            //         //     $checkbox=$request->addproduct;
            //         //
            //         //     session()->put('name',$who);
            //         //     Session::flash('warning','The Mobile number is invalid');
            //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //         // }
            //
            //         $i=0;
            //
            //         //sending a message again if not sent first time
            //
            //         //while the report generated doesn't have the message OK in it
            //         while($report!="OK" || $i<=1)
            //         {
            //             //even if i<1 trigger this only in cases where report is not = OK
            //             if($report!="OK")
            //             {
            //                 $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //                 $time = time();
            //                 $date = date('d/m/Y',$time);
            //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //                 $ch =curl_init($url);
            //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //                 $curl_scraped_page =curl_exec($ch);
            //                 $report = substr($curl_scraped_page,0,2);
            //                 curl_close($ch);
            //             }
            //             $i++;
            //         }
            //     }
            //
            //     // SMS for PM alternate contact number
            //
            //     if($pm_altcontact!=NULL)
            //     {
            //         $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //         //for extracting the date for generating reports
            //         $time = time();
            //         $date = date('m/d/Y',$time);
            //
            //         //actual sending of the message
            //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //         $ch =curl_init($url);
            //
            //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page =curl_exec($ch);
            //         // dd(curl_exec($ch));
            //         //expecting the report 'OK' here
            //         $report = substr($curl_scraped_page,0,2);
            //
            //         //extracting the job no of the message sent if it is at all
            //         $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //         curl_close($ch);
            //
            //         //url for retrieving the report
            //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //         $ch1 =curl_init($report1);
            //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //         $curl_scraped_page1 =curl_exec($ch1);
            //         // $report = substr($curl_scraped_page1,0,2);
            //         curl_close($ch1);
            //
            //         //retrieving the invalid mobile no text part out of the report
            //         $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //         // if($invalidmno == "Invalid Mno")
            //         // {
            //         //
            //         //     $checkbox=$request->addproduct;
            //         //
            //         //     session()->put('name',$who);
            //         //     Session::flash('warning','The Mobile number is invalid');
            //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //         // }
            //
            //         $i=0;
            //
            //         //sending a message again if not sent first time
            //
            //         //while the report generated doesn't have the message OK in it
            //         while($report!="OK" || $i<=1)
            //         {
            //             //even if i<1 trigger this only in cases where report is not = OK
            //             if($report!="OK")
            //             {
            //                 $message="Dear ".$pm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //                 $time = time();
            //                 $date = date('d/m/Y',$time);
            //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($pm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //                 $ch =curl_init($url);
            //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //                 $curl_scraped_page =curl_exec($ch);
            //                 $report = substr($curl_scraped_page,0,2);
            //                 curl_close($ch);
            //             }
            //             $i++;
            //         }
            //     }
            // }
            // // if($phonenimber!=NULL)
            // // {
            // //     $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            // //
            // //
            // //     //for extracting the date for generating reports
            // //     $time = time();
            // //     $date = date('m/d/Y',$time);
            // //
            // //     //actual sending of the message
            // //     $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            // //     $ch =curl_init($url);
            // //
            // //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // //     $curl_scraped_page =curl_exec($ch);
            // //     // dd(curl_exec($ch));
            // //     //expecting the report 'OK' here
            // //     $report = substr($curl_scraped_page,0,2);
            // //
            // //     //extracting the job no of the message sent if it is at all
            // //     $jobno = trim(substr($curl_scraped_page,3));
            // //
            // //
            // //     curl_close($ch);
            // //
            // //     //url for retrieving the report
            // //     $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            // //     $ch1 =curl_init($report1);
            // //     curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            // //     $curl_scraped_page1 =curl_exec($ch1);
            // //     // $report = substr($curl_scraped_page1,0,2);
            // //     curl_close($ch1);
            // //
            // //     //retrieving the invalid mobile no text part out of the report
            // //     $invalidmno = substr($curl_scraped_page1,0,11);
            // //
            // //     //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            // //     if($invalidmno == "Invalid Mno")
            // //     {
            // //
            // //         $checkbox=$request->addproduct;
            // //
            // //         session()->put('name',$who);
            // //         Session::flash('warning','The Mobile number is invalid');
            // //         return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            // //     }
            // //
            // //     $i=0;
            // //
            // //     //sending a message again if not sent first time
            // //
            // //     //while the report generated doesn't have the message OK in it
            // //     while($report!="OK" || $i<=1)
            // //     {
            // //         //even if i<1 trigger this only in cases where report is not = OK
            // //         if($report!="OK")
            // //         {
            // //             $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            // //             $phonenimber=$contact;
            // //             $time = time();
            // //             $date = date('d/m/Y',$time);
            // //             $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            // //             $ch =curl_init($url);
            // //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // //             $curl_scraped_page =curl_exec($ch);
            // //             $report = substr($curl_scraped_page,0,2);
            // //             curl_close($ch);
            // //         }
            // //         $i++;
            // //     }
            // // }
            //
            // /*
            // SMS to Product Managers when Product is added -- sms going to Product Managers only ends here
            // */


            // code for calculate sum of all products

            /* code for product data by jatin starts here */
            //take out the details from the product table for that particular lead id
            $detail=DB::table('products')->where('id',$productid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail1);

            //retrieving id for product

            // Converting all products from json to array
            $ProductName = $detail1[0]['ProductName'];
            // dd($Mednames);
            $k =explode("\r\n",$ProductName);

            $SellingPrice =$detail1[0]['SellingPrice'];
            $k4 =explode("\r\n",$SellingPrice);


            $Type =$detail1[0]['Type'];
            $k5 =$Type;

            $Quantity =$detail1[0]['Quantity'];
            $k7 =explode("\r\n",$Quantity);



            $RentalPrice =$detail1[0]['RentalPrice'];
            $k15 =explode("\r\n",$RentalPrice);

            $TransportCharges = $detail1[0]['TransportCharges'];

            // dd($TransportCharges);
            // $k12 = explode("\r\n", $TransportCharges);



            $FinalAmount =$detail1[0]['FinalAmount'];



            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum = 0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {        if($k5=="Rent")
                {

                    $sum = $sum + ($k15[$cnt1]*$k7[$cnt1]);
                }
                else{
                    $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                }

            }


            if($k5=="Rent")
            {

                $sum = $sum  + $TransportCharges;
            }

            // dd($sum);

            $finalamt = product::find($productid);
            $finalamt->FinalAmount= $sum;
            $finalamt->save();

            // code end




            // Code for Product Logs starts here

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            // dd($leadid);

            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Lead Created for Product";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->lead_id = $leadid;
            $activity->save();
        }







        // checking if more than one product is inserted or not

        $no=$request->nooffileds;
        $no1=$request->nooffiledsPharmacyDetails;


        // creating 2 variable to check if they create both rent and sell request in one submit. this will help to split the request
        $productid2=1;
        $try = null;
        // dd($no);
        if($no!=NULL)
        {

            for($i=2;$i<=$no;$i++)
            {

                $SKUid1="SKUid".$i;
                $ProductName1="ProductName".$i;
                $DemoRequired1="DemoRequired".$i;
                $AvailabilityStatus1="AvailabilityStatus".$i;
                $AvailabilityAddress1="AvailabilityAddress".$i;
                $SellingPrice1="SellingPrice".$i;
                $RentalPrice1="RentalPrice".$i;
                $Type1="Type".$i;
                $OrderStatus1="OrderStatus".$i;
                $Quantity1="Quantity".$i;
                $ModeofPaymentrent1="ModeofPaymentrent".$i;
                $OrderStatusrent1="OrderStatusrent".$i;
                $ModeofPayment1="ModeofPayment".$i;
                $AdvanceAmt1="AdvanceAmt".$i;
                $StartDate1="StartDate".$i;
                $EndDate1="EndDate".$i;
                $OverdueAmt1="OverdueAmt".$i;


                $SKUid=$request->$SKUid1;
                $ProductName=$request->$ProductName1;
                $DemoRequired=$request->$DemoRequired1;
                $AvailabilityStatus=$request->$AvailabilityStatus1;
                $AvailabilityAddress=$request->$AvailabilityAddress1;
                $SellingPrice=$request->$SellingPrice1;
                $RentalPrice=$request->$RentalPrice1;
                $Type=$request->$Type1;
                $OrderStatus=$request->$OrderStatus1;
                $Quantity=$request->$Quantity1;
                $ModeofPaymentrent=$request->$ModeofPaymentrent1;
                $OrderStatusrent=$request->$OrderStatusrent1;
                $ModeofPayment=$request->$ModeofPayment1;
                $AdvanceAmt=$request->$AdvanceAmt1;
                $StartDate=$request->$StartDate1;
                $EndDate=$request->$EndDate1;
                $OverdueAmt=$request->$OverdueAmt1;


                // dd($productid);
                $checktype=DB::table('products')->where('id',$productid)->value('Type');

                if($Type == NULL || $Type=="" )
                {
                    $Type="Sell";
                }


                $try[] = $Type;

                if($Type==$checktype)
                {

                    $skudetail=DB::table('products')->where('id',$productid)->value('SKUid');
                    $ProductNamedetail=DB::table('products')->where('id',$productid)->value('ProductName');
                    // $DemoRequireddetail=DB::table('products')->where('id',$productid)->value('DemoRequired');
                    $AvailabilityStatusdetail=DB::table('products')->where('id',$productid)->value('AvailabilityStatus');
                    $AvailabilityAddressdetail=DB::table('products')->where('id',$productid)->value('AvailabilityAddress');
                    $SellingPricedetail=DB::table('products')->where('id',$productid)->value('SellingPrice');
                    $RentalPricedetail=DB::table('products')->where('id',$productid)->value('RentalPrice');
                    // $Typedetail=DB::table('products')->where('id',$productid)->value('Type');
                    // $OrderStatusdetail=DB::table('products')->where('id',$productid)->value('OrderStatus');
                    $Quantitydetail=DB::table('products')->where('id',$productid)->value('Quantity');
                    // $ModeofPaymentrentdetail=DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
                    // $OrderStatusrentdetail=DB::table('products')->where('id',$productid)->value('OrderStatusrent');
                    // $ModeofPaymentdetail=DB::table('products')->where('id',$productid)->value('ModeofPayment');
                    $AdvanceAmtdetail=DB::table('products')->where('id',$productid)->value('AdvanceAmt');
                    $StartDatedetail=DB::table('products')->where('id',$productid)->value('StartDate');
                    $EndDatedetail=DB::table('products')->where('id',$productid)->value('EndDate');
                    $OverdueAmtdetail=DB::table('products')->where('id',$productid)->value('OverdueAmt');
                }
                else
                {
                    $skudetail=DB::table('products')->where('id',$productid2)->value('SKUid');
                    $ProductNamedetail=DB::table('products')->where('id',$productid2)->value('ProductName');
                    // $DemoRequireddetail=DB::table('products')->where('id',$productid)->value('DemoRequired');
                    $AvailabilityStatusdetail=DB::table('products')->where('id',$productid2)->value('AvailabilityStatus');
                    $AvailabilityAddressdetail=DB::table('products')->where('id',$productid2)->value('AvailabilityAddress');
                    $SellingPricedetail=DB::table('products')->where('id',$productid2)->value('SellingPrice');
                    $RentalPricedetail=DB::table('products')->where('id',$productid2)->value('RentalPrice');
                    // $Typedetail=DB::table('products')->where('id',$productid)->value('Type');
                    // $OrderStatusdetail=DB::table('products')->where('id',$productid)->value('OrderStatus');
                    $Quantitydetail=DB::table('products')->where('id',$productid2)->value('Quantity');
                    // $ModeofPaymentrentdetail=DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
                    // $OrderStatusrentdetail=DB::table('products')->where('id',$productid)->value('OrderStatusrent');
                    // $ModeofPaymentdetail=DB::table('products')->where('id',$productid)->value('ModeofPayment');
                    $AdvanceAmtdetail=DB::table('products')->where('id',$productid2)->value('AdvanceAmt');
                    $StartDatedetail=DB::table('products')->where('id',$productid2)->value('StartDate');
                    $EndDatedetail=DB::table('products')->where('id',$productid2)->value('EndDate');
                    $OverdueAmtdetail=DB::table('products')->where('id',$productid2)->value('OverdueAmt');
                }




                // in case if they skip 1 product details and insert 2nd detail
                if($productid ==1)
                {


                    $lead = new lead;
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    //If the source field is not filled, then take the reference id of the selected Reference
                    //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
                    if($request->source == Null)
                    {
                        //take out the value in the "reference" field, and assign the same value to "source" as well
                        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                        $source=$refer;
                    }
                    //if the Other option is selected, then whatever is filled in the source field
                    //pass that to the "Source" field
                    else
                    {
                        $source=$request->source;
                    }

                    //fetch all the corresponding fields
                    $lead->fName=$request->clientfname;
                    $lead->mName=$request->clientmname;
                    $lead->lName=$request->clientlname;
                    $lead->EmailId=$request->clientemail;
                    $lead->Source=$source;
                    $lead->MobileNumber=$request->clientmob;
                    $lead->Alternatenumber=$request->clientalternateno;
                    $lead->EmergencyContact =$request->EmergencyContact;
                    $lead->AssesmentReq=$request->assesmentreq;
                    $lead->createdby=$request->loginname;
                    $lead->Referenceid=$ref;
                    $lead->empid=$empid;
                    $lead->save();

                    $leadid=DB::table('leads')->max('id');

                    $address = new address;

                    //fetch and store all the address details
                    $address->Address1=$request->Address1;
                    $address->Address2=$request->Address2;
                    $address->City=$request->City;
                    $address->District=$request->District;
                    $address->State=$request->State;
                    $address->PinCode=$request->PinCode;


                    $same1=$request->presentcontact;

                    if($same1=='same')
                    {
                        $address->PAddress1=$request->Address1;
                        $address->PAddress2=$request->Address2;
                        $address->PCity=$request->City;
                        $address->PDistrict=$request->District;
                        $address->PState=$request->State;
                        $address->PPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->PAddress1=$request->PAddress1;
                        $address->PAddress2=$request->PAddress2;
                        $address->PCity=$request->PCity;
                        $address->PDistrict=$request->PDistrict;
                        $address->PState=$request->PState;
                        $address->PPinCode=$request->PPinCode;
                    }

                    $same=$request->emergencycontact;
                    if($same=='same')
                    {
                        $address->EAddress1=$request->Address1;
                        $address->EAddress2=$request->Address2;
                        $address->ECity=$request->City;
                        $address->EDistrict=$request->District;
                        $address->EState=$request->State;
                        $address->EPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->EAddress1=$request->EAddress1;
                        $address->EAddress2=$request->EAddress2;
                        $address->ECity=$request->ECity;
                        $address->EDistrict=$request->EDistrict;
                        $address->EState=$request->EState;
                        $address->EPinCode=$request->EPinCode;
                    }

                    $same2=$request->shippingcontact;
                    if($same2=='same')
                    {
                        $address->SAddress1=$request->Address1;
                        $address->SAddress2=$request->Address2;
                        $address->SCity=$request->City;
                        $address->SDistrict=$request->District;
                        $address->SState=$request->State;
                        $address->SPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->SAddress1=$request->SAddress1;
                        $address->SAddress2=$request->SAddress2;
                        $address->SCity=$request->SCity;
                        $address->SDistrict=$request->SDistrict;
                        $address->SState=$request->SState;
                        $address->SPinCode=$request->SPinCode;
                    }


                    $address->leadid=$leadid;
                    $address->empid=$empid;
                    $address->save();
                    $addressid=DB::table('addresses')->max('id');

                    // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"

                    //
                    // //retrieving the session id of the person who is logged in
                    // $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                    //
                    // //retrieving the username of the presently logged in user
                    // $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
                    //
                    // // retrieving the employee id of the presently logged in user
                    // $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                    //
                    // //retrieving the log id of the person who logged in just now
                    // $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
                    //
                    //
                    // $activity = new Activity;
                    // $activity->emp_id = $emp_id;
                    // $activity->activity = "New Lead Created ";
                    // $activity->activity_time = new \DateTime();
                    // $activity->log_id = $log_id;
                    // $activity->lead_id = $leadid;
                    // $activity->save();



                    $order=new order;
                    $order->leadid=$leadid;
                    $order->save();
                    $orderid=DB::table('orders')->max('id');


                    $OrderStatus="New";
                    $OrderStatusrent="New";

                    if($request->Type=="Sell"  || $request->Type==NULL || $request->Type=="")
                    {
                        $OrderStatusrent=NULL;
                    }

                    if($request->Type=="Rent")
                    {
                        $OrderStatus=NULL;
                    }


                    $products = new product;
                    $products->SKUid=$SKUid;
                    $products->ProductName=$ProductName;
                    $products->DemoRequired=$DemoRequired;
                    $products->AvailabilityStatus=$AvailabilityStatus;
                    $products->AvailabilityAddress=$AvailabilityAddress;
                    $products->SellingPrice=$SellingPrice;
                    $products->RentalPrice=$RentalPrice;
                    $products->Type=$Type;
                    $products->OrderStatus=$OrderStatus;
                    $products->Quantity=$Quantity;
                    $products->deliverydate=$request->deliverydate;
                    $products->ModeofPaymentrent=$ModeofPaymentrent;
                    $products->OrderStatusrent=$OrderStatusrent;
                    $products->ModeofPayment=$ModeofPayment;
                    $products->AdvanceAmt=$AdvanceAmt;
                    $products->StartDate=$StartDate;
                    $products->EndDate=$EndDate;
                    $products->TransportCharges=$TransportCharges;
                    $products->OverdueAmt=$OverdueAmt;
                    $products->Requestcreatedby=$name1;
                    $products->Leadid=$leadid;
                    $products->save();




                    $maxproductid = DB::table('products')->max('id');

                    $productid=DB::table('products')->max('id');


                    $prodlead=new prodlead;
                    $prodlead->leadid=$leadid;
                    $prodlead->Prodid=$productid;
                    $prodlead->PharmacyId=$pharmacyid;
                    $prodlead->orderid=$orderid;
                    $prodlead->save();


                }
                else
                {
                    if($Type == NULL || $Type=="")
                    {
                        $Type="Sell";
                    }

                    if($Type==$checktype)
                    {
                        $products=product::find($productid);
                        $products->SKUid=$skudetail."\r\n".$SKUid;
                        $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                        // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                        $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                        $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                        $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                        $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                        // $products->Type=$Typedetail."\r\n".$Type;
                        // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                        $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                        // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                        // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                        // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                        $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                        $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                        $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                        $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;
                        $products->save();

                        $maxproductid = DB::table('products')->max('id');
                    }
                    else
                    {
                        if($productid2==1)
                        {


                            $OrderStatus="New";
                            $OrderStatusrent="New";

                            if($Type=="Sell" || $Type==NULL || $Type=="")
                            {
                                $OrderStatusrent=NULL;
                            }

                            if($Type=="Rent")
                            {
                                $OrderStatus=NULL;
                            }

                            $products = new product;
                            $products->SKUid=$SKUid;
                            $products->ProductName=$ProductName;
                            $products->DemoRequired=$DemoRequired;
                            $products->AvailabilityStatus=$AvailabilityStatus;
                            $products->AvailabilityAddress=$AvailabilityAddress;
                            $products->SellingPrice=$SellingPrice;
                            $products->RentalPrice=$RentalPrice;
                            $products->Type=$Type;
                            $products->OrderStatus=$OrderStatus;
                            $products->Quantity=$Quantity;
                            $products->deliverydate=$request->deliverydate;
                            $products->ModeofPaymentrent=$ModeofPaymentrent;
                            $products->OrderStatusrent=$OrderStatusrent;
                            $products->ModeofPayment=$ModeofPayment;
                            $products->AdvanceAmt=$AdvanceAmt;
                            $products->StartDate=$StartDate;
                            $products->EndDate=$EndDate;
                            // $products->TransportCharges=$TransportCharges;
                            $products->OverdueAmt=$OverdueAmt;
                            $products->Requestcreatedby=$name1;
                            $products->Leadid=$leadid;
                            $products->save();

                            // dd($products);


                            $maxproductid2 = DB::table('products')->max('id');

                            $productid2=DB::table('products')->max('id');

                            // dd($productid,$productid2);
                            $prodlead=new prodlead;
                            $prodlead->leadid=$leadid;
                            $prodlead->Prodid=$productid2;
                            $prodlead->PharmacyId=$pharmacyid;
                            $prodlead->orderid=$orderid;
                            $prodlead->save();

                            if($Type=="Sell" || $Type==NULL || $Type=="")
                            {
                                // dd($Type);
                                $pm_desig = "Product Manager";
                                $pm_dept = "Product - Selling";

                                $pm_name = DB::table('employees')->select('FirstName')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->get();
                                $pm_name = json_decode($pm_name,true);
                                $count_name = count($pm_name);
                                // dd($count_name);

                                for($j=0;$j<$count_name;$j++)
                                {
                                    $pm_mail = DB::table('employees')->where('FirstName',$pm_name[$j]['FirstName'])->value('Email_id');
                                    // dd($pm_mail);
                                    $pm_email[] = $pm_mail;
                                }
                            }
                            else if($Type=="Rent")
                            {

                                $pm_desig = "Product Manager";
                                $pm_dept = "Product - Rental";

                                $pm_name = DB::table('employees')->select('FirstName')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->get();
                                $pm_name = json_decode($pm_name,true);
                                $count_name = count($pm_name);
                                // dd($count_name);

                                for($j=0;$j<$count_name;$j++)
                                {
                                    $pm_mail = DB::table('employees')->where('FirstName',$pm_name[$j]['FirstName'])->value('Email_id');
                                    // dd($pm_mail);
                                    $pm_email[] = $pm_mail;
                                }



                            }
                            // dd($pm_email);
                            for($j=0;$j<$count_name;$j++)
                            {
                                if($pm_email[$j]!=NULL && $active_state==0)
                                {
                                    // dd($pm_email);
                                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_email'=>$customer_email,'cust_mob'=>$customer_contact,'pm_name'=>$pm_name,'pm_email'=>$pm_email,'prod_creator'=>$name1];


                                    Mail::send('mail_for_product2', $prod_data, function($message)use($prod_data) {
                                        $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                                        ("Order Id [".$prod_data['orderid']."] Created!!!");
                                        $message->from('care@healthheal.in','Healthheal');
                                    });
                                }
                            }

                        }
                        else
                        {
                            $products=product::find($productid2);
                            $products->SKUid=$skudetail."\r\n".$SKUid;
                            $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                            // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                            $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                            $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                            $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                            $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                            // $products->Type=$Typedetail."\r\n".$Type;
                            // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                            $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                            // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                            // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                            // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                            $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                            $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                            $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                            $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;
                            $products->save();

                            $maxproductid2 = DB::table('products')->max('id');
                        }

                    }


                    // append all product in one row

                    /*


                    $products=product::find($productid);
                    $products->SKUid=$skudetail."\r\n".$SKUid;
                    $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                    // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                    $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                    $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                    $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                    $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                    // $products->Type=$Typedetail."\r\n".$Type;
                    // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                    $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                    // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                    // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                    // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                    $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                    $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                    $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                    $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;
                    $products->save();


                    */

                    //  $products = new product;
                    // $products->SKUid=$SKUid;
                    // $products->ProductName=$ProductName;
                    // $products->DemoRequired=$DemoRequired;
                    // $products->AvailabilityStatus=$AvailabilityStatus;
                    // $products->AvailabilityAddress=$AvailabilityAddress;
                    // $products->SellingPrice=$SellingPrice;
                    // $products->RentalPrice=$RentalPrice;
                    // $products->Type=$Type;
                    // $products->OrderStatus=$OrderStatus;
                    // $products->Quantity=$Quantity;
                    // $products->ModeofPaymentrent=$ModeofPaymentrent;
                    // $products->OrderStatusrent=$OrderStatusrent;
                    // $products->ModeofPayment=$ModeofPayment;
                    // $products->AdvanceAmt=$AdvanceAmt;
                    // $products->StartDate=$StartDate;
                    // $products->EndDate=$EndDate;
                    // $products->OverdueAmt=$OverdueAmt;
                    // $products->Leadid=$leadid;
                    // $products->save();

                    // $productid=DB::table('products')->max('id');


                    // $prodlead=new prodlead;
                    // $prodlead->leadid=$leadid;
                    // $prodlead->Prodid=$productid;
                    // $prodlead->PharmacyId="1";
                    // $prodlead->orderid=$orderid;
                    // $prodlead->save();


                }
            }


            // code for calculate sum of all products

            /* code for product data by jatin starts here */
            //take out the details from the product table for that particular lead id
            $detail=DB::table('products')->where('id',$productid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail1);

            //retrieving id for product

            // Converting all products from json to array
            $ProductName = $detail1[0]['ProductName'];
            // dd($Mednames);
            $k =explode("\r\n",$ProductName);



            $SKUid =$detail1[0]['SKUid'];
            $k0 = explode("\r\n",$SKUid);

            $DemoRequired =$detail1[0]['DemoRequired'];
            $k1 = $DemoRequired;


            $AvailabilityStatus =$detail1[0]['AvailabilityStatus'];
            $k2 =explode("\r\n",$AvailabilityStatus);


            $AvailabilityAddress =$detail1[0]['AvailabilityAddress'];
            $k3 =explode("\r\n",$AvailabilityAddress);

            $SellingPrice =$detail1[0]['SellingPrice'];
            $k4 =explode("\r\n",$SellingPrice);


            $Type =$detail1[0]['Type'];
            $k5 =$Type;




            $OrderStatus =$detail1[0]['OrderStatus'];
            $k6 =$OrderStatus;


            $Quantity =$detail1[0]['Quantity'];
            $k7 =explode("\r\n",$Quantity);

            $ModeofPaymentrent =$detail1[0]['ModeofPaymentrent'];
            $k8 =$ModeofPaymentrent;

            $OrderStatusrent =$detail1[0]['OrderStatusrent'];
            $k9 =$OrderStatusrent;

            $ModeofPayment =$detail1[0]['ModeofPayment'];
            $k10 =$ModeofPayment;

            $AdvanceAmt =$detail1[0]['AdvanceAmt'];
            $k11 =explode("\r\n",$AdvanceAmt);

            $StartDate =$detail1[0]['StartDate'];
            $k12 =explode("\r\n",$StartDate);

            $EndDate =$detail1[0]['EndDate'];
            $k13 =explode("\r\n",$EndDate);

            $OverdueAmt =$detail1[0]['OverdueAmt'];
            $k14 =explode("\r\n",$OverdueAmt);

            $RentalPrice =$detail1[0]['RentalPrice'];
            $k15 =explode("\r\n",$RentalPrice);

            $Receipt =$detail1[0]['Receipt'];


            $Cheque =$detail1[0]['Cheque'];

            $cashstatus =$detail1[0]['CashStatus'];

            $discount =$detail1[0]['Discount'];

            $apaid =$detail1[0]['AmountPaid'];

            $pdiscount =$detail1[0]['PostDiscountPrice'];

            $FinalAmount =$detail1[0]['FinalAmount'];



            $cnt2 = count($k);

            // dd($productid2,$productid);
            $cnt1 = 0;


            $sum = 0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {        if($k5=="Rent")
                {

                    $sum = $sum + $k15[$cnt1]*$k7[$cnt1];
                }
                else{
                    $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                }

            }



            $finalamt = product::find($productid);
            $finalamt->FinalAmount= $sum;
            $finalamt->save();

            // dd($productid2);

            if($productid2!=1)
            {

                $detail=DB::table('products')->where('id',$productid2)->get();
                $detail1 = json_decode($detail,true);
                // dd($detail1);

                //retrieving id for product

                // Converting all products from json to array
                $ProductName = $detail1[0]['ProductName'];
                // dd($Mednames);
                $k =explode("\r\n",$ProductName);



                $SKUid =$detail1[0]['SKUid'];
                $k0 = explode("\r\n",$SKUid);

                $DemoRequired =$detail1[0]['DemoRequired'];
                $k1 = $DemoRequired;


                $AvailabilityStatus =$detail1[0]['AvailabilityStatus'];
                $k2 =explode("\r\n",$AvailabilityStatus);


                $AvailabilityAddress =$detail1[0]['AvailabilityAddress'];
                $k3 =explode("\r\n",$AvailabilityAddress);

                $SellingPrice =$detail1[0]['SellingPrice'];
                $k4 =explode("\r\n",$SellingPrice);


                $Type =$detail1[0]['Type'];
                $k5 =$Type;


                $OrderStatus =$detail1[0]['OrderStatus'];
                $k6 =$OrderStatus;


                $Quantity =$detail1[0]['Quantity'];
                $k7 =explode("\r\n",$Quantity);

                $ModeofPaymentrent =$detail1[0]['ModeofPaymentrent'];
                $k8 =$ModeofPaymentrent;

                $OrderStatusrent =$detail1[0]['OrderStatusrent'];
                $k9 =$OrderStatusrent;

                $ModeofPayment =$detail1[0]['ModeofPayment'];
                $k10 =$ModeofPayment;

                $AdvanceAmt =$detail1[0]['AdvanceAmt'];
                $k11 =explode("\r\n",$AdvanceAmt);

                $StartDate =$detail1[0]['StartDate'];
                $k12 =explode("\r\n",$StartDate);

                $EndDate =$detail1[0]['EndDate'];
                $k13 =explode("\r\n",$EndDate);

                $OverdueAmt =$detail1[0]['OverdueAmt'];
                $k14 =explode("\r\n",$OverdueAmt);

                $RentalPrice =$detail1[0]['RentalPrice'];
                $k15 =explode("\r\n",$RentalPrice);

                $Receipt =$detail1[0]['Receipt'];


                $Cheque =$detail1[0]['Cheque'];

                $cashstatus =$detail1[0]['CashStatus'];

                $discount =$detail1[0]['Discount'];

                $apaid =$detail1[0]['AmountPaid'];

                $pdiscount =$detail1[0]['PostDiscountPrice'];

                $FinalAmount =$detail1[0]['FinalAmount'];



                $cnt2 = count($k);

                // dd($cnt4);
                $cnt1 = 0;


                $sum = 0;

                for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
                {        if($k5=="Rent")
                    {

                        $sum = $sum + $k15[$cnt1]*$k7[$cnt1];
                    }
                    else{
                        $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                    }

                }


                $finalamt = product::find($productid2);
                $finalamt->FinalAmount= $sum;
                $finalamt->save();

                // code end
            }

            // code end
        }





        if($c1!=NULL)
        {


            $pOrderStatus="New";


            $pharmacy = new pharmacy;
            $pharmacy->MedName=$request->MedName;
            $pharmacy->Strength=$request->Strength;
            $pharmacy->MedType=$request->MedType;
            $pharmacy->PQuantity=$request->pQuantity;
            $pharmacy->QuantityType=$request->QuantityType;
            $pharmacy->PAvailabilityStatus=$request->pAvailabilityStatus;
            $pharmacy->Price=$request->pSellingPrice;
            $pharmacy->POrderStatus=$pOrderStatus;
            $pharmacy->PModeofpayment=$request->PModeofpayment;
            $pharmacy->Pdeliverydate=$request->Pdeliverydate;
            $pharmacy->Prequestcreatedby=$name1;
            $pharmacy->leadid=$leadid;
            $pharmacy->save();

            /*
            SMS for Pharmacy -- message going to Pharmacy Manager starts here
            */
            //
            // $phm_desig = "Pharmacy Manager";
            //
            // $phm_name = DB::table('employees')->where('Designation2', $phm_desig)->value('FirstName');
            //
            // $phm_contact = DB::table('employees')->where('FirstName',$phm_name)->value('MobileNumber');
            // $phm_altcontact = DB::table('employees')->where('FirstName',$phm_name)->value('AlternateNumber');
            //
            // if($phm_contact!=NULL)
            // {
            //     $message="Dear ".$phm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //     //for extracting the date for generating reports
            //     $time = time();
            //     $date = date('m/d/Y',$time);
            //
            //     //actual sending of the message
            //     $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phm_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //     $ch =curl_init($url);
            //
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //     $curl_scraped_page =curl_exec($ch);
            //     // dd(curl_exec($ch));
            //     //expecting the report 'OK' here
            //     $report = substr($curl_scraped_page,0,2);
            //
            //     //extracting the job no of the message sent if it is at all
            //     $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //     curl_close($ch);
            //
            //     //url for retrieving the report
            //     $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //     $ch1 =curl_init($report1);
            //     curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //     $curl_scraped_page1 =curl_exec($ch1);
            //     // $report = substr($curl_scraped_page1,0,2);
            //     curl_close($ch1);
            //
            //     //retrieving the invalid mobile no text part out of the report
            //     $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //     // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //     // if($invalidmno == "Invalid Mno")
            //     // {
            //     //
            //     //     $checkbox=$request->addproduct;
            //     //
            //     //     session()->put('name',$who);
            //     //     Session::flash('warning','The Mobile number is invalid');
            //     //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //     // }
            //
            //     $i=0;
            //
            //     //sending a message again if not sent first time
            //
            //     //while the report generated doesn't have the message OK in it
            //     while($report!="OK" || $i<=1)
            //     {
            //         //even if i<1 trigger this only in cases where report is not = OK
            //         if($report!="OK")
            //         {
            //             $message="Dear ".$phm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //             $time = time();
            //             $date = date('d/m/Y',$time);
            //             $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //             $ch =curl_init($url);
            //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //             $curl_scraped_page =curl_exec($ch);
            //             $report = substr($curl_scraped_page,0,2);
            //             curl_close($ch);
            //         }
            //         $i++;
            //     }
            // }
            //
            // // SMS for PM alternate contact number
            //
            // if($phm_altcontact!=NULL)
            // {
            //     $message="Dear ".$phm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //
            //
            //     //for extracting the date for generating reports
            //     $time = time();
            //     $date = date('m/d/Y',$time);
            //
            //     //actual sending of the message
            //     $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phm_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            //     $ch =curl_init($url);
            //
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //     $curl_scraped_page =curl_exec($ch);
            //     // dd(curl_exec($ch));
            //     //expecting the report 'OK' here
            //     $report = substr($curl_scraped_page,0,2);
            //
            //     //extracting the job no of the message sent if it is at all
            //     $jobno = trim(substr($curl_scraped_page,3));
            //
            //
            //     curl_close($ch);
            //
            //     //url for retrieving the report
            //     $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            //     $ch1 =curl_init($report1);
            //     curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            //     $curl_scraped_page1 =curl_exec($ch1);
            //     // $report = substr($curl_scraped_page1,0,2);
            //     curl_close($ch1);
            //
            //     //retrieving the invalid mobile no text part out of the report
            //     $invalidmno = substr($curl_scraped_page1,0,11);
            //
            //     // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            //     // if($invalidmno == "Invalid Mno")
            //     // {
            //     //
            //     //     $checkbox=$request->addproduct;
            //     //
            //     //     session()->put('name',$who);
            //     //     Session::flash('warning','The Mobile number is invalid');
            //     //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
            //     // }
            //
            //     $i=0;
            //
            //     //sending a message again if not sent first time
            //
            //     //while the report generated doesn't have the message OK in it
            //     while($report!="OK" || $i<=1)
            //     {
            //         //even if i<1 trigger this only in cases where report is not = OK
            //         if($report!="OK")
            //         {
            //             $message="Dear ".$phm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            //             $time = time();
            //             $date = date('d/m/Y',$time);
            //             $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phm_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            //             $ch =curl_init($url);
            //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //             $curl_scraped_page =curl_exec($ch);
            //             $report = substr($curl_scraped_page,0,2);
            //             curl_close($ch);
            //         }
            //         $i++;
            //     }
            // }
            //
            //

            /*
            SMS for Pharmacy -- message going to Pharmacy Manager ends here
            */

            // Mail going to Pharmacy Manager if the Pharmacy Lead is created starts here


            $pm_desig = "Pharmacy Manager";
            //
            $pm_name = DB::table('employees')->select('FirstName')->where('Designation2', $pm_desig)->get();
            $pm_name = json_decode($pm_name,true);
            $count_name = count($pm_name);
            // dd($count_name);

            for($i=0;$i<$count_name;$i++)
            {
                $pm_mail = DB::table('employees')->where('FirstName',$pm_name[$i]['FirstName'])->value('Email_id');
                // dd($pm_mail);
                $pm_email[] = $pm_mail;
            }

            // dd($pm_email);
            $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_email'=>$customer_email,'cust_mob'=>$customer_contact,'pm_name'=>$pm_name,'pm_email'=>$pm_email,'prod_creator'=>$name1];

            $active_state = DB::table('sms_email_filter')->value('active_state');

            for($i=0;$i<$count_name;$i++)
            {
                if($pm_email!=NULL && $active_state==0)
                {
                    Mail::send('mail_for_product2', $prod_data, function($message)use($prod_data) {
                        $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                        ("Order Id [".$prod_data['orderid']."] Created!!!");
                        $message->from('care@healthheal.in','Healthheal');
                    });
                }
            }
            // dd($pm_email);
            // Mail going to Pharmacy Manager if the Pharmacy Lead is created ends here


            $pharmacyid=DB::table('pharmacies')->max('id');
            // dd($pharmacyid);
            // Inserting data in Prodleads table to map id of pharmacy table and product table
            $prodlead=new prodlead;
            $prodlead->leadid=$leadid;
            $prodlead->Prodid="1";
            $prodlead->PharmacyId=$pharmacyid;
            $prodlead->orderid=$orderid;
            $prodlead->save();

            // Code for Product Logs starts here

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            // dd($leadid);

            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Lead Created for Pharmacy";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->lead_id = $leadid;
            $activity->save();
        }





        if($no1!=NULL)
        {

            for($i=2;$i<=$no1;$i++)
            {

                $MedName1="MedName".$i;
                $Strength1="Strength".$i;
                $PQuantity1="pQuantity".$i;
                $QuantityType1="QuantityType".$i;
                $MedType1="MedType".$i;
                $Price1="pSellingPrice".$i;
                $PAvailabilityStatus1="PAvailabilityStatus".$i;
                $POrderStatus1="POrderStatus".$i;
                $PModeofpayment1="PModeofpayment".$i;
                // dd($request->$POrderStatus1);


                $MedName=$request->$MedName1;
                $Strength=$request->$Strength1;
                $PQuantity=$request->$PQuantity1;
                $QuantityType=$request->$QuantityType1;
                $MedType=$request->$MedType1;
                $Price=$request->$Price1;
                $PAvailabilityStatus=$request->$PAvailabilityStatus1;
                $POrderStatus=$request->$POrderStatus1;
                $PModeofpayment=$request->$PModeofpayment1;



                $meddetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('MedName');
                $strengthdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('Strength');
                $typedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('MedType');
                $quantitydetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PQuantity');
                $QuantityTypedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');
                $availablitydetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PAvailabilityStatus');
                $pricedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('Price');
                $statusdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('POrderStatus');
                $paymentdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PModeofpayment');


                if($pharmacyid == 1)
                {
                    $lead = new lead;
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    //If the source field is not filled, then take the reference id of the selected Reference
                    //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
                    if($request->source == Null)
                    {
                        //take out the value in the "reference" field, and assign the same value to "source" as well
                        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                        $source=$refer;
                    }
                    //if the Other option is selected, then whatever is filled in the source field
                    //pass that to the "Source" field
                    else
                    {
                        $source=$request->source;
                    }

                    //fetch all the corresponding fields
                    $lead->fName=$request->clientfname;
                    $lead->mName=$request->clientmname;
                    $lead->lName=$request->clientlname;
                    $lead->EmailId=$request->clientemail;
                    $lead->Source=$source;
                    $lead->MobileNumber=$request->clientmob;
                    $lead->Alternatenumber=$request->clientalternateno;
                    $lead->EmergencyContact =$request->EmergencyContact;
                    $lead->AssesmentReq=$request->assesmentreq;
                    $lead->createdby=$request->loginname;
                    $lead->Referenceid=$ref;
                    $lead->empid=$empid;
                    $lead->save();

                    $leadid=DB::table('leads')->max('id');

                    $address = new address;

                    //fetch and store all the address details
                    $address->Address1=$request->Address1;
                    $address->Address2=$request->Address2;
                    $address->City=$request->City;
                    $address->District=$request->District;
                    $address->State=$request->State;
                    $address->PinCode=$request->PinCode;


                    $same1=$request->presentcontact;

                    if($same1=='same')
                    {
                        $address->PAddress1=$request->Address1;
                        $address->PAddress2=$request->Address2;
                        $address->PCity=$request->City;
                        $address->PDistrict=$request->District;
                        $address->PState=$request->State;
                        $address->PPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->PAddress1=$request->PAddress1;
                        $address->PAddress2=$request->PAddress2;
                        $address->PCity=$request->PCity;
                        $address->PDistrict=$request->PDistrict;
                        $address->PState=$request->PState;
                        $address->PPinCode=$request->PPinCode;
                    }

                    $same=$request->emergencycontact;
                    if($same=='same')
                    {
                        $address->EAddress1=$request->Address1;
                        $address->EAddress2=$request->Address2;
                        $address->ECity=$request->City;
                        $address->EDistrict=$request->District;
                        $address->EState=$request->State;
                        $address->EPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->EAddress1=$request->EAddress1;
                        $address->EAddress2=$request->EAddress2;
                        $address->ECity=$request->ECity;
                        $address->EDistrict=$request->EDistrict;
                        $address->EState=$request->EState;
                        $address->EPinCode=$request->EPinCode;
                    }

                    $same2=$request->shippingcontact;
                    if($same2=='same')
                    {
                        $address->SAddress1=$request->Address1;
                        $address->SAddress2=$request->Address2;
                        $address->SCity=$request->City;
                        $address->SDistrict=$request->District;
                        $address->SState=$request->State;
                        $address->SPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->SAddress1=$request->SAddress1;
                        $address->SAddress2=$request->SAddress2;
                        $address->SCity=$request->SCity;
                        $address->SDistrict=$request->SDistrict;
                        $address->SState=$request->SState;
                        $address->SPinCode=$request->SPinCode;
                    }


                    $address->leadid=$leadid;
                    $address->empid=$empid;
                    $address->save();
                    $addressid=DB::table('addresses')->max('id');

                    // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"


                    // //retrieving the session id of the person who is logged in
                    // $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                    //
                    // //retrieving the username of the presently logged in user
                    // $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
                    //
                    // // retrieving the employee id of the presently logged in user
                    // $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                    //
                    // //retrieving the log id of the person who logged in just now
                    // $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
                    //
                    //
                    // $activity = new Activity;
                    // $activity->emp_id = $emp_id;
                    // $activity->activity = "New Lead Created ";
                    // $activity->activity_time = new \DateTime();
                    // $activity->log_id = $log_id;
                    // $activity->lead_id = $leadid;
                    // $activity->save();



                    $order=new order;
                    $order->leadid=$leadid;
                    $order->save();
                    $orderid=DB::table('orders')->max('id');



                    $POrderStatus="New";


                    $pharmacy = new pharmacy;
                    $pharmacy->MedName=$MedName;
                    $pharmacy->Strength=$Strength;
                    $pharmacy->MedType=$MedType;
                    $pharmacy->PQuantity=$PQuantity;
                    $pharmacy->QuantityType=$QuantityType;
                    $pharmacy->PAvailabilityStatus=$PAvailabilityStatus;
                    $pharmacy->Price=$Price;
                    $pharmacy->POrderStatus=$POrderStatus;
                    $pharmacy->PModeofpayment=$PModeofpayment;
                    $pharmacy->Pdeliverydate=$request->Pdeliverydate;
                    $pharmacy->Prequestcreatedby=$name1;
                    $pharmacy->leadid=$leadid;
                    $pharmacy->save();

                    /*
                    SMS for Pharmacy -- message going to Pharmacy Manager starts here
                    */
                    //
                    // $phm_desig = "Pharmacy Manager";
                    //
                    // $phm_name = DB::table('employees')->where('Designation2', $phm_desig)->value('FirstName');
                    //
                    // $phm_contact = DB::table('employees')->where('FirstName',$phm_name)->value('MobileNumber');
                    // $phm_altcontact = DB::table('employees')->where('FirstName',$phm_name)->value('AlternateNumber');
                    //
                    // if($phm_contact!=NULL)
                    // {
                    //     $message="Dear ".$phm_name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //
                    //
                    //     //for extracting the date for generating reports
                    //     $time = time();
                    //     $date = date('m/d/Y',$time);
                    //
                    //     //actual sending of the message
                    //     $url ="http
                    $pharmacyid=DB::table('pharmacies')->max('id');
                    // dd($pharmacyid);
                    // Inserting data in Prodleads table to map id of pharmacy table and product table
                    $prodlead=new prodlead;
                    $prodlead->leadid=$leadid;
                    $prodlead->Prodid="1";
                    $prodlead->PharmacyId=$pharmacyid;
                    $prodlead->orderid=$orderid;
                    $prodlead->save();

                    // Code for Product Logs starts here

                    //retrieving the session id of the person who is logged in
                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                    //retrieving the username of the presently logged in user
                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                    // retrieving the employee id of the presently logged in user
                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

                    //retrieving the log id of the person who logged in just now
                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                    // dd($leadid);

                    $activity = new Activity;
                    $activity->emp_id = $emp_id;
                    $activity->activity = "Lead Created for Pharmacy";
                    $activity->activity_time = new \DateTime();
                    $activity->log_id = $log_id;
                    $activity->lead_id = $leadid;
                    $activity->save();




                }
                else
                {


                    $pharmacy=pharmacy::find($pharmacyid);
                    $pharmacy->MedName=$meddetail."\r\n".$MedName;
                    $pharmacy->Strength=$strengthdetail."\r\n".$Strength;
                    $pharmacy->MedType=$typedetail."\r\n".$MedType;
                    $pharmacy->PQuantity=$quantitydetail."\r\n".$PQuantity;
                    $pharmacy->QuantityType=$QuantityTypedetail."\r\n".$QuantityType;
                    $pharmacy->PAvailabilityStatus=$availablitydetail."\r\n".$PAvailabilityStatus;
                    $pharmacy->Price=$pricedetail."\r\n".$Price;
                    // $pharmacy->POrderStatus=$statusdetail."\r\n".$POrderStatus;
                    // $pharmacy->PModeofpayment=$paymentdetail."\r\n".$PModeofpayment;
                    $pharmacy->save();
                    $pharmacyid=DB::table('pharmacies')->max('id');
                }



                // $prodlead=new prodlead;
                //     $prodlead->leadid=$leadid;
                //     $prodlead->Prodid="1";
                //     $prodlead->PharmacyId=$pharmacyid;
                //     $prodlead->orderid=$orderid;
                //     $prodlead->save();

            }

            /* code for pharmacy data by jatin starts here */
            //take out the details from the pharmacy table for that particular lead id
            $detail=DB::table('pharmacies')->where('id',$pharmacyid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail);


            //retrieving id for pharmacy

            // Converting all Mednames from json to array
            $Mednames = $detail1[0]['MedName'];
            // dd($Mednames);
            $k =explode("\r\n",$Mednames);


            $Strength =$detail1[0]  ['Strength'];
            $k1 = explode("\r\n",$Strength);


            $PQuantity =$detail1[0]['PQuantity'];
            $k2 =explode("\r\n",$PQuantity);

            $QuantityType =$detail1[0]['QuantityType'];
            $QuantityType =explode("\r\n",$QuantityType);


            $MedType =$detail1[0]['MedType'];
            $k3 =explode("\r\n",$MedType);

            $Price =$detail1[0]['Price'];
            $k4 =explode("\r\n",$Price);


            $PAvailabilityStatus =$detail1[0]['PAvailabilityStatus'];
            $k5 =explode("\r\n",$PAvailabilityStatus);


            $POrderStatus =$detail1[0]['POrderStatus'];
            $k6 =$POrderStatus;


            $PModeofpayment =$detail1[0]['PModeofpayment'];
            $k7 =$PModeofpayment;

            $Receipt =$detail1[0]['PReceipt'];

            $Cheque =$detail1[0]['PCheque'];

            $cashstatus =$detail1[0]['PCashStatus'];

            $discount =$detail1[0]['PDiscount'];

            $apaid =$detail1[0]['PAmountPaid'];

            $pdiscount =$detail1[0]['PPostDiscountPrice'];

            $PFinalAmount =$detail1[0]['PFinalAmount'];

            $Pdeliverydate =$detail1[0]['Pdeliverydate'];

            // dd($PFinalAmount);


            // $Quantities =$detail1[0]['PQuantity'];
            // $k3 = explode("\r\n",$Quantities);

            // $Quantities =$detail1[0]['PQuantity'];
            // $k4 = explode("\r\n",$Quantities);
            // dd($k1);
            // // $posd = NULL;
            //  $d = "Jatin\r\nSharma";
            // $posd = strpos($d,"\r\n");
            // dd($posd);

            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum=0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {
                $sum = $sum + $k4[$cnt1]*$k2[$cnt1];
            }

            $ph=pharmacy::find($pharmacyid);
            $ph->PFinalAmount=$sum;
            $ph->save();

        }











        /* checking if services has to be include or not */



        $checkbox=$request->addservice;

        if($checkbox == "add")
        {



            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();



            $leads=DB::table('leads')
            ->select('addresses.*','leads.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->where('leads.id',$leadid)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);


            session()->put('name',$name1);
            return view('cc.addservice',compact('gender','gender1','city','branch','leadtype','condition','language','relation','vertical','emp','leads','aid'));
        }

        $desig=DB::table('employees')->where('FirstName',$name1)->value('Designation');
        $desig2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
        $desig3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');


        if($desig=="Marketing")
        {
            session()->put('name',$name1);
            return redirect('/product');
        }
        if($desig=="Customer Care")
        {
            session()->put('name',$name1);
            return redirect('/product');
        }
        else
        {
            if($desig2=="Pharmacy Manager")
            {
                session()->put('name',$name1);
                return redirect('/product');
            }
            else
            {
                if($desig2=="Product Manager")
                {
                    session()->put('name',$name1);
                    return redirect('/product');
                }
                else
                {
                    if($desig=="Admin")
                    {
                        session()->put('name',$name1);
                        return redirect('/product');
                    }
                    else
                    {
                        if($desig=="Management")
                        {
                            session()->put('name',$name1);
                            return redirect('/product');
                        }
                        else
                        {
                            if($desig=="Vertical Head")
                            {
                                session()->put('name',$name1);
                                return redirect('/product');
                            }
                            else
                            {
                                if($desig3=="Field Executive")
                                {
                                    session()->put('name',$name1);
                                    return redirect('/product');
                                }
                                else
                                {
                                    if($desig3=="Field Officer")
                                    {
                                        session()->put('name',$name1);
                                        return redirect('/product');
                                    }
                                    else

                                    {
                                        session()->put('name',$name1);
                                        return redirect('/product');
                                    }
                                }
                            }
                        }

                    }

                }

            }



        }
    }


    /*
    to add service details if produt is added first and then added service later
    */
    public function addservice(Request $request)
    {
        $name1=$request->loginname;
        $leadid=$request->leaddetails;

        // $leadid=$request->leadid;
        // $addressid=$request->addressid;


        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');
        $empid=Null;

        //retrieve the employee id of the logged in user
        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');


        $details=DB::table('leads')->where('id',$leadid)->select('fName','mName','lName','EmailId','Source','MobileNumber','Alternatenumber','EmergencyContact','AssesmentReq','createdby','Referenceid','empid')->get();
        $details=json_decode($details);
        $detail1=$details[0]->fName;
        $detail2=$details[0]->mName;
        $detail3=$details[0]->lName;
        $detail4=$details[0]->EmailId;
        $detail5=$details[0]->Source;
        $detail6=$details[0]->MobileNumber;
        $detail7=$details[0]->Alternatenumber;
        $detail8=$details[0]->EmergencyContact;
        $detail9=$details[0]->AssesmentReq;
        $detail10=$details[0]->createdby;
        $detail11=$details[0]->Referenceid;
        $detail12=$details[0]->empid;


        $addressdetail=DB::table('addresses')->where('leadid',$leadid)->select('Address1','Address2','City','District','State','PinCode','PAddress1','PAddress2','PCity','PDistrict','PState','PPinCode','EAddress1','EAddress2','ECity','EDistrict','EState','EPinCode','SAddress1','SAddress2','SCity','SDistrict','SState','SPinCode')->get();
        $addressdetail=json_decode($addressdetail);
        $addressdetail1=$addressdetail[0]->Address1;
        $addressdetail2=$addressdetail[0]->Address2;
        $addressdetail3=$addressdetail[0]->City;
        $addressdetail4=$addressdetail[0]->District;
        $addressdetail5=$addressdetail[0]->State;
        $addressdetail6=$addressdetail[0]->PinCode;
        $addressdetail7=$addressdetail[0]->PAddress1;
        $addressdetail8=$addressdetail[0]->PAddress2;
        $addressdetail9=$addressdetail[0]->PCity;
        $addressdetail10=$addressdetail[0]->PDistrict;
        $addressdetail11=$addressdetail[0]->PState;
        $addressdetail12=$addressdetail[0]->PPinCode;
        $addressdetail13=$addressdetail[0]->EAddress1;
        $addressdetail14=$addressdetail[0]->EAddress2;
        $addressdetail15=$addressdetail[0]->ECity;
        $addressdetail16=$addressdetail[0]->EDistrict;
        $addressdetail17=$addressdetail[0]->EState;
        $addressdetail18=$addressdetail[0]->EPinCode;
        $addressdetail19=$addressdetail[0]->SAddress1;
        $addressdetail20=$addressdetail[0]->SAddress2;
        $addressdetail21=$addressdetail[0]->SCity;
        $addressdetail22=$addressdetail[0]->SDistrict;
        $addressdetail23=$addressdetail[0]->SState;
        $addressdetail24=$addressdetail[0]->SPinCode;



        $lead = new lead;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        //fetch all the corresponding fields
        $lead->fName=$detail1;
        $lead->mName=$detail2;
        $lead->lName=$detail3;
        $lead->EmailId=$detail4;
        $lead->Source=$detail5;
        $lead->MobileNumber=$detail6;
        $lead->Alternatenumber=$detail7;
        $lead->EmergencyContact =$detail8;
        $lead->AssesmentReq=$detail9;
        $lead->createdby=$detail10;
        $lead->Referenceid=$detail11;
        $lead->empid=$detail12;
        $lead->save();

        $leadid=DB::table('leads')->max('id');

        $address = new address;

        //fetch and store all the address details
        $address->Address1=$addressdetail1;
        $address->Address2=$addressdetail2;
        $address->City=$addressdetail3;
        $address->District=$addressdetail4;
        $address->State=$addressdetail5;
        $address->PinCode=$addressdetail6;


        $same1=$request->presentcontact;

        if($same1=='same')
        {
            $address->PAddress1=$addressdetail1;
            $address->PAddress2=$addressdetail2;
            $address->PCity=$addressdetail3;
            $address->PDistrict=$addressdetail4;
            $address->PState=$addressdetail5;
            $address->PPinCode=$addressdetail6;
        }

        else
        {
            $address->PAddress1=$addressdetail7;
            $address->PAddress2=$addressdetail8;
            $address->PCity=$addressdetail9;
            $address->PDistrict=$addressdetail10;
            $address->PState=$addressdetail11;
            $address->PPinCode=$addressdetail12;
        }

        $same=$request->emergencycontact;
        if($same=='same')
        {
            $address->EAddress1=$addressdetail1;
            $address->EAddress2=$addressdetail2;
            $address->ECity=$addressdetail3;
            $address->EDistrict=$addressdetail4;
            $address->EState=$addressdetail5;
            $address->EPinCode=$addressdetail6;
        }

        else
        {
            $address->EAddress1=$addressdetail13;
            $address->EAddress2=$addressdetail14;
            $address->ECity=$addressdetail15;
            $address->EDistrict=$addressdetail16;
            $address->EState=$addressdetail17;
            $address->EPinCode=$addressdetail18;
        }

        $same2=$request->shippingcontact;
        if($same2=='same')
        {
            $address->SAddress1=$addressdetail1;
            $address->SAddress2=$addressdetail2;
            $address->SCity=$addressdetail3;
            $address->SDistrict=$addressdetail4;
            $address->SState=$addressdetail5;
            $address->SPinCode=$addressdetail6;
        }

        else
        {
            $address->SAddress1=$addressdetail19;
            $address->SAddress2=$addressdetail20;
            $address->SCity=$addressdetail21;
            $address->SDistrict=$addressdetail22;
            $address->SState=$addressdetail23;
            $address->SPinCode=$addressdetail24;
        }

        $address->leadid=$leadid;
        $address->empid=$empid;
        $address->save();
        $addressid=DB::table('addresses')->max('id');





        $personnel = new personneldetail;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
        $personnel->Addressid=$addressid;
        $personnel->Leadid=$leadid;

        $personnel->save();



        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
        $langid=json_decode($l);


        //Enter the Service Details and store them
        $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assignedto;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
        $service->ServiceStatus=$request->servicestatus;
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$request->preferedlanguage;
        $service->Remarks=$request->remarks;
        $service->langId=$langid;
        $service->LeadId=$leadid;

        $service->save();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Services Added along with Products";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->lead_id = $leadid;
        $activity->save();

        $Design=DB::table('employees')->where('FirstName',$name1)->value('Designation');
        $Design1=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
        $Design2=DB::table('employees')->where('FirstName',$name1)->value('Designation3');

        if($Design=="Marketing")
        {
            session()->put('name',$name1);
            return redirect('/cc');
        }
        if($Design=="Customer Care")
        {
            session()->put('name',$name1);
            return redirect('/cc');
        }
        else
        {
            if($Design2=="Pharmacy Manager")
            {
                session()->put('name',$name1);
                return redirect('/product');
            }
            else
            {
                if($Design2=="Product Manager")
                {
                    session()->put('name',$name1);
                    return redirect('/product');
                }
                else
                {
                    if($Design=="Admin")
                    {
                        session()->put('name',$name1);
                        return redirect('/product');
                    }
                    else
                    {
                        if($Design=="Management")
                        {
                            session()->put('name',$name1);
                            return redirect('/product');
                        }
                        else
                        {
                            if($Design=="Vertical Head")
                            {
                                session()->put('name',$name1);
                                return redirect('/vh');
                            }
                            else
                            {
                                if($Design3=="Field Executive")
                                {
                                    session()->put('name',$name1);
                                    return redirect('/product');
                                }
                                else
                                {
                                    if($Design3=="Field Officer")
                                    {
                                        session()->put('name',$name1);
                                        return redirect('/product');
                                    }
                                    else

                                    {
                                        session()->put('name',$name1);
                                        return redirect('/vh');
                                    }
                                }
                            }
                        }

                    }

                }

            }
        }
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */


    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    **/


    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        // $shiftrequired=shiftrequired::find($id);
        // $shiftrequired->delete();
        // session()->flash('message','Delete Successfully');
        // return redirect('/shiftrequireds');
    }

    public function clientdetails(Request $request)
    {
        $clientid=$request->clientid1;
        $reference=DB::table('refers')->get();
        $gender=DB::table('genders')->get();
        $gender1=DB::table('genders')->get();
        $city=DB::table('cities')->get();
        $pcity=DB::table('cities')->get();
        $ecity=DB::table('cities')->get();
        $branch=DB::table('cities')->get();
        $status=DB::table('leadstatus')->get();
        $leadtype=DB::table('leadtypes')->get();
        $condition=DB::table('ptconditions')->get();
        $language=DB::table('languages')->get();
        $relation=DB::table('relationships')->get();

        $clientdetails=DB::table('leads')->where('id',$clientid)->get();

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $data1 = file_get_contents("indian_cities.json");

        $indian_cities = json_decode($data1,true);

        $count1 = count($indian_cities);



        $leads=DB::table('leads')
        ->select('addresses.*','leads.*')
        ->where('leads.id',$clientid)
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->get();

        $checkleads=DB::table('leads')
        ->select('addresses.*','leads.*')
        ->where('leads.id',$clientid)
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->value('leads.id');

        // dd($checkleads);

        if($checkleads==NULL)
        {
            session()->flash('message','Lead ID not found');
            return view('cc.clientdetailsnotfound',compact('count1','country_codes','indian_cities','count','reference'));
        }
        else
        {
            return view('cc.clientdetails',compact('count1','country_codes','indian_cities','count','clientdetails','reference','city','pcity','ecity','leads'));
        }


    }

    public function productdetails(Request $request)
    {
        $productname = $request->productname1;
        // dd($productname);
    }


    public function moreinput(Request $request)
    {
        $products=DB::table('productdetails')->get();
        $noofinputfiledsadded=$request->noofinputfiledsadded;
        $type = $request->type1;
        // dd($type);
        return view('cc/moreinput',compact('noofinputfiledsadded','products','type'));
    }


    public function moreinputPharmacyDetails(Request $request)
    {
        $countPharmacyDetails=$request->countPharmacyDetails;
        return view('cc/moreinputPharmacyDetails',compact('countPharmacyDetails'));
    }


    public function pharmacyshow(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

            $leadid=$_GET['id'];

            $name=$_GET['name'];
            $role=DB::table('employees')->where('FirstName',$name)->value('Designation');

            $id=DB::table('pharmacies')->where('id',$leadid)->value('leadid');


            // dd($leadid);

            $executive=DB::table('employees')->where('Designation3','Field Officer')->get();


            $orderid=DB::table('prodleads')->where('PharmacyId',$leadid)->value('orderid');
            $employeeid=DB::table('prodleads')->where('ProdId',$leadid)->value('empid');


            $brancheadname=DB::table('employees')->where('Designation',"Branch Head")->get();

            // dd($officerassign);
            $assign=DB::table('employees')->where('id',$employeeid)->value('FirstName');


            $officers=DB::table('employees')->where('Designation3','Field Officer')->get();
            $officerassign=DB::table('pharmacies')->where('id',$leadid)->value('PAssignedTo');

            // dd($officerassign);

            $comments=DB::table('prodleads')->where('PharmacyId',$leadid)->value('comments');
            $desig=DB::table('employees')->where('FirstName',$name)->value('Designation2');


            $leaddetails=DB::table('leads')
            ->select('addresses.*','leads.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->where('leads.id', $id)
            ->get();
            // dd($leaddetails);

            /* code for pharmacy data by jatin starts here */
            //take out the details from the pharmacy table for that particular lead id
            $detail=DB::table('pharmacies')->where('id',$leadid)->get();
            $detail1 = json_decode($detail,true);
            // dd($leadid);


            //retrieving id for pharmacy

            // Converting all Mednames from json to array
            $Mednames = $detail1[0]['MedName'];
            // dd($Mednames);
            $k =explode("\r\n",$Mednames);


            $Strength =$detail1[0]  ['Strength'];
            $k1 = explode("\r\n",$Strength);


            $QuantityType =$detail1[0]['QuantityType'];
            $QuantityType =explode("\r\n",$QuantityType);

            $PQuantity =$detail1[0]['PQuantity'];
            $k2 =explode("\r\n",$PQuantity);


            $MedType =$detail1[0]['MedType'];
            $k3 =explode("\r\n",$MedType);

            $Price =$detail1[0]['Price'];
            $k4 =explode("\r\n",$Price);


            $PAvailabilityStatus =$detail1[0]['PAvailabilityStatus'];
            $k5 =explode("\r\n",$PAvailabilityStatus);


            $POrderStatus =$detail1[0]['POrderStatus'];
            $k6 =$POrderStatus;


            $PModeofpayment =$detail1[0]['PModeofpayment'];
            $k7 =$PModeofpayment;

            $Receipt =$detail1[0]['PReceipt'];

            $Cheque =$detail1[0]['PCheque'];

            $cashstatus =$detail1[0]['PCashStatus'];

            $discount =$detail1[0]['PDiscount'];

            $apaid =$detail1[0]['PAmountPaid'];

            $pdiscount =$detail1[0]['PPostDiscountPrice'];

            $PFinalAmount =$detail1[0]['PFinalAmount'];


            $Pdeliverydate =$detail1[0]['Pdeliverydate'];

            // dd($PFinalAmount);


            // $Quantities =$detail1[0]['PQuantity'];
            // $k3 = explode("\r\n",$Quantities);

            // $Quantities =$detail1[0]['PQuantity'];
            // $k4 = explode("\r\n",$Quantities);
            // dd($k1);
            // // $posd = NULL;
            //  $d = "Jatin\r\nSharma";
            // $posd = strpos($d,"\r\n");
            // dd($posd);

            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum=0;


            // int $i=0;
            // while($cnt1--)
            // {
            // $Med1 = $k1[$cnt1-1];

            //  }
            // // for(int $i=0;$i<$cnt1;$i++)
            // // {
            // // $Med = implode("\r\n", $Mednames);

            // while
            // $Medpos = strpos($Mednames,"\r\n");
            // $FirstMed = substr($Mednames, 0,$Medpos);
            // dd($FirstMed);
            // $SecondMed = substr($Mednames, $Medpos+2);
            // dd($SecondMed);
            // }


            // for(int $i=0;$i<$cnt1;$i++)
            // {


            // }
            // dd($k,$k1,$k2,$k3,$k4,$k5,$k6,$k7,$cnt1,$cnt2);



            if($role=="Admin" || $role=="Management")
            {

                session()->put('name',$name);
                return view('product/adminpharmacydetails',compact('leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','cnt1','cnt2','sum','orderid','id','comments','desig','officers','officerassign','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','assign','executive','PFinalAmount','Pdeliverydate','brancheadname','QuantityType'));
            }


            session()->put('name',$name);
            return view('product/pharmacydetails',compact('leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','cnt1','cnt2','sum','orderid','id','comments','desig','officers','officerassign','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','assign','executive','PFinalAmount','Pdeliverydate','brancheadname','QuantityType'));

            /* code for pharmacy data by jatin ends here */
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }




    public function productshow(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

            $productid=$_GET['id'];
            $name=$_GET['name'];

            $underid=DB::table('employees')->where('FirstName',$name)->value('id');

            $executive=DB::table('employees')->where('Designation3','Field Executive')->where('under',$underid)->get();


            $officers=DB::table('employees')->where('Designation3','Field Officer')->get();
            $officerassign=DB::table('products')->where('id',$productid)->value('AssignedTo');



            $branchheadunderid=DB::table('employees')->where('FirstName',$officerassign)->value('id');
            $otherbranchexecutive=DB::table('employees')->where('Designation3','Field Executive')->where('under',$branchheadunderid)->get();

            $brancheadname=DB::table('employees')->where('Designation',"Branch Head")->get();

            $id=DB::table('products')->where('id',$productid)->value('leadid');
            $orderid=DB::table('prodleads')->where('ProdId',$productid)->value('orderid');
            $employeeid=DB::table('prodleads')->where('ProdId',$productid)->value('empid');



            $assign=DB::table('employees')->where('id',$employeeid)->value('FirstName');

            $comments=DB::table('prodleads')->where('ProdId',$productid)->value('comments');
            $desig=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $role=DB::table('employees')->where('FirstName',$name)->value('Designation');

            // dd($role);

            $leaddetails=DB::table('leads')
            ->select('addresses.*','leads.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->where('leads.id', $id)
            ->get();

            // dd($leaddetails);
            /* code for product data by jatin starts here */
            //take out the details from the product table for that particular lead id
            $detail=DB::table('products')->where('id',$productid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail1);

            //retrieving id for product

            // Converting all products from json to array
            $ProductName = $detail1[0]['ProductName'];
            // dd($Mednames);
            $k =explode("\r\n",$ProductName);



            $SKUid =$detail1[0]['SKUid'];
            $k0 = explode("\r\n",$SKUid);

            $DemoRequired =$detail1[0]['DemoRequired'];
            $k1 = $DemoRequired;


            $AvailabilityStatus =$detail1[0]['AvailabilityStatus'];
            $k2 =explode("\r\n",$AvailabilityStatus);


            $AvailabilityAddress =$detail1[0]['AvailabilityAddress'];
            $k3 =explode("\r\n",$AvailabilityAddress);

            $SellingPrice =$detail1[0]['SellingPrice'];
            $k4 =explode("\r\n",$SellingPrice);


            $Type =$detail1[0]['Type'];
            $k5 =$Type;


            $OrderStatus =$detail1[0]['OrderStatus'];
            $k6 =$OrderStatus;


            $Quantity =$detail1[0]['Quantity'];
            $k7 =explode("\r\n",$Quantity);

            $ModeofPaymentrent =$detail1[0]['ModeofPaymentrent'];
            $k8 =$ModeofPaymentrent;

            $OrderStatusrent =$detail1[0]['OrderStatusrent'];
            $k9 =$OrderStatusrent;

            $ModeofPayment =$detail1[0]['ModeofPayment'];
            $k10 =$ModeofPayment;

            $AdvanceAmt =$detail1[0]['AdvanceAmt'];
            $k11 =explode("\r\n",$AdvanceAmt);

            $StartDate =$detail1[0]['StartDate'];
            $k12 =explode("\r\n",$StartDate);

            $EndDate =$detail1[0]['EndDate'];
            $k13 =explode("\r\n",$EndDate);



            $OverdueAmt =$detail1[0]['OverdueAmt'];
            $k14 =explode("\r\n",$OverdueAmt);

            $RentalPrice =$detail1[0]['RentalPrice'];
            $k15 =explode("\r\n",$RentalPrice);

            $TransportCharges =$detail1[0]['TransportCharges'];


            $Receipt =$detail1[0]['Receipt'];


            $Cheque =$detail1[0]['Cheque'];

            $cashstatus =$detail1[0]['CashStatus'];

            $discount =$detail1[0]['Discount'];

            $apaid =$detail1[0]['AmountPaid'];

            $pdiscount =$detail1[0]['PostDiscountPrice'];

            $FinalAmount =$detail1[0]['FinalAmount'];

            $deliverydate =$detail1[0]['deliverydate'];







            // $Quantities =$detail1[0]['PQuantity'];
            // $k3 = explode("\r\n",$Quantities);

            // $Quantities =$detail1[0]['PQuantity'];
            // $k4 = explode("\r\n",$Quantities);
            // dd($k1);
            // // $posd = NULL;
            //  $d = "Jatin\r\nSharma";
            // $posd = strpos($d,"\r\n");
            // dd($posd);

            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum=0;


            // int $i=0;
            // while($cnt1--)
            // {
            // $Med1 = $k1[$cnt1-1];

            //  }
            // // for(int $i=0;$i<$cnt1;$i++)
            // // {
            // // $Med = implode("\r\n", $Mednames);

            // while
            // $Medpos = strpos($Mednames,"\r\n");
            // $FirstMed = substr($Mednames, 0,$Medpos);
            // dd($FirstMed);
            // $SecondMed = substr($Mednames, $Medpos+2);
            // dd($SecondMed);
            // }

            // for(int $i=0;$i<$cnt1;$i++)
            // {

            // }

            // }
            // dd($cnt1,$cnt2);

            if($role=="Admin" || $role=="Management" || $role=="Branch Head")
            {

                session()->put('name',$name);
                return view('product/adminproductdetails',compact('role','leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','k0','k8','k9','k10','k11','k12','k13','k14','k15','cnt1','cnt2','sum','orderid','productid','comments','desig','assign','officers','officerassign','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','FinalAmount','TransportCharges','deliverydate','brancheadname','otherbranchexecutive'));

            }

            session()->put('name',$name);

            return view('product/productdetails',compact('role','leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','k0','k8','k9','k10','k11','k12','k13','k14','k15','cnt1','cnt2','sum','orderid','productid','comments','desig','assign','officers','officerassign','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','FinalAmount','TransportCharges','deliverydate','brancheadname'));


            /* code for pharmacy data by jatin ends here */


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }


    //this function takes care of the scenario when the user changes the status in the any product detail
    public function productstatus(Request $request)
    {

        if(Auth::guard('admin')->check())
        {
            $name=$request->loginname;

            //if there is no Order status in the field or it is "New", change it to "Processing" when the person updates the form
            if($request->OrderStatus==NULL || $request->OrderStatus=="New")
            {
                $status="Processing";
            }
            //else accept the default order status
            else
            {
                $status=$request->OrderStatus;
            }

            //these hidden values are coming from "products/adminproductdetails.blade.php"
            $type=$request->type;

            $productid=$request->productid;

            $orderid=$request->id;

            $req=$request->leadfor;

            // $orderstatus = DB::table('products')->where('id',$productid)->value('OrderStatus');
            // $orderstatusrent = DB::table('products')->where('id',$productid)->value('orderstatusrent');
            // dd($orderstatus);
            // dd($request->TransportCharges);
            $ostatus=null;
            $ostatusr=null;

            // dd($request->TransportCharges);

            // if the "type" field has no value or it has the value "Sell"
            if($type==NULL || $type=="Sell")
            {

                //this is for finding the "row" where this product id exists
                $product = product::find($productid);

                //this will update the "OrderStatus" column with the status mentioned above
                $product->OrderStatus=$status;

                //we are retrieving the "OrderStatus" that is already present in the table
                $ostatus = DB::table('products')->where('id',$productid)->value('OrderStatus');

                //here, we are checking whether the status from the table is differnet from the one sent on "submit"
                if($ostatus != $request->OrderStatus)
                {
                    // Store the Changed field's "column name" and "value"
                    $proc[] = "Order Status";
                    $prod[] = $request->OrderStatus;
                }
                $product->save();
            }
            // if the "type" field has no value or it has the value "Rent"
            else
            {
                $product = product::find($productid);
                $product->OrderStatusrent=$status;

                $ostatusr = DB::table('products')->where('id',$productid)->value('OrderStatusrent');
                // dd($ostatusr);

                if($ostatusr != $request->OrderStatus)
                {
                    $proc[] = "Order Status Rent";
                    $prod[] = $request->OrderStatus;
                }


                $product->save();
            }

            // dd($request->TransportCharges);
            // check where the lead is assigned to filed officer or not

            /* Refer to comments for status above to get an idea of what is happening here */

            $transportcharges = DB::table('products')->where('id',$productid)->value('TransportCharges');

            if($request->TransportCharges!=NULL)
            {
                $product = product::find($productid);
                $product->TransportCharges = $request->TransportCharges;

                $product->save();
            }

            //only product manager can change status for this
            if($request->assignofficer!=NULL)
            {
                $product = product::find($productid);
                $product->AssignedTo=$request->assignofficer;

                $assigned = DB::table('products')->where('id',$productid)->value('AssignedTo');

                if($assigned != $request->assignofficer)
                {
                    $proc[] = "Assigned To";
                    $prod[] = $request->assignofficer;
                }


                $branchheadcheck=DB::table('employees')->where('FirstName',$request->assignofficer)->value('Designation');


                if($branchheadcheck == "Branch Head")
                {
                    $branchheadcity=DB::table('employees')->where('FirstName',$request->assignofficer)->value('City');

                    $leadidforproduct=DB::table('prodleads')->where('Prodid',$productid)->value('leadid');

                    DB::table('addresses')->where('leadid',$leadidforproduct)->update(['City'=>$branchheadcity]);
                }

                if($assigned != $request->assignofficer)
                {
                    $proc[] = "Assigned To";
                    $prod[] = $request->assignofficer;


                    $product->save();


                    //SMS when the Order is assigned to someone - Message going to Field officer -- starts here

                    //     $fo_desig = "Field Officer";
                    //
                    //     $fo_name = DB::table('employees')->where('Designation2', $fo_desig)->value('FirstName');
                    //
                    //
                    //     $fo_contact = DB::table('employees')->where('FirstName',$fo_name)->value('MobileNumber');
                    //
                    //     $fo_altcontact = DB::table('employees')->where('FirstName',$fo_name)->value('AlternateNumber');
                    //
                    //     if($fo_contact!=NULL)
                    //     {
                    //         $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //
                    //
                    //         //for extracting the date for generating reports
                    //         $time = time();
                    //         $date = date('m/d/Y',$time);
                    //
                    //         //actual sending of the message
                    //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                    //         $ch =curl_init($url);
                    //
                    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page =curl_exec($ch);
                    //         // dd(curl_exec($ch));
                    //         //expecting the report 'OK' here
                    //         $report = substr($curl_scraped_page,0,2);
                    //
                    //         //extracting the job no of the message sent if it is at all
                    //         $jobno = trim(substr($curl_scraped_page,3));
                    //
                    //
                    //         curl_close($ch);
                    //
                    //         //url for retrieving the report
                    //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                    //         $ch1 =curl_init($report1);
                    //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page1 =curl_exec($ch1);
                    //         // $report = substr($curl_scraped_page1,0,2);
                    //         curl_close($ch1);
                    //
                    //         //retrieving the invalid mobile no text part out of the report
                    //         $invalidmno = substr($curl_scraped_page1,0,11);
                    //
                    //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                    //         // if($invalidmno == "Invalid Mno")
                    //         // {
                    //         //
                    //         //     $checkbox=$request->addproduct;
                    //         //
                    //         //     session()->put('name',$who);
                    //         //     Session::flash('warning','The Mobile number is invalid');
                    //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                    //         // }
                    //
                    //         $i=0;
                    //
                    //         //sending a message again if not sent first time
                    //
                    //         //while the report generated doesn't have the message OK in it
                    //         while($report!="OK" || $i<=1)
                    //         {
                    //             //even if i<1 trigger this only in cases where report is not = OK
                    //             if($report!="OK")
                    //             {
                    //                 $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //                 $time = time();
                    //                 $date = date('d/m/Y',$time);
                    //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    //                 $ch =curl_init($url);
                    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //                 $curl_scraped_page =curl_exec($ch);
                    //                 $report = substr($curl_scraped_page,0,2);
                    //                 curl_close($ch);
                    //             }
                    //             $i++;
                    //         }
                    //     }
                    //
                    //     // SMS for Field officer alternate contact number
                    //
                    //     if($fo_altcontact!=NULL)
                    //     {
                    //         $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //
                    //
                    //         //for extracting the date for generating reports
                    //         $time = time();
                    //         $date = date('m/d/Y',$time);
                    //
                    //         //actual sending of the message
                    //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                    //         $ch =curl_init($url);
                    //
                    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page =curl_exec($ch);
                    //         // dd(curl_exec($ch));
                    //         //expecting the report 'OK' here
                    //         $report = substr($curl_scraped_page,0,2);
                    //
                    //         //extracting the job no of the message sent if it is at all
                    //         $jobno = trim(substr($curl_scraped_page,3));
                    //
                    //
                    //         curl_close($ch);
                    //
                    //         //url for retrieving the report
                    //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                    //         $ch1 =curl_init($report1);
                    //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page1 =curl_exec($ch1);
                    //         // $report = substr($curl_scraped_page1,0,2);
                    //         curl_close($ch1);
                    //
                    //         //retrieving the invalid mobile no text part out of the report
                    //         $invalidmno = substr($curl_scraped_page1,0,11);
                    //
                    //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                    //         // if($invalidmno == "Invalid Mno")
                    //         // {
                    //         //
                    //         //     $checkbox=$request->addproduct;
                    //         //
                    //         //     session()->put('name',$who);
                    //         //     Session::flash('warning','The Mobile number is invalid');
                    //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                    //         // }
                    //
                    //         $i=0;
                    //
                    //         //sending a message again if not sent first time
                    //
                    //         //while the report generated doesn't have the message OK in it
                    //         while($report!="OK" || $i<=1)
                    //         {
                    //             //even if i<1 trigger this only in cases where report is not = OK
                    //             if($report!="OK")
                    //             {
                    //                 $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //                 $time = time();
                    //                 $date = date('d/m/Y',$time);
                    //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    //                 $ch =curl_init($url);
                    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //                 $curl_scraped_page =curl_exec($ch);
                    //                 $report = substr($curl_scraped_page,0,2);
                    //                 curl_close($ch);
                    //             }
                    //             $i++;
                    //         }
                    //     }
                    // }

                    //SMS when the Order is assigned to someone - Message going to Field officer -- ends here

                    // dd($request->TransportCharges);
                    //Mail going to Field Officer when order is assigned to them and the status changes to "Processing" starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //who created the product
                    $product_creator_name = DB::table('products')->where('leadid',$leadid)->value('Requestcreatedby');


                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $customer_address1 = DB::table('addresses')->where('id',$leadid)->value('Address1');
                    $customer_shippingaddress1 = DB::table('addresses')->where('id',$leadid)->value('SAddress1');

                    if($customer_shippingaddress1!=NULL)
                    {
                        $customer_shippingaddress2 = DB::table('addresses')->where('leadid',$leadid)->value('SAddress2');
                        $customer_shipping_city = DB::table('addresses')->where('leadid',$leadid)->value('SCity');
                        $customer_shipping_district = DB::table('addresses')->where('leadid',$leadid)->value('SDistrict');
                        $customer_shipping_state = DB::table('addresses')->where('leadid',$leadid)->value('SState');
                        $customer_shipping_pincode = DB::table('addresses')->where('leadid',$leadid)->value('SPinCode');

                        $customer_address = $customer_shippingaddress1."".$customer_shippingaddress2." ".$customer_shipping_city." ".$customer_shipping_district." ".$customer_shipping_state." ".$customer_shipping_pincode;
                    }
                    else
                    {
                        $customer_address2 = DB::table('addresses')->where('leadid',$leadid)->value('Address2');
                        $customer_city = DB::table('addresses')->where('leadid',$leadid)->value('City');
                        $customer_district = DB::table('addresses')->where('leadid',$leadid)->value('District');
                        $customer_state = DB::table('addresses')->where('leadid',$leadid)->value('State');
                        $customer_pincode = DB::table('addresses')->where('leadid',$leadid)->value('PinCode');

                        $customer_address = $customer_address1." ".$customer_address2." ".$customer_city." ".$customer_district." ".$customer_state." ".$customer_pincode;

                    }

                    // dd($customer_address);

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'cust_addr'=>$customer_address,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name,'prod_creator'=>$product_creator_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product3', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id ".$prod_data['orderid']."[OrderID] In Processing!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail going to Field Officer when order is assigned to them and the status changes to "Processing" ends here
                }
                /* Refer to comments for status above to get an idea of what is happening here */
            }
            if($request->receipt!=NULL || $request->cheque!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->Receipt=$request->receipt;
                $product->Cheque=$request->cheque;

                $receipt = DB::table('products')->where('id',$productid)->value('Receipt');
                $cheque = DB::table('products')->where('id',$productid)->value('Cheque');

                if($receipt != $request->receipt)
                {
                    $proc[] = "Receipt";
                    $prod[] = $request->receipt;
                }

                if($cheque != $request->cheque)
                {
                    $proc[] = "Cheque";
                    $prod[] = $request->cheque;
                }

                $product->save();
            }

            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->modeofpaymentrent!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->ModeofPaymentrent=$request->modeofpaymentrent;

                $moder = DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');

                if($moder != $request->modeofpaymentrent)
                {
                    $proc[] = "Mode of Payment rent";
                    $prod[] = $request->modeofpaymentrent;
                }
                $product->save();
            }
            else
            {
                $product = product::find($productid);
                // dd($product);

                $product->ModeofPayment=$request->modeofpayment;

                $mode = DB::table('products')->where('id',$productid)->value('ModeofPayment');

                if($mode != $request->modeofpayment)
                {
                    $proc[] = "Mode of Payment";
                    $prod[] = $request->modeofpayment;
                }
                $product->save();
            }


            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->cashstatus!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->CashStatus=$request->cashstatus;

                $cash = DB::table('products')->where('id',$productid)->value('CashStatus');

                if($cash != $request->cashstatus)
                {
                    $proc[] = "Cash Status";
                    $prod[] = $request->cashstatus;
                }

                $product->save();
            }

            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->apaid!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->AmountPaid=$request->apaid;

                $amp = DB::table('products')->where('id',$productid)->value('AmountPaid');

                if($amp != $request->apaid)
                {
                    $proc[] = "Amount Paid";
                    $prod[] = $request->apaid;
                }

                $product->save();
            }

            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->discount!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->Discount=$request->discount;

                $dis = DB::table('products')->where('id',$productid)->value('Discount');

                if($dis != $request->discount)
                {
                    $proc[] = "Discount";
                    $prod[] = $request->discount;
                }
                $product->save();

            }

            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->total!=NULL)
            {
                $product = product::find($productid);
                // dd($product);

                $product->FinalAmount=$request->total - $transportcharges + $request->TransportCharges;
                // dd($product);

                $fa = DB::table('products')->where('id',$productid)->value('FinalAmount');

                if($fa != $request->total)
                {
                    $proc[] = "FinalAmount";
                    $prod[] = $request->total;
                }
                $product->save();
            }

            /* Refer to comments for status above to get an idea of what is happening here */

            if($request->discount!=NULL && $request->total!=NULL)
            {
                $sum=0;
                $d=$request->discount;
                $t=$request->total;

                $sum=(1- ($d/100))*$t;
                // dd($sum);

                $product = product::find($productid);
                // dd($product);

                $product->PostDiscountPrice=$sum;

                $pdp = DB::table('products')->where('id',$productid)->value('PostDiscountPrice');

                if($pdp != $sum)
                {
                    $proc[] = "PostDiscountPrice";
                    $prod[] = $sum;
                }
                $product->save();
            }






            $r=DB::table('prodleads')->where('ProdId',$productid)->value('comments');
            $a=$request->comment;

            /* Refer to comments for status above to get an idea of what is happening here */

            if($a!=NULL)
            {
                $time=time();
                $timestamp=date('m/d/Y h:i:s a', time());

                //here, we are appending the remarks that are entered by the user
                $k =$timestamp."\r\n".$name.":   ".$a."\r\n\n".$r."\r\n";

                //we are updating the remarks into the "comment" column
                $re=DB::table('prodleads')->where('ProdId', $productid)->update(['comments'=>$k]);

                if($r != $a )
                {
                    $proc[] = "Comment";
                    $prod[] = $a;
                }
            }

            // dd($productid);

            // dd($orderstatus);
            // dd($request->OrderStatus);
            if($request->OrderStatus=="Ready to ship" || $request->OrderStatusrent=="Ready to ship")
            {
                // dd($request->OrderStatus,$request->OrderStatusrent);
                //when field officer assigns to field executive (product delivery guy)

                // dd($request->assignto);
                if($request->assignto!=NULL)
                {
                    $field_executive_db_id = DB::table('prodleads')->where('Prodid',$productid)->value('empid');
                    // dd($field_executive_db);
                    $field_executive_db_name = DB::table('employees')->where('id',$field_executive_db_id)->value('FirstName');

                    $e=DB::table('employees')->where('FirstName',$request->assignto)->value('id');
                    $pid=DB::table('prodleads')->where('ProdId',$productid)->value('id');
                    $update = prodlead::find($pid);
                    $update->empid=$e;
                    $update->save();



                    // dd($field_executive_db_name,$request->ass);
                    // dd($request->OrderStatus,$request->OrderStatusrent, $request->assignto);

                    // //SMS when the Order is assigned by Field Officer to Field executive - Message going to Field executive -- starts here
                    //
                    if($field_executive_db_name!=$request->assignto)
                    {
                        $fe_contact = DB::table('employees')->where('FirstName',$request->assignto)->value('MobileNumber');

                        $fe_altcontact = DB::table('employees')->where('FirstName',$request->assignto)->value('AlternateNumber');

                        // dd($fe_contact,$fe_altcontact);
                        if($fe_contact!=NULL)
                        {
                            $message="Dear ".$request->assignto.", Order no: ".$orderid." is Ready to Ship. Thank you.";


                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fe_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);



                        }

                        // // SMS for Field executive alternate contact number

                        if($fe_altcontact!=NULL)
                        {
                            $message="Dear ".$request->assignto.", Order no: ".$orderid." is Ready to Ship. Thank you.";

                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fe_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                        }


                        //SMS when the Order is assigned by Field Officer to Field executive - Message going to Field executive -- ends here

                    }
                }
            }


            if(empty($proc))
            {
                $prof = " ";
                $prog = " ";
                $prof = trim($prof);
                $prog = trim($prog);
            }
            else
            {
                $prof = implode(": ", $proc).":";
                $prog = implode(",", $prod).", ";
            }

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            $leadid = DB::table('products')->where('id',$productid)->value('leadid');

            if((trim($prof)!==""))
            {
                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "Product Fields Updated";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->lead_id = $leadid;
                $activity->field_updated = $prof;
                $activity->value_updated = $prog;

                $activity->save();
            }

            // dd($ostatus, $request->OrderStatus, $ostatusr, $request->OrderStatus);


            //SMS when status is changed to "Delivered"
            if($request->OrderStatus=="Delivered")
            {
                //checking if the previous status was something other than "Delivered", then only send an sms
                if(($ostatus!=$request->OrderStatus && $ostatus!=null) || ($ostatusr!=$request->OrderStatus && $ostatusr!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Delivered" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been delivered.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been delivered.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been delivered.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been delivered.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Delivered" -- ends here


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Delivered" -- starts here

                    //checking whether the lead id was existing or created newly
                    $exist_lead = DB::table('services')->where('Leadid', $leadid)->value('Leadid');

                    // extracting the name of the vertical head that lead has been assigned to
                    $assigned_to_vname = DB::table('services')->where('Leadid', $leadid)->value('AssignedTo');

                    //extracting the status of that lead
                    $service_status = DB::table('services')->where('Leadid', $leadid)->value('ServiceStatus');

                    //extracting the employee id of the coordinator to whom the lead has been assigned if at all
                    $assigned_to_cid = DB::table('verticalcoordinations')->where('leadid', $leadid)->value('empid');


                    if($exist_lead!=NULL)
                    {
                        if($service_status=="Converted" || $service_status=="In Progress" || $service_status=="New")
                        {
                            //send SMS to Coordinators
                            if($assigned_to_cid!=NULL)
                            {
                                $assigned_to_cname = DB::table('employees')->where('id',$assigned_to_cid)->value('FirstName');

                                $coord_name = $assigned_to_cname;
                                $coord_mob = DB::table('employees')->where('FirstName',$coord_name)->value('MobileNumber');
                                $coord_altmob = DB::table('employees')->where('FirstName',$coord_name)->value('AlternateNumber');


                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for Coordinator alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }

                            //send SMS to Vertical Head
                            else
                            {
                                $vertical_name = $assigned_to_vname;
                                $v_mob = DB::table('employees')->where('FirstName',$vertical_name)->value('MobileNumber');
                                $v_altmob = DB::table('employees')->where('FirstName',$vertical_name)->value('AlternateNumber');

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for vertical alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Delivered" -- ends here

                    //Mail sent to Customer when the Order has been Delivered starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product5', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Delivered");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has been Delivered ends here

                    //Mail sent to Field Officer and Product Manager when the Order has been Delivered starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product6', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Delivered!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //finding the product type
                    $type = $request->type;

                    if($type="Sell")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Selling";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }
                    else if($type="Rent")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Rental";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($pm_email!=NULL && $active_state==0)
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product7', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Delivered!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has been Delivered ends here
                }
            }
            // dd($request->OrderStatus);
            //SMS when status is changed to "Cancelled"
            if($request->OrderStatus=="Cancelled")
            {
                //checking if the previous status was something other than "Cancelled", then only send an sms
                if(($ostatus!=$request->OrderStatus && $ostatus!=null) || ($ostatusr!=$request->OrderStatus && $ostatusr!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Cancelled" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no:".$orderid." has been cancelled.

                        Looking forward to serving you better in future.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no:".$orderid." has been cancelled.

                                Looking forward to serving you better in future.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no:".$orderid." has been cancelled.

                        Looking forward to serving you better in future.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no:".$orderid." has been cancelled.

                                Looking forward to serving you better in future.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Cancelled" -- ends here

                    //Mail sent to Customer when the Order has been Cancelled starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product8', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Cancelled");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has been Cancelled ends here

                    //Mail sent to Field Officer and Product Manager when the Order has been Cancelled starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product9', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Cancelled!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //finding the product type
                    $type = $request->type;

                    if($type="Sell")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Selling";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }
                    else if($type="Rent")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Rental";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($pm_email!=NULL && $active_state==0)
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product10', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Cancelled!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has been Cancelled ends here

                }

            }

            //SMS when status is changed to "Order Return"
            if($request->OrderStatus=="Order Return")
            {
                //checking if the previous status was something other than "Order Return", then only send an sms
                if(($ostatus!=$request->OrderStatus && $ostatus!=null) || ($ostatusr!=$request->OrderStatus && $ostatusr!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Order Return" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Order Return" -- ends here

                    //Mail sent to Customer when the Order has beenReturned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product11', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Returned Order Update");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has beenReturned ends here

                    //Mail sent to Field Officer and Product Manager when the Order has beenReturned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product12', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Order Returned!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //finding the product type
                    $type = $request->type;

                    if($type="Sell")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Selling";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }
                    else if($type="Rent")
                    {
                        $pm_desig = "Product Manager";
                        $pm_dept = "Product - Rental";

                        $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->where('Department2',$pm_dept)->value('FirstName');

                        $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    }

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($pm_email!=NULL && $active_state==0)
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product13', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Order Returned!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has beenReturned ends here
                }
            }

            //Mail when the order is returned to the origin place
            if($request->OrderStatus=="Received Order Return")
            {
                //checking if the previous status was something other than "Received Order Return", then only send an sms
                if(($ostatus!=$request->OrderStatus && $ostatus!=null) || ($ostatusr!=$request->OrderStatus && $ostatusr!=null))
                {

                    //Mail sent to Customer when the Order has beenReturned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    //fetching the name and email id of the field officer
                    // $fieldofficer_name = $request->assignofficer;

                    $fieldofficer_name = DB::table('products')->where('id',$productid)->value('AssignedTo');
                    // dd($fieldofficer_name);
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    // dd($fieldofficer_email);
                    $prod_data = ['orderid'=>$orderid,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];


                    //Mail sent to Field Officer when the Order has been Returned and has reached the place of origin starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product14', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Returned Order Update!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Field Officer when the Order has been Returned and reaches the place of Origin ends here
                }
            }

            //SMS when status is changed to "Out for Delivery"
            if($request->OrderStatus=="Out for Delivery")
            {
                //checking if the previous status was something other than "Out for Delivery", then only send an sms
                if(($ostatus!=$request->OrderStatus && $ostatus!=null) || ($ostatusr!=$request->OrderStatus && $ostatusr!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Out for Delivery" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Out for Delivery" -- ends here


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Out for Delivery" -- starts here

                    //checking whether the lead id was existing or created newly
                    $exist_lead = DB::table('services')->where('Leadid', $leadid)->value('Leadid');

                    // extracting the name of the vertical head that lead has been assigned to
                    $assigned_to_vname = DB::table('services')->where('Leadid', $leadid)->value('AssignedTo');

                    //extracting the status of that lead
                    $service_status = DB::table('services')->where('Leadid', $leadid)->value('ServiceStatus');

                    //extracting the employee id of the coordinator to whom the lead has been assigned if at all
                    $assigned_to_cid = DB::table('verticalcoordinations')->where('leadid', $leadid)->value('empid');


                    if($exist_lead!=NULL)
                    {
                        if($service_status=="Converted" || $service_status=="In Progress" || $service_status=="New")
                        {
                            //send SMS to Coordinators
                            if($assigned_to_cid!=NULL)
                            {
                                $assigned_to_cname = DB::table('employees')->where('id',$assigned_to_cid)->value('FirstName');

                                $coord_name = $assigned_to_cname;
                                $coord_mob = DB::table('employees')->where('FirstName',$coord_name)->value('MobileNumber');
                                $coord_altmob = DB::table('employees')->where('FirstName',$coord_name)->value('AlternateNumber');

                                $active_state = DB::table('sms_email_filter')->value('active_state');


                                if($coord_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for Coordinator alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }

                            //send SMS to Vertical Head
                            else
                            {
                                $vertical_name = $assigned_to_vname;
                                $v_mob = DB::table('employees')->where('FirstName',$vertical_name)->value('MobileNumber');
                                $v_altmob = DB::table('employees')->where('FirstName',$vertical_name)->value('AlternateNumber');

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for vertical alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Out for Delivery" -- ends here

                    // Mail going to customer when the product is "Out for Delivery" starts here
                    //fetching the details of the customer

                    $leadid = DB::table('products')->where('id',$productid)->value('leadid');

                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product4', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Shipped");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    // Mail going to customer when the product is "Out for Delivery" ends here
                }
            }
            session()->put('name',$name);
            return redirect('/product');
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }


    }

    //this function is for the scenario when the user changes that pharmacy details
    public function pharmacystatus(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

            $name=$request->loginname;

            //refer to the comments in the "productstatus" function above to understand what is happening here
            if($request->OrderStatus==NULL || $request->OrderStatus=="New")
            {
                $status="Processing";
            }
            else
            {
                $status=$request->OrderStatus;
            }

            //these hidden fields are coming from "product/adminpharmacydetails.blade.php"
            $type=$request->type;

            $orderid=$request->id;
            $pharmacyid=$request->pharmacyid;

            $req=$request->leadfor;


            $pharmacies = pharmacy::find($pharmacyid);

            $pstatus = DB::table('pharmacies')->where('id',$pharmacyid)->value('POrderStatus');

            $pharmacies->POrderStatus=$status;


            if($pstatus != $request->OrderStatus)
            {
                // Store the Changed field's "column name" and "value"
                $proc[] = "Order Status";
                $prod[] = $request->OrderStatus;
            }
            $pharmacies->save();


            $r=DB::table('prodleads')->where('PharmacyId',$pharmacyid)->value('comments');
            $a=$request->comment;


            if($a!=NULL)
            {
                $time=time();
                $timestamp=date('m/d/Y h:i:s a', time());

                $k =$timestamp."\r\n".$name.":   ".$a."\r\n\n".$r."\r\n";
                $re=DB::table('prodleads')->where('PharmacyId', $pharmacyid)->update(['comments'=>$k]);

                if($r != $a )
                {
                    $proc[] = "Comment";
                    $prod[] = $a;
                }
            }

            // check where the lead is assigned to filed officer or not

            if($request->assignofficer!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                $pharmacy->PAssignedTo=$request->assignofficer;

                $assigned = DB::table('pharmacies')->where('id',$pharmacyid)->value('PAssignedTo');

                // dd($assigned);
                if($assigned != $request->assignofficer)
                {
                    $proc[] = "Assigned To";
                    $prod[] = $request->assignofficer;
                }

                $pharmacy->save();

                if($assigned != $request->assignofficer)
                {
                    $proc[] = "Assigned To";
                    $prod[] = $request->assignofficer;

                    //SMS when the Order is assigned to someone - Message going to Field officer -- starts here

                    //     $fo_desig = "Field Officer";
                    //
                    //     $fo_name = DB::table('employees')->where('Designation2', $fo_desig)->value('FirstName');
                    //
                    //
                    //     $fo_contact = DB::table('employees')->where('FirstName',$fo_name)->value('MobileNumber');
                    //
                    //     $fo_altcontact = DB::table('employees')->where('FirstName',$fo_name)->value('AlternateNumber');
                    //
                    //     if($fo_contact!=NULL)
                    //     {
                    //         $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //
                    //
                    //         //for extracting the date for generating reports
                    //         $time = time();
                    //         $date = date('m/d/Y',$time);
                    //
                    //         //actual sending of the message
                    //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                    //         $ch =curl_init($url);
                    //
                    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page =curl_exec($ch);
                    //         // dd(curl_exec($ch));
                    //         //expecting the report 'OK' here
                    //         $report = substr($curl_scraped_page,0,2);
                    //
                    //         //extracting the job no of the message sent if it is at all
                    //         $jobno = trim(substr($curl_scraped_page,3));
                    //
                    //
                    //         curl_close($ch);
                    //
                    //         //url for retrieving the report
                    //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                    //         $ch1 =curl_init($report1);
                    //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page1 =curl_exec($ch1);
                    //         // $report = substr($curl_scraped_page1,0,2);
                    //         curl_close($ch1);
                    //
                    //         //retrieving the invalid mobile no text part out of the report
                    //         $invalidmno = substr($curl_scraped_page1,0,11);
                    //
                    //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                    //         // if($invalidmno == "Invalid Mno")
                    //         // {
                    //         //
                    //         //     $checkbox=$request->addproduct;
                    //         //
                    //         //     session()->put('name',$who);
                    //         //     Session::flash('warning','The Mobile number is invalid');
                    //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                    //         // }
                    //
                    //         $i=0;
                    //
                    //         //sending a message again if not sent first time
                    //
                    //         //while the report generated doesn't have the message OK in it
                    //         while($report!="OK" || $i<=1)
                    //         {
                    //             //even if i<1 trigger this only in cases where report is not = OK
                    //             if($report!="OK")
                    //             {
                    //                 $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //                 $time = time();
                    //                 $date = date('d/m/Y',$time);
                    //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    //                 $ch =curl_init($url);
                    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //                 $curl_scraped_page =curl_exec($ch);
                    //                 $report = substr($curl_scraped_page,0,2);
                    //                 curl_close($ch);
                    //             }
                    //             $i++;
                    //         }
                    //     }
                    //
                    //     // SMS for Field officer alternate contact number
                    //
                    //     if($fo_altcontact!=NULL)
                    //     {
                    //         $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //
                    //
                    //         //for extracting the date for generating reports
                    //         $time = time();
                    //         $date = date('m/d/Y',$time);
                    //
                    //         //actual sending of the message
                    //         $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                    //         $ch =curl_init($url);
                    //
                    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page =curl_exec($ch);
                    //         // dd(curl_exec($ch));
                    //         //expecting the report 'OK' here
                    //         $report = substr($curl_scraped_page,0,2);
                    //
                    //         //extracting the job no of the message sent if it is at all
                    //         $jobno = trim(substr($curl_scraped_page,3));
                    //
                    //
                    //         curl_close($ch);
                    //
                    //         //url for retrieving the report
                    //         $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                    //         $ch1 =curl_init($report1);
                    //         curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    //         $curl_scraped_page1 =curl_exec($ch1);
                    //         // $report = substr($curl_scraped_page1,0,2);
                    //         curl_close($ch1);
                    //
                    //         //retrieving the invalid mobile no text part out of the report
                    //         $invalidmno = substr($curl_scraped_page1,0,11);
                    //
                    //         // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                    //         // if($invalidmno == "Invalid Mno")
                    //         // {
                    //         //
                    //         //     $checkbox=$request->addproduct;
                    //         //
                    //         //     session()->put('name',$who);
                    //         //     Session::flash('warning','The Mobile number is invalid');
                    //         //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                    //         // }
                    //
                    //         $i=0;
                    //
                    //         //sending a message again if not sent first time
                    //
                    //         //while the report generated doesn't have the message OK in it
                    //         while($report!="OK" || $i<=1)
                    //         {
                    //             //even if i<1 trigger this only in cases where report is not = OK
                    //             if($report!="OK")
                    //             {
                    //                 $message="Dear ".$fo_name.",Thank you for contacting Health Heal. Your ref id is ".$orderid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    //                 $time = time();
                    //                 $date = date('d/m/Y',$time);
                    //                 $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fo_contact)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    //                 $ch =curl_init($url);
                    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //                 $curl_scraped_page =curl_exec($ch);
                    //                 $report = substr($curl_scraped_page,0,2);
                    //                 curl_close($ch);
                    //             }
                    //             $i++;
                    //         }
                    //     }
                    // }

                    //SMS when the Order is assigned to someone - Message going to Field officer -- ends here

                    //Mail going to Field Officer when order is assigned to them and the status changes to "Processing" starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //who created the product
                    $product_creator_name = DB::table('products')->where('leadid',$leadid)->value('Requestcreatedby');


                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $customer_address1 = DB::table('addresses')->where('id',$leadid)->value('Address1');
                    $customer_shippingaddress1 = DB::table('addresses')->where('id',$leadid)->value('SAddress1');

                    if($customer_shippingaddress1!=NULL)
                    {
                        $customer_shippingaddress2 = DB::table('addresses')->where('leadid',$leadid)->value('SAddress2');
                        $customer_shipping_city = DB::table('addresses')->where('leadid',$leadid)->value('SCity');
                        $customer_shipping_district = DB::table('addresses')->where('leadid',$leadid)->value('SDistrict');
                        $customer_shipping_state = DB::table('addresses')->where('leadid',$leadid)->value('SState');
                        $customer_shipping_pincode = DB::table('addresses')->where('leadid',$leadid)->value('SPinCode');

                        $customer_address = $customer_shippingaddress1."".$customer_shippingaddress2." ".$customer_shipping_city." ".$customer_shipping_district." ".$customer_shipping_state." ".$customer_shipping_pincode;
                    }
                    else
                    {
                        $customer_address2 = DB::table('addresses')->where('leadid',$leadid)->value('Address2');
                        $customer_city = DB::table('addresses')->where('leadid',$leadid)->value('City');
                        $customer_district = DB::table('addresses')->where('leadid',$leadid)->value('District');
                        $customer_state = DB::table('addresses')->where('leadid',$leadid)->value('State');
                        $customer_pincode = DB::table('addresses')->where('leadid',$leadid)->value('PinCode');

                        $customer_address = $customer_address1." ".$customer_address2." ".$customer_city." ".$customer_district." ".$customer_state." ".$customer_pincode;

                    }

                    // dd($customer_address);

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'cust_addr'=>$customer_address,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name,'prod_creator'=>$product_creator_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product3', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id ".$prod_data['orderid']." In Processing!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail going to Field Officer when order is assigned to them and the status changes to "Processing" ends here


                }
            }
            // dd($request->assignofficer);

            if($request->OrderStatus=="Ready to ship")
            {
                if($request->assignto!=NULL)
                {
                    $field_executive_db_id = DB::table('prodleads')->where('PharmacyId',$pharmacyid)->value('empid');
                    // dd($field_executive_db);
                    $field_executive_db_name = DB::table('employees')->where('id',$field_executive_db_id)->value('FirstName');

                    $e=DB::table('employees')->where('FirstName',$request->assignto)->value('id');
                    $pid=DB::table('prodleads')->where('PharmacyId',$pharmacyid)->value('id');
                    $update = prodlead::find($pid);
                    $update->empid=$e;


                    $update->save();

                    //SMS when the Order is assigned by Field Officer to Field executive - Message going to Field executive -- starts here

                    if($field_executive_db_name!=$request->assignto)
                    {
                        $fe_contact = DB::table('employees')->where('FirstName',$request->assignto)->value('MobileNumber');

                        $fe_altcontact = DB::table('employees')->where('FirstName',$request->assignto)->value('AlternateNumber');

                        // dd($fe_contact,$fe_altcontact);
                        if($fe_contact!=NULL)
                        {
                            $message="Dear ".$request->assignto.", Order no: ".$orderid." is Ready to Ship. Thank you.";


                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fe_contact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);



                        }

                        // // SMS for Field executive alternate contact number

                        if($fe_altcontact!=NULL)
                        {
                            $message="Dear ".$request->assignto.", Order no: ".$orderid." is Ready to Ship. Thank you.";

                            //for extracting the date for generating reports
                            $time = time();
                            $date = date('m/d/Y',$time);

                            //actual sending of the message
                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($fe_altcontact)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                            $ch =curl_init($url);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_scraped_page =curl_exec($ch);
                            // dd(curl_exec($ch));
                            //expecting the report 'OK' here
                            $report = substr($curl_scraped_page,0,2);

                            //extracting the job no of the message sent if it is at all
                            $jobno = trim(substr($curl_scraped_page,3));


                            curl_close($ch);

                        }


                        //SMS when the Order is assigned by Field Officer to Field executive - Message going to Field executive -- ends here

                    }
                }
            }

            if($request->receipt!=NULL || $request->cheque!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PReceipt=$request->receipt;
                $pharmacy->PCheque=$request->cheque;

                $receipt = DB::table('pharmacies')->where('id',$pharmacyid)->value('PReceipt');
                $cheque = DB::table('pharmacies')->where('id',$pharmacyid)->value('PCheque');


                if($receipt != $request->receipt)
                {
                    $proc[] = "Receipt";
                    $prod[] = $request->receipt;
                }

                if($cheque != $request->cheque)
                {
                    $proc[] = "Cheque";
                    $prod[] = $request->cheque;
                }

                $pharmacy->save();
            }

            if($request->modeofpayment!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($product);

                $pharmacy->PModeofpayment=$request->modeofpayment;

                $mode = DB::table('pharmacies')->where('id',$pharmacyid)->value('PModeofPayment');

                if($mode != $request->modeofpayment)
                {
                    $proc[] = "Mode of Payment";
                    $prod[] = $request->modeofpayment;
                }

                $pharmacy->save();
            }




            if($request->cashstatus!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PCashStatus=$request->cashstatus;

                $cash = DB::table('pharmacies')->where('id',$pharmacyid)->value('PCashStatus');

                if($cash != $request->cashstatus)
                {
                    $proc[] = "Cash Status";
                    $prod[] = $request->cashstatus;
                }

                $pharmacy->save();
            }

            if($request->apaid!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PAmountPaid=$request->apaid;

                $amp = DB::table('pharmacies')->where('id',$pharmacyid)->value('PAmountPaid');

                if($amp != $request->apaid)
                {
                    $proc[] = "Amount Paid";
                    $prod[] = $request->apaid;
                }

                $pharmacy->save();
            }
            if($request->discount!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PDiscount=$request->discount;

                $dis = DB::table('pharmacies')->where('id',$pharmacyid)->value('PDiscount');

                if($dis != $request->discount)
                {
                    $proc[] = "Discount";
                    $prod[] = $request->discount;
                }

                $pharmacy->save();

            }

            if($request->total!=NULL)
            {
                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PFinalAmount=$request->total;

                $fa = DB::table('pharmacies')->where('id',$pharmacyid)->value('PFinalAmount');

                if($fa != $request->total)
                {
                    $proc[] = "FinalAmount";
                    $prod[] = $request->total;
                }

                $pharmacy->save();
            }

            // dd($request->total);
            if($request->discount!=NULL && $request->total!=NULL)
            {
                $sum=0;
                $d=$request->discount;
                $t=$request->total;

                $sum=(1- ($d/100))*$t;
                // dd($sum);

                $pharmacy = pharmacy::find($pharmacyid);
                // dd($pharmacy);

                $pharmacy->PPostDiscountPrice=$sum;

                $pdp = DB::table('pharmacies')->where('id',$pharmacyid)->value('PPostDiscountPrice');

                if($pdp != $sum)
                {
                    $proc[] = "PostDiscountPrice";
                    $prod[] = $sum;
                }

                $pharmacy->save();
            }

            // dd($proc);
            if(empty($proc))
            {
                $prof = " ";
                $prog = " ";
                $prof = trim($prof);
                $prog = trim($prog);
            }
            else
            {
                $prof = implode(": ", $proc).":";
                $prog = implode(",", $prod).", ";
            }

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');
            if((trim($prof)!==""))
            {
                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "Product Fields Updated";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->lead_id = $leadid;
                $activity->field_updated = $prof;
                $activity->value_updated = $prog;

                $activity->save();
            }

            //SMS when status is changed to "Delivered"
            if($request->OrderStatus=="Delivered")
            {
                //checking if the previous status was something other than "Delivered", then only send an sms
                if(($pstatus!=$request->OrderStatus && $pstatus!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Delivered" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been delivered.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been delivered.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been delivered.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been delivered.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Delivered" -- ends here


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Delivered" -- starts here

                    //checking whether the lead id was existing or created newly
                    $exist_lead = DB::table('services')->where('Leadid', $leadid)->value('Leadid');

                    // extracting the name of the vertical head that lead has been assigned to
                    $assigned_to_vname = DB::table('services')->where('Leadid', $leadid)->value('AssignedTo');

                    //extracting the status of that lead
                    $service_status = DB::table('services')->where('Leadid', $leadid)->value('ServiceStatus');

                    //extracting the employee id of the coordinator to whom the lead has been assigned if at all
                    $assigned_to_cid = DB::table('verticalcoordinations')->where('leadid', $leadid)->value('empid');


                    if($exist_lead!=NULL)
                    {
                        if($service_status=="Converted" || $service_status=="In Progress" || $service_status=="New")
                        {
                            //send SMS to Coordinators
                            if($assigned_to_cid!=NULL)
                            {
                                $assigned_to_cname = DB::table('employees')->where('id',$assigned_to_cid)->value('FirstName');

                                $coord_name = $assigned_to_cname;
                                $coord_mob = DB::table('employees')->where('FirstName',$coord_name)->value('MobileNumber');
                                $coord_altmob = DB::table('employees')->where('FirstName',$coord_name)->value('AlternateNumber');


                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for Coordinator alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }

                            //send SMS to Vertical Head
                            else
                            {
                                $vertical_name = $assigned_to_vname;
                                $v_mob = DB::table('employees')->where('FirstName',$vertical_name)->value('MobileNumber');
                                $v_altmob = DB::table('employees')->where('FirstName',$vertical_name)->value('AlternateNumber');

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for vertical alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been delivered.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been delivered.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Delivered" -- ends here

                    //Mail sent to Customer when the Order has been Delivered starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product5', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Delivered");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has been Delivered ends here

                    //Mail sent to Field Officer and Product Manager when the Order has been Delivered starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product6', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Delivered!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //finding the product type

                    $pm_desig = "Pharmacy Manager";

                    $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->value('FirstName');

                    $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    // dd($pm_name);
                    if($pm_email!=NULL && $active_state==0)
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product7', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Delivered!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has been Delivered ends here


                }

            }

            //SMS when status is changed to "Cancelled"
            if($request->OrderStatus=="Cancelled")
            {
                //checking if the previous status was something other than "Cancelled", then only send an sms
                if(($pstatus!=$request->OrderStatus && $pstatus!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Cancelled" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no:".$orderid." has been cancelled.

                        Looking forward to serving you better in future.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no:".$orderid." has been cancelled.

                                Looking forward to serving you better in future.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no:".$orderid." has been cancelled.

                        Looking forward to serving you better in future.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no:".$orderid." has been cancelled.

                                Looking forward to serving you better in future.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Cancelled" -- ends here

                    //Mail sent to Customer when the Order has been Cancelled starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product8', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Cancelled");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has been Cancelled ends here

                    //Mail sent to Field Officer and Product Manager when the Order has been Cancelled starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product9', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Cancelled!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }


                    $pm_desig = "Pharmacy Manager";

                    $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->value('FirstName');

                    $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($pm_email!=NULL && $active_state==0 )
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product10', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Cancelled!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has been Cancelled ends here



                }

            }

            //SMS when status is changed to "Order Return"
            if($request->OrderStatus=="Order Return")
            {
                //checking if the previous status was something other than "Order Return", then only send an sms
                if(($pstatus!=$request->OrderStatus && $pstatus!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Order Return" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number
                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been returned back due to cancellation or non-availability.

                                Contact: 8880004422
                                Visit: www.healthheal.in";

                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Order Return" -- ends here

                    //Mail sent to Customer when the Order has been Returned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product11', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Returned Order Update");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Customer when the Order has beenReturned ends here

                    //Mail sent to Field Officer and Product Manager when the Order has beenReturned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');

                    //fetching the details of the customer
                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product12', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Order Returned!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }



                    $pm_desig = "Pharmacy Manager";


                    $pm_name = DB::table('employees')->where('Designation2', $pm_desig)->value('FirstName');

                    $pm_email = DB::table('employees')->where('FirstName',$pm_name)->value('Email_id');

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($pm_email!=NULL && $active_state==0)
                    {
                        // dd($pm_email);
                        $prod_data = ['orderid'=>$orderid,'pm_name'=>$pm_name,'pm_email'=>$pm_email];

                        Mail::send('mail_for_product13', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['pm_email'], $prod_data['pm_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Order Returned!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }
                    //Mail sent to Field Officer and Product Manager when the Order has been Returned ends here


                }

            }

            //SMS when status is changed to "Out for Delivery"
            if($request->OrderStatus=="Out for Delivery")
            {
                //checking if the previous status was something other than "Out for Delivery", then only send an sms
                if(($pstatus!=$request->OrderStatus && $pstatus!=null))
                {
                    //SMS sent to customer when the Order status is changed to "Out for Delivery" -- starts here

                    $cust_name = DB::table('leads')->where('id',$leadid)->value('fName');
                    $cust_mob = DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $cust_altmob = DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                    // dd($cust_mob);

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_mob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }

                    // SMS for customer alternate contact number

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($cust_altmob!=NULL && $active_state==0)
                    {
                        $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                        Contact: 8880004422
                        Visit: www.healthheal.in";


                        //for extracting the date for generating reports
                        $time = time();
                        $date = date('m/d/Y',$time);

                        //actual sending of the message
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        // dd(curl_exec($ch));
                        //expecting the report 'OK' here
                        $report = substr($curl_scraped_page,0,2);

                        //extracting the job no of the message sent if it is at all
                        $jobno = trim(substr($curl_scraped_page,3));


                        curl_close($ch);

                        //url for retrieving the report
                        $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                        $ch1 =curl_init($report1);
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page1 =curl_exec($ch1);
                        // $report = substr($curl_scraped_page1,0,2);
                        curl_close($ch1);

                        //retrieving the invalid mobile no text part out of the report
                        $invalidmno = substr($curl_scraped_page1,0,11);

                        // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                        // if($invalidmno == "Invalid Mno")
                        // {
                        //
                        //     $checkbox=$request->addproduct;
                        //
                        //     session()->put('name',$who);
                        //     Session::flash('warning','The Mobile number is invalid');
                        //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                        // }

                        $i=0;

                        //sending a message again if not sent first time

                        //while the report generated doesn't have the message OK in it
                        while($report!="OK" || $i<=1)
                        {
                            //even if i<1 trigger this only in cases where report is not = OK
                            if($report!="OK")
                            {
                                $message="Your Order no: ".$orderid." has been shipped and will be delivered soon.

                                Contact: 8880004422
                                Visit: www.healthheal.in";
                                $time = time();
                                $date = date('d/m/Y',$time);
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($cust_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }
                    }



                    //SMS sent to customer when the Order status is changed to "Out for Delivery" -- ends here


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Out for Delivery" -- starts here

                    //checking whether the lead id was existing or created newly
                    $exist_lead = DB::table('services')->where('Leadid', $leadid)->value('Leadid');

                    // extracting the name of the vertical head that lead has been assigned to
                    $assigned_to_vname = DB::table('services')->where('Leadid', $leadid)->value('AssignedTo');

                    //extracting the status of that lead
                    $service_status = DB::table('services')->where('Leadid', $leadid)->value('ServiceStatus');

                    //extracting the employee id of the coordinator to whom the lead has been assigned if at all
                    $assigned_to_cid = DB::table('verticalcoordinations')->where('leadid', $leadid)->value('empid');


                    if($exist_lead!=NULL)
                    {
                        if($service_status=="Converted" || $service_status=="In Progress" || $service_status=="New")
                        {
                            //send SMS to Coordinators
                            if($assigned_to_cid!=NULL)
                            {
                                $assigned_to_cname = DB::table('employees')->where('id',$assigned_to_cid)->value('FirstName');

                                $coord_name = $assigned_to_cname;
                                $coord_mob = DB::table('employees')->where('FirstName',$coord_name)->value('MobileNumber');
                                $coord_altmob = DB::table('employees')->where('FirstName',$coord_name)->value('AlternateNumber');


                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for Coordinator alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($coord_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$coord_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$coord_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coord_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }

                            //send SMS to Vertical Head
                            else
                            {
                                $vertical_name = $assigned_to_vname;
                                $v_mob = DB::table('employees')->where('FirstName',$vertical_name)->value('MobileNumber');
                                $v_altmob = DB::table('employees')->where('FirstName',$vertical_name)->value('AlternateNumber');

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_mob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";


                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                // SMS for vertical alternate contact number

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($v_altmob!=NULL && $active_state==0)
                                {
                                    $message=
                                    "Dear ".$vertical_name.",
                                    The Order no: ".$orderid." has been shipped.

                                    Thank you.";

                                    //for extracting the date for generating reports
                                    $time = time();
                                    $date = date('m/d/Y',$time);

                                    //actual sending of the message
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);

                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    // dd(curl_exec($ch));
                                    //expecting the report 'OK' here
                                    $report = substr($curl_scraped_page,0,2);

                                    //extracting the job no of the message sent if it is at all
                                    $jobno = trim(substr($curl_scraped_page,3));


                                    curl_close($ch);

                                    //url for retrieving the report
                                    $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
                                    $ch1 =curl_init($report1);
                                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page1 =curl_exec($ch1);
                                    // $report = substr($curl_scraped_page1,0,2);
                                    curl_close($ch1);

                                    //retrieving the invalid mobile no text part out of the report
                                    $invalidmno = substr($curl_scraped_page1,0,11);

                                    // //if the mobile number is invalid , redirect it to cc.edit page with a flash message
                                    // if($invalidmno == "Invalid Mno")
                                    // {
                                    //
                                    //     $checkbox=$request->addproduct;
                                    //
                                    //     session()->put('name',$who);
                                    //     Session::flash('warning','The Mobile number is invalid');
                                    //     return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                                    // }

                                    $i=0;

                                    //sending a message again if not sent first time

                                    //while the report generated doesn't have the message OK in it
                                    while($report!="OK" || $i<=1)
                                    {
                                        //even if i<1 trigger this only in cases where report is not = OK
                                        if($report!="OK")
                                        {
                                            $message=
                                            "Dear ".$vertical_name.",
                                            The Order no: ".$orderid." has been shipped.

                                            Thank you.";

                                            $time = time();
                                            $date = date('d/m/Y',$time);
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($v_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);
                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }


                    //SMS sent to the Coordinator/Vertical Head when the Order status is changed to "Out for Delivery" -- ends here

                    // Mail going to customer when the product is "Out for Delivery" starts here
                    //fetching the details of the customer

                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    $customer_name= DB::table('leads')->where('id',$leadid)->value('fName');
                    $customer_contact= DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                    $customer_email= DB::table('leads')->where('id',$leadid)->value('EmailId');

                    $prod_data = ['orderid'=>$orderid, 'cust_name'=>$customer_name,'cust_mob'=>$customer_contact,'cust_email'=>$customer_email];

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($customer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product4', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['cust_email'], $prod_data['cust_name'] )->subject
                            ("[Happy to Care] Health Heal Order Shipped");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    // Mail going to customer when the product is "Out for Delivery" ends here

                }
            }

            //Mail when the order is returned to the origin place
            if($request->OrderStatus=="Received Order Return")
            {
                //checking if the previous status was something other than "Received Order Return", then only send an sms
                if(($pstatus!=$request->OrderStatus && $pstatus!=null))
                {

                    //Mail sent to Customer when the Order has beenReturned starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    //fetching the name and email id of the field officer
                    $fieldofficer_name = $request->assignofficer;
                    $fieldofficer_email = DB::table('employees')->where('FirstName',$fieldofficer_name)->value('Email_id');


                    $prod_data = ['orderid'=>$orderid,'fo_email'=>$fieldofficer_email,'fo_name'=>$fieldofficer_name];


                    //Mail sent to Field Officer when the Order has been Returned and has reached the place of origin starts here

                    //fetching the corresponding leadid for that product
                    $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($fieldofficer_email!=NULL && $active_state==0)
                    {
                        Mail::send('mail_for_product14', $prod_data, function($message)use($prod_data) {
                            $message->to($prod_data['fo_email'], $prod_data['fo_name'] )->subject
                            ("Order Id [".$prod_data['orderid']."] Returned Order Update!!!");
                            $message->from('care@healthheal.in','Healthheal');
                        });
                    }

                    //Mail sent to Field Officer when the Order has been Returned and reaches the place of Origin ends here
                }
            }


            session()->put('name',$name);
            return redirect('/product');


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }

    public function fieldofficerview(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

            $type=$_GET['type'];

            $name=$_GET['name'];

            $orderid=$_GET['id'];

            $underid=DB::table('employees')->where('FirstName',$name)->value('id');

            $executive=DB::table('employees')->where('Designation3','Field Executive')->where('under',$underid)->orwhere('under1',$underid)->get();

            // dd($executive);


            $desig=DB::table('employees')->where('FirstName',$name)->value('Designation3');


            if($type=="Product")
            {

                $d=DB::table('prodleads')->where('orderid',$orderid)->value('ProdId');


                $productid=$d;

                $id=DB::table('products')->where('id',$productid)->value('leadid');
                $orderid=DB::table('prodleads')->where('ProdId',$productid)->value('orderid');


                $employeeid=DB::table('prodleads')->where('ProdId',$productid)->value('empid');

                $assign=DB::table('employees')->where('id',$employeeid)->value('FirstName');


                $comments=DB::table('prodleads')->where('ProdId',$productid)->value('comments');


                $leaddetails=DB::table('leads')
                ->select('addresses.*','leads.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->where('leads.id', $id)
                ->get();

                // dd($leaddetails);
                /* code for product data by jatin starts here */
                //take out the details from the product table for that particular lead id
                $detail=DB::table('products')->where('id',$productid)->get();
                $detail1 = json_decode($detail,true);
                // dd($detail1);

                //retrieving id for product

                // Converting all products from json to array
                $ProductName = $detail1[0]['ProductName'];
                // dd($Mednames);
                $k =explode("\r\n",$ProductName);



                $SKUid =$detail1[0]['SKUid'];
                $k0 = explode("\r\n",$SKUid);

                $DemoRequired =$detail1[0]['DemoRequired'];
                $k1 = $DemoRequired;


                $AvailabilityStatus =$detail1[0]['AvailabilityStatus'];
                $k2 =explode("\r\n",$AvailabilityStatus);


                $AvailabilityAddress =$detail1[0]['AvailabilityAddress'];
                $k3 =explode("\r\n",$AvailabilityAddress);

                $SellingPrice =$detail1[0]['SellingPrice'];
                $k4 =explode("\r\n",$SellingPrice);


                $Type =$detail1[0]['Type'];
                $k5 =$Type;


                $OrderStatus =$detail1[0]['OrderStatus'];
                $k6 =$OrderStatus;


                $Quantity =$detail1[0]['Quantity'];
                $k7 =explode("\r\n",$Quantity);

                $ModeofPaymentrent =$detail1[0]['ModeofPaymentrent'];
                $k8 =$ModeofPaymentrent;

                $OrderStatusrent =$detail1[0]['OrderStatusrent'];
                $k9 =$OrderStatusrent;

                $ModeofPayment =$detail1[0]['ModeofPayment'];
                $k10 =$ModeofPayment;

                $AdvanceAmt =$detail1[0]['AdvanceAmt'];
                $k11 =explode("\r\n",$AdvanceAmt);

                $StartDate =$detail1[0]['StartDate'];
                $k12 =explode("\r\n",$StartDate);

                $EndDate =$detail1[0]['EndDate'];
                $k13 =explode("\r\n",$EndDate);

                $OverdueAmt =$detail1[0]['OverdueAmt'];
                $k14 =explode("\r\n",$OverdueAmt);

                $RentalPrice =$detail1[0]['RentalPrice'];
                $k15 =explode("\r\n",$RentalPrice);

                $Receipt =$detail1[0]['Receipt'];


                $Cheque =$detail1[0]['Cheque'];


                $cashstatus =$detail1[0]['CashStatus'];

                $discount =$detail1[0]['Discount'];

                $apaid =$detail1[0]['AmountPaid'];

                $pdiscount =$detail1[0]['PostDiscountPrice'];

                $FinalAmount =$detail1[0]['FinalAmount'];


                $deliverydate =$detail1[0]['deliverydate'];




                $cnt2 = count($k);

                // dd($cnt4);
                $cnt1 = 0;


                $sum=0;



                session()->put('name',$name);
                return view('product/productdetails',compact('leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','k0','k8','k9','k10','k11','k12','k13','k14','k15','cnt1','cnt2','sum','orderid','productid','executive','assign','comments','desig','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','FinalAmount','deliverydate'));







            }
            else
            {
                $e=DB::table('prodleads')->where('orderid',$orderid)->count();

                if($e==1)
                {
                    $d=DB::table('prodleads')->where('orderid',$orderid)->value('PharmacyId');
                }
                else
                {

                    $d=DB::table('prodleads')->where('orderid',$orderid)->select('PharmacyId')->get(1);
                    $d = json_decode($d,true);
                    $d=$d[1]['PharmacyId'];
                }



                $leadid=$d;

                $id=DB::table('pharmacies')->where('id',$leadid)->value('leadid');
                $orderid=DB::table('prodleads')->where('PharmacyId',$leadid)->value('orderid');

                $employeeid=DB::table('prodleads')->where('PharmacyId',$leadid)->value('empid');

                $assign=DB::table('employees')->where('id',$employeeid)->value('FirstName');

                $comments=DB::table('prodleads')->where('PharmacyId',$leadid)->value('comments');


                $leaddetails=DB::table('leads')
                ->select('addresses.*','leads.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->where('leads.id', $id)
                ->get();

                /* code for pharmacy data by jatin starts here */
                //take out the details from the pharmacy table for that particular lead id
                $detail=DB::table('pharmacies')->where('id',$leadid)->get();
                $detail1 = json_decode($detail,true);
                // dd($detail1);

                //retrieving id for pharmacy

                // Converting all Mednames from json to array
                $Mednames = $detail1[0]['MedName'];
                // dd($Mednames);
                $k =explode("\r\n",$Mednames);


                $Strength =$detail1[0]  ['Strength'];
                $k1 = explode("\r\n",$Strength);


                $PQuantity =$detail1[0]['PQuantity'];
                $k2 =explode("\r\n",$PQuantity);

                $QuantityType =$detail1[0]['QuantityType'];
                $QuantityType =explode("\r\n",$QuantityType);

                $MedType =$detail1[0]['MedType'];
                $k3 =explode("\r\n",$MedType);

                $Price =$detail1[0]['Price'];
                $k4 =explode("\r\n",$Price);


                $PAvailabilityStatus =$detail1[0]['PAvailabilityStatus'];
                $k5 =explode("\r\n",$PAvailabilityStatus);


                $POrderStatus =$detail1[0]['POrderStatus'];
                $k6 =$POrderStatus;


                $PModeofpayment =$detail1[0]['PModeofpayment'];
                $k7 =$PModeofpayment;

                $Receipt =$detail1[0]['PReceipt'];


                $Cheque =$detail1[0]['PCheque'];

                $cashstatus =$detail1[0]['PCashStatus'];

                $discount =$detail1[0]['PDiscount'];

                $apaid =$detail1[0]['PAmountPaid'];

                $pdiscount =$detail1[0]['PPostDiscountPrice'];

                $PFinalAmount =$detail1[0]['PFinalAmount'];

                $Pdeliverydate =$detail1[0]['Pdeliverydate'];




                // $Quantities =$detail1[0]['PQuantity'];
                // $k3 = explode("\r\n",$Quantities);

                // $Quantities =$detail1[0]['PQuantity'];
                // $k4 = explode("\r\n",$Quantities);
                // dd($k1);
                // // $posd = NULL;
                //  $d = "Jatin\r\nSharma";
                // $posd = strpos($d,"\r\n");
                // dd($posd);

                $cnt2 = count($k);

                // dd($cnt4);
                $cnt1 = 0;


                $sum=0;


                // int $i=0;
                // while($cnt1--)
                // {
                // $Med1 = $k1[$cnt1-1];

                //  }
                // // for(int $i=0;$i<$cnt1;$i++)
                // // {
                // // $Med = implode("\r\n", $Mednames);

                // while
                // $Medpos = strpos($Mednames,"\r\n");
                // $FirstMed = substr($Mednames, 0,$Medpos);
                // dd($FirstMed);
                // $SecondMed = substr($Mednames, $Medpos+2);
                // dd($SecondMed);
                // }

                // for(int $i=0;$i<$cnt1;$i++)
                // {

                // }

                // }
                session()->put('name',$name);
                return view('product/pharmacydetails',compact('leadid','leaddetails','name','detail','k','k1','k2','k3','k4','k5','k6','k7','cnt1','cnt2','sum','orderid','id','executive','assign','comments','desig','Receipt','Cheque','cashstatus','discount','apaid','pdiscount','PFinalAmount','Pdeliverydate','QuantityType'));


            }

        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }




    }

    public function productname(Request $request)
    {
        $productname =$request->productname;
        $sku=DB::table('productdetails')->where('Name',$productname)->value('SKU');
        return $sku;
    }





    // produt filter for admin and management

    public function adminfilter(Request $request)
    {
        // dd("asd");
        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $keyword2=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;
        //to ensure that all tables are not fetched -- only for the logged in user


        $designation = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $designation2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $designation3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        $empid = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');


        // dd($city);

        if($designation3=="Field Officer")
        {

            if($keyword1 == "")
            {

                return view('fieldexecutive.index');
            }
            // dd($status1);
            // query for assigned to and check in product and pharamchy table
            if($filter1=="AssignedTo")
            {

                if($status1=="All")
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                    ->orWhere('PAssignedTo', 'like',   $keyword2 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(200);
                }
                else
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->where('OrderStatus',$status1)
                    ->where('OrderStatusrent',$status1)
                    ->where('pOrderStatus',$status1)
                    ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                    ->orWhere('PAssignedTo', 'like',   $keyword2 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(200);
                }

                // dd($data1);

                return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
            }

            // query for order id

            if($filter1=="orderid")
            {
                if($status1=="All")
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->Where('prodleads.empid',$logged_in_user)
                    ->orWhere('AssignedTo', $logged_in_user)
                    ->orWhere('PAssignedTo', $logged_in_user)
                    ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(200);
                }
                else
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->Where('prodleads.empid',$logged_in_user)
                    ->Where('PAssignedTo', $logged_in_user)
                    ->where('OrderStatus',$status1)
                    ->where('OrderStatusrent',$status1)
                    ->where('pOrderStatus',$status1)
                    ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(200);
                }

                // dd($data1);

                return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
            }

            // query for order status and it will check for all three order status product (sell and rent) and pharamcy

            if($filter1=="OrderStatus")
            {

                $data1=DB::table('leads')
                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->Where('products.OrderStatus', 'like',   $keyword1 . '%')
                ->Where('AssignedTo',$logged_in_user)
                ->Where('PAssignedTo', $logged_in_user)
                ->orWhere('products.OrderStatusrent', 'like',   $keyword1 . '%')
                ->orWhere('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);

                return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
            }




            // if($filter1=="type")
            // {



            //     if($keyword1=="p" || $keyword1=="P")
            //     {
            //             $data1=DB::table('leads')
            //        ->select('addresses.*','leads.*','prodleads.*','products.*')
            //        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            //        ->join('products', 'leads.id', '=', 'products.leadid')
            //        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            //         ->orderBy('products.id', 'DESC')
            //         ->paginate(50);


            //          return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));

            //     }
            //     else
            //     {

            //        $data1=DB::table('leads')
            //        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            //        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            //        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            //        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
            //        ->orderBy('pharmacies.id', 'DESC')
            //         ->paginate(50);

            //         // dd($data1);

            //          return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));

            //     }
            // }

            // common query for first name, mobile number, orderid, leadid

            if($status1=="All")
            {
                $data1=DB::table('leads')
                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->Where('AssignedTo',$logged_in_user)
                ->orWhere('PAssignedTo', $logged_in_user)
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);
            }
            else
            {
                $data1=DB::table('leads')
                ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->where('OrderStatus',$status1)
                ->where('OrderStatusrent',$status1)
                ->where('pOrderStatus',$status1)
                ->Where('AssignedTo',$logged_in_user)
                ->orWhere('PAssignedTo', $logged_in_user)
                ->orderBy('prodleads.id', 'DESC')
                ->paginate(50);
            }

            return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));



        }
        else
        {

            if($designation3=="Field Executive")
            {

                if($keyword1 == "")
                {

                    return view('fieldexecutive.index');
                }

                // query for assigned to and check in product and pharamchy table
                if($filter1=="AssignedTo")
                {
                    if($status1=="All")
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$empid)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }
                    else
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$empid)
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }
                    // dd($data1);

                    return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                }

                // query for order id

                if($filter1=="orderid")
                {
                    if($status1=="All")
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$empid)
                        ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }
                    else
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$empid)
                        ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }


                    // dd($data1);

                    return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                }

                // query for order status and it will check for all three order status product (sell and rent) and pharamcy

                if($filter1=="OrderStatus")
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->Where('prodleads.empid',$empid)
                    ->orWhere('products.OrderStatus', 'like',   $keyword1 . '%')
                    ->orWhere('products.OrderStatusrent', 'like',   $keyword1 . '%')
                    ->orWhere('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);

                    return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                }




                // common query for first name, mobile number, orderid, leadid
                if($status1=="All")
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->Where('prodleads.empid',$empid)
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);
                }
                else
                {
                    $data1=DB::table('leads')
                    ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                    ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'prodleads.Prodid', '=', 'products.id')
                    ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                    ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->Where('prodleads.empid',$empid)
                    ->where('OrderStatus',$status1)
                    ->where('OrderStatusrent',$status1)
                    ->where('pOrderStatus',$status1)
                    ->orderBy('prodleads.id', 'DESC')
                    ->paginate(50);
                }

                return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));


            }

            //else if designation is Branch Head
            else
            {
                if($designation=="Branch Head")
                {


                    if($keyword1 == "")
                    {

                        return view('admin.allproductindex');
                    }


                    // query for assigned to and check in product and pharamchy table
                    if($filter1=="AssignedTo")
                    {
                        if($status1=="All")
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('City',$city)
                            ->Where('AssignedTo', 'like',   $keyword1 . '%')
                            ->Where('PAssignedTo', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }
                        else
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('City',$city)
                            ->where('OrderStatus',$status1)
                            ->where('OrderStatusrent',$status1)
                            ->where('pOrderStatus',$status1)
                            ->Where('AssignedTo', 'like',   $keyword1 . '%')
                            ->Where('PAssignedTo', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }

                        // dd($data1);

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }

                    // query for order id

                    if($filter1=="orderid")
                    {

                        if($status1=="All")
                        {

                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('City',$city)
                            ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);

                        }
                        else
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('City',$city)
                            ->where('OrderStatus',$status1)
                            ->where('OrderStatusrent',$status1)
                            ->where('pOrderStatus',$status1)
                            ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }

                        // dd($data1);

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }

                    // query for order status and it will check for all three order status product (sell and rent) and pharamcy

                    if($filter1=="OrderStatus")
                    {

                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->where('City',$city)
                        ->Where('products.OrderStatus', 'like',   $keyword1 . '%')
                        ->Where('products.OrderStatusrent', 'like',   $keyword1 . '%')
                        ->Where('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                        dd('asd');

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }




                    // common query for first name, mobile number, orderid, leadid
                    if($status1=="All")
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->where('City',$city)
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);

                    }
                    else
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->where('City',$city)
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                    }

                    return view('product.adminproductfilter',compact('data1','keyword1','filter1'));




                    if($keyword1 == "")
                    {

                        return view('admin.allproductindex');
                    }
                }
                //else if designation is admin
                else
                {

                    if($keyword1 == "")
                    {

                        return view('admin.allproductindex');
                    }


                    // query for assigned to and check in product and pharamchy table
                    if($filter1=="AssignedTo")
                    {
                        if($status1=="All")
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                            ->orWhere('PAssignedTo', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }
                        else
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('OrderStatus',$status1)
                            ->where('OrderStatusrent',$status1)
                            ->where('pOrderStatus',$status1)
                            ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                            ->orWhere('PAssignedTo', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }

                        // dd($data1);

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }

                    // query for order id

                    if($filter1=="orderid")
                    {
                        if($status1=="All")
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->orWhere('prodleads.orderid', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }
                        else
                        {
                            $data1=DB::table('leads')
                            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('products', 'prodleads.Prodid', '=', 'products.id')
                            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                            ->where('OrderStatus',$status1)
                            ->where('OrderStatusrent',$status1)
                            ->where('pOrderStatus',$status1)
                            ->orWhere('prodleads.orderid', 'like',   $keyword1 . '%')
                            ->orderBy('prodleads.id', 'DESC')
                            ->paginate(200);
                        }

                        // dd($data1);

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }

                    // query for order status and it will check for all three order status product (sell and rent) and pharamcy

                    if($filter1=="OrderStatus")
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('products.OrderStatus', 'like',   $keyword1 . '%')
                        ->orWhere('products.OrderStatusrent', 'like',   $keyword1 . '%')
                        ->orWhere('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);

                        return view('product.adminproductfilter',compact('data1','keyword1','filter1'));
                    }




                    // common query for first name, mobile number, orderid, leadid
                    if($status1=="All")
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                    }
                    else
                    {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                    }

                    return view('product.adminproductfilter',compact('data1','keyword1','filter1'));




                    if($keyword1 == "")
                    {

                        return view('admin.allproductindex');
                    }
                }
            }
        }


    }
    // search filter ends here



    // produt filter for customer care and marketing manager

    public function ccfilter(Request $request)
    {
        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;
        //to ensure that all tables are not fetched -- only for the logged in user


        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        // dd($desig);


        if($keyword1 == "")
        {

            return view('product.index');
        }


        // query for assigned to and check in product and pharamchy table
        if($filter1=="AssignedTo")
        {
            $data1=DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
            ->orWhere('PAssignedTo', 'like',   $keyword1 . '%')
            ->orderBy('prodleads.id', 'DESC')
            ->paginate(200);

            // dd($data1);

            return view('cc.productfilter',compact('data1','keyword1','filter1'));
        }

        // query for order id

        if($filter1=="orderid")
        {
            $data1=DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->orWhere('prodleads.orderid', 'like',   $keyword1 . '%')
            ->orderBy('prodleads.id', 'DESC')
            ->paginate(200);

            // dd($data1);

            return view('cc.productfilter',compact('data1','keyword1','filter1'));
        }

        // query for order status and it will check for all three order status product (sell and rent) and pharamcy

        if($filter1=="OrderStatus")
        {
            $data1=DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->Where('products.OrderStatus', 'like',   $keyword1 . '%')
            ->orWhere('products.OrderStatusrent', 'like',   $keyword1 . '%')
            ->orWhere('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
            ->orderBy('prodleads.id', 'DESC')
            ->paginate(50);

            return view('cc.productfilter',compact('data1','keyword1','filter1'));
        }




        // common query for first name, mobile number, orderid, leadid

        $data1=DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->Where($filter1, 'like',   $keyword1 . '%')
        ->orderBy('prodleads.id', 'DESC')
        ->paginate(50);

        return view('cc.productfilter',compact('data1','keyword1','filter1'));




        if($keyword1 == "")
        {

            return view('product.index');
        }


    }
    // search filter ends here



    public function assigned()
    {

        $status=$_GET['status'];
        $logged_in_user = Auth::guard('admin')->user()->name;
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;



                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->orderBy('prodleads.id', 'DESC')
                ->get();






                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }
        }

        /*
        Logic for downloading CSV goes here -- ends here
        */


    }



    // admin and management filter for product if type is selling or rental






    public function adminassigned(Request $request)
    {
        // dd("Sd");
        $filter1=$request->filter1;
        // dd($filter1);

        $keyword1=$request->keyword1;
        // dd($keyword1);

        $status1=$request->status1;
        $type=$request->type;
        $logged_in_user=$request->name;

        // dd($keyword1,$filter1,$status1,$logged_in_user,$type);

        if($keyword1 == "")
        {

            return view('product.productindex');
        }



        // $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');


        if($type=="Pharmacy")
        {

            if($keyword1 == "")
            {

                return view('pharmacy.pharmacyindex');
            }

            // dd($filter1);
            if($status1=="All")
            {
                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('pharmacies.id', 'DESC')
                ->paginate(50);
            }
            else
            {
                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
                ->where('pOrderStatus',$status1)
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('pharmacies.id', 'DESC')
                ->paginate(50);
            }

            // dd($leads);
            return view('product.pharmacyfilter',compact('data1','keyword1','filter1'));







        }
        // else
        // {

        // //join query to retrieve all the search filter values
        // $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
        // ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        // ->join('services', 'leads.id', '=', 'services.LeadId')
        // ->Where($filter1, 'like',   $keyword1 . '%')
        // ->orderBy('leads.id', 'DESC')->paginate(200);

        // return view('cc.alldata',compact('data1','keyword1','filter1'));

        // }






        if($type=="Selling")
        {

            if($status1=="All")
            {

                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','products.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'leads.id', '=', 'products.leadid')
                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                ->where('products.Type',"Sell")
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('products.id', 'DESC')
                ->paginate(50);
            }
            else
            {

                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','products.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'leads.id', '=', 'products.leadid')
                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                ->where('products.Type',"Sell")
                ->where('OrderStatus',$status1)
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('products.id', 'DESC')
                ->paginate(50);
            }

            return view('product.productfilter',compact('data1','keyword1','filter1'));
        }
        else
        {
            if($filter1=="OrderStatus")
            {
                $filter1="OrderStatusrent";
            }
            if($status1=="All")
            {
                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','products.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'leads.id', '=', 'products.leadid')
                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                ->where('products.Type',"Rent")
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('products.id', 'DESC')
                ->paginate(50);
            }
            else
            {
                $data1=DB::table('leads')
                ->select('addresses.*','leads.*','prodleads.*','products.*')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'leads.id', '=', 'products.leadid')
                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                ->where('products.Type',"Rent")
                ->where('OrderStatusrent',$status1)
                ->Where($filter1, 'like',   $keyword1 . '%')
                ->orderBy('products.id', 'DESC')
                ->paginate(50);
            }

            return view('product.productfilter',compact('data1','keyword1','filter1'));
        }







    }

    public function edit($id)
    {
        // dd($id);
        $productid = $id;

        // $pharmacyid = $leadid;
        $logged_in_user= Auth::guard('admin')->user()->name;

        $name = $logged_in_user;
        $value = "Product";

        if(Auth::guard('admin')->check())
        {

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp1=DB::table('employees')->where('Designation2','Vertical Head')->get();
            $products=DB::table('productdetails')->get();


            $empid=DB::table('employees')->where('FirstName',$name)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            // dd($eid);
            $Designation2=DB::table('employees')->where('id',$eid)->value('Department2');
            $Designation=DB::table('employees')->where('id',$eid)->value('Designation');
            $Department=DB::table('employees')->where('id',$eid)->value('Department');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);


            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            $leadid = DB::table('products')->where('id',$productid)->value('leadid');

            $referenceid = DB::table('leads')->where('id',$leadid)->value('Referenceid');

            $referencedetails = DB::table('refers')->where('id',$referenceid)->value('Reference');



            // dd($products);

            $leaddetails = lead::find($leadid);

            $addressid = DB::table('addresses')->where('leadid',$leadid)->value('id');
            // dd($addressid);
            $addressdetails = address::find($addressid);
            // dd($addressdetails);

            $productall = DB::table('products')->where('id',$productid)->get();
            // dd($productall);
            $productName = DB::table('products')->where('id',$productid)->value('ProductName');
            $SKUid = DB::table('products')->where('id',$productid)->value('SKUid');
            $DemoRequired = DB::table('products')->where('id',$productid)->value('DemoRequired');

            // dd($DemoRequired);
            $AvailabilityStatus = DB::table('products')->where('id',$productid)->value('AvailabilityStatus');
            $AvailabilityAddress = DB::table('products')->where('id',$productid)->value('AvailabilityAddress');
            $SellingPrice = DB::table('products')->where('id',$productid)->value('SellingPrice');
            $RentalPrice = DB::table('products')->where('id',$productid)->value('RentalPrice');
            $AdvanceAmt = DB::table('products')->where('id',$productid)->value('AdvanceAmt');
            $StartDate = DB::table('products')->where('id',$productid)->value('StartDate');
            $EndDate = DB::table('products')->where('id',$productid)->value('EndDate');
            $TransportCharges = DB::table('products')->where('id',$productid)->value('TransportCharges');

            // dd($AdvanceAmt,$StartDate,$EndDate,$TransportCharges);

            // dd($RentalPrice);
            $Type = DB::table('products')->where('id',$productid)->value('Type');
            // dd($Type);
            $Quantity = DB::table('products')->where('id',$productid)->value('Quantity');
            $ModeofPaymentrent = DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
            $ModeofPayment = DB::table('products')->where('id',$productid)->value('ModeofPayment');
            $TransportationCharges = DB::table('products')->where('id',$productid)->value('TransportCharges');
            // dd($productName);
            // dd($ModeofPaymentrent);


            $productName_array = explode("\r\n",$productName);
            $SKUid_array = explode("\r\n",$SKUid);
            $AvailabilityStatus_array = explode("\r\n",$AvailabilityStatus);
            $AvailabilityAddress_array = explode("\r\n",$AvailabilityAddress);
            $SellingPrice_array = explode("\r\n",$SellingPrice);
            $RentalPrice_array = explode("\r\n",$RentalPrice);
            $Quantity_array = explode("\r\n",$Quantity);
            // $Type_array = explode("\r\n",$Type);
            $AdvanceAmt_array = explode("\r\n",$AdvanceAmt);
            $StartDate_array = explode("\r\n",$StartDate);
            $EndDate_array = explode("\r\n",$EndDate);
            // dd($productName_array);
            $product_count = count($productName_array);

            // dd($productName_array,$SKUid_array,$DemoRequired,$AvailabilityStatus_array,$RentalPrice_array,$AvailabilityAddress_array,$Quantity_array, $ModeofPaymentrent);

            // dd($DemoRequired_array);
            $pdept = "Product";

            // dd($products);
            // dd($Type_array);


            $adminid = DB::table('admins')->where('name',$name)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');


            $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

            $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');

            $user_department=DB::table('employees')->where('FirstName',$name)->value('Department');

            $check_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
            $fetch_city=DB::table('addresses')->where('leadid',$leadid)->value('City');

            if($designation=="Admin" || $designation== "Management" || ($designation=="Branch Head" && $check_city==$fetch_city) || $designation2=="Product Manager")
            {
                return view('product/editproduct',compact('leadid','value','productid','leaddetails','count','country_codes','reference','count1','indian_cities','products','Designation','Designation2'
                ,'Department','addressdetails','referencedetails','product_count','productName_array','SKUid_array','DemoRequired','AvailabilityStatus_array','SellingPrice_array'
                ,'AvailabilityAddress_array','RentalPrice_array','Type','Quantity_array','ModeofPaymentrent','ModeofPayment','AdvanceAmt_array','StartDate_array','EndDate_array','TransportationCharges','pdept'));

            }
            else
            {
                return redirect('/admin');
            }






        }
    }

    public function update(Request $request, $id)
    {

        $requested_products_values = $request->all();
        // dd($requested_products_values);
        unset($requested_products_values['_token'], $requested_products_values['_method'],
        $requested_products_values['clientfname'],$requested_products_values['clientmname'],$requested_products_values['clientlname'],
        $requested_products_values['clientemail'],$requested_products_values['code'],$requested_products_values['clientmob'],
        $requested_products_values['clientalternateno'],$requested_products_values['EmergencyContact'],$requested_products_values['reference'],
        $requested_products_values['source'],$requested_products_values['Address1'],$requested_products_values['Address2'],
        $requested_products_values['City'],$requested_products_values['District'],$requested_products_values['State'],
        $requested_products_values['PinCode'],$requested_products_values['PAddress1'],$requested_products_values['PAddress2'],
        $requested_products_values['PCity'],$requested_products_values['PDistrict'],$requested_products_values['PState'],
        $requested_products_values['PPinCode'],$requested_products_values['SAddress1'],$requested_products_values['SAddress2'],
        $requested_products_values['SCity'],$requested_products_values['SDistrict'],
        $requested_products_values['SState'],$requested_products_values['SPinCode'],$requested_products_values['nooffileds'],$requested_products_values['value']);
        // dd($requested_products_values,$request->all());

        $new_product_values = null;
        $new_product_columns = null;

        foreach($requested_products_values as $key=>$value)
        {
            $new_product_columns[] = $key;
            $new_product_values[] = $value;
        }
        // dd($new_product_columns,$new_product_values);
        /* fetch who has created this lead */
        $productid = $id;
        $pharmacyid = $id;
        $value= $request->value;

        if($value=="Product")
        {
            $leadid = DB::table('prodleads')->where('Prodid',$productid)->value('leadid');
            $orderid = DB::table('prodleads')->where('Prodid',$productid)->value('orderid');
            $previous_value_details = DB::table('products')->where('id',$productid)->get();
            $previous_value_details = json_decode($previous_value_details,true);

            // dd($previous_value_details);
            if($previous_value_details[0]['Type']=="Sell")
            {
                $skuid_prev_details = DB::table('products')->select('SKUid')->where('id',$productid)->get();
                $skuid_prev_details = json_decode($skuid_prev_details,true);
                $skuid_prev_details = explode("\r\n",$skuid_prev_details[0]['SKUid']);

                $productname_prev_details = DB::table('products')->select('ProductName')->where('id',$productid)->get();
                $productname_prev_details = json_decode($productname_prev_details,true);
                $productname_prev_details = explode("\r\n",$productname_prev_details[0]['ProductName']);

                $demorequired_prev_details = DB::table('products')->where('id',$productid)->value('DemoRequired');
                // $demorequired_prev_details = json_decode($demorequired_prev_details,true);
                // $demorequired_prev_details = explode("\r\n",$demorequired_prev_details[0]['DemoRequired']);

                $availabilitystatus_prev_details = DB::table('products')->select('AvailabilityStatus')->where('id',$productid)->get();
                $availabilitystatus_prev_details = json_decode($availabilitystatus_prev_details,true);
                $availabilitystatus_prev_details = explode("\r\n",$availabilitystatus_prev_details[0]['AvailabilityStatus']);

                $availabilityaddress_prev_details = DB::table('products')->select('AvailabilityAddress')->where('id',$productid)->get();
                $availabilityaddress_prev_details = json_decode($availabilityaddress_prev_details,true);
                $availabilityaddress_prev_details = explode("\r\n",$availabilityaddress_prev_details[0]['AvailabilityAddress']);

                $sellingprice_prev_details = DB::table('products')->select('SellingPrice')->where('id',$productid)->get();
                $sellingprice_prev_details = json_decode($sellingprice_prev_details,true);
                $sellingprice_prev_details = explode("\r\n",$sellingprice_prev_details[0]['SellingPrice']);

                $type_prev_details = DB::table('products')->where('id',$productid)->value('Type');
                // $type_prev_details = json_decode($type_prev_details,true);
                // $type_prev_details = explode("\r\n",$type_prev_details[0]['Type']);

                $quantity_prev_details = DB::table('products')->select('Quantity')->where('id',$productid)->get();
                $quantity_prev_details = json_decode($quantity_prev_details,true);
                $quantity_prev_details = explode("\r\n",$quantity_prev_details[0]['Quantity']);

                $modeofpayment_prev_details = DB::table('products')->where('id',$productid)->value('ModeofPayment');


                $deliverydate_prev_details = DB::table('products')->where('id',$productid)->value('deliverydate');
                // $deliverydate_prev_details = json_decode($deliverydate_prev_details,true);
                // $deliverydate_prev_details = explode("\r\n",$deliverydate_prev_details[0]['deliverydate']);

                //     dd($skuid_prev_details,$productname_prev_details,$demorequired_prev_details,$availabilitystatus_prev_details,$availabilityaddress_prev_details,$sellingprice_prev_details,$type_prev_details,$quantity_prev_details
                // ,$modeofpayment_prev_details,$deliverydate_prev_details);
                $product_name_count = count($productname_prev_details);
                // dd($product_name_count);

                $previous_values_array = null;
                $previous_columns_array = null;

                for($z=0;$z<$product_name_count;$z++)
                {
                    $previous_values_array[] = $productname_prev_details[$z];
                    $previous_values_array[] = $skuid_prev_details[$z];
                    $previous_values_array[] = $type_prev_details;
                    if($z==0)
                    {
                        $previous_values_array[] = $demorequired_prev_details[$z];
                    }
                    $previous_values_array[] = $availabilitystatus_prev_details[$z];
                    $previous_values_array[] = $availabilityaddress_prev_details[$z];
                    $previous_values_array[] = $quantity_prev_details[$z];
                    if($z==0)
                    {
                        $previous_values_array[] = $deliverydate_prev_details;
                    }
                    $previous_values_array[] = $sellingprice_prev_details[$z];


                    if($z==0)
                    {
                        $previous_values_array[] = $modeofpayment_prev_details;
                    }


                    $previous_columns_array[] = "ProductName";
                    $previous_columns_array[] = "SKUDid";
                    $previous_columns_array[] = "Type";
                    if($z==0)
                    {
                        $previous_columns_array[] = "DemoRequired";
                    }
                    $previous_columns_array[] = "AvailabilityStatus";
                    $previous_columns_array[] = "AvailabilityAddress";
                    $previous_columns_array[] = "Quantity";
                    if($z==0)
                    {
                        $previous_columns_array[] = "deliverydate";
                    }
                    $previous_columns_array[] = "SellingPrice";


                    if($z==0)
                    {
                        $previous_columns_array[] = "ModeofPayment";
                    }

                }

                // dd($previous_values_array,$previous_columns_array);
                // dd($skuid_prev_details,$productname_prev_details);
            }
            else
            {
                $skuid_prev_details = DB::table('products')->select('SKUid')->where('id',$productid)->get();
                $skuid_prev_details = json_decode($skuid_prev_details,true);
                $skuid_prev_details = explode("\r\n",$skuid_prev_details[0]['SKUid']);

                $productname_prev_details = DB::table('products')->select('ProductName')->where('id',$productid)->get();
                $productname_prev_details = json_decode($productname_prev_details,true);
                $productname_prev_details = explode("\r\n",$productname_prev_details[0]['ProductName']);

                $demorequired_prev_details = DB::table('products')->where('id',$productid)->value('DemoRequired');
                // $demorequired_prev_details = json_decode($demorequired_prev_details,true);
                // $demorequired_prev_details = explode("\r\n",$demorequired_prev_details[0]['DemoRequired']);

                $availabilitystatus_prev_details = DB::table('products')->select('AvailabilityStatus')->where('id',$productid)->get();
                $availabilitystatus_prev_details = json_decode($availabilitystatus_prev_details,true);
                $availabilitystatus_prev_details = explode("\r\n",$availabilitystatus_prev_details[0]['AvailabilityStatus']);

                $availabilityaddress_prev_details = DB::table('products')->select('AvailabilityAddress')->where('id',$productid)->get();
                $availabilityaddress_prev_details = json_decode($availabilityaddress_prev_details,true);
                $availabilityaddress_prev_details = explode("\r\n",$availabilityaddress_prev_details[0]['AvailabilityAddress']);

                $rentalprice_prev_details = DB::table('products')->select('RentalPrice')->where('id',$productid)->get();
                $rentalprice_prev_details = json_decode($rentalprice_prev_details,true);
                $rentalprice_prev_details = explode("\r\n",$rentalprice_prev_details[0]['RentalPrice']);

                $type_prev_details = DB::table('products')->where('id',$productid)->value('Type');
                // $type_prev_details = json_decode($type_prev_details,true);
                // $type_prev_details = explode("\r\n",$type_prev_details[0]['Type']);

                $quantity_prev_details = DB::table('products')->select('Quantity')->where('id',$productid)->get();
                $quantity_prev_details = json_decode($quantity_prev_details,true);
                $quantity_prev_details = explode("\r\n",$quantity_prev_details[0]['Quantity']);

                $modeofpaymentrent_prev_details = DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
                // dd($modeofpaymentrent_prev_details);

                $deliverydate_prev_details = DB::table('products')->where('id',$productid)->value('deliverydate');
                // $deliverydate_prev_details = json_decode($deliverydate_prev_details,true);
                // $deliverydate_prev_details = explode("\r\n",$deliverydate_prev_details[0]['deliverydate']);

                $advanceamt_prev_details = DB::table('products')->select('AdvanceAmt')->where('id',$productid)->get();
                $advanceamt_prev_details = json_decode($advanceamt_prev_details,true);
                $advanceamt_prev_details = explode("\r\n",$advanceamt_prev_details[0]['AdvanceAmt']);

                $startdate_prev_details = DB::table('products')->select('StartDate')->where('id',$productid)->get();
                $startdate_prev_details = json_decode($startdate_prev_details,true);
                $startdate_prev_details = explode("\r\n",$startdate_prev_details[0]['StartDate']);

                $enddate_prev_details = DB::table('products')->select('EndDate')->where('id',$productid)->get();
                $enddate_prev_details = json_decode($enddate_prev_details,true);
                $enddate_prev_details = explode("\r\n",$enddate_prev_details[0]['EndDate']);

                $transportcharges_prev_details = DB::table('products')->select('TransportCharges')->where('id',$productid)->get();
                $transportcharges_prev_details = json_decode($transportcharges_prev_details,true);
                $transportcharges_prev_details = explode("\r\n",$transportcharges_prev_details[0]['TransportCharges']);

                //     dd($skuid_prev_details,$productname_prev_details,$demorequired_prev_details,$availabilitystatus_prev_details,$availabilityaddress_prev_details,$sellingprice_prev_details,$type_prev_details,$quantity_prev_details
                // ,$modeofpayment_prev_details,$deliverydate_prev_details);
                $product_name_count = count($productname_prev_details);
                // dd($product_name_count);

                $previous_values_array = null;
                $previous_columns_array = null;

                for($z=0;$z<$product_name_count;$z++)
                {
                    $previous_values_array[] = $productname_prev_details[$z];
                    $previous_values_array[] = $skuid_prev_details[$z];
                    $previous_values_array[] = $type_prev_details;

                    if($z==0)
                    {
                        $previous_values_array[] = $demorequired_prev_details;
                    }
                    $previous_values_array[] = $availabilitystatus_prev_details[$z];
                    $previous_values_array[] = $availabilityaddress_prev_details[$z];
                    $previous_values_array[] = $quantity_prev_details[$z];

                    if($z==0)
                    {
                        $previous_values_array[] = $deliverydate_prev_details;
                    }
                    $previous_values_array[] = $rentalprice_prev_details[$z];
                    $previous_values_array[] = $advanceamt_prev_details[$z];
                    $previous_values_array[] = $startdate_prev_details[$z];
                    $previous_values_array[] = $enddate_prev_details[$z];
                    if($z==0)
                    {
                        $previous_values_array[] = $transportcharges_prev_details[$z];
                        $previous_values_array[] = $modeofpaymentrent_prev_details;
                    }

                    $previous_columns_array[] = "ProductName";
                    $previous_columns_array[] = "SKUDid";
                    $previous_columns_array[] = "Type";

                    if($z==0)
                    {
                        $previous_columns_array[] = "DemoRequired";
                    }

                    $previous_columns_array[] = "AvailabilityStatus";
                    $previous_columns_array[] = "AvailabilityAddress";

                    $previous_columns_array[] = "Quantity";


                    if($z==0)
                    {
                        $previous_columns_array[] = "deliverydate";
                    }
                    $previous_columns_array[] = "RentalPrice";
                    $previous_columns_array[] = "AdvanceAmount";
                    $previous_columns_array[] = "StartDate";
                    $previous_columns_array[] = "EndDate";


                    if($z==0)
                    {
                        $previous_columns_array[] = "TransportCharges";
                        $previous_columns_array[] = "ModeofPaymentRent";
                    }

                }

                // dd($previous_values_array,$previous_columns_array);
                // dd($skuid_prev_details,$productname_prev_details);                // dd($skuid_prev_details,"asd");

            }
        }
        else
        {
            $leadid = DB::table('prodleads')->where('Pharmacyid',$pharmacyid)->value('leadid');
            $orderid = DB::table('prodleads')->where('Pharmacyid',$pharmacyid)->value('orderid');

            $previous_value_details = DB::table('pharmacies')->where('id',$pharmacyid)->get();
            // dd($previous_value_details);
            // dd($request->all());

            $requested_products_values = $request->all();

            unset($requested_products_values['_token'], $requested_products_values['_method'],
            $requested_products_values['clientfname'],$requested_products_values['clientmname'],$requested_products_values['clientlname'],
            $requested_products_values['clientemail'],$requested_products_values['code'],$requested_products_values['clientmob'],
            $requested_products_values['clientalternateno'],$requested_products_values['EmergencyContact'],$requested_products_values['reference'],
            $requested_products_values['source'],$requested_products_values['Address1'],$requested_products_values['Address2'],
            $requested_products_values['City'],$requested_products_values['District'],$requested_products_values['State'],
            $requested_products_values['PinCode'],$requested_products_values['PAddress1'],$requested_products_values['PAddress2'],
            $requested_products_values['PCity'],$requested_products_values['PDistrict'],$requested_products_values['PState'],
            $requested_products_values['PPinCode'],$requested_products_values['SAddress1'],$requested_products_values['SAddress2'],
            $requested_products_values['SCity'],$requested_products_values['SDistrict'],
            $requested_products_values['SState'],$requested_products_values['SPinCode'],$requested_products_values['nooffileds'],$requested_products_values['value']);
            // dd($requested_products_values,$request->all());

            $new_product_values = null;
            $new_product_columns = null;

            foreach($requested_products_values as $key=>$value)
            {
                $new_product_columns[] = $key;
                $new_product_values[] = $value;
            }

            $medname_prev_details = DB::table('pharmacies')->select('MedName')->where('id',$pharmacyid)->get();
            $medname_prev_details = json_decode($medname_prev_details,true);
            $medname_prev_details = explode("\r\n",$medname_prev_details[0]['MedName']);

            $strength_prev_details = DB::table('pharmacies')->select('Strength')->where('id',$pharmacyid)->get();
            $strength_prev_details = json_decode($strength_prev_details,true);
            $strength_prev_details = explode("\r\n",$strength_prev_details[0]['Strength']);

            $pquantity_prev_details = DB::table('pharmacies')->select('PQuantity')->where('id',$pharmacyid)->get();
            $pquantity_prev_details = json_decode($pquantity_prev_details,true);
            $pquantity_prev_details = explode("\r\n",$pquantity_prev_details[0]['PQuantity']);

            $medtype_prev_details = DB::table('pharmacies')->select('MedType')->where('id',$pharmacyid)->get();
            $medtype_prev_details = json_decode($medtype_prev_details,true);
            $medtype_prev_details = explode("\r\n",$medtype_prev_details[0]['MedType']);

            $price_prev_details = DB::table('pharmacies')->select('Price')->where('id',$pharmacyid)->get();
            $price_prev_details = json_decode($price_prev_details,true);
            $price_prev_details = explode("\r\n",$price_prev_details[0]['Price']);

            $quantitytype_prev_details = DB::table('pharmacies')->select('QuantityType')->where('id',$pharmacyid)->get();
            $quantitytype_prev_details = json_decode($quantitytype_prev_details,true);
            $quantitytype_prev_details = explode("\r\n",$quantitytype_prev_details[0]['QuantityType']);

            $pavailabilitystatus_prev_details = DB::table('pharmacies')->select('PAvailabilityStatus')->where('id',$pharmacyid)->get();
            $pavailabilitystatus_prev_details = json_decode($pavailabilitystatus_prev_details,true);
            $pavailabilitystatus_prev_details = explode("\r\n",$pavailabilitystatus_prev_details[0]['PAvailabilityStatus']);

            $pmodeofpayment_prev_details = DB::table('pharmacies')->select('PModeofpayment')->where('id',$pharmacyid)->get();
            $pmodeofpayment_prev_details = json_decode($pmodeofpayment_prev_details,true);
            $pmodeofpayment_prev_details = explode("\r\n",$pmodeofpayment_prev_details[0]['PModeofpayment']);

            $product_name_count = count($medname_prev_details);

            $previous_values_array = null;
            $previous_columns_array = null;

            for($z=0;$z<$product_name_count;$z++)
            {
                $previous_values_array[] = $medtype_prev_details[$z];
                $previous_values_array[] = $medname_prev_details[$z];
                $previous_values_array[] = $strength_prev_details[$z];
                $previous_values_array[] = $pquantity_prev_details[$z];
                $previous_values_array[] = $quantitytype_prev_details[$z];
                $previous_values_array[] = $pavailabilitystatus_prev_details[$z];
                $previous_values_array[] = $price_prev_details[$z];
                $previous_values_array[] = $pmodeofpayment_prev_details[$z];

                $previous_columns_array[] = "MedType";
                $previous_columns_array[] = "Medicine Name";
                $previous_columns_array[] = "Strength";
                $previous_columns_array[] = "Quantity";
                $previous_columns_array[] = "Quantity Type";
                $previous_columns_array[] = "Availability Status";
                $previous_columns_array[] = "Selling Price";
                $previous_columns_array[] = "Mode of payment";
            }
        }



        // $previous_value_details = json_decode($previous_value_details,true);
        // dd($previous_value_details);

        $name1= $request->loginname;


        // dd($new_product_values,$previous_values_array,$new_product_columns,$previous_columns_array);
        // dd($new_product_columns,$previous_columns_array);


        if($value=="Product")
        {
            $count_for_products = count($previous_values_array);
            $logs_for_product_column = null;
            $logs_for_product_values = null;
            // dd($count_for_products);
            // dd($previous_values_array[2],$new_product_values[2]);


            $k=2;
            $flag = 0;
            for($i=0;$i<$count_for_products;$i++)
            {
                // echo $k.'<br />';
                // if($)
                // if($previous_values_array[$k] == $new_product_values[$k])
                // {
                if($previous_values_array[$i]!=$new_product_values[$i] || $i==$k)
                {

                    $count = count($logs_for_product_values);

                    if($i==$k)
                    {
                        if($previous_values_array[$i]!=$new_product_values[$i])
                        {
                            // dd('some change occured');
                            //unset the previous values in log
                            if($previous_values_array[$i]=="Rent")
                            {
                                if($i==2)
                                {
                                    $i = $i+12;
                                    $k = $k+14;

                                    // here we are trying to get the session id of the present logged in person
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                                    //here we are extracting the name of the present logged in person
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    //here we are extracting the emp id and the log id of the present logged in person
                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "New Product Has been Created";
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid." / ".$orderid;


                                    $activity->save();

                                }
                                else
                                {
                                    $i = $i+10;
                                    $k = $k+10;

                                    // here we are trying to get the session id of the present logged in person
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                                    //here we are extracting the name of the present logged in person
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    //here we are extracting the emp id and the log id of the present logged in person
                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "New Product Has been Created";
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid." / ".$orderid;


                                    $activity->save();

                                }

                            }
                            else
                            {

                                if($i==2)
                                {
                                    $i = $i+6;
                                    $k = $k+8;

                                    // here we are trying to get the session id of the present logged in person
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                                    //here we are extracting the name of the present logged in person
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    //here we are extracting the emp id and the log id of the present logged in person
                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "New Product Has been Created";
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid." / ".$orderid;


                                    $activity->save();

                                }
                                else
                                {
                                    $i = $i+9;
                                    $k = $k+9;

                                    // here we are trying to get the session id of the present logged in person
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                                    //here we are extracting the name of the present logged in person
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    //here we are extracting the emp id and the log id of the present logged in person
                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "New Product Has been Created";
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid." / ".$orderid;


                                    $activity->save();

                                }
                            }


                        }
                        else if($previous_values_array[$i]==$new_product_values[$i])
                        {
                            // dd('reset the value of k');
                            if($previous_values_array[$i]=="Rent")
                            {
                                $k = $k+14;
                            }
                            else
                            {
                                $k = $k+8;
                            }
                        }
                    }
                    else
                    {
                        if($previous_values_array[$i]!=$new_product_values[$i])
                        {
                            $logs_for_product_column[] = $previous_columns_array[$i];
                            $logs_for_product_values[] = $new_product_values[$i];
                        }
                    }

                }

            }
        }
        else
        {
            $count_for_products = count($previous_values_array);
            $logs_for_product_column = null;
            $logs_for_product_values = null;

            for($i=0;$i<$count_for_products;$i++)
            {
            if($previous_values_array[$i]!=$new_product_values[$i])
            {
                $logs_for_product_column[] = $previous_columns_array[$i];
                $logs_for_product_values[] = $new_product_values[$i];
            }
        }
        }
        // dd($logs_for_product_column,$logs_for_product_values);

        if($logs_for_product_column!=NULL)
        {
            $fields_updated = implode("\r\n",$logs_for_product_column);
            $values_updated = implode("\r\n",$logs_for_product_values);
        }
        else
        {
            $fields_updated="";
            $values_updated="";
        }
        // here we are trying to get the session id of the present logged in person
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //here we are extracting the name of the present logged in person
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        //here we are extracting the emp id and the log id of the present logged in person
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        if((trim($fields_updated)!==""))
        {
            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Fields Updated";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->lead_id = $leadid." / ".$orderid;
            $activity->field_updated = $fields_updated;
            $activity->value_updated = $values_updated;

            $activity->save();
        }

        // dd('for loop ends');
        /* Code for storing the details entered in the form starts here */
        //Whatever reference is selected by the user , it's id needs to be retrieved and passed into the Lead reference id column
        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=Null;

        //retrieve the employee id of the logged in user
        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        //fetching product details
        $c=$request->SKUid;
        $c2=$request->ProductName;
        $c1=$request->MedName;






        //checking if not existing client than create a new lead
        // if the existing client is not entered by the user, then it is "NULL" by default
        //In such a case, create a new lead altogether

        if($c!=NULL || $c1!=NULL || $c2!=NULL)

        {
            $lead = lead::find($leadid);
            // $this->validate($request,[
            //         'Languages'=>'required',
            //     ]);

            //If the source field is not filled, then take the reference id of the selected Reference
            //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
            if($request->source == Null)
            {
                //take out the value in the "reference" field, and assign the same value to "source" as well
                $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                $source=$refer;
            }
            //if the Other option is selected, then whatever is filled in the source field
            //pass that to the "Source" field
            else
            {
                $source=$request->source;
            }

            //fetch all the corresponding fields
            $lead->fName=$request->clientfname;
            $lead->mName=$request->clientmname;
            $lead->lName=$request->clientlname;
            $lead->EmailId=$request->clientemail;
            $lead->Source=$source;
            $lead->MobileNumber=$request->clientmob;
            $lead->Alternatenumber=$request->clientalternateno;
            $lead->EmergencyContact =$request->EmergencyContact;
            $lead->AssesmentReq=$request->assesmentreq;
            $lead->createdby=$request->loginname;
            $lead->Referenceid=$ref;
            $lead->empid=$empid;
            $lead->save();


            $addressid = DB::table('addresses')->where('leadid',$leadid)->value('id');
            // dd($addressid);
            $address = address::find($addressid);

            //fetch and store all the address details
            $address->Address1=$request->Address1;
            $address->Address2=$request->Address2;
            $address->City=$request->City;
            $address->District=$request->District;
            $address->State=$request->State;
            $address->PinCode=$request->PinCode;


            $same1=$request->presentcontact;

            if($same1=='same')
            {
                $address->PAddress1=$request->Address1;
                $address->PAddress2=$request->Address2;
                $address->PCity=$request->City;
                $address->PDistrict=$request->District;
                $address->PState=$request->State;
                $address->PPinCode=$request->PinCode;
            }

            else
            {
                $address->PAddress1=$request->PAddress1;
                $address->PAddress2=$request->PAddress2;
                $address->PCity=$request->PCity;
                $address->PDistrict=$request->PDistrict;
                $address->PState=$request->PState;
                $address->PPinCode=$request->PPinCode;
            }

            $same=$request->emergencycontact;
            if($same=='same')
            {
                $address->EAddress1=$request->Address1;
                $address->EAddress2=$request->Address2;
                $address->ECity=$request->City;
                $address->EDistrict=$request->District;
                $address->EState=$request->State;
                $address->EPinCode=$request->PinCode;
            }

            else
            {
                $address->EAddress1=$request->EAddress1;
                $address->EAddress2=$request->EAddress2;
                $address->ECity=$request->ECity;
                $address->EDistrict=$request->EDistrict;
                $address->EState=$request->EState;
                $address->EPinCode=$request->EPinCode;
            }

            $same2=$request->shippingcontact;
            if($same2=='same')
            {
                $address->SAddress1=$request->Address1;
                $address->SAddress2=$request->Address2;
                $address->SCity=$request->City;
                $address->SDistrict=$request->District;
                $address->SState=$request->State;
                $address->SPinCode=$request->PinCode;
            }

            else
            {
                $address->SAddress1=$request->SAddress1;
                $address->SAddress2=$request->SAddress2;
                $address->SCity=$request->SCity;
                $address->SDistrict=$request->SDistrict;
                $address->SState=$request->SState;
                $address->SPinCode=$request->SPinCode;
            }


            $address->leadid=$leadid;
            $address->empid=$empid;
            $address->save();
            $addressid=DB::table('addresses')->max('id');

            //     // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"
            //
            //
            //     //retrieving the session id of the person who is logged in
            //     $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
            //
            //     //retrieving the username of the presently logged in user
            //     $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
            //
            //     // retrieving the employee id of the presently logged in user
            //     $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
            //
            //     //retrieving the log id of the person who logged in just now
            //     $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
            //
            //
            //     $activity = new Activity;
            //     $activity->emp_id = $emp_id;
            //     $activity->activity = "New Lead Created ";
            //     $activity->activity_time = new \DateTime();
            //     $activity->log_id = $log_id;
            //     $activity->lead_id = $leadid;
            //     $activity->save();
        }




        // checking if product id or pharmacy id is null don't enter the details to DB
        $c=$request->SKUid;
        $c2=$request->ProductName;
        $c1=$request->MedName;
        // dd($c,$c2,$c1);



        if($c!=NULL || $c2!=NULL)
        {



            if($request->Type=="Sell"  || $request->Type==NULL || $request->Type=="")
            {
                $OrderStatusrent=NULL;
            }

            if($request->Type=="Rent")
            {
                $OrderStatus=NULL;
            }

            $products = product::find($productid);

            $products->SKUid=NULL;
            $products->ProductName=NULL;
            $products->DemoRequired=NULL;
            $products->AvailabilityStatus=NULL;
            $products->AvailabilityAddress=NULL;
            $products->SellingPrice=NULL;
            $products->RentalPrice=NULL;
            $products->Type=NULL;
            // $products->OrderStatus=$OrderStatus;
            $products->Quantity=NULL;
            $products->deliverydate=NULL;
            $products->ModeofPaymentrent=NULL;
            // $products->OrderStatusrent=$OrderStatusrent;
            $products->ModeofPayment=NULL;
            $products->AdvanceAmt=NULL;
            $products->StartDate=NULL;
            $products->EndDate=NULL;
            $products->TransportCharges=NULL;
            $products->OverdueAmt=NULL;
            $products->save();

            $products = product::find($productid);
            // dd($request->SKUid);
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;
            $products->Type=$request->Type;
            // $products->OrderStatus=$OrderStatus;
            $products->Quantity=$request->Quantity1;
            $products->deliverydate=$request->deliverydate;
            $products->ModeofPaymentrent=$request->ModeofPaymentrent;
            // $products->OrderStatusrent=$OrderStatusrent;
            $products->ModeofPayment=$request->ModeofPayment;
            $products->AdvanceAmt=$request->AdvanceAmt;
            $products->StartDate=$request->StartDate;
            $products->EndDate=$request->EndDate;
            $products->TransportCharges=$request->TransportCharges;
            $products->OverdueAmt=$request->OverdueAmt;


            $products->save();

            // dd($request->Quantity1);


            // code for calculate sum of all products
            // dd($log_id);

            $old_values_id = DB::table('activities')->where('log_Id',$log_id)->orderBy('created_at','desc')->value('id');

            // dd($old_fields_log,$old_values_log,$old_values_id);
            /* code for product data by jatin starts here */
            //take out the details from the product table for that particular lead id
            $detail=DB::table('products')->where('id',$productid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail1);

            //retrieving id for product

            // Converting all products from json to array
            $ProductName = $detail1[0]['ProductName'];
            // dd($Mednames);
            $k =explode("\r\n",$ProductName);

            $SellingPrice =$detail1[0]['SellingPrice'];
            $k4 =explode("\r\n",$SellingPrice);


            $Type =$detail1[0]['Type'];
            $k5 =$Type;

            $Quantity =$detail1[0]['Quantity'];
            $k7 =explode("\r\n",$Quantity);



            $RentalPrice =$detail1[0]['RentalPrice'];
            $k15 =explode("\r\n",$RentalPrice);

            $TransportCharges = $detail1[0]['TransportCharges'];

            // dd($TransportCharges);
            // $k12 = explode("\r\n", $TransportCharges);



            $FinalAmount =$detail1[0]['FinalAmount'];



            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum = 0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {        if($k5=="Rent")
                {

                    $sum = $sum + ($k15[$cnt1]*$k7[$cnt1]);
                }
                else{
                    $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                }

            }


            if($k5=="Rent")
            {

                $sum = $sum  + $TransportCharges;
            }

            // dd($sum);

            $finalamt = product::find($productid);
            $finalamt->FinalAmount= $sum;
            $finalamt->save();

            // code end




            // Code for Product Logs starts here

            // //retrieving the session id of the person who is logged in
            // $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
            //
            // //retrieving the username of the presently logged in user
            // $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
            //
            // // retrieving the employee id of the presently logged in user
            // $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
            //
            // //retrieving the log id of the person who logged in just now
            // $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
            //
            // // dd($leadid);
            //
            // $activity = new Activity;
            // $activity->emp_id = $emp_id;
            // $activity->activity = "Lead Created for Product";
            // $activity->activity_time = new \DateTime();
            // $activity->log_id = $log_id;
            // $activity->lead_id = $leadid;
            // $activity->save();
        }







        // checking if more than one product is inserted or not

        $no=$request->nooffileds;
        $no1=$request->nooffiledsPharmacyDetails;
        //dd($no1);

        // creating 2 variable to check if they create both rent and sell request in one submit. this will help to split the request

        // dd($no);

        $productid2=1;
        $count_check_for_product2=0;

        if($no!=NULL)
        {

            for($i=2;$i<=$no;$i++)
            {

                $SKUid1="SKUid".$i;
                $ProductName1="ProductName".$i;
                $DemoRequired1="DemoRequired".$i;
                $AvailabilityStatus1="AvailabilityStatus".$i;
                $AvailabilityAddress1="AvailabilityAddress".$i;
                $SellingPrice1="SellingPrice".$i;
                $RentalPrice1="RentalPrice".$i;
                $Type1="Type".$i;
                $OrderStatus1="OrderStatus".$i;
                $Quantity1="Quantity".$i;
                $ModeofPaymentrent1="ModeofPaymentrent".$i;
                $OrderStatusrent1="OrderStatusrent".$i;
                $ModeofPayment1="ModeofPayment".$i;
                $AdvanceAmt1="AdvanceAmt".$i;
                $StartDate1="StartDate".$i;
                $EndDate1="EndDate".$i;
                $OverdueAmt1="OverdueAmt".$i;


                $SKUid=$request->$SKUid1;
                $ProductName=$request->$ProductName1;
                $DemoRequired=$request->$DemoRequired1;
                $AvailabilityStatus=$request->$AvailabilityStatus1;
                $AvailabilityAddress=$request->$AvailabilityAddress1;
                $SellingPrice=$request->$SellingPrice1;
                $RentalPrice=$request->$RentalPrice1;
                $Type=$request->$Type1;
                $OrderStatus=$request->$OrderStatus1;
                $Quantity=$request->$Quantity1;
                $ModeofPaymentrent=$request->$ModeofPaymentrent1;
                $OrderStatusrent=$request->$OrderStatusrent1;
                $ModeofPayment=$request->$ModeofPayment1;
                $AdvanceAmt=$request->$AdvanceAmt1;
                $StartDate=$request->$StartDate1;
                $EndDate=$request->$EndDate1;
                $OverdueAmt=$request->$OverdueAmt1;



                $checktype=DB::table('products')->where('id',$productid)->value('Type');
                // dd($checktype);


                if($Type==$checktype)
                {

                    $skudetail=DB::table('products')->where('id',$productid)->value('SKUid');
                    $ProductNamedetail=DB::table('products')->where('id',$productid)->value('ProductName');
                    // $DemoRequireddetail=DB::table('products')->where('id',$productid)->value('DemoRequired');
                    $AvailabilityStatusdetail=DB::table('products')->where('id',$productid)->value('AvailabilityStatus');
                    $AvailabilityAddressdetail=DB::table('products')->where('id',$productid)->value('AvailabilityAddress');
                    $SellingPricedetail=DB::table('products')->where('id',$productid)->value('SellingPrice');
                    $RentalPricedetail=DB::table('products')->where('id',$productid)->value('RentalPrice');
                    // $Typedetail=DB::table('products')->where('id',$productid)->value('Type');
                    // $OrderStatusdetail=DB::table('products')->where('id',$productid)->value('OrderStatus');
                    $Quantitydetail=DB::table('products')->where('id',$productid)->value('Quantity');
                    // $ModeofPaymentrentdetail=DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
                    // $OrderStatusrentdetail=DB::table('products')->where('id',$productid)->value('OrderStatusrent');
                    // $ModeofPaymentdetail=DB::table('products')->where('id',$productid)->value('ModeofPayment');
                    $AdvanceAmtdetail=DB::table('products')->where('id',$productid)->value('AdvanceAmt');
                    $StartDatedetail=DB::table('products')->where('id',$productid)->value('StartDate');
                    $EndDatedetail=DB::table('products')->where('id',$productid)->value('EndDate');
                    $OverdueAmtdetail=DB::table('products')->where('id',$productid)->value('OverdueAmt');

                }
                else
                {


                    $check_orderid1=DB::table('prodleads')->where('Prodid',$productid)->value('orderid');


                    $check_id1=DB::table('prodleads')->where('orderid',$check_orderid1)->where('Prodid',"<>","1")->get();

                    $check_count1=DB::table('prodleads')->where('orderid',$check_orderid1)->where('Prodid',"<>","1")->count();

                    // dd($check_id);

                    if($check_count1==1)
                    {
                        $productid2=1;

                    }
                    else
                    {
                        $check_id11=json_decode($check_id1);
                        $check_id21= $check_id11[0]->id;
                        $check_id31= $check_id11[1]->id;

                        $get_prodid11=DB::table('prodleads')->where('id',$check_id21)->value('Prodid');
                        $get_prodid21=DB::table('prodleads')->where('id',$check_id31)->value('Prodid');

                        $check_product_type1=DB::table('products')->where('id',$get_prodid11)->value('Type');



                        if($check_product_type1== $Type)
                        {
                            $productid2=$get_prodid11;
                        }
                        else
                        {
                            $productid2=$get_prodid21;
                        }




                    }


                    $skudetail=DB::table('products')->where('id',$productid2)->value('SKUid');
                    $ProductNamedetail=DB::table('products')->where('id',$productid2)->value('ProductName');
                    // $DemoRequireddetail=DB::table('products')->where('id',$productid)->value('DemoRequired');
                    $AvailabilityStatusdetail=DB::table('products')->where('id',$productid2)->value('AvailabilityStatus');
                    $AvailabilityAddressdetail=DB::table('products')->where('id',$productid2)->value('AvailabilityAddress');
                    $SellingPricedetail=DB::table('products')->where('id',$productid2)->value('SellingPrice');
                    $RentalPricedetail=DB::table('products')->where('id',$productid2)->value('RentalPrice');
                    // $Typedetail=DB::table('products')->where('id',$productid)->value('Type');
                    // $OrderStatusdetail=DB::table('products')->where('id',$productid)->value('OrderStatus');
                    $Quantitydetail=DB::table('products')->where('id',$productid2)->value('Quantity');
                    // $ModeofPaymentrentdetail=DB::table('products')->where('id',$productid)->value('ModeofPaymentrent');
                    // $OrderStatusrentdetail=DB::table('products')->where('id',$productid)->value('OrderStatusrent');
                    // $ModeofPaymentdetail=DB::table('products')->where('id',$productid)->value('ModeofPayment');
                    $AdvanceAmtdetail=DB::table('products')->where('id',$productid2)->value('AdvanceAmt');
                    $StartDatedetail=DB::table('products')->where('id',$productid2)->value('StartDate');
                    $EndDatedetail=DB::table('products')->where('id',$productid2)->value('EndDate');
                    $OverdueAmtdetail=DB::table('products')->where('id',$productid2)->value('OverdueAmt');

                    if($count_check_for_product2==0)
                    {
                        $products = product::find($productid2);

                        $products->SKUid=NULL;
                        $products->ProductName=NULL;
                        $products->DemoRequired=NULL;
                        $products->AvailabilityStatus=NULL;
                        $products->AvailabilityAddress=NULL;
                        $products->SellingPrice=NULL;
                        $products->RentalPrice=NULL;
                        // $products->Type=NULL;
                        // $products->OrderStatus=$OrderStatus;
                        $products->Quantity=NULL;
                        $products->deliverydate=NULL;
                        $products->ModeofPaymentrent=NULL;
                        // $products->OrderStatusrent=$OrderStatusrent;
                        $products->ModeofPayment=NULL;
                        $products->AdvanceAmt=NULL;
                        $products->StartDate=NULL;
                        $products->EndDate=NULL;
                        $products->TransportCharges=NULL;
                        $products->OverdueAmt=NULL;
                        $products->save();

                        $count_check_for_product2=1;
                    }
                    // dd($skudetail);
                }



                // in case if they skip 1 product details and insert 2nd detail
                if($productid ==1)
                {


                    $lead = lead::find($leadid);
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    //If the source field is not filled, then take the reference id of the selected Reference
                    //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
                    if($request->source == Null)
                    {
                        //take out the value in the "reference" field, and assign the same value to "source" as well
                        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                        $source=$refer;
                    }
                    //if the Other option is selected, then whatever is filled in the source field
                    //pass that to the "Source" field
                    else
                    {
                        $source=$request->source;
                    }

                    //fetch all the corresponding fields
                    $lead->fName=$request->clientfname;
                    $lead->mName=$request->clientmname;
                    $lead->lName=$request->clientlname;
                    $lead->EmailId=$request->clientemail;
                    $lead->Source=$source;
                    $lead->MobileNumber=$request->clientmob;
                    $lead->Alternatenumber=$request->clientalternateno;
                    $lead->EmergencyContact =$request->EmergencyContact;
                    $lead->AssesmentReq=$request->assesmentreq;
                    $lead->createdby=$request->loginname;
                    $lead->Referenceid=$ref;
                    $lead->empid=$empid;
                    $lead->save();


                    $address = address::find($addressid);

                    //fetch and store all the address details
                    $address->Address1=$request->Address1;
                    $address->Address2=$request->Address2;
                    $address->City=$request->City;
                    $address->District=$request->District;
                    $address->State=$request->State;
                    $address->PinCode=$request->PinCode;


                    $same1=$request->presentcontact;

                    if($same1=='same')
                    {
                        $address->PAddress1=$request->Address1;
                        $address->PAddress2=$request->Address2;
                        $address->PCity=$request->City;
                        $address->PDistrict=$request->District;
                        $address->PState=$request->State;
                        $address->PPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->PAddress1=$request->PAddress1;
                        $address->PAddress2=$request->PAddress2;
                        $address->PCity=$request->PCity;
                        $address->PDistrict=$request->PDistrict;
                        $address->PState=$request->PState;
                        $address->PPinCode=$request->PPinCode;
                    }

                    $same=$request->emergencycontact;
                    if($same=='same')
                    {
                        $address->EAddress1=$request->Address1;
                        $address->EAddress2=$request->Address2;
                        $address->ECity=$request->City;
                        $address->EDistrict=$request->District;
                        $address->EState=$request->State;
                        $address->EPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->EAddress1=$request->EAddress1;
                        $address->EAddress2=$request->EAddress2;
                        $address->ECity=$request->ECity;
                        $address->EDistrict=$request->EDistrict;
                        $address->EState=$request->EState;
                        $address->EPinCode=$request->EPinCode;
                    }

                    $same2=$request->shippingcontact;
                    if($same2=='same')
                    {
                        $address->SAddress1=$request->Address1;
                        $address->SAddress2=$request->Address2;
                        $address->SCity=$request->City;
                        $address->SDistrict=$request->District;
                        $address->SState=$request->State;
                        $address->SPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->SAddress1=$request->SAddress1;
                        $address->SAddress2=$request->SAddress2;
                        $address->SCity=$request->SCity;
                        $address->SDistrict=$request->SDistrict;
                        $address->SState=$request->SState;
                        $address->SPinCode=$request->SPinCode;
                    }


                    $address->leadid=$leadid;
                    $address->empid=$empid;
                    $address->save();

                    // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"


                    //retrieving the session id of the person who is logged in
                    // $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                    //
                    // //retrieving the username of the presently logged in user
                    // $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
                    //
                    // // retrieving the employee id of the presently logged in user
                    // $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                    //
                    // //retrieving the log id of the person who logged in just now
                    // $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
                    //
                    //
                    // $activity = new Activity;
                    // $activity->emp_id = $emp_id;
                    // $activity->activity = "New Lead Created ";
                    // $activity->activity_time = new \DateTime();
                    // $activity->log_id = $log_id;
                    // $activity->lead_id = $leadid;
                    // $activity->save();





                    if($request->Type=="Sell"  || $request->Type==NULL || $request->Type=="")
                    {
                        $OrderStatusrent=NULL;
                    }

                    if($request->Type=="Rent")
                    {
                        $OrderStatus=NULL;
                    }


                    $products = product::find($productid);
                    $products->SKUid=$SKUid;
                    $products->ProductName=$ProductName;
                    $products->DemoRequired=$DemoRequired;
                    $products->AvailabilityStatus=$AvailabilityStatus;
                    $products->AvailabilityAddress=$AvailabilityAddress;
                    $products->SellingPrice=$SellingPrice;
                    $products->RentalPrice=$RentalPrice;
                    $products->Type=$Type;
                    // $products->OrderStatus=$OrderStatus;
                    $products->Quantity=$Quantity;
                    $products->deliverydate=$request->deliverydate;
                    $products->ModeofPaymentrent=$ModeofPaymentrent;
                    // $products->OrderStatusrent=$OrderStatusrent;
                    $products->ModeofPayment=$ModeofPayment;
                    $products->AdvanceAmt=$AdvanceAmt;
                    $products->StartDate=$StartDate;
                    $products->EndDate=$EndDate;
                    // $products->TransportCharges=$TransportCharges;
                    $products->OverdueAmt=$OverdueAmt;
                    $products->Requestcreatedby=$name1;
                    $products->Leadid=$leadid;


                    $products->save();







                }
                else
                {
                    if($Type==$checktype)
                    {

                        $products=product::find($productid);
                        $products->SKUid=$skudetail."\r\n".$SKUid;
                        $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                        // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                        $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                        $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                        $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                        $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                        // $products->Type=$Typedetail."\r\n".$Type;
                        // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                        $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                        // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                        // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                        // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                        $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                        $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                        $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                        $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;
                        // dd('asd');
                        $old_fields_log = DB::table('activities')->where('log_Id',$log_id)->orderBy('created_at','desc')->value('field_updated');
                        $old_values_log = DB::table('activities')->where('log_Id',$log_id)->orderBy('created_at','desc')->value('value_updated');
                        // dd($old_fields_log,$old_values_log);

                        $products->save();

                    }
                    else
                    {
                        $check_orderid=DB::table('prodleads')->where('Prodid',$productid)->value('orderid');
                        $orderid=$check_orderid;

                        $check_id=DB::table('prodleads')->where('orderid',$check_orderid)->where('Prodid',"<>","1")->get();

                        $check_count=DB::table('prodleads')->where('orderid',$check_orderid)->where('Prodid',"<>","1")->count();

                        // dd($check_id);

                        if($check_count==1)
                        {
                            $productid2=1;

                        }
                        else
                        {
                            $check_id1=json_decode($check_id);
                            $check_id2= $check_id1[0]->id;
                            $check_id3= $check_id1[1]->id;

                            $get_prodid1=DB::table('prodleads')->where('id',$check_id2)->value('Prodid');
                            $get_prodid2=DB::table('prodleads')->where('id',$check_id3)->value('Prodid');

                            $check_product_type=DB::table('products')->where('id',$get_prodid1)->value('Type');



                            if($check_product_type== $Type)
                            {
                                $productid2=$get_prodid1;
                            }
                            else
                            {
                                $productid2=$get_prodid2;
                            }


                        }

                        if($productid2==1)
                        {


                            $OrderStatus="New";
                            $OrderStatusrent="New";

                            if($Type=="Sell")
                            {
                                $OrderStatusrent=NULL;
                            }

                            if($Type=="Rent")
                            {
                                $OrderStatus=NULL;
                            }

                            $products = new product;
                            $products->SKUid=$SKUid;
                            $products->ProductName=$ProductName;
                            $products->DemoRequired=$DemoRequired;
                            $products->AvailabilityStatus=$AvailabilityStatus;
                            $products->AvailabilityAddress=$AvailabilityAddress;
                            $products->SellingPrice=$SellingPrice;
                            $products->RentalPrice=$RentalPrice;
                            $products->Type=$Type;
                            $products->OrderStatus=$OrderStatus;
                            $products->Quantity=$Quantity;
                            $products->deliverydate=$request->deliverydate;
                            $products->ModeofPaymentrent=$ModeofPaymentrent;
                            $products->OrderStatusrent=$OrderStatusrent;
                            $products->ModeofPayment=$ModeofPayment;
                            $products->AdvanceAmt=$AdvanceAmt;
                            $products->StartDate=$StartDate;
                            $products->EndDate=$EndDate;
                            // $products->TransportCharges=$TransportCharges;
                            $products->OverdueAmt=$OverdueAmt;
                            $products->Requestcreatedby=$name1;
                            $products->Leadid=$leadid;
                            $products->save();

                            // dd($products);


                            $maxproductid2 = DB::table('products')->max('id');

                            $productid2=DB::table('products')->max('id');



                            // dd($productid,$productid2);
                            $prodlead=new prodlead;
                            $prodlead->leadid=$leadid;
                            $prodlead->Prodid=$productid2;
                            $prodlead->PharmacyId="1";
                            $prodlead->orderid=$orderid;
                            $prodlead->save();

                        }
                        else
                        {

                            $products=product::find($productid2);
                            $products->SKUid=$skudetail."\r\n".$SKUid;
                            $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                            // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                            $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                            $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                            $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                            $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                            // $products->Type=$Typedetail."\r\n".$Type;
                            // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                            $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                            // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                            // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                            // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                            $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                            $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                            $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                            $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;

                            $products->save();

                            $maxproductid2 = DB::table('products')->max('id');
                        }

                    }


                    // append all product in one row

                    /*


                    $products=product::find($productid);
                    $products->SKUid=$skudetail."\r\n".$SKUid;
                    $products->ProductName=$ProductNamedetail."\r\n".$ProductName;
                    // $products->DemoRequired=$DemoRequireddetail."\r\n".$DemoRequired;
                    $products->AvailabilityStatus=$AvailabilityStatusdetail."\r\n".$AvailabilityStatus;
                    $products->AvailabilityAddress=$AvailabilityAddressdetail."\r\n".$AvailabilityAddress;
                    $products->SellingPrice=$SellingPricedetail."\r\n".$SellingPrice;
                    $products->RentalPrice=$RentalPricedetail."\r\n".$RentalPrice;
                    // $products->Type=$Typedetail."\r\n".$Type;
                    // $products->OrderStatus=$OrderStatusdetail."\r\n".$OrderStatus;
                    $products->Quantity=$Quantitydetail."\r\n".$Quantity;
                    // $products->ModeofPaymentrent=$ModeofPaymentrentdetail."\r\n".$ModeofPaymentrent;
                    // $products->OrderStatusrent=$OrderStatusrentdetail."\r\n".$OrderStatusrent;
                    // $products->ModeofPayment=$ModeofPaymentdetail."\r\n".$ModeofPayment;
                    $products->AdvanceAmt=$AdvanceAmtdetail."\r\n".$AdvanceAmt;
                    $products->StartDate=$StartDatedetail."\r\n".$StartDate;
                    $products->EndDate=$EndDatedetail."\r\n".$EndDate;
                    $products->OverdueAmt=$OverdueAmtdetail."\r\n".$OverdueAmt;
                    $products->save();


                    */

                    //  $products = new product;
                    // $products->SKUid=$SKUid;
                    // $products->ProductName=$ProductName;
                    // $products->DemoRequired=$DemoRequired;
                    // $products->AvailabilityStatus=$AvailabilityStatus;
                    // $products->AvailabilityAddress=$AvailabilityAddress;
                    // $products->SellingPrice=$SellingPrice;
                    // $products->RentalPrice=$RentalPrice;
                    // $products->Type=$Type;
                    // $products->OrderStatus=$OrderStatus;
                    // $products->Quantity=$Quantity;
                    // $products->ModeofPaymentrent=$ModeofPaymentrent;
                    // $products->OrderStatusrent=$OrderStatusrent;
                    // $products->ModeofPayment=$ModeofPayment;
                    // $products->AdvanceAmt=$AdvanceAmt;
                    // $products->StartDate=$StartDate;
                    // $products->EndDate=$EndDate;
                    // $products->OverdueAmt=$OverdueAmt;
                    // $products->Leadid=$leadid;
                    // $products->save();

                    // $productid=DB::table('products')->max('id');


                    // $prodlead=new prodlead;
                    // $prodlead->leadid=$leadid;
                    // $prodlead->Prodid=$productid;
                    // $prodlead->PharmacyId="1";
                    // $prodlead->orderid=$orderid;
                    // $prodlead->save();


                }
            }

            // code for calculate sum of all products

            /* code for product data by jatin starts here */
            //take out the details from the product table for that particular lead id
            $detail=DB::table('products')->where('id',$productid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail1);

            //retrieving id for product

            // Converting all products from json to array
            $ProductName = $detail1[0]['ProductName'];
            // dd($Mednames);
            $k =explode("\r\n",$ProductName);



            $SKUid =$detail1[0]['SKUid'];
            $k0 = explode("\r\n",$SKUid);

            $DemoRequired =$detail1[0]['DemoRequired'];
            $k1 = $DemoRequired;


            $AvailabilityStatus =$detail1[0]['AvailabilityStatus'];
            $k2 =explode("\r\n",$AvailabilityStatus);


            $AvailabilityAddress =$detail1[0]['AvailabilityAddress'];
            $k3 =explode("\r\n",$AvailabilityAddress);

            $SellingPrice =$detail1[0]['SellingPrice'];
            $k4 =explode("\r\n",$SellingPrice);


            $Type =$detail1[0]['Type'];
            $k5 =$Type;




            $OrderStatus =$detail1[0]['OrderStatus'];
            $k6 =$OrderStatus;


            $Quantity =$detail1[0]['Quantity'];
            $k7 =explode("\r\n",$Quantity);

            $ModeofPaymentrent =$detail1[0]['ModeofPaymentrent'];
            $k8 =$ModeofPaymentrent;

            $OrderStatusrent =$detail1[0]['OrderStatusrent'];
            $k9 =$OrderStatusrent;

            $ModeofPayment =$detail1[0]['ModeofPayment'];
            $k10 =$ModeofPayment;

            $AdvanceAmt =$detail1[0]['AdvanceAmt'];
            $k11 =explode("\r\n",$AdvanceAmt);

            $StartDate =$detail1[0]['StartDate'];
            $k12 =explode("\r\n",$StartDate);

            $EndDate =$detail1[0]['EndDate'];
            $k13 =explode("\r\n",$EndDate);

            $OverdueAmt =$detail1[0]['OverdueAmt'];
            $k14 =explode("\r\n",$OverdueAmt);

            $RentalPrice =$detail1[0]['RentalPrice'];
            $k15 =explode("\r\n",$RentalPrice);

            $Receipt =$detail1[0]['Receipt'];


            $Cheque =$detail1[0]['Cheque'];

            $cashstatus =$detail1[0]['CashStatus'];

            $discount =$detail1[0]['Discount'];

            $apaid =$detail1[0]['AmountPaid'];

            $pdiscount =$detail1[0]['PostDiscountPrice'];

            $FinalAmount =$detail1[0]['FinalAmount'];



            $cnt2 = count($k);

            // dd($productid2,$productid);
            $cnt1 = 0;


            $sum = 0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {        if($k5=="Rent")
                {

                    $sum = $sum + $k15[$cnt1]*$k7[$cnt1];
                }
                else{
                    $sum = $sum + $k4[$cnt1]*$k7[$cnt1];
                }

            }



            $finalamt = product::find($productid);
            $finalamt->FinalAmount= $sum;
            $finalamt->save();

            // dd($productid2);


            // code end
        }



        if($c1!=NULL)
        {


            $pharmacy = pharmacy::find($pharmacyid);
            $pharmacy->MedName=NULL;
            $pharmacy->Strength=NULL;
            $pharmacy->MedType=NULL;
            $pharmacy->PQuantity=NULL;
            $pharmacy->PAvailabilityStatus=NULL;
            $pharmacy->Price=$request->NULL;
            // $pharmacy->POrderStatus=$pOrderStatus;
            $pharmacy->PModeofpayment=NULL;
            $pharmacy->QuantityType=NULL;
            $pharmacy->Pdeliverydate=NULL;
            $pharmacy->save();



            $pharmacy = pharmacy::find($pharmacyid);
            $pharmacy->MedName=$request->MedName;
            $pharmacy->Strength=$request->Strength;
            $pharmacy->MedType=$request->MedType;
            $pharmacy->PQuantity=$request->pQuantity;
            $pharmacy->PAvailabilityStatus=$request->pAvailabilityStatus;
            $pharmacy->Price=$request->pSellingPrice;
            $pharmacy->QuantityType=$request->QuantityType;
            // $pharmacy->POrderStatus=$pOrderStatus;
            $pharmacy->PModeofpayment=$request->PModeofpayment;
            $pharmacy->Pdeliverydate=$request->Pdeliverydate;
            $pharmacy->save();




            // Code for Product Logs starts here

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            // dd($leadid);

            // $activity = new Activity;
            // $activity->emp_id = $emp_id;
            // $activity->activity = "Lead Created for Pharmacy";
            // $activity->activity_time = new \DateTime();
            // $activity->log_id = $log_id;
            // $activity->lead_id = $leadid;
            // $activity->save();
        }





        if($no1!=NULL)
        {

            for($i=2;$i<=$no1;$i++)
            {

                $MedName1="MedName".$i;
                $Strength1="Strength".$i;
                $PQuantity1="pQuantity".$i;
                $MedType1="MedType".$i;
                $Price1="pSellingPrice".$i;
                $PAvailabilityStatus1="PAvailabilityStatus".$i;
                $POrderStatus1="POrderStatus".$i;
                $PModeofpayment1="PModeofpayment".$i;
                $QuantityType1="QuantityType".$i;
                // dd($request->$POrderStatus1);


                $MedName=$request->$MedName1;
                $Strength=$request->$Strength1;
                $PQuantity=$request->$PQuantity1;
                $QuantityType=$request->$QuantityType1;
                $MedType=$request->$MedType1;
                $Price=$request->$Price1;
                $PAvailabilityStatus=$request->$PAvailabilityStatus1;
                // $POrderStatus=$request->$POrderStatus1;
                $PModeofpayment=$request->$PModeofpayment1;

                // dd($QuantityType);

                $meddetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('MedName');
                $strengthdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('Strength');
                $typedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('MedType');
                $quantitydetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PQuantity');
                $quantitytypedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');
                $availablitydetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PAvailabilityStatus');
                $pricedetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('Price');
                $statusdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('POrderStatus');
                $paymentdetail=DB::table('pharmacies')->where('id',$pharmacyid)->value('PModeofpayment');


                if($pharmacyid == 1)
                {
                    $lead = lead::find($leadid);
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    //If the source field is not filled, then take the reference id of the selected Reference
                    //here, we are selecting any option except the "Other" option,  the the "Source" option beside the "Reference" field is NULL
                    if($request->source == Null)
                    {
                        //take out the value in the "reference" field, and assign the same value to "source" as well
                        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
                        $source=$refer;
                    }
                    //if the Other option is selected, then whatever is filled in the source field
                    //pass that to the "Source" field
                    else
                    {
                        $source=$request->source;
                    }

                    //fetch all the corresponding fields
                    $lead->fName=$request->clientfname;
                    $lead->mName=$request->clientmname;
                    $lead->lName=$request->clientlname;
                    $lead->EmailId=$request->clientemail;
                    $lead->Source=$source;
                    $lead->MobileNumber=$request->clientmob;
                    $lead->Alternatenumber=$request->clientalternateno;
                    $lead->EmergencyContact =$request->EmergencyContact;
                    $lead->AssesmentReq=$request->assesmentreq;
                    $lead->createdby=$request->loginname;
                    $lead->Referenceid=$ref;
                    $lead->empid=$empid;
                    $lead->save();


                    $address = address::find($addressid);

                    //fetch and store all the address details
                    $address->Address1=$request->Address1;
                    $address->Address2=$request->Address2;
                    $address->City=$request->City;
                    $address->District=$request->District;
                    $address->State=$request->State;
                    $address->PinCode=$request->PinCode;


                    $same1=$request->presentcontact;

                    if($same1=='same')
                    {
                        $address->PAddress1=$request->Address1;
                        $address->PAddress2=$request->Address2;
                        $address->PCity=$request->City;
                        $address->PDistrict=$request->District;
                        $address->PState=$request->State;
                        $address->PPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->PAddress1=$request->PAddress1;
                        $address->PAddress2=$request->PAddress2;
                        $address->PCity=$request->PCity;
                        $address->PDistrict=$request->PDistrict;
                        $address->PState=$request->PState;
                        $address->PPinCode=$request->PPinCode;
                    }

                    $same=$request->emergencycontact;
                    if($same=='same')
                    {
                        $address->EAddress1=$request->Address1;
                        $address->EAddress2=$request->Address2;
                        $address->ECity=$request->City;
                        $address->EDistrict=$request->District;
                        $address->EState=$request->State;
                        $address->EPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->EAddress1=$request->EAddress1;
                        $address->EAddress2=$request->EAddress2;
                        $address->ECity=$request->ECity;
                        $address->EDistrict=$request->EDistrict;
                        $address->EState=$request->EState;
                        $address->EPinCode=$request->EPinCode;
                    }

                    $same2=$request->shippingcontact;
                    if($same2=='same')
                    {
                        $address->SAddress1=$request->Address1;
                        $address->SAddress2=$request->Address2;
                        $address->SCity=$request->City;
                        $address->SDistrict=$request->District;
                        $address->SState=$request->State;
                        $address->SPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->SAddress1=$request->SAddress1;
                        $address->SAddress2=$request->SAddress2;
                        $address->SCity=$request->SCity;
                        $address->SDistrict=$request->SDistrict;
                        $address->SState=$request->SState;
                        $address->SPinCode=$request->SPinCode;
                    }


                    $address->leadid=$leadid;
                    $address->empid=$empid;
                    $address->save();

                    // Code to save Log when the existing client doesn't exist and the user creates a "New Lead"


                    //retrieving the session id of the person who is logged in
                    // $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                    //
                    // //retrieving the username of the presently logged in user
                    // $username = DB::table('logs')->where('session_id', session()->getId())->value('name');
                    //
                    // // retrieving the employee id of the presently logged in user
                    // $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                    //
                    // //retrieving the log id of the person who logged in just now
                    // $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');
                    //
                    //
                    // $activity = new Activity;
                    // $activity->emp_id = $emp_id;
                    // $activity->activity = "New Lead Created ";
                    // $activity->activity_time = new \DateTime();
                    // $activity->log_id = $log_id;
                    // $activity->lead_id = $leadid;
                    // $activity->save();






                    // dd($QuantityType);

                    $pharmacy =  pharmacy::find($pharmacyid);
                    $pharmacy->MedName=$MedName;
                    $pharmacy->Strength=$Strength;
                    $pharmacy->MedType=$MedType;
                    $pharmacy->PQuantity=$PQuantity;
                    $pharmacy->QuantityType=$QuantityType;
                    $pharmacy->PAvailabilityStatus=$PAvailabilityStatus;
                    $pharmacy->Price=$Price;
                    // $pharmacy->POrderStatus=$POrderStatus;
                    $pharmacy->PModeofpayment=$PModeofpayment;
                    $pharmacy->Pdeliverydate=$request->Pdeliverydate;
                    $pharmacy->save();


                    // Code for Product Logs starts here

                    //retrieving the session id of the person who is logged in
                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                    //retrieving the username of the presently logged in user
                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                    // retrieving the employee id of the presently logged in user
                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

                    //retrieving the log id of the person who logged in just now
                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                    // dd($leadid);

                    $activity = new Activity;
                    $activity->emp_id = $emp_id;
                    $activity->activity = "Lead Created for Pharmacy";
                    $activity->activity_time = new \DateTime();
                    $activity->log_id = $log_id;
                    $activity->lead_id = $leadid;
                    $activity->save();




                }
                else
                {


                    $pharmacy=pharmacy::find($pharmacyid);
                    $pharmacy->MedName=$meddetail."\r\n".$MedName;
                    $pharmacy->Strength=$strengthdetail."\r\n".$Strength;
                    $pharmacy->MedType=$typedetail."\r\n".$MedType;
                    $pharmacy->PQuantity=$quantitydetail."\r\n".$PQuantity;
                    $pharmacy->QuantityType=$quantitytypedetail."\r\n".$QuantityType;
                    $pharmacy->PAvailabilityStatus=$availablitydetail."\r\n".$PAvailabilityStatus;
                    $pharmacy->Price=$pricedetail."\r\n".$Price;
                    // $pharmacy->POrderStatus=$statusdetail."\r\n".$POrderStatus;
                    // $pharmacy->PModeofpayment=$paymentdetail."\r\n".$PModeofpayment;
                    $pharmacy->save();
                }



                // $prodlead=new prodlead;
                //     $prodlead->leadid=$leadid;
                //     $prodlead->Prodid="1";
                //     $prodlead->PharmacyId=$pharmacyid;
                //     $prodlead->orderid=$orderid;
                //     $prodlead->save();

            }

            /* code for pharmacy data by jatin starts here */
            //take out the details from the pharmacy table for that particular lead id
            $detail=DB::table('pharmacies')->where('id',$pharmacyid)->get();
            $detail1 = json_decode($detail,true);
            // dd($detail);


            //retrieving id for pharmacy

            // Converting all Mednames from json to array
            $Mednames = $detail1[0]['MedName'];
            // dd($Mednames);
            $k =explode("\r\n",$Mednames);


            $Strength =$detail1[0]  ['Strength'];
            $k1 = explode("\r\n",$Strength);


            $PQuantity =$detail1[0]['PQuantity'];
            $k2 =explode("\r\n",$PQuantity);


            $MedType =$detail1[0]['MedType'];
            $k3 =explode("\r\n",$MedType);

            $Price =$detail1[0]['Price'];
            $k4 =explode("\r\n",$Price);


            $PAvailabilityStatus =$detail1[0]['PAvailabilityStatus'];
            $k5 =explode("\r\n",$PAvailabilityStatus);


            $POrderStatus =$detail1[0]['POrderStatus'];
            $k6 =$POrderStatus;


            $PModeofpayment =$detail1[0]['PModeofpayment'];
            $k7 =$PModeofpayment;

            $Receipt =$detail1[0]['PReceipt'];

            $Cheque =$detail1[0]['PCheque'];

            $cashstatus =$detail1[0]['PCashStatus'];

            $discount =$detail1[0]['PDiscount'];

            $apaid =$detail1[0]['PAmountPaid'];

            $pdiscount =$detail1[0]['PPostDiscountPrice'];

            $PFinalAmount =$detail1[0]['PFinalAmount'];

            $Pdeliverydate =$detail1[0]['Pdeliverydate'];

            // dd($PFinalAmount);


            // $Quantities =$detail1[0]['PQuantity'];
            // $k3 = explode("\r\n",$Quantities);

            // $Quantities =$detail1[0]['PQuantity'];
            // $k4 = explode("\r\n",$Quantities);
            // dd($k1);
            // // $posd = NULL;
            //  $d = "Jatin\r\nSharma";
            // $posd = strpos($d,"\r\n");
            // dd($posd);

            $cnt2 = count($k);

            // dd($cnt4);
            $cnt1 = 0;


            $sum=0;

            for($cnt1=0,$i=0;$cnt1<$cnt2;$cnt1++)
            {
                $sum = $sum + $k4[$cnt1]*$k2[$cnt1];
            }

            $ph=pharmacy::find($pharmacyid);
            $ph->PFinalAmount=$sum;
            $ph->save();

        }












        $desig=DB::table('employees')->where('FirstName',$name1)->value('Designation');
        $desig2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
        $desig3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');


        if($desig=="Marketing")
        {
            session()->put('name',$name1);
            return redirect('/product');
        }
        if($desig=="Customer Care")
        {
            session()->put('name',$name1);
            return redirect('/product');
        }
        else
        {
            if($desig2=="Pharmacy Manager")
            {
                session()->put('name',$name1);
                return redirect('/product');
            }
            else
            {
                if($desig2=="Product Manager")
                {
                    session()->put('name',$name1);
                    return redirect('/product');
                }
                else
                {
                    if($desig=="Admin")
                    {
                        session()->put('name',$name1);
                        return redirect('/product');
                    }
                    else
                    {
                        if($desig=="Management")
                        {
                            session()->put('name',$name1);
                            return redirect('/product');
                        }
                        else
                        {
                            if($desig=="Vertical Head")
                            {
                                session()->put('name',$name1);
                                return redirect('/product');
                            }
                            else
                            {
                                if($desig3=="Field Executive")
                                {
                                    session()->put('name',$name1);
                                    return redirect('/product');
                                }
                                else
                                {
                                    if($desig3=="Field Officer")
                                    {
                                        session()->put('name',$name1);
                                        return redirect('/product');
                                    }
                                    else

                                    {
                                        session()->put('name',$name1);
                                        return redirect('/product');
                                    }
                                }
                            }
                        }

                    }

                }

            }



        }

    }

    public function pharmacyedit()
    {
        $pdept = "Pharmacy";
        $pharmacyid = $_GET['id'];

        $value = "Pharmacy";
        // $pharmacyid = $leadid;
        $logged_in_user= Auth::guard('admin')->user()->name;

        $name = $logged_in_user;

        if(Auth::guard('admin')->check())
        {

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp1=DB::table('employees')->where('Designation2','Vertical Head')->get();
            $products=DB::table('productdetails')->get();


            $empid=DB::table('employees')->where('FirstName',$name)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            // dd($eid);
            $Designation2=DB::table('employees')->where('id',$eid)->value('Department2');
            $Designation=DB::table('employees')->where('id',$eid)->value('Designation');
            $Department=DB::table('employees')->where('id',$eid)->value('Department');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);


            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            $leadid = DB::table('pharmacies')->where('id',$pharmacyid)->value('leadid');

            $referenceid = DB::table('leads')->where('id',$leadid)->value('Referenceid');

            $referencedetails = DB::table('refers')->where('id',$referenceid)->value('Reference');



            // dd($products);

            $leaddetails = lead::find($leadid);

            $addressid = DB::table('addresses')->where('leadid',$leadid)->value('id');
            // dd($addressid);
            $addressdetails = address::find($addressid);
            // dd($addressdetails);

            $pharmacyall = DB::table('pharmacies')->where('id',$pharmacyid)->get();

            // dd($pharmacyall);

            $QuantityType = DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');

            $MedType = DB::table('pharmacies')->where('id',$pharmacyid)->value('MedType');


            $MedType_array = explode("\r\n",$MedType);

            $MedType_count = count($MedType_array);

            $pharmacy=pharmacy::find($pharmacyid);

            if($QuantityType==null)
            {
                for($i=0;$i<$MedType_count;$i++)
                {
                    if($MedType_array[$i]=="Tablet")
                    {
                        $old_QuantityType = DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');
                        // dd($old_QuantityType);
                        $pharmacy->QuantityType = $old_QuantityType."Tablet\r\n";
                        $pharmacy->save();
                    }
                    else
                    {
                        $old_QuantityType = DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');
                        // dd($old_QuantityType);
                        $pharmacy->QuantityType = $old_QuantityType."Stripes\r\n";
                        $pharmacy->save();
                    }
                }
            }


            $medName = DB::table('pharmacies')->where('id',$pharmacyid)->value('MedName');
            $Strength = DB::table('pharmacies')->where('id',$pharmacyid)->value('Strength');
            $MedType = DB::table('pharmacies')->where('id',$pharmacyid)->value('MedType');
            $PQuantity = DB::table('pharmacies')->where('id',$pharmacyid)->value('PQuantity');
            $QuantityType = DB::table('pharmacies')->where('id',$pharmacyid)->value('QuantityType');
            // dd($QuantityType);
            $PAvailabilityStatus = DB::table('pharmacies')->where('id',$pharmacyid)->value('PAvailabilityStatus');
            $Price = DB::table('pharmacies')->where('id',$pharmacyid)->value('Price');
            $PModeofpayment = DB::table('pharmacies')->where('id',$pharmacyid)->value('PModeofpayment');


            $medName_array = explode("\r\n",$medName);
            $Strength_array = explode("\r\n",$Strength);
            $MedType_array = explode("\r\n",$MedType);
            $PQuantity_array = explode("\r\n",$PQuantity);
            $QuantityType_array = explode("\r\n",$QuantityType);
            $PAvailabilityStatus_array = explode("\r\n",$PAvailabilityStatus);
            $Price_array = explode("\r\n",$Price);

            // dd($QuantityType_array);

            // dd($QuantityType_count);
            // dd($MedType_count);




            $product_count = count($medName_array);
            $Type=NULL;
            // dd($value);
            $productid = $pharmacyid;
            // dd($QuantityType_array);




            $adminid = DB::table('admins')->where('name',$name)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');


            $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

            $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');

            $user_department=DB::table('employees')->where('FirstName',$name)->value('Department');

            $check_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
            $fetch_city=DB::table('addresses')->where('leadid',$leadid)->value('City');

            if($designation=="Admin" || $designation== "Management" || ($designation=="Branch Head" && $check_city==$fetch_city) || $designation2=="Pharmacy Manager")
            {
                return view('product/editproduct',compact('leadid','productid','value','leaddetails','count','country_codes','reference','count1','indian_cities','products','Designation','Designation2'
                ,'Department','addressdetails','referencedetails','pdept','medName_array','Strength_array','MedType_array','PQuantity_array','PAvailabilityStatus_array','Price_array','PModeofpayment','product_count','Type','QuantityType_array'));
            }
            else
            {
                return redirect('/admin');
            }





        }

    }

}
