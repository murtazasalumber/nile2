<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Response;


class BranchHeadController extends Controller
{
     	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('branch_head');
    }

    public function index()
    {
        if(Auth::guard('admin')->check())
        {
        $data = DB::table('cities')->get();
        // dd($data);
        $designation="management";

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        // dd($user_city);
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        //Code for displaying the counts the Admin Page for different statuses
        $newcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status0)
        ->where('Branch',$user_city)
        ->count();

        $inprogresscountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status1)
        ->where('Branch',$user_city)
        ->count();

        $convertedcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status2)
        ->where('Branch',$user_city)
        ->count();

        $droppedcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status3)
        ->where('Branch',$user_city)
        ->count();

        $deferredcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status4)
        ->where('Branch',$user_city)
        ->count();

        //Counting the number of verticals in Health Heal
        $verticals_count = DB::table('employees')->where('Designation','Vertical Head')->where('city',$user_city)->count();
        // dd($verticals_count);

        //Extract the names of all the Vertical Heads
        $vertical_names =  DB::table('employees')->select('FirstName')->where('Designation','Vertical Head')->where('city',$user_city)->get();



        //converting the names of all Verticals into an Array
        $vertical_names = json_decode($vertical_names,true);
        // dd($vertical_names);
        // dd($vertical_names);

        //Initalizing Vertical_names array with Null
        $Vertical_names = null;

        $branch_head_names =  DB::table('employees')->select('FirstName')->where('Designation','Branch Head')->where('city',$user_city)->get();

        $branch_head_names = json_decode($branch_head_names,true);

        $branch_head_count = count($branch_head_names);

        // dd($branch_head_count);

        $totalcount = $verticals_count + $branch_head_count;
        // dd($totalcount);


        //Extracting the vertical names from the above array and storing it in a 1-D array
        for($i=0;$i<$verticals_count;$i++)
        {
            $Vertical_names[] = $vertical_names[$i]['FirstName'];
        }
        //
        // for($i=0;$i<$branch_head_count;$i++)
        // {
        //     $Vertical_names[] = $branch_head_names[$i]['FirstName'];
        // }

        // dd($Vertical_cities);

        // dd($Vertical_names);

        $city_name = null;
        $servicetype_name = null;

        //For every Vertical, Calculating the individual counts for their statuses
        for($j=0;$j<$verticals_count;$j++)
        {
            /* Retrieving the counts of all the users -- started */
            //assigning the possible statuses for the Coordinator

            $status0 = "New";
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            //extracting the name of the presently accessed Vertical
            $vertical = $Vertical_names[$j];
            // dd($Vertical_names);

            $dept = DB::table('employees')->where('FirstName',$vertical)->value('Department');
            $designation1 = DB::table('employees')->where('FirstName',$vertical)->value('Designation');

            $user_city = DB::table('employees')->where('FirstName',$vertical)->value('city');


            $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $city_name[] = $user_city;
            $servicetype_name[] = $dept;


             //assigning default value as "NULL" for all variables

            $newcount2 = $inprogresscount2 = $convertedcount2 = $droppedcount2 = $deferredcount2 = NULL;



            // dd($Vertical_cities[0]);
            if($user_city=="Bengaluru")
            {
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Pune")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Chennai")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Hyderabad")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Hubballi-Dharwad")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }



            $statuscounts[] = array
            (
                "0" => $newcount2,"1"=>$inprogresscount2, "2" =>$convertedcount2,"3"=>$droppedcount2,"4"=>$deferredcount2
            );
            // dd($ServiceTypearray);
        }

        // dd($statuscounts);
        // dd($verticals_count);
        // dd($Vertical_names);

        // Fetching Product count details to show on dashboard

//retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $designation="productmanager";

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";
        // dd($check);

         $newcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();


             $newcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount=count($newcountamount);

            $newcountamount=json_decode($newcountamount,true);

            $newsum=0;
            for($i=0;$i<$countamount;$i++)
            {
                $newsum+= $newcountamount[$i]['FinalAmount'];
            }




            $processingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('leads.id', 'DESC')
            ->count();


                 $processingcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount1=count($processingcountamount);

            $processingcountamount=json_decode($processingcountamount,true);

            $processingsum=0;
            for($i=0;$i<$countamount1;$i++)
            {
                $processingsum+= $processingcountamount[$i]['FinalAmount'];
            }


            $awaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $awaitingpickupcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();


            $countamount2=count($awaitingpickupcountamount);

            $awaitingpickupcountamount=json_decode($awaitingpickupcountamount,true);

            $awaitingpickupsum=0;
            for($i=0;$i<$countamount2;$i++)
            {
                $awaitingpickupsum+= $awaitingpickupcountamount[$i]['FinalAmount'];
            }



            $outfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $outfordeliverycountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount3=count($outfordeliverycountamount);

            $outfordeliverycountamount=json_decode($outfordeliverycountamount,true);

            $outfordeliverysum=0;
            for($i=0;$i<$countamount3;$i++)
            {
                $outfordeliverysum+= $outfordeliverycountamount[$i]['FinalAmount'];
            }




            $readytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $readytoshipcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount4=count($readytoshipcountamount);

            $readytoshipcountamount=json_decode($readytoshipcountamount,true);

            $readytoshipsum=0;
            for($i=0;$i<$countamount4;$i++)
            {
                $readytoshipsum+= $readytoshipcountamount[$i]['FinalAmount'];
            }



            $orderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $orderreturncountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount5=count($orderreturncountamount);

            $orderreturncountamount=json_decode($orderreturncountamount,true);

            $orderreturnsum=0;
            for($i=0;$i<$countamount5;$i++)
            {
                $orderreturnsum+= $orderreturncountamount[$i]['FinalAmount'];
            }

            $recievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $recievedorderreturncountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount30=count($recievedorderreturncountamount);

            $recievedorderreturncountamount=json_decode($recievedorderreturncountamount,true);

            $recievedorderreturnsum=0;
            for($i=0;$i<$countamount30;$i++)
            {
                $recievedorderreturnsum+= $recievedorderreturncountamount[$i]['FinalAmount'];
            }





            $canceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $canceledcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount6=count($canceledcountamount);

            $canceledcountamount=json_decode($canceledcountamount,true);

            $canceledsum=0;
            for($i=0;$i<$countamount6;$i++)
            {
                $canceledsum+= $canceledcountamount[$i]['FinalAmount'];
            }




            $deliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $deliveredcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount7=count($deliveredcountamount);

            $deliveredcountamount=json_decode($deliveredcountamount,true);

            $deliveredsum=0;
            for($i=0;$i<$countamount7;$i++)
            {
                $deliveredsum+= $deliveredcountamount[$i]['FinalAmount'];
            }





            $newcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                 $newcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount8=count($newcountrentamount);

            $newcountrentamount=json_decode($newcountrentamount,true);

            $newrentsum=0;
            for($i=0;$i<$countamount8;$i++)
            {
                $newrentsum+= $newcountrentamount[$i]['FinalAmount'];
            }





            $processingcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $processingcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();
            // dd($processingcountrentamount);

             $countamount9=count($processingcountrentamount);

            $processingcountrentamount=json_decode($processingcountrentamount,true);

            $processingrentsum=0;
            for($i=0;$i<$countamount9;$i++)
            {
                $processingrentsum+= $processingcountrentamount[$i]['FinalAmount'];
            }


            $awaitingpickupcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $awaitingpickupcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount10=count($awaitingpickupcountrentamount);

            $awaitingpickupcountrentamount=json_decode($awaitingpickupcountrentamount,true);

            $awaitingpickuprentsum=0;
            for($i=0;$i<$countamount10;$i++)
            {
                $awaitingpickuprentsum+= $awaitingpickupcountrentamount[$i]['FinalAmount'];
            }


// dd($awaitingpickupcountrentamount);



            $outfordeliverycountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $outfordeliverycountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount11=count($outfordeliverycountrentamount);

            $outfordeliverycountrentamount=json_decode($outfordeliverycountrentamount,true);

            $outfordeliveryrentsum=0;
            for($i=0;$i<$countamount11;$i++)
            {
                $outfordeliveryrentsum+= $outfordeliverycountrentamount[$i]['FinalAmount'];
            }






            $readytoshipcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $readytoshipcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount12=count($readytoshipcountrentamount);

            $readytoshipcountrentamount=json_decode($readytoshipcountrentamount,true);

            $readytoshiprentsum=0;
            for($i=0;$i<$countamount12;$i++)
            {
                $readytoshiprentsum+= $readytoshipcountrentamount[$i]['FinalAmount'];
            }








            $orderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $orderreturncountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount13=count($orderreturncountrentamount);

            $orderreturncountrentamount=json_decode($orderreturncountrentamount,true);

            $orderreturnrentsum=0;
            for($i=0;$i<$countamount13;$i++)
            {
                $orderreturnrentsum+= $orderreturncountrentamount[$i]['FinalAmount'];
            }
// dd($orderreturncountrentamount);


            $recievedorderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $recievedorderreturncountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount26=count($recievedorderreturncountrentamount);

            $recievedorderreturncountrentamount=json_decode($recievedorderreturncountrentamount,true);

            $recievedorderreturnrentsum=0;
            for($i=0;$i<$countamount26;$i++)
            {
                $recievedorderreturnrentsum+= $recievedorderreturncountrentamount[$i]['FinalAmount'];
            }






            $canceledcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $canceledcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount14=count($canceledcountrentamount);

            $canceledcountrentamount=json_decode($canceledcountrentamount,true);

            $canceledrentsum=0;
            for($i=0;$i<$countamount14;$i++)
            {
                $canceledrentsum+= $canceledcountrentamount[$i]['FinalAmount'];
            }






            $deliveredcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $deliveredcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount15=count($deliveredcountrentamount);

            $deliveredcountrentamount=json_decode($deliveredcountrentamount,true);

            $deliveredrentsum =0;
            for($i=0;$i<$countamount15;$i++)
            {
                $deliveredrentsum += $deliveredcountrentamount[$i]['FinalAmount'];
            }







        // product count end



            // Fetching pharmacy details count


                $status0 = "New";
         $status1 = "Processing";
         $status2  = "Awaiting Pickup";
         $status3  = "Out for Delivery";
         $status4  = "Ready to ship";
         $status5  = "Order Return";
         $status6  = "Cancelled";
         $status7  = "Delivered";
         $status8  = "Received Order Return";

         $pnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

            $pnewcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount16=count($pnewcountamount);

            $pnewcountamount=json_decode($pnewcountamount,true);

            $pnewsum =0;
            for($i=0;$i<$countamount16;$i++)
            {
                $pnewsum += $pnewcountamount[$i]['PFinalAmount'];
            }






         $pprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pprocessingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount17=count($pprocessingcountamount);

            $pprocessingcountamount=json_decode($pprocessingcountamount,true);

            $pprocessingsum=0;
            for($i=0;$i<$countamount17;$i++)
            {
                $pprocessingsum += $pprocessingcountamount[$i]['PFinalAmount'];
            }








         $pawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pawaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount18=count($pawaitingpickupcountamount);

            $pawaitingpickupcountamount=json_decode($pawaitingpickupcountamount,true);

            $pawaitingpickupsum =0;
            for($i=0;$i<$countamount18;$i++)
            {
                $pawaitingpickupsum += $pawaitingpickupcountamount[$i]['PFinalAmount'];
            }






         $poutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $poutfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount19=count($poutfordeliverycountamount);

            $poutfordeliverycountamount=json_decode($poutfordeliverycountamount,true);

            $poutfordeliverysum =0;
            for($i=0;$i<$countamount19;$i++)
            {
                $poutfordeliverysum += $poutfordeliverycountamount[$i]['PFinalAmount'];
            }







         $preadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $preadytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount20=count($preadytoshipcountamount);

            $preadytoshipcountamount=json_decode($preadytoshipcountamount,true);

            $preadytoshipsum =0;
            for($i=0;$i<$countamount20;$i++)
            {
                $preadytoshipsum += $preadytoshipcountamount[$i]['PFinalAmount'];
            }








         $porderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $porderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount21=count($porderreturncountamount);

            $porderreturncountamount=json_decode($porderreturncountamount,true);

            $porderreturnsum =0;
            for($i=0;$i<$countamount21;$i++)
            {
                $porderreturnsum += $porderreturncountamount[$i]['PFinalAmount'];
            }


             $precievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $precievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount31=count($precievedorderreturncountamount);

            $precievedorderreturncountamount=json_decode($precievedorderreturncountamount,true);

            $precievedorderreturnsum =0;
            for($i=0;$i<$countamount31;$i++)
            {
                $precievedorderreturnsum += $precievedorderreturncountamount[$i]['PFinalAmount'];
            }







         $pcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pcanceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount22=count($pcanceledcountamount);

            $pcanceledcountamount=json_decode($pcanceledcountamount,true);

            $pcanceledsum =0;
            for($i=0;$i<$countamount22;$i++)
            {
                $pcanceledsum += $pcanceledcountamount[$i]['PFinalAmount'];
            }







         $pdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pdeliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount23=count($pdeliveredcountamount);

            $pdeliveredcountamount=json_decode($pdeliveredcountamount,true);

            $pdeliveredsum =0;
            for($i=0;$i<$countamount23;$i++)
            {
                $pdeliveredsum += $pdeliveredcountamount[$i]['PFinalAmount'];
            }

            // dd($data);




            // pharmacy end



            // dd($Vertical_names);

        //returning all the above code for individual counts and dynamic counts onto the Admin dashboard
        return view('admin.BranchHead',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
            'deferredcountforall','verticals_count','Vertical_names','statuscounts','designation','city_name','servicetype_name','newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','newcountrent','processingcountrent','awaitingpickupcountrent','outfordeliverycountrent','readytoshipcountrent','orderreturncountrent','canceledcountrent','deliveredcountrent','pnewcount','pprocessingcount','pawaitingpickupcount','poutfordeliverycount','preadytoshipcount','porderreturncount','pcanceledcount','pdeliveredcount','newsum','processingsum','awaitingpickupsum','outfordeliverysum','readytoshipsum','orderreturnsum','canceledsum','deliveredsum','newrentsum','processingrentsum','awaitingpickuprentsum','outfordeliveryrentsum','readytoshiprentsum','orderreturnrentsum','canceledrentsum','deliveredrentsum','pnewsum','pprocessingsum','pawaitingpickupsum','poutfordeliverysum','preadytoshipsum','porderreturnsum','pcanceledsum','pdeliveredsum','precievedorderreturncount','recievedorderreturncountrent','recievedorderreturncount','recievedorderreturnsum','recievedorderreturnrentsum','precievedorderreturnsum'));


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

       public function assigned()
    {
        //If the status is present in the URL extract from there or retrieve it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        $logged_in_user = Auth::guard('admin')->user()->name;

        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');


        $leads = DB::table('services')
        ->select('services.*','leads.*')
        ->join('leads','leads.id','=','services.Leadid')
        ->where('ServiceStatus',$status)
        ->where('Branch',$city)
        ->orderBy('leads.id','DESC')
        ->paginate(50);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //Check if the status is "download" as passed in the management/index.blade.php file
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            // Check if the status is All (this is when the user clicks on the View Leads buttton and wishes to download the data from there
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('Branch',$city)
                ->orderBy('leads.id', 'DESC')
                ->get();

                $leads = json_decode($leads,true);

                // dd($leads);

                //create a separate array to take care of the column names to be show in the CSV file
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }
            //if the status is returned as "New/In Progress etc", then run this (This is the case where the user clicks on the Individual Counts and wishes to Download the CSV for that view)
            else
            {
                // dd($status);
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('ServiceStatus',$status)
                ->where('Branch',$city)
                ->orderBy('leads.id','DESC')
                ->get();

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }


        session()->put('name',$logged_in_user);

        return view('BranchHead.index',compact('leads'));
    }



            // Product onclick function


    public function productassigned()
    {
        //if the status is present in the URL get it from there, else get it from the session
        $type=$_GET['type'];
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;


         $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        if($type=="Selling")
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->where('addresses.City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        else
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->where('addresses.City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if($type!="All")
        {
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;


                if($type=="Selling")
                {

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('addresses.City',$city)
                    ->where('products.Type','Sell')
                    ->orwhere('products.Type','')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('addresses.City',$city)
                    ->where('products.Type','Rent')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                if($type=="Selling")
                {
                     //dd($type);
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatus',$status)
                    ->where('addresses.City',$city)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatusrent',$status)
                    ->where('addresses.City',$city)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Reference', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }
    }else
        {
            // dd("sd");





                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','orders.id','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice','prodleads.comments')
                      ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                      ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                      ->join('products', 'prodleads.Prodid', '=', 'products.id')
                      ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                      ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                    ->where('addresses.City',$city)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

// dd($leads);


                     $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Reference', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','order ID','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice'
,'Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);


        }

        session()->put('name',$logged_in_user);

        return view('BranchHead.productindex',compact('leads'));
    }




    // pharmacy assigned function



     public function pharmacyassigned()
     {
         //if the status is present in the URL get it from there, else get it from the session
         if(isset($_GET['status']))
         {
             $status = $_GET['status'];
         }
         else
         {
             $status = session()->get('status');
         }
            //  dd($status);

             session()->put('status',$status);
             //retrieving the name of the logged in user
             $logged_in_user = Auth::guard('admin')->user()->name;

         $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

             $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
            ->where('POrderStatus',$status)
            ->where('addresses.City',$city)
            ->orderBy('pharmacies.id', 'DESC')
            ->paginate(50);

            /*
            Logic for downloading CSV goes here -- starts here
            */
            //if the link generated from co/index.blade.php sets "download==true", run this
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->where('addresses.City',$city)
                    ->orderBy('pharmacies.id', 'DESC')

                    ->get();


                    $leads = json_decode($leads,true);

                    // dd($leads);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');


                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->where('addresses.City',$city)
                    ->where('POrderStatus',$status)
                    ->orderBy('pharmacies.id', 'DESC')
                    ->get();
                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');

                    // $list = array (
                    //     $leads
                    // );
                    //
                    //         $lists = array (
                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                    // array('123', '456', '789'),
                    // array('aaa', 'bbb')
                    // );
                    // dd($list);

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);
                }
                /*
                Logic for downloading CSV goes here -- ends here
                */
            }


         session()->put('name',$logged_in_user);

         return view('BranchHead.pharmacyindex',compact('leads'));
 }



   public function mobilecount()
    {
              
        //assigning the possible status for the All



         if(Auth::guard('admin')->check())
        {
        $data = DB::table('cities')->get();
        // dd($data);
        $designation="Branch Head";

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

// dd($logged_in_user);
        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        // dd($user_city);
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        //Code for displaying the counts the Admin Page for different statuses
        $newcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status0)
        ->where('Branch',$user_city)
        ->count();

        $inprogresscountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status1)
        ->where('Branch',$user_city)
        ->count();

        $convertedcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status2)
        ->where('Branch',$user_city)
        ->count();

        $droppedcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status3)
        ->where('Branch',$user_city)
        ->count();

        $deferredcountforall = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        ->where('ServiceStatus',$status4)
        ->where('Branch',$user_city)
        ->count();
// dd($newcountforall);
        //Counting the number of verticals in Health Heal
        $verticals_count = DB::table('employees')->where('Designation','Vertical Head')->where('city',$user_city)->count();
        // dd($verticals_count);

        //Extract the names of all the Vertical Heads
        $vertical_names =  DB::table('employees')->select('FirstName')->where('Designation','Vertical Head')->where('city',$user_city)->get();



        //converting the names of all Verticals into an Array
        $vertical_names = json_decode($vertical_names,true);
        // dd($vertical_names);
        // dd($vertical_names);

        //Initalizing Vertical_names array with Null
        $Vertical_names = null;

        $branch_head_names =  DB::table('employees')->select('FirstName')->where('Designation','Branch Head')->where('city',$user_city)->get();

        $branch_head_names = json_decode($branch_head_names,true);

        $branch_head_count = count($branch_head_names);

        // dd($branch_head_count);

        $totalcount = $verticals_count + $branch_head_count;
        // dd($totalcount);


        //Extracting the vertical names from the above array and storing it in a 1-D array
        for($i=0;$i<$verticals_count;$i++)
        {
            $Vertical_names[] = $vertical_names[$i]['FirstName'];
        }
        //
        // for($i=0;$i<$branch_head_count;$i++)
        // {
        //     $Vertical_names[] = $branch_head_names[$i]['FirstName'];
        // }

        // dd($Vertical_cities);

        // dd($Vertical_names);

        $city_name = null;
        $servicetype_name = null;

        //For every Vertical, Calculating the individual counts for their statuses
        for($j=0;$j<$verticals_count;$j++)
        {
            /* Retrieving the counts of all the users -- started */
            //assigning the possible statuses for the Coordinator

            $status0 = "New";
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            //extracting the name of the presently accessed Vertical
            $vertical = $Vertical_names[$j];
            // dd($Vertical_names);

            $dept = DB::table('employees')->where('FirstName',$vertical)->value('Department');
            $designation1 = DB::table('employees')->where('FirstName',$vertical)->value('Designation');

            $user_city = DB::table('employees')->where('FirstName',$vertical)->value('city');


            $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $city_name[] = $user_city;
            $servicetype_name[] = $dept;


             //assigning default value as "NULL" for all variables

            $newcount2 = $inprogresscount2 = $convertedcount2 = $droppedcount2 = $deferredcount2 = NULL;



            // dd($Vertical_cities[0]);
            if($user_city=="Bengaluru")
            {
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Pune")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Chennai")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Hyderabad")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }
            else if($user_city=="Hubballi-Dharwad")
            {
                $vertical1=null;
                // dd($dept);
                if($dept=="Nursing Services")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($dept=="Personal Supportive Care")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($dept=="Mathrutvam - Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($dept=="Physiotherapy - Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }
                // dd($vertical1);
                $newcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('id', 'DESC')
                ->count();
            }



            $statuscounts[] = array
            (
                "0" => $newcount2,"1"=>$inprogresscount2, "2" =>$convertedcount2,"3"=>$droppedcount2,"4"=>$deferredcount2
            );
            // dd($ServiceTypearray);
        }

        // dd($statuscounts);
        // dd($verticals_count);
        // dd($Vertical_names);

        // Fetching Product count details to show on dashboard

//retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $designation="productmanager";

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";
        // dd($check);

         $newcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();


             $newcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount=count($newcountamount);

            $newcountamount=json_decode($newcountamount,true);

            $newsum=0;
            for($i=0;$i<$countamount;$i++)
            {
                $newsum+= $newcountamount[$i]['FinalAmount'];
            }




            $processingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('leads.id', 'DESC')
            ->count();


                 $processingcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount1=count($processingcountamount);

            $processingcountamount=json_decode($processingcountamount,true);

            $processingsum=0;
            for($i=0;$i<$countamount1;$i++)
            {
                $processingsum+= $processingcountamount[$i]['FinalAmount'];
            }


            $awaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $awaitingpickupcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();


            $countamount2=count($awaitingpickupcountamount);

            $awaitingpickupcountamount=json_decode($awaitingpickupcountamount,true);

            $awaitingpickupsum=0;
            for($i=0;$i<$countamount2;$i++)
            {
                $awaitingpickupsum+= $awaitingpickupcountamount[$i]['FinalAmount'];
            }



            $outfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $outfordeliverycountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount3=count($outfordeliverycountamount);

            $outfordeliverycountamount=json_decode($outfordeliverycountamount,true);

            $outfordeliverysum=0;
            for($i=0;$i<$countamount3;$i++)
            {
                $outfordeliverysum+= $outfordeliverycountamount[$i]['FinalAmount'];
            }




            $readytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $readytoshipcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount4=count($readytoshipcountamount);

            $readytoshipcountamount=json_decode($readytoshipcountamount,true);

            $readytoshipsum=0;
            for($i=0;$i<$countamount4;$i++)
            {
                $readytoshipsum+= $readytoshipcountamount[$i]['FinalAmount'];
            }



            $orderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $orderreturncountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount5=count($orderreturncountamount);

            $orderreturncountamount=json_decode($orderreturncountamount,true);

            $orderreturnsum=0;
            for($i=0;$i<$countamount5;$i++)
            {
                $orderreturnsum+= $orderreturncountamount[$i]['FinalAmount'];
            }

            $recievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $recievedorderreturncountamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount30=count($recievedorderreturncountamount);

            $recievedorderreturncountamount=json_decode($recievedorderreturncountamount,true);

            $recievedorderreturnsum=0;
            for($i=0;$i<$countamount30;$i++)
            {
                $recievedorderreturnsum+= $recievedorderreturncountamount[$i]['FinalAmount'];
            }





            $canceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $canceledcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount6=count($canceledcountamount);

            $canceledcountamount=json_decode($canceledcountamount,true);

            $canceledsum=0;
            for($i=0;$i<$countamount6;$i++)
            {
                $canceledsum+= $canceledcountamount[$i]['FinalAmount'];
            }




            $deliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $deliveredcountamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount7=count($deliveredcountamount);

            $deliveredcountamount=json_decode($deliveredcountamount,true);

            $deliveredsum=0;
            for($i=0;$i<$countamount7;$i++)
            {
                $deliveredsum+= $deliveredcountamount[$i]['FinalAmount'];
            }





            $newcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                 $newcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status0)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount8=count($newcountrentamount);

            $newcountrentamount=json_decode($newcountrentamount,true);

            $newrentsum=0;
            for($i=0;$i<$countamount8;$i++)
            {
                $newrentsum+= $newcountrentamount[$i]['FinalAmount'];
            }





            $processingcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                     $processingcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status1)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();
            // dd($processingcountrentamount);

             $countamount9=count($processingcountrentamount);

            $processingcountrentamount=json_decode($processingcountrentamount,true);

            $processingrentsum=0;
            for($i=0;$i<$countamount9;$i++)
            {
                $processingrentsum+= $processingcountrentamount[$i]['FinalAmount'];
            }


            $awaitingpickupcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $awaitingpickupcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status2)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount10=count($awaitingpickupcountrentamount);

            $awaitingpickupcountrentamount=json_decode($awaitingpickupcountrentamount,true);

            $awaitingpickuprentsum=0;
            for($i=0;$i<$countamount10;$i++)
            {
                $awaitingpickuprentsum+= $awaitingpickupcountrentamount[$i]['FinalAmount'];
            }


// dd($awaitingpickupcountrentamount);



            $outfordeliverycountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $outfordeliverycountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status3)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount11=count($outfordeliverycountrentamount);

            $outfordeliverycountrentamount=json_decode($outfordeliverycountrentamount,true);

            $outfordeliveryrentsum=0;
            for($i=0;$i<$countamount11;$i++)
            {
                $outfordeliveryrentsum+= $outfordeliverycountrentamount[$i]['FinalAmount'];
            }






            $readytoshipcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $readytoshipcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status4)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount12=count($readytoshipcountrentamount);

            $readytoshipcountrentamount=json_decode($readytoshipcountrentamount,true);

            $readytoshiprentsum=0;
            for($i=0;$i<$countamount12;$i++)
            {
                $readytoshiprentsum+= $readytoshipcountrentamount[$i]['FinalAmount'];
            }








            $orderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $orderreturncountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status5)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount13=count($orderreturncountrentamount);

            $orderreturncountrentamount=json_decode($orderreturncountrentamount,true);

            $orderreturnrentsum=0;
            for($i=0;$i<$countamount13;$i++)
            {
                $orderreturnrentsum+= $orderreturncountrentamount[$i]['FinalAmount'];
            }
// dd($orderreturncountrentamount);


            $recievedorderreturncountrent =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $recievedorderreturncountrentamount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status8)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount26=count($recievedorderreturncountrentamount);

            $recievedorderreturncountrentamount=json_decode($recievedorderreturncountrentamount,true);

            $recievedorderreturnrentsum=0;
            for($i=0;$i<$countamount26;$i++)
            {
                $recievedorderreturnrentsum+= $recievedorderreturncountrentamount[$i]['FinalAmount'];
            }






            $canceledcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                        $canceledcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status6)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

             $countamount14=count($canceledcountrentamount);

            $canceledcountrentamount=json_decode($canceledcountrentamount,true);

            $canceledrentsum=0;
            for($i=0;$i<$countamount14;$i++)
            {
                $canceledrentsum+= $canceledcountrentamount[$i]['FinalAmount'];
            }






            $deliveredcountrent = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

                    $deliveredcountrentamount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status7)
            ->where('addresses.City',$user_city)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $countamount15=count($deliveredcountrentamount);

            $deliveredcountrentamount=json_decode($deliveredcountrentamount,true);

            $deliveredrentsum =0;
            for($i=0;$i<$countamount15;$i++)
            {
                $deliveredrentsum += $deliveredcountrentamount[$i]['FinalAmount'];
            }







        // product count end



            // Fetching pharmacy details count


                $status0 = "New";
         $status1 = "Processing";
         $status2  = "Awaiting Pickup";
         $status3  = "Out for Delivery";
         $status4  = "Ready to ship";
         $status5  = "Order Return";
         $status6  = "Cancelled";
         $status7  = "Delivered";
         $status8  = "Received Order Return";

         $pnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

            $pnewcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount16=count($pnewcountamount);

            $pnewcountamount=json_decode($pnewcountamount,true);

            $pnewsum =0;
            for($i=0;$i<$countamount16;$i++)
            {
                $pnewsum += $pnewcountamount[$i]['PFinalAmount'];
            }






         $pprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pprocessingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount17=count($pprocessingcountamount);

            $pprocessingcountamount=json_decode($pprocessingcountamount,true);

            $pprocessingsum=0;
            for($i=0;$i<$countamount17;$i++)
            {
                $pprocessingsum += $pprocessingcountamount[$i]['PFinalAmount'];
            }








         $pawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pawaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount18=count($pawaitingpickupcountamount);

            $pawaitingpickupcountamount=json_decode($pawaitingpickupcountamount,true);

            $pawaitingpickupsum =0;
            for($i=0;$i<$countamount18;$i++)
            {
                $pawaitingpickupsum += $pawaitingpickupcountamount[$i]['PFinalAmount'];
            }






         $poutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $poutfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount19=count($poutfordeliverycountamount);

            $poutfordeliverycountamount=json_decode($poutfordeliverycountamount,true);

            $poutfordeliverysum =0;
            for($i=0;$i<$countamount19;$i++)
            {
                $poutfordeliverysum += $poutfordeliverycountamount[$i]['PFinalAmount'];
            }







         $preadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $preadytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount20=count($preadytoshipcountamount);

            $preadytoshipcountamount=json_decode($preadytoshipcountamount,true);

            $preadytoshipsum =0;
            for($i=0;$i<$countamount20;$i++)
            {
                $preadytoshipsum += $preadytoshipcountamount[$i]['PFinalAmount'];
            }








         $porderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $porderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount21=count($porderreturncountamount);

            $porderreturncountamount=json_decode($porderreturncountamount,true);

            $porderreturnsum =0;
            for($i=0;$i<$countamount21;$i++)
            {
                $porderreturnsum += $porderreturncountamount[$i]['PFinalAmount'];
            }


             $precievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $precievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount31=count($precievedorderreturncountamount);

            $precievedorderreturncountamount=json_decode($precievedorderreturncountamount,true);

            $precievedorderreturnsum =0;
            for($i=0;$i<$countamount31;$i++)
            {
                $precievedorderreturnsum += $precievedorderreturncountamount[$i]['PFinalAmount'];
            }







         $pcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pcanceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount22=count($pcanceledcountamount);

            $pcanceledcountamount=json_decode($pcanceledcountamount,true);

            $pcanceledsum =0;
            for($i=0;$i<$countamount22;$i++)
            {
                $pcanceledsum += $pcanceledcountamount[$i]['PFinalAmount'];
            }







         $pdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->where('addresses.City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pdeliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->where('addresses.City',$user_city)
         ->orderBy('prodleads.id', 'DESC')
         ->get();

           $countamount23=count($pdeliveredcountamount);

            $pdeliveredcountamount=json_decode($pdeliveredcountamount,true);

            $pdeliveredsum =0;
            for($i=0;$i<$countamount23;$i++)
            {
                $pdeliveredsum += $pdeliveredcountamount[$i]['PFinalAmount'];
            }

            // dd($data);




            // pharmacy end



            // dd($Vertical_names);

        //returning all the above code for individual counts and dynamic counts onto the Admin dashboard
        return view('admin.branchheadmobile',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
            'deferredcountforall','verticals_count','Vertical_names','statuscounts','designation','city_name','servicetype_name','newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','newcountrent','processingcountrent','awaitingpickupcountrent','outfordeliverycountrent','readytoshipcountrent','orderreturncountrent','canceledcountrent','deliveredcountrent','pnewcount','pprocessingcount','pawaitingpickupcount','poutfordeliverycount','preadytoshipcount','porderreturncount','pcanceledcount','pdeliveredcount','newsum','processingsum','awaitingpickupsum','outfordeliverysum','readytoshipsum','orderreturnsum','canceledsum','deliveredsum','newrentsum','processingrentsum','awaitingpickuprentsum','outfordeliveryrentsum','readytoshiprentsum','orderreturnrentsum','canceledrentsum','deliveredrentsum','pnewsum','pprocessingsum','pawaitingpickupsum','poutfordeliverysum','preadytoshipsum','porderreturnsum','pcanceledsum','pdeliveredsum','precievedorderreturncount','recievedorderreturncountrent','recievedorderreturncount','recievedorderreturnsum','recievedorderreturnrentsum','precievedorderreturnsum'));


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
        // return view('admin.branchheadmobile',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        // 'deferredcountforall'));
    }

}
