<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\refer;
use App\Admin;
use App\role_admin;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class referencesController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
     if(Auth::guard('admin')->check())
        {
          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $references= refer::paginate(50);
                    return view('reference.index',compact('references'));
                }
                else
                {
                    return redirect('/admin');
                }
  }
   else
        {
            return redirect('/admin');
        }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
     if(Auth::guard('admin')->check())
        {
           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                $Reference=NULL;
                $ReferalType=NULL;
                 $ContactPerson=NULL;
                $ReferenceAddress=NULL;
                $ReferenceContact=NULL;
                $ReferenceEmail=NULL;

                if($check=="Admin" || $check=="Management")
                {
                return view('reference.create',compact('Reference', 'ReferalType', 'ContactPerson', 'ReferenceAddress', 'ReferenceContact', 'ReferenceEmail'));
                }
                else
                {
                    return redirect('/admin');
                }
  }
   else
        {
            return redirect('/admin');
        }
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    /* Code for creation of new Reference starts here -- by Murtuza and Jatin */

    $reference = new refer;
    $this->validate($request,[
      'Reference'=>'required',
      'ReferalType'=>'required',
    //   'ReferenceEmail' =>'unique:admins'
    ]);

    $email = $request->ReferenceEmail;

    $email_exists = DB::table('admins')->where('email',$email)->value('id');

    if($email_exists!=NULL)
    {
        $Reference=$request->Reference;
        $ReferalType=$request->ReferalType;
         $ContactPerson=$request->ContactPerson;
        $ReferenceAddress=$request->ReferenceAddress;
        $ReferenceContact=$request->ReferenceContact;
        $ReferenceEmail=$request->ReferenceEmail;
        session()->flash('message','This email id already exists');
        return view('reference.create',compact('Reference', 'ReferalType', 'ContactPerson', 'ReferenceAddress', 'ReferenceContact', 'ReferenceEmail'));
    }
    else
    {
    $reference->Reference=$request->Reference;
    $reference->ReferalType=$request->ReferalType;
     $reference->ContactPerson=$request->ContactPerson;
    $reference->ReferenceAddress=$request->ReferenceAddress;
    $reference->ReferenceContact=$request->ReferenceContact;
    $reference->ReferenceEmail=$request->ReferenceEmail;

    $reference->save();

    }

    /* Code for creation of new Reference ends here -- by Murtuza and Jatin */

    /* Code for Login creation of new Reference starts here -- by Murtuza and Jatin */

    //retrieve the id of the most new entered reference
    $referenceid=DB::table('refers')->max('id');

    //retrieve the reference name and email id and give a default password for his/her LOGIN
    $refname=$request->Reference;
    $refemail=$request->ReferenceEmail;
    $refpassword=bcrypt('password');

    //check if there is an existing name in the admins table for that reference
    $check=DB::table('admins')->where('name',$refname)->value('name');

    /*
    If there is no existing row for Referal partner for login in admin table
    and the ref email HAS been entered
    then create a login for him/her
    */
    if($check == NULL & $refemail != Null)
    {
      //if the above conditions are met, add a new entry in admin table to enable LOGIN for the following columns
      $admin = new Admin;

      $admin->name=$refname;
      $admin->email=$refemail;
      $admin->password=$refpassword;
      $admin->save();
      $adminid=DB::table('admins')->max('id');

      //give a default role id and add a new entry in the role_admin table
      $roleid=8;

      $role_admin = new role_admin;

      $role_admin->role_id=$roleid;
      $role_admin->admin_id=$adminid;
      $role_admin->save();
    }

    //retrieving the session id of the person who is logged in
    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

    //retrieving the username of the presently logged in user
    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

    // retrieving the employee id of the presently logged in user
    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

    //retrieving the log id of the person who logged in just now
    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


    $activity = new Activity;
    $activity->emp_id = $emp_id;
    $activity->activity = $request->Reference." Reference Added ";
    $activity->activity_time = new \DateTime();
    $activity->log_id = $log_id;
    $activity->save();

    return redirect('/references');
}

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    return $id;
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
     if(Auth::guard('admin')->check())
        {

           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                      $reference = refer::find($id);
                      return view('reference.edit',compact('reference'));
                 }
                else
                {
                    return redirect('/admin');
                }
  }
   else
        {
            return redirect('/admin');
        }
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reference = refer::find($id);
        $reference->Reference=$request->Reference;
        $reference->ReferalType=$request->ReferalType;
         $reference->ContactPerson=$request->ContactPerson;
        $reference->ReferenceAddress=$request->ReferenceAddress;
        $reference->ReferenceContact=$request->ReferenceContact;
        $reference->ReferenceEmail=$request->ReferenceEmail;

        $ref = $reference->getDirty();
        // dd($lang);

        foreach($ref as $key=>$value)
        {
            $refc[] = $key;

        }

        foreach($ref as $key=>$value)
        {
            $refv[] = $value;
        }

        //this is for converting array values into a string with new line

        //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
        if(empty($refc))
        {
            $refd = " ";
            $reff = " ";
            $refd = trim($refd);
            $reff = trim($reff);
        }
        else
        {
            $refd = implode("\r\n", $refc);
            $reff = implode("\r\n", $refv);
        }

        $reference->save();


/* code for creating login for refer partner */


        $refinfo=DB::table('refers')->where('id',$id)->select('Reference','ReferenceEmail')->get();
        $refinfo=json_decode($refinfo);
        $refname= $refinfo[0]->Reference;
       $refemail= $refinfo[0]->ReferenceEmail;

$refpassword=bcrypt('password');

$checkid=DB::table('admins')->where('name',$refname)->value('id');
$check=DB::table('admins')->where('name',$refname)->value('email');

$re=$request->ReferenceEmail;
if($re==NULL)
{
$rid=DB::table('admins')->where('name',$refname)->value('id');
$vid=DB::table('role_admins')->where('admin_id',$rid)->value('id');


$role_admin=role_admin::find($vid);
    $role_admin->delete();

$admins=admin::find($rid);
    $admins->delete();


}
/*
    If there is no existing row for Referal partner for login in admin table
    and the ref email HAS been entered
    then create a login for him/her
*/
if($check == NULL & $refemail != Null)
{
        $admin = new Admin;

       $admin->name=$refname;
       $admin->email=$refemail;
       $admin->password=$refpassword;
       $admin->save();
       $adminid=DB::table('admins')->max('id');

        $roleid=8;

        $role_admin = new role_admin;

       $role_admin->role_id=$roleid;
       $role_admin->admin_id=$adminid;
       $role_admin->save();
}

   $email=$request->ReferenceEmail;
if($email!=NULL)
{
  $checkid=DB::table('admins')->where('name',$refname)->value('id');
$check=DB::table('admins')->where('name',$refname)->value('email');

   if($check!=$email)
   {
     $admin = admin::find($checkid);
        $admin->email=$email;
        $admin->save();
   }
}
/*end*/
//retrieving the session id of the person who is logged in
$log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

//retrieving the username of the presently logged in user
$username = DB::table('logs')->where('session_id', session()->getId())->value('name');

// retrieving the employee id of the presently logged in user
$emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

//retrieving the log id of the person who logged in just now
$log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

//all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation

if($refd==" " || $refd==' ' || $refd=="" || $refd=='')
{
    $refd = trim($refd);
    $reff = trim($reff);
}
else
{
    $refd = $refd."\r\n";
    $reff = $reff."\r\n";
}

$fields_updated = $refd;
$values_updated = $reff;

if((trim($fields_updated)!==""))
{
    // dd($fields_updated);
    $activity = new Activity;
    $activity->emp_id = $emp_id;
    $activity->activity = "Fields Updated";
    $activity->activity_time = new \DateTime();
    $activity->log_id = $log_id;
    $activity->field_updated = $fields_updated;
    $activity->value_updated = $values_updated;

    $activity->save();
}

       session()->flash('message','Updated Successfully');
       return redirect('/references');
    }







  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $reference=refer::find($id);

    $ref_del = DB::table('refers')->where('id',$id)->value('Reference');

    $reference->delete();

    //retrieving the session id of the person who is logged in
    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

    //retrieving the username of the presently logged in user
    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

    // retrieving the employee id of the presently logged in user
    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

    //retrieving the log id of the person who logged in just now
    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

    $activity = new Activity;
    $activity->emp_id = $emp_id;
    $activity->activity = $ref_del." Referal Partner Deleted";
    $activity->activity_time = new \DateTime();
    $activity->log_id = $log_id;

    $activity->save();

    session()->flash('message','Delete Successfully');
    return redirect('/references');
  }
}
