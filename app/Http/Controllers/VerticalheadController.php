<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\lead;
use App\personneldetail;
use App\service;
use App\address;
use App\language;
use App\employee;
use App\product;
use App\enquiryproduct;
use App\verticalcoordination;
use App\abdomen;
use App\communication;
use App\circulatory;
use App\denture;
use App\extremitie;
use App\fecal;
use App\genito;
use App\memoryintact;
use App\mobility;
use App\nutrition;
use App\orientation;
use App\vitalsign;
use App\respiratory;
use App\visionhearing;
use App\mother;
use App\mathruthvam;
use App\physiotheraphy;
use App\physioreport;
use App\assessment;
use App\generalhistory;
use App\verticalref;
use App\refer;
use App\position;
use App\Activity;
use App\Log;
use App\generalcondition;


use Mail;
use Session;

class VerticalheadController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {

    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {

            $name=Auth::guard('admin')->user()->name;
            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();



            $empid=DB::table('employees')->where('FirstName',$name)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();


            $adminid = DB::table('admins')->where('name',$name)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');


            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);


            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);


            $underwhom=DB::table('employees')->where('under',$eid)->where('Designation','Coordinator')->get();
            // dd($underwhom);
            //retrieve the designation of the logged in user
            $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');
            $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');

            // dd($designation2,$designation3,$department2);
            if($designation == "Branch Head")
            {
                $designation="BranchHead";
                return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
            }
            else
            {
                if($roles=="Vertical_ProductSelling")
                {

                    $designation=$roles;
                    return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                }
                else
                {
                    if($roles=="Vertical_ProductRental")
                    {
                        $designation=$roles;
                        return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager")
                        {
                            $designation=$roles;
                            return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager")
                            {
                                $designation=$roles;
                                return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy")
                                {
                                    $designation=$roles;
                                    return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                }
                                else
                                {

                                    if($roles=="Vertical_ProductSelling_Pharmacy")
                                    {

                                        $designation=$roles;
                                        return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy")
                                        {
                                            $designation=$roles;
                                            return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                        }
                                        else
                                        {

                                            if($roles=="Vertical_FieldOfficer")

                                            {
                                                $designation=$roles;
                                                return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldExecutive")
                                                {
                                                    $designation=$roles;
                                                    return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                                }
                                                else
                                                {
                                                    $designation="vertical";
                                                    return view('vh.create',compact('indian_cities','count1','count','country_codes','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','emp1','status','underwhom','designation'));
                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {


        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=Null;
        $name1123=$request->loginname;
        $logged_in_user=$name1123;

        $client_mobile_number=$request->clientmob;

        $check_for_existing=DB::table('leads')->where('MobileNumber',$client_mobile_number)->value('id');
        $existingcheck=$request->existing;


        $Preferredlanguages=$request->Preferredlanguages;

        $Preferredlanguages = str_replace(array('[',']' ,),' ',$Preferredlanguages);
        $Preferredlanguages = preg_replace('/"/', '', $Preferredlanguages);
        $Preferredlanguages = trim($Preferredlanguages);

        // Checking if the client is already existing then show the leads related to that and ask user whether they want to create the same lead or not


        if($check_for_existing!=NULL && $existingcheck=="existing")
        {
            // dd($check_for_existing);

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('MobileNumber',$client_mobile_number)
            ->orderBy('leads.id', 'DESC')
            ->paginate(150);


            $fName=$request->clientfname;
            $mName=$request->clientmname;
            $lName=$request->clientlname;
            $EmailId=$request->clientemail;
            $Source=$request->source;
            // $Country_Name=$country_name;
            $Country_Code= $request->code;
            $MobileNumber=$request->clientmob;
            $Alternatenumber=$request->clientalternateno;
            $EmergencyContact =$request->EmergencyContact;
            $AssesmentReq=$request->assesmentreq;
            $createdby=$request->loginname;
            $Referenceid=$request->reference;


            //fetch and store all the address details
            $Address1=$request->Address1;
            $Address2=$request->Address2;
            $City=$request->City;
            $District=$request->District;
            $State=$request->State;
            $PinCode=$request->PinCode;


            $same1=$request->presentcontact;

            $PAddress1=$request->PAddress1;
            $PAddress2=$request->PAddress2;
            $PCity=$request->PCity;
            $PDistrict=$request->PDistrict;
            $PState=$request->PState;
            $PPinCode=$request->PPinCode;


            $same=$request->emergencycontact;


            $EAddress1=$request->EAddress1;
            $EAddress2=$request->EAddress2;
            $ECity=$request->ECity;
            $EDistrict=$request->EDistrict;
            $EState=$request->EState;
            $EPinCode=$request->EPinCode;





            $PtfName=$request->patientfname;
            $PtmName=$request->patientmname;
            $PtlName=$request->patientlname;
            $age=$request->age;
            $Gender=$request->gender;
            $Relationship=$request->relationship;
            $Occupation=$request->Occupation;
            $AadharNum=$request->aadhar;
            $AlternateUHIDType=$request->AlternateUHIDType;
            $AlternateUHIDNumber=$request->AlternateUHIDNumber;
            $PTAwareofDisease=$request->PTAwareofDisease;




            $ServiceType=$request->servicetype;
            $GeneralCondition=$request->GeneralCondition;
            $LeadType=$request->leadtype;
            $Branch=$request->branch;
            $RequestDateTime=$request->requesteddate;
            $AssignedTo=$request->assignedto;
            $QuotedPrice=$request->quotedprice;
            $ExpectedPrice=$request->expectedprice;
            $ServiceStatus=$request->servicestatus;
            $PreferedGender=$request->preferedgender;
            $PreferedLanguage=$Preferredlanguages;
            $Remarks=$request->remarks;
            $langId=$request->preferedlanguage;


            // dd($request->servicestatus);
            $pid=$request->pid;
            $checkbox=$request->addproduct;
            $coordinator_assigned=$request->assigned;

            // dd($checkbox);

            $designations=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
            // dd($designations);

            if($designations=="Admin")
            {
                $layout="home";
            }
            else
            {
                if($designations=="Coordinator")
                {
                    $layout="coordinator";
                }
                else
                {
                    if($designations=="Vertical Head")
                    {
                        $layout="vertical";
                    }
                    else
                    {
                        if($designations=="Management")
                        {
                            $layout="management";
                        }
                        else
                        {
                            if($designations=="Branch Head")
                            {
                                $layout="BranchHead";
                            }
                            else
                            {
                                if($designations=="Marketing")
                                {
                                    $layout="marketing";
                                }
                                else
                                {
                                    $layout="customercare";
                                }
                            }
                        }
                    }
                }
            }

            return view('vh.showlead',compact('leads','fName','mName','lName','EmailId','Source','Country_Code','MobileNumber','Alternatenumber','EmergencyContact','AssesmentReq','createdby','Referenceid','Address1','Address2','City','District','State','PinCode','same1','PAddress1','PAddress2','PCity','PDistrict','PState','PPinCode','same','EAddress1','EAddress2','ECity','EDistrict','EState','EPinCode','PtfName','PtmName','PtlName','age','Gender','Relationship','Occupation','AadharNum','AlternateUHIDType','AlternateUHIDNumber','PTAwareofDisease','ServiceType','GeneralCondition','LeadType','Branch','RequestDateTime','AssignedTo','QuotedPrice','ExpectedPrice','ServiceStatus','PreferedGender','PreferedLanguage','Remarks','langId','checkbox','logged_in_user','coordinator_assigned','layout'));
        }



        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        $code = $request->code;

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $country_name=NULL;
        for($i=0;$i<$count;$i++)
        {
            if($code == $country_codes[$i]['dial_code'])
            {
                $country_name = $country_codes[$i]['name'];
            }
        }

        // dd($country_name);
        if($request->source == Null)
        {

            $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
            $source=$refer;
        }
        else
        {
            $source=$request->source;
        }



        $time = time();

        $current_time=date('Y-m-d',$time);

        $mytime=  date('Y-m-d', strtotime("-1 month"));

        // dd($mytime);

        $check_for_existing=DB::table('leads')
        ->select('services.*','personneldetails.*','addresses.*','leads.*')
        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('services', 'leads.id', '=', 'services.LeadId')->where('MobileNumber',$client_mobile_number)->where('ServiceType',$request->servicetype)->where('Branch',$request->branch)->where('ServiceStatus',"Dropped")->orwhere('leads.created_at',$current_time)->whereBetween('leads.created_at',[$mytime,$current_time])->orderby('leads.id','DESC')->get();
        // $check_for_existing=DB::table('leads')->where('MobileNumber',$client_mobile_number)->orderby('id','DESC')->first();

        // dd($check_for_existing);

        if(count($check_for_existing)>0)
        {

            $check_for_existing=json_decode($check_for_existing,true);
            $lid= $check_for_existing[0]['id'];
            // dd($lid);

            $created_at_time=DB::table('leads')->where('id',$lid)->value('created_at');
            $lead_time = substr($created_at_time, 0,10);





            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id',$lid)
            ->orderBy('leads.id', 'DESC')
            ->get();

            $leads=json_decode($leads);
            $branch_name= $leads[0]->Branch;
            $service_request= $leads[0]->ServiceType;
            $service_status= $leads[0]->ServiceStatus;


            if($lead_time>=$mytime)
            {
                if($service_status=="Dropped")
                {

                    if($branch_name==$request->branch && $service_request== $request->servicetype)
                    {


                        $leadid=$lid;
                        $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');

                        $service_id=DB::table('services')->where('Leadid',$lid)->value('id');
                        $update_status=service::find($service_id);
                        // dd($lid);
                        $update_status->ServiceStatus='New';
                        $update_status->save();

                    }
                    else
                    {

                        $lead = new lead;
                        // $this->validate($request,[
                        //         'Languages'=>'required',
                        //     ]);


                        $lead->fName=$request->clientfname;
                        $lead->mName=$request->clientmname;
                        $lead->lName=$request->clientlname;
                        $lead->EmailId=$request->clientemail;
                        $lead->Source=$source;
                        $lead->Country_Name=$country_name;
                        $lead->Country_Code=$code;
                        $lead->MobileNumber=$request->clientmob;
                        $lead->Alternatenumber=$request->clientalternateno;
                        $lead->EmergencyContact =$request->EmergencyContact;
                        $lead->AssesmentReq=$request->assesmentreq;
                        $lead->createdby=$request->loginname;
                        $lead->Referenceid=$ref;
                        $lead->empid=$empid;




                        $lead->save();
                        $leadid=DB::table('leads')->max('id');

                        $address = new address;

                        $address->Address1=$request->Address1;
                        $address->Address2=$request->Address2;
                        $address->City=$request->City;
                        $address->District=$request->District;
                        $address->State=$request->State;
                        $address->PinCode=$request->PinCode;

                        $same1=$request->presentcontact;
                        if($same1=='same')
                        {
                            $address->PAddress1=$request->Address1;
                            $address->PAddress2=$request->Address2;
                            $address->PCity=$request->City;
                            $address->PDistrict=$request->District;
                            $address->PState=$request->State;
                            $address->PPinCode=$request->PinCode;
                        }

                        else
                        {
                            $address->PAddress1=$request->PAddress1;
                            $address->PAddress2=$request->PAddress2;
                            $address->PCity=$request->PCity;
                            $address->PDistrict=$request->PDistrict;
                            $address->PState=$request->PState;
                            $address->PPinCode=$request->PPinCode;
                        }

                        $same=$request->emergencycontact;
                        if($same=='same')
                        {
                            $address->EAddress1=$request->Address1;
                            $address->EAddress2=$request->Address2;
                            $address->ECity=$request->City;
                            $address->EDistrict=$request->District;
                            $address->EState=$request->State;
                            $address->EPinCode=$request->PinCode;
                        }

                        else
                        {
                            $address->EAddress1=$request->EAddress1;
                            $address->EAddress2=$request->EAddress2;
                            $address->ECity=$request->ECity;
                            $address->EDistrict=$request->EDistrict;
                            $address->EState=$request->EState;
                            $address->EPinCode=$request->EPinCode;
                        }

                        $address->leadid=$leadid;
                        $address->empid=$empid;
                        $address->save();
                        $addressid=DB::table('addresses')->max('id');

                        $personnel = new personneldetail;
                        // $this->validate($request,[
                        //         'Languages'=>'required',
                        //     ]);

                        $personnel->PtfName=$request->patientfname;
                        $personnel->PtmName=$request->patientmname;
                        $personnel->PtlName=$request->patientlname;
                        $personnel->age=$request->age;
                        $personnel->Gender=$request->gender;
                        $personnel->Relationship=$request->relationship;
                        $personnel->Occupation=$request->Occupation;
                        $personnel->AadharNum=$request->aadhar;
                        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
                        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
                        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
                        $personnel->Addressid=$addressid;
                        $personnel->Leadid=$leadid;

                        $personnel->save();



                        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
                        $langid=json_decode($l);

                        $service = new service;
                        // $this->validate($request,[
                        //         'Languages'=>'required',
                        //     ]);

                        $service->ServiceType=$request->servicetype;
                        $service->GeneralCondition=$request->GeneralCondition;
                        $service->LeadType=$request->leadtype;
                        $service->Branch=$request->branch;
                        $service->RequestDateTime=$request->requesteddate;
                        $service->AssignedTo=$request->assignedto;
                        $service->QuotedPrice=$request->quotedprice;
                        $service->ExpectedPrice=$request->expectedprice;
                        $service->ServiceStatus=$request->servicestatus;
                        $service->PreferedGender=$request->preferedgender;
                        $service->PreferedLanguage=$request->preferedlanguage;
                        $service->Remarks=$request->remarks;
                        $service->langId=$langid;
                        $service->LeadId=$leadid;

                        $service->save();



                        $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                        //$e=json_decode($e);
                        //      $e1=$e[0]->id;

                        $verticalcoordination = new verticalcoordination;
                        $verticalcoordination->empid=$e;
                        $verticalcoordination->leadid=$leadid;
                        $verticalcoordination->ename=$request->assigned;
                        $verticalcoordination->save();


                    }
                }
                else
                {

                    $lead = new lead;
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);


                    $lead->fName=$request->clientfname;
                    $lead->mName=$request->clientmname;
                    $lead->lName=$request->clientlname;
                    $lead->EmailId=$request->clientemail;
                    $lead->Source=$source;
                    $lead->Country_Name=$country_name;
                    $lead->Country_Code=$code;
                    $lead->MobileNumber=$request->clientmob;
                    $lead->Alternatenumber=$request->clientalternateno;
                    $lead->EmergencyContact =$request->EmergencyContact;
                    $lead->AssesmentReq=$request->assesmentreq;
                    $lead->createdby=$request->loginname;
                    $lead->Referenceid=$ref;
                    $lead->empid=$empid;




                    $lead->save();
                    $leadid=DB::table('leads')->max('id');

                    $address = new address;

                    $address->Address1=$request->Address1;
                    $address->Address2=$request->Address2;
                    $address->City=$request->City;
                    $address->District=$request->District;
                    $address->State=$request->State;
                    $address->PinCode=$request->PinCode;

                    $same1=$request->presentcontact;
                    if($same1=='same')
                    {
                        $address->PAddress1=$request->Address1;
                        $address->PAddress2=$request->Address2;
                        $address->PCity=$request->City;
                        $address->PDistrict=$request->District;
                        $address->PState=$request->State;
                        $address->PPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->PAddress1=$request->PAddress1;
                        $address->PAddress2=$request->PAddress2;
                        $address->PCity=$request->PCity;
                        $address->PDistrict=$request->PDistrict;
                        $address->PState=$request->PState;
                        $address->PPinCode=$request->PPinCode;
                    }

                    $same=$request->emergencycontact;
                    if($same=='same')
                    {
                        $address->EAddress1=$request->Address1;
                        $address->EAddress2=$request->Address2;
                        $address->ECity=$request->City;
                        $address->EDistrict=$request->District;
                        $address->EState=$request->State;
                        $address->EPinCode=$request->PinCode;
                    }

                    else
                    {
                        $address->EAddress1=$request->EAddress1;
                        $address->EAddress2=$request->EAddress2;
                        $address->ECity=$request->ECity;
                        $address->EDistrict=$request->EDistrict;
                        $address->EState=$request->EState;
                        $address->EPinCode=$request->EPinCode;
                    }

                    $address->leadid=$leadid;
                    $address->empid=$empid;
                    $address->save();
                    $addressid=DB::table('addresses')->max('id');

                    $personnel = new personneldetail;
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    $personnel->PtfName=$request->patientfname;
                    $personnel->PtmName=$request->patientmname;
                    $personnel->PtlName=$request->patientlname;
                    $personnel->age=$request->age;
                    $personnel->Gender=$request->gender;
                    $personnel->Relationship=$request->relationship;
                    $personnel->Occupation=$request->Occupation;
                    $personnel->AadharNum=$request->aadhar;
                    $personnel->AlternateUHIDType=$request->AlternateUHIDType;
                    $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
                    $personnel->PTAwareofDisease=$request->PTAwareofDisease;
                    $personnel->Addressid=$addressid;
                    $personnel->Leadid=$leadid;

                    $personnel->save();



                    $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
                    $langid=json_decode($l);

                    $service = new service;
                    // $this->validate($request,[
                    //         'Languages'=>'required',
                    //     ]);

                    $service->ServiceType=$request->servicetype;
                    $service->GeneralCondition=$request->GeneralCondition;
                    $service->LeadType=$request->leadtype;
                    $service->Branch=$request->branch;
                    $service->RequestDateTime=$request->requesteddate;
                    $service->AssignedTo=$request->assignedto;
                    $service->QuotedPrice=$request->quotedprice;
                    $service->ExpectedPrice=$request->expectedprice;
                    $service->ServiceStatus=$request->servicestatus;
                    $service->PreferedGender=$request->preferedgender;
                    $service->PreferedLanguage=$request->preferedlanguage;
                    $service->Remarks=$request->remarks;
                    $service->langId=$langid;
                    $service->LeadId=$leadid;

                    $service->save();



                    $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                    //$e=json_decode($e);
                    //      $e1=$e[0]->id;

                    $verticalcoordination = new verticalcoordination;
                    $verticalcoordination->empid=$e;
                    $verticalcoordination->leadid=$leadid;
                    $verticalcoordination->ename=$request->assigned;
                    $verticalcoordination->save();

                }
            }

            else
            {

                $lead = new lead;
                // $this->validate($request,[
                //         'Languages'=>'required',
                //     ]);


                $lead->fName=$request->clientfname;
                $lead->mName=$request->clientmname;
                $lead->lName=$request->clientlname;
                $lead->EmailId=$request->clientemail;
                $lead->Source=$source;
                $lead->Country_Name=$country_name;
                $lead->Country_Code=$code;
                $lead->MobileNumber=$request->clientmob;
                $lead->Alternatenumber=$request->clientalternateno;
                $lead->EmergencyContact =$request->EmergencyContact;
                $lead->AssesmentReq=$request->assesmentreq;
                $lead->createdby=$request->loginname;
                $lead->Referenceid=$ref;
                $lead->empid=$empid;




                $lead->save();
                $leadid=DB::table('leads')->max('id');

                $address = new address;

                $address->Address1=$request->Address1;
                $address->Address2=$request->Address2;
                $address->City=$request->City;
                $address->District=$request->District;
                $address->State=$request->State;
                $address->PinCode=$request->PinCode;

                $same1=$request->presentcontact;
                if($same1=='same')
                {
                    $address->PAddress1=$request->Address1;
                    $address->PAddress2=$request->Address2;
                    $address->PCity=$request->City;
                    $address->PDistrict=$request->District;
                    $address->PState=$request->State;
                    $address->PPinCode=$request->PinCode;
                }

                else
                {
                    $address->PAddress1=$request->PAddress1;
                    $address->PAddress2=$request->PAddress2;
                    $address->PCity=$request->PCity;
                    $address->PDistrict=$request->PDistrict;
                    $address->PState=$request->PState;
                    $address->PPinCode=$request->PPinCode;
                }

                $same=$request->emergencycontact;
                if($same=='same')
                {
                    $address->EAddress1=$request->Address1;
                    $address->EAddress2=$request->Address2;
                    $address->ECity=$request->City;
                    $address->EDistrict=$request->District;
                    $address->EState=$request->State;
                    $address->EPinCode=$request->PinCode;
                }

                else
                {
                    $address->EAddress1=$request->EAddress1;
                    $address->EAddress2=$request->EAddress2;
                    $address->ECity=$request->ECity;
                    $address->EDistrict=$request->EDistrict;
                    $address->EState=$request->EState;
                    $address->EPinCode=$request->EPinCode;
                }

                $address->leadid=$leadid;
                $address->empid=$empid;
                $address->save();
                $addressid=DB::table('addresses')->max('id');

                $personnel = new personneldetail;
                // $this->validate($request,[
                //         'Languages'=>'required',
                //     ]);

                $personnel->PtfName=$request->patientfname;
                $personnel->PtmName=$request->patientmname;
                $personnel->PtlName=$request->patientlname;
                $personnel->age=$request->age;
                $personnel->Gender=$request->gender;
                $personnel->Relationship=$request->relationship;
                $personnel->Occupation=$request->Occupation;
                $personnel->AadharNum=$request->aadhar;
                $personnel->AlternateUHIDType=$request->AlternateUHIDType;
                $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
                $personnel->PTAwareofDisease=$request->PTAwareofDisease;
                $personnel->Addressid=$addressid;
                $personnel->Leadid=$leadid;

                $personnel->save();



                $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
                $langid=json_decode($l);

                $service = new service;
                // $this->validate($request,[
                //         'Languages'=>'required',
                //     ]);

                $service->ServiceType=$request->servicetype;
                $service->GeneralCondition=$request->GeneralCondition;
                $service->LeadType=$request->leadtype;
                $service->Branch=$request->branch;
                $service->RequestDateTime=$request->requesteddate;
                $service->AssignedTo=$request->assignedto;
                $service->QuotedPrice=$request->quotedprice;
                $service->ExpectedPrice=$request->expectedprice;
                $service->ServiceStatus=$request->servicestatus;
                $service->PreferedGender=$request->preferedgender;
                $service->PreferedLanguage=$request->preferedlanguage;
                $service->Remarks=$request->remarks;
                $service->langId=$langid;
                $service->LeadId=$leadid;

                $service->save();



                $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                //$e=json_decode($e);
                //      $e1=$e[0]->id;

                $verticalcoordination = new verticalcoordination;
                $verticalcoordination->empid=$e;
                $verticalcoordination->leadid=$leadid;
                $verticalcoordination->ename=$request->assigned;
                $verticalcoordination->save();

            }
        }
        else
        {
            $lead = new lead;
            // $this->validate($request,[
            //         'Languages'=>'required',
            //     ]);


            $lead->fName=$request->clientfname;
            $lead->mName=$request->clientmname;
            $lead->lName=$request->clientlname;
            $lead->EmailId=$request->clientemail;
            $lead->Source=$source;
            $lead->Country_Name=$country_name;
            $lead->Country_Code=$code;
            $lead->MobileNumber=$request->clientmob;
            $lead->Alternatenumber=$request->clientalternateno;
            $lead->EmergencyContact =$request->EmergencyContact;
            $lead->AssesmentReq=$request->assesmentreq;
            $lead->createdby=$request->loginname;
            $lead->Referenceid=$ref;
            $lead->empid=$empid;




            $lead->save();
            $leadid=DB::table('leads')->max('id');

            $address = new address;

            $address->Address1=$request->Address1;
            $address->Address2=$request->Address2;
            $address->City=$request->City;
            $address->District=$request->District;
            $address->State=$request->State;
            $address->PinCode=$request->PinCode;

            $same1=$request->presentcontact;
            if($same1=='same')
            {
                $address->PAddress1=$request->Address1;
                $address->PAddress2=$request->Address2;
                $address->PCity=$request->City;
                $address->PDistrict=$request->District;
                $address->PState=$request->State;
                $address->PPinCode=$request->PinCode;
            }

            else
            {
                $address->PAddress1=$request->PAddress1;
                $address->PAddress2=$request->PAddress2;
                $address->PCity=$request->PCity;
                $address->PDistrict=$request->PDistrict;
                $address->PState=$request->PState;
                $address->PPinCode=$request->PPinCode;
            }

            $same=$request->emergencycontact;
            if($same=='same')
            {
                $address->EAddress1=$request->Address1;
                $address->EAddress2=$request->Address2;
                $address->ECity=$request->City;
                $address->EDistrict=$request->District;
                $address->EState=$request->State;
                $address->EPinCode=$request->PinCode;
            }

            else
            {
                $address->EAddress1=$request->EAddress1;
                $address->EAddress2=$request->EAddress2;
                $address->ECity=$request->ECity;
                $address->EDistrict=$request->EDistrict;
                $address->EState=$request->EState;
                $address->EPinCode=$request->EPinCode;
            }

            $address->leadid=$leadid;
            $address->empid=$empid;
            $address->save();
            $addressid=DB::table('addresses')->max('id');

            $personnel = new personneldetail;
            // $this->validate($request,[
            //         'Languages'=>'required',
            //     ]);

            $personnel->PtfName=$request->patientfname;
            $personnel->PtmName=$request->patientmname;
            $personnel->PtlName=$request->patientlname;
            $personnel->age=$request->age;
            $personnel->Gender=$request->gender;
            $personnel->Relationship=$request->relationship;
            $personnel->Occupation=$request->Occupation;
            $personnel->AadharNum=$request->aadhar;
            $personnel->AlternateUHIDType=$request->AlternateUHIDType;
            $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
            $personnel->PTAwareofDisease=$request->PTAwareofDisease;
            $personnel->Addressid=$addressid;
            $personnel->Leadid=$leadid;

            $personnel->save();



            $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
            $langid=json_decode($l);

            $service = new service;
            // $this->validate($request,[
            //         'Languages'=>'required',
            //     ]);

            $service->ServiceType=$request->servicetype;
            $service->GeneralCondition=$request->GeneralCondition;
            $service->LeadType=$request->leadtype;
            $service->Branch=$request->branch;
            $service->RequestDateTime=$request->requesteddate;
            $service->AssignedTo=$request->assignedto;
            $service->QuotedPrice=$request->quotedprice;
            $service->ExpectedPrice=$request->expectedprice;
            $service->ServiceStatus=$request->servicestatus;
            $service->PreferedGender=$request->preferedgender;
            $service->PreferedLanguage=$request->preferedlanguage;
            $service->Remarks=$request->remarks;
            $service->langId=$langid;
            $service->LeadId=$leadid;

            $service->save();



            $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
            //$e=json_decode($e);
            //      $e1=$e[0]->id;

            $verticalcoordination = new verticalcoordination;
            $verticalcoordination->empid=$e;
            $verticalcoordination->leadid=$leadid;
            $verticalcoordination->ename=$request->assigned;
            $verticalcoordination->save();
        }
        if($e!= NULL)
        {
            DB::table('services')->where('leadid',$leadid)->update(['ServiceStatus' =>'In Progress' ]);
        }




        /*
        Code for creation log starts here -- by jatin
        */
        //We are trying to retrive the session id for the person who is logged in


        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Lead Created";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->lead_id = $leadid;
        $activity->save();

        /*
        Code for creation log ends here -- by jatin
        */


        $name=DB::table('leads')->where('id',$leadid)->select('createdby','id')->get();
        $name=json_decode($name);
        $name1=$name[0]->createdby;
        $id=$name[0]->id;

        $n=DB::table('admins')->where('name',$name1)->select('id')->get();
        $n=json_decode($n);
        $n1=$n[0]->id;

        $a=DB::table('role_admins')->where('admin_id',$n1)->select('role_id')->get();
        $a=json_decode($a);
        $a1=$a[0]->role_id;

        $b=DB::table('roles')->where('id',$a1)->select('name')->get();
        $b=json_decode($b);
        $b1=$b[0]->name;

        // dd($b1);
        // if($b1=="vertical")
        // {
        //             $name2=DB::table('services')->where('leadid',$leadid)->select('AssignedTo')->get();
        // $name2=json_decode($name2);
        //         $name2=$name2[0]->AssignedTo;





        // $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
        // //$e=json_decode($e);
        // //      $e1=$e[0]->id;

        // $verticalcoordination = new verticalcoordination;
        // $verticalcoordination->empid=$e;
        // $verticalcoordination->leadid=$leadid;
        // $verticalcoordination->save();




        /* SMS functionality when Coordinator is assigned in the Creation part only starts here -- by jatin   ---- Working*/
        //name of coordinator to whom assessment has been assigned
        $coordname = $request->assigned;

        //coordinator contact number
        $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

        //coordinator alternate contact number
        $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

        //customer name
        $name=DB::table('leads')->where('id',$leadid)->value('fName');

        //customer mobile number
        $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

        //alternate customer mobile number
        $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

        //location selected during lead creation
        $location=DB::table('services')->where('leadid',$leadid)->value('Branch');


        //mail
        $name=$request->clientfname;
        $user=$request->assignedto;
        $contact=$request->clientmob;
        $location=$request->branch;
        $who = $request->loginname;
        $from = DB::table('admins')->where('name',$request->loginname)->value('email');
        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
        $to1=$request->clientemail;
        $to2=DB::table('admins')->where('name',$request->assigned)->value('email');



        if($request->assigned)
        {
            /* SMS functionality when Coordinator is assigned in the Creation part only starts here -- by jatin*/

            //service type retrieval done here
            $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

            $active_state = DB::table('sms_email_filter')->value('active_state');

            if($coordmob!=NULL && $active_state==0)
            {
                // sending message to coordinator
                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                $phonenimber=$coordmob;
                $time = time();
                $date = date('m/d/Y',$time);
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                $report = substr($curl_scraped_page,0,2);
                $jobno = trim(substr($curl_scraped_page,3));
                curl_close($ch);



                $i=0;
                //sending a message to coordinator again if not sent first time
                while($report!="OK" || $i<=1)
                {
                    if($report!="OK")
                    {
                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                        $phonenimber=$coordmob;
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }

            //Message sending to alternate contact number of Coordinator

            $active_state = DB::table('sms_email_filter')->value('active_state');


            if($altcoordmob!=NULL && $active_state==0)
            {
                // sending message to coordinator
                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                $phonenimber=$altcoordmob;
                $time = time();
                $date = date('m/d/Y',$time);
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($altcoordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                $report = substr($curl_scraped_page,0,2);
                $jobno = trim(substr($curl_scraped_page,3));
                curl_close($ch);



                $i=0;
                //sending a message to coordinator again if not sent first time
                while($report!="OK" || $i<=1)
                {
                    if($report!="OK")
                    {
                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                        $phonenimber=$altcoordmob;
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($altcoordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }

            /* SMS functionality when Coordinator is assigned in the Creation part only ends here -- by jatin*/

        }


        /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to customer -- by jatin --WORKING*/
        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($request->clientmob!=NULL && $active_state==0)
        {
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            $phonenimber=$contact;
            // dd($phonenimber);

            //grabbing the date for report generation
            $time = time();
            $date = date('m/d/Y',$time);

            //url generation for sending message
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);

            //retrieving the term "OK" from the message generated if it is sent at all
            $report = substr($curl_scraped_page,0,2);

            //retrieving the job no of the message sent - to be used in the invalid mobile no report
            $jobno = trim(substr($curl_scraped_page,3));
            curl_close($ch);

            //report for checking for invalid mobile no
            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch1 =curl_init($report1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page1 =curl_exec($ch1);
            curl_close($ch1);

            //if mobile number is invalid this will store the term "Invalid Mno"
            $invalidmno = substr($curl_scraped_page1,0,11);

            // dd($invalidmno);


            //checking if Invalid Mobile no occurred
            if($invalidmno == "Invalid Mno")
            {
                /*
                for fetching all values for view
                */

                $reference=DB::table('refers')->get();
                $gender=DB::table('genders')->get();
                $gender1=DB::table('genders')->get();
                $city=DB::table('cities')->get();
                $pcity=DB::table('cities')->get();
                $ecity=DB::table('cities')->get();
                $branch=DB::table('cities')->get();
                $status=DB::table('leadstatus')->get();
                $leadtype=DB::table('leadtypes')->get();
                $condition=DB::table('ptconditions')->get();
                $language=DB::table('languages')->get();
                $relation=DB::table('relationships')->get();
                $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
                $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
                // $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
                $em=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                $em1=DB::table('employees')->where('id',$em)->get();
                $eid = DB::table('employees')->where('FirstName',$who)->value('id');

                $emp12 = DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$id)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);



                $lead = lead::find($leadid);

                $s=DB::table('services')->where('leadid',$leadid)->value('id');
                $service=service::find($s);

                $p=DB::table('personneldetails')->where('leadid',$leadid)->value('id');
                $personneldetail=personneldetail::find($p);

                $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                $nv=DB::table('employees')->where('id',$v)->value('id');
                $vert=employee::find($nv);

                $a=DB::table('addresses')->where('leadid',$leadid)->value('id');
                $address=address::find($a);


                $p=DB::table('products')->where('Leadid',$leadid)->value('id');


                $product=product::find(1);
                if($p != NULL)
                {

                    $product=product::find($p);
                }



                $checkbox=$request->addproduct;

 $designation="vertical";
 $checkdesig ="vertical";
  $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('ename');
// dd($designation);
                /*
                end fetching
                */
                //sending the data to the update lead of Vertical Head
                session()->put('name',$who);
               Session::flash('message', 'Customer Mobile number is invalid'); 
                Session::flash('alert-class', 'alert-danger'); 
               

                  return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
            }

            $i=0;
            // sending a message to customer again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$contact;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }

        //message going to alternate number of customer

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($request->clientalternateno!=NULL && $active_state==0)
        {
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            $phonenimber=$request->clientalternateno;
            // dd($phonenimber);

            //grabbing the date for report generation
            $time = time();
            $date = date('m/d/Y',$time);

            //url generation for sending message
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);

            //retrieving the term "OK" from the message generated if it is sent at all
            $report = substr($curl_scraped_page,0,2);

            //retrieving the job no of the message sent - to be used in the invalid mobile no report
            $jobno = trim(substr($curl_scraped_page,3));
            curl_close($ch);

            //report for checking for invalid mobile no
            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch1 =curl_init($report1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page1 =curl_exec($ch1);
            curl_close($ch1);

            //if mobile number is invalid this will store the term "Invalid Mno"
            $invalidmno = substr($curl_scraped_page1,0,11);

            // dd($invalidmno);


            //checking if Invalid Mobile no occurred
            if($invalidmno == "Invalid Mno")
            {
                /*
                for fetching all values for view
                */

                $reference=DB::table('refers')->get();
                $gender=DB::table('genders')->get();
                $gender1=DB::table('genders')->get();
                $city=DB::table('cities')->get();
                $pcity=DB::table('cities')->get();
                $ecity=DB::table('cities')->get();
                $branch=DB::table('cities')->get();
                $status=DB::table('leadstatus')->get();
                $leadtype=DB::table('leadtypes')->get();
                $condition=DB::table('ptconditions')->get();
                $language=DB::table('languages')->get();
                $relation=DB::table('relationships')->get();
                $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
                $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
                // $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
                $em=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                $em1=DB::table('employees')->where('id',$em)->get();
                $eid = DB::table('employees')->where('FirstName',$who)->value('id');

                $emp12 = DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();



                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$id)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);


                $lead = lead::find($leadid);

                $s=DB::table('services')->where('leadid',$leadid)->value('id');
                $service=service::find($s);

                $p=DB::table('personneldetails')->where('leadid',$leadid)->value('id');
                $personneldetail=personneldetail::find($p);

                $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                $nv=DB::table('employees')->where('id',$v)->value('id');
                $vert=employee::find($nv);

                $a=DB::table('addresses')->where('leadid',$leadid)->value('id');
                $address=address::find($a);


                $p=DB::table('products')->where('Leadid',$leadid)->value('id');


                $product=product::find(1);
                if($p != NULL)
                {

                    $product=product::find($p);
                }



                $checkbox=$request->addproduct;


                /*
                end fetching
                */
                //sending the data to the update lead of Vertical Head
                session()->put('name',$who);
                Session::flash('warning','Customer Mobile number is invalid');
                return view('cc.edit',compact('code','country_name','count1','country_codes','indian_cities','count','lead','vert','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','emp12','em1','checkbox'));
            }

            $i=0;
            // sending a message to customer again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$request->clientalternateno;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }



        //mail

        //customer name
        $name=$request->clientfname;

        //vertical head name
        $user=$request->assignedto;

        //vertical head contact number
        $vmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('MobileNumber');

        //vertical head contact number
        $altvmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('AlternateNumber');

        //customer mobile number
        $contact=$request->clientmob;

        //location selected during lead creation
        $location=$request->branch;

        //service type retrieval done here
        $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

        //who created the lead
        $who = $request->loginname;
        $from = DB::table('admins')->where('name',$request->loginname)->value('email');
        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
        $to1=$request->clientemail;
        $to2=DB::table('admins')->where('name',$request->assigned)->value('email');

        //service type
        $service_type = DB::table('services')->where('leadid',$leadid)->value('ServiceType');

        $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'to1'=>$to2];

        $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];

        //referal partner name
        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

        //referal partner contact number
        $rmob = DB::table('refers')->where('Reference',$request->reference)->value('ReferenceContact');

        /*check if coordinator is assigned or not */
        if($e != NULL)
        {
            DB::table('services')->where('id',$leadid)->update(['ServiceStatus' =>'In Progress' ]);

            $active_state = DB::table('sms_email_filter')->value('active_state');

            if($active_state==0)
            {
                Mail::send('mail', $data, function($message)use($data) {
                    $message->to($data['to1'], $data['user'] )->subject
                    ('Lead ['.$data['leadid'].'] Assigned!!!');
                    $message->from($data['from'],$data['who']);
                });
            }
        }
        /* check end */

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($to!=NULL && $active_state==0)
        {
            Mail::send('mail', $data, function($message)use($data) {
                $message->to($data['to'], $data['user'] )->subject
                ('Lead ['.$data['leadid'].'] Created!!!');
                $message->from($data['from'],$data['who']);
            });

        }


        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to customer -- by jatin*/

        /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to vertical head -- by jatin --WORKING*/

        //service type retrieved here
        $service_type = DB::table('services')->where('Leadid',$leadid)->value('ServiceType');

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($vmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Request Type: ".$service_type."
            Contact: ".$contact."
            Location:".$location;
            $phonenimber=$vmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$vmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }

        //Message going to alternate number of customer

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($altvmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Request Type: ".$service_type."
            Contact: ".$contact."
            Location:".$location;
            $phonenimber=$vmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$valtmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }

        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to vertical head -- by jatin --WORKING*/

        /* SMS for Lead Creation by  Vertical Leads starts here..Message going to referal partner -- by jatin --WORKING*/

        $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.". Our Care Team will get in touch with the ".$name.", shortly.
        - Health Heal";
        $phonenimber=$rmob;
        // dd($phonenimber);

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenimber!=NULL && $active_state==0)
        {
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message to referal partner again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.". Our Care Team will get in touch with the ".$name.", shortly.
                    - Health Heal";
                    $phonenimber=$rmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }

        /* SMS for Lead Creation by  Vertical Leads ends here..Message going to referal partner -- by jatin*/

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($to1!=NULL && $active_state==0)
        {
            Mail::send('mail1', $data1, function($message)use($data1) {
                $message->to($data1['to'], $data1['name'] )->subject
                ('[Happy to Care] Health Heal');
                $message->from('care@healthheal.in','Healthheal');
            });
        }

        //mail end

        /*
        Code for creation log starts here -- by jatin
        */
        //We are trying to retrive the session id for the person who is logged in


        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Lead Created";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->lead_id = $leadid;
        $activity->save();

        /*
        Code for creation log ends here -- by jatin
        */



        /* checking if products has to include or not */



        $checkbox=$request->addproduct;

        if($checkbox == "add")
        {
            $products=DB::table('productdetails')->get();
            $name1=DB::table('leads')->where('id',$leadid)->value('createdby');

            session()->put('name',$name1);
            return view("product.addproduct",compact('leadid','products'));

        }





        $name=DB::table('leads')->where('id',$leadid)->select('createdby','id')->get();
        $name=json_decode($name);
        $name1=$name[0]->createdby;
        $id=$name[0]->id;


        $checkvertical = DB::table('employees')->where('FirstName',$name1)->value('Designation');
        //   dd($checkvertical);


        $n=DB::table('admins')->where('name',$name1)->select('id')->get();
        $n=json_decode($n);
        $n1=$n[0]->id;

        $a=DB::table('role_admins')->where('admin_id',$n1)->select('role_id')->get();
        $a=json_decode($a);
        $a1=$a[0]->role_id;

        $b=DB::table('roles')->where('id',$a1)->select('name')->get();
        $b=json_decode($b);
        $b1=$b[0]->name;
        if($checkvertical=="Vertical Head")
        {
            //             $name2=DB::table('services')->where('leadid',$leadid)->select('AssignedTo')->get();
            // $name2=json_decode($name2);
            //         $name2=$name2[0]->AssignedTo;



            session()->put('name',$name1123);
            return redirect('/vh');
        }
        else{
            return redirect('/cc');
        }



    }


    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */


    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        if(Auth::guard('admin')->check())
        {


            $logged_in_user=Auth::guard('admin')->user()->name;

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
            $em=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $em1=DB::table('employees')->where('id',$em)->get();

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $service_name=DB::table('services')->where('Leadid',$id)->value('ServiceType');

            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');



            if (session()->has('name'))

            {

                $name1=session()->get('name');

            }
            else{
                $name1=Auth::guard('admin')->user()->name;
            }
            $empid=DB::table('employees')->where('FirstName',$name1)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);
            $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$id)->value('ename');


            $p=DB::table('products')->where('Leadid',$id)->value('id');

            $product=product::find(1);
            if($p != NULL)
            {

                $product=product::find($p);
            }


            if (session()->has('name'))
            {

                $name1=session()->get('name');

            }
            else
            {
                $name1=Auth::guard('admin')->user()->name;
            }

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');
            $department3=DB::table('employees')->where('FirstName',$name1)->value('Department3');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                {
                    $designation=$roles;

                    $checkdesig="coordinator";

                    return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                }
                else
                {
                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                    {
                        $designation=$roles;
                        $checkdesig="coordinator";
                        return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                        {
                            $designation=$roles;
                            $checkdesig="coordinator";
                            return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                $checkdesig="coordinator";
                                return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    $checkdesig="coordinator";
                                    return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        $checkdesig="coordinator";
                                        return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            $checkdesig="coordinator";
                                            return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                        }
                                        else
                                        {
                                            if($roles=="Branch Head" && $user_city==$service_city)
                                            {
                                                $designation=$roles;
                                                $checkdesig="branchhead";
                                                return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="coordinator";
                                                    return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="coordinator";
                                                        return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                    }
                                                    else
                                                    {
                                                        if($designation == "Coordinator" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation="coordinator";
                                                            $checkdesig="coordinator";
                                                            // dd($checkdesig);
                                                            return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('/admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            else
            if($designation == "Management")
            {
                $designation="management";
                $checkdesig="management";
                return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                $checkdesig="admin";
                return view('admin.leadedit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
            }
            else
            {

                if($roles=="Vertical_ProductSelling" && $user_department==$service_name)
                {
                    $designation=$roles;
                    $checkdesig="vertical";
                    return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));

                }
                else
                {
                    if($roles=="Vertical_ProductRental" && $user_department==$service_name)
                    {
                        $designation=$roles;
                        $checkdesig="vertical";
                        return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager" && $user_department==$service_name)
                        {
                            $designation=$roles;
                            $checkdesig="vertical";
                            return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager" && $user_department==$service_name)
                            {
                                $designation=$roles;
                                $checkdesig="vertical";
                                return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$service_name)
                                {
                                    $designation=$roles;
                                    $checkdesig="vertical";
                                    return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$service_name)
                                    {
                                        $designation=$roles;
                                        $checkdesig="vertical";
                                        return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$service_name)
                                        {
                                            $designation=$roles;
                                            $checkdesig="vertical";
                                            return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                        }
                                        else
                                        {
                                            if($designation == "Branch Head" && $user_city==$service_city )
                                            {
                                                $designation="BranchHead";
                                                $checkdesig="branchhead";
                                                return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer" && $user_department==$service_name)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="vertical";
                                                    return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$service_name)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="vertical";
                                                        return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                    }
                                                    else
                                                    {
                                                        // dd($roles,$user_department,$service_name);
                                                        if($roles=="vertical" && $user_department==$service_name)
                                                        {
                                                            $designation="vertical";
                                                            $checkdesig="vertical";
                                                            return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('\admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }



    }


    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function update(Request $request, $id)
    {
        $leadid = DB::table('leads')->where('id',$id)->value('id');

        //echo $lead;

        $name1=$request->loginname;
        /*
        Check if any value has been given to products or not
        check the lead id for that ..
        if it gives null, enter the data first time
        if for SKUID or Product name nothing is entered which means
        most fields have null so don't enter anything in db and don't create an activity -- by jatin
        */

        $p2=DB::table('products')->where('Leadid',$leadid)->select('id')->get();
        $p2=json_decode($p2);


        $code = $request->code;

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $country_name=NULL;
        for($i=0;$i<$count;$i++)
        {
            if($code == $country_codes[$i]['dial_code'])
            {
                $country_name = $country_codes[$i]['name'];
            }
        }

        // dd($code, $country_name);

        if($p2 == NULL)
        {
            $products = new product;
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;
            $products->Leadid=$leadid;
            $pid=NULL;

            if($products->SKUid !=NULL || $products->ProductName !=NULL)
            {
                $products->save();
                $pid=DB::table('products')->max('id');
            }
            else
            {
                $prod = " ";
                $prof = " ";
            }

        }

        /*
        if some product values have been already entered , then perform the updation thing -- by jatin
        */

        else
        {

            $products=DB::table('products')->where('Leadid',$leadid)->select('id')->get();
            $products=json_decode($products);
            $products=$products[0]->id;
            $pid=$products;
            // echo $pid;
            $products=product::find($products);
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;

            /*
            Code for capturing the products part of Log Edit starts here -- by jatin
            */

            //this will retrieve all the fields and values that are updated in the form of an array
            $products_c = $products->getDirty();

            //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
            foreach($products_c as $key=>$value)
            {
                $proc[] = $key;

            }

            foreach($products_c as $key=>$value)
            {
                $prov[] = $value;
            }

            //this is for converting array values into a string with new line

            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
            if(empty($proc))
            {
                $prod = " ";
                $prof = " ";
                $prod = trim($prod);
                $prof = trim($prof);
            }
            else
            {
                $prod = implode("\r\n", $proc);
                $prof = implode("\r\n", $prov);
            }


            /*
            Code for capturing the products part of Log Edit ends here -- by jatin
            */

            $products->save();
        }

        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

        if($request->source != $refer)
        {

            $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
            $source=$refer;
        }
        else
        {
            $source=$request->source;
        }

        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        //these values will be used later -- by jatin
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $lead_id = DB::table('assessments')->where('Leadid',$leadid)->value('id');
        //these values will be used later -- by jatin

        $lead=DB::table('leads')->where('id',$leadid)->value('id');

        // dd($lead);
        $lead=lead::find($lead);
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;

        $lead->Referenceid=$ref;
        $lead->empid=$empid;

        // dd($empid);
        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $lead_c = $lead->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($lead_c as $key=>$value)
        {
            $leadc[] = $key;

        }

        foreach($lead_c as $key=>$value)
        {
            $leadv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($leadc))
        {
            $leadd = " ";
            $leadd = trim($leadd);

            $leadf = " ";
            $leadf = trim($leadf);
        }
        else
        {
            $leadd = implode("\r\n", $leadc);
            $leadf = implode("\r\n", $leadv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $lead->save();

        // dd($leadd, $leadf);
        $address=DB::table('addresses')->where('leadid',$leadid)->value('id');

        $address=address::find($address);
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;
        $address->PAddress1=$request->PAddress1;
        $address->PAddress2=$request->PAddress2;
        $address->PCity=$request->PCity;
        $address->PDistrict=$request->PDistrict;
        $address->PState=$request->PState;
        $address->PPinCode=$request->PPinCode;
        $address->EAddress1=$request->EAddress1;
        $address->EAddress2=$request->EAddress2;
        $address->ECity=$request->ECity;
        $address->EDistrict=$request->EDistrict;
        $address->EState=$request->EState;
        $address->EPinCode=$request->EPinCode;

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $address_c = $address->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($address_c as $key=>$value)
        {
            $addc[] = $key;

        }

        foreach($address_c as $key=>$value)
        {
            $addv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($addc))
        {
            $addd = " ";
            $addd = trim($addd);

            $addf = " ";
            $addf = trim($addf);
        }
        else
        {
            $addd = implode("\r\n", $addc);
            $addf = implode("\r\n", $addv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $address->save();
        // dd($addd,$addf);


        $personnel=DB::table('personneldetails')->where('leadid',$leadid)->value('id');

        $personnel=personneldetail::find($personnel);
        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $personnel_c = $personnel->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($personnel_c as $key=>$value)
        {
            $perc[] = $key;

        }

        foreach($personnel_c as $key=>$value)
        {
            $perv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($perc))
        {
            $perd = " ";
            $perd = trim($perd);

            $perf = " ";
            $perf = trim($perf);
        }
        else
        {
            $perd = implode("\r\n", $perc);
            $perf = implode("\r\n", $perv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $personnel->save();

        // dd($perd,$perf);
        $e1=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
        $coor_name=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('ename');
        $service_check=DB::table('services')->where('leadid',$leadid)->value('AssignedTo');


        $vertical_assign=$request->assigntonew;
        // if lead is transfer to another vertical head remove coordinator from vertical coordination table if assigned
        // dd($service_check);
        $coordinatorassigned=$request->assigned;

        if($service_check!=$vertical_assign)
        {

            $null_value=NULL;

            // checking if the coordinator value is passed from view but the vertical head is changed then make the request null
            if($coor_name==$coordinatorassigned)
            {
                $coordinatorassigned=NULL;
                // dd($request->assigned);
            }
            $coordinatorassigned=NULL;
            $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('id');

            $verticalcoordination=verticalcoordination::find($verticalcoordination);

            $verticalcoordination->empid=$null_value;
            $verticalcoordination->ename=$null_value;
            $verticalcoordination->save();

        }


        $verticalcoordination123=DB::table('verticalcoordinations')->where('leadid',$leadid)->get();
        // dd($verticalcoordination123);

        // dd('asd');
        // dd($request->assignedto);

        $Preferredlanguages=$request->Preferredlanguages;
        $Preferredlanguages = str_replace(array('[',']' ,),' ',$Preferredlanguages);
        $Preferredlanguages = preg_replace('/"/', '', $Preferredlanguages);
        $Preferredlanguages = trim($Preferredlanguages);
        $service=DB::table('services')->where('leadid',$leadid)->value('id');

        $service=service::find($service);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assigntonew;

        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$Preferredlanguages;
        //dd($request->assigntonew , $request->Preferredlanguages);
        $service->Remarks=$request->remarks;

        // dd($request->assigntoo);

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $service_c = $service->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($service_c as $key=>$value)
        {
            $serc[] = $key;

        }

        foreach($service_c as $key=>$value)
        {
            $serv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($serc))
        {
            $serd = " ";
            $serd = trim($serd);

            $serf = " ";
            $serf = trim($serf);
        }
        else
        {
            $serd = implode("\r\n", $serc);
            $serf = implode("\r\n", $serv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */

        $service->save();
        // dd($serd, $serf);
        /* SMS sent to Vertical Head / Branch Head if the service is changed in edit form starts here */

        // dd($request->assignedto);
        $user = $request->assignedto;
        $name = $request->clientfname;
        $contact = $request->clientmob;
        $service_type = $request->servicetype;
        $location = $request->branch;

        $vertical_branchhead_mob = DB::table('employees')->where('FirstName',$user)->value('MobileNumber');
        $vertical_branchhead_altmob = DB::table('employees')->where('FirstName',$user)->value('AlternateNumber');
        $active_state = DB::table('sms_email_filter')->value('active_state');

        // dd($user, $name, $contact, $service_type, $vertical_branchhead_mob, $vertical_branchhead_altmob );
        if($vertical_branchhead_mob!=NULL && $active_state==0)
        {

            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Contact: ".$contact."
            Request Type: ".$service_type."
            Location:".$location;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($vertical_branchhead_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($vertical_branchhead_mob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }

        //message going to alternate number of vh

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($vertical_branchhead_altmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Contact: ".$contact."
            Request Type: ".$service_type."
            Location:".$location;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($vertical_branchhead_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($vertical_branchhead_altmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }





        /* SMS sent to Vertical Head / Branch Head if the service is changed in edit form ends here */

        /*Mail sent to Vertical Head / Branch Head if the service is changed in edit form starts here */

        $who = Auth::guard('admin')->user()->name;
        $from = DB::table('employees')->where('FirstName',$who)->value('Email_id');
        $to = DB::table('employees')->where('FirstName',$user)->value('Email_id');
        $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

        // dd($who, $from, $to, $leadid, $name,$user, $service_type,$contact, $location, $to);

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($to!=NULL && $active_state==0)
        {
            Mail::send('mail', $data, function($message)use($data) {
                $message->to($data['to'], $data['user'] )->subject
                ('Lead ['.$data['leadid'].'] Assigned To You!!!');
                $message->from($data['from'],$data['who']);
            });
        }

        /*Mail sent to Vertical Head / Branch Head if the service is changed in edit form ends here */


        if($request->servicetype == "Mathrutvam - Baby Care")
        {

            $checkforid=DB::table('mothers')->where('Leadid',$leadid)->value('Leadid');

            if($checkforid==NULL)
            {
                $checkassessment_id=DB::table('assessments')->where('Leadid',$leadid)->value('id');

                if($checkassessment_id==NULL)
                {
                    $assessment = new assessment;
                    $assessment->Leadid=$leadid;
                    $assessment->save();
                }


                $checkmother_id=DB::table('mothers')->where('Leadid',$leadid)->value('id');

                if($checkmother_id==NULL)
                {
                    $mother = new mother;
                    $mother->Leadid=$leadid;
                    $mother->save();

                    $motherid=DB::table('mothers')->where('Leadid',$leadid)->value('id');

                    $mathruthvam = new mathruthvam;
                    $mathruthvam->Motherid=$motherid;
                    $mathruthvam->save();

                }


                $checkmemoryintact_id=DB::table('memoryintacts')->where('leadid',$leadid)->value('id');

                if($checkmemoryintact_id==NULL)
                {
                    $memoryintact= new memoryintact;
                    $memoryintact->leadid=$leadid;
                    $memoryintact->save();
                }

                $checkvitalsigns_id=DB::table('vitalsigns')->where('leadid',$leadid)->value('id');

                if($checkvitalsigns_id==NULL)
                {
                    $vitalsigns=new vitalsign;
                    $vitalsigns->leadid=$leadid;
                    $vitalsigns->save();
                }


                $checkrespiratory_id=DB::table('respiratories')->where('leadid',$leadid)->value('id');

                if($checkrespiratory_id==NULL)
                {
                    $respiratory =new respiratory;
                    $respiratory->leadid=$leadid;
                    $respiratory->save();
                }

                $checknutrition_id=DB::table('nutrition')->where('leadid',$leadid)->value('id');

                if($checknutrition_id==NULL)
                {
                    $nutrition =new nutrition;
                    $nutrition->leadid=$leadid;
                    $nutrition->save();
                }




            }
        }

        if($request->servicetype == "Nursing Services" || $request->servicetype == "Personal Supportive Care")
        {

            $checkforid=DB::table('abdomens')->where('leadid',$id)->value('leadid');

            if($checkforid==NULL)
            {
                // dd('ns || psc');

                $checkabdomens_id=DB::table('abdomens')->where('leadid',$id)->value('id');

                if($checkabdomens_id==NULL)
                {
                    $abdomens = new abdomen;

                    $abdomens->leadid=$leadid;
                    $abdomens->save();
                }

                // $checkabdomens_id=DB::table('assessments')->where('leadid',$id)->value('id');
                $assessment = new assessment;
                $assessment->Leadid=$leadid;
                $assessment->save();


                $circulatory =new circulatory;
                $circulatory->leadid=$leadid;
                $circulatory->save();

                $communication = new communication;
                $communication->leadid=$leadid;
                $communication->save();

                $dentures= new denture;
                $dentures->leadid=$leadid;
                $dentures->save();

                $extremitie =new extremitie;
                $extremitie->leadid=$leadid;
                $extremitie->save();

                $fecal= new fecal;
                $fecal->leadid=$leadid;
                $fecal->save();

                $genito = new genito;
                $genito->leadid=$leadid;
                $genito->save();

                $generalcondition = new generalcondition;
                $generalcondition->Leadid=$leadid;
                $generalcondition->save();

                $generalhistory= new generalhistory;
                $generalhistory->leadid=$leadid;
                $generalhistory->save();


                $memoryintact= new memoryintact;
                $memoryintact->leadid=$leadid;
                $memoryintact->save();

                $mobility= new mobility;
                $mobility->leadid=$leadid;
                $mobility->save();

                $orientation=new orientation;
                $orientation->leadid=$leadid;
                $orientation->save();


                $vitalsigns=new vitalsign;
                $vitalsigns->leadid=$leadid;
                $vitalsigns->save();

                $respiratory =new respiratory;
                $respiratory->leadid=$leadid;
                $respiratory->save();


                $position =new position;
                $position->leadid=$leadid;
                $position->save();


                $visionhearing =new visionhearing;
                $visionhearing->leadid=$leadid;
                $visionhearing->save();

                $nutrition =new nutrition;
                $nutrition->leadid=$leadid;
                $nutrition->save();


                $verticalref=new verticalref;
                $verticalref->leadid=$leadid;
                $verticalref->save();




            }
        }

        if($request->servicetype =="Physiotherapy - Home" || $request->servicetype == "Physiotherapy - Clinic")
        {

            $checkforid=DB::table('physiotheraphies')->where('Leadid',$id)->value('Leadid');

            if($checkforid==NULL)
            {



                $physiotheraphies = new physiotheraphy;
                $physiotheraphies->Leadid=$leadid;
                $physiotheraphies->save();

                $assessments = new assessment;
                $assessments->Leadid=$leadid;
                $assessments->save();

                $Physioid=DB::table('physiotheraphies')->max('id');


                $physioreport = new physioreport;
                $physioreport->Physioid=$Physioid;
                $physioreport->save();



            }
        }




        $e=DB::table('employees')->where('FirstName',$coordinatorassigned)->value('id');
        // dd($e);
        //$e=json_decode($e);
        //      $e1=$e[0]->id;
        $e1=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');

        $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('id');



        if($verticalcoordination!=NULL)
        {
            $verticalcoordination=verticalcoordination::find($verticalcoordination);

            if($e!=$e1)
            {

                /*
                Code for capturing assigned to status starts here -- by jatin
                */
                if($e!=NULL)
                {
                    $verticalcoordination->empid=$e;
                    $verticalcoordination->ename=$coordinatorassigned;
                    $asid = 'AssignedTo:';
                    $asif = $coordinatorassigned.', ';
                    $verticalcoordination->save();
                }
                else
                {
                    $asid = ' ';
                    $asif = ' ';
                }
            }
            else
            {

                $asid = ' ';
                $asif = ' ';
            }
            // dd($request->assigned);

        }
        else
        {
            $verticalcoordination= new verticalcoordination;
            $verticalcoordination->empid=$e;
            $verticalcoordination->ename=$coordinatorassigned;
            $verticalcoordination->leadid=$leadid;
            $verticalcoordination->save();

            $asid = ' ';
            $asif = ' ';
        }


        if($e != NULL)
        {
            DB::table('services')->where('leadid',$leadid)->update(['ServiceStatus' =>'In Progress' ]);
        }

        /*
        Code for capturing assigned to ends here -- by jatin
        */


        /*
        Code for final updation of activity with field and values starts here -- by jatin
        */

        if($leadd==" " || $leadd==' ' || $leadd=="" || $leadd=='')
        {
            $leadd = trim($leadd);
            $leadf = trim($leadf);
        }
        else
        {
            $leadd = $leadd."\r\n";
            $leadf = $leadf."\r\n";
        }
        if($addd==" " || $addd==' ' || $addd=="" || $addd=='')
        {
            $addd = trim($addd);
            $addf = trim($addf);
        }
        else
        {
            $addd = $addd."\r\n";
            $addf = $addf."\r\n";
        }
        if($perd==" " || $perd==' ' || $perd=="" || $perd=='')
        {
            $perd = trim($perd);
            $perf = trim($perf);
        }
        else
        {
            $perd = $perd."\r\n";
            $perf = $perf."\r\n";
        }
        if($serd==" " || $serd==' ' || $serd=="" || $serd=='')
        {
            $serd = trim($serd);
            $serf = trim($serf);
        }
        else
        {
            $serd = $serd."\r\n";
            $serf = $serf."\r\n";
        }
        if($prod==" " || $prod==' ' || $prod=="" || $prod=='')
        {
            $prod = trim($prod);
            $prof = trim($prof);
        }
        else
        {
            $prod = $prod."\r\n";
            $prof = $prof."\r\n";
        }
        if($asid==" " || $asid==' ' || $asid=="" || $asid=='')
        {
            $asid = trim($asid);
            $asif = trim($asif);
        }
        else
        {
            $asid = $asid."\r\n";
            $asif = $asif."\r\n";
        }

        //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
        $fields_updated = $leadd.''.$addd.''.$perd.''.$serd.''.$prod.''.$asid;
        $values_updated = $leadf.''.$addf.''.$perf.''.$serf.''.$prof.''.$asif;

        // dd($fields_updated,$values_updated);
        //If no field is updated and all are empty then don't log the activity
        if((trim($fields_updated)!==""))
        {
            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Fields Updated";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->lead_id = $leadid;
            $activity->field_updated = $fields_updated;
            $activity->value_updated = $values_updated;

            $activity->save();
        }

        session()->put('name',$name1);



        return redirect("vh/create?id=$leadid&name=$name1");

        // return redirect('vh/create?id=155&name=Ligo');

        // /Verticalhead/'.$lead->id.'/edit
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }


}
