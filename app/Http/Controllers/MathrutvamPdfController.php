<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use \PDF;

class MathrutvamPdfController extends Controller
{
    public function index(Request $request)
    {
        $leadid = $_GET['leadid'];
        // dd($leadid);
        if($request->has('download'))
        {
            $leaddata=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id','=',$leadid)
            ->orderBy('leads.id', 'DESC')
            ->get();

            $assessmentdata = DB::table('assessments')->where('Leadid',$leadid)->get();

            $vitalsignsdata = DB::table('vitalsigns')->where('leadid',$leadid)->get();



            $memoryintactsdata = DB::table('memoryintacts')->where('leadid',$leadid)->get();



            $respiratorydata = DB::table('respiratories')->where('leadid',$leadid)->get();




            $nutritiondata = DB::table('nutrition')->where('leadid',$leadid)->get();


            $motherdata = DB::table('mothers')->where('leadid',$leadid)->get();

            $motherid = DB::table('mothers')->where('leadid',$leadid)->value('id');
            $mathrutvamdata = DB::table('mathruthvams')->where('Motherid',$motherid)->get();

            // dd($mathrutvamdata);


            // dd($abdomendata);
            $pdf = PDF::loadView('pdf/Mathrutvam_pdfview',compact('leaddata','assessmentdata','vitalsignsdata','memoryintactsdata','respiratorydata','nutritiondata','motherdata','mathrutvamdata'));
            return $pdf->download("Assessment_Form_for_$leadid.pdf");
        }

        // return view('pdf/pdfview');
    }
}
