<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;
use Auth;

class UpdateController extends Controller
{
  /* Code for Password change in User Settings starts here -- by jatin */

  public function index()
  {
    if(Auth::guard('admin')->check())
        {
    $name=Auth::guard('admin')->user()->name;
   
    $desig=DB::table('employees')->where('FirstName',$name)->value('Designation');
    $desig2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
    $desig3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

     $adminid = DB::table('admins')->where('name',$name)->value('id');

        $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
        $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');
// dd($adminid,$role_adminsid,$roles);

    if($roles=="customercare")
    {
      $designation="customercare";
    }
    else
    {
        if($roles=="vertical")
          {
              $designation="vertical";
          }
          else
          {
              if($roles=="Coordinator")
              {
              $designation="coordinator";
              }
              else
              { 
                  if($roles=="admin")
                  {
                    $designation="home";
                  }
                  else
                  { 
                     if($roles=="Management")
                      {
                        $designation="management";
                      }
                      else
                      { 
                        if($roles=="Pharmacy Manager")
                          {
                           $designation="pharmacymanager";
                          }
                        else
                         { 
                          if($roles=="Product Manager")
                            {
                               $designation="productmanager";
                            }
                            else
                            { 
                              if($roles=="Field Officer")
                                {
                                   $designation="fieldofficer";
                                }
                              else
                              { 
                                if($roles=="Field Executive")
                                {
                                   $designation="fieldexecutive";
                                }
                                else
                                 { 
                                  if($roles=="Branch Head")
                                  {
                                    $designation="BranchHead";
                                  }
                                else
                                {

                                   if($roles=="Vertical_ProductManager")
                                   {
                                        $designation="Vertical_ProductManager";
                                   }
                                   else
                                   {
                                    if($roles=="Vertical_PharmacyManager")
                                    {
                                      $designation="Vertical_PharmacyManager";

                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductSelling")
                                        {
                                          $designation="Vertical_ProductSelling";

                                        }
                                        else
                                        {
                                          if($roles=="Vertical_ProductRental")
                                          {
                                            $designation="Vertical_ProductRental";

                                          }
                                          else
                                          {
                                            if($roles=="Vertical_Product_Pharmacy")
                                            {
                                              $designation="Vertical_Product_Pharmacy";

                                            }
                                            else
                                            {
                                              if($roles=="Vertical_ProductSelling_Pharmacy")
                                              {
                                                $designation="Vertical_ProductSelling_Pharmacy";

                                              }
                                              else
                                              {
                                                if($roles=="Vertical_ProductRental_Pharmacy")
                                                {
                                                  $designation="Vertical_ProductRental_Pharmacy";

                                                }
                                                else
                                                {
                                                  if($roles=="Coordinator_ProductManager")
                                                  {
                                                    $designation="Coordinator_ProductManager";

                                                  }
                                                  else
                                                  {
                                                    if($roles=="Coordinator_PharmacyManager")
                                                    {
                                                      $designation="Coordinator_PharmacyManager";

                                                    }
                                                    else
                                                    {
                                                      if($roles=="Coordinator_ProductSelling")
                                                      {
                                                        $designation="Coordinator_ProductSelling";

                                                      }
                                                      else
                                                      {
                                                        if($roles=="Coordinator_ProductRental")
                                                        {
                                                          $designation="Coordinator_ProductRental";

                                                        }
                                                        else
                                                        {
                                                          if($roles=="Coordinator_Product_Pharmacy")
                                                          {
                                                            $designation="Coordinator_Product_Pharmacy";

                                                          }
                                                          else
                                                          {
                                                            if($roles=="Coordinator_ProductSelling_Pharmacy")
                                                            {
                                                              $designation="Coordinator_ProductSelling_Pharmacy";

                                                            }
                                                            else
                                                            {
                                                              if($roles=="Coordinator_ProductRental_Pharmacy")
                                                              {
                                                                  $designation="Coordinator_ProductRental_Pharmacy";
                                                              }
                                                              else
                                                              {
                                                                if($roles=="Vertical_FieldOfficer")
                                                                {
                                                                  $designation="Vertical_FieldOfficer";

                                                                }
                                                                else
                                                                {
                                                                    if($roles=="Vertical_FieldExecutive")
                                                                    {
                                                                      $designation="Vertical_FieldExecutive";

                                                                    }
                                                                    else
                                                                    {
                                                                      if($roles=="Coordinator_FieldOfficer")
                                                                      {
                                                                        $designation="Coordinator_FieldOfficer";

                                                                      }
                                                                      else
                                                                      {
                                                                        if($roles=="Coordinator_FieldExecutive")
                                                                        {
                                                                          $designation="Coordinator_FieldExecutive";

                                                                        }
                                                                        else
                                                                        {
                                                                            $designation="referalpartner";
                                                                        }
                                                                      }
                                                                    }
                                                                }

                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                    }
                                   }
                                 }
                                 }
                              }
                            }
                          }
                      }
                  }
              }
          }
    }

    




    //show the change password view to the logged in person
    return view('admin/passwords/change-password',compact('designation'));
  }
  else
        {
            return redirect('/admin');
        }
  }

  public function update(Request $request)
  {
    //validate the fields of the change password fields
    $this->validate($request, [
      'old' => 'required',
      'password' => 'required|min:6|confirmed',
    ]);

    //retrieve the name of the logged in user
    $name=$request->name;

    //retrieve the corresponding hashed password from the DB
    $admin = DB::table('admins')->where('name',$name)->value('password');

    //rename the hashed password
    $hashedPassword = $admin;

    //whatever is the desired password by the user, hash it
    $newpassword = Hash::make($request->password);

    //check the Old password entered in the field with the one in the DB
    if (Hash::check($request->old, $hashedPassword))
    {
      //Change the password
      DB::table('admins')->where('name',$name)->update(['password'=>$newpassword]);

      //display the correspoding message on success
      $request->session()->flash('success', 'Your password has been changed.');

      //destroy the session of that user so that he/she cannot do the process again
      $request->session()->flush();

      //redirect to the corresponding user's login screen
      return redirect()->route('admin.login');
    }
    else
    {
      //in case of failure, display the following message and redirect to the change password view AGAIN
      $request->session()->flash('failure', 'Your password has not been changed.');

      return redirect('change-password');
    }

    /* Code for Password change in User Settings ends here -- by jatin */


  }
}
