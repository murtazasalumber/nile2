<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\language;
use DB;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;

class languagesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {
            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {

                     $languages= language::paginate(50);
                     return view('language.index',compact('languages'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                     return view('language.create');
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $lang=$request->Languages;
        $language = new language;
        $this->validate($request,[
            'Languages'=>'required',
        ]);
        $language->Languages=$request->Languages;


        $language->save();

        $langid = DB::table('languages')->max('id');
        //    dd($langid);

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $request->Languages." Language Added ";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->save();

        // $name = $request->input('Languages');
        // DB::insert('insert into languages (Languages) values(?)',[$name]);
        //
        //DB::table('languages')->save($name)





        // $lang = $this->language->newEntity();
        //    if ($this->request->is('language')) {
        //       $lang = $this->Languages->patchEntity($lang, $this->request->getData());
        //      if ($this->languages->save($lang)) {
        //        $this->Flash->success(__('The lang has been saved.'));
        //
        //         return $this->redirect(['action' => 'index']);
        //     }
        //      $this->Flash->error(__('The lang could not be saved. Please, try again.'));
        //   }
        //   $this->set(compact('lang'));
        //    $this->set('_serialize', ['lang']);




        // $language = new language;
        //  $this->validate($request,[
        //           'Languages'=>'required',
        //       ]);
        //  $language->Languages=$request->Languages;

        //   $language->save();


        // $languages=$request->input('Languages');

        // $data=array('Languages'=>$languages);
        // DB::table('languages')->insert($data);
        // return redirect('/languages');

        // $name = $request->input('Languages');
        // DB::insert('insert into languages (Languages) values(?)',[$name]);
        //
        //DB::table('languages')->save($name);
        //
        // return redirect('/languages');


        // return language::create([
        //       'Languages' => $name,
        //   ]);


        //  $data = Request::all(); //requested data via Facade
        //prepare validatation
        // $validation = Validator::make($data, [
        //         'Languages' => 'required|max:45',
        //         ]);

        //   if ($validation->fails())
        //   {
        //       return redirect()->back()->withErrors($v->errors());
        //    }

        //  language::create([
        //        'Languages' => Request::input('Languages'),

        //   ]);

        //$data = $request->only('Languages'); // you can use Input::all() too
        //  return App\Person::create($data);






        //   $languages=$request->input('Languages');

        //   $data=array('Languages'=>$languages);


        //  // DB::table('languages')->insert($data);

        //   language::create([
        //      'Languages' => $data['Languages'],
        //  ]);


        return redirect('/languages');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $language = language::find($id);
                    return view('language.edit',compact('language'));

                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        $c=DB::table('languages')->where('id',$id)->value('Languages');
        // dd($c);
        if($request->Languages!=$c)
        {
            $language = language::find($id);
            $language->Languages=$request->Languages;

            $lang = $language->getDirty();
            // dd($lang);

            foreach($lang as $key=>$value)
            {
                $langc[] = $key;

            }

            foreach($lang as $key=>$value)
            {
                $langv[] = $value;
            }

            //this is for converting array values into a string with new line

            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
            if(empty($langc))
            {
                $langd = " ";
                $langf = " ";
                $langd = trim($langd);
                $langf = trim($langf);
            }
            else
            {
                $langd = implode("\r\n", $langc);
                $langf = implode("\r\n", $langv);
            }

            $language->save();

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            if($langd==" " || $langd==' ' || $langd=="" || $langd=='')
            {
                $langd = trim($langd);
                $langf = trim($langf);
            }
            else
            {
                $langd = $langd."\r\n";
                $langf = $langf."\r\n";
            }

            //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
            $fields_updated = $langd;
            $values_updated = $langf;

            if((trim($fields_updated)!==""))
            {
                // dd($fields_updated);
                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "Fields Updated";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->field_updated = $fields_updated;
                $activity->value_updated = $values_updated;

                $activity->save();
            }
            session()->flash('message','Updated Successfully');
        }
        return redirect('/languages');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $language=language::find($id);

        $lang_del = DB::table('languages')->where('id',$id)->value('Languages');
        // dd($lang_del);

        $language->delete();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $lang_del." Language Deleted";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;

        $activity->save();
        session()->flash('message','Delete Successfully');
        return redirect('/languages');
    }
}
