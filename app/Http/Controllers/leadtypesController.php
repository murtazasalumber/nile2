<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\leadtype;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class leadtypesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {
            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                        $leadtypes= leadtype::paginate(50);
                     return view('leadtype.index',compact('leadtypes'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {
            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    return view('leadtype.create');
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $leadtype = new leadtype;
        $this->validate($request,[
            'leadtypes'=>'required',
        ]);
        $leadtype->leadtypes=$request->leadtypes;

        $leadtype->save();

        $leadtypeid = DB::table('leadtypes')->max('id');
        //    dd($langid);

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $request->leadtypes." Lead Type Added ";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->save();

        return redirect('/leadtypes');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        if(Auth::guard('admin')->check())
        {
            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $leadtype = leadtype::find($id);
                    return view('leadtype.edit',compact('leadtype'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        $c=DB::table('leadtypes')->where('id',$id)->value('leadtypes');
        if($request->leadtypes!=$c)
        {
            $leadtype = leadtype::find($id);
            $leadtype->leadtypes=$request->leadtypes;

            $leadtype1 = $leadtype->getDirty();
            // dd($lang);

            foreach($leadtype1 as $key=>$value)
            {
                $leadtypec[] = $key;

            }

            foreach($leadtype1 as $key=>$value)
            {
                $leadtypev[] = $value;
            }

            //this is for converting array values into a string with new line

            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
            if(empty($leadtypec))
            {
                $leadtyped = " ";
                $leadtypef = " ";
                $leadtyped = trim($leadtyped);
                $leadtypef = trim($leadtypef);
            }
            else
            {
                $leadtyped = implode("\r\n", $leadtypec);
                $leadtypef = implode("\r\n", $leadtypev);
            }


            $leadtype->save();

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            if($leadtyped==" " || $leadtyped==' ' || $leadtyped=="" || $leadtyped=='')
            {
                $leadtyped = trim($leadtyped);
                $leadtypef = trim($leadtypef);
            }
            else
            {
                $leadtyped = $leadtyped."\r\n";
                $leadtypef = $leadtypef."\r\n";
            }

            //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
            $fields_updated = $leadtyped;
            $values_updated = $leadtypef;

            if((trim($fields_updated)!==""))
            {
                // dd($fields_updated);
                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "Fields Updated";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->field_updated = $fields_updated;
                $activity->value_updated = $values_updated;
                $activity->save();
            }

            session()->flash('message','Updated Successfully');
        }
        return redirect('/leadtypes');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $leadtype=leadtype::find($id);

        $leadtype_del = DB::table('leadtypes')->where('id',$id)->value('leadtypes');
        // dd($lang_del);

        $leadtype->delete();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $leadtype_del." Lead Type Deleted";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;

        $activity->save();

        session()->flash('message','Delete Successfully');
        return redirect('/leadtypes');
    }
}
