<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Response;

use Session;


class Coordinator_FieldExecutiveController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('coordinator_fieldexecutive');
    }

    public function index()
    {
        if(Auth::guard('admin')->check())
        {
            //retrieving the name of the user who is logged in
            $logged_in_user = Auth::user()->name;
            $designation="coordinator";

            $under = DB::table('employees')->where('FirstName',$logged_in_user)->value('under');
            $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('City');
            $serviceType = DB::table('employees')->where('id',$under)->value('Department');
            $log_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            // dd($serviceType);
            //assigning the possible statuses for the Coordinator
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            $status00 = "New";
            $status11 = "Processing";
            $status22  = "Awaiting Pickup";
            $status33  = "Out for Delivery";
            $status44  = "Ready to ship";
            $status55  = "Order Return";
            $status66  = "Cancelled";
            $status77  = "Delivered";
            $status88  = "Received Order Return";


            // find the count of all the leads which have been assigned to the logged in coordinator
            // a linkage exists between the vertical coordinations and employees table
            // - if a lead has been assigned to a coordinator then the coordinators employee id will be present in "vertical coordinations" table
            // - if not , then "Null" goes there
            // here, we are checking for the status "In Progress" which comes as "New" for the //coordinator
            $assignedcount = DB::table('leads')
            ->select('employees.*','services.*','leads.*')
            ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            ->join('employees','verticalcoordinations.empid','=','employees.id')
            ->join('services', 'leads.id', '=', 'services.Leadid')
            ->where('ServiceStatus',$status1)
            ->where('FirstName',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->count();

            $convertedcount = DB::table('leads')
            ->select('employees.*','services.*','leads.*')
            ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            ->join('employees','verticalcoordinations.empid','=','employees.id')
            ->join('services', 'leads.id', '=', 'services.Leadid')
            ->where('ServiceStatus',$status2)
            ->where('FirstName',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('employees.*','services.*','leads.*')
            ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            ->join('employees','verticalcoordinations.empid','=','employees.id')
            ->join('services', 'leads.id', '=', 'services.Leadid')
            ->where('ServiceStatus',$status3)
            ->where('FirstName',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('employees.*','services.*','leads.*')
            ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            ->join('employees','verticalcoordinations.empid','=','employees.id')
            ->join('services', 'leads.id', '=', 'services.Leadid')
            ->where('ServiceStatus',$status4)
            ->where('FirstName',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->count();



            //product details
            $processingcount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status11)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();



            $processingcount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status11)
            ->where('prodleads.empid',$log_id)

            ->orderBy('prodleads.id', 'DESC')
            ->count();


            $processingcount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status11)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();


            $processingcount = $processingcount1 + $processingcount2 + $processingcount3;




            $awaitingpickupcount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status22)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $awaitingpickupcount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status22)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $awaitingpickupcount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status22)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $awaitingpickupcount = $awaitingpickupcount1 + $awaitingpickupcount2 + $awaitingpickupcount3;

            $outfordeliverycount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status33)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $outfordeliverycount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status33)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $outfordeliverycount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status33)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $outfordeliverycount = $outfordeliverycount1 + $outfordeliverycount2 + $outfordeliverycount3;

            $readytoshipcount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status44)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $readytoshipcount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status44)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $readytoshipcount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status44)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $readytoshipcount = $readytoshipcount1 + $readytoshipcount2 + $readytoshipcount3;

            $orderreturncount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status55)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $orderreturncount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status55)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $orderreturncount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status55)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $orderreturncount = $orderreturncount1 + $orderreturncount2 + $orderreturncount3;

            $canceledcount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status66)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $canceledcount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status66)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $canceledcount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status66)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $canceledcount = $canceledcount1 + $canceledcount2 + $canceledcount3;

            $deliveredcount1= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status77)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $deliveredcount2= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatusrent',$status77)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $deliveredcount3= DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('pharmacies.pOrderStatus',$status77)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->count();

            $deliveredcount = $deliveredcount1 + $deliveredcount2 + $deliveredcount3;


            // product count end

            return view('admin.Coordinator_FieldExecutive',compact('assignedcount','convertedcount','droppedcount','deferredcount','designation','user_city','serviceType','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation'));

        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }



    }

    // This function is for the scenario when the "Coordinator" on their dashboard clicks on the status count image
    //and sees the details depending on that count for "each status"

    public function assigned(Request $request)
    {

        // retrieving the status that was passed as parameter in "admin.coordinator blade" so that it can retrieve the records
        // assigned for that status

        // dd($_GET['status']);

        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $employeeid=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        //general query for Viewing Leads on the basis of count and status
        $leads = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status)
        ->where('FirstName',$logged_in_user)
        ->where('verticalcoordinations.empid',$employeeid)
        ->orderBy('leads.id', 'DESC')
        ->paginate(50);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('services.AssignedTo',$logged_in_user)
                ->orwhere('verticalcoordinations.empid',$empname1)
                ->orderBy('leads.id', 'DESC')
                ->get();

                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                ->join('addresses','leads.id','=','addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status)
                ->where('FirstName',$logged_in_user)
                ->where('verticalcoordinations.empid',$employeeid)
                ->orderBy('leads.id', 'DESC')
                ->get();

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        // dd($leads);

        // Checking the status returned from the URL and redirecting to the appropriate view

        session()->put('status',$status);
        return view('Coordinator_FieldExecutive.index',compact('leads'));
    }


    public function assigned1()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;

        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        $log_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        if($status!="All")
        {
            $leads = DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            ->where('OrderStatus',$status)
            // ->orwhere('OrderStatusrent',$status)
            // ->orwhere('pharmacies.pOrderStatus',$status)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $leads = json_decode($leads,true);


            $leads1 = DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            // ->where('OrderStatus',$status)
            ->orwhere('OrderStatusrent',$status)
            // ->orwhere('pharmacies.pOrderStatus',$status)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $leads1 = json_decode($leads1,true);


            $leads2 = DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            // ->where('OrderStatus',$status)
            // ->orwhere('OrderStatusrent',$status)
            ->orwhere('pharmacies.pOrderStatus',$status)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $leads2 = json_decode($leads2,true);
            // dd($leads2);

            $leads_all = array_merge($leads,$leads1,$leads2);
        }
        else
        {

            $leads = DB::table('leads')
            ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
            ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'prodleads.Prodid', '=', 'products.id')
            ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
            ->join('orders', 'prodleads.orderid', '=', 'orders.id')
            // ->orwhere('OrderStatusrent',$status)
            // ->orwhere('pharmacies.pOrderStatus',$status)
            ->where('prodleads.empid',$log_id)
            ->orderBy('prodleads.id', 'DESC')
            ->get();

            $leads = json_decode($leads,true);


            $leads_all = $leads;
        }

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));


        $leads1 = $leads_all;
        $count = count($leads1);
        // dd($count);
        // dd($leads_all);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            $eid=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {

                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('prodleads.empid','=',$eid)
                ->orderBy('prodleads.id', 'DESC')
                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatus',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);

                $leads2  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatusrent',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads2 = json_decode($leads2,true);

                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('pharmacies.pOrderStatus',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads2,$leads3);


                usort($leads_all, make_comparer('id'));

                $leads = $leads_all;


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        session()->put('name',$logged_in_user);

        return view('Coordinator_FieldExecutive.productindex',compact('leads1','d','count','desig'));
    }

}
