<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ptcondition;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class ptconditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::guard('admin')->check())
        {
           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $ptconditions= ptcondition::paginate(50);
                    return view('ptcondition.index',compact('ptconditions'));
                   }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                   return view('ptcondition.create');
                 }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ptcondition = new ptcondition;
        $this->validate($request,[
                'conditiontypes'=>'required',
            ]);
       $ptcondition->conditiontypes=$request->conditiontypes;

       $ptcondition->save();

       $ptcid = DB::table('ptconditions')->max('id');
       //    dd($langid);

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $request->conditiontypes." Patient Condition Added ";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;
       $activity->save();

       return redirect('/ptconditions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                     $ptcondition = ptcondition::find($id);
                     return view('ptcondition.edit',compact('ptcondition'));
                }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $c=DB::table('ptconditions')->where('id',$id)->value('conditiontypes');
        if($request->conditiontypes!=$c)
        {
             $ptcondition = ptcondition::find($id);
              $ptcondition->conditiontypes=$request->conditiontypes;

              $pt = $ptcondition->getDirty();
              // dd($lang);

              foreach($pt as $key=>$value)
              {
                  $ptc[] = $key;

              }

              foreach($pt as $key=>$value)
              {
                  $ptv[] = $value;
              }

              //this is for converting array values into a string with new line

              //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
              if(empty($ptc))
              {
                  $ptd = " ";
                  $ptf = " ";
                  $ptd = trim($ptd);
                  $ptf = trim($ptf);
              }
              else
              {
                  $ptd = implode("\r\n", $ptc);
                  $ptf = implode("\r\n", $ptv);
              }


             $ptcondition->save();

             //retrieving the session id of the person who is logged in
             $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

             //retrieving the username of the presently logged in user
             $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

             // retrieving the employee id of the presently logged in user
             $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

             //retrieving the log id of the person who logged in just now
             $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

             //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation

             if($ptd==" " || $ptd==' ' || $ptd=="" || $ptd=='')
             {
                 $ptd = trim($ptd);
                 $ptf = trim($ptf);
             }
             else
             {
                 $ptd = $ptd."\r\n";
                 $ptf = $ptf."\r\n";
             }

             $fields_updated = $ptd;
             $values_updated = $ptf;

             if((trim($fields_updated)!==""))
             {
                 // dd($fields_updated);
                 $activity = new Activity;
                 $activity->emp_id = $emp_id;
                 $activity->activity = "Fields Updated";
                 $activity->activity_time = new \DateTime();
                 $activity->log_id = $log_id;
                 $activity->field_updated = $fields_updated;
                 $activity->value_updated = $values_updated;

                 $activity->save();
             }

             session()->flash('message','Updated Successfully');
        }
       return redirect('/ptconditions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ptcondition=ptcondition::find($id);

        $pt_del = DB::table('ptconditions')->where('id',$id)->value('conditiontypes');
        // dd($lang_del);

       $ptcondition->delete();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $pt_del." Patient Condtion Deleted";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;

       $activity->save();

       session()->flash('message','Delete Successfully');
       return redirect('/ptconditions');
    }
}
