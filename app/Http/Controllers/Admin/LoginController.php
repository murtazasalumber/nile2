<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use DB;
use App\Activity;
use App\Log;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


 



 protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        
// dd($request->logged_in_user);
 session()->put('token',$request->token); 
 session()->put('email',$request->email); 
 session()->put('password',$request->password);
 session()->put('logged_in_user',$request->logged_in_user);


        $this->clearLoginAttempts($request);

        foreach ($this->guard('admin')->user()->role as $role) {
           if($role->name == 'admin')
           {

              return redirect('admin/home');
           }elseif($role->name == 'customercare')
           {
            return redirect('admin/customercare');
           }elseif($role->name == 'Management')
           {
            return redirect('admin/management');
           }elseif($role->name == 'Coordinator')
           {
            return redirect('admin/coordinator');
           }elseif($role->name == 'Care_provider')
           {
            return redirect('admin/careprovider');
           }elseif($role->name == 'super_user')
           {
            return redirect('admin/superuser');
           }elseif($role->name == 'vertical')
           {
            return redirect('admin/vertical');
           }
           elseif($role->name == 'Referal_Partner')
           {
            return redirect('admin/referalpartner');
           }
           elseif($role->name == 'Product Manager')
           {
            return redirect('admin/productmanager');
           }
           elseif($role->name == 'Pharmacy Manager')
           {
            return redirect('admin/pharmacymanager');
           }
           elseif($role->name == 'Field Officer')
           {
            return redirect('admin/fieldofficer');
           }
           elseif($role->name == 'Field Executive')
           {
            return redirect('admin/fieldexecutive');
           }elseif($role->name == 'Marketing')
           {
            return redirect('admin/marketing');
           }
           elseif($role->name == 'Vertical_ProductManager')
           {
            return redirect('admin/Vertical_ProductManager');
           }
           elseif($role->name == 'Vertical_PharmacyManager')
           {
            return redirect('admin/Vertical_PharmacyManager');
           }
           elseif($role->name == 'Vertical_ProductSelling')
           {
            return redirect('admin/Vertical_ProductSelling');
           }
           elseif($role->name == 'Vertical_ProductRental')
           {
            return redirect('admin/Vertical_ProductRental');
           }
           elseif($role->name == 'Vertical_Product_Pharmacy')
           {
            return redirect('admin/Vertical_Product_Pharmacy');
           }
           elseif($role->name == 'Vertical_ProductSelling_Pharmacy')
           {
            return redirect('admin/Vertical_ProductSelling_Pharmacy');
           }
           elseif($role->name == 'Vertical_ProductRental_Pharmacy')
           {
            return redirect('admin/Vertical_ProductRental_Pharmacy');
           }
           elseif($role->name == 'Coordinator_ProductManager')
           {
            return redirect('admin/Coordinator_ProductManager');
           }
           elseif($role->name == 'Coordinator_PharmacyManager')
           {
            return redirect('admin/Coordinator_PharmacyManager');
           }
           elseif($role->name == 'Coordinator_ProductSelling')
           {
            return redirect('admin/Coordinator_ProductSelling');
           }
           elseif($role->name == 'Coordinator_ProductRental')
           {
            return redirect('admin/Coordinator_ProductRental');
           }
           elseif($role->name == 'Coordinator_Product_Pharmacy')
           {
            return redirect('admin/Coordinator_Product_Pharmacy');
           }
           elseif($role->name == 'Coordinator_ProductSelling_Pharmacy')
           {
            return redirect('admin/Coordinator_ProductSelling_Pharmacy');
           }
           elseif($role->name == 'Coordinator_ProductRental_Pharmacy')
           {
            return redirect('admin/Coordinator_ProductRental_Pharmacy');
           }
           elseif($role->name == 'Vertical_FieldOfficer')
           {
            return redirect('admin/Vertical_FieldOfficer');
           }
           elseif($role->name == 'Vertical_FieldExecutive')
           {
            return redirect('admin/Vertical_FieldExecutive');
           }
           elseif($role->name == 'Coordinator_FieldOfficer')
           {
            return redirect('admin/Coordinator_FieldOfficer');
           }
           elseif($role->name == 'Coordinator_FieldExecutive')
           {
            return redirect('admin/Coordinator_FieldExecutive');
           }
           elseif($role->name == 'Branch Head')
           {
            return redirect('admin/BranchHead');
           }



        }

    }


     /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm(Request $request)
    {
      // dd($request);
      $token=NULL;
      $email=NULL;
      $password=NULL;
      $logged_in_user=NULL;
      
      if($request->token)
      {
        $token=$request->token;
        $email=$request->email;
        $password=$request->password;
        $logged_in_user=$request->logged_in_user;
        // dd($request->logged_in_user);
      }
      
        return view('admin.login',compact('token','email','password','logged_in_user'));
      
    }

     /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function logout(Request $request)
    {
      /*
      Code for storing Logout info starts here - by jatin
      */



      //Here we are trying to retrieve the session id of the person who is logged in
      $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
      $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

      $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
      $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

      $activity = new Activity;
      $activity->emp_id = $emp_id;
      $activity->activity = "Log Out";
      $activity->activity_time = new \DateTime();
      $activity->log_id = $log_id;
      $activity->save();

      // //Here we want to get the created_at column value for the login session
      // $log_created_at = DB::table('logs')->where('session_id', session()->getId())->value('created_at');
      //
      //
      //
      //   $time = time();
      //   $logout_atc = date('Y-m-d H:i:s', $time);
      //   $logout_ats = ['logout_time' => $logout_atc];
      //   $status = "Logged out";
      //   $logout_atk = ['login_time' => $log_created_at,'status'=>$status];
      //   $status1 = "Logged in";
      //
      //
      //
      //   //Here we are updating the login_at and logout_at fields with their appropriate values
      //   DB::table('logs')->where('session_id',$log_session_id)->update($logout_ats);
      //   DB::table('logs')->where([['session_id','=',$log_session_id],['status','=',$status1]])->update($logout_atk);
      //



      /*
      Code for logging info ends here - by jatin
      */


      // code to redirect back to admin or management login for impersonet

      
//       if($request->token1)
//       {
//         $token=$request->token1;
//          $request->session()->flush();

//         $request->session()->regenerate();

// //           $a="Murtaza.s@healthheal.in";
// // $b="$2y$10$9gBGcTd18cRao4h9GdpV6.Gm0A3i.oUwI7.u4XA8ipX10Gn0rIZmC";

// $logged_in_user=$request->logged_in_user;
// $email=DB::table('admins')->where('name',$logged_in_user)->value('email');
// $password=DB::table('admins')->where('name',$logged_in_user)->value('password');

// // if($logged_in_user != NULL)
// // {
// //   $this->guard()->logout();

// //         $request->session()->flush();

// //         $request->session()->regenerate();

// //         return redirect('/');
// // }

// if($logged_in_user == NULL)
// {
//    $this->guard()->logout();

//         $request->session()->flush();

//         $request->session()->regenerate();

//         return redirect('/');

// }
// else
// {

// return Redirect::action('Admin\LoginController@login', array('email' => $email,'password' => $password,'token'=>$token));
// }
//       }
//       else
//       {


        $this->guard()->logout();

        $request->session()->flush();
        
        $request->session()->regenerate();

        return redirect('/');


      // }
    }





}
