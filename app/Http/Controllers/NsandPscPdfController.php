<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use \PDF;

class NsandPscPdfController extends Controller
{
    public function index(Request $request)
    {
        $leadid = $_GET['leadid'];
        if($request->has('download'))
        {
            $leaddata=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id','=',$leadid)
            ->orderBy('leads.id', 'DESC')
            ->get();

            $assessmentdata = DB::table('assessments')->where('Leadid',$leadid)->get();

            $vitalsignsdata = DB::table('vitalsigns')->where('leadid',$leadid)->get();

            $generalconditionsdata = DB::table('generalconditions')->where('Leadid',$leadid)->get();

            $orientationsdata = DB::table('orientations')->where('leadid',$leadid)->get();

            $memoryintactsdata = DB::table('memoryintacts')->where('leadid',$leadid)->get();

            $communicationsdata = DB::table('communications')->where('leadid',$leadid)->get();

            $visionhearingsdata = DB::table('visionhearings')->where('leadid',$leadid)->get();

            $respiratorydata = DB::table('respiratories')->where('leadid',$leadid)->get();

            $positiondata = DB::table('positions')->where('leadid',$leadid)->get();

            $circulatorydata = DB::table('circulatories')->where('leadid',$leadid)->get();

            $denturesdata = DB::table('dentures')->where('leadid',$leadid)->get();

            $nutritiondata = DB::table('nutrition')->where('leadid',$leadid)->get();

            $abdomendata = DB::table('abdomens')->where('leadid',$leadid)->get();

            $genitosdata = DB::table('genitos')->where('leadid',$leadid)->get();

            $fecaldata = DB::table('fecals')->where('leadid',$leadid)->get();

            $mobilitydata = DB::table('mobilities')->where('leadid',$leadid)->get();

            $generalhistorydata = DB::table('generalhistories')->where('leadid',$leadid)->get();



            // dd($abdomendata);
            $pdf = PDF::loadView('pdf/NS_and_PSC_pdfview',compact('leaddata','assessmentdata','vitalsignsdata','generalconditionsdata','orientationsdata','memoryintactsdata','communicationsdata'
        ,'visionhearingsdata','respiratorydata','positiondata','circulatorydata','denturesdata','nutritiondata','abdomendata','genitosdata','fecaldata','mobilitydata',
    'generalhistorydata'));
            return $pdf->download("Assessment_Form_for_$leadid.pdf");
        }

        return view('pdf/pdfview');
    }
}
