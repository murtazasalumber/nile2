<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\gender;
use DB;
use Auth;

class gendersController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {
            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
               
                if($check=="Admin" || $check=="Management")
                {
                    $genders= gender::paginate(50);
                    return view('gender.index',compact('genders'));
                }
                 else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
               
                if($check=="Admin" || $check=="Management")
                {
                     return view('gender.create');
                 }   
                 else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {


        $gender = new gender;
        $this->validate($request,[
            'gendertypes'=>'required',
        ]);
        $gender->gendertypes=$request->gendertypes;

        $gender->save();
        return redirect('/genders');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
               
                if($check=="Admin" || $check=="Management")
                {
                    $gender = gender::find($id);
                    return view('gender.edit',compact('gender'));
                }
                else
                {
                    return redirect('/admin');
                }

        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $c=DB::table('genders')->where('id',$id)->value('gendertypes');
        if($request->gendertypes!=$c)
        {
            $gender = gender::find($id);
            $gender->gendertypes=$request->gendertypes;
            $gender->save();
            session()->flash('message','Updated Successfully');
        }
        return redirect('/genders');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $gender=gender::find($id);
        $gender->delete();
        session()->flash('message','Delete Successfully');
        return redirect('/genders');
    }
}
