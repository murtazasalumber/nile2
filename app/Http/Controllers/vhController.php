<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use DB;
use App\abdomen;
use App\communication;
use App\circulatory;
use App\denture;
use App\extremitie;
use App\fecal;
use App\genito;
use App\memoryintact;
use App\mobility;
use App\nutrition;
use App\orientation;
use App\vitalsign;
use App\respiratory;
use App\visionhearing;
use App\mother;
use App\mathruthvam;
use App\physiotheraphy;
use App\physioreport;
use App\assessment;
use App\product;
use App\generalhistory;
use App\verticalref;
use App\refer;
use App\lead;
use App\address;
use App\verticalcoordination;
use App\employee;
use App\service;
use App\denverscale;
use App\position;
use App\Activity;
use App\mar;
use Session;
use App\Log;
use App\generalcondition;
use Mail;
use Auth;


class vhController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    // This function is used to redirect different users to their respective pages when they click on "View Leads"
    public function index()
    {
        if(Auth::guard('admin')->check())
        {


            // route being used is :: Route::resource('vh','vhController');

            if (session()->has('name'))
            {

                $name1=session()->get('name');
            }
            else{
                $name1=$_GET['name'];
            }

            //extract the logged in users "ID","Designation", "City" and "Department"
            $empname=DB::table('employees')->where('FirstName',$name1)->select('id','Designation','city','Department')->get();
            $empname=json_decode($empname);
            $empname1= $empname[0]->id;
            $empname2= $empname[0]->Designation;
            $empname3= $empname[0]->city;
            $empname4= $empname[0]->Department;

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');

            //if the employee is the Vertical Head
            if($empname2=='Vertical Head')
            {


                //if the vertical head doesn't belong to "Bangalore"
                // if($empname2!="Bengaluru")
                // {
                //     $leads=DB::table('leads')
                //     ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                //     ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                //     ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                //     ->join('services', 'leads.id', '=', 'services.LeadId')
                //     ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                //     ->where('services.branch',$empname3)
                //     ->orderBy('leads.id', 'DESC')
                //     ->paginate(50);

                //     session()->put('name',$name1);
                //     session()->put('status',"All");
                //     return view('verticalheads.index',compact('leads'));
                // }

                //if the Service Type of the Vertical Head is Mathrutvam
                if($empname4=="Mathrutvam - Baby Care")
                {
                    $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                    ->where('services.branch',$empname3)
                    ->where('services.ServiceType',$empname4)
                    ->orwhere('services.ServiceType','Infant Care')
                    ->orwhere('services.ServiceType','Nanny Care')
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(50);


                    session()->put('name',$name1);
                    session()->put('status',"All");

                    if($roles=="Vertical_ProductSelling")
                    {
                        return view('Vertical_ProductSelling.index',compact('leads'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductRental")
                        {

                            return view('Vertical_ProductRental.index',compact('leads'));
                        }
                        else
                        {
                            if($roles=="Vertical_ProductManager")
                            {
                                return view('Vertical_ProductManager.index',compact('leads'));
                            }
                            else
                            {
                                if($roles=="Vertical_PharmacyManager")
                                {
                                    return view('Vertical_PharmacyManager.index',compact('leads'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy")
                                    {
                                        return view('Vertical_ProductSelling_Pharmacy.index',compact('leads'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy")
                                        {

                                            return view('Vertical_ProductRental_Pharmacy.index',compact('leads'));
                                        }
                                        else
                                        {
                                            if($roles=="Vertical_Product_Pharmacy")
                                            {
                                                return view('Vertical_Product_Pharmacy.index',compact('leads'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer")
                                                {
                                                    return view('Vertical_FieldOfficer.index',compact('leads'));
                                                }
                                                else{
                                                    if($roles=="Vertical_FieldExecutive")
                                                    {
                                                        return view('Vertical_FieldExecutive.index',compact('leads'));
                                                    }
                                                    else {

                                                        return view('verticalheads.index',compact('leads'));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }






                }
                else
                {
                    //if the Service Type of the Vertical Head is Nursing Services
                    if($empname4=="Nursing Services")

                    {

                        $leads=DB::table('leads')
                        ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                        ->where('services.branch',$empname3)
                        ->where('services.ServiceType',$empname4)
                        ->orwhere('services.ServiceType','Respiratory Care')
                        ->orwhere('services.ServiceType','Intravenous Therapy')
                        ->orwhere('services.ServiceType','Medication Administration')
                        ->orwhere('services.ServiceType','Nutrition')
                        ->orwhere('services.ServiceType','Assisting in Elimination')
                        ->orwhere('services.ServiceType','Post Operative Care')
                        ->orwhere('services.ServiceType','Diabetic/Wound Care')
                        ->orwhere('services.ServiceType','Rehabilitation')
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);

                        session()->put('name',$name1);                session()->put('status',"All");

                        if($designation2=="Product Manager" && $department2=="Product - Selling" && $designation3!="Pharmacy Manager")
                        {
                            return view('Vertical_ProductSelling.index',compact('leads'));
                        }
                        else
                        {
                            if($designation2=="Product Manager" && $department2=="Product - Rental" && $designation3!="Pharmacy Manager")
                            {
                                return view('Vertical_ProductRental.index',compact('leads'));
                            }
                            else
                            {
                                if($designation2=="Product Manager" && $department2=="Product" && $designation3!="Pharmacy Manager")
                                {
                                    return view('Vertical_ProductManager.index',compact('leads'));
                                }
                                else
                                {
                                    if($designation2=="Pharmacy Manager" && $department2=="Product" && $designation3!="Product Manager")
                                    {
                                        return view('Vertical_PharmacyManager.index',compact('leads'));
                                    }
                                    else
                                    {
                                        if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Selling") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Selling"))
                                        {
                                            return view('Vertical_ProductSelling_Pharmacy.index',compact('leads'));
                                        }
                                        else
                                        {
                                            if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Rental") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Rental"))
                                            {
                                                return view('Vertical_ProductRental_Pharmacy.index',compact('leads'));
                                            }
                                            else
                                            {
                                                if(($designation2=="Product and Pharmacy Manager" && $department2=="Product") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product"))
                                                {
                                                    return view('Vertical_Product_Pharmacy.index',compact('leads'));
                                                }
                                                else
                                                {
                                                    if($designation3=="Field Officer" && $department2=="Product" )
                                                    {
                                                        return view('Vertical_FieldOfficer.index',compact('leads'));
                                                    }
                                                    else{
                                                        if($designation3=="Field Executive" && $department2=="Product" )
                                                        {
                                                            return view('Vertical_FieldExecutive.index',compact('leads'));
                                                        }
                                                        else {
                                                            return view('verticalheads.index',compact('leads'));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {

                        //if the Service Type of the Vertical Head is PSC
                        if($empname4=="Personal Supportive Care")
                        {

                            $leads=DB::table('leads')
                            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->where('services.branch',$empname3)
                            ->where('services.ServiceType',$empname4)
                            ->orwhere('services.ServiceType','Personal Care')
                            ->orwhere('services.ServiceType','Companionship')
                            ->orwhere('services.ServiceType','Live -in Care')
                            ->orwhere('services.ServiceType','Palliative/End of Life Care')
                            ->orwhere('services.ServiceType','Alzheimers and Dementia')
                            ->orderBy('leads.id', 'DESC')
                            ->paginate(50);

                            session()->put('name',$name1);                session()->put('status',"All");

                            if($designation2=="Product Manager" && $department2=="Product - Selling" && $designation3!="Pharmacy Manager")
                            {

                                return view('Vertical_ProductSelling.index',compact('leads'));
                            }
                            else
                            {
                                if($designation2=="Product Manager" && $department2=="Product - Rental" && $designation3!="Pharmacy Manager")
                                {
                                    return view('Vertical_ProductRental.index',compact('leads'));
                                }
                                else
                                {
                                    if($designation2=="Product Manager" && $department2=="Product" && $designation3!="Pharmacy Manager")
                                    {
                                        return view('Vertical_ProductManager.index',compact('leads'));
                                    }
                                    else
                                    {
                                        if($designation2=="Pharmacy Manager" && $department2=="Product" && $designation3!="Product Manager")
                                        {
                                            return view('Vertical_PharmacyManager.index',compact('leads'));
                                        }
                                        else
                                        {
                                            if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Selling") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Rental"))
                                            {
                                                return view('Vertical_ProductSelling_Pharmacy.index',compact('leads'));
                                            }
                                            else
                                            {
                                                if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Rental") ||($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Rental"))
                                                {
                                                    return view('Vertical_ProductRental_Pharmacy.index',compact('leads'));
                                                }
                                                else
                                                {
                                                    if(($designation2=="Product and Pharmacy Manager" && $department2=="Product") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product"))
                                                    {
                                                        return view('Vertical_Product_Pharmacy.index',compact('leads'));
                                                    }
                                                    else
                                                    {
                                                        if($designation3=="Field Officer" && $department2=="Product" )
                                                        {
                                                            return view('Vertical_FieldOfficer.index',compact('leads'));
                                                        }
                                                        else{
                                                            if($designation3=="Field Executive" && $department2=="Product" )
                                                            {
                                                                return view('Vertical_FieldExecutive.index',compact('leads'));
                                                            }
                                                            else {
                                                                return view('verticalheads.index',compact('leads'));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //if the Service Type of the Vertical Head is Physiotheraphy
                        else
                        {
                            if($empname4=="Physiotherapy - Home")
                            {
                                $leads=DB::table('leads')
                                ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                ->join('services', 'leads.id', '=', 'services.LeadId')
                                ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                                ->where('services.branch',$empname3)
                                ->where('services.ServiceType',$empname4)
                                ->orwhere('services.ServiceType','Physiotherapy')
                                ->orderBy('leads.id', 'DESC')
                                ->paginate(50);

                                session()->put('name',$name1);                session()->put('status',"All");


                                if($designation2=="Product Manager" && $department2=="Product - Selling" && $designation3!="Pharmacy Manager")
                                {

                                    return view('Vertical_ProductSelling.index',compact('leads'));
                                }
                                else
                                {
                                    if($designation2=="Product Manager" && $department2=="Product - Rental" && $designation3!="Pharmacy Manager")
                                    {
                                        return view('Vertical_ProductRental.index',compact('leads'));
                                    }
                                    else
                                    {
                                        if($designation2=="Product Manager" && $department2=="Product" && $designation3!="Pharmacy Manager")
                                        {
                                            return view('Vertical_ProductManager.index',compact('leads'));
                                        }
                                        else
                                        {
                                            if($designation2=="Pharmacy Manager" && $department2=="Product" && $designation3!="product Manager")
                                            {
                                                return view('Vertical_PharmacyManager.index',compact('leads'));
                                            }
                                            else
                                            {
                                                if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Selling") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Selling"))
                                                {
                                                    return view('Vertical_ProductSelling_Pharmacy.index',compact('leads'));
                                                }
                                                else
                                                {
                                                    if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Rental") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Rental"))
                                                    {
                                                        return view('Vertical_ProductRental_Pharmacy.index',compact('leads'));
                                                    }
                                                    else
                                                    {
                                                        if(($designation2=="Product and Pharmacy Manager" && $department2=="Product") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product"))
                                                        {
                                                            return view('Vertical_Product_Pharmacy.index',compact('leads'));
                                                        }
                                                        else
                                                        {
                                                            if($designation3=="Field Officer" && $department2=="Product" )
                                                            {
                                                                return view('Vertical_FieldOfficer.index',compact('leads'));
                                                            }
                                                            else{
                                                                if($designation3=="Field Executive" && $department2=="Product" )
                                                                {
                                                                    return view('Vertical_FieldExecutive.index',compact('leads'));
                                                                }
                                                                else {
                                                                    return view('verticalheads.index',compact('leads'));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                //If the designation of loggged in user is "Management"
                if($empname2=='Management')
                {
                    $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(50);

                    session()->put('name',$name1);                session()->put('status',"All");

                    return view('management.index',compact('leads'));
                }
                else
                {
                    //If the designation of loggged in user is "Admin"
                    if($empname2=='Admin')
                    {
                        $leads=DB::table('leads')
                        ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);

                        session()->put('name',$name1);                session()->put('status',"All");

                        return view('admin.index',compact('leads'));
                    }
                    //If the designation of loggged in user is "Coordinator"
                    else
                    {
                        if($empname2=='Branch Head')
                        {
                            $leads=DB::table('leads')
                            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->where('Branch',$empname3)
                            ->orderBy('leads.id', 'DESC')
                            ->paginate(50);

                            session()->put('name',$name1);
                            session()->put('status',"All");

                            return view('BranchHead.index',compact('leads'));
                        }
                        else
                        {
                            $leads=DB::table('leads')
                            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->where('services.AssignedTo',$name1)
                            ->orwhere('verticalcoordinations.empid',$empname1)
                            ->orderBy('leads.id', 'DESC')
                            ->paginate(50);


                            session()->put('name',$name1);                session()->put('status',"All");

                            if($designation2=="Product Manager" && $department2=="Product - Selling" && $designation3!="Pharmacy Manager")
                            {

                                return view('Coordinator_ProductSelling.index',compact('leads'));
                            }
                            else
                            {
                                if($designation2=="Product Manager" && $department2=="Product - Rental" && $designation3!="Pharmacy Manager")
                                {
                                    return view('Coordinator_ProductRental.index',compact('leads'));
                                }
                                else
                                {
                                    if($designation2=="Product Manager" && $department2=="Product" && $designation3!="Pharmacy Manager")
                                    {

                                        return view('Coordinator_ProductManager.index',compact('leads'));
                                    }
                                    else
                                    {
                                        if($designation2=="Pharmacy Manager" && $department2=="Product" && $designation3!="product Manager")
                                        {
                                            return view('Coordinator_PharmacyManager.index',compact('leads'));
                                        }
                                        else
                                        {
                                            if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Selling") ||($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Selling"))
                                            {
                                                return view('Coordinator_ProductSelling_Pharmacy.index',compact('leads'));
                                            }
                                            else
                                            {
                                                if(($designation2=="Product and Pharmacy Manager" && $department2=="Product - Rental") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product - Rental"))
                                                {
                                                    return view('Coordinator_ProductRental_Pharmacy.index',compact('leads'));
                                                }
                                                else
                                                {
                                                    if(($designation2=="Product and Pharmacy Manager" && $department2=="Product") || ($designation2=="Product Manager" && $designation3=="Pharmacy Manager" && $department2=="Product"))
                                                    {
                                                        return view('Coordinator_Product_Pharmacy.index',compact('leads'));
                                                    }
                                                    else
                                                    {
                                                        if($designation3=="Field Officer" && $department2=="Product" )
                                                        {
                                                            return view('Coordinator_FieldOfficer.index',compact('leads'));
                                                        }
                                                        else{
                                                            if($designation3=="Field Executive" && $department2=="Product" )
                                                            {
                                                                return view('Coordinator_FieldExecutive.index',compact('leads'));
                                                            }
                                                            else {
                                                                return view('co.index',compact('leads'));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
            }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }


    // This function is for adding the Search functionality on the View Leads page and Individual Coutns page for Customer care, Admin, Management, Vertical Heads
    public function filter(Request $request)
    {

        // the route for this function : Route::get('lead','vhController@filter');
        // dd($keyword1, $filter1, $status1,$desig);


        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;


        // return view('alldata',compact('keyword1','filter1'));
        //$data = DB::table('leads')->where($filter1, 'like', $keyword1 .'%')->get();
        //$data = DB::table('leads')->Where($filter1, 'like',   $keyword1 . '%')->get();
        // $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')->join('addresses', 'leads.id', '=', 'addresses.leadid')->join('services', 'leads.id', '=', 'services.LeadId')->Where($filter1, 'like',   $keyword1 . '%')->get();

        //$data=DB::table('leads')->get();

        // $lead=DB::table('leads')
        //      ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //      ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //      ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //      ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //      ->join('services', 'leads.id', '=', 'services.LeadId')
        //      ->where('verticalcoordinations.empid',$empname1)
        //      ->orderBy('leads.id', 'DESC')
        //      ->get();

        $logged_in_user = Auth::guard('admin')->user()->name;


        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $employeeid = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');



        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $under=DB::table('employees')->where('FirstName',$logged_in_user)->value('under');

        $vcity = DB::table('employees')->where('id',$under)->value('city');
        // dd($under);
        $serviceType1= DB::table('employees')->where('id',$under)->value('Department');

        $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        // dd($keyword1, $filter1, $status1);

        // dd($city);
        // if($desig=="Customer Care")
        // {
        //     $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
        //     ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //     ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //     ->join('services', 'leads.id', '=', 'services.LeadId')
        //     ->Where($filter1, 'like',   $keyword1 . '%')
        //     ->where('services.ServiceStatus',$status1)
        //     ->where('leads.createdby',$logged_in_user)
        //     ->orderBy('leads.id', 'DESC')->paginate(200);
        //
        //     return view('cc.alldata',compact('data1','keyword1','filter1'));
        // }

        //if the user, deletes the value that he/she entered in the "keyword" field, the page should reload with the "non-filtered" content
        if($keyword1 == "")
        {

            return view('verticalheads.index1');
        }
        // dd($status1);


        //checking whether the status received from the URL is "NULL" or not
        if($status1!=NULL)
        {
            //search filter for Admin or Management
            if($desig=="Admin" || $desig=="Management")
            {

                //if we have clicked on "View Service Leads" in "Admin or Management" and we wish to apply search functionality there, then status is ALL
                if($status1=="All")
                {
                    //this will retreive the values based on the type of filter(dropdown) and the keyword(like Customer name, Assigned to, Lead source, Service Type) as entered by the user
                    $data1 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(200);
                    // dd($keyword1, $filter1, $status1);
                    // dd($data1);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1','desig'));
                }
                //if we have clicked on "View Individual Counts (like New, In progress etc.)" in "Admin or Management" and we wish to apply search functionality there

                else
                {

                    $data1 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('ServiceStatus',$status1)
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(200);



                    return view('admin.alldata',compact('data1','keyword1','filter1','status1','desig'));
                }
            }



            if($desig=="Branch Head")
            {

                //if we have clicked on "View Service Leads" in "Admin or Management" and we wish to apply search functionality there, then status is ALL
                if($status1=="All")
                {
                    //this will retreive the values based on the type of filter(dropdown) and the keyword(like Customer name, Assigned to, Lead source, Service Type) as entered by the user
                    $data1 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('Branch',$city)
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(200);

                    // dd($keyword1,$status1,$filter1,$data1,$desig);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1','desig'));
                }
                //if we have clicked on "View Individual Counts (like New, In progress etc.)" in "Admin or Management" and we wish to apply search functionality there

                else
                {

                    $data1 = DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('Branch',$city)
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('ServiceStatus',$status1)
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(200);



                    return view('admin.alldata',compact('data1','keyword1','filter1','status1','desig'));
                }
            }



            //This search filter is for vertical heads except "Bangalore"
            // if($city=="Hubballi-Dharwad" || $city=="Hyderabad" || $city=="Pune" || $city=="Chennai")
            //         {
            //             //if it's a vertical head
            //             if($desig=="Vertical Head")
            //             {
            //                 // dd($desig);
            //                 //assign all "Service Types" to vertical heads except the ones from "bangalore"
            //                 $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            //                 // dd($desig);

            //                 //refer to comments in "customer care" ifs
            //                 if($status1=="All")
            //                 {
            //                     $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
            //                     ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            //                     ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            //                     ->join('services', 'leads.id', '=', 'services.LeadId')
            //                     ->Where($filter1, 'like',   $keyword1 . '%')
            //                     ->where('Branch',$city)
            //                     ->wherein('ServiceType',$Servicesarray)
            //                     ->orderBy('leads.id', 'DESC')->paginate(200);

            //                     return view('verticalheads.alldata',compact('data1','keyword1','filter1'));
            //                 }
            //                 else
            //                 {
            //                     $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
            //                     ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            //                     ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            //                     ->join('services', 'leads.id', '=', 'services.LeadId')
            //                     ->where('Branch',$city)
            //                     ->where('ServiceStatus',$status1)
            //                     ->wherein('ServiceType',$Servicesarray)
            //                     ->Where($filter1, 'like',   $keyword1 . '%')
            //                     ->orderBy('leads.id', 'DESC')->paginate(200);

            //                     return view('verticalheads.alldata',compact('data1','keyword1','filter1'));
            //                 }
            //             }
            //             //if it's a Coordinator from all cities except "Bangalore"
            //             else
            //             {

            //                 if($serviceType1=="Mathrutvam - Baby Care"){
            //                     $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
            //                 }
            //                 if($serviceType1=="Personal Supportive Care")
            //                 {
            //                     $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
            //                 }
            //                 if($serviceType1=="Nursing Services")
            //                 {
            //                     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
            //                 }
            //                 if($serviceType1=="Physiotherapy - Home")
            //                 {
            //                     // dd($serviceType);
            //                     $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
            //                 }

            //                 //refer to comments in "customer care" if's
            //                 if($status1=="All")
            //                 {

            //                     $data1 = DB::table('leads')
            //                     ->select('employees.*','services.*','verticalcoordinations.*','leads.*')
            //                     ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            //                     ->join('employees','verticalcoordinations.empid','=','employees.id')
            //                     ->join('services', 'leads.id', '=', 'services.Leadid')
            //                     ->wherein('ServiceType',$Servicesarray)
            //                     ->Where($filter1, 'like',   $keyword1 . '%')
            //                     ->orderBy('leads.id', 'DESC')
            //                     ->paginate(50);

            //                     return view('verticalheads.alldata',compact('data1','keyword1','filter1'));
            //                 }
            //                 else
            //                 {
            //                     $data1 = DB::table('leads')
            //                     ->select('employees.*','services.*','verticalcoordinations.*','leads.*')
            //                     ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            //                     ->join('employees','verticalcoordinations.empid','=','employees.id')
            //                     ->join('services', 'leads.id', '=', 'services.Leadid')
            //                     ->where('ServiceStatus',$status1)
            //                     ->wherein('ServiceType',$Servicesarray)
            //                     ->Where($filter1, 'like',   $keyword1 . '%')
            //                     ->orderBy('leads.id', 'DESC')
            //                     ->paginate(50);

            //                     return view('verticalheads.alldata',compact('data1','keyword1','filter1'));
            //                 }
            //             }
            //         }
            //if the people belong to "Bangalore"
            else
            {
                //if it's a vertical head
                if($desig=="Vertical Head")
                {
                    // dd($keyword1, $status1, $filter1,$desig);

                    if($serviceType=="Physiotherapy - Home")
                    {
                        // dd($serviceType);
                        $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                        // dd($keyword1, $status1, $filter1,$desig);
                        //refer to comments in "customer care" ifs
                        if($status1=="All")
                        {


                            if($filter1=="Alternatenumber")
                            {
                                // echo "hello";
                                $filter1= 'leads.Alternatenumber';
                            }



                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1.'%')
                            // ->where('Branch',$city)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);



                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }
                        else
                        {
                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->where('ServiceStatus',$status1)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }

                    }

                    else if($serviceType=="Nursing Services")
                    {
                        $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                        if($status1=="All")
                        {

                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }
                        else
                        {
                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->where('ServiceStatus',$status1)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }


                    }

                    else if($serviceType=="Personal Supportive Care")
                    {
                        $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

                        if($status1=="All")
                        {

                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }
                        else
                        {
                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->where('ServiceStatus',$status1)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }

                    }

                    // checking if the service type is "Mathrutvam" and calculating counts according to that
                    else if($serviceType=="Mathrutvam - Baby Care")
                    {
                        $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

                        if($status1=="All")
                        {

                            if($filter1=="Alternatenumber")
                            {
                                // echo "hello";
                                $filter1= 'leads.Alternatenumber';
                            }

                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1.'%')
                            // ->where('Branch',$city)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);


                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }
                        else
                        {

                            if($filter1=="Alternatenumber")
                            {
                                // echo "hello";
                                $filter1= 'leads.Alternatenumber';
                            }
                            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                            ->Where($filter1, 'like',   $keyword1 . '%')
                            ->where('Branch',$city)
                            ->where('ServiceStatus',$status1)
                            ->wherein('ServiceType',$Servicesarray)
                            ->orderBy('leads.id', 'DESC')->paginate(200);

                            // dd($keyword1, $status1, $filter1,$desig,$data1);

                            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                        }



                    }
                }

                //if it's a coordinator
                else {
                    $desig="coordinator";
                    if($serviceType1=="Mathrutvam - Baby Care"){
                        $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }
                    if($serviceType1=="Personal Supportive Care")
                    {
                        $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }
                    if($serviceType1=="Nursing Services")
                    {
                        $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }
                    if($serviceType1=="Physiotherapy - Home")
                    {
                        // dd($serviceType);
                        $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }


                    //refer to comments in "customer care" ifs
                    if($status1=="All")
                    {
                        if($filter1=="Alternatenumber")

                        {

                            $filter1 = 'leads.Alternatenumber';
                        }

                        $data1 = DB::table('leads')
                        ->select('employees.*','services.*','verticalcoordinations.*','leads.*')
                        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                        ->join('employees','verticalcoordinations.empid','=','employees.id')
                        ->join('services', 'leads.id', '=', 'services.Leadid')
                        ->Where($filter1, 'like',   $keyword1.'%')
                        ->wherein('ServiceType',$Servicesarray)
                        // ->where('Branch',$city)
                        ->where('verticalcoordinations.empid',$employeeid)
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);


                        return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                    }
                    else
                    {

                        if($filter1=="Alternatenumber")
                        {
                            $filter1 = 'leads.Alternatenumber';
                        }
                        // dd($Servicesarray);
                        $data1 = DB::table('leads')
                        ->select('employees.*','services.*','verticalcoordinations.*','leads.*')
                        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                        ->join('employees','verticalcoordinations.empid','=','employees.id')
                        ->join('services', 'leads.id', '=', 'services.Leadid')
                        ->where('ServiceStatus',$status1)
                        // ->where('Branch',$city)
                        ->wherein('ServiceType',$Servicesarray)
                        ->Where($filter1, 'like',   $keyword1.'%')
                        ->where('verticalcoordinations.empid',$employeeid)
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);


                        return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
                    }
                }
            }
        }
        else {

            $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')->join('addresses', 'leads.id', '=', 'addresses.leadid')->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')->join('services', 'leads.id', '=', 'services.LeadId')->Where($filter1, 'like',   $keyword1 . '%')->orderBy('leads.id', 'DESC')->paginate(300);
            return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1','desig'));
        }



    }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {

            $name=Auth::guard('admin')->user()->name;
            $logged_in_user=Auth::guard('admin')->user()->name;
            // dd($logged_in_user);
            //echo $name;
            $id=input::get('id');
            $servicetypeinurl=input::get('servicetype');

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->get();
            $shift=DB::table('shiftrequireds')->get();

            $service_request=DB::table('services')->where('leadid',$id)->where('serviceType',$servicetypeinurl)->select('id')->get();
            $service_request=json_decode($service_request);
            $service_request= $service_request[0]->id;
            $service_request=service::find($service_request);

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');
            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');


            // $empid=DB::table('leads')->where('id',$id)->select('empid')->get();
            // $empid=json_decode($empid);
            // $meid= $empid[0]->empid;

            $eid = DB::table('employees')->where('FirstName',$name)->value('id');
            $adminid = DB::table('admins')->where('name',$name)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');
            // dd($roles);

            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();
            $emp123=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            // list of coordinator to assigned for management and admin view
            $managementeid=DB::table('services')->where('Leadid',$id)->value('AssignedTo');
            $meid=DB::table('employees')->where('FirstName',$managementeid)->value('id');
            $memp=DB::table('employees')->where('Designation','Coordinator')->where('under',$meid)->get();
            // dd($memp);

            //

            $s=DB::table('services')->where('leadid',$id)->where('serviceType',$servicetypeinurl)->value('servicetype');

            $service=DB::table('verticals')->where('verticaltype',$s)->get();
            $service1=DB::table('leadstatus')->get();


            $leaddata=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id','=',$id)
            ->where('serviceType',$servicetypeinurl)
            ->orderBy('leads.id', 'DESC')
            ->get();


            //     $leaddata=DB::table('leads')
            //     ->select('services.*','personneldetails.*','addresses.*','leads.*')
            //     ->join('personneldetails', 'leads.id','=','personneldetails.Leadid')
            //     ->join('services', 'leads.id', '=', 'services.LeadId')
            //     ->join('addresses','leads.id','=','addresses.leadid')
            //     ->where('leads.id','=',$id)
            //     ->get();



            $r=DB::table('role_admins')->where('role_id',2)->orwhere('role_id',6)->select('admin_id')->get();

            $l=DB::table('leads')->where('id',$id)->select('id')->get();
            $l=json_decode($l);
            $l= $l[0]->id;
            $lead=lead::find($l);

            $lsd=lead::find($l);


            // $vci=DB::table('verticalcoordinations')->where('leadid',$id)->select('empid')->get();
            // $vci=json_decode($vci);
            //  $vci= $vci[0]->empid;

            //  $assignto=DB::table('employees')->where('id',$vci)->select('FirstName')->get();



            // $osi=json_decode($id1);

            // $b= $osi[0]->id;

            // echo "other";
            // echo $medicalcondition;

            // echo "abdomen\n";
            // echo $abdomen;



            $vc=DB::table('verticalcoordinations')->where('leadid',$id)->select('empid')->get();
            $vc=json_decode($vc);
            if($vc == [])
            {
                $verc=NULL;
            }
            else
            {
                $vc= $vc[0]->empid;
                $verc=employee::find($vc);
            }

            // count for medicine

            $doctorvalue=DB::table('mars')->where('leadid',$id)->value('Prescribedby');
            $doctorvalue1=DB::table('mars')->where('leadid',$id)->value('Prescribedby');
            $RegNo=DB::table('mars')->where('leadid',$id)->value('RegNo');
            $PresDate=DB::table('mars')->where('leadid',$id)->value('PresDate');

            if($doctorvalue==NULL)
            {
                $values= array();
                $Medication_value[] = array();

                $MedicationForm_value[] = array();
                $DateTime_value[] = array();
                $Route_value[] = array();
                $Frequency_value[] = array();
                $Dosage_value[] = array();
                $ValidTill_value[] = array();
                $FormType_value[] = array();
                $Strength_value[] = array();
                $ACPC_value[] = array();
                $count_of_docs=0;

            }
            else
            {


                $values=DB::table('mars')->where('leadid',$id)->get();

                //passing values of each column to there repective array

                //value for medication


                $values=DB::table('mars')->where('leadid',$id)->get();

                //Medication
                $Medication=DB::table('mars')->where('leadid',$id)->value('Medication');
                $Medication = explode("\r\n",$Medication);
                $Medication_count=count($Medication);

                $k=0;

                for($i=0;$i<$Medication_count;$i++)
                {

                    $Medication_value[$k] = $Medication[$i];
                    $k++;

                }

                // value for medication form

                $MedicationForm=DB::table('mars')->where('leadid',$id)->value('MedicationForm');
                $MedicationForm = explode("\r\n",$MedicationForm);
                $MedicationForm_count=count($MedicationForm);

                $k=0;


                for($i=0;$i<$MedicationForm_count;$i++)
                {

                    $MedicationForm_value[$k] = $MedicationForm[$i];
                    $k++;

                }
                // dd($MedicationForm_value);
                // value for date and time

                $DateTime=DB::table('mars')->where('leadid',$id)->value('DateTime');
                $DateTime = explode("\r\n",$DateTime);
                $DateTime_count=count($DateTime);

                $k=0;

                for($i=0;$i<$DateTime_count;$i++)
                {

                    $DateTime_value[$k] = $DateTime[$i];
                    $k++;

                }

                // value for Route
                $Route=DB::table('mars')->where('leadid',$id)->value('Route');
                $Route = explode("\r\n",$Route);
                $Route_count=count($Route);

                $k=0;

                for($i=0;$i<$Route_count;$i++)
                {

                    $Route_value[$k] = $Route[$i];
                    $k++;

                }

                // value for Frequency
                $Frequency=DB::table('mars')->where('leadid',$id)->value('Frequency');
                $Frequency = explode("\r\n",$Frequency);
                $Frequency_count=count($Frequency);

                $k=0;

                for($i=0;$i<$Frequency_count;$i++)
                {

                    $Frequency_value[$k] = $Frequency[$i];
                    $k++;

                }

                // value for Dosage
                $Dosage=DB::table('mars')->where('leadid',$id)->value('Dosage');
                $Dosage = explode("\r\n",$Dosage);
                $Dosage_count=count($Dosage);

                $k=0;

                for($i=0;$i<$Dosage_count;$i++)
                {

                    $Dosage_value[$k] = $Dosage[$i];
                    $k++;

                }

                // value for ValidTill
                $ValidTill=DB::table('mars')->where('leadid',$id)->value('ValidTill');
                $ValidTill = explode("\r\n",$ValidTill);
                $ValidTill_count=count($ValidTill);

                $k=0;

                for($i=0;$i<$ValidTill_count;$i++)
                {

                    $ValidTill_value[$k] = $ValidTill[$i];
                    $k++;

                }

                // value for FormType
                $FormType=DB::table('mars')->where('leadid',$id)->value('FormType');
                $FormType = explode("\r\n",$FormType);
                $FormType_count=count($FormType);

                $k=0;

                for($i=0;$i<$FormType_count;$i++)
                {

                    $FormType_value[$k] = $FormType[$i];
                    $k++;

                }

                // value for Strength
                $Strength=DB::table('mars')->where('leadid',$id)->value('Strength');
                $Strength = explode("\r\n",$Strength);
                $Strength_count=count($Strength);

                $k=0;

                for($i=0;$i<$Strength_count;$i++)
                {

                    $Strength_value[$k] = $Strength[$i];
                    $k++;

                }

                // value for ACPC
                $ACPC=DB::table('mars')->where('leadid',$id)->value('ACPC');
                $ACPC = explode("\r\n",$ACPC);
                $ACPC_count=count($ACPC);

                $k=0;

                for($i=0;$i<$ACPC_count;$i++)
                {

                    $ACPC_value[$k] = $ACPC[$i];
                    $k++;

                }



                //dd($ACPC_value);
                //dd($Medication_count);




                //Prescribed By
                $doctorvalue = explode("\r\n",$doctorvalue);
                $doctorcount = count($doctorvalue);

                $RegNo = explode("\r\n",$RegNo);
                // dd($RegNo);
                $RegNocount = count($RegNo);

                $s=0;
                $RegistrationNo_value_unique[] = array();

                $l=0;
                $Prescribedby_value_unique[] = array();

                $PresDate = explode("\r\n",$PresDate);
                // dd($PresDate);
                $PresDatecount = count($PresDate);
                // dd($PresDatecount);
                $w=0;
                $Prescriptiondate_value_unique[] = array();

                for($i=0;$i<$doctorcount;$i++)
                {
                    if($i!=0)
                    {
                        if($doctorvalue[$i-1] != $doctorvalue[$i])
                        {
                            $Prescribedby_value_unique[$l]=$doctorvalue[$i];
                            $l++;
                            $RegistrationNo_value_unique[$s]=$RegNo[$i];
                            $s++;
                            $Prescriptiondate_value_unique[$w]=$PresDate[$i];
                            $w++;

                        }

                    }else
                    {
                        $Prescribedby_value_unique[$l]=$doctorvalue[0];
                        $l++;
                        $RegistrationNo_value_unique[$s]=$RegNo[0];
                        $s++;
                        $Prescriptiondate_value_unique[$w]=$PresDate[0];
                        $w++;
                    }

                }

                $j=0;
                $medicine_count[] = array();
                $col1=1;
                // dd($doctorcount);
                $count_of_docs=0;
                if($doctorvalue!=NULL)
                {
                    for($i=0;$i<$doctorcount;$i++)
                    {
                        if($i!=0)
                        {
                            if($doctorvalue[$i-1]==$doctorvalue[$i])
                            {
                                $col1+=1;


                            }
                            else
                            {
                                $medicine_count[$j] = $col1;
                                $j++;
                                $col1=1;
                            }
                        }
                        else
                        {
                            $medicine_count[$j] = $col1;
                        }
                    }
                }
                else
                {
                    $count_of_docs=0;
                    $medicine_count=array();
                }


                $count_of_docs = count($medicine_count);

                // dd($medicine_count,$doctorvalue,$MedicationForm_value);
                // dd($Prescribedby_value_unique,$RegistrationNo_value_unique,$Prescriptiondate_value_unique);

            }

            //count for medicine ends here

            $servicetype=DB::table('services')->where('leadid',$id)->where('serviceType',$servicetypeinurl)->select('ServiceType')->get();
            $servicetype=json_decode($servicetype);
            $servicetype=$servicetype[0]->ServiceType;
            // echo $servicetype;

            $aid=DB::table('abdomens')->where('leadid',$id)->value('leadid');
            $mid=DB::table('mothers')->where('Leadid',$id)->value('Leadid');
            $pid=DB::table('physiotheraphies')->where('Leadid',$id)->value('Leadid');


            $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');
            $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');


            // echo $servicetype;
            // echo $id;
            // echo $servicetype;
            // echo $aid;
            // echo $mid;
            // echo $pid;


            $statuscheck=DB::table('services')->where('leadid',$id)->where('serviceType',$servicetypeinurl)->value('ServiceStatus');
            // dd($statuscheck);

            // if($aid=='[{"leadid":'.$id.'}]' || $mid=='[{"Leadid":'.$id.'}]' || $pid=='[{"Leadid":'.$id.'}]')
            // dd($count_of_docs);
            if($statuscheck != "New")
            {




                $ab=DB::table('abdomens')->where('leadid',$id)->value('id');
                // $ab=json_decode($ab);
                // $ab= $ab[0]->id;

                if($ab==NULL)
                {

                    $abdomens = new abdomen;

                    $abdomens->leadid=$id;
                    $abdomens->save();

                    $ab=DB::table('abdomens')->where('leadid',$id)->value('id');
                    $abdomen=abdomen::find($ab);
                }
                else
                {
                    $abdomen=abdomen::find($ab);
                }


                $co=DB::table('communications')->where('leadid',$id)->value('id');
                // $co=json_decode($co);
                // $co= $co[0]->id;

                if($co==NULL)
                {
                    $communication = new communication;
                    $communication->leadid=$id;
                    $communication->save();

                    $co=DB::table('communications')->where('leadid',$id)->value('id');
                    $communication=communication::find($co);
                }
                else
                {
                    $communication=communication::find($co);
                }



                $ci=DB::table('circulatories')->where('leadid',$id)->value('id');
                // $ci=json_decode($ci);
                // $ci= $ci[0]->id;

                if($ci==NULL)
                {

                    $circulatory =new circulatory;
                    $circulatory->leadid=$id;
                    $circulatory->save();

                    $ci=DB::table('circulatories')->where('leadid',$id)->value('id');
                    $circulatory=circulatory::find($ci);
                }
                else
                {
                    $circulatory=circulatory::find($ci);
                }


                $de=DB::table('dentures')->where('leadid',$id)->value('id');
                // $de=json_decode($de);
                // $de= $de[0]->id;

                if($de==NULL)
                {
                    $dentures= new denture;
                    $dentures->leadid=$id;
                    $dentures->save();

                    $de=DB::table('dentures')->where('leadid',$id)->value('id');
                    $denture=denture::find($de);
                }
                else
                {
                    $denture=denture::find($de);
                }



                $ex=DB::table('extremities')->where('leadid',$id)->value('id');
                // $ex=json_decode($ex);
                // $ex= $ex[0]->id;

                if($ex==NULL)
                {
                    $extremitie =new extremitie;
                    $extremitie->leadid=$id;
                    $extremitie->save();

                    $ex=DB::table('extremities')->where('leadid',$id)->value('id');
                    $extremitie=extremitie::find($ex);
                }
                else
                {
                    $extremitie=extremitie::find($ex);
                }


                $f=DB::table('fecals')->where('leadid',$id)->value('id');
                // $f=json_decode($f);
                // $f= $f[0]->id;

                if($f==NULL)
                {
                    $fecal= new fecal;
                    $fecal->leadid=$id;
                    $fecal->save();

                    $f=DB::table('fecals')->where('leadid',$id)->value('id');
                    $fecal=fecal::find($f);
                }
                else
                {
                    $fecal=fecal::find($f);
                }



                $ge=DB::table('genitos')->where('leadid',$id)->value('id');
                // $ge=json_decode($ge);
                // $ge= $ge[0]->id;

                if($ge==NULL)
                {
                    $genito = new genito;
                    $genito->leadid=$id;
                    $genito->save();

                    $ge=DB::table('genitos')->where('leadid',$id)->value('id');
                    $genito=genito::find($ge);
                }
                else
                {
                    $genito=genito::find($ge);
                }



                $me=DB::table('memoryintacts')->where('leadid',$id)->value('id');
                // $me=json_decode($me);
                // $me= $me[0]->id;

                if($me==NULL)
                {
                    $memoryintact= new memoryintact;
                    $memoryintact->leadid=$id;
                    $memoryintact->save();

                    $me=DB::table('memoryintacts')->where('leadid',$id)->value('id');
                    $memoryintact=memoryintact::find($me);

                }
                else
                {
                    $memoryintact=memoryintact::find($me);
                    // dd($memoryintact);
                }


                $mo=DB::table('mobilities')->where('leadid',$id)->value('id');
                // $mo=json_decode($mo);
                // $mo= $mo[0]->id;

                if($mo==NULL)
                {
                    $mobility= new mobility;
                    $mobility->leadid=$id;
                    $mobility->save();

                    $mo=DB::table('mobilities')->where('leadid',$id)->value('id');
                    $mobility=mobility::find($mo);

                }
                else
                {
                    $mobility=mobility::find($mo);
                    // dd($mobility);
                }


                $nu=DB::table('nutrition')->where('leadid',$id)->value('id');
                // $nu=json_decode($nu);
                // $nu= $nu[0]->id;

                if($nu==NULL)
                {
                    $nutrition =new nutrition;
                    $nutrition->leadid=$id;
                    $nutrition->save();

                    $nu=DB::table('nutrition')->where('leadid',$id)->value('id');
                    $nutrition=nutrition::find($nu);
                }
                else
                {
                    $nutrition=nutrition::find($nu);
                }


                $or=DB::table('orientations')->where('leadid',$id)->value('id');
                // $or=json_decode($or);
                // $or= $or[0]->id;

                if($or==NULL)
                {
                    $orientation=new orientation;
                    $orientation->leadid=$id;
                    $orientation->save();

                    $or=DB::table('orientations')->where('leadid',$id)->value('id');
                    $orientation=orientation::find($or);

                }
                else
                {

                    $orientation=orientation::find($or);


                }



                $pa=DB::table('vitalsigns')->where('leadid',$id)->value('id');
                // $pa=json_decode($pa);
                // $pa= $pa[0]->id;

                if($pa==NULL)
                {
                    $vitalsigns=new vitalsign;
                    $vitalsigns->leadid=$id;
                    $vitalsigns->save();

                    $pa=DB::table('vitalsigns')->where('leadid',$id)->value('id');
                    $vitalsign=vitalsign::find($pa);
                }
                else
                {
                    $vitalsign=vitalsign::find($pa);
                }



                $re=DB::table('respiratories')->where('leadid',$id)->value('id');
                // $re=json_decode($re);
                // $re= $re[0]->id;

                if($re==NULL)
                {
                    $respiratory =new respiratory;
                    $respiratory->leadid=$id;
                    $respiratory->save();

                    $re=DB::table('respiratories')->where('leadid',$id)->value('id');
                    $respiratory=respiratory::find($re);
                }
                else
                {
                    $respiratory=respiratory::find($re);
                }


                $po=DB::table('positions')->where('leadid',$id)->value('id');
                // $po=json_decode($po);
                // $po= $po[0]->id;

                if($po==NULL)
                {
                    $position =new position;
                    $position->leadid=$id;
                    $position->save();

                    $po=DB::table('positions')->where('leadid',$id)->value('id');
                    $position=position::find($po);
                }
                else
                {
                    $position=position::find($po);
                }



                $vh=DB::table('visionhearings')->where('leadid',$id)->value('id');
                // $vh=json_decode($vh);
                // $vh= $vh[0]->id;

                if($vh==NULL)
                {
                    $visionhearing =new visionhearing;
                    $visionhearing->leadid=$id;
                    $visionhearing->save();

                    $vh=DB::table('visionhearings')->where('leadid',$id)->value('id');
                    $visionhearing=visionhearing::find($vh);
                }
                else
                {
                    $visionhearing=visionhearing::find($vh);
                    // dd($visionhearing);
                }


                $gh=DB::table('generalhistories')->where('leadid',$id)->value('id');
                // $gh=json_decode($gh);
                // $gh= $gh[0]->id;

                if($gh==NULL)
                {
                    $generalhistory= new generalhistory;
                    $generalhistory->leadid=$id;
                    $generalhistory->save();

                    $gh=DB::table('generalhistories')->where('leadid',$id)->value('id');
                    $generalhistory=generalhistory::find($gh);
                }
                else
                {
                    $generalhistory=generalhistory::find($gh);
                }


                $mc=DB::table('generalconditions')->where('Leadid',$id)->value('id');
                // $mc=json_decode($mc);
                // $mc= $mc[0]->id;
                if($mc==NULL)
                {
                    $generalcondition = new generalcondition;
                    $generalcondition->Leadid=$id;
                    $generalcondition->save();

                    $mc=DB::table('generalconditions')->where('Leadid',$id)->value('id');
                    $generalcondition=generalcondition::find($mc);

                }
                else
                {
                    $generalcondition=generalcondition::find($mc);
                }






                // count for medicine ends here






                //code for mathrutvam edit



                if($servicetype == "Mathrutvam - Baby Care")
                {

                    $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                    // dd($as);
                    // $as=DB::table('assessments')->where('Leadid',$id)->select('id')->get();
                    // $as=json_decode($as);
                    // $as= $as[0]->id;
                    if($as==NULL)
                    {
                        $assessment = new assessment;
                        $assessment->Leadid=$id;
                        $assessment->save();
                        $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                        $assessment=assessment::find($as);
                        // dd($assessment);

                    }
                    else
                    {
                        $assessment=assessment::find($as);

                    }



                    $mother=DB::table('mothers')->where('Leadid',$id)->value('id');
                    // $mother=json_decode($mother);

                    // $motherid= $mother[0]->id;

                    if($mother==NULL)
                    {
                        $mother = new mother;
                        $mother->Leadid=$id;
                        $mother->save();

                        $motherid=DB::table('mothers')->where('Leadid',$id)->value('id');
                        $mother = mother::find($motherid);


                    }
                    else
                    {
                        $motherid=DB::table('mothers')->where('Leadid',$id)->value('id');
                        $mother = mother::find($motherid);


                    }




                    $ma=DB::table('mathruthvams')->where('Motherid',$motherid)->value('id');
                    // $ma=json_decode($ma);
                    // $ma= $ma[0]->id;

                    if($ma==NULL)
                    {
                        // dd($motherid);
                        $mathruthvam = new mathruthvam;
                        $mathruthvam->Motherid=$motherid;
                        $mathruthvam->save();
                        $ma=DB::table('mathruthvams')->where('Motherid',$motherid)->value('id');
                        $mathrutvam = mathruthvam::find($ma);

                    }
                    else
                    {
                        // dd($motherid);

                        $mathrutvam = mathruthvam::find($ma);

                    }
                    // dd($mathrutvam->ImmunizationUpdated);
                    $ImmunizationUpdated_show = $mathrutvam->ImmunizationUpdated;

                    $ImmunizationUpdated_show = explode(",",$ImmunizationUpdated_show);

                    $count_immunization = count($ImmunizationUpdated_show);
                    // dd($ImmunizationUpdated_show);
                    // $count_immunization = count($mathrutvam->ImmunizationUpdated);
                    // dd($count_immunization);

                    for($i=0;$i<$count_immunization;$i++)
                    {
                        $ImmunizationUpdated_show[$i] = substr($ImmunizationUpdated_show[$i],20);
                    }

                    $ImmunizationUpdated_show = implode(",",$ImmunizationUpdated_show);

                    // dd($ImmunizationUpdated_show);

                    $denverid=DB::table('denverscales')->where('leadid',$id)->value('id');

                    if($denverid==NULL)
                    {
                        $denver = new denverscale;
                        $denver->leadid=$id;
                        $denver->save();

                        $motherid=DB::table('denverscales')->where('leadid',$id)->value('id');
                        $denver = denverscale::find($denverid);


                    }
                    else
                    {
                        $denverid=DB::table('denverscales')->where('leadid',$id)->value('id');
                        $denver = denverscale::find($denverid);


                    }

                    $Muscles=DB::table('denverscales')->where('leadid',$id)->value('Muscles');
                    $Coordination_Eyes=DB::table('denverscales')->where('leadid',$id)->value('Coordination_Eyes');
                    $Hearing_Speech=DB::table('denverscales')->where('leadid',$id)->value('Hearing_Speech');
                    $Social_Skills=DB::table('denverscales')->where('leadid',$id)->value('Social_Skills');
                    $Safety=DB::table('denverscales')->where('leadid',$id)->value('Safety');
                    //dd($Coordination_Eyes);

                    //Showing Muscles
                    $Muscles_show = explode(",",$Muscles);
                    $count_muscles = count($Muscles_show);
                    // dd($Muscles_show);
                    // $count_muscles = count($mathrutvam->ImmunizationUpdated);
                    // dd($count_muscles);

                    for($i=0;$i<$count_muscles;$i++)
                    {
                        $Muscles_show[$i] = substr($Muscles_show[$i],20);
                    }

                    $Muscles_show = implode(",",$Muscles_show);
                    // dd($Muscles_show);

                    //Showing Coordination_Eyes

                    $Coordination_Eyes_show = explode(",",$Coordination_Eyes);
                    $count_coeyes = count($Coordination_Eyes_show);


                    for($i=0;$i<$count_coeyes;$i++)
                    {
                        $Coordination_Eyes_show[$i] = substr($Coordination_Eyes_show[$i],20);
                    }

                    $Coordination_Eyes_show = implode(",",$Coordination_Eyes_show);
                    // dd($Coordination_Eyes_show);

                    //Showing Hearing_Speech

                    $Hearing_Speech_show = explode(",",$Hearing_Speech);
                    $count_hearingspeech = count($Hearing_Speech_show);


                    for($i=0;$i<$count_hearingspeech;$i++)
                    {
                        $Hearing_Speech_show[$i] = substr($Hearing_Speech_show[$i],20);
                    }

                    $Hearing_Speech_show = implode(",",$Hearing_Speech_show);
                    // dd($Hearing_Speech_show);

                    //Showing Social_Skills

                    $Social_Skills_show = explode(",",$Social_Skills);
                    $count_socialskills = count($Social_Skills_show);


                    for($i=0;$i<$count_socialskills;$i++)
                    {
                        $Social_Skills_show[$i] = substr($Social_Skills_show[$i],20);
                    }

                    $Social_Skills_show = implode(",",$Social_Skills_show);
                    // dd($Social_Skills_show);

                    //Showing Safety

                    $Safety_show = explode(",",$Safety);
                    $count_safety = count($Safety_show);


                    for($i=0;$i<$count_safety;$i++)
                    {
                        $Safety_show[$i] = substr($Safety_show[$i],20);
                    }

                    $Safety_show = implode(",",$Safety_show);


                    $p1=DB::table('assessments')->where('Leadid',$id)->select('Productid')->get();
                    $p1=json_decode($p1);
                    $p1= $p1[0]->Productid;
                    // echo $p1;
                    $product=product::find(1);
                    if($p1 != NULL)
                    {
                        $p=DB::table('products')->where('Leadid',$id)->select('id')->get();
                        $p=json_decode($p);
                        $p= $p[0]->id;
                        $product=product::find($p);
                    }


                    $nid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                    $nid=json_decode($nid);
                    $nid= $nid[0]->Designation;

                    $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                    $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                    $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                    $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');

                    // $user_department=DB::table('employees')->where('FirstName',$name)->value('Department');


                    if($nid=="Vertical Head" && $user_department=="Mathrutvam - Baby Care")
                    {



                        if($roles=="Vertical_ProductSelling" && $user_department=="Mathrutvam - Baby Care")
                        {
                            $designation=$roles;


                            return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));





                        }
                        else
                        {
                            if($roles=="Vertical_ProductRental" && $user_department=="Mathrutvam - Baby Care")
                            {
                                $designation=$roles;

                                return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                            }
                            else
                            {
                                if($roles=="Vertical_ProductManager" && $user_department=="Mathrutvam - Baby Care")
                                {

                                    $designation=$roles;


                                    return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                }
                                else
                                {
                                    if($roles=="Vertical_PharmacyManager" && $user_department=="Mathrutvam - Baby Care")
                                    {

                                        $designation=$roles;

                                        return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                        {

                                            $designation=$roles;

                                            return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductRental_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                            {

                                                $designation=$roles;

                                                return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_Product_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                                {
                                                    $designation=$roles;

                                                    return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department=="Mathrutvam - Baby Care")
                                                    {

                                                        $designation=$roles;

                                                        return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));

                                                    }
                                                    else
                                                    {
                                                        if($roles=="Vertical_FieldOfficer" && $user_department=="Mathrutvam - Baby Care")
                                                        {
                                                            $designation=$roles;

                                                            return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));

                                                        }
                                                        else
                                                        {
                                                            if($nid=="Vertical Head" && $user_department=="Mathrutvam - Baby Care")
                                                            {

                                                                $designation="vertical";

                                                                return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                                            }
                                                            else
                                                            {
                                                                return redirect('/admin');
                                                            }

                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                    else
                    if($nid=="Management")
                    {
                        $designation="management";
                        return view('admin.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','memp','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                    }
                    else
                    if($nid=="Admin")
                    {
                        $designation="home";

                        return view('admin.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','memp','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','Muscles','Coordination_Eyes','Hearing_Speech','Social_Skills','Safety'));

                    }
                    else
                    {
                        if($nid=="Branch Head" && $user_city==$service_city)
                        {
                            $designation="BranchHead";

                            return view('verticalheads.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));

                        }
                        else
                        {


                            if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                            {

                                $designation=$roles;
                                return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                            }
                            else
                            {
                                if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                {

                                    $designation=$roles;
                                    return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                    {

                                        $designation=$roles;
                                        return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                        {

                                            $designation=$roles;
                                            return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                            {

                                                $designation=$roles;
                                                return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {

                                                    $designation=$roles;
                                                    return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));

                                                            }
                                                            else
                                                            {
                                                                if($nid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation="coordinator";
                                                                    return view('co.mathrutvamedit',compact('Muscles_show','Coordination_Eyes_show','Hearing_Speech_show','Social_Skills_show','Safety_show','ImmunizationUpdated_show','mathrutvam','mother','assessment','product','lead','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memoryintact','nutrition','vitalsign','respiratory','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','mobility','orientation','position','visionhearing','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value','denver'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }

                                                            }
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }






                }else{
                    if($servicetype =="Physiotherapy - Home" || $servicetype == "Physiotherapy - Clinic")
                    {

                        $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                        // $as=json_decode($as);
                        // $as= $as[0]->id;
                        if($as==NULL)
                        {
                            $assessment = new assessment;
                            $assessment->Leadid=$id;
                            $assessment->save();

                            $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                            $assessment=assessment::find($as);

                        }
                        else
                        {
                            $assessment=assessment::find($as);
                        }


                        $phy=DB::table('physiotheraphies')->where('leadid',$id)->value('id');
                        // $phy=json_decode($phy);
                        // $phy= $phy[0]->id;

                        if($phy==NULL)
                        {
                            $physiotheraphies = new physiotheraphy;
                            $physiotheraphies->Leadid=$id;
                            $physiotheraphies->save();

                            $phy=DB::table('physiotheraphies')->where('leadid',$id)->value('id');
                            $physiotheraphy = physiotheraphy::find($phy);
                        }
                        else
                        {
                            $physiotheraphy = physiotheraphy::find($phy);
                        }




                        $phr=DB::table('physioreports')->where('Physioid',$phy)->value('id');
                        // $phr=json_decode($phr);
                        // $phr= $phr[0]->id;

                        if($phr==NULL)
                        {


                            $physioreport = new physioreport;
                            $physioreport->Physioid=$phy;
                            $physioreport->save();

                            $phr=DB::table('physioreports')->where('Physioid',$phy)->value('id');
                            $physioreport = physioreport::find($phr);

                        }
                        else
                        {
                            $physioreport = physioreport::find($phr);
                        }




                        $p1=DB::table('assessments')->where('Leadid',$id)->select('Productid')->get();
                        $p1=json_decode($p1);
                        $p1= $p1[0]->Productid;
                        // echo $p1;
                        $product=product::find(1);
                        if($p1 != NULL)
                        {
                            $p=DB::table('products')->where('Leadid',$id)->select('id')->get();
                            $p=json_decode($p);
                            $p= $p[0]->id;
                            $product=product::find($p);
                        }





                        $nid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                        $nid=json_decode($nid);
                        $nid= $nid[0]->Designation;

                        $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                        $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                        $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                        $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');


                        if($nid=="Vertical Head" && $user_department=="Physiotherapy - Home")
                        {
                            if($roles=="Vertical_ProductSelling" && $user_department=="Physiotherapy - Home")
                            {

                                $designation=$roles;
                                return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {
                                if($roles=="Vertical_ProductRental" && $user_department=="Physiotherapy - Home")
                                {
                                    $designation=$roles;
                                    return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));





                                }
                                else
                                {
                                    if($roles=="Vertical_ProductManager" && $user_department=="Physiotherapy - Home")
                                    {

                                        $designation=$roles;
                                        return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_PharmacyManager" && $user_department=="Physiotherapy - Home")
                                        {
                                            $designation=$roles;
                                            return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department=="Physiotherapy - Home")
                                            {
                                                $designation=$roles;
                                                return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Vertical_ProductRental_Pharmacy" && $user_department=="Physiotherapy - Home")
                                                {
                                                    $designation=$roles;
                                                    return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_Product_Pharmacy" && $user_department=="Physiotherapy - Home")
                                                    {
                                                        $designation=$roles;
                                                        return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                    }
                                                    else
                                                    {

                                                        if($roles=="Vertical_FieldExecutive" && $user_department=="Physiotherapy - Home")
                                                        {
                                                            $designation=$roles;
                                                            return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Vertical_FieldOfficer" && $user_department=="Physiotherapy - Home")
                                                            {
                                                                $designation=$roles;
                                                                return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($nid=="Vertical Head" && $user_department=="Physiotherapy - Home")
                                                                {
                                                                    $designation="vertical";
                                                                    return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }

                                                            }
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        }
                        else
                        if($nid=="Management")
                        {
                            $designation="management";
                            return view('admin.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memp','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        if($nid=="Admin")
                        {
                            $designation="home";
                            return view('admin.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','memp','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        {
                            if($nid=="Branch Head" && $user_city==$service_city)
                            {
                                $designation="BranchHead";
                                return view('verticalheads.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {

                                if($roles=="Coordiantor_ProductSelling" && $check_coordinator==$coordinator_id)
                                {

                                    $designation=$roles;
                                    return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                }
                                else
                                {
                                    if($roles=="Coordiantor_ProductRental" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Coordiantor_ProductManager" && $check_coordinator==$coordinator_id)
                                        {

                                            $designation=$roles;
                                            return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                        }
                                        else
                                        {
                                            if($roles=="Coordiantor_PharmacyManager" && $check_coordinator==$coordinator_id)
                                            {
                                                $designation=$roles;
                                                return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Coordiantor_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Coordiantor_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordiantor_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                        }
                                                        else
                                                        {
                                                            if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation=$roles;
                                                                    return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                                }
                                                                else
                                                                {
                                                                    if($nid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                    {
                                                                        $designation="coordinator";
                                                                        return view('co.physiotherapyedit',compact('physiotheraphy','lead','physioreport','assessment','product','r','leaddata','emp','verc','shift','vertical','service','service_request','service1','lsd','designation','reference','gender','relation','city','language','gender1','leadtype','condition','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','generalcondition','generalhistory','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                    }
                                                                    else
                                                                    {
                                                                        return redirect('/admin');
                                                                    }

                                                                }
                                                            }


                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                        }




                    }
                    else
                    {


                        $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                        // $as=json_decode($as);
                        // $as= $as[0]->id;
                        if($as==NULL)
                        {
                            $assessment = new assessment;
                            $assessment->Leadid=$id;
                            $assessment->save();

                            $as=DB::table('assessments')->where('Leadid',$id)->value('id');
                            $assessment=assessment::find($as);
                        }
                        else
                        {

                            $assessment=assessment::find($as);
                        }



                        $p1=DB::table('assessments')->where('Leadid',$id)->select('Productid')->get();
                        $p1=json_decode($p1);
                        $p1= $p1[0]->Productid;
                        // echo $p1;
                        $product=product::find(1);
                        if($p1 != NULL)
                        {
                            $p=DB::table('products')->where('Leadid',$id)->select('id')->get();
                            $p=json_decode($p);
                            $p= $p[0]->id;
                            $product=product::find($p);
                        }






                        $nid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                        $nid=json_decode($nid);
                        $nid= $nid[0]->Designation;

                        $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                        $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                        $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                        $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');



                        if($nid=="Vertical Head" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                        {

                            if($roles=="Vertical_ProductSelling" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                            {
                                $designation=$roles;
                                return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {
                                if($roles=="Vertical_ProductRental" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                {
                                    $designation=$roles;
                                    return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Vertical_ProductManager" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                    {

                                        $designation=$roles;
                                        return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Vertical_PharmacyManager" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                        {

                                            $designation=$roles;
                                            return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductSelling_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                            {
                                                $designation=$roles;
                                                return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Vertical_ProductRental_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                {
                                                    $designation=$roles;
                                                    return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_Product_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                    {
                                                        $designation=$roles;
                                                        return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {

                                                        if($roles=="Vertical_FieldExecutive" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                        {
                                                            $designation=$roles;
                                                            return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Vertical_FieldOfficer" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                            {
                                                                $designation=$roles;
                                                                return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($nid=="Vertical Head"  && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                                {
                                                                    $designation="vertical";
                                                                    return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('admin');
                                                                }

                                                            }
                                                        }



                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        }
                        else
                        if($nid=="Management")
                        {
                            $designation="management";
                            return view('admin.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        if($nid=="Admin")
                        {



                            $designation="home";

                            return view('admin.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));



                        }
                        else
                        {
                            if($nid=="Branch Head" && $user_city==$service_city)
                            {
                                $designation="BranchHead";
                                return view('verticalheads.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','visionhearing','position','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {


                                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;

                                        return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                        {

                                            $designation=$roles;
                                            return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                            {

                                                $designation=$roles;
                                                return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {

                                                            if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation=$roles;
                                                                    return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    if($nid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                    {
                                                                        $designation="coordinator";
                                                                        return view('co.assessmentedit',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','abdomen','communication','circulatory','denture','extremitie','genito','fecal','memoryintact','mobility','nutrition','orientation','vitalsign','respiratory','position','visionhearing','assessment','product','generalcondition','lead','generalhistory','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                    }
                                                                    else
                                                                    {
                                                                        return redirect('/admin');
                                                                    }

                                                                }
                                                            }



                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }
                }





            }else
            {

                if($servicetype == "Mathrutvam - Baby Care")
                {


                    $emid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                    $emid=json_decode($emid);
                    $emid=$emid[0]->Designation;

                    $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                    $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                    $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                    $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');
                    // $emid="Vertical Head";
                    if($emid=="Vertical Head" && $user_department=="Mathrutvam - Baby Care")
                    {

                        if($roles=="Vertical_ProductSelling" && $user_department=="Mathrutvam - Baby Care")
                        {
                            $designation=$roles;
                            return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                        }
                        else
                        {
                            if($roles=="Vertical_ProductRental" && $user_department=="Mathrutvam - Baby Care")
                            {
                                $designation=$roles;
                                return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                            }
                            else
                            {
                                if($roles=="Vertical_ProductManager" && $user_department=="Mathrutvam - Baby Care")
                                {
                                    $designation=$roles;
                                    return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Vertical_PharmacyManager" && $user_department=="Mathrutvam - Baby Care")
                                    {
                                        $designation=$roles;
                                        return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                        {
                                            $designation=$roles;
                                            return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductRental_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                            {
                                                $designation=$roles;
                                                return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Vertical_Product_Pharmacy" && $user_department=="Mathrutvam - Baby Care")
                                                {
                                                    $designation=$roles;
                                                    return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {


                                                    if($roles=="Vertical_FieldExecutive" && $user_department=="Mathrutvam - Baby Care")
                                                    {
                                                        $designation=$roles;
                                                        return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {
                                                        if($roles=="Vertical_FieldOfficer" && $user_department=="Mathrutvam - Baby Care")
                                                        {
                                                            $designation=$roles;
                                                            return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                        }
                                                        else
                                                        {
                                                            if($emid=="Vertical Head" && $user_department=="Mathrutvam - Baby Care")
                                                            {
                                                                $designation="vertical";
                                                                return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                            }
                                                            else
                                                            {
                                                                return redirect('/admin');
                                                            }

                                                        }
                                                    }



                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }



                    }
                    else
                    if($emid=="Management")
                    {
                        $designation="management";
                        return view('admin.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                    }
                    else
                    if($emid=="Admin")
                    {
                        $designation="home";
                        return view('admin.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                    }
                    else
                    {
                        if($emid=="Branch Head" && $user_city==$service_city)
                        {
                            $designation="BranchHead";
                            return view('verticalheads.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        {

                            if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                            }
                            else
                            {
                                if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                            {
                                                $designation=$roles;
                                                return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {


                                                        if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));


                                                        }
                                                        else
                                                        {
                                                            if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($emid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation="coordinator";
                                                                    return view('co.mathrutvam',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }


                                                            }
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }





                }else{
                    if($servicetype == "Physiotherapy - Home" || $servicetype == "Physiotherapy - Clinic")
                    {



                        $emid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                        $emid=json_decode($emid);
                        $emid=$emid[0]->Designation;

                        $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                        $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                        $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                        $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');


                        // $emid="Vertical Head";

                        if($emid== "Vertical Head" && $user_department=="Physiotherapy - Home")
                        {
                            if($roles=="Vertical_ProductSelling" && $user_department=="Physiotherapy - Home")
                            {

                                $designation=$roles;
                                return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                            }
                            else
                            {
                                if($roles=="Vertical_ProductRental" && $user_department=="Physiotherapy - Home")
                                {
                                    $designation=$roles;
                                    return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Vertical_ProductManager" && $user_department=="Physiotherapy - Home")
                                    {
                                        $designation=$roles;
                                        return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Vertical_PharmacyManager" && $user_department=="Physiotherapy - Home")
                                        {
                                            $designation=$roles;
                                            return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department=="Physiotherapy - Home")
                                            {
                                                $designation=$roles;
                                                return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Vertical_ProductRental_Pharmacy" && $user_department=="Physiotherapy - Home")
                                                {
                                                    $designation=$roles;
                                                    return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_Product_Pharmacy" && $user_department=="Physiotherapy - Home")
                                                    {
                                                        $designation=$roles;
                                                        return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {

                                                        if($roles=="Vertical_FieldExecutive" && $user_department=="Physiotherapy - Home")
                                                        {
                                                            $designation=$roles;
                                                            return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Vertical_FieldOfficer" && $user_department=="Physiotherapy - Home")
                                                            {
                                                                $designation=$roles;
                                                                return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                            }
                                                            else
                                                            {
                                                                if($emid== "Vertical Head" && $user_department=="Physiotherapy - Home")
                                                                {
                                                                    $designation="vertical";
                                                                    return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }


                                                            }
                                                        }



                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }




                        }
                        else
                        if($emid=="Management")
                        {
                            $designation="management";
                            return view('admin.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        if($emid=="Admin")
                        {
                            $designation="home";
                            return view('admin.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        {
                            if($emid=="Branch Head" && $user_city==$service_city)
                            {
                                $designation="BranchHead";
                                return view('verticalheads.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {

                                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                                {

                                    $designation=$roles;
                                    return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                            {
                                                $designation=$roles;
                                                return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {

                                                            if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                            }
                                                            else
                                                            {
                                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation=$roles;
                                                                    return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    if($emid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                    {
                                                                        $designation="coordinator";
                                                                        return view('co.physiotherapy',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                    }
                                                                    else
                                                                    {
                                                                        return redirect('/admin');
                                                                    }


                                                                }
                                                            }


                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                        }



                    }
                    else{

                        $emid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                        $emid=json_decode($emid);
                        $emid=$emid[0]->Designation;

                        $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                        $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                        $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                        $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');
                        // $emid="Vertical Head";

                        if($emid=="Vertical Head" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                        {
                            if($roles=="Vertical_ProductSelling" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                            {
                                $designation=$roles;
                                return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                            }
                            else
                            {
                                if($roles=="Vertical_ProductRental" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                {
                                    $designation=$roles;
                                    return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductManager" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                    {
                                        $designation=$roles;
                                        return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                    }
                                    else
                                    {
                                        if($roles=="Vertical_PharmacyManager" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                        {
                                            $designation=$roles;
                                            return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));


                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductSelling_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                            {
                                                $designation=$roles;
                                                return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_ProductRental_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                {
                                                    $designation=$roles;
                                                    return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_Product_Pharmacy" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                    {
                                                        $designation=$roles;
                                                        return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));


                                                    }
                                                    else
                                                    {
                                                        if($roles=="Vertical_FieldExecutive" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                        {
                                                            $designation=$roles;
                                                            return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Vertical_FieldOfficer" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                            {
                                                                $designation=$roles;
                                                                return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                            }
                                                            else
                                                            {
                                                                if($emid=="Vertical Head" && ($user_department=="Personal Supportive Care" || $user_department=="Nursing Services"))
                                                                {
                                                                    $designation="vertical";
                                                                    return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }


                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }



                        }
                        else

                        if($emid=="Management")
                        {
                            $designation="management";
                            return view('admin.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        if($emid=="Admin")
                        {
                            $designation="home";
                            return view('admin.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','memp','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                        }
                        else
                        {
                            if($emid=="Branch Head" && $user_city==$service_city)
                            {
                                $designation="BranchHead";
                                return view('verticalheads.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                            }
                            else
                            {

                                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                            {
                                                $designation=$roles;
                                                return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));


                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));


                                                        }
                                                        else
                                                        {

                                                            if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));

                                                            }
                                                            else
                                                            {
                                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation=$roles;
                                                                    return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                }
                                                                else
                                                                {
                                                                    if($emid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                    {
                                                                        $designation="coordinator";
                                                                        return view('co.create',compact('reference','gender','relation','city','language','gender1','leadtype','condition','vertical','r','leaddata','emp','verc','shift','service','service_request','service1','lsd','designation','count_of_docs','medicine_count','values','Medication_value','DateTime_value','Route_value','Frequency_value','Dosage_value','ValidTill_value','FormType_value','Strength_value','ACPC_value','RegistrationNo_value_unique','Prescriptiondate_value_unique','Prescribedby_value_unique','MedicationForm_value'));
                                                                    }
                                                                    else
                                                                    {
                                                                        return redirect('/admin');
                                                                    }


                                                                }
                                                            }


                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }



                            }

                        }

                    }
                }
            }
        }

        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $leadid=$request->leadid;
        $name1=$request->sname;





        $r=DB::table('services')->where('leadid',$leadid)->select('Remarks')->value('Remarks');
        $a=$request->comment;

        $current_status = DB::table('services')->where('leadid',$leadid)->value('ServiceStatus');
        // dd($current_status,$request->ServiceStatus);
        if($a!=NULL)
        {
            $time=time();
            $timestamp=date('m/d/Y h:i:s a', time());

            $k =$timestamp."\r\n".$name1.":   ".$a."\r\n\n".$r."\r\n";
            $re=DB::table('services')->where('leadid', $leadid)->update(['Remarks'=>$k]);
        }

        //echo $name1;
        //         $name=DB::table('services')->where('leadid',$leadid)->select('AssignedTo')->get();
        // $name=json_decode($name);

        // $vcid=DB::table('verticalcoordinations')->where('leadid',$leadid)->select('empid')->get();
        // $vcid=json_decode($vcid);

        // $name=DB::table('employees')->where('FirstName',$name12345)->select('Designation')->get();
        // $name=json_decode($name);
        // $Des=$name[0]->Designation;

        // if($Des == "Vertical Head")
        // {
        //         $name1= $name[0]->AssignedTo;
        // }
        // else
        // {
        //   $vcid=$vcid[0]->empid;
        // $vid=DB::table('employees')->where('id',$vcid)->select('FirstName')->get();
        // $vid=json_decode($vid);
        // $name1=$vid[0]->FirstName;

        // }

        $products = new product;
        $products->SKUid=$request->SKUid;
        $products->ProductName=$request->ProductName;
        $products->DemoRequired=$request->DemoRequired;
        $products->AvailabilityStatus=$request->AvailabilityStatus;
        $products->AvailabilityAddress=$request->AvailabilityAddress;
        $products->SellingPrice=$request->SellingPrice;
        $products->RentalPrice=$request->RentalPrice;



        $productid=NULL;


        if($products->SKUid !=NULL || $products->ProductName !=NULL)
        {
            $products->Leadid=$leadid;
            $products->save();
            $productid=DB::table('products')->max('id');
        }



        $assessment = new assessment;
        $assessment->Assessor=$request->Assessor;
        $assessment->AssessDateTime=$request->AssessDateTime;
        $assessment->AssessPlace=$request->AssessPlace;
        $assessment->ServiceStartDate=$request->ServiceStartDate;
        $assessment->ServicePause=$request->ServicePause;
        $assessment->ServiceEndDate=$request->ServiceEndDate;
        $assessment->ShiftPreference=$request->ShiftPreference;
        $assessment->SpecificRequirements=$request->SpecificRequirements;
        $assessment->DaysWorked=$request->DaysWorked;
        $assessment->Latitude=$request->Latitude;
        $assessment->Longitude=$request->Longitude;
        $assessment->Productid=$productid;
        $assessment->Leadid=$leadid;
        $assessment->save();


        $a=$request->requested_service;
        $b=$request->ServiceStatus;

        DB::table('services')
        ->where('leadid', $leadid)
        ->update(['requested_service' => $a]);

        if($b=="New")
        {
            // dd($b);
            DB::table('services')->where('leadid', $leadid)->update(['ServiceStatus' => "In Progress"]);
        }
        else
        {
            DB::table('services')
            ->where('leadid', $leadid)
            ->update(['ServiceStatus' => $request->ServiceStatus]);
        }


        DB::table('leads')
        ->where('id', $leadid)
        ->update(['followupdate' => $request->followupdate]);

        DB::table('leads')
        ->where('id', $leadid)
        ->update(['reason' => $request->reason]);




        //mar table logic

        $leadcheck=DB::table('mars')->where('leadid',$leadid)->value('id');

        if($leadcheck==NULL)
        {
            $mara=new mar;

            $mara->leadid=$leadid;
            $mara->save();
            $marid=DB::table('mars')->max('id');
        }
        else
        {
            $marid=DB::table('mars')->where('leadid',$leadid)->value('id');

            $mar=mar::find($marid);
            $mar->FormType=NULL;
            $mar->Prescribedby=NULL;
            $mar->RegNo=NULL;
            $mar->PresDate=NULL;
            $mar->MedicationForm=NULL;
            $mar->Medication=NULL;
            $mar->Route=NULL;
            $mar->Frequency=NULL;
            $mar->Dosage=NULL;
            $mar->ValidTill=NULL;
            $mar->ACPC=NULL;
            $mar->save();

        }


        //Prescribed Doctor Sarts here
        $firstdoctornoofmedicine=$request->PrescribedbyDocfirstnomedicices;
        $noofPrescribedDoctors = $request->NumberofPrescribedbydoc;
        // dd($firstdoctornoofmedicine);
        $prescribed="Prescribed";
        $NumberofmedicineofPrescribeddoctors = [];
        for($i=2; $i<=$noofPrescribedDoctors; $i++)
        {
            $fieldname= $prescribed.$i;
            $value=$request->$fieldname;
            array_push($NumberofmedicineofPrescribeddoctors,$value);
        }
        // dd($NumberofmedicineofPrescribeddoctors);
        //to fecth doctor details of prscribed by first
        $firstdocprescribedbyname=$request->doc1;
        $firstdocprescribedbyregid=$request->reg1;
        $firstdocprescribedbydate1=$request->date1;
        $formTypevalue1="PB";
        // for first doctor all Medicine to Fetch
        $tocheckoutputeachmedicine = [];


        if($firstdocprescribedbyname!=NULL)
        {
            for($j=1 ; $j <= $firstdoctornoofmedicine ; $j++ )
            {

                $valuetobefetchedmedictionform = "doc1med".$j."form";
                $Medication = "doc1med".$j."medication";
                $Route = "doc1med".$j."route";
                $Frquency = "doc1med".$j."Frquency";
                $Dosage = "doc1med".$j."dosage";
                $Validtill = "doc1med".$j."validtill";
                $ACPC = "doc1med".$j."ACPC";
                array_push($tocheckoutputeachmedicine,$request->$Medication);


                $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');
                $ACPCvalue=DB::table('mars')->where('id',$marid)->value('ACPC');


                if($request->$Medication != NULL)
                {


                    $mar=mar::find($marid);
                    $mar->FormType=$formTypevalue.$formTypevalue1."\r\n";
                    $mar->Prescribedby=$Prescribedbyvalue.$firstdocprescribedbyname."\r\n";
                    $mar->RegNo=$RegNovalue.$firstdocprescribedbyregid."\r\n";
                    $mar->PresDate=$PresDatevalue.$firstdocprescribedbydate1."\r\n";
                    $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionform."\r\n";
                    $mar->Medication=$Medicationvalue.$request->$Medication."\r\n";
                    $mar->Route=$Routevalue.$request->$Route."\r\n";
                    $mar->Frequency=$Frequencyvalue.$request->$Frquency."\r\n";
                    $mar->Dosage=$Dosagevalue.$request->$Dosage."\r\n";
                    $mar->ValidTill=$ValidTillvalue.$request->$Validtill."\r\n";
                    $mar->ACPC=$ACPCvalue.$request->$ACPC."\r\n";
                    $mar->save();
                }


            }

        }

        // dd($tocheckoutputeachmedicine);
        //for other added more doctors
        //dd($noofPrescribedDoctors,$NumberofmedicineofPrescribeddoctors);
        if($noofPrescribedDoctors > 1)
        {
            $check = [];
            $k=0;
            for($m=2; $m <= $noofPrescribedDoctors; $m++)
            {    //to fetch Doctor details
                $valueofdocp="doc".$m;
                $pdocname=$request->$valueofdocp;
                $regidp="reg".$m;
                $pdocregid=$request->$regidp;
                $prescriptionpdate="date".$m;

                $pdocpdate=$request->$prescriptionpdate;
                // dd($request->$prescriptionpdate);

                $toloop = $NumberofmedicineofPrescribeddoctors[$k];
                // to fetch medicine details
                for($n=1 ; $n <= $toloop ; $n++)
                {

                    $valuetobefetchedmedictionformseconddoconwards = "doc".$m."med".$n."form";
                    $Medication = "doc".$m."med".$n."medication";
                    $DateandTime = "doc".$m."med".$n."dateandtime";
                    $Route = "doc".$m."med".$n."route";
                    $Frquency = "doc".$m."med".$n."Frquency";
                    $Dosage = "doc".$m."med".$n."dosage";
                    $Validtill = "doc".$m."med".$n."validtill";
                    $ACPC = "doc".$m."med".$n."ACPC";

                    array_push($check,$request->$valuetobefetchedmedictionformseconddoconwards);


                    $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                    $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                    $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                    $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                    $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                    $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                    $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                    $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                    $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                    $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');
                    $ACPCvalue=DB::table('mars')->where('id',$marid)->value('ACPC');


                    if($request->$Medication != NULL)
                    {
                        $mar=mar::find($marid);
                        $mar->FormType=$formTypevalue.$formTypevalue1."\r\n";
                        $mar->Prescribedby=$Prescribedbyvalue.$pdocname."\r\n";
                        $mar->RegNo=$RegNovalue.$pdocregid."\r\n";
                        $mar->PresDate=$PresDatevalue.$pdocpdate."\r\n";
                        $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionformseconddoconwards."\r\n";
                        $mar->Medication=$Medicationvalue.$request->$Medication."\r\n";
                        $mar->Route=$Routevalue.$request->$Route."\r\n";
                        $mar->Frequency=$Frequencyvalue.$request->$Frquency."\r\n";
                        $mar->Dosage=$Dosagevalue.$request->$Dosage."\r\n";
                        $mar->ValidTill=$ValidTillvalue.$request->$Validtill."\r\n";
                        $mar->ACPC=$ACPCvalue.$request->$ACPC."\r\n";
                        $mar->save();

                    }

                }

                $k=$k+1;
            }

        }

        //Prescribed Doctor Ends here
        //Nebulization  Start here

        //    $NumberofNebulizationdoc=$request->NumberofNebulizationdoc;
        //    //Nebulization for first doc starts
        //    $noofmedicine= $request->nebulizationnoofmedioffirstdoc;
        //    $firstdoc=[];
        //    $nebilizationdoc1name=$request->nebulizationdoc1;
        //    $nebilizationdoc1reg=$request->nebulizationreg1;
        //    $nebilizationdoc1date=$request->nebulizationdate1;
        //    $formTypevalue2="MED";
        //    for($q=1; $q<= $noofmedicine ; $q++ )
        //    {
        //        $formvaluetobefetched="nebulizationdoc1med".$q."form";
        //        $medicationvaluetobefetched="nebulizationdoc1med".$q."medication";
        //        $routevaluetobefetched="nebulizationdoc1med".$q."route";
        //        $Frquencyvaluetobefetched="nebulizationdoc1med".$q."Frquency";
        //        $dosagevaluetobefetched="nebulizationdoc1med".$q."dosage";
        //        $validtillvaluetobefetched="nebulizationdoc1med".$q."validtill";

        //        // array_push($firstdoc,$request->$valuetobefetched);


        //        $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
        //          $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
        //          $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
        //          $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
        //          $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
        //          $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
        //          $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
        //          $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
        //          $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
        //          $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');


        //          if($request->$formvaluetobefetched != NULL || $request->$medicationvaluetobefetched != NULL || $request->$routevaluetobefetched != NULL || $request->$Frquencyvaluetobefetched != NULL)
        //          {


        //                 $mar=mar::find($marid);
        //                  $mar->FormType=$formTypevalue.$formTypevalue2."\r\n";
        //                 $mar->Prescribedby=$Prescribedbyvalue.$nebilizationdoc1name."\r\n";
        //                 $mar->RegNo=$RegNovalue.$nebilizationdoc1reg."\r\n";
        //                 $mar->PresDate=$PresDatevalue.$nebilizationdoc1date."\r\n";
        //                 $mar->MedicationForm=$MedicationFormvalue.$request->$formvaluetobefetched."\r\n";
        //                 $mar->Medication=$Medicationvalue.$request->$medicationvaluetobefetched."\r\n";
        //                 $mar->Route=$Routevalue.$request->$routevaluetobefetched."\r\n";
        //                 $mar->Frequency=$Frequencyvalue.$request->$Frquencyvaluetobefetched."\r\n";
        //                 $mar->Dosage=$Dosagevalue.$request->$dosagevaluetobefetched."\r\n";
        //                 $mar->ValidTill=$ValidTillvalue.$request->$validtillvaluetobefetched."\r\n";
        //                 $mar->save();

        //            }




        //    }
        //    //Nebulization for first doc ends

        //    //Nebulization from second doc starts



        //    $Nebulization="nebulizationnoofeachdocsmedicine";
        //    $Numberofmedicineofeachnebulizationddoctors = [];
        //    //dd($NumberofNebulizationdoc);
        //    for($i=2; $i<=$NumberofNebulizationdoc; $i++)
        //    {
        //        $fieldname= $Nebulization.$i;
        //        $value=$request->$fieldname;
        //        array_push($Numberofmedicineofeachnebulizationddoctors,$value);
        //    }
        //    //dd($NumberofNebulizationdoc,$Numberofmedicineofeachnebulizationddoctors);

        //     if($NumberofNebulizationdoc > 1)
        //    {

        //     $checknebulizationfromseconddoc= [];
        //     $s=0;
        //     for($p=2;$p <= $NumberofNebulizationdoc ; $p++)
        //     {
        //            $toloopnebulization = $Numberofmedicineofeachnebulizationddoctors[$s];
        //        $valueofdoc="nebulizationdoc".$p;
        //         $nebulizationdocname=$request->$valueofdoc;
        //         $regid="nebulizationreg".$p;
        //         $nebulizationdocregid=$request->$regid;
        //         $prescriptionnebulizationdate="nebulizationdate".$p;
        //         $nebulizationdocpdate=$request->$prescriptionnebulizationdate;
        //        for($n=1 ; $n <= $toloopnebulization ; $n++)
        //       {

        //         $valuetobefetchedmedictionformseconddoconwardsneb = "docnebulization".$p."med".$n."form";
        //         $Medicationnebulization = "docnebulization".$p."med".$n."medication";

        //         $Routenebulization = "docnebulization".$p."med".$n."route";
        //         $Frquencynebulization = "docnebulization".$p."med".$n."Frquency";
        //         $Dosagenebulization = "docnebulization".$p."med".$n."dosage";
        //         $Validtillnebulization = "docnebulization".$p."med".$n."validtill";

        //         array_push($checknebulizationfromseconddoc,$nebulizationdocname);


        //          $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
        //          $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
        //          $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
        //          $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
        //          $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
        //          $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
        //          $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
        //          $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
        //          $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
        //          $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');

        //          if($request->$valuetobefetchedmedictionformseconddoconwardsneb != NULL || $request->$Medicationnebulization != NULL || $request->$Routenebulization != NULL || $request->$Frquencynebulization != NULL)
        //          {


        //          $mar=mar::find($marid);
        //          $mar->FormType=$formTypevalue.$formTypevalue2."\r\n";
        //          $mar->Prescribedby=$Prescribedbyvalue.$nebulizationdocname."\r\n";
        //          $mar->RegNo=$RegNovalue.$nebulizationdocregid."\r\n";
        //          $mar->PresDate=$PresDatevalue.$nebulizationdocpdate."\r\n";
        //          $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionformseconddoconwardsneb."\r\n";
        //          $mar->Medication=$Medicationvalue.$request->$Medicationnebulization."\r\n";
        //          $mar->Route=$Routevalue.$request->$Routenebulization."\r\n";
        //          $mar->Frequency=$Frequencyvalue.$request->$Frquencynebulization."\r\n";
        //          $mar->Dosage=$Dosagevalue.$request->$Dosagenebulization."\r\n";
        //          $mar->ValidTill=$ValidTillvalue.$request->$Validtillnebulization."\r\n";
        //          $mar->save();
        //            }

        //       }
        //            $s=$s+1;
        //     }

        // }
        //dd($Numberofmedicineofeachnebulizationddoctors,$checknebulizationfromseconddoc);
        //Nebulization from second doc Ends




        //not significants for all tables starts here
        if($request->notsignificantforabdomen == "true")
        {
            $notsignificantforabdomen="true";
        }
        else
        {
            $notsignificantforabdomen=NULL;
        }


        $abdomen = new abdomen;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $abdomen->Inspection=$request->Inspection;
        $abdomen->AusculationofBS=$request->AusculationofBS;
        $abdomen->Palpation=$request->Palpation;
        $abdomen->Percussion=$request->Percussion;
        $abdomen->Ileostomy=$request->Ileostomy;
        $abdomen->Colostomy=$request->Colostomy;
        $abdomen->Functioning=$request->Functioning;
        $abdomen->Shape=$request->Shape;
        $abdomen->girth=$request->girth;
        $abdomen->changeincolor=$request->changeincolor;
        $abdomen->BulgeType=$request->BulgeType;
        $abdomen->abdomennotess=$request->abdomennotess;
        $abdomen->notsignificantforabdomen=$notsignificantforabdomen;
        $abdomen->leadid=$leadid;
        $abdomen->save();
        $abdomen=DB::table('abdomens')->max('id');



        if($request->notsignificantforcirculatory == "true")
        {
            $notsignificantforcirculatory="true";
        }
        else
        {
            $notsignificantforcirculatory=NULL;
        }

        $hoHTNCADCHF=$request->Historyofcirculatory;

        $hoHTNCADCHF = str_replace(array('[',']' ,),' ',$hoHTNCADCHF);
        $hoHTNCADCHF = preg_replace('/"/', '', $hoHTNCADCHF);
        $hoHTNCADCHF = trim($hoHTNCADCHF);

        $circulatory =new circulatory;
        $circulatory->ChestPain=$request->ChestPain;
        $circulatory->hoHTNCADCHF=$hoHTNCADCHF;
        $circulatory->HR=$request->HR;
        $circulatory->PeripheralCyanosis=$request->PeripheralCyanosis;
        $circulatory->JugularVein=$request->JugularVein;
        $circulatory->SurgeryHistory=$request->SurgeryHistory;
        $circulatory->circulatorynotes=$request->circulatorynotes;
        $circulatory->notsignificantforcirculatory=$notsignificantforcirculatory;
        $circulatory->leadid=$leadid;
        $circulatory->save();
        $circulatory=DB::table('circulatories')->max('id');

        $communication = new communication;
        // $communication->Language=$request->Language;
        // Retriving ar muliselect values
        $Language=$request->languageschecked;

        //dd($Language);
        // dd($language);


        $a = gettype($Language);


        $Language = str_replace(array('[',']' ,),' ',$Language);
        $Language = preg_replace('/"/', '', $Language);
        $Language = trim($Language);
        // dd($Language);

        $communication->Language=$Language;

        $communication->AdequateforAllActivities=$request->AdequateforAllActivities;
        $communication->unableToCommunicate=$request->unableToCommunicate;
        $communication->communicationnotes=$request->communicationnotes;
        $communication->notsignificantforcommunication=$request->notsignificantforcommunication;
        $communication->leadid=$leadid;
        $communication->Language = $Language;
        $communication->save();
        $communication=DB::table('communications')->max('id');



        if($request->notsignificantfordenture == "true")
        {
            $notsignificantfordenture="true";
        }
        else
        {
            $notsignificantfordenture=NULL;
        }
        $dentures= new denture;
        $dentures->Upper=$request->Upper;
        $dentures->Lower=$request->Lower;
        $dentures->Cleaning=$request->Cleaning;
        $dentures->denturenotes=$request->denturenotes;
        $dentures->notsignificantfordenture=$notsignificantfordenture;
        $dentures->leadid=$leadid;
        $dentures->save();
        $dentures=DB::table('dentures')->max('id');


        $extremitie =new extremitie;
        $extremitie->UppROM=$request->UppROM;

        $extremitie->UppMuscleStrength=$request->UppMuscleStrength;
        $extremitie->LowROM=$request->LowROM;

        $extremitie->LowMuscleStrength=$request->LowMuscleStrength;
        $extremitie->leadid=$leadid;
        $extremitie->save();

        $extremitie=DB::table('extremities')->max('id');



        $Typeoffecal=$request->Typeoffecalchecked;
        $Typeoffecal = str_replace(array('[',']' ,),' ',$Typeoffecal);
        $Typeoffecal = preg_replace('/"/', '', $Typeoffecal);
        $Typeoffecal = trim($Typeoffecal);

        $fecal= new fecal;
        $fecal->FecalType=$request->FecalType;
        $fecal->IncontinentOccasionally=$request->IncontinentOccasionally;
        $fecal->IncontinentAlways=$request->IncontinentAlways;
        $fecal->Commodechair=$request->Commodechair;
        $fecal->Needassistanceoffecal=$request->Needassistanceoffecal;
        $fecal->Typeoffecal=$Typeoffecal;
        $fecal->fecalnotes=$request->fecalnotes;
        $fecal->leadid=$leadid;
        $fecal->save();
        $fecal=DB::table('fecals')->max('id');






        $Typeofurinary=$request->Typeofurinarychecked;
        $Typeofurinary = str_replace(array('[',']' ,),' ',$Typeofurinary);
        $Typeofurinary = preg_replace('/"/', '', $Typeofurinary);
        $Typeofurinary = trim($Typeofurinary);


        if($request->notsignificantforurinary == "true")
        {
            $notsignificantforurinary="true";
        }
        else
        {
            $notsignificantforurinary=NULL;
        }

        $genito = new genito;
        $genito->UrinaryContinent=$request->UrinaryContinent;
        $genito->UrinaryContinent=$request->UrinaryContinent;
        $genito->Needassistanceofurinary=$request->Needassistanceofurinary;
        $genito->Typeofurinary=$Typeofurinary;
        $genito->urinarynotes=$request->urinarynotes;
        $genito->HoPOSTTURP=$request->HoPOSTTURP;
        $genito->HoUTI=$request->HoUTI;
        $genito->CompletelyContinet=$request->CompletelyContinet;
        $genito->IncontinentUrineOccasionally=$request->IncontinentUrineOccasionally;
        $genito->IncontinentUrineNightOnly=$request->IncontinentUrineNightOnly;
        $genito->IncontinentUrineAlways=$request->IncontinentUrineAlways;
        $genito->notsignificantforurinary=$notsignificantforurinary;
        $genito->leadid=$leadid;
        $genito->save();
        $genito=DB::table('genitos')->max('id');


        $generalcondition = new generalcondition;
        $generalcondition->Weight=$request->Weight;
        $generalcondition->Height=$request->Height;
        $generalcondition->Height_Measured_In=$request->Height_Measured_In;
        $generalcondition->BMI=$request->BMI;
        $generalcondition->MedicalHistory=$request->MedicalHistory;
        $generalcondition->Leadid=$leadid;
        $generalcondition->save();
        $generalcondition=DB::table('generalconditions')->max('id');


        $generalhistory= new generalhistory;
        $generalhistory->PresentHistory=$request->PresentHistory;
        $generalhistory->PastHistory=$request->PastHistory;
        $generalhistory->FamilyHistory=$request->FamilyHistory;
        $generalhistory->Mensural_OBGHistory=$request->Mensural_OBGHistory;
        $generalhistory->AllergicHistory=$request->AllergicHistory;
        $generalhistory->AllergicStatus=$request->AllergicStatus;
        $generalhistory->AllergicSeverity=$request->AllergicSeverity;
        $generalhistory->leadid=$leadid;
        $generalhistory->save();
        $generalhistory=DB::table('generalhistories')->max('id');


        $memoryintact= new memoryintact;
        $memoryintact->ShortTerm=$request->ShortTerm;
        $memoryintact->LongTerm=$request->LongTerm;
        $memoryintact->memoryintactnotes=$request->memoryintactnotes;
        $memoryintact->notsignificantformemoryintact=$request->notsignificantformemoryintact;

        $memoryintact->leadid=$leadid;
        $memoryintact->save();
        $memoryintact=DB::table('memoryintacts')->max('id');



        $Typeofmobility=$request->Typeofmobilitychecked;
        $Typeofmobility = str_replace(array('[',']' ,),' ',$Typeofmobility);
        $Typeofmobility = preg_replace('/"/', '', $Typeofmobility);
        $Typeofmobility = trim($Typeofmobility);



        if($request->notsignificantformobility == "true")
        {
            $notsignificantformobility="true";
        }
        else
        {
            $notsignificantformobility=NULL;
        }

        $mobility= new mobility;
        $mobility->Independent=$request->Independent;
        $mobility->NeedAssistance=$request->NeedAssistance;
        $mobility->Typeofmobility=$Typeofmobility;
        $mobility->mobilitynotes=$request->mobilitynotes;
        $mobility->Walker=$request->Walker;
        $mobility->WheelChair=$request->WheelChair;
        $mobility->Crutch=$request->Crutch;
        $mobility->Cane=$request->Cane;
        $mobility->ChairFast=$request->ChairFast;
        $mobility->BedFast=$request->BedFast;
        $mobility->notsignificantformobility=$notsignificantformobility;
        $mobility->leadid=$leadid;
        $mobility->save();
        $mobility=DB::table('mobilities')->max('id');


        $Eyeopening=$request->Eyeopening;
        $Eyeopening= explode(" ",$Eyeopening);
        $count=count($Eyeopening);
        unset($Eyeopening[$count-1]);
        $Eyeopening=implode(" ", $Eyeopening);



        $verbalresponse=$request->verbalresponse;
        $verbalresponse= explode(" ",$verbalresponse);
        $count2=count($verbalresponse);
        unset($verbalresponse[$count2-1]);
        $verbalresponse=implode(" ", $verbalresponse);

        $motorresponse=$request->motorresponse;
        $motorresponse= explode(" ",$motorresponse);
        $count3=count($motorresponse);
        unset($motorresponse[$count3-1]);
        $motorresponse=implode(" ", $motorresponse);


        $orientation=new orientation;
        $orientation->Person=$request->Person;
        $orientation->Place=$request->Place;
        $orientation->Time=$request->Time;
        $orientation->orientationnotes=$request->orientationnotes;
        $orientation->Eyeopening=$Eyeopening;
        $orientation->verbalresponse=$verbalresponse;
        $orientation->motorresponse=$motorresponse;
        $orientation->totalscoregcs=$request->totalscoregcs;
        $orientation->notsignificantfornervoussystem=$request->notsignificantfornervoussystem;
        $orientation->leadid=$leadid;
        $orientation->save();
        $orientation=DB::table('orientations')->max('id');

        $vitalsigns=new vitalsign;
        $vitalsigns->BP=$request->BP;
        $vitalsigns->BP_equipment=$request->BP_equipment;
        $vitalsigns->BP_taken_from=$request->BP_taken_from;
        $vitalsigns->RR=$request->RR;
        $vitalsigns->Temperature=$request->Temperature;
        $vitalsigns->TemperatureType=$request->TemperatureType;
        $vitalsigns->vitalnotes=$request->vitalnotes;
        $vitalsigns->Temperature_Measured_In=$request->Temperature_Measured_In;
        $vitalsigns->P1=$request->P1;
        $vitalsigns->P2=$request->P2;
        $vitalsigns->P3=$request->P3;
        $vitalsigns->R1=$request->R1;
        $vitalsigns->R2=$request->R2;
        $vitalsigns->R3=$request->R3;
        $vitalsigns->R4=$request->R4;
        $vitalsigns->T1=$request->T1;
        $vitalsigns->T2=$request->T2;
        $vitalsigns->Pulse=$request->Pulse;
        $vitalsigns->PainScale=$request->PainScale;
        $vitalsigns->Quality=$request->Quality;
        $vitalsigns->SeverityScale=$request->SeverityScale;
        $vitalsigns->MedicalDiagnosis=$request->MedicalDiagnosis;
        $vitalsigns->q1=$request->q1;
        $vitalsigns->leadid=$leadid;
        $vitalsigns->save();
        $vitalsigns=DB::table('vitalsigns')->max('id');



        if($request->notsignificantforrespiratory == "true")
        {
            $notsignificantforrespiratory="true";
        }
        else
        {
            $notsignificantforrespiratory=NULL;
        }



        $H_OTB_Asthma_COPD=$request->Historyofrespiratory;

        $H_OTB_Asthma_COPD = str_replace(array('[',']' ,),' ',$H_OTB_Asthma_COPD);
        $H_OTB_Asthma_COPD = preg_replace('/"/', '', $H_OTB_Asthma_COPD);
        $H_OTB_Asthma_COPD = trim($H_OTB_Asthma_COPD);

        $respiratory =new respiratory;

        $respiratory->SOB=$request->SOB;
        $respiratory->Cough=$request->Cough;
        $respiratory->ColorOfPhlegm=$request->ColorOfPhlegm;
        $respiratory->Nebulization=$request->Nebulization;
        $respiratory->Tracheostomy=$request->Tracheostomy;
        $respiratory->CPAP_BIPAP=$request->CPAP_BIPAP;
        $respiratory->ICD=$request->ICD;
        $respiratory->placementoficd=$request->placementoficd;
        $respiratory->noofdays=$request->noofdays;
        $respiratory->respiratorynotes=$request->respiratorynotes;
        $respiratory->H_OTB_Asthma_COPD=$H_OTB_Asthma_COPD;
        $respiratory->SPO2=$request->SPO2;
        $respiratory->central_cynaosis=$request->central_cynaosis;
        $respiratory->notsignificantforrespiratory=$notsignificantforrespiratory;
        $respiratory->leadid=$leadid;
        $respiratory->save();
        $respiratory=DB::table('respiratories')->max('id');



        $Position_Type11=$request->Position_Typechecked;

        $Position_Type11 = str_replace(array('[',']' ,),' ',$Position_Type11);
        $Position_Type11 = preg_replace('/"/', '', $Position_Type11);
        $Position_Type11 = trim($Position_Type11);

        // dd($Position_Type11);

        if($request->notsignificantforposition == "true")
        {
            $notsignificantforposition="true";
        }
        else
        {
            $notsignificantforposition=NULL;
        }

        $position =new position;
        $position->Position_Type=$Position_Type11;
        $position->positionnotes=$request->positionnotes;
        $position->notsignificantforposition=$request->notsignificantforposition;
        $position->leadid=$leadid;
        $position->save();

        $position=DB::table('positions')->max('id');



        if($request->notsignificantforvision == "true")
        {
            $visionnotsignificant="true";
        }
        else
        {
            $visionnotsignificant=NULL;
        }

        if($request->hearingnotsignificant == "true")
        {
            $hearingnotsignificant="true";
        }
        else
        {
            $hearingnotsignificant=NULL;
        }


        $visionhearing =new visionhearing;
        $visionhearing->Impared=$request->Impared;
        $visionhearing->HImpared=$request->HImpared;
        $visionhearing->ShortSight=$request->ShortSight;
        $visionhearing->LongSight=$request->LongSight;
        $visionhearing->WearsGlasses=$request->WearsGlasses;
        $visionhearing->HearingAids=$request->HearingAids;
        $visionhearing->visionnotes=$request->visionnotes;
        $visionhearing->hearingnotes=$request->hearingnotes;
        $visionhearing->notsignificantforvision=$visionnotsignificant;
        $visionhearing->notsignificantforhear=$hearingnotsignificant;
        $visionhearing->leadid=$leadid;
        $visionhearing->save();
        $visionhearing=DB::table('visionhearings')->max('id');



        $nutrition =new nutrition;
        $nutrition->Intact=$request->Intact;
        $nutrition->Type=$request->Type;
        $nutrition->nutritionnotes=$request->nutritionnotes;
        $nutrition->Diet=$request->Diet;
        $nutrition->BFTime=$request->BFTime;
        $nutrition->LunchTime=$request->LunchTime;
        $nutrition->SnacksTime=$request->SnacksTime;
        $nutrition->DinnerTime=$request->DinnerTime;
        $nutrition->TPN=$request->TPN;
        $nutrition->RTFeeding=$request->RTFeeding;
        $nutrition->RTFeeding_Measured_In=$request->RTFeeding_Measured_In;
        $nutrition->PEGFeeding_Measured_In=$request->PEGFeeding_Measured_In;
        $nutrition->PEGFeeding=$request->PEGFeeding;
        $nutrition->feedingtype=$request->feedingtype;
        $nutrition->frequency=$request->frequency;
        $nutrition->routes=$request->routes;
        $nutrition->Quantityinfusion=$request->Quantityinfusion;
        $nutrition->timeinfusion=$request->timeinfusion;
        $nutrition->infusionratedropspermin=$request->infusionratedropspermin;
        $nutrition->diettnotes=$request->diettnotes;
        $nutrition->notsignificantfornutrition=$request->notsignificantfornutrition;
        $nutrition->leadid=$leadid;
        $nutrition->save();
        $nutrition=DB::table('nutrition')->max('id');



        $verticalref=new verticalref;
        $verticalref->AbdomenId=$abdomen;
        $verticalref->CirculatoryId=$circulatory;
        $verticalref->CommunicationId=$communication;
        $verticalref->DentureId=$dentures;
        $verticalref->ExtremitiesId=$extremitie;
        $verticalref->GenitoId=$genito;
        $verticalref->generalconditionId=$generalcondition;
        $verticalref->MemoryIntactId=$memoryintact;
        $verticalref->MobilityId=$mobility;
        $verticalref->NutitionId=$nutrition;
        $verticalref->vitalsignId=$vitalsigns;
        $verticalref->RespiratoryId=$respiratory;
        $verticalref->VisionHearingId=$visionhearing;
        $verticalref->OrientationId=$orientation;
        $verticalref->generalhistoryId=$generalhistory;
        $verticalref->leadid=$leadid;
        $verticalref->save();




        // fetching service type for if loops

        $servicetype=DB::table('services')->where('leadid',$leadid)->select('ServiceType')->get();
        $servicetype=json_decode($servicetype);
        $servicetype=$servicetype[0]->ServiceType;

        //Everything related to Mathrutvam form starts here -- comments by jatin
        if($servicetype == "Mathrutvam - Baby Care")
        {

            $mother = new mother;

            $mother->FName=$request->FName;
            $mother->MName=$request->MName;
            $mother->LName=$request->LName;
            $mother->Medications=$request->Medications;
            $mother->MedicalDiagnosis=$request->MedicalDiagnosis;
            $mother->DeliveryPlace=$request->DeliveryPlace;
            $mother->DueDate=$request->DueDate;
            $mother->DeliveryType=$request->DeliveryType;
            $mother->NoOfBabies=$request->NoOfBabies;
            $mother->NoOfAttenderRequired=$request->NoOfAttenderRequired;
            $mother->Leadid=$leadid;
            $mother->save();

            $motherid=DB::table('mothers')->max('id');





            $Feedingissue11=$request->Feedingissue12;

            $Feedingissue11 = str_replace(array('[',']' ,),' ',$Feedingissue11);
            $Feedingissue11 = preg_replace('/"/', '', $Feedingissue11);
            $Feedingissue11 = trim($Feedingissue11);


            $current_timestamp = date("d-m-Y H:i:s");

            if($request->ImmunizationUpdatedvalue)
            {
                $final_Immunizationupdated = explode(",",$request->ImmunizationUpdatedvalue);
                $count_for_iu = count($final_Immunizationupdated);

                for($i=0;$i<$count_for_iu;$i++)
                {
                    $final_Immunizationupdated[$i] = $current_timestamp." ".$final_Immunizationupdated[$i];
                }

                $final_Immunizationupdated = implode(",",$final_Immunizationupdated);
            }
            else
            {
                $final_Immunizationupdated = NULL;
            }


            $mathruthvam = new mathruthvam;
            $mathruthvam->ChildDOB=$request->ChildDOB;
            $mathruthvam->ChildMedicalDiagnosis=$request->ChildMedicalDiagnosis;
            $mathruthvam->ChildMedications=$request->ChildMedications;
            $mathruthvam->ImmunizationUpdated=$final_Immunizationupdated;
            $mathruthvam->BirthWeight=$request->BirthWeight;
            $mathruthvam->DischargeWeight=$request->DischargeWeight;
            $mathruthvam->FeedingFormula=$request->FeedingFormula;
            $mathruthvam->Quantity=$request->Quantity;
            $mathruthvam->Frequency=$request->Frequency;
            $mathruthvam->FeedingIssue=$Feedingissue11;
            $mathruthvam->FeedingType=$request->FeedingType;
            $mathruthvam->FeedingEstablished=$request->FeedingEstablished;
            $mathruthvam->other=$request->other;
            $mathruthvam->Length=$request->Length;
            $mathruthvam->HeadCircumference=$request->HeadCircumference;
            $mathruthvam->SleepingPattern=$request->SleepingPattern;
            $mathruthvam->Motherid=$motherid;
            $mathruthvam->save();


            $current_timestamp = date("d-m-Y H:i:s");

            if($request->DenverMuscles!=NULL)
            {
                $Muscles = explode(",",$request->DenverMuscles);
                // dd($final_Immunizationupdated);
                $count_for_muscles = count($Muscles);

                for($i=0;$i<$count_for_muscles;$i++)
                {
                    $Muscles[$i] = $current_timestamp." ".$Muscles[$i];
                }

                $Muscles = implode(",",$Muscles);            }
                else
                {
                    $Muscles = NULL;
                }

                if($request->Denvercoordinationeyes!=NULL)
                {
                    $Coordination_Eyes = explode(",",$request->Denvercoordinationeyes);
                    // dd($final_Immunizationupdated);
                    $count_for_coeyes = count($Coordination_Eyes);

                    for($i=0;$i<$count_for_coeyes;$i++)
                    {
                        $Coordination_Eyes[$i] = $current_timestamp." ".$Coordination_Eyes[$i];
                    }

                    $Coordination_Eyes = implode(",",$Coordination_Eyes);        }
                    else
                    {
                        $Coordination_Eyes = NULL;
                    }

                    if($request->Denverhearingspeech!=NULL)
                    {

                        $Hearing_Speech = explode(",",$request->Denverhearingspeech);
                        // dd($final_Immunizationupdated);
                        $count_for_hs = count($Hearing_Speech);

                        for($i=0;$i<$count_for_hs;$i++)
                        {
                            $Hearing_Speech[$i] = $current_timestamp." ".$Hearing_Speech[$i];
                        }

                        $Hearing_Speech = implode(",",$Hearing_Speech);        }
                        else
                        {
                            $Hearing_Speech = NULL;
                        }

                        if($request->Denversocailskills)
                        {
                            $Social_Skills = explode(",",$request->Denversocailskills);
                            // dd($final_Immunizationupdated);
                            $count_for_ss = count($Social_Skills);

                            for($i=0;$i<$count_for_ss;$i++)
                            {
                                $Social_Skills[$i] = $current_timestamp." ".$Social_Skills[$i];
                            }

                            $Social_Skills = implode(",",$Social_Skills);

                        }
                        else
                        {
                            $Social_Skills = NULL;
                        }

                        if($request->Denversafety)
                        {
                            $Safety = explode(",",$request->Denversafety);
                            // dd($final_Immunizationupdated);
                            $count_for_safety = count($Safety);

                            for($i=0;$i<$count_for_safety;$i++)
                            {
                                $Safety[$i] = $current_timestamp." ".$Safety[$i];
                            }

                            $Safety = implode(",",$Safety);
                        }
                        else
                        {
                            $Safety = NULL;
                        }


                        $denver = new denverscale;
                        $denver->Muscles = $Muscles;
                        $denver->Coordination_Eyes = $Coordination_Eyes;
                        $denver->Hearing_Speech = $Hearing_Speech;
                        $denver->Social_Skills = $Social_Skills;
                        $denver->Safety = $Safety;
                        $denver->leadid=$leadid;
                        $denver->save();








                        //the coordinator who has been assigned fetch their employee id
                        $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                        //$e=json_decode($e);
                        //      $e1=$e[0]->id;

                        $verticalcoordination = new verticalcoordination;
                        $verticalcoordination->empid=$e;
                        $verticalcoordination->ename=$request->assigned;
                        $verticalcoordination->leadid=$leadid;

                        /*
                        Customer care only had privilege to create lead but not assign coordinator so no verticalcoordination table exists for that purpose
                        vertical head can create lead and assign coordinator

                        Case 1: Coordinator not assigned
                        save value in id, leadid
                        empid is null

                        CAse 2: Coordinator  assigned
                        save value in id, leadid
                        empid is assigned

                        */

                        //fetching the coordinator's emp id if it exists  -- it will be null if not assigned by vertical head else some value
                        $w=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');

                        //fetching the lead id if coordinator has been assigned or not ----extract from db
                        $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('leadid');

                        //scenario when no row exists and we have to add the row for assigning to coordinator
                        if($leadid != $v)
                        {
                            $verticalcoordination->save();

                            /*
                            Mail when assigned to coordinator starts here -- by jatin
                            */

                            $name=DB::table('leads')->where('id',$leadid)->value('fName');
                            $user=DB::table('services')->where('leadid',$leadid)->value('AssignedTo');
                            $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                            $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                            $who = $name1;
                            $from = DB::table('admins')->where('name',$name1)->value('email');
                            $to = DB::table('admins')->where('name',$request->assigned)->value('email');

                            //service type retrieval done here
                            $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                            $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                            $coordname = $request->assigned;
                            //coordinator contact number
                            $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                            //coordinator alternate contact number
                            $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                            //customer name
                            $name=DB::table('leads')->where('id',$leadid)->value('fName');

                            //customer mobile number
                            $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                            //location selected during lead creation
                            $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                            //service type
                            $service_type = DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                            /* SMS functionality for Mathrutvam Form when Coordinator is assigned starts here -- by jatin   22222---- Working*/

                            // dd($request->assigned);
                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($request->assigned!=null  && $coordmob!=NULL && $active_state==0)
                            {
                                $message="Dear ".$coordname.",
                                A new request is assigned.

                                ID: ".$leadid."
                                Name: ".$name."
                                Request Type: ".$service_type."
                                Contact: ".$contact."
                                Location: ".$location;
                                $phonenimber=$coordmob;

                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);

                                curl_close($ch);

                                $i=0;
                                //sending a message again if not sent first time
                                while($report!="OK" || $i<=1)
                                {
                                    if($report!="OK")
                                    {
                                        $message="Dear ".$coordname.",
                                        A new request is assigned.

                                        ID: ".$leadid."
                                        Name: ".$name."
                                        Request Type: ".$service_type."
                                        Contact: ".$contact."
                                        Location: ".$location;
                                        $phonenimber=$coordmob;

                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);
                                    }
                                    $i++;
                                }
                            }

                            // Message for alternate number of coordinator
                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($request->assigned!=null  &&$altcoordmob!=NULL && $active_state==0)
                            {
                                $message="Dear ".$coordname.",
                                A new request is assigned.

                                ID: ".$leadid."
                                Name: ".$name."
                                Request Type: ".$service_type."
                                Contact: ".$contact."
                                Location: ".$location;
                                $phonenimber=$altcoordmob;

                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);

                                curl_close($ch);

                                $i=0;
                                //sending a message again if not sent first time
                                while($report!="OK" || $i<=1)
                                {
                                    if($report!="OK")
                                    {
                                        $message="Dear ".$coordname.",
                                        A new request is assigned.

                                        ID: ".$leadid."
                                        Name: ".$name."
                                        Request Type: ".$service_type."
                                        Contact: ".$contact."
                                        Location: ".$location;
                                        $phonenimber=$altcoordmob;

                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);
                                    }
                                    $i++;
                                }
                            }


                            /* SMS functionality for Mathrutvam Form when Coordinator is assigned ends here -- by jatin*/

                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($to!=Null && $active_state==0)
                            {
                                Mail::send('mail', $data, function($message)use($data) {
                                    $message->to($data['to'], $data['user'] )->subject
                                    ('Lead ['.$data['leadid'].'] Assigned!!!');
                                    $message->from($data['from'],$data['who']);
                                });


                            }

                            /*
                            Mail when assigned to coordinator ends here -- by jatin
                            */



                        }
                        else
                        {
                            //whether empid is null or not
                            if($w == NULL)
                            {
                                //here a row exists therefore fetch id for that row
                                $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->select('id')->get();
                                $verticalcoordination=json_decode($verticalcoordination);
                                $verticalcoordination=$verticalcoordination[0]->id;

                                $verticalcoordination=verticalcoordination::find($verticalcoordination);
                                $verticalcoordination->empid=$e;
                                $verticalcoordination->ename=$request->assigned;


                                // here we are trying to get the session id of the present logged in person
                                $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

                                //here we are extracting the name of the present logged in person
                                $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                //here we are extracting the emp id and the log id of the present logged in person
                                $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


                                //if during creation of any form, NO Coordinator is assigned, by default it'll be the vertical lead
                                //else the Coordinator him/her self
                                if(!$request->assigned)
                                {
                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "Lead to be assessed by Vertical Head";
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid;
                                    $activity->save();
                                }

                                else
                                {
                                    $activity = new Activity;
                                    $activity->emp_id = $emp_id;
                                    $activity->activity = "Lead assigned to ".$request->assigned ;
                                    $activity->activity_time = new \DateTime();
                                    $activity->log_id = $log_id;
                                    $activity->lead_id = $leadid;
                                    $activity->save();
                                }

                                /*
                                Mathrutvam Form by vertical head ends here -- by jatin
                                */

                                $verticalcoordination->save();


                                /*
                                Mail when assigned to coordinator
                                */

                                //coordinator's name
                                $n=$request->assigned;

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                //vertical head name
                                $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');


                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');


                                $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                //name of coordinator to whom assessment has been assigned
                                $coordname = $request->assigned;

                                //coordinator contact number
                                $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                                //coordinator contact number
                                $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                //service type retrieval
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                /* SMS functionality for Mathrutvam Form when Coordinator is assigned starts here -- by jatin   ---- Working*/

                                // dd($request->assigned);

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned && $coordmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.",
                                    A new request is assigned.

                                    ID: ".$leadid."
                                    Name: ".$name."
                                    Request Type: ".$service_type."
                                    Contact: ".$contact."
                                    Location: ".$location;
                                    $phonenimber=$coordmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.",
                                            A new request is assigned.

                                            ID: ".$leadid."
                                            Name: ".$name."
                                            Request Type: ".$service_type."
                                            Contact: ".$contact."
                                            Location: ".$location;
                                            $phonenimber=$coordmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                //Message going to alternate number of coordinator

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned && $altcoordmob !=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.",
                                    A new request is assigned.

                                    ID: ".$leadid."
                                    Name: ".$name."
                                    Request Type: ".$service_type."
                                    Contact: ".$contact."
                                    Location: ".$location;
                                    $phonenimber=$altcoordmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.",
                                            A new request is assigned.

                                            ID: ".$leadid."
                                            Name: ".$name."
                                            Request Type: ".$service_type."
                                            Contact: ".$contact."
                                            Location: ".$location;
                                            $phonenimber=$altcoordmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                /* SMS functionality for Mathrutvam Form when Coordinator is assigned ends here -- by jatin*/

                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$user)->value('email');
                                $to1 = DB::table('leads')->where('id',$leadid)->value('EmailId');

                                //service type retrieval done here
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=Null && $active_state==0)
                                {
                                    Mail::send('mail', $data, function($message)use($data) {
                                        $message->to($data['to'], $data['user'] )->subject
                                        ('Lead ['.$data['leadid'].'] Assigned!!!');
                                        $message->from($data['from'],$data['who']);
                                    });
                                }

                                /*
                                mail end
                                */

                            }
                            //sms when coordinator is assigned in Lead Creation and the assesment is done
                            else
                            {
                                //coordinator's name
                                $n=$request->assigned;

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');


                                $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');



                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');


                                $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];


                                /*
                                SMS to Customer when Assessment done by Vertical Lead For Mathrutvam Form starts here -- by jatin
                                */

                                //vertical contact number
                                $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                                //vertical alternate contact number
                                $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //customer alternate mobile number
                                $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                // dd($status1, $request->ServiceStatus);

                                if($current_status!=$request->ServiceStatus)
                                {

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($contact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$contact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);
                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$contact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);
                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    //Message to customer on his/her alternate number

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($altcontact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$altcontact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);
                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$altcontact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);
                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    /*
                                    SMS to Customer Assessment done by Vertical Head For Mathrutvam Form ends here -- by jatin
                                    */

                                    /*
                                    SMS to Vertical Head when Assessment done by Vertical Lead For Mathrutvam Form starts here -- by jatin
                                    */
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($vmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                        Thank You.";
                                        $phonenimber=$vmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                Thank You.";
                                                $phonenimber=$vmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    // Message to alternate mobile number of vh

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($altvmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                        Thank You.";
                                        $phonenimber=$altvmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                Thank You.";
                                                $phonenimber=$altvmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }
                                }
                                /*
                                SMS when Assessment done by Vertical Head For Mathrutvam Form ends here -- by jatin
                                */
                            }
                        }

                        session()->put('name',$name1);
                        return redirect('/vh');
                    }
                    else
                    {
                        if($servicetype == "Physiotherapy - Home" || $servicetype == "Physiotherapy - Clinic")
                        {


                            $physiotheraphy = new physiotheraphy;
                            // $this->validate($request,[
                            //         'Languages'=>'required',
                            //     ]);


                            $physiotheraphy->PhysiotheraphyType=$request->PhysiotheraphyType;
                            $physiotheraphy->MetalImplant=$request->MetalImplant;
                            $physiotheraphy->Hypertension=$request->Hypertension;
                            $physiotheraphy->Medications=$request->Medications;

                            $physiotheraphy->PregnantOrBreastFeeding=$request->PregnantOrBreastFeeding;
                            $physiotheraphy->Diabetes=$request->Diabetes;
                            $physiotheraphy->ChronicInfection=$request->ChronicInfection;
                            $physiotheraphy->HeartDisease=$request->HeartDisease;
                            $physiotheraphy->Epilepsy=$request->Epilepsy;
                            $physiotheraphy->SurgeryUndergone=$request->SurgeryUndergone;
                            $physiotheraphy->AffectedArea=$request->AffectedArea;
                            $physiotheraphy->AssesmentDate=$request->AssesmentDate;
                            $physiotheraphy->PainPattern=$request->PainPattern;
                            $physiotheraphy->ExaminationReport=$request->ExaminationReport;
                            $physiotheraphy->LabOrRadiologicalReport=$request->LabOrRadiologicalReport;
                            $physiotheraphy->MedicalDisgnosis=$request->MedicalDisgnosis;
                            $physiotheraphy->PhysiotherapeuticDiagnosis=$request->PhysiotherapeuticDiagnosis;
                            $physiotheraphy->ShortTermGoal=$request->ShortTermGoal;
                            $physiotheraphy->LongTermGoal=$request->LongTermGoal;
                            $physiotheraphy->chiefcomplains=$request->chiefcomplains;
                            $physiotheraphy->ho_pc=$request->ho_pc;
                            $physiotheraphy->previousmedical=$request->previousmedical;
                            $physiotheraphy->occupationalH=$request->occupationalH;
                            $physiotheraphy->labq=$request->labq;
                            $physiotheraphy->affectedq=$request->affectedq;
                            $physiotheraphy->painq=$request->painq;
                            $physiotheraphy->envhistory=$request->envhistory;
                            $physiotheraphy->examinationq=$request->examinationq;
                            $physiotheraphy->Leadid=$leadid;
                            $physiotheraphy->save();



                            $Physioid=DB::table('physiotheraphies')->max('id');


                            $physioreport = new physioreport;
                            $physioreport->ProblemIdentified=$request->ProblemIdentified;
                            $physioreport->Treatment=$request->Treatment;
                            $physioreport->Physioid=$Physioid;
                            $physioreport->p1=$request->p1;
                            $physioreport->t1=$request->t1;
                            $physioreport->p2=$request->p2;
                            $physioreport->t2=$request->t2;
                            $physioreport->p3=$request->p3;
                            $physioreport->t3=$request->t3;
                            $physioreport->p4=$request->p4;
                            $physioreport->t4=$request->t4;
                            $physioreport->p5=$request->p5;
                            $physioreport->t5=$request->t5;
                            $physioreport->p6=$request->p6;
                            $physioreport->t6=$request->t6;
                            $physioreport->p7=$request->p7;
                            $physioreport->t7=$request->t7;
                            $physioreport->p8=$request->p8;
                            $physioreport->t8=$request->t8;
                            $physioreport->p9=$request->p9;
                            $physioreport->t9=$request->t9;

                            $physioreport->save();



                            $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                            //$e=json_decode($e);
                            //      $e1=$e[0]->id;



                            $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                            //$e=json_decode($e);
                            //      $e1=$e[0]->id;

                            $verticalcoordination = new verticalcoordination;
                            $verticalcoordination->empid=$e;
                            $verticalcoordination->ename=$request->assigned;
                            $verticalcoordination->leadid=$leadid;

                            $w=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                            $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('leadid');
                            if($leadid != $v)
                            {
                                $verticalcoordination->save();


                                /*
                                Mail when assigned to coordinator
                                */
                                $n=$request->assigned;
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');
                                $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');

                                //service type retrieval done here
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                $coordname = $request->assigned;
                                //coordinator contact number
                                $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                                //coordinator alternate contact number
                                $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                //service type retrieval
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                /* SMS functionality for Physiotherapy Form when Coordinator is assigned starts here -- by jatin   22222---- Working*/

                                // dd($request->assigned);
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned!=null && $coordmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                    Contact: ".$contact." Location: ".$location;

                                    $phonenimber=$coordmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                            Contact: ".$contact." Location: ".$location;
                                            $phonenimber=$coordmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                //Message going to alternate number of coordinator
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned!=null && $altcoordmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                    Contact: ".$contact." Location: ".$location;

                                    $phonenimber=$altcoordmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                            Contact: ".$contact." Location: ".$location;
                                            $phonenimber=$altcoordmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }



                                /* SMS functionality for Physiotherapy Form when Coordinator is assigned ends here -- by jatin*/

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=Null && $active_state==0)
                                {
                                    Mail::send('mail', $data, function($message)use($data) {
                                        $message->to($data['to'], $data['user'] )->subject
                                        ('Lead ['.$data['leadid'].'] Assigned!!!');
                                        $message->from($data['from'],$data['who']);
                                    });
                                }

                                /*
                                mail end
                                */

                            }
                            else
                            {
                                if($w == NULL)
                                {

                                    $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->select('id')->get();
                                    $verticalcoordination=json_decode($verticalcoordination);
                                    $verticalcoordination=$verticalcoordination[0]->id;

                                    $verticalcoordination=verticalcoordination::find($verticalcoordination);
                                    $verticalcoordination->empid=$e;
                                    $verticalcoordination->ename=$request->assigned;


                                    /*
                                    Physiotherapy Form by vertical head starts here -- by jatin
                                    refer to Mathurtvam form for commments
                                    */
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                                    if(!$request->assigned)
                                    {
                                        $activity = new Activity;
                                        $activity->emp_id = $emp_id;
                                        $activity->activity = "Lead to be assessed by Vertical Head";
                                        $activity->activity_time = new \DateTime();
                                        $activity->log_id = $log_id;
                                        $activity->lead_id = $leadid;
                                        $activity->save();
                                    }

                                    else
                                    {
                                        $activity = new Activity;
                                        $activity->emp_id = $emp_id;
                                        $activity->activity = "Lead assigned to ".$request->assigned ;
                                        $activity->activity_time = new \DateTime();
                                        $activity->log_id = $log_id;
                                        $activity->lead_id = $leadid;
                                        $activity->save();
                                    }


                                    /*
                                    Physiotherapy Form by vertical head ends here -- by jatin
                                    */

                                    $verticalcoordination->save();


                                    /*
                                    Mail when assigned to coordinator
                                    */
                                    $n=$request->assigned;
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');
                                    $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                                    $who = $name1;
                                    $from = DB::table('admins')->where('name',$name1)->value('email');
                                    $to = DB::table('admins')->where('name',$n)->value('email');

                                    //service type retrieval done here
                                    $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                    $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                    //name of coordinator to whom assessment has been assigned
                                    $coordname = $request->assigned;

                                    //coordinator contact number
                                    $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                                    //coordinator alternate contact number
                                    $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    //service type
                                    $service_type = DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                    /* SMS functionality for Physiotherapy Form when Coordinator is assigned starts here -- by jatin   ---- Working*/

                                    // dd($request->assigned);
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($request->assigned && $coordmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                        Contact: ".$contact." Location: ".$location;
                                        $phonenimber=$coordmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                                Contact: ".$contact." Location: ".$location;
                                                $phonenimber=$coordmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    //Message going to alternate number of coordinator

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($request->assigned!=null && $altcoordmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                        Contact: ".$contact." Location: ".$location;

                                        $phonenimber=$altcoordmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type."
                                                Contact: ".$contact." Location: ".$location;
                                                $phonenimber=$altcoordmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }


                                    /* SMS functionality for Physiotherapy Form when Coordinator is assigned ends here -- by jatin*/

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($to!=Null && $active_state==0)
                                    {
                                        Mail::send('mail', $data, function($message)use($data) {
                                            $message->to($data['to'], $data['user'] )->subject
                                            ('Lead ['.$data['leadid'].'] Assigned!!!');
                                            $message->from($data['from'],$data['who']);
                                        });
                                    }

                                    /*
                                    mail end
                                    */



                                }
                                //sms when coordinator is assigned in Lead Creation and the assesment is done
                                else
                                {
                                    //coordinator's name
                                    $n=$request->assigned;

                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //customer alternate mobile number
                                    $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    $who = $name1;
                                    $from = DB::table('admins')->where('name',$name1)->value('email');
                                    $to = DB::table('admins')->where('name',$n)->value('email');

                                    //service type
                                    $service_type = DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                    $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                    /*
                                    SMS to Customer when Assessment done by Vertical Lead For Physiotherapy Form starts here -- by jatin
                                    */

                                    //vertical contact number
                                    $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                                    //vertical alternate contact number
                                    $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');

                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    if($current_status!=$request->ServiceStatus)
                                    {
                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($contact!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$contact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                    - Health Heal";
                                                    $phonenimber=$contact;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }

                                        //Message going to alternate number of customer

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($altcontact!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$altcontact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                    - Health Heal";
                                                    $phonenimber=$altcontact;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }

                                        /*
                                        SMS to Customer Assessment done by Vertical Head For Physiotherapy Form ends here -- by jatin
                                        */

                                        //vertical contact number
                                        $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                                        //vertical alternate contact number
                                        $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');


                                        /*
                                        SMS to Vertical Head when Assessment done by Vertical Lead For Physiotherapy Form starts here -- by jatin
                                        */

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($vmob!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                            Thank You.";
                                            $phonenimber=$vmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                    Thank You.";
                                                    $phonenimber=$vmob;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }

                                        // Message going to alternate number of vh

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($altvmob!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                            Thank You.";
                                            $phonenimber=$altvmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                    Thank You.";
                                                    $phonenimber=$altvmob;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }
                                    }
                                    /*
                                    SMS when Assessment done by Vertical Head For Physiotherapy Form ends here -- by jatin
                                    */
                                }
                            }


                            session()->put('name',$name1);
                            return redirect('/vh');

                        }
                        else
                        {
                            //code for psc and ns assessment form

                            $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                            $verticalcoordination = new verticalcoordination;
                            $verticalcoordination->empid=$e;
                            $verticalcoordination->ename=$request->assigned;
                            $verticalcoordination->leadid=$leadid;





                            $w=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
                            $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('leadid');
                            if($leadid != $v)
                            {
                                $verticalcoordination->save();
                                /*
                                Mail when assigned to coordinator
                                */
                                $n=$request->assigned;
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');
                                $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');



                                $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type'=>$service_type];

                                $coordname = $request->assigned;
                                //coordinator contact number
                                $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                                //coordinator alternate contact number
                                $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                                //customer name
                                $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                //customer alternate mobile number
                                $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                //service type retrieval done here
                                $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');



                                /* SMS functionality for PSC Form when Coordinator is assigned starts here -- by jatin   22222---- Working*/

                                // dd($request->assigned);
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned!=null && $coordmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                    $phonenimber=$coordmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                            $phonenimber=$coordmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                //Message going to alternate number of Coordinator

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($request->assigned!=null && $altcoordmob !=NULL && $active_state==0)
                                {
                                    $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                    $phonenimber=$altcoordmob ;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name."Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                            $phonenimber=$altcoordmob ;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }


                                /* SMS functionality for PSC Form when Coordinator is assigned ends here -- by jatin*/

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=Null && $active_state==0)
                                {

                                    Mail::send('mail', $data, function($message)use($data) {
                                        $message->to($data['to'], $data['user'] )->subject
                                        ('Lead ['.$data['leadid'].'] Assigned!!!');
                                        $message->from($data['from'],$data['who']);
                                    });
                                }

                                /*
                                mail end
                                */
                            }
                            else
                            {
                                if($w == NULL)
                                {

                                    $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->select('id')->get();
                                    $verticalcoordination=json_decode($verticalcoordination);
                                    $verticalcoordination=$verticalcoordination[0]->id;

                                    $verticalcoordination=verticalcoordination::find($verticalcoordination);
                                    $verticalcoordination->empid=$e;
                                    $verticalcoordination->ename=$request->assigned;

                                    /*
                                    Assessment Form by vertical head starts here -- by jatin
                                    refer to Mathrutvam form for comments
                                    */
                                    $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                                    $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                                    $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                                    $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                                    if(!$request->assigned)
                                    {
                                        $activity = new Activity;
                                        $activity->emp_id = $emp_id;
                                        $activity->activity = "Lead to be assessed by Vertical Head";
                                        $activity->activity_time = new \DateTime();
                                        $activity->log_id = $log_id;
                                        $activity->lead_id = $leadid;
                                        $activity->save();
                                    }

                                    else
                                    {
                                        $activity = new Activity;
                                        $activity->emp_id = $emp_id;
                                        $activity->activity = "Lead assigned to ".$request->assigned ;
                                        $activity->activity_time = new \DateTime();
                                        $activity->log_id = $log_id;
                                        $activity->lead_id = $leadid;
                                        $activity->save();
                                    }


                                    /*
                                    Assessment Form by vertical head starts here -- by jatin
                                    */


                                    $verticalcoordination->save();




                                    /*
                                    Mail when assigned to coordinator
                                    */
                                    $n=$request->assigned;
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');
                                    $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');
                                    $who = $name1;
                                    $from = DB::table('admins')->where('name',$name1)->value('email');
                                    $to = DB::table('admins')->where('name',$n)->value('email');
                                    $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');


                                    $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type'=>$service_type];
                                    //name of coordinator to whom assessment has been assigned
                                    $coordname = $request->assigned;
                                    //coordinator contact number
                                    $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                                    //coordinator alternate contact number
                                    $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    //service type retrieval done here
                                    $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                                    /* SMS functionality for PSC Form when Coordinator is assigned starts here -- by jatin   ---- Working*/

                                    // dd($request->assigned);
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($request->assigned && $coordmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                        $phonenimber=$coordmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                                $phonenimber=$coordmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    //Message going to alternate number of Coordinator

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($request->assigned && $altcoordmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                        $phonenimber=$altcoordmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                                                $phonenimber=$altcoordmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }


                                    /* SMS functionality for PSC Form when Coordinator is assigned ends here -- by jatin*/

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($to!=Null && $active_state==0)
                                    {
                                        Mail::send('mail', $data, function($message)use($data) {
                                            $message->to($data['to'], $data['user'] )->subject
                                            ('Lead ['.$data['leadid'].'] Assigned!!!');
                                            $message->from($data['from'],$data['who']);
                                        });
                                    }

                                    /*
                                    mail end
                                    */


                                }
                                //sms when coordinator is assigned in Lead Creation and the assesment is done
                                else
                                {
                                    //coordinator's name
                                    $n=$request->assigned;

                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    //vertical head name
                                    $user=DB::table('employees')->where('FirstName',$n)->value('FirstName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //customer alternate mobile number
                                    $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    // coordinator contact number
                                    $coordmob = DB::table('employees')->where('FirstName',$n)->value('MobileNumber');

                                    //coordinator alternate contact number
                                    $altcoordmob = DB::table('employees')->where('FirstName',$n)->value('AlternateNumber');

                                    $who = $name1;
                                    $from = DB::table('admins')->where('name',$name1)->value('email');
                                    $to = DB::table('admins')->where('name',$n)->value('email');

                                    $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                    /*
                                    SMS to Customer when Assessment done by Vertical Lead For Personal Supportive Care Form starts here -- by jatin
                                    */

                                    //vertical contact number
                                    $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                                    //vertical alternate contact number
                                    $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');


                                    //customer name
                                    $name=DB::table('leads')->where('id',$leadid)->value('fName');

                                    //customer mobile number
                                    $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                                    //location selected during lead creation
                                    $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                                    if($current_status!=$request->ServiceStatus)
                                    {
                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($contact!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$contact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                    - Health Heal";
                                                    $phonenimber=$contact;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }
                                        //Message going to alternate number of customer

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($altcontact!=NULL && $active_state==0)
                                        {


                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$altcontact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$leadid.". Looking forward to serve you better.
                                                    - Health Heal";
                                                    $phonenimber=$altcontact;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }
                                        /*
                                        SMS to Customer Assessment done by Vertical Head For Mathrutvam Form ends here -- by jatin
                                        */

                                        /*
                                        SMS to Vertical Head when Assessment done by Vertical Lead For Mathrutvam Form starts here -- by jatin
                                        */

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($vmob!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                            Thank You.";
                                            $phonenimber=$vmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                    Thank You.";
                                                    $phonenimber=$vmob;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }

                                        //Message going to alternate number of vh

                                        $active_state = DB::table('sms_email_filter')->value('active_state');

                                        if($altvmob!=NULL && $active_state==0)
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                            Thank You.";
                                            $phonenimber=$altvmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);

                                            $i=0;
                                            //sending a message again if not sent first time
                                            while($report!="OK" || $i<=1)
                                            {
                                                if($report!="OK")
                                                {
                                                    $message="Dear ".$name1.", The assessment for the ID ".$leadid." is completed.
                                                    Thank You.";
                                                    $phonenimber=$altvmob;
                                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                    $ch =curl_init($url);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    $curl_scraped_page =curl_exec($ch);
                                                    $report = substr($curl_scraped_page,0,2);

                                                    curl_close($ch);
                                                }
                                                $i++;
                                            }
                                        }
                                    }
                                    /*
                                    SMS when Assessment done by Vertical Head For Personal Supportive Care Form ends here -- by jatin
                                    */
                                }
                            }

                            session()->put('name',$name1);
                            return redirect('/vh');

                        }
                    }

                }

                /**
                * Display the specified resource.
                *
                * @param  int  $id
                * @return \Illuminate\Http\Response
                */
                public function show($id)
                {
                    //
                }

                /**
                * Show the form for editing the specified resource.
                *
                * @param  int  $id
                * @return \Illuminate\Http\Response
                */
                public function edit($id)
                {
                    $assessment = assessment::find($id);
                    return view('verticalheads.assessmentedit',compact('assessment'));
                }

                /**
                * Update the specified resource in storage.
                *
                * @param  \Illuminate\Http\Request  $request
                * @param  int  $id
                * @return \Illuminate\Http\Response
                */

                public function update(Request $request, $id)
                {
                    $lead = DB::table('leads')->where('id',$id)->select('id')->get();
                    $lead=json_decode($lead);
                    $lead=$lead[0]->id;
                    //echo $lead;
                    $lead = DB::table('leads')->where('id',$id)->select('id')->get();
                    $lead=json_decode($lead);
                    $lead=$lead[0]->id;



                    //echo $lead;

                    $leadcheck=DB::table('mars')->where('leadid',$lead)->value('id');

                    if($leadcheck==NULL)
                    {
                        $mara=new mar;

                        $mara->leadid=$lead;
                        $mara->save();
                        $marid=DB::table('mars')->max('id');
                    }
                    else
                    {
                        $marid=DB::table('mars')->where('leadid',$lead)->value('id');

                        $mar=mar::find($marid);
                        $mar->FormType=NULL;
                        $mar->Prescribedby=NULL;
                        $mar->RegNo=NULL;
                        $mar->PresDate=NULL;
                        $mar->MedicationForm=NULL;
                        $mar->Medication=NULL;
                        $mar->Route=NULL;
                        $mar->Frequency=NULL;
                        $mar->Dosage=NULL;
                        $mar->ValidTill=NULL;
                        $mar->ACPC=NULL;
                        $mar->save();

                    }


                    //Prescribed Doctor Sarts here
                    $firstdoctornoofmedicine=$request->PrescribedbyDocfirstnomedicices;
                    $noofPrescribedDoctors = $request->NumberofPrescribedbydoc;
                    //dd($noofPrescribedDoctors);
                    // dd($firstdoctornoofmedicine);
                    $prescribed="Prescribed";
                    $NumberofmedicineofPrescribeddoctors = [];
                    for($i=2; $i<=$noofPrescribedDoctors; $i++)
                    {
                        $fieldname= $prescribed.$i;
                        $value=$request->$fieldname;
                        array_push($NumberofmedicineofPrescribeddoctors,$value);
                    }
                    // dd($NumberofmedicineofPrescribeddoctors);
                    //to fecth doctor details of prscribed by first
                    $firstdocprescribedbyname=$request->doc1;
                    $firstdocprescribedbyregid=$request->reg1;
                    $firstdocprescribedbydate1=$request->date1;
                    // dd($firstdocprescribedbyregid);
                    $formTypevalue1="PB";
                    // for first doctor all Medicine to Fetch
                    $tocheckoutputeachmedicine = [];

                    if($firstdocprescribedbyname!=NULL)
                    {
                        for($j=1 ; $j <= $firstdoctornoofmedicine ; $j++ )
                        {

                            $valuetobefetchedmedictionform = "doc1med".$j."form";
                            $Medication = "doc1med".$j."medication";
                            $Route = "doc1med".$j."route";
                            $Frquency = "doc1med".$j."Frquency";
                            $Dosage = "doc1med".$j."dosage";
                            $Validtill = "doc1med".$j."validtill";
                            $ACPC = "doc1med".$j."ACPC";
                            array_push($tocheckoutputeachmedicine,$request->$Medication);


                            $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                            $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                            $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                            $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                            $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                            $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                            $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                            $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                            $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                            $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');
                            $ACPCvalue=DB::table('mars')->where('id',$marid)->value('ACPC');


                            if($request->$Medication != NULL)
                            {
                                //dd($request->$valuetobefetchedmedictionform);

                                $mar=mar::find($marid);
                                $mar->FormType=$formTypevalue.$formTypevalue1."\r\n";
                                $mar->Prescribedby=$Prescribedbyvalue.$firstdocprescribedbyname."\r\n";
                                $mar->RegNo=$RegNovalue.$firstdocprescribedbyregid."\r\n";
                                $mar->PresDate=$PresDatevalue.$firstdocprescribedbydate1."\r\n";
                                $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionform."\r\n";
                                $mar->Medication=$Medicationvalue.$request->$Medication."\r\n";
                                $mar->Route=$Routevalue.$request->$Route."\r\n";
                                $mar->Frequency=$Frequencyvalue.$request->$Frquency."\r\n";
                                $mar->Dosage=$Dosagevalue.$request->$Dosage."\r\n";
                                $mar->ValidTill=$ValidTillvalue.$request->$Validtill."\r\n";
                                $mar->ACPC=$ACPCvalue.$request->$ACPC."\r\n";
                                $mar->save();
                            }


                        }
                    }

                    // dd($tocheckoutputeachmedicine);
                    //for other added more doctors
                    //dd($noofPrescribedDoctors,$NumberofmedicineofPrescribeddoctors);
                    if($noofPrescribedDoctors > 1)
                    {
                        $check = [];
                        $k=0;
                        for($m=2; $m <= $noofPrescribedDoctors; $m++)
                        {    //to fetch Doctor details
                            $valueofdocp="doc".$m;
                            $pdocname=$request->$valueofdocp;
                            $regidp="reg".$m;
                            $pdocregid=$request->$regidp;
                            $prescriptionpdate="date".$m;

                            $pdocpdate=$request->$prescriptionpdate;
                            // dd($request->$prescriptionpdate);

                            $toloop = $NumberofmedicineofPrescribeddoctors[$k];
                            // to fetch medicine details
                            for($n=1 ; $n <= $toloop ; $n++)
                            {

                                $valuetobefetchedmedictionformseconddoconwards = "doc".$m."med".$n."form";
                                $Medication = "doc".$m."med".$n."medication";
                                $DateandTime = "doc".$m."med".$n."dateandtime";
                                $Route = "doc".$m."med".$n."route";
                                $Frquency = "doc".$m."med".$n."Frquency";
                                $Dosage = "doc".$m."med".$n."dosage";
                                $Validtill = "doc".$m."med".$n."validtill";
                                $ACPC = "doc".$m."med".$n."ACPC";

                                array_push($check,$request->$valuetobefetchedmedictionformseconddoconwards);


                                $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                                $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                                $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                                $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                                $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                                $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                                $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                                $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                                $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                                $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');
                                $ACPCvalue=DB::table('mars')->where('id',$marid)->value('ACPC');


                                if($request->$Medication != NULL)
                                {
                                    $mar=mar::find($marid);
                                    $mar->FormType=$formTypevalue.$formTypevalue1."\r\n";
                                    $mar->Prescribedby=$Prescribedbyvalue.$pdocname."\r\n";
                                    $mar->RegNo=$RegNovalue.$pdocregid."\r\n";
                                    $mar->PresDate=$PresDatevalue.$pdocpdate."\r\n";
                                    $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionformseconddoconwards."\r\n";
                                    $mar->Medication=$Medicationvalue.$request->$Medication."\r\n";
                                    $mar->Route=$Routevalue.$request->$Route."\r\n";
                                    $mar->Frequency=$Frequencyvalue.$request->$Frquency."\r\n";
                                    $mar->Dosage=$Dosagevalue.$request->$Dosage."\r\n";
                                    $mar->ValidTill=$ValidTillvalue.$request->$Validtill."\r\n";
                                    $mar->ACPC=$ACPCvalue.$request->$ACPC."\r\n";
                                    $mar->save();

                                }

                            }

                            $k=$k+1;
                        }

                    }

                    //    //Prescribed Doctor Ends here
                    //    //Nebulization  Start here

                    //    $NumberofNebulizationdoc=$request->NumberofNebulizationdoc;
                    //    //Nebulization for first doc starts
                    //    $noofmedicine= $request->nebulizationnoofmedioffirstdoc;
                    //    $firstdoc=[];
                    //    $nebilizationdoc1name=$request->nebulizationdoc1;
                    //    $nebilizationdoc1reg=$request->nebulizationreg1;
                    //    $nebilizationdoc1date=$request->nebulizationdate1;
                    //    $formTypevalue2="MED";
                    //    for($q=1; $q<= $noofmedicine ; $q++ )
                    //    {
                    //        $formvaluetobefetched="nebulizationdoc1med".$q."form";
                    //        $medicationvaluetobefetched="nebulizationdoc1med".$q."medication";
                    //        $routevaluetobefetched="nebulizationdoc1med".$q."route";
                    //        $Frquencyvaluetobefetched="nebulizationdoc1med".$q."Frquency";
                    //        $dosagevaluetobefetched="nebulizationdoc1med".$q."dosage";
                    //        $validtillvaluetobefetched="nebulizationdoc1med".$q."validtill";

                    //        // array_push($firstdoc,$request->$valuetobefetched);


                    //        $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                    //          $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                    //          $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                    //          $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                    //          $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                    //          $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                    //          $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                    //          $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                    //          $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                    //          $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');


                    //          if($request->$formvaluetobefetched != NULL || $request->$medicationvaluetobefetched != NULL || $request->$routevaluetobefetched != NULL || $request->$Frquencyvaluetobefetched != NULL)
                    //          {


                    //                 $mar=mar::find($marid);
                    //                  $mar->FormType=$formTypevalue.$formTypevalue2."\r\n";
                    //                 $mar->Prescribedby=$Prescribedbyvalue.$nebilizationdoc1name."\r\n";
                    //                 $mar->RegNo=$RegNovalue.$nebilizationdoc1reg."\r\n";
                    //                 $mar->PresDate=$PresDatevalue.$nebilizationdoc1date."\r\n";
                    //                 $mar->MedicationForm=$MedicationFormvalue.$request->$formvaluetobefetched."\r\n";
                    //                 $mar->Medication=$Medicationvalue.$request->$medicationvaluetobefetched."\r\n";
                    //                 $mar->Route=$Routevalue.$request->$routevaluetobefetched."\r\n";
                    //                 $mar->Frequency=$Frequencyvalue.$request->$Frquencyvaluetobefetched."\r\n";
                    //                 $mar->Dosage=$Dosagevalue.$request->$dosagevaluetobefetched."\r\n";
                    //                 $mar->ValidTill=$ValidTillvalue.$request->$validtillvaluetobefetched."\r\n";
                    //                 $mar->save();

                    //            }




                    //    }
                    //    //Nebulization for first doc ends

                    //    //Nebulization from second doc starts



                    //    $Nebulization="nebulizationnoofeachdocsmedicine";
                    //    $Numberofmedicineofeachnebulizationddoctors = [];
                    //    //dd($NumberofNebulizationdoc);
                    //    for($i=2; $i<=$NumberofNebulizationdoc; $i++)
                    //    {
                    //        $fieldname= $Nebulization.$i;
                    //        $value=$request->$fieldname;
                    //        array_push($Numberofmedicineofeachnebulizationddoctors,$value);
                    //    }
                    //    //dd($NumberofNebulizationdoc,$Numberofmedicineofeachnebulizationddoctors);

                    //     if($NumberofNebulizationdoc > 1)
                    //    {

                    //     $checknebulizationfromseconddoc= [];
                    //     $s=0;
                    //     for($p=2;$p <= $NumberofNebulizationdoc ; $p++)
                    //     {
                    //            $toloopnebulization = $Numberofmedicineofeachnebulizationddoctors[$s];
                    //        $valueofdoc="nebulizationdoc".$p;
                    //         $nebulizationdocname=$request->$valueofdoc;
                    //         $regid="nebulizationreg".$p;
                    //         $nebulizationdocregid=$request->$regid;
                    //         $prescriptionnebulizationdate="nebulizationdate".$p;
                    //         $nebulizationdocpdate=$request->$prescriptionnebulizationdate;
                    //        for($n=1 ; $n <= $toloopnebulization ; $n++)
                    //       {

                    //         $valuetobefetchedmedictionformseconddoconwardsneb = "docnebulization".$p."med".$n."form";
                    //         $Medicationnebulization = "docnebulization".$p."med".$n."medication";

                    //         $Routenebulization = "docnebulization".$p."med".$n."route";
                    //         $Frquencynebulization = "docnebulization".$p."med".$n."Frquency";
                    //         $Dosagenebulization = "docnebulization".$p."med".$n."dosage";
                    //         $Validtillnebulization = "docnebulization".$p."med".$n."validtill";

                    //         array_push($checknebulizationfromseconddoc,$nebulizationdocname);


                    //          $formTypevalue=DB::table('mars')->where('id',$marid)->value('FormType');
                    //          $Prescribedbyvalue=DB::table('mars')->where('id',$marid)->value('Prescribedby');
                    //          $RegNovalue=DB::table('mars')->where('id',$marid)->value('RegNo');
                    //          $PresDatevalue=DB::table('mars')->where('id',$marid)->value('PresDate');
                    //          $MedicationFormvalue=DB::table('mars')->where('id',$marid)->value('MedicationForm');
                    //          $Medicationvalue=DB::table('mars')->where('id',$marid)->value('Medication');
                    //          $Routevalue=DB::table('mars')->where('id',$marid)->value('Route');
                    //          $Frequencyvalue=DB::table('mars')->where('id',$marid)->value('Frequency');
                    //          $Dosagevalue=DB::table('mars')->where('id',$marid)->value('Dosage');
                    //          $ValidTillvalue=DB::table('mars')->where('id',$marid)->value('ValidTill');

                    //          if($request->$valuetobefetchedmedictionformseconddoconwardsneb != NULL || $request->$Medicationnebulization != NULL || $request->$Routenebulization != NULL || $request->$Frquencynebulization != NULL)
                    //          {


                    //          $mar=mar::find($marid);
                    //          $mar->FormType=$formTypevalue.$formTypevalue2."\r\n";
                    //          $mar->Prescribedby=$Prescribedbyvalue.$nebulizationdocname."\r\n";
                    //          $mar->RegNo=$RegNovalue.$nebulizationdocregid."\r\n";
                    //          $mar->PresDate=$PresDatevalue.$nebulizationdocpdate."\r\n";
                    //          $mar->MedicationForm=$MedicationFormvalue.$request->$valuetobefetchedmedictionformseconddoconwardsneb."\r\n";
                    //          $mar->Medication=$Medicationvalue.$request->$Medicationnebulization."\r\n";
                    //          $mar->Route=$Routevalue.$request->$Routenebulization."\r\n";
                    //          $mar->Frequency=$Frequencyvalue.$request->$Frquencynebulization."\r\n";
                    //          $mar->Dosage=$Dosagevalue.$request->$Dosagenebulization."\r\n";
                    //          $mar->ValidTill=$ValidTillvalue.$request->$Validtillnebulization."\r\n";
                    //          $mar->save();
                    //            }

                    //       }
                    //            $s=$s+1;
                    //     }

                    // }
                    //dd($Numberofmedicineofeachnebulizationddoctors,$checknebulizationfromseconddoc);
                    //Nebulization from second doc Ends


                    $name1=$request->sname;
                    //echo $name1;

                    //code for appending remarks
                    $r=DB::table('services')->where('leadid',$lead)->select('Remarks')->value('Remarks');
                    $vertical_Head_name = DB::table('services')->where('Leadid',$id)->value('AssignedTo');
                    // dd($vertical_Head_name);
                    $a=$request->comment;

                    if($a!=NULL)
                    {
                        $time=time();
                        $timestamp=date('m/d/Y h:i:s a', time());

                        $k =$timestamp."\r\n".$name1.":   ".$a."\r\n\n".$r."\r\n";
                        $re=DB::table('services')->where('leadid', $lead)->update(['Remarks'=>$k]);

                        //send Mail if the comments are updated

                        $vertical_Head_name = DB::table('services')->where('Leadid',$id)->value('AssignedTo');
                        $vertical_email_id = DB::table('employees')->where('FirstName',$vertical_Head_name)->value('Email_id');

                        // dd($vertical_email_id);
                        $comment_data = ['leadid'=>$id, 'vert_name'=>$vertical_Head_name,'vert_email'=>$vertical_email_id,'comment'=>$a];

                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($vertical_email_id!=NULL && $active_state==0)
                        {
                            Mail::send('mail_for_comment', $comment_data, function($message)use($comment_data) {
                                $message->to($comment_data['vert_email'], $comment_data['vert_name'] )->subject
                                ("[Happy to Care Health Heal] Comment Updated For [".$comment_data['leadid']."] !!!");
                                $message->from('care@healthheal.in','Healthheal');
                            });
                        }
                    }
                    //  $name=DB>>>>>>> effb18df417b3633270997f8a5781e5ab6b831ea::table('services')->where('leadid',$id)->select('AssignedTo')->get();
                    // $name=json_decode($name);

                    // $vcid=DB::table('verticalcoordinations')->where('leadid',$id)->select('empid')->get();
                    // $vcid=json_decode($vcid);

                    // if($vcid == [])
                    // {
                    //         $name1= $name[0]->AssignedTo;
                    // }
                    // else
                    // {
                    //   $vcid=$vcid[0]->empid;
                    // $vid=DB::table('employees')->where('id',$vcid)->select('FirstName')->get();
                    // $vid=json_decode($vid);
                    // $name1=$vid[0]->FirstName;

                    // }

                    //     $r=DB::table('services')->where('leadid',$lead)->select('Remarks')->value('Remarks');
                    //
                    //     $append = $request->remark;
                    //
                    //     $re=DB::table('services')->where('id', $lead)->update(['Remarks' =>$r.' '.$append]);

                    $prod = " ";
                    $prof = " ";

                    $p2=DB::table('products')->where('Leadid',$lead)->select('id')->get();
                    $p2=json_decode($p2);
                    //echo $p2;
                    //echo $lead;
                    if($p2 == NULL)
                    {


                        $products = new product;
                        $products->SKUid=$request->SKUid;
                        $products->ProductName=$request->ProductName;
                        $products->DemoRequired=$request->DemoRequired;
                        $products->AvailabilityStatus=$request->AvailabilityStatus;
                        $products->AvailabilityAddress=$request->AvailabilityAddress;
                        $products->SellingPrice=$request->SellingPrice;
                        $products->RentalPrice=$request->RentalPrice;
                        $products->Leadid=$lead;
                        $pid=NULL;

                        if($products->SKUid !=NULL || $products->ProductName !=NULL)
                        {
                            $products->save();
                            $pid=DB::table('products')->max('id');
                        }

                    }
                    else
                    {

                        $products=DB::table('products')->where('Leadid',$lead)->select('id')->get();
                        $products=json_decode($products);
                        $products=$products[0]->id;
                        $pid=$products;
                        // echo $pid;
                        $products=product::find($products);
                        $products->SKUid=$request->SKUid;
                        $products->ProductName=$request->ProductName;
                        $products->DemoRequired=$request->DemoRequired;
                        $products->AvailabilityStatus=$request->AvailabilityStatus;
                        $products->AvailabilityAddress=$request->AvailabilityAddress;
                        $products->SellingPrice=$request->SellingPrice;
                        $products->RentalPrice=$request->RentalPrice;


                        /*
                        Code for capturing the products part of Log Edit starts here -- by jatin
                        */

                        //this will retrieve all the fields and values that are updated in the form of an array
                        $products_c = $products->getDirty();

                        //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                        foreach($products_c as $key=>$value)
                        {
                            $proc[] = $key;

                        }

                        foreach($products_c as $key=>$value)
                        {
                            $prov[] = $value;
                        }

                        //this is for converting array values into a string with new line

                        //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                        if(empty($proc))
                        {
                            $prod = " ";
                            $prof = " ";
                            $prod = trim($prod);
                            $prof = trim($prof);
                        }
                        else
                        {
                            $prod = implode("\r\n", $proc);
                            $prof = implode("\r\n", $prov);
                        }

                        /*
                        Code for capturing the products part of Log Edit ends here -- by jatin
                        */
                        $products->save();


                    }


                    /*
                    Code for capturing requested service and service status starts here -- by jatin
                    */

                    //here we are retrieving the id of the Lead we wish to log for
                    $lead_s = DB::table('leads')->where('id',$id)->value('id');

                    //here we are trying to capture the previous value stored in the requested service field (the one in the db)
                    $current_service = DB::table('services')->where('leadid',$lead_s)->value('requested_service');

                    //here we are trying to capture the previous value stored in the Service Status field (the one in the db)
                    $current_status = DB::table('services')->where('leadid',$lead_s)->value('ServiceStatus');

                    $current_followupdate = DB::table('leads')->where('id',$lead_s)->value('followupdate');
                    $current_reason = DB::table('leads')->where('id',$lead_s)->value('reason');

                    DB::table('services')
                    ->where('leadid', $lead)
                    ->update(['requested_service' => $request->requested_service]);


                    $leadstate=$request->ServiceStatus;

                    if($leadstate=="New")
                    {

                        DB::table('services')->where('leadid', $lead)->update(['ServiceStatus' => "In Progress"]);
                        $a=DB::table('services')->where('leadid', $lead)->get();
                        // dd($a);

                    }
                    else
                    {
                        if($request->ServiceStatus=="Deferred")
                        {
                            if($current_followupdate!= $request->followupdate)
                            {
                                DB::table('leads')->where('id',$lead)->update(['frequency_of_sms'=>NULL]);
                            }
                        }
                        DB::table('services')
                        ->where('leadid', $lead)
                        ->update(['ServiceStatus' => $request->ServiceStatus]);
                    }



                    // DB::table('services')
                    // ->where('leadid', $lead)
                    // ->update(['ServiceStatus' => $request->ServiceStatus]);

                    DB::table('leads')
                    ->where('id', $lead)
                    ->update(['followupdate' => $request->followupdate]);

                    DB::table('leads')
                    ->where('id', $lead)
                    ->update(['reason' => $request->reason]);


                    //if the value from the db doesn't match the one that has been entered by the user Log it as activity values
                    if(trim($current_status) != trim($request->ServiceStatus))
                    {
                        $serd = 'ServiceStatus: ';
                        $serf = $request->ServiceStatus.', ';
                    }
                    else
                    {

                        $serd = ' ';
                        $serf = ' ';
                    }
                    if($current_service != $request->requested_service)
                    {
                        $reqd = 'requested_service: ';
                        $reqf = $request->requested_service.', ';
                    }
                    else
                    {
                        $reqd = ' ';
                        $reqf = ' ';

                    }

                    if(trim($current_followupdate)!= trim($request->followupdate))
                    {
                        $followd = 'Follow Update';
                        $followf = $request->followupdate;
                    }
                    else
                    {
                        $followd = ' ';
                        $followf = ' ';
                    }

                    if(trim($current_reason)!= trim($request->reason))
                    {
                        $reasond = 'Reason For Drop';
                        $reasonf = $request->reason;
                    }
                    else
                    {
                        $reasond = ' ';
                        $reasonf = ' ';
                    }
                    /*
                    Code for capturing requested service status and service status ends here -- by jatin
                    */




                    $assessment=DB::table('assessments')->where('Leadid',$lead)->select('id')->get();
                    $assessment=json_decode($assessment);
                    $assessment=$assessment[0]->id;
                    $assessment=assessment::find($assessment);
                    $assessment->Assessor=$request->Assessor;
                    $assessment->AssessDateTime=$request->AssessDateTime;
                    $assessment->AssessPlace=$request->AssessPlace;
                    $assessment->ServiceStartDate=$request->ServiceStartDate;
                    $assessment->ServicePause=$request->ServicePause;
                    $assessment->ServiceEndDate=$request->ServiceEndDate;
                    $assessment->ShiftPreference=$request->ShiftPreference;
                    $assessment->SpecificRequirements=$request->SpecificRequirements;
                    $assessment->DaysWorked=$request->DaysWorked;
                    $assessment->Latitude=$request->Latitude;
                    $assessment->Longitude=$request->Longitude;
                    $assessment->Productid=$pid;

                    /*
                    Code for capturing assessment part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $assessment_c = $assessment->getDirty();

                    // $matc[] = " ";
                    // $matv[] = " ";

                    //the array fetched needs to be fetched on the basis of key and their corresponding value
                    foreach($assessment_c as $key=>$value)
                    {
                        $assc[] = $key;

                    }

                    foreach($assessment_c as $key=>$value)
                    {
                        $assv[] = $value;
                    }

                    //this is for converting array values into a string with new line
                    if(empty($assc))
                    {
                        $assd = ' ';
                        $assd = trim($assd);

                        $assf = ' ';
                        $assf = trim($assf);
                    }
                    else
                    {
                        $assd = implode("\r\n", $assc);
                        $assf = implode("\r\n ", $assv);
                    }


                    $assessment->save();

                    /*
                    Code for capturing assessment part of Log Edit ends here -- by jatin
                    */

                    $servicetype=DB::table('services')->where('Leadid',$lead)->select('ServiceType')->get();
                    $servicetype=json_decode($servicetype);
                    $servicetype=$servicetype[0]->ServiceType;
                    // echo $servicetype;

                    //not significants for all tables starts here
                    if($request->notsignificantforabdomen == "true")
                    {
                        $notsignificantforabdomen="true";
                    }
                    else
                    {
                        $notsignificantforabdomen=NULL;
                    }


                    $abdomen=DB::table('abdomens')->where('leadid',$lead)->select('id')->get();
                    $abdomen=json_decode($abdomen);
                    $abdomen=$abdomen[0]->id;
                    $abdomen=abdomen::find($abdomen);
                    $abdomen->Inspection=$request->Inspection;
                    $abdomen->AusculationofBS=$request->AusculationofBS;
                    $abdomen->Palpation=$request->Palpation;
                    $abdomen->Percussion=$request->Percussion;
                    $abdomen->Ileostomy=$request->Ileostomy;
                    $abdomen->Colostomy=$request->Colostomy;
                    $abdomen->Functioning=$request->Functioning;
                    $abdomen->Shape=$request->Shape;
                    $abdomen->girth=$request->girth;
                    $abdomen->changeincolor=$request->changeincolor;
                    $abdomen->BulgeType=$request->BulgeType;
                    $abdomen->abdomennotess=$request->abdomennotess;
                    $abdomen->notsignificantforabdomen=$notsignificantforabdomen;

                    /*
                    Code for capturing the abdomen part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $abdomen_c = $abdomen->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($abdomen_c as $key=>$value)
                    {
                        $abdc[] = $key;

                    }

                    foreach($abdomen_c as $key=>$value)
                    {
                        $abdv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($abdc))
                    {
                        $abdd = ' ';
                        $abdf = ' ';
                        $abdd = trim($abdd);
                        $abdf = trim($abdf);
                    }
                    else
                    {
                        $abdd = implode("\r\n", $abdc);
                        $abdf = implode("\r\n", $abdv);
                    }

                    /*
                    Code for capturing the abdomen part of Log Edit ends here -- by jatin
                    */

                    $abdomen->save();

                    if($request->notsignificantforcirculatory == "true")
                    {
                        $notsignificantforcirculatory="true";
                    }
                    else
                    {
                        $notsignificantforcirculatory=NULL;
                    }

                    $hoHTNCADCHF=$request->Historyofcirculatory;
                    // dd($hoHTNCADCHF);
                    $hoHTNCADCHF = str_replace(array('[',']' ,),' ',$hoHTNCADCHF);
                    $hoHTNCADCHF = preg_replace('/"/', '', $hoHTNCADCHF);
                    $hoHTNCADCHF = trim($hoHTNCADCHF);


                    // dd($hoHTNCADCHF);

                    $circulatory=DB::table('circulatories')->where('leadid',$lead)->select('id')->get();
                    $circulatory=json_decode($circulatory);
                    $circulatory=$circulatory[0]->id;
                    $circulatory=circulatory::find($circulatory);
                    $circulatory->ChestPain=$request->ChestPain;
                    $circulatory->hoHTNCADCHF=$hoHTNCADCHF;
                    $circulatory->HR=$request->HR;
                    $circulatory->PeripheralCyanosis=$request->PeripheralCyanosis;
                    $circulatory->JugularVein=$request->JugularVein;
                    $circulatory->SurgeryHistory=$request->SurgeryHistory;
                    $circulatory->circulatorynotes=$request->circulatorynotes;
                    $circulatory->notsignificantforcirculatory=$notsignificantforcirculatory;

                    /*
                    Code for capturing the circulatory part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $circulatory_c = $circulatory->getDirty();
                    dd($circulatory_c);
                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($circulatory_c as $key=>$value)
                    {
                        $circ[] = $key;

                    }

                    foreach($circulatory_c as $key=>$value)
                    {
                        $cirv[] = $value;
                    }
                    // dd($circ,$cirv);
                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($circ))
                    {
                        $cird = ' ';
                        $cirf = ' ';
                        $cird = trim($cird);
                        $cirf = trim($cirf);
                    }
                    else
                    {
                        $cird = implode("\r\n", $circ);
                        $cirf = implode("\r\n", $cirv);
                    }

                    /*
                    Code for capturing the circulatory part of Log Edit ends here -- by jatin
                    */

                    $circulatory->save();

                    $communication=DB::table('communications')->where('leadid',$lead)->select('id')->get();
                    $communication=json_decode($communication);
                    $communication=$communication[0]->id;
                    $communication=communication::find($communication);
                    // $communication->Language=$request->Language;
                    // dd($request->languageschecked);
                    $languagefortrim= $request->languageschecked;
                    // dd($languagefortrim);

                    $Language = str_replace(array('[',']' ,),' ',$languagefortrim);
                    $Language = preg_replace('/"/', '', $Language);
                    $Language = trim($Language);

                    // dd($Language);
                    $communication->Language=$Language;

                    $communication->AdequateforAllActivities=$request->AdequateforAllActivities;
                    $communication->unableToCommunicate=$request->unableToCommunicate;
                    $communication->communicationnotes=$request->communicationnotes;
                    $communication->notsignificantforcommunication=$request->notsignificantforcommunication;

                    /*
                    Code for capturing the communication part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $communication_c = $communication->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($communication_c as $key=>$value)
                    {
                        $comc[] = $key;

                    }

                    foreach($communication_c as $key=>$value)
                    {
                        $comv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($comc))
                    {
                        $comd = ' ';
                        $comf = ' ';
                        $comd = trim($comd);
                        $comf = trim($comf);
                    }
                    else
                    {
                        $comd = implode("\r\n", $comc);
                        $comf = implode("\r\n", $comv);
                    }

                    /*
                    Code for capturing the communication part of Log Edit ends here -- by jatin
                    */

                    $communication->save();


                    if($request->notsignificantfordenture == "true")
                    {
                        $notsignificantfordenture="true";
                    }
                    else
                    {
                        $notsignificantfordenture=NULL;
                    }


                    $dentures=DB::table('dentures')->where('leadid',$lead)->select('id')->get();
                    $dentures=json_decode($dentures);
                    $dentures=$dentures[0]->id;
                    $dentures=denture::find($dentures);
                    $dentures->Upper=$request->Upper;
                    $dentures->Lower=$request->Lower;
                    $dentures->Cleaning=$request->Cleaning;
                    $dentures->denturenotes=$request->denturenotes;
                    $dentures->notsignificantfordenture=$notsignificantfordenture;

                    /*
                    Code for capturing the denture part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $dentures_c = $dentures->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($dentures_c as $key=>$value)
                    {
                        $denc[] = $key;

                    }

                    foreach($dentures_c as $key=>$value)
                    {
                        $denv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($denc))
                    {
                        $dend = ' ';
                        $denf = ' ';
                        $dend = trim($dend);
                        $denf = trim($denf);
                    }
                    else
                    {
                        $dend = implode("\r\n", $denc);
                        $denf = implode("\r\n", $denv);
                    }

                    /*
                    Code for capturing the dentures part of Log Edit ends here -- by jatin
                    */

                    $dentures->save();


                    $extremitie=DB::table('extremities')->where('leadid',$lead)->select('id')->get();
                    $extremitie=json_decode($extremitie);
                    $extremitie=$extremitie[0]->id;
                    $extremitie=extremitie::find($extremitie);
                    $extremitie->UppROM=$request->UppROM;
                    $extremitie->UppMuscleStrength=$request->UppMuscleStrength;
                    $extremitie->LowROM=$request->LowROM;
                    $extremitie->LowMuscleStrength=$request->LowMuscleStrength;

                    /*
                    Code for capturing the extremitie part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $extremitie_c = $extremitie->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($extremitie_c as $key=>$value)
                    {
                        $extc[] = $key;

                    }

                    foreach($extremitie_c as $key=>$value)
                    {
                        $extv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($extc))
                    {
                        $extd = ' ';
                        $extf = ' ';
                        $extd = trim($extd);
                        $extf = trim($extf);
                    }
                    else
                    {
                        $extd = implode("\r\n", $extc);
                        $extf = implode("\r\n", $extv);
                    }

                    /*
                    Code for capturing the extremitie part of Log Edit ends here -- by jatin
                    */

                    $extremitie->save();


                    $Typeoffecal=$request->Typeoffecalchecked;
                    $Typeoffecal = str_replace(array('[',']' ,),' ',$Typeoffecal);
                    $Typeoffecal = preg_replace('/"/', '', $Typeoffecal);
                    $Typeoffecal = trim($Typeoffecal);

                    $fecal=DB::table('fecals')->where('leadid',$lead)->select('id')->get();
                    $fecal=json_decode($fecal);
                    $fecal=$fecal[0]->id;
                    $fecal=fecal::find($fecal);

                    $fecal->FecalType=$request->FecalType;
                    $fecal->IncontinentOccasionally=$request->IncontinentOccasionally;
                    $fecal->IncontinentAlways=$request->IncontinentAlways;
                    $fecal->Commodechair=$request->Commodechair;
                    $fecal->Needassistanceoffecal=$request->Needassistanceoffecal;
                    $fecal->Typeoffecal=$Typeoffecal;
                    $fecal->fecalnotes=$request->fecalnotes;

                    /*
                    Code for capturing the fecal part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $fecal_c = $fecal->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($fecal_c as $key=>$value)
                    {
                        $fecc[] = $key;

                    }

                    foreach($fecal_c as $key=>$value)
                    {
                        $fecv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($fecc))
                    {
                        $fecd = ' ';
                        $fecf = ' ';
                        $fecd = trim($fecd);
                        $fecf = trim($fecf);
                    }
                    else
                    {
                        $fecd = implode("\r\n", $fecc);
                        $fecf = implode("\r\n", $fecv);
                    }

                    /*
                    Code for capturing the fecal part of Log Edit ends here -- by jatin
                    */

                    $fecal->save();


                    $Typeofurinary=$request->Typeofurinarychecked;
                    $Typeofurinary = str_replace(array('[',']' ,),' ',$Typeofurinary);
                    $Typeofurinary = preg_replace('/"/', '', $Typeofurinary);
                    $Typeofurinary = trim($Typeofurinary);


                    $genito=DB::table('genitos')->where('leadid',$lead)->select('id')->get();
                    $genito=json_decode($genito);
                    $genito=$genito[0]->id;



                    if($request->notsignificantforurinary == "true")
                    {
                        $notsignificantforurinary="true";
                    }
                    else
                    {
                        $notsignificantforurinary=NULL;
                    }

                    $genito=genito::find($genito);
                    $genito->UrinaryContinent=$request->UrinaryContinent;
                    $genito->Needassistanceofurinary=$request->Needassistanceofurinary;
                    $genito->Typeofurinary=$Typeofurinary;
                    $genito->urinarynotes=$request->urinarynotes;
                    $genito->HoPOSTTURP=$request->HoPOSTTURP;
                    $genito->HoUTI=$request->HoUTI;
                    $genito->CompletelyContinet=$request->CompletelyContinet;
                    $genito->IncontinentUrineOccasionally=$request->IncontinentUrineOccasionally;
                    $genito->IncontinentUrineNightOnly=$request->IncontinentUrineNightOnly;
                    $genito->IncontinentUrineAlways=$request->IncontinentUrineAlways;
                    $genito->notsignificantforurinary=$notsignificantforurinary;

                    /*
                    Code for capturing the genito part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $genito_c = $genito->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($genito_c as $key=>$value)
                    {
                        $genc[] = $key;

                    }

                    foreach($genito_c as $key=>$value)
                    {
                        $genv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($genc))
                    {
                        $gend = ' ';
                        $genf = ' ';
                        $gend = trim($gend);
                        $genf = trim($genf);
                    }
                    else
                    {
                        $gend = implode("\r\n", $genc);
                        $genf = implode("\r\n", $genv);
                    }

                    /*
                    Code for capturing the genito part of Log Edit ends here -- by jatin
                    */

                    $genito->save();



                    $generalcondition=DB::table('generalconditions')->where('leadid',$lead)->select('id')->get();
                    $generalcondition=json_decode($generalcondition);
                    $generalcondition=$generalcondition[0]->id;
                    $generalcondition=generalcondition::find($generalcondition);
                    $generalcondition->Weight=$request->Weight;
                    $generalcondition->Height=$request->Height;
                    $generalcondition->Height_Measured_In=$request->Height_Measured_In;
                    $generalcondition->BMI=$request->BMI;
                    $generalcondition->MedicalHistory=$request->MedicalHistory;

                    /*
                    Code for capturing the generalcondition part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $generalcondition_c = $generalcondition->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($generalcondition_c as $key=>$value)
                    {
                        $genec[] = $key;

                    }

                    foreach($generalcondition_c as $key=>$value)
                    {
                        $genev[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($genec))
                    {
                        $gened = ' ';
                        $genef = ' ';
                        $gened = trim($gened);
                        $genef = trim($genef);
                    }
                    else
                    {
                        $gened = implode("\r\n", $genec);
                        $genef = implode("\r\n", $genev);
                    }

                    /*
                    Code for capturing the generalcondition part of Log Edit ends here -- by jatin
                    */

                    $generalcondition->save();



                    $generalhistory=DB::table('generalhistories')->where('leadid',$lead)->select('id')->get();
                    $generalhistory=json_decode($generalhistory);
                    $generalhistory=$generalhistory[0]->id;
                    $generalhistory=generalhistory::find($generalhistory);


                    $generalhistory->PresentHistory=$request->PresentHistory;
                    $generalhistory->PastHistory=$request->PastHistory;
                    $generalhistory->FamilyHistory=$request->FamilyHistory;
                    $generalhistory->Mensural_OBGHistory=$request->Mensural_OBGHistory;
                    $generalhistory->AllergicHistory=$request->AllergicHistory;
                    $generalhistory->AllergicStatus=$request->AllergicStatus;
                    $generalhistory->AllergicSeverity=$request->AllergicSeverity;

                    /*
                    Code for capturing the generalhistory part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $generalhistory_c = $generalhistory->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($generalhistory_c as $key=>$value)
                    {
                        $genhc[] = $key;

                    }

                    foreach($generalhistory_c as $key=>$value)
                    {
                        $genhv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($genhc))
                    {
                        $genhd = ' ';
                        $genhf = ' ';
                        $genhd = trim($genhd);
                        $genhf = trim($genhf);
                    }
                    else
                    {
                        $genhd = implode("\r\n", $genhc);
                        $genhf = implode("\r\n", $genhv);
                    }

                    /*
                    Code for capturing the generalhistory part of Log Edit ends here -- by jatin
                    */
                    $generalhistory->save();


                    $memoryintact=DB::table('memoryintacts')->where('leadid',$lead)->select('id')->get();
                    $memoryintact=json_decode($memoryintact);
                    $memoryintact=$memoryintact[0]->id;
                    $memoryintact=memoryintact::find($memoryintact);
                    $memoryintact->ShortTerm=$request->ShortTerm;
                    $memoryintact->LongTerm=$request->LongTerm;
                    $memoryintact->memoryintactnotes=$request->memoryintactnotes;
                    $memoryintact->notsignificantformemoryintact=$request->notsignificantformemoryintact;

                    /*
                    Code for capturing the memoryintact part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $memoryintact_c = $memoryintact->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($memoryintact_c as $key=>$value)
                    {
                        $memoc[] = $key;

                    }

                    foreach($memoryintact_c as $key=>$value)
                    {
                        $memov[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($memoc))
                    {
                        $memod = ' ';
                        $memof = ' ';
                        $memod = trim($memod);
                        $memof = trim($memof);
                    }
                    else
                    {
                        $memod = implode("\r\n", $memoc);
                        $memof = implode("\r\n", $memov);
                    }

                    /*
                    Code for capturing the memoryintact part of Log Edit ends here -- by jatin
                    */

                    $memoryintact->save();


                    $Typeofmobility=$request->Typeofmobilitychecked;
                    $Typeofmobility = str_replace(array('[',']' ,),' ',$Typeofmobility);
                    $Typeofmobility = preg_replace('/"/', '', $Typeofmobility);
                    $Typeofmobility = trim($Typeofmobility);



                    if($request->notsignificantformobility == "true")
                    {
                        $notsignificantformobility="true";
                    }
                    else
                    {
                        $notsignificantformobility=NULL;
                    }

                    $mobility=DB::table('mobilities')->where('leadid',$lead)->select('id')->get();
                    $mobility=json_decode($mobility);
                    $mobility=$mobility[0]->id;
                    $mobility=mobility::find($mobility);
                    $mobility->Independent=$request->Independent;
                    $mobility->NeedAssistance=$request->NeedAssistance;
                    $mobility->Typeofmobility=$Typeofmobility;
                    $mobility->mobilitynotes=$request->mobilitynotes;
                    $mobility->Walker=$request->Walker;
                    $mobility->WheelChair=$request->WheelChair;
                    $mobility->Crutch=$request->Crutch;
                    $mobility->Cane=$request->Cane;
                    $mobility->ChairFast=$request->ChairFast;
                    $mobility->BedFast=$request->BedFast;
                    $mobility->notsignificantformobility=$notsignificantformobility;

                    /*
                    Code for capturing the mobility part of Log Edit starts here -- by jatin
                    Refer to comments for Products to understand what is happening in each field

                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $mobility_c = $mobility->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($mobility_c as $key=>$value)
                    {
                        $mobc[] = $key;

                    }

                    foreach($mobility_c as $key=>$value)
                    {
                        $mobv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($mobc))
                    {
                        $mobd = ' ';
                        $mobf = ' ';
                        $mobd = trim($mobd);
                        $mobf = trim($mobf);
                    }
                    else
                    {
                        $mobd = implode("\r\n", $mobc);
                        $mobf = implode("\r\n", $mobv);
                    }

                    /*
                    Code for capturing the mobility part of Log Edit ends here -- by jatin
                    */

                    $mobility->save();



                    $Eyeopening=$request->Eyeopening;
                    $Eyeopening= explode(" ",$Eyeopening);
                    $count=count($Eyeopening);
                    unset($Eyeopening[$count-1]);
                    $Eyeopening=implode(" ", $Eyeopening);



                    $verbalresponse=$request->verbalresponse;
                    $verbalresponse= explode(" ",$verbalresponse);
                    $count2=count($verbalresponse);
                    unset($verbalresponse[$count2-1]);
                    $verbalresponse=implode(" ", $verbalresponse);

                    $motorresponse=$request->motorresponse;
                    $motorresponse= explode(" ",$motorresponse);
                    $count3=count($motorresponse);
                    unset($motorresponse[$count3-1]);
                    $motorresponse=implode(" ", $motorresponse);


                    // dd($request->totalscoregcs);


                    $orientation=DB::table('orientations')->where('leadid',$lead)->select('id')->get();
                    $orientation=json_decode($orientation);
                    $orientation=$orientation[0]->id;
                    $orientation=orientation::find($orientation);
                    $orientation->Person=$request->Person;
                    $orientation->Place=$request->Place;
                    $orientation->Time=$request->Time;
                    $orientation->Eyeopening=$Eyeopening;
                    $orientation->verbalresponse=$verbalresponse;
                    $orientation->motorresponse=$motorresponse;
                    $orientation->totalscoregcs=$request->totalscoregcs;
                    $orientation->orientationnotes=$request->orientationnotes;
                    $orientation->notsignificantfornervoussystem=$request->notsignificantfornervoussystem;




                    /*
                    Code for capturing the orientation part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $orientation_c = $orientation->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($orientation_c as $key=>$value)
                    {
                        $oric[] = $key;

                    }

                    foreach($orientation_c as $key=>$value)
                    {
                        $oriv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($oric))
                    {
                        $orid = ' ';
                        $orif = ' ';
                        $orid = trim($orid);
                        $orif = trim($orif);
                    }
                    else
                    {
                        $orid = implode("\r\n", $oric);
                        $orif = implode("\r\n", $oriv);
                    }

                    /*
                    Code for capturing the orientation part of Log Edit ends here -- by jatin
                    */

                    $orientation->save();




                    $vitalsigns=DB::table('vitalsigns')->where('leadid',$lead)->select('id')->get();
                    $vitalsigns=json_decode($vitalsigns);
                    $vitalsigns=$vitalsigns[0]->id;
                    $vitalsigns=vitalsign::find($vitalsigns);
                    $vitalsigns->BP=$request->BP;
                    $vitalsigns->BP_equipment=$request->BP_equipment;
                    $vitalsigns->BP_taken_from=$request->BP_taken_from;
                    $vitalsigns->RR=$request->RR;
                    $vitalsigns->Temperature=$request->Temperature;
                    $vitalsigns->TemperatureType=$request->TemperatureType;
                    $vitalsigns->vitalnotes=$request->vitalnotes;
                    $vitalsigns->Temperature_Measured_In=$request->Temperature_Measured_In;
                    $vitalsigns->P1=$request->P1;
                    $vitalsigns->P2=$request->P2;
                    $vitalsigns->P3=$request->P3;
                    $vitalsigns->R1=$request->R1;
                    $vitalsigns->R2=$request->R2;
                    $vitalsigns->R3=$request->R3;
                    $vitalsigns->R4=$request->R4;
                    $vitalsigns->T1=$request->T1;
                    $vitalsigns->T2=$request->T2;
                    $vitalsigns->Pulse=$request->Pulse;
                    $vitalsigns->PainScale=$request->PainScale;
                    $vitalsigns->Quality=$request->Quality;
                    $vitalsigns->SeverityScale=$request->SeverityScale;
                    $vitalsigns->MedicalDiagnosis=$request->MedicalDiagnosis;
                    $vitalsigns->q1=$request->q1;

                    /*
                    Code for capturing the vitalsigns part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $vitalsigns_c = $vitalsigns->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($vitalsigns_c as $key=>$value)
                    {
                        $vitc[] = $key;

                    }

                    foreach($vitalsigns_c as $key=>$value)
                    {
                        $vitv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($vitc))
                    {
                        $vitd = ' ';
                        $vitf = ' ';
                        $vitd = trim($vitd);
                        $vitf = trim($vitf);
                    }
                    else
                    {
                        $vitd = implode("\r\n", $vitc);
                        $vitf = implode("\r\n", $vitv);
                    }

                    /*
                    Code for capturing the vitalsigns part of Log Edit ends here -- by jatin
                    */
                    $vitalsigns->save();

                    if($request->notsignificantforrespiratory == "true")
                    {
                        $notsignificantforrespiratory="true";
                    }
                    else
                    {
                        $notsignificantforrespiratory=NULL;
                    }




                    $H_OTB_Asthma_COPD=$request->Historyofrespiratory;

                    $H_OTB_Asthma_COPD = str_replace(array('[',']' ,),' ',$H_OTB_Asthma_COPD);
                    $H_OTB_Asthma_COPD = preg_replace('/"/', '', $H_OTB_Asthma_COPD);
                    $H_OTB_Asthma_COPD = trim($H_OTB_Asthma_COPD);



                    $respiratory=DB::table('respiratories')->where('leadid',$lead)->select('id')->get();
                    $respiratory=json_decode($respiratory);
                    $respiratory=$respiratory[0]->id;
                    $respiratory=respiratory::find($respiratory);
                    $respiratory->SOB=$request->SOB;
                    $respiratory->Cough=$request->Cough;
                    $respiratory->ColorOfPhlegm=$request->ColorOfPhlegm;
                    $respiratory->Nebulization=$request->Nebulization;
                    $respiratory->Tracheostomy=$request->Tracheostomy;
                    $respiratory->CPAP_BIPAP=$request->CPAP_BIPAP;
                    $respiratory->ICD=$request->ICD;
                    $respiratory->placementoficd=$request->placementoficd;
                    $respiratory->noofdays=$request->noofdays;
                    $respiratory->respiratorynotes=$request->respiratorynotes;
                    $respiratory->H_OTB_Asthma_COPD=$H_OTB_Asthma_COPD;
                    $respiratory->SPO2=$request->SPO2;
                    $respiratory->central_cynaosis=$request->central_cynaosis;
                    $respiratory->notsignificantforrespiratory=$notsignificantforrespiratory;

                    /*
                    Code for capturing the respiratory part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $respiratory_c = $respiratory->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($respiratory_c as $key=>$value)
                    {
                        $resc[] = $key;

                    }

                    foreach($respiratory_c as $key=>$value)
                    {
                        $resv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($resc))
                    {
                        $resd = ' ';
                        $resf = ' ';
                        $resd = trim($resd);
                        $resf = trim($resf);
                    }
                    else
                    {
                        $resd = implode("\r\n", $resc);
                        $resf = implode("\r\n", $resv);
                    }

                    /*
                    Code for capturing the respiratory part of Log Edit ends here -- by jatin
                    */
                    $respiratory->save();


                    $Position_Type11=$request->Position_Typechecked;

                    $Position_Type11 = str_replace(array('[',']' ,),' ',$Position_Type11);
                    $Position_Type11 = preg_replace('/"/', '', $Position_Type11);
                    $Position_Type11 = trim($Position_Type11);



                    if($request->notsignificantforposition == "true")
                    {
                        $notsignificantforposition="true";
                    }
                    else
                    {
                        $notsignificantforposition=NULL;
                    }



                    $position=DB::table('positions')->where('leadid',$lead)->select('id')->get();
                    $position=json_decode($position);
                    $position=$position[0]->id;
                    $position=position::find($position);
                    $position->Position_Type=$Position_Type11;
                    $position->positionnotes=$request->positionnotes;
                    $position->notsignificantforposition=$notsignificantforposition;

                    /*
                    Code for capturing the position part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $position_c = $position->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($position_c as $key=>$value)
                    {
                        $posc[] = $key;

                    }

                    foreach($position_c as $key=>$value)
                    {
                        $posv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($posc))
                    {
                        $posd = ' ';
                        $posf = ' ';
                        $posd = trim($posd);
                        $posf = trim($posf);
                    }
                    else
                    {
                        $posd = implode("\r\n", $posc);
                        $posf = implode("\r\n", $posv);
                    }

                    /*
                    Code for capturing the position part of Log Edit ends here -- by jatin
                    */

                    $position->save();

                    if($request->notsignificantforvision == "true")
                    {
                        $visionnotsignificant="true";
                    }
                    else
                    {
                        $visionnotsignificant=NULL;
                    }

                    if($request->hearingnotsignificant == "true")
                    {
                        $hearingnotsignificant="true";
                    }
                    else
                    {
                        $hearingnotsignificant=NULL;
                    }

                    $visionhearing=DB::table('visionhearings')->where('leadid',$lead)->select('id')->get();
                    $visionhearing=json_decode($visionhearing);
                    $visionhearing=$visionhearing[0]->id;
                    $visionhearing=visionhearing::find($visionhearing);
                    $visionhearing->Impared=$request->Impared;
                    $visionhearing->HImpared=$request->HImpared;
                    $visionhearing->ShortSight=$request->ShortSight;
                    $visionhearing->LongSight=$request->LongSight;
                    $visionhearing->WearsGlasses=$request->WearsGlasses;
                    $visionhearing->HearingAids=$request->HearingAids;
                    $visionhearing->visionnotes=$request->visionnotes;
                    $visionhearing->hearingnotes=$request->hearingnotes;
                    $visionhearing->notsignificantforvision=$visionnotsignificant;
                    $visionhearing->notsignificantforhear=$hearingnotsignificant;


                    /*
                    Code for capturing the visionhearing part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $visionhearing_c = $visionhearing->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($visionhearing_c as $key=>$value)
                    {
                        $visc[] = $key;

                    }

                    foreach($visionhearing_c as $key=>$value)
                    {
                        $visv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($visc))
                    {
                        $visd = ' ';
                        $visf = ' ';
                        $visd = trim($visd);
                        $visf = trim($visf);
                    }
                    else
                    {
                        $visd = implode("\r\n", $visc);
                        $visf = implode("\r\n", $visv);
                    }

                    /*
                    Code for capturing the visionhearing part of Log Edit ends here -- by jatin
                    */

                    $visionhearing->save();



                    $nutrition=DB::table('nutrition')->where('leadid',$lead)->select('id')->get();
                    $nutrition=json_decode($nutrition);
                    $nutrition=$nutrition[0]->id;
                    $nutrition=nutrition::find($nutrition);
                    $nutrition->Intact=$request->Intact;
                    $nutrition->Type=$request->Type;
                    $nutrition->nutritionnotes=$request->nutritionnotes;
                    $nutrition->Diet=$request->Diet;
                    $nutrition->BFTime=$request->BFTime;
                    $nutrition->LunchTime=$request->LunchTime;
                    $nutrition->SnacksTime=$request->SnacksTime;
                    $nutrition->DinnerTime=$request->DinnerTime;
                    $nutrition->TPN=$request->TPN;
                    $nutrition->RTFeeding=$request->RTFeeding;
                    $nutrition->RTFeeding_Measured_In=$request->RTFeeding_Measured_In;
                    $nutrition->PEGFeeding_Measured_In=$request->PEGFeeding_Measured_In;
                    $nutrition->PEGFeeding=$request->PEGFeeding;
                    $nutrition->feedingtype=$request->feedingtype;
                    $nutrition->frequency=$request->frequency;
                    $nutrition->routes=$request->routes;
                    $nutrition->Quantityinfusion=$request->Quantityinfusion;
                    $nutrition->timeinfusion=$request->timeinfusion;
                    $nutrition->infusionratedropspermin=$request->infusionratedropspermin;
                    $nutrition->diettnotes=$request->diettnotes;
                    $nutrition->notsignificantfornutrition=$request->notsignificantfornutrition;

                    /*
                    Code for capturing the nutrition part of Log Edit starts here -- by jatin
                    */

                    //this will retrieve all the fields and values that are updated in the form of an array
                    $nutrition_c = $nutrition->getDirty();

                    //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                    foreach($nutrition_c as $key=>$value)
                    {
                        $nutc[] = $key;

                    }

                    foreach($nutrition_c as $key=>$value)
                    {
                        $nutv[] = $value;
                    }

                    //this is for converting array values into a string with new line

                    //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                    if(empty($nutc))
                    {
                        $nutd = ' ';
                        $nutf = ' ';
                        $nutd = trim($nutd);
                        $nutf = trim($nutf);
                    }
                    else
                    {
                        $nutd = implode("\r\n", $nutc);
                        $nutf = implode("\r\n", $nutv);
                    }

                    /*
                    Code for capturing the nutrition part of Log Edit ends here -- by jatin
                    */

                    $nutrition->save();



                    $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');

                    $w=DB::table('verticalcoordinations')->where('leadid',$lead)->value('empid');
                    //not significants ends here


                    if($servicetype == "Mathrutvam - Baby Care")
                    {
                        //
                        //dd($request->ImmunizationUpdatedvalue);
                        $mother=DB::table('mothers')->where('Leadid',$lead)->select('id')->get();
                        $mother=json_decode($mother);
                        $motherid=$mother[0]->id;
                        $mother=mother::find($motherid);

                        // dd($request->all());

                        $mother->FName=$request->FName;
                        $mother->MName=$request->MName;
                        $mother->LName=$request->LName;
                        $mother->Medications=$request->Medications;
                        $mother->MedicalDiagnosis=$request->MedicalDiagnosis;
                        $mother->DeliveryPlace=$request->DeliveryPlace;
                        $mother->DueDate=$request->DueDate;
                        $mother->DeliveryType=$request->DeliveryType;
                        $mother->NoOfBabies=$request->NoOfBabies;
                        $mother->NoOfAttenderRequired=$request->NoOfAttenderRequired;
                        $mother->Allergies=$request->Allergies;

                        /*
                        Code for capturing mother part of Log Edit starts here -- by jatin
                        Refer to comments for Products to understand what is happening in each field

                        */

                        //this will retrieve all the fields and values that are updated in the form of an array
                        $mother_c = $mother->getDirty();

                        // $matc[] = ' ';
                        // $matv[] = ' ';
                        foreach($mother_c as $key=>$value)
                        {
                            $motc[] = $key;

                        }

                        foreach($mother_c as $key=>$value)
                        {
                            $motv[] = $value;
                        }

                        //this is for converting array values into a string with new line

                        //  Here we are checking whether the user has tried to change this field or not
                        if(empty($motc))
                        {
                            $motd = ' ';
                            $motf = ' ';
                            $motd = trim($motd);
                            $motf = trim($motf);
                        }
                        else
                        {
                            $motd = implode("\r\n", $motc);
                            $motf = implode("\r\n", $motv);
                        }

                        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                        $lead_id = DB::table('assessments')->where('Leadid',$lead)->value('id');

                        /*
                        Code for capturing mother part of Log Edit ends here -- by jatin
                        */

                        $mother->save();

                        // dd($motherid);

                        $current_timestamp = date('d-m-Y H:i:s');

                        $oldImmunizationval = DB::table('mathruthvams')->where('Motherid',$motherid)->value('ImmunizationUpdated');

                        // dd($request->ImmunizationUpdatedvalue);
                        if($request->ImmunizationUpdatedvalue!=NULL)
                        {
                            if($oldImmunizationval==NULL)
                            {
                                // $final_Immunizationupdated = $current_timestamp.' '.$request->ImmunizationUpdatedvalue;

                                $final_Immunizationupdated = explode(",",$request->ImmunizationUpdatedvalue);
                                // dd($final_Immunizationupdated);
                                $count_for_Immunization = count($final_Immunizationupdated);

                                for($i=0;$i<$count_for_Immunization;$i++)
                                {
                                    $final_Immunizationupdated[$i] = $current_timestamp." ".$final_Immunizationupdated[$i];
                                }

                                $final_Immunizationupdated = implode(",",$final_Immunizationupdated);

                                // dd($final_Immunizationupdated);

                            }
                            else
                            {
                                $oldImmunizationval = explode(",",$oldImmunizationval);
                                $no_of_oldimmunization = count($oldImmunizationval);

                                $newImmunizationvalue = $request->ImmunizationUpdatedvalue;
                                $newImmunizationvalue = explode(",",$newImmunizationvalue);
                                $no_of_newImmunization = count($newImmunizationvalue);

                                $oldImmunization_data = array();

                                for($i=0;$i<$no_of_oldimmunization;$i++)
                                {
                                    $oldImmunization_date[] = substr($oldImmunizationval[$i],0,19);
                                    $oldImmunizationval[$i] = substr($oldImmunizationval[$i],20);
                                }

                                // dd($oldImmunization_date);

                                // dd($oldImmunizationval,$newImmunizationvalue);

                                $final_Immunizationupdated = array();
                                $k=0;
                                for($i=0;$i<$no_of_newImmunization;$i++)
                                {
                                    for($j=0;$j<$no_of_oldimmunization;$j++)
                                    {
                                        if($oldImmunizationval[$j]==$newImmunizationvalue[$i])
                                        {
                                            $final_Immunizationupdated[] = $oldImmunization_date[$j]." ".$oldImmunizationval[$j];
                                            $old_date = $oldImmunization_date[$j];
                                            $k = $j;
                                        }
                                        // else
                                        // {
                                        //     $old_date = $oldImmunization_date[$k];
                                        //     break;
                                        // }
                                        $old_date = $oldImmunization_date[$k];


                                    }

                                    if(!in_array($old_date." ".$newImmunizationvalue[$i],$final_Immunizationupdated))
                                    {
                                        $final_Immunizationupdated[] = $current_timestamp." ".$newImmunizationvalue[$i];
                                    }

                                }

                                $final_Immunizationupdated = implode(",",$final_Immunizationupdated);


                                // dd($final_Immunizationupdated);

                                // $newImmunization_with_date = array();
                                // for($i=$no_of_oldimmunization;$i<$no_of_newImmunization;$i++)
                                // {
                                //     $newImmunization_with_date[] = $newImmunizationvalue[$i];
                                // }
                                // // dd($newImmunization_with_date);
                                //
                                // $oldImmunizationval = DB::table('mathruthvams')->where('Motherid',$motherid)->value('ImmunizationUpdated');
                                // $oldImmunizationval = explode(",",$oldImmunizationval);
                                // $no_of_oldimmunization = count($oldImmunizationval);
                                //
                                // $newImmunizationvalue = $request->ImmunizationUpdatedvalue;
                                // $newImmunizationvalue = explode(",",$newImmunizationvalue);
                                // $no_of_newImmunization = count($newImmunizationvalue);
                                //
                                // // dd($no_of_oldimmunization,$no_of_newImmunization);
                                // $loop = $no_of_newImmunization - $no_of_oldimmunization;
                                // for($i=0;$i<$loop;$i++)
                                // {
                                //     $newImmunization_with_date[$i] = $current_timestamp." ".$newImmunization_with_date[$i];
                                // }
                                // dd($newImmunization_with_date);

                                // $final_Immunizationupdated = array_merge($oldImmunizationval, $newImmunization_with_date);
                                //
                                // $final_Immunizationupdated = implode(",",$final_Immunizationupdated);
                                // dd($final_Immunizationupdated);
                            }
                        }
                        else
                        {
                            $final_Immunizationupdated = NULL;
                        }


                        $mathruthvam=DB::table('mathruthvams')->where('Motherid',$motherid)->select('id')->get();
                        $mathruthvam=json_decode($mathruthvam);
                        $mathruthvam=$mathruthvam[0]->id;


                        // dd($final_Immunizationupdated);
                        $Feedingissue11=$request->Feedingissue12;

                        $Feedingissue11 = str_replace(array('[',']' ,),' ',$Feedingissue11);
                        $Feedingissue11 = preg_replace('/"/', '', $Feedingissue11);
                        $Feedingissue11 = trim($Feedingissue11);


                        $mathruthvam=mathruthvam::find($mathruthvam);
                        $mathruthvam->ChildDOB=$request->ChildDOB;
                        $mathruthvam->ChildMedicalDiagnosis=$request->ChildMedicalDiagnosis;
                        $mathruthvam->ChildMedications=$request->ChildMedications;
                        $mathruthvam->ImmunizationUpdated=$final_Immunizationupdated;
                        $mathruthvam->BirthWeight=$request->BirthWeight;
                        $mathruthvam->DischargeWeight=$request->DischargeWeight;
                        $mathruthvam->FeedingFormula=$request->FeedingFormula;
                        $mathruthvam->Quantity=$request->Quantity;
                        $mathruthvam->Frequency=$request->Frequency;
                        $mathruthvam->FeedingIssue=$Feedingissue11;
                        $mathruthvam->BabyAllergies=$request->BabyAllergies;
                        $mathruthvam->FeedingType=$request->FeedingType;
                        $mathruthvam->FeedingEstablished=$request->FeedingEstablished;
                        $mathruthvam->other=$request->other;
                        $mathruthvam->Length=$request->Length;
                        $mathruthvam->HeadCircumference=$request->HeadCircumference;
                        $mathruthvam->ChestCircumference=$request->ChestCircumference;
                        $mathruthvam->MidArmCircumference=$request->MidArmCircumference;
                        $mathruthvam->SleepingPattern=$request->SleepingPattern;
                        // dd($request->ImmunizationUpdatedvalue);
                        /*
                        Code for capturing mathruthvam part of Log Edit starts here -- by jatin
                        Refer to comments for Products to understand what is happening in each field

                        */

                        //this will retrieve all the fields and values that are updated in the form of an array
                        $mathruthvam_c = $mathruthvam->getDirty();

                        foreach($mathruthvam_c as $key=>$value)
                        {
                            $matc[] = $key;

                        }

                        foreach($mathruthvam_c as $key=>$value)
                        {
                            $matv[] = $value;
                        }

                        //this is for converting array values into a string with new line
                        if(empty($matc))
                        {
                            $matd = ' ';
                            $matf = ' ';
                            $matd = trim($matd);
                            $matf = trim($matf);
                        }
                        else
                        {
                            $matd = implode("\r\n", $matc);
                            $matf = implode("\r\n", $matv);
                        }


                        /*
                        Code for capturing mathruthvam part of Log Edit ends here -- by jatin
                        */

                        $mathruthvam->save();
                        // dd($request->DenverMuscles,$request->Denvercoordinationeyes);

                        $denverid = DB::table('denverscales')->where('leadid',$lead)->value('id');

                        if($request->DenverMuscles!=NULL)
                        {
                            $Muscles_old = DB::table('denverscales')->where('id',$denverid)->value('Muscles');

                            if($Muscles_old==NULL)
                            {
                                $Muscles = explode(",",$request->DenverMuscles);
                                // dd($final_Immunizationupdated);
                                $count_for_muscles = count($Muscles);

                                for($i=0;$i<$count_for_muscles;$i++)
                                {
                                    $Muscles[$i] = $current_timestamp." ".$Muscles[$i];
                                }

                                $Muscles = implode(",",$Muscles);

                            }
                            else
                            {
                                $Muscles_old = DB::table('denverscales')->where('id',$denverid)->value('Muscles');

                                $Muscles_old = explode(",",$Muscles_old);
                                $no_of_oldMuscles = count($Muscles_old);

                                $Muscles_new = $request->DenverMuscles;
                                $Muscles_new = explode(",",$Muscles_new);
                                $no_of_newMuscles = count($Muscles_new);

                                $Muscles_old_date = array();

                                for($i=0;$i<$no_of_oldMuscles;$i++)
                                {
                                    $Muscles_old_date[] = substr($Muscles_old[$i],0,19);
                                    $Muscles_old[$i] = substr($Muscles_old[$i],20);
                                }
                                // dd($Muscles_old_date,$Muscles_old,$Muscles_new);
                                $Muscles = array();

                                $k=0;
                                for($i=0;$i<$no_of_newMuscles;$i++)
                                {
                                    for($j=0;$j<$no_of_oldMuscles;$j++)
                                    {
                                        if($Muscles_old[$j]==$Muscles_new[$i])
                                        {
                                            $Muscles[] = $Muscles_old_date[$j]." ".$Muscles_old[$j];
                                            $old_date = $Muscles_old_date[$j];
                                            $k= $j;
                                        }
                                        $old_date = $Muscles_old_date[$k];

                                    }

                                    if(!in_array($old_date." ".$Muscles_new[$i],$Muscles))
                                    {
                                        $Muscles[] = $current_timestamp." ".$Muscles_new[$i];
                                    }
                                }
                                $Muscles = implode(",",$Muscles);
                                // dd($Muscles);
                            }
                        }
                        else
                        {
                            $Muscles = NULL;
                        }

                        // dd($request->Denvercoordinationeyes);
                        //for Coordination Eyes code for date updation starts here
                        if($request->Denvercoordinationeyes!=NULL)
                        {
                            $Coordination_Eyes_old = DB::table('denverscales')->where('id',$denverid)->value('Coordination_Eyes');

                            // dd($Coordination_Eyes_old);
                            if($Coordination_Eyes_old==NULL)
                            {
                                $Coordination_Eyes = explode(",",$request->Denvercoordinationeyes);
                                // dd($final_Immunizationupdated);
                                $count_for_coeyes = count($Coordination_Eyes);

                                for($i=0;$i<$count_for_coeyes;$i++)
                                {
                                    $Coordination_Eyes[$i] = $current_timestamp." ".$Coordination_Eyes[$i];
                                }

                                $Coordination_Eyes = implode(",",$Coordination_Eyes);
                            }
                            else
                            {
                                $Coordination_Eyes_old = DB::table('denverscales')->where('id',$denverid)->value('Coordination_Eyes');

                                $Coordination_Eyes_old = explode(",",$Coordination_Eyes_old);
                                $no_of_oldCoordination_Eyes = count($Coordination_Eyes_old);

                                $Coordination_Eyes_new = $request->Denvercoordinationeyes;
                                $Coordination_Eyes_new = explode(",",$Coordination_Eyes_new);
                                $no_of_newCoordination_Eyes = count($Coordination_Eyes_new);

                                $Coordination_Eyes_old_date = array();

                                for($i=0;$i<$no_of_oldCoordination_Eyes;$i++)
                                {
                                    $Coordination_Eyes_old_date[] = substr($Coordination_Eyes_old[$i],0,19);
                                    $Coordination_Eyes_old[$i] = substr($Coordination_Eyes_old[$i],20);
                                }
                                // dd($Coordination_Eyes_old_date,$Coordination_Eyes_old,$Coordination_Eyes_new);
                                $Coordination_Eyes = array();

                                $k=0;
                                for($i=0;$i<$no_of_newCoordination_Eyes;$i++)
                                {
                                    for($j=0;$j<$no_of_oldCoordination_Eyes;$j++)
                                    {
                                        if($Coordination_Eyes_old[$j]==$Coordination_Eyes_new[$i])
                                        {
                                            $Coordination_Eyes[] = $Coordination_Eyes_old_date[$j]." ".$Coordination_Eyes_old[$j];
                                            $old_date = $Coordination_Eyes_old_date[$j];
                                            $k= $j;
                                        }
                                        $old_date = $Coordination_Eyes_old_date[$k];

                                    }

                                    if(!in_array($old_date." ".$Coordination_Eyes_new[$i],$Coordination_Eyes))
                                    {
                                        $Coordination_Eyes[] = $current_timestamp." ".$Coordination_Eyes_new[$i];
                                    }
                                }
                                $Coordination_Eyes = implode(",",$Coordination_Eyes);
                            }
                        }
                        else
                        {
                            $Coordination_Eyes = NULL;
                        }

                        //date append for Hearing Speech starts here

                        if($request->Denverhearingspeech!=NULL)
                        {
                            $Hearing_Speech_old = DB::table('denverscales')->where('id',$denverid)->value('Hearing_Speech');

                            // dd($Coordination_Eyes_old);
                            if($Hearing_Speech_old==NULL)
                            {
                                $Hearing_Speech = explode(",",$request->Denverhearingspeech);
                                // dd($final_Immunizationupdated);
                                $count_for_hs = count($Hearing_Speech);

                                for($i=0;$i<$count_for_hs;$i++)
                                {
                                    $Hearing_Speech[$i] = $current_timestamp." ".$Hearing_Speech[$i];
                                }

                                $Hearing_Speech = implode(",",$Hearing_Speech);
                            }
                            else
                            {
                                $Hearing_Speech_old = DB::table('denverscales')->where('id',$denverid)->value('Hearing_Speech');

                                $Hearing_Speech_old = explode(",",$Hearing_Speech_old);
                                $no_of_oldHearing_Speech = count($Hearing_Speech_old);

                                $Hearing_Speech_new = $request->Denverhearingspeech;
                                $Hearing_Speech_new = explode(",",$Hearing_Speech_new);
                                $no_of_newHearing_Speech = count($Hearing_Speech_new);

                                $Hearing_Speech_old_date = array();

                                for($i=0;$i<$no_of_oldHearing_Speech;$i++)
                                {
                                    $Hearing_Speech_old_date[] = substr($Hearing_Speech_old[$i],0,19);
                                    $Hearing_Speech_old[$i] = substr($Hearing_Speech_old[$i],20);
                                }
                                // dd($Hearing_Speech_old_date,$Hearing_Speech_old,$Hearing_Speech_new);
                                $Hearing_Speech = array();

                                $k=0;
                                for($i=0;$i<$no_of_newHearing_Speech;$i++)
                                {
                                    for($j=0;$j<$no_of_oldHearing_Speech;$j++)
                                    {
                                        if($Hearing_Speech_old[$j]==$Hearing_Speech_new[$i])
                                        {
                                            $Hearing_Speech[] = $Hearing_Speech_old_date[$j]." ".$Hearing_Speech_old[$j];
                                            $old_date = $Hearing_Speech_old_date[$j];
                                            $k= $j;
                                        }
                                        $old_date = $Hearing_Speech_old_date[$k];

                                    }

                                    if(!in_array($old_date." ".$Hearing_Speech_new[$i],$Hearing_Speech))
                                    {
                                        $Hearing_Speech[] = $current_timestamp." ".$Hearing_Speech_new[$i];
                                    }
                                }
                                $Hearing_Speech = implode(",",$Hearing_Speech);
                            }
                        }
                        else
                        {
                            $Hearing_Speech = NULL;
                        }

                        //date append for Social Skills starts here

                        if($request->Denversocailskills!=NULL)
                        {
                            $Social_Skills_old = DB::table('denverscales')->where('id',$denverid)->value('Social_Skills');

                            // dd($Coordination_Eyes_old);
                            if($Social_Skills_old==NULL)
                            {
                                $Social_Skills = explode(",",$request->Denversocailskills);
                                // dd($final_Immunizationupdated);
                                $count_for_ss = count($Social_Skills);

                                for($i=0;$i<$count_for_ss;$i++)
                                {
                                    $Social_Skills[$i] = $current_timestamp." ".$Social_Skills[$i];
                                }

                                $Social_Skills = implode(",",$Social_Skills);

                            }
                            else
                            {
                                $Social_Skills_old = DB::table('denverscales')->where('id',$denverid)->value('Social_Skills');

                                $Social_Skills_old = explode(",",$Social_Skills_old);
                                $no_of_oldSocial_Skills = count($Social_Skills_old);

                                $Social_Skills_new = $request->Denversocailskills;
                                $Social_Skills_new = explode(",",$Social_Skills_new);
                                $no_of_newSocial_Skills = count($Social_Skills_new);

                                $Social_Skills_old_date = array();

                                for($i=0;$i<$no_of_oldSocial_Skills;$i++)
                                {
                                    $Social_Skills_old_date[] = substr($Social_Skills_old[$i],0,19);
                                    $Social_Skills_old[$i] = substr($Social_Skills_old[$i],20);
                                }
                                // dd($Social_Skills_old_date,$Social_Skills_old,$Social_Skills_new);
                                $Social_Skills = array();

                                $k=0;
                                for($i=0;$i<$no_of_newSocial_Skills;$i++)
                                {
                                    for($j=0;$j<$no_of_oldSocial_Skills;$j++)
                                    {
                                        if($Social_Skills_old[$j]==$Social_Skills_new[$i])
                                        {
                                            $Social_Skills[] = $Social_Skills_old_date[$j]." ".$Social_Skills_old[$j];
                                            $old_date = $Social_Skills_old_date[$j];
                                            $k= $j;
                                        }
                                        $old_date = $Social_Skills_old_date[$k];

                                    }

                                    if(!in_array($old_date." ".$Social_Skills_new[$i],$Social_Skills))
                                    {
                                        $Social_Skills[] = $current_timestamp." ".$Social_Skills_new[$i];
                                    }
                                }
                                $Social_Skills = implode(",",$Social_Skills);
                            }
                        }
                        else
                        {
                            $Social_Skills = NULL;
                        }


                        // date append for Safety starts here

                        if($request->Denversafety!=NULL)
                        {
                            $Safety_old = DB::table('denverscales')->where('id',$denverid)->value('Safety');

                            // dd($Coordination_Eyes_old);
                            if($Safety_old==NULL)
                            {
                                $Safety = explode(",",$request->Denversafety);
                                // dd($final_Immunizationupdated);
                                $count_for_safety = count($Safety);

                                for($i=0;$i<$count_for_safety;$i++)
                                {
                                    $Safety[$i] = $current_timestamp." ".$Safety[$i];
                                }

                                $Safety = implode(",",$Safety);
                            }
                            else
                            {
                                $Safety_old = DB::table('denverscales')->where('id',$denverid)->value('Safety');

                                $Safety_old = explode(",",$Safety_old);
                                $no_of_oldSafety = count($Safety_old);

                                $Safety_new = $request->Denversafety;
                                $Safety_new = explode(",",$Safety_new);
                                $no_of_newSafety = count($Safety_new);

                                $Safety_old_date = array();

                                for($i=0;$i<$no_of_oldSafety;$i++)
                                {
                                    $Safety_old_date[] = substr($Safety_old[$i],0,19);
                                    $Safety_old[$i] = substr($Safety_old[$i],20);
                                }
                                // dd($Safety_old_date,$Safety_old,$Safety_new);
                                $Safety = array();

                                $k=0;
                                for($i=0;$i<$no_of_newSafety;$i++)
                                {
                                    for($j=0;$j<$no_of_oldSafety;$j++)
                                    {
                                        if($Safety_old[$j]==$Safety_new[$i])
                                        {
                                            $Safety[] = $Safety_old_date[$j]." ".$Safety_old[$j];
                                            $old_date = $Safety_old_date[$j];
                                            $k= $j;
                                        }
                                        $old_date = $Safety_old_date[$k];

                                    }

                                    if(!in_array($old_date." ".$Safety_new[$i],$Safety))
                                    {
                                        $Safety[] = $current_timestamp." ".$Safety_new[$i];
                                    }
                                }
                                $Safety = implode(",",$Safety);
                            }
                        }
                        else
                        {
                            $Safety = NULL;
                        }
                        // $Safety_old = DB::table('denverscales')->where('id',$denverid)->value('Safety');
                        // if($Safety_old==NULL)
                        // {
                        //     $Safety = $request->Denversafety;
                        // }
                        // else
                        // {
                        //     $Safety = $Safety_old.",".$request->Denversafety;
                        // }

                        // dd($denverid);




                        $denver = denverscale::find($denverid);
                        $denver->Muscles = $Muscles;
                        $denver->Coordination_Eyes = $Coordination_Eyes;
                        $denver->Hearing_Speech = $Hearing_Speech;
                        $denver->Social_Skills = $Social_Skills;
                        $denver->Safety = $Safety;

                        $denver_c = $denver->getDirty();

                        // dd($denver_c);

                        foreach($denver_c as $key=>$value)
                        {
                            $denverc[] = $key;
                        }
                        foreach($denver_c as $key=>$value)
                        {
                            $denverv[] = $value;
                        }

                        if(empty($denverc))
                        {
                            $denverd = ' ';
                            $denverd = trim($denverd);

                            $denverf = ' ';
                            $denverf = trim($denverf);
                        }
                        else
                        {
                            $denverd = implode("\r\n", $denverc);
                            $denverf = implode("\r\n", $denverv);
                        }
                        $denver->save();
                        //dd($Muscles);

                        /*
                        Code for capturing assigned to status starts here -- by jatin
                        */

                        /*
                        Code for capturing assigned to status starts here -- by jatin
                        */

                        $lead_s = DB::table('leads')->where('id',$id)->value('id');
                        // $current_assignedTo = DB::table('employees')->where('FirstName',$request->assigned)->value('FirstName');

                        //if a new request is assigned and it is not null or space,
                        // add the assigned to field and value to db as activity along with others
                        if($request->assigned)
                        {
                            $asid = 'AssignedTo';
                            $asif = $request->assigned;
                        }
                        else
                        {

                            $asid = ' ';
                            $asif = ' ';
                        }

                        /*
                        Code for capturing assigned to ends here -- by jatin
                        */

                        if($w == NULL)
                        {

                            $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$lead)->select('id')->get();
                            $verticalcoordination=json_decode($verticalcoordination);
                            $verticalcoordination=$verticalcoordination[0]->id;

                            $verticalcoordination=verticalcoordination::find($verticalcoordination);
                            $verticalcoordination->empid=$e;
                            $verticalcoordination->ename=$request->assigned;

                            $verticalcoordination->save();

                            /*
                            Mail when assigned to coordinator
                            */
                            $n=$request->assigned;
                            $name=DB::table('leads')->where('id',$lead)->value('fName');
                            $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');
                            $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                            $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                            $who = $name1;
                            $from = DB::table('admins')->where('name',$name1)->value('email');
                            $to = DB::table('admins')->where('name',$n)->value('email');
                            $service_type=DB::table('services')->where('leadid',$lead)->value('ServiceType');


                            $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type'=>$service_type];

                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($to!=NULL && $active_state==0)
                            {
                                Mail::send('mail', $data, function($message)use($data) {
                                    $message->to($data['to'], $data['user'] )->subject
                                    ('Lead ['.$data['leadid'].'] Assigned!!!');
                                    $message->from($data['from'],$data['who']);
                                });

                            }
                            /*
                            mail end
                            */

                        }




                        /*
                        Mail when assessment is complete to coordinator
                        */


                        $n=$request->assigned;

                        //customer name
                        $name=DB::table('leads')->where('id',$lead)->value('fName');

                        //vertical lead name
                        $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');


                        $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                        $who = $name1;
                        $from = DB::table('admins')->where('name',$name1)->value('email');
                        $to = DB::table('admins')->where('name',$user)->value('email');
                        $to1 = DB::table('leads')->where('id',$lead)->value('EmailId');
                        //customer mobile number
                        $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                        $service_type=DB::table('services')->where('leadid',$lead)->value('ServiceType');

                        $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type'=>$service_type];

                        $data1 = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who,'service_type'=>$service_type];



                        //vertical contact number
                        $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                        //vertical alternate contact number
                        $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');


                        //customer name
                        $name=DB::table('leads')->where('id',$lead)->value('fName');

                        //customer mobile number
                        $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');

                        //customer alternate mobile number
                        $altcontact=DB::table('leads')->where('id',$lead)->value('Alternatenumber');


                        //location selected during lead creation
                        $location=DB::table('services')->where('leadid',$lead)->value('Branch');

                        /*
                        SMS to Customer when Assessment done by Vertical Lead For Mathurtvam Form starts here -- by jatin
                        */
                        // when status is changed from "IN progress" to any other
                        if($request->ServiceStatus!="In Progress" )
                        {
                            // dd($current_status,$request->ServiceStatus);
                            if($current_status!= $request->ServiceStatus)
                            {
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($contact!=NULL && $active_state==0)
                                {

                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                    - Health Heal";
                                    $phonenimber=$contact;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$contact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }



                                //Message going to alternate numbero of customer

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($altcontact!=NULL && $active_state==0)
                                {


                                    $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                    - Health Heal";
                                    $phonenimber=$altcontact;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                            - Health Heal";
                                            $phonenimber=$altcontact;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                /*
                                SMS to Customer Assessment done by Vertical Head For Mathrutvam Form ends here -- by jatin
                                */

                                /*
                                SMS to Vertical Head when Assessment done by Vertical Lead For Mathrutvam Form starts here -- by jatin
                                */

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($vmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                    Thank You.";
                                    $phonenimber=$vmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                            Thank You.";
                                            $phonenimber=$vmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }

                                //Message going to alternate number of vh

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($altvmob!=NULL && $active_state==0)
                                {
                                    $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                    Thank You.";
                                    $phonenimber=$altvmob;
                                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                    $ch =curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $curl_scraped_page =curl_exec($ch);
                                    $report = substr($curl_scraped_page,0,2);

                                    curl_close($ch);

                                    $i=0;
                                    //sending a message again if not sent first time
                                    while($report!="OK" || $i<=1)
                                    {
                                        if($report!="OK")
                                        {
                                            $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                            Thank You.";
                                            $phonenimber=$altvmob;
                                            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                            $ch =curl_init($url);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            $curl_scraped_page =curl_exec($ch);
                                            $report = substr($curl_scraped_page,0,2);

                                            curl_close($ch);
                                        }
                                        $i++;
                                    }
                                }
                            }


                            /*
                            SMS when Assessment done by Vertical Head For Mathurtvam Form ends here -- by jatin
                            */

                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($to!=NULL && $active_state==0)
                            {
                                Mail::send('mail2', $data, function($message)use($data) {
                                    $message->to($data['to'],$data['user']  )->subject
                                    ('Lead ['.$data['leadid'].']  Assessment Completed!!!');
                                    $message->from($data['from'],$data['who']);
                                });
                            }

                            $active_state = DB::table('sms_email_filter')->value('active_state');

                            if($to1 !=NULL && $active_state==0)
                            {

                                Mail::send('mail3', $data1, function($message)use($data1) {
                                    $message->to($data1['to'], $data1['user'] )->subject
                                    ('Lead ['.$data1['leadid'].']  Assessment Completed!!!');
                                    $message->from('care@healthheal.in',$data1['name']);
                                });


                            }
                        }
                        /*
                        mail end
                        */

                        /*
                        Code for final updation of activity with field and values starts here -- by jatin
                        */
                        if($followd==" " || $followd==' ' || $followd=="" || $followd=='')
                        {
                            $followd = trim($followd);
                            $followf = trim($followf);
                        }
                        else
                        {
                            $followd = $followd."\r\n";
                            $followf = $followf."\r\n";
                        }
                        if($reasond==" " || $reasond==' ' || $reasond=="" || $reasond=='')
                        {
                            $reasond = trim($reasond);
                            $reasonf = trim($reasonf);
                        }
                        else
                        {
                            $reasond = $reasond."\r\n";
                            $reasonf = $reasonf."\r\n";
                        }
                        if($reqd==" " || $reqd==' ' || $reqd=="" || $reqd=='')
                        {
                            $reqd = trim($reqd);
                            $reqf = trim($reqf);
                        }
                        else
                        {
                            $reqd = $reqd."\r\n";
                            $reqf = $reqf."\r\n";
                        }
                        if($serd==" " || $serd==' ' || $serd=="" || $serd=='')
                        {
                            $serd = trim($serd);
                            $serf = trim($serf);
                        }
                        else
                        {
                            $serd = $serd."\r\n";
                            $serf = $serf."\r\n";
                        }
                        if($asid==" " || $asid==' ' || $asid=="" || $asid=='')
                        {
                            $asid = trim($asid);
                            $asif = trim($asif);
                        }
                        else
                        {
                            $asid = $asid."\r\n";
                            $asif = $asif."\r\n";
                        }
                        if($assd==" " || $assd==' ' || $assd=="" || $assd=='')
                        {
                            $assd = trim($assd);
                            $assf = trim($assf);
                        }
                        else
                        {
                            $assd = $assd."\r\n";
                            $assf = $assf."\r\n";
                        }
                        if($motd==" " || $motd==' ' || $motd=="" || $motd=='')
                        {
                            $motd = trim($motd);
                            $motf = trim($motf);
                        }
                        else
                        {
                            $motd = $motd."\r\n";
                            $motf = $motf."\r\n";
                        }
                        if($matd==" " || $matd==' ' || $matd=="" || $matd=='')
                        {
                            $matd = trim($matd);
                            $matf = trim($matf);
                        }
                        else
                        {
                            $matd = $matd."\r\n";
                            $matf = $matf."\r\n";
                        }
                        if($abdd==" " || $abdd==' ' || $abdd=="" || $abdd=='')
                        {
                            $abdd = trim($abdd);
                            $abdf = trim($abdf);
                        }
                        else
                        {
                            $abdd = $abdd."\r\n";
                            $abdf = $abdf."\r\n";
                        }
                        if($cird==" " || $cird==' ' || $cird=="" || $cird=='')
                        {
                            $cird = trim($cird);
                            $cirf = trim($cirf);
                        }
                        else
                        {
                            $cird = $cird."\r\n";
                            $cirf = $cirf."\r\n";
                        }
                        if($comd==" " || $comd==' ' || $comd=="" || $comd=='')
                        {
                            $comd = trim($comd);
                            $comf = trim($comf);
                        }
                        else
                        {
                            $comd = $comd."\r\n";
                            $comf = $comf."\r\n";
                        }
                        if($dend==" " || $dend==' ' || $dend=="" || $dend=='')
                        {
                            $dend = trim($dend);
                            $denf = trim($denf);
                        }
                        else
                        {
                            $dend = $dend."\r\n";
                            $denf = $denf."\r\n";
                        }
                        if($extd==" " || $extd==' ' || $extd=="" || $extd=='')
                        {
                            $extd = trim($extd);
                            $extf = trim($extf);
                        }
                        else
                        {
                            $extd = $extd."\r\n";
                            $extf = $extf."\r\n";
                        }
                        if($fecd==" " || $fecd==' ' || $fecd=="" || $fecd=='')
                        {
                            $fecd = trim($fecd);
                            $fecf = trim($fecf);
                        }
                        else
                        {
                            $fecd = $fecd."\r\n";
                            $fecf = $fecf."\r\n";
                        }
                        if($gend==" " || $gend==' ' || $gend=="" || $gend=='')
                        {
                            $gend = trim($gend);
                            $genf = trim($genf);
                        }
                        else
                        {
                            $gend = $gend."\r\n";
                            $genf = $genf."\r\n";
                        }
                        if($gened==" " || $gened==' ' || $gened=="" || $gened=='')
                        {
                            $gened = trim($gened);
                            $genef = trim($genef);
                        }
                        else
                        {
                            $gened = $gened."\r\n";
                            $genef = $genef."\r\n";
                        }
                        if($genhd==" " || $genhd==' ' || $genhd=="" || $genhd=='')
                        {
                            $genhd = trim($genhd);
                            $genhf = trim($genhf);
                        }
                        else
                        {
                            $genhd = $genhd."\r\n";
                            $genhf = $genhf."\r\n";
                        }
                        if($memod==" " || $memod==' ' || $memod=="" || $memod=='')
                        {
                            $memod = trim($memod);
                            $memof = trim($memof);
                        }
                        else
                        {
                            $memod = $memod."\r\n";
                            $memof = $memof."\r\n";
                        }
                        if($mobd==" " || $mobd==' ' || $mobd=="" || $mobd=='')
                        {
                            $mobd = trim($mobd);
                            $mobf = trim($mobf);
                        }
                        else
                        {
                            $mobd = $mobd."\r\n";
                            $mobf = $mobf."\r\n";
                        }
                        if($orid==" " || $orid==' ' || $orid=="" || $orid=='')
                        {
                            $orid = trim($orid);
                            $orif = trim($orif);
                        }
                        else
                        {
                            $orid = $orid."\r\n";
                            $orif = $orif."\r\n";
                        }
                        if($vitd==" " || $vitd==' ' || $vitd=="" || $vitd=='')
                        {
                            $vitd = trim($vitd);
                            $vitf = trim($vitf);
                        }
                        else
                        {
                            $vitd = $vitd."\r\n";
                            $vitf = $vitf."\r\n";
                        }
                        if($resd==" " || $resd==' ' || $resd=="" || $resd=='')
                        {
                            $resd = trim($resd);
                            $resf = trim($resf);
                        }
                        else
                        {
                            $resd = $resd."\r\n";
                            $resf = $resf."\r\n";
                        }
                        if($posd==" " || $posd==' ' || $posd=="" || $posd=='')
                        {
                            $posd = trim($posd);
                            $posf = trim($posf);
                        }
                        else
                        {
                            $posd = $posd."\r\n";
                            $posf = $posf."\r\n";
                        }
                        if($visd==" " || $visd==' ' || $visd=="" || $visd=='')
                        {
                            $visd = trim($visd);
                            $visf = trim($visf);
                        }
                        else
                        {
                            $visd = $visd."\r\n";
                            $visf = $visf."\r\n";
                        }
                        if($nutd==" " || $nutd==' ' || $nutd=="" || $nutd=='')
                        {
                            $nutd = trim($nutd);
                            $nutf = trim($nutf);
                        }
                        else
                        {
                            $nutd = $nutd."\r\n";
                            $nutf = $nutf."\r\n";
                        }
                        if($prod==" " || $prod==' ' || $prod=="" || $prod=='')
                        {
                            $prod = trim($prod);
                            $prof = trim($prof);
                        }
                        else
                        {
                            $prod = $prod."\r\n";
                            $prof = $prof."\r\n";
                        }
                        if($denverd==" " || $denverd==' ' || $denverd=="" || $denverd=='')
                        {
                            $denverd = trim($denverd);
                            $denverf = trim($denverf);
                        }
                        else
                        {
                            $denverd = $denverd."\r\n";
                            $denverf = $denverf."\r\n";
                        }

                        //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation

                        $fields_updated = $followd.''.$reasond.''.$reqd.''.$serd.''.$asid.''.$assd.''.$motd.''.$matd.''.''.$abdd.''.$cird.''.$comd.''.$dend.''.$extd.''.$fecd.''.$gend.''.$gened.''.$genhd.''.$memod.''.$mobd.''.$orid.''.$vitd.''.$resd.''.$posd.''.$visd.''.$nutd.''.$prod.''.$denverd;

                        $values_updated = $followf.''.$reasonf.''.$reqf.''.$serf.''.$asif.''.$assf.''.$motf.''.$matf.''.$abdf.''.$cirf.''.$comf.''.$denf.''.$extf.''.$fecf.''.$genf.''.$genef.''.$genhf.''.$memof.''.$mobf.''.$orif.''.$vitf.''.$resf.''.$posf.''.$visf.''.$nutf.''.$prof.''.$denverf;
                        //If no field is updated and all are empty then don't log the activity
                        if((trim($fields_updated)!==""))
                        {
                            $activity = new Activity;
                            $activity->emp_id = $emp_id;
                            $activity->activity = "Fields Updated";
                            $activity->activity_time = new \DateTime();
                            $activity->log_id = $log_id;
                            $activity->lead_id = $lead_s;
                            $activity->field_updated = $fields_updated;
                            $activity->value_updated = $values_updated;

                            $activity->save();
                        }




                        /*
                        Code for final updation of activity with field and values ends here -- by jatin
                        */


                        session()->put('name',$name1);
                        return redirect('/vh');



                    }
                    else
                    {
                        if($servicetype == "Physiotherapy - Home" || $servicetype == "Physiotherapy - Clinic")
                        {
                            //these values will be used later -- by jatin
                            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                            $lead_id = DB::table('assessments')->where('Leadid',$lead)->value('id');
                            //these values will be used later -- by jatin

                            $physiotheraphy=DB::table('physiotheraphies')->where('Leadid',$lead)->select('id')->get();
                            $physiotheraphy=json_decode($physiotheraphy);
                            $Physioid=$physiotheraphy[0]->id;
                            $physiotheraphy=physiotheraphy::find($Physioid);

                            $physiotheraphy->PhysiotheraphyType=$request->PhysiotheraphyType;
                            $physiotheraphy->MetalImplant=$request->MetalImplant;
                            $physiotheraphy->Hypertension=$request->Hypertension;
                            $physiotheraphy->Medications=$request->Medications;
                            $physiotheraphy->chiefcomplains=$request->chiefcomplains;
                            $physiotheraphy->ho_pc=$request->ho_pc;
                            $physiotheraphy->previousmedical=$request->previousmedical;
                            $physiotheraphy->occupationalH=$request->occupationalH;
                            $physiotheraphy->labq=$request->labq;
                            $physiotheraphy->affectedq=$request->affectedq;
                            $physiotheraphy->painq=$request->painq;
                            $physiotheraphy->envhistory=$request->envhistory;
                            $physiotheraphy->examinationq=$request->examinationq;
                            $physiotheraphy->PregnantOrBreastFeeding=$request->PregnantOrBreastFeeding;
                            $physiotheraphy->Diabetes=$request->Diabetes;
                            $physiotheraphy->ChronicInfection=$request->ChronicInfection;
                            $physiotheraphy->HeartDisease=$request->HeartDisease;
                            $physiotheraphy->Epilepsy=$request->Epilepsy;
                            $physiotheraphy->SurgeryUndergone=$request->SurgeryUndergone;
                            $physiotheraphy->AffectedArea=$request->AffectedArea;
                            $physiotheraphy->AssesmentDate=$request->AssesmentDate;
                            $physiotheraphy->PainPattern=$request->PainPattern;
                            $physiotheraphy->ExaminationReport=$request->ExaminationReport;
                            $physiotheraphy->LabOrRadiologicalReport=$request->LabOrRadiologicalReport;
                            $physiotheraphy->MedicalDisgnosis=$request->MedicalDisgnosis;
                            $physiotheraphy->PhysiotherapeuticDiagnosis=$request->PhysiotherapeuticDiagnosis;
                            $physiotheraphy->ShortTermGoal=$request->ShortTermGoal;
                            $physiotheraphy->LongTermGoal=$request->LongTermGoal;

                            /*
                            Code for capturing the physiotheraphy part of Log Edit starts here -- by jatin
                            Refer to comments for Products to understand what is happening in each field

                            */

                            //this will retrieve all the fields and values that are updated in the form of an array
                            $physiotheraphy_c = $physiotheraphy->getDirty();

                            //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                            foreach($physiotheraphy_c as $key=>$value)
                            {
                                $phyc[] = $key;

                            }

                            foreach($physiotheraphy_c as $key=>$value)
                            {
                                $phyv[] = $value;
                            }

                            //this is for converting array values into a string with new line

                            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                            if(empty($phyc))
                            {
                                $phyd = ' ';
                                $phyf = ' ';
                                $phyd = trim($phyd);
                                $phyf = trim($phyf);
                            }
                            else
                            {
                                $phyd = implode("\r\n", $phyc);
                                $phyf = implode("\r\n", $phyv);
                            }

                            /*
                            Code for capturing the physiotheraphy part of Log Edit ends here -- by jatin
                            */
                            $physiotheraphy->save();




                            $physioreport=DB::table('physioreports')->where('Physioid',$Physioid)->select('id')->get();
                            $physioreport=json_decode($physioreport);
                            $physioreport=$physioreport[0]->id;
                            $physioreport=physioreport::find($physioreport);
                            $physioreport->ProblemIdentified=$request->ProblemIdentified;
                            $physioreport->Treatment=$request->Treatment;
                            $physioreport->p1=$request->p1;
                            $physioreport->t1=$request->t1;
                            $physioreport->p2=$request->p2;
                            $physioreport->t2=$request->t2;
                            $physioreport->p3=$request->p3;
                            $physioreport->t3=$request->t3;
                            $physioreport->p4=$request->p4;
                            $physioreport->t4=$request->t4;
                            $physioreport->p5=$request->p5;
                            $physioreport->t5=$request->t5;
                            $physioreport->p6=$request->p6;
                            $physioreport->t6=$request->t6;
                            $physioreport->p7=$request->p7;
                            $physioreport->t7=$request->t7;
                            $physioreport->p8=$request->p8;
                            $physioreport->t8=$request->t8;
                            $physioreport->p9=$request->p9;
                            $physioreport->t9=$request->t9;

                            /*
                            Code for capturing the physioreport part of Log Edit starts here -- by jatin
                            Refer to comments for Products to understand what is happening in each field

                            */

                            //this will retrieve all the fields and values that are updated in the form of an array
                            $physioreport_c = $physioreport->getDirty();

                            //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
                            foreach($physioreport_c as $key=>$value)
                            {
                                $phoc[] = $key;

                            }

                            foreach($physioreport_c as $key=>$value)
                            {
                                $phov[] = $value;
                            }

                            //this is for converting array values into a string with new line

                            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
                            if(empty($phoc))
                            {
                                $phod = ' ';
                                $phof = ' ';
                                $phod = trim($phod);
                                $phof = trim($phof);
                            }
                            else
                            {
                                $phod = implode("\r\n", $phoc);
                                $phof = implode("\r\n", $phov);
                            }

                            /*
                            Code for capturing the physioreport part of Log Edit ends here -- by jatin
                            */

                            $physioreport->save();




                            $e=DB::table('employees')->where('FirstName',$request->assigned)->value('id');

                            $w=DB::table('verticalcoordinations')->where('leadid',$lead)->value('empid');

                            /*
                            Code for capturing assigned to status starts here -- by jatin
                            */

                            $lead_s = DB::table('leads')->where('id',$id)->value('id');
                            // $current_assignedTo = DB::table('employees')->where('FirstName',$request->assigned)->value('FirstName');

                            //if a new request is assigned and it is not null or space,
                            // add the assigned to field and value to db as activity along with others
                            if($request->assigned)
                            {
                                $asid = 'AssignedTo';
                                $asif = $request->assigned;
                            }
                            else
                            {

                                $asid = ' ';
                                $asif = ' ';
                            }

                            /*
                            Code for capturing assigned to ends here -- by jatin
                            */

                            if($w == NULL)
                            {



                                $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$lead)->select('id')->get();
                                $verticalcoordination=json_decode($verticalcoordination);
                                $verticalcoordination=$verticalcoordination[0]->id;

                                $verticalcoordination=verticalcoordination::find($verticalcoordination);
                                $verticalcoordination->empid=$e;
                                $verticalcoordination->ename=$request->assigned;

                                $verticalcoordination->save();


                                /*
                                Mail when assigned to coordinator
                                */
                                $n=$request->assigned;
                                $name=DB::table('leads')->where('id',$lead)->value('fName');
                                $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');
                                $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                                $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');

                                //service type
                                $service_type = DB::table('services')->where('leadid',$lead)->value('ServiceType');

                                $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=NULL && $active_state==0)
                                {
                                    Mail::send('mail', $data, function($message)use($data) {
                                        $message->to($data['to'], $data['user'] )->subject
                                        ('Lead ['.$data['leadid'].'] Assigned!!!');
                                        $message->from($data['from'],$data['who']);
                                    });

                                }
                                /*
                                mail end
                                */

                            }



                            /*
                            Code for Physiotheraphy editing starts here -- by jatin
                            */
                            if($followd==" " || $followd==' ' || $followd=="" || $followd=='')
                            {
                                $followd = trim($followd);
                                $followf = trim($followf);
                            }
                            else
                            {
                                $followd = $followd."\r\n";
                                $followf = $followf."\r\n";
                            }
                            if($reasond==" " || $reasond==' ' || $reasond=="" || $reasond=='')
                            {
                                $reasond = trim($reasond);
                                $reasonf = trim($reasonf);
                            }
                            else
                            {
                                $reasond = $reasond."\r\n";
                                $reasonf = $reasonf."\r\n";
                            }
                            if($reqd==" " || $reqd==' ' || $reqd=="" || $reqd=='')
                            {
                                $reqd = trim($reqd);
                                $reqf = trim($reqf);
                            }
                            else
                            {
                                $reqd = $reqd."\r\n";
                                $reqf = $reqf."\r\n";
                            }
                            if($serd==" " || $serd==' ' || $serd=="" || $serd=='')
                            {
                                $serd = trim($serd);
                                $serf = trim($serf);
                            }
                            else
                            {
                                $serd = $serd."\r\n";
                                $serf = $serf."\r\n";
                            }
                            if($asid==" " || $asid==' ' || $asid=="" || $asid=='')
                            {
                                $asid = trim($asid);
                                $asif = trim($asif);
                            }
                            else
                            {
                                $asid = $asid."\r\n";
                                $asif = $asif."\r\n";
                            }
                            if($assd==" " || $assd==' ' || $assd=="" || $assd=='')
                            {
                                $assd = trim($assd);
                                $assf = trim($assf);
                            }
                            else
                            {
                                $assd = $assd."\r\n";
                                $assf = $assf."\r\n";
                            }

                            if($abdd==" " || $abdd==' ' || $abdd=="" || $abdd=='')
                            {
                                $abdd = trim($abdd);
                                $abdf = trim($abdf);
                            }
                            else
                            {
                                $abdd = $abdd."\r\n";
                                $abdf = $abdf."\r\n";
                            }
                            if($cird==" " || $cird==' ' || $cird=="" || $cird=='')
                            {
                                $cird = trim($cird);
                                $cirf = trim($cirf);
                            }
                            else
                            {
                                $cird = $cird."\r\n";
                                $cirf = $cirf."\r\n";
                            }
                            if($comd==" " || $comd==' ' || $comd=="" || $comd=='')
                            {
                                $comd = trim($comd);
                                $comf = trim($comf);
                            }
                            else
                            {
                                $comd = $comd."\r\n";
                                $comf = $comf."\r\n";
                            }
                            if($dend==" " || $dend==' ' || $dend=="" || $dend=='')
                            {
                                $dend = trim($dend);
                                $denf = trim($denf);
                            }
                            else
                            {
                                $dend = $dend."\r\n";
                                $denf = $denf."\r\n";
                            }
                            if($extd==" " || $extd==' ' || $extd=="" || $extd=='')
                            {
                                $extd = trim($extd);
                                $extf = trim($extf);
                            }
                            else
                            {
                                $extd = $extd."\r\n";
                                $extf = $extf."\r\n";
                            }
                            if($fecd==" " || $fecd==' ' || $fecd=="" || $fecd=='')
                            {
                                $fecd = trim($fecd);
                                $fecf = trim($fecf);
                            }
                            else
                            {
                                $fecd = $fecd."\r\n";
                                $fecf = $fecf."\r\n";
                            }
                            if($gend==" " || $gend==' ' || $gend=="" || $gend=='')
                            {
                                $gend = trim($gend);
                                $genf = trim($genf);
                            }
                            else
                            {
                                $gend = $gend."\r\n";
                                $genf = $genf."\r\n";
                            }
                            if($gened==" " || $gened==' ' || $gened=="" || $gened=='')
                            {
                                $gened = trim($gened);
                                $genef = trim($genef);
                            }
                            else
                            {
                                $gened = $gened."\r\n";
                                $genef = $genef."\r\n";
                            }
                            if($genhd==" " || $genhd==' ' || $genhd=="" || $genhd=='')
                            {
                                $genhd = trim($genhd);
                                $genhf = trim($genhf);
                            }
                            else
                            {
                                $genhd = $genhd."\r\n";
                                $genhf = $genhf."\r\n";
                            }
                            if($memod==" " || $memod==' ' || $memod=="" || $memod=='')
                            {
                                $memod = trim($memod);
                                $memof = trim($memof);
                            }
                            else
                            {
                                $memod = $memod."\r\n";
                                $memof = $memof."\r\n";
                            }
                            if($mobd==" " || $mobd==' ' || $mobd=="" || $mobd=='')
                            {
                                $mobd = trim($mobd);
                                $mobf = trim($mobf);
                            }
                            else
                            {
                                $mobd = $mobd."\r\n";
                                $mobf = $mobf."\r\n";
                            }
                            if($orid==" " || $orid==' ' || $orid=="" || $orid=='')
                            {
                                $orid = trim($orid);
                                $orif = trim($orif);
                            }
                            else
                            {
                                $orid = $orid."\r\n";
                                $orif = $orif."\r\n";
                            }
                            if($vitd==" " || $vitd==' ' || $vitd=="" || $vitd=='')
                            {
                                $vitd = trim($vitd);
                                $vitf = trim($vitf);
                            }
                            else
                            {
                                $vitd = $vitd."\r\n";
                                $vitf = $vitf."\r\n";
                            }
                            if($resd==" " || $resd==' ' || $resd=="" || $resd=='')
                            {
                                $resd = trim($resd);
                                $resf = trim($resf);
                            }
                            else
                            {
                                $resd = $resd."\r\n";
                                $resf = $resf."\r\n";
                            }
                            if($posd==" " || $posd==' ' || $posd=="" || $posd=='')
                            {
                                $posd = trim($posd);
                                $posf = trim($posf);
                            }
                            else
                            {
                                $posd = $posd."\r\n";
                                $posf = $posf."\r\n";
                            }
                            if($visd==" " || $visd==' ' || $visd=="" || $visd=='')
                            {
                                $visd = trim($visd);
                                $visf = trim($visf);
                            }
                            else
                            {
                                $visd = $visd."\r\n";
                                $visf = $visf."\r\n";
                            }
                            if($nutd==" " || $nutd==' ' || $nutd=="" || $nutd=='')
                            {
                                $nutd = trim($nutd);
                                $nutf = trim($nutf);
                            }
                            else
                            {
                                $nutd = $nutd."\r\n";
                                $nutf = $nutf."\r\n";
                            }
                            if($prod==" " || $prod==' ' || $prod=="" || $prod=='')
                            {
                                $prod = trim($prod);
                                $prof = trim($prof);
                            }
                            else
                            {
                                $prod = $prod."\r\n";
                                $prof = $prof."\r\n";
                            }

                            if($phyd==" " || $phyd==' ' || $phyd=="" || $phyd=='')
                            {
                                $phyd = trim($phyd);
                                $phyf = trim($phyf);
                            }
                            else
                            {
                                $phyd = $phyd."\r\n";
                                $phyf = $phyf."\r\n";
                            }
                            if($phod==" " || $phod==' ' || $phod=="" || $phod=='')
                            {
                                $phod = trim($phod);
                                $phof = trim($phof);
                            }
                            else
                            {
                                $phod = $phod."\r\n";
                                $phof = $phof."\r\n";
                            }


                            //final updationg of fields in db

                            $fields_updated = $followd.''.$reasond.''.$reqd.''.$serd.''.$asid.''.$assd.''.$phyd.''.$phod.''.$abdd.''.$cird.''.$comd.''.$dend.
                            ''.$extd.''.$fecd.''.$gend.''.$gened.''.$genhd.''.$memod.'' .$mobd.''.$orid.''.$vitd.''.$resd.''.$posd.''.$visd.''.$nutd.''.$prod;

                            $values_updated = $followf.''.$reasonf.''.$reqf.''.$serf.''.$asif.''.$assf.''.$phyf.''.$phof.''.$abdf.''.$cirf.''.
                            $comf.''.$denf.''.$extf.''.$fecf.''.$genf.''.$genef.''.$genhf.''.$memof.''.$mobf.''.$orif.''.$vitf.''.$resf.''.$posf.''.$visf.''.$nutf.''.$prof;

                            if((trim($fields_updated)!==""))
                            {
                                $activity = new Activity;
                                $activity->emp_id = $emp_id;
                                $activity->activity = "Fields Updated";
                                $activity->activity_time = new \DateTime();
                                $activity->log_id = $log_id;
                                $activity->lead_id = $lead_s;
                                $activity->field_updated = $fields_updated;
                                $activity->value_updated = $values_updated;

                                $activity->save();
                            }



                            /*
                            Code for Physiotheraphy editing ends here -- by jatin
                            */




                            /*
                            Mail when assessment is complete to coordinator
                            */
                            $n=$request->assigned;
                            $name=DB::table('leads')->where('id',$lead)->value('fName');
                            $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');
                            $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                            $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                            $who = $name1;
                            $from = DB::table('admins')->where('name',$name1)->value('email');
                            $to = DB::table('admins')->where('name',$user)->value('email');
                            $to1 = DB::table('leads')->where('id',$lead)->value('EmailId');
                            $service_type=DB::table('services')->where('leadid',$lead)->value('ServiceType');

                            $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type',$service_type];

                            $data1 = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];

                            if($request->ServiceStatus!="In Progress")
                            {
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=NULL && $active_state==0)
                                {
                                    Mail::send('mail2', $data, function($message)use($data) {
                                        $message->to($data['to'],$data['user']  )->subject
                                        ('Lead ['.$data['leadid'].']  Assessment Completed!!!');
                                        $message->from($data['from'],$data['who']);
                                    });
                                }
                                if($to1 !=NULL && $active_state==0)
                                {

                                    Mail::send('mail3', $data1, function($message)use($data1) {
                                        $message->to($data1['to'], $data1['user'] )->subject
                                        ('Lead ['.$data1['leadid'].']  Assessment Completed!!!');
                                        $message->from('care@healthheal.in',$data1['name']);
                                    });

                                }
                                /*
                                mail end
                                */

                                /*
                                SMS to Customer when Assessment done by Vertical Lead For Physiotheraphy Form starts here -- by jatin
                                */

                                //vertical contact number
                                $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                                //vertical alternate contact number
                                $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');

                                //customer name
                                $name=DB::table('leads')->where('id',$lead)->value('fName');

                                //customer mobile number
                                $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');

                                //customer alternate mobile number
                                $altcontact=DB::table('leads')->where('id',$lead)->value('Alternatenumber');

                                //location selected during lead creation
                                $location=DB::table('services')->where('leadid',$lead)->value('Branch');

                                if($current_status!=$request->ServiceStatus)
                                {
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($contact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$contact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$contact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    //Message going to alternate number of customer

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($altcontact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$altcontact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$altcontact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    /*
                                    SMS to Customer Assessment done by Vertical Head For Physiotheraphy Form ends here -- by jatin
                                    */

                                    /*
                                    SMS to Vertical Head when Assessment done by Vertical Lead For Physiotheraphy Form starts here -- by jatin
                                    */
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($vmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                        Thank You.";
                                        $phonenimber=$vmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                                Thank You.";
                                                $phonenimber=$vmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }

                                    }

                                    //Message going to alternate number of vertical

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($altvmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                        Thank You.";
                                        $phonenimber=$altvmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                                Thank You.";
                                                $phonenimber=$altvmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }

                                    }
                                }
                            }

                            /*
                            SMS when Assessment done by Vertical Head For Physiotheraphy Form ends here -- by jatin
                            */


                            session()->put('name',$name1);
                            return redirect('/vh');


                        }
                        else
                        {


                            // Assessment update code for psc and ns


                            //these values will be used later -- by jatin
                            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
                            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

                            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
                            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

                            $lead_id = DB::table('assessments')->where('Leadid',$lead)->value('id');
                            //these values will be used later -- by jatin

                            // dd($request->notsignificantforabdomen);

                            /*
                            Code for capturing assigned to status starts here -- by jatin
                            */

                            $lead_s = DB::table('leads')->where('id',$id)->value('id');
                            // $current_assignedTo = DB::table('employees')->where('FirstName',$request->assigned)->value('FirstName');

                            //if a new request is assigned and it is not null or space,
                            // add the assigned to field and value to db as activity along with others
                            if($request->assigned)
                            {
                                $asid = 'AssignedTo';
                                $asif = $request->assigned;
                            }
                            else
                            {

                                $asid = ' ';
                                $asif = ' ';
                            }

                            /*
                            Code for capturing assigned to ends here -- by jatin
                            */


                            if($w == NULL)
                            {

                                $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$lead)->select('id')->get();
                                $verticalcoordination=json_decode($verticalcoordination);
                                $verticalcoordination=$verticalcoordination[0]->id;

                                $verticalcoordination=verticalcoordination::find($verticalcoordination);
                                $verticalcoordination->empid=$e;
                                $verticalcoordination->ename=$request->assigned;

                                $verticalcoordination->save();


                                /*
                                Mail when assigned to coordinator
                                */
                                $n=$request->assigned;
                                $name=DB::table('leads')->where('id',$lead)->value('fName');
                                $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');
                                $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                                $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                                $who = $name1;
                                $from = DB::table('admins')->where('name',$name1)->value('email');
                                $to = DB::table('admins')->where('name',$n)->value('email');
                                $service_type=DB::table('services')->where('leadid',$lead)->value('ServiceType');

                                $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type'=>$service_type];

                                if($to!=NULL)
                                {


                                }

                                /*
                                mail end
                                */



                            }

                            /*
                            Mail when assessment is complete to coordinator
                            */
                            $n=$request->assigned;
                            $name=DB::table('leads')->where('id',$lead)->value('fName');
                            $user=DB::table('services')->where('leadid',$lead)->value('AssignedTo');
                            $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');
                            $location=DB::table('services')->where('leadid',$lead)->value('Branch');
                            $who = $name1;
                            $from = DB::table('admins')->where('name',$name1)->value('email');
                            $to = DB::table('admins')->where('name',$user)->value('email');
                            $to1 = DB::table('leads')->where('id',$lead)->value('EmailId');
                            $service_type=DB::table('services')->where('leadid',$lead)->value('ServiceType');


                            $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'service_type',$service_type];

                            $data1 = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who,'service_type'=>$service_type];





                            $data = ['leadid'=>$lead,'name'=>$name,'user'=>$user,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];


                            /*
                            SMS to Customer when Assessment done by Vertical Lead For Personal Supportive Care Form starts here -- by jatin
                            */

                            //vertical contact number
                            $vmob = DB::table('employees')->where('FirstName',$name1)->value('MobileNumber');

                            //vertical alternate contact number
                            $altvmob = DB::table('employees')->where('FirstName',$name1)->value('AlternateNumber');

                            //customer name
                            $name=DB::table('leads')->where('id',$lead)->value('fName');

                            //customer mobile number
                            $contact=DB::table('leads')->where('id',$lead)->value('MobileNumber');

                            //customer alternate mobile number
                            $altcontact=DB::table('leads')->where('id',$lead)->value('Alternatenumber');

                            //location selected during lead creation
                            $location=DB::table('services')->where('leadid',$lead)->value('Branch');


                            if($request->ServiceStatus!="In Progress")
                            {
                                if($current_status!=$request->ServiceStatus)
                                {
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($contact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$contact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$contact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    //Message going to alternate number of customer
                                    if($altcontact!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                        - Health Heal";
                                        $phonenimber=$altcontact;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name.", Thank you for your valuable time for the assessment for ref ID ".$lead.". Looking forward to serve you better.
                                                - Health Heal";
                                                $phonenimber=$altcontact;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }

                                    /*
                                    SMS to Customer Assessment done by Vertical Head For Personal Supportive Care Form ends here -- by jatin
                                    */

                                    /*
                                    SMS to Vertical Head when Assessment done by Vertical Lead For Personal Supportive Care Form starts here -- by jatin
                                    */
                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($vmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                        Thank You.";
                                        $phonenimber=$vmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                                Thank You.";
                                                $phonenimber=$vmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }
                                    // Message going to alternate number of vh

                                    $active_state = DB::table('sms_email_filter')->value('active_state');

                                    if($altvmob!=NULL && $active_state==0)
                                    {
                                        $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                        Thank You.";
                                        $phonenimber=$altvmob;
                                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                        $ch =curl_init($url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        $curl_scraped_page =curl_exec($ch);
                                        $report = substr($curl_scraped_page,0,2);

                                        curl_close($ch);

                                        $i=0;
                                        //sending a message again if not sent first time
                                        while($report!="OK" || $i<=1)
                                        {
                                            if($report!="OK")
                                            {
                                                $message="Dear ".$name1.", The assessment for the ID ".$lead." is completed.
                                                Thank You.";
                                                $phonenimber=$altvmob;
                                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                                $ch =curl_init($url);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $curl_scraped_page =curl_exec($ch);
                                                $report = substr($curl_scraped_page,0,2);

                                                curl_close($ch);
                                            }
                                            $i++;
                                        }
                                    }
                                }
                                /*
                                SMS when Assessment done by Vertical Head For Personal Supportive Care Form ends here -- by jatin
                                */
                                $active_state = DB::table('sms_email_filter')->value('active_state');

                                if($to!=NULL && $active_state==0)
                                {
                                    Mail::send('mail2', $data, function($message)use($data) {
                                        $message->to($data['to'],$data['user']  )->subject
                                        ('Lead ['.$data['leadid'].']  Assessment Completed!!!');
                                        $message->from($data['from'],$data['who']);
                                    });
                                }
                                if($to1 !=NULL && $active_state==0)
                                {

                                    Mail::send('mail3', $data1, function($message)use($data1) {
                                        $message->to($data1['to'], $data1['user'] )->subject
                                        ('Lead ['.$data1['leadid'].']  Assessment Completed!!!');
                                        $message->from('care@healthheal.in','Health Heal');
                                    });

                                }
                            }
                            /*
                            mail end
                            */

                            /*
                            Code for final updation of activity with field and values starts here -- by jatin
                            */
                            if($followd==" " || $followd==' ' || $followd=="" || $followd=='')
                            {
                                $followd = trim($followd);
                                $followf = trim($followf);
                            }
                            else
                            {
                                $followd = $followd."\r\n";
                                $followf = $followf."\r\n";
                            }
                            if($reasond==" " || $reasond==' ' || $reasond=="" || $reasond=='')
                            {
                                $reasond = trim($reasond);
                                $reasonf = trim($reasonf);
                            }
                            else
                            {
                                $reasond = $reasond."\r\n";
                                $reasonf = $reasonf."\r\n";
                            }
                            if($reqd==" " || $reqd==' ' || $reqd=="" || $reqd=='')
                            {
                                $reqd = trim($reqd);
                                $reqf = trim($reqf);
                            }
                            else
                            {
                                $reqd = $reqd."\r\n";
                                $reqf = $reqf."\r\n";
                            }
                            if($serd==" " || $serd==' ' || $serd=="" || $serd=='')
                            {
                                $serd = trim($serd);
                                $serf = trim($serf);
                            }
                            else
                            {
                                $serd = $serd."\r\n";
                                $serf = $serf."\r\n";
                            }
                            if($asid==" " || $asid==' ' || $asid=="" || $asid=='')
                            {
                                $asid = trim($asid);
                                $asif = trim($asif);
                            }
                            else
                            {
                                $asid = $asid."\r\n";
                                $asif = $asif."\r\n";
                            }
                            if($assd==" " || $assd==' ' || $assd=="" || $assd=='')
                            {
                                $assd = trim($assd);
                                $assf = trim($assf);
                            }
                            else
                            {
                                $assd = $assd."\r\n";
                                $assf = $assf."\r\n";
                            }

                            if($abdd==" " || $abdd==' ' || $abdd=="" || $abdd=='')
                            {
                                $abdd = trim($abdd);
                                $abdf = trim($abdf);
                            }
                            else
                            {
                                $abdd = $abdd."\r\n";
                                $abdf = $abdf."\r\n";
                            }
                            if($cird==" " || $cird==' ' || $cird=="" || $cird=='')
                            {
                                $cird = trim($cird);
                                $cirf = trim($cirf);
                            }
                            else
                            {
                                $cird = $cird."\r\n";
                                $cirf = $cirf."\r\n";
                            }
                            if($comd==" " || $comd==' ' || $comd=="" || $comd=='')
                            {
                                $comd = trim($comd);
                                $comf = trim($comf);
                            }
                            else
                            {
                                $comd = $comd."\r\n";
                                $comf = $comf."\r\n";
                            }
                            if($dend==" " || $dend==' ' || $dend=="" || $dend=='')
                            {
                                $dend = trim($dend);
                                $denf = trim($denf);
                            }
                            else
                            {
                                $dend = $dend."\r\n";
                                $denf = $denf."\r\n";
                            }
                            if($extd==" " || $extd==' ' || $extd=="" || $extd=='')
                            {
                                $extd = trim($extd);
                                $extf = trim($extf);
                            }
                            else
                            {
                                $extd = $extd."\r\n";
                                $extf = $extf."\r\n";
                            }
                            if($fecd==" " || $fecd==' ' || $fecd=="" || $fecd=='')
                            {
                                $fecd = trim($fecd);
                                $fecf = trim($fecf);
                            }
                            else
                            {
                                $fecd = $fecd."\r\n";
                                $fecf = $fecf."\r\n";
                            }
                            if($gend==" " || $gend==' ' || $gend=="" || $gend=='')
                            {
                                $gend = trim($gend);
                                $genf = trim($genf);
                            }
                            else
                            {
                                $gend = $gend."\r\n";
                                $genf = $genf."\r\n";
                            }
                            if($gened==" " || $gened==' ' || $gened=="" || $gened=='')
                            {
                                $gened = trim($gened);
                                $genef = trim($genef);
                            }
                            else
                            {
                                $gened = $gened."\r\n";
                                $genef = $genef."\r\n";
                            }
                            if($genhd==" " || $genhd==' ' || $genhd=="" || $genhd=='')
                            {
                                $genhd = trim($genhd);
                                $genhf = trim($genhf);
                            }
                            else
                            {
                                $genhd = $genhd."\r\n";
                                $genhf = $genhf."\r\n";
                            }
                            if($memod==" " || $memod==' ' || $memod=="" || $memod=='')
                            {
                                $memod = trim($memod);
                                $memof = trim($memof);
                            }
                            else
                            {
                                $memod = $memod."\r\n";
                                $memof = $memof."\r\n";
                            }
                            if($mobd==" " || $mobd==' ' || $mobd=="" || $mobd=='')
                            {
                                $mobd = trim($mobd);
                                $mobf = trim($mobf);
                            }
                            else
                            {
                                $mobd = $mobd."\r\n";
                                $mobf = $mobf."\r\n";
                            }
                            if($orid==" " || $orid==' ' || $orid=="" || $orid=='')
                            {
                                $orid = trim($orid);
                                $orif = trim($orif);
                            }
                            else
                            {
                                $orid = $orid."\r\n";
                                $orif = $orif."\r\n";
                            }
                            if($vitd==" " || $vitd==' ' || $vitd=="" || $vitd=='')
                            {
                                $vitd = trim($vitd);
                                $vitf = trim($vitf);
                            }
                            else
                            {
                                $vitd = $vitd."\r\n";
                                $vitf = $vitf."\r\n";
                            }
                            if($resd==" " || $resd==' ' || $resd=="" || $resd=='')
                            {
                                $resd = trim($resd);
                                $resf = trim($resf);
                            }
                            else
                            {
                                $resd = $resd."\r\n";
                                $resf = $resf."\r\n";
                            }
                            if($posd==" " || $posd==' ' || $posd=="" || $posd=='')
                            {
                                $posd = trim($posd);
                                $posf = trim($posf);
                            }
                            else
                            {
                                $posd = $posd."\r\n";
                                $posf = $posf."\r\n";
                            }
                            if($visd==" " || $visd==' ' || $visd=="" || $visd=='')
                            {
                                $visd = trim($visd);
                                $visf = trim($visf);
                            }
                            else
                            {
                                $visd = $visd."\r\n";
                                $visf = $visf."\r\n";
                            }
                            if($nutd==" " || $nutd==' ' || $nutd=="" || $nutd=='')
                            {
                                $nutd = trim($nutd);
                                $nutf = trim($nutf);
                            }
                            else
                            {
                                $nutd = $nutd."\r\n";
                                $nutf = $nutf."\r\n";
                            }
                            if($prod==" " || $prod==' ' || $prod=="" || $prod=='')
                            {
                                $prod = trim($prod);
                                $prof = trim($prof);
                            }
                            else
                            {
                                $prod = $prod."\r\n";
                                $prof = $prof."\r\n";
                            }
                            //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
                            $fields_updated = $followd.''.$reasond.''.$reqd.''.$serd.''.$asid.''.$abdd.''.$cird.''.$comd.''.$dend.''.$extd.''.$fecd.''.$gend.''. $gened.''.$genhd.''.$memod.''.$mobd.
                            ''.$orid.''.$vitd.''.$resd.''.$posd.''.$visd.''.$nutd.''.$prod;

                            $values_updated = $followf.''.$reasonf.''.$reqf.''.$serf.''.$asif.''.$abdf.''.$cirf.''.$comf.''.$denf.''.$extf.''.$fecf.''.$genf.''.$genef.''.$genhf.''.$memof.''.$mobf.
                            ''.$orif.''.$vitf.''.$resf.''.$posf.''.$visf.''.$nutf.''.$prof;



                            //If no field is updated and all are empty then don't log the activity
                            if((trim($fields_updated)!==""))
                            {
                                $activity = new Activity;
                                $activity->emp_id = $emp_id;
                                $activity->activity = "Fields Updated";
                                $activity->activity_time = new \DateTime();
                                $activity->log_id = $log_id;
                                $activity->lead_id = $lead_s;
                                $activity->field_updated = $fields_updated;
                                $activity->value_updated = $values_updated;

                                $activity->save();
                            }



                            // $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$lead)->select('id')->get();
                            // $verticalcoordination=json_decode($verticalcoordination);
                            //        $verticalcoordination=$verticalcoordination[0]->id;
                            //        $verticalcoordination=verticalcoordination::find($verticalcoordination);
                            // $verticalcoordination->empid=$e;

                            // $verticalcoordination->save();

                            session()->put('name',$name1);
                            return redirect('/vh');
                        }
                    }


                }

                /**
                * Remove the specified resource from storage.
                *
                * @param  int  $id
                * @return \Illuminate\Http\Response
                */
                public function destroy($id)
                {
                    //
                }



                public function newdoctor(Request $request)
                {


                    $count = $request->noofnewdoc;

                    return view('admin.newdocadd',compact('count'));
                }
                public function addmedicine(Request $request)
                {

                    $docnum = $request->classidtobeplaced;
                    $noofclicks = $request->currentvalue;



                    return view('admin.addmedicine',compact('docnum','noofclicks'));
                }


                public function nebulizationaddmedicine(Request $request)
                {

                    $docnum = $request->classidtobeplaced;
                    $noofclicks = $request->currentvalue;



                    return view('admin.nebulizationaddmedicines',compact('docnum','noofclicks'));
                }


                public function nebulizationnewdoctor(Request $request)
                {


                    $count = $request->noofnebulizationnewdoc;

                    return view('admin.nebulizationnewadddoc',compact('count'));
                }


                public function history()
                {
                    $leadid = $_GET['id'];
                    //dd($leadid);
                    $logged_in_user = Auth::guard('admin')->user()->name;
                    //dd($leadid,$logged_in_user);
                    $designation = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
                    $id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
                    //dd($id);

                    $servicetypecheck=DB::table('services')->where('Leadid',$leadid)->value('ServiceType');


                    $log = DB::table('logs')
                    ->select('logs.*','activities.*')
                    ->join('activities', 'logs.id', '=', 'activities.log_id')
                    ->where('activities.lead_Id',$leadid)
                    ->orderBy('activities.id', 'DESC')
                    ->paginate(50);


                    $name=$logged_in_user;


                    $adminid = DB::table('admins')->where('name',$name)->value('id');

                    $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
                    $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




                    $nid=DB::table('employees')->where('FirstName',$name)->select('Designation')->get();
                    $nid=json_decode($nid);
                    $nid= $nid[0]->Designation;

                    $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
                    $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
                    $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');

                    $department2=DB::table('employees')->where('FirstName',$name)->value('Department2');

                    $user_department=DB::table('employees')->where('FirstName',$name)->value('Department');

                    $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
                    $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');

                    if($nid=="Vertical Head" && $user_department==$servicetypecheck)
                    {



                        if($roles=="Vertical_ProductSelling" && $user_department==$servicetypecheck)
                        {
                            $designation=$roles;

                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));





                        }
                        else
                        {
                            if($roles=="Vertical_ProductRental" && $user_department==$servicetypecheck)
                            {
                                $designation=$roles;

                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                            }
                            else
                            {
                                if($roles=="Vertical_ProductManager" && $user_department==$servicetypecheck)
                                {

                                    $designation=$roles;

                                    return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                }
                                else
                                {
                                    if($roles=="Vertical_PharmacyManager" && $user_department==$servicetypecheck)
                                    {

                                        $designation=$roles;

                                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$servicetypecheck)
                                        {

                                            $designation=$roles;
                                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                        }
                                        else
                                        {
                                            if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$servicetypecheck)
                                            {

                                                $designation=$roles;
                                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$servicetypecheck)
                                                {
                                                    $designation=$roles;

                                                    return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$servicetypecheck)
                                                    {

                                                        $designation=$roles;

                                                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                                                    }
                                                    else
                                                    {
                                                        if($roles=="Vertical_FieldOfficer" && $user_department==$servicetypecheck)
                                                        {
                                                            $designation=$roles;

                                                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                                                        }
                                                        else
                                                        {
                                                            if($nid=="Vertical Head" && $user_department==$servicetypecheck)
                                                            {

                                                                $designation="vertical";

                                                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                                            }
                                                            else
                                                            {
                                                                return redirect('/admin');
                                                            }

                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                    else
                    if($nid=="Management")
                    {
                        $designation="management";
                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                    }
                    else
                    if($nid=="Admin")
                    {
                        $designation="home";

                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                    }
                    else
                    {
                        if($nid=="Branch Head" && $user_city==$service_city)
                        {
                            $designation="BranchHead";

                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                        }
                        else
                        {


                            if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                            {

                                $designation=$roles;
                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                            }
                            else
                            {
                                if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                                {

                                    $designation=$roles;
                                    return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                                    {

                                        $designation=$roles;
                                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                                        {

                                            $designation=$roles;
                                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                        }
                                        else
                                        {
                                            if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                            {

                                                $designation=$roles;
                                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                                {

                                                    $designation=$roles;
                                                    return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                                    }
                                                    else
                                                    {
                                                        if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation=$roles;
                                                            return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                                                        }
                                                        else
                                                        {
                                                            if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                            {
                                                                $designation=$roles;
                                                                return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));

                                                            }
                                                            else
                                                            {
                                                                if($nid=="Coordinator" && $check_coordinator==$coordinator_id)
                                                                {
                                                                    $designation="coordinator";
                                                                    return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                                                                }
                                                                else
                                                                {
                                                                    return redirect('/admin');
                                                                }

                                                            }
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }




                    // return view('admin.history',compact('log','designation','leadid','logged_in_user','id'));
                }

            }
