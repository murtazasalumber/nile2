<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Response;


class Vertical_FieldOfficerController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('vertical_fieldofficer');
    }

    public function index()
    {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $name = Auth::guard('admin')->user()->name;

        $designation="vertical";

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //grabbing the city for which we want the counts
        //this city will be specific to the logged in user
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // dd($serviceType);

        // checking if the service type is "Physiotherapy" and calculating counts according to that

        // if($user_city=="Hyderabad" || $user_city=="Chennai" || $user_city=="Pune" || $user_city=="Hubballi-Dharwad")
        // {
        //     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //     $newcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status0)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $inprogresscount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status1)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();


        //     $convertedcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status2)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $droppedcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status3)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $deferredcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status4)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();
        // }

        if($serviceType=="Physiotherapy - Home")
        {

            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            // Depending on the branch , the ServiceStatus from above, the subtypes of Physiotherapy , we are fetching the count
            // this count consists of all counts (for vertical, all coordinators under him/her)
            $newcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "Nursing Services" and calculating counts according to that
        else if($serviceType=="Nursing Services")
        {

            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $newcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "PSC" and calculating counts according to that
        else if($serviceType=="Personal Supportive Care")
        {

            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $newcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        // checking if the service type is "Mathrutvam" and calculating counts according to that
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];


            $newcount=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $inprogresscount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        /*Code for Graphs and Pie Chart of Vertical Dashboard starts here */

        //Find the id of the vertical that is currently logged in
        $logged_in_user_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
        // dd($logged_in_user_id);

        $coords_under_vert = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->get();
        $coords_under_vert = json_decode($coords_under_vert,true);

        $statuscounts[-1][-1] = null;

        $coords_under_vert_count = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->count();

        // dd($coords_under_vert_count);

        // dd($coords_under_vert[0]['FirstName']);
        $User=null;

        for($i=0;$i<$coords_under_vert_count;$i++)
        {
            $User[] =  $coords_under_vert[$i]['FirstName'];

        }
        // dd($User[1]);

        if($User!=NULL)
        {
            for($j=0;$j<$coords_under_vert_count;$j++)
            {
                /* Retrieving the counts of all the users -- started */
                //assigning the possible statuses for the Coordinator
                $status1 = "In Progress";
                $status2  = "Converted";
                $status3  = "Dropped";
                $status4  = "Deferred";

                $logged_in_user = $User[$j];

                $assignedcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $convertedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status2)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status3)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $deferredcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status4)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $statuscounts[] = array
                (
                    "0" => $assignedcount2, "1" =>$convertedcount2,"2"=>$droppedcount2,"3"=>$deferredcount2
                );

                // $statuscounts[$j][0] = $assignedcount2;
                // $statuscounts[$j][1] = $convertedcount2;
                // $statuscounts[$j][2] = $droppedcount2;
                // $statuscounts[$j][3] = $deferredcount2;

                $coord_id1 = DB::table('employees')->where('FirstName',$User[$j])->value('id');
                $totalcount = DB::table('verticalcoordinations')->where('empid',$coord_id1)->count();


                // $percentage[$j][0] = ($statuscounts[$j][1]/$totalcount)*100;

                /* Retrieving the counts of all the users -- end */
            }

            // dd($statuscounts);


            // $colors1[-1] = null;
            //
            // // $colors1[0] = #4DFF4D;
            // // $colors1[1] = #ff0000;
            // // $colors1[2] = #0066ff;
            // // $colors1[3] = #b87333;
        }
        // dd($colors1[0]);
        $colors1 = array('#b87333','silver','gold','#e5e4e2');

        /*Code for Graphs and Pie Chart of Vertical Dashboard ends here */

        // dd($deferredcount2);

        session()->put('name',$name);

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        // dd($logged_in_user);


        $productstatus1 = "Processing";
        $productstatus2  = "Awaiting Pickup";
        $productstatus3  = "Out for Delivery";
        $productstatus4  = "Ready to ship";
        $productstatus5  = "Order Return";
        $productstatus6  = "Cancelled";
        $productstatus7  = "Delivered";

        // $city1 = DB::

        $processingcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus1)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus1)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus1)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount = $processingcount1 + $processingcount2 + $processingcount3;




        $awaitingpickupcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus2)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus2)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus2)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount = $awaitingpickupcount1 + $awaitingpickupcount2 + $awaitingpickupcount3;

        $outfordeliverycount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus3)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus3)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus3)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount = $outfordeliverycount1 + $outfordeliverycount2 + $outfordeliverycount3;

        $readytoshipcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus4)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus4)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus4)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount = $readytoshipcount1 + $readytoshipcount2 + $readytoshipcount3;

        $orderreturncount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus5)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus5)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus5)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount = $orderreturncount1 + $orderreturncount2 + $orderreturncount3;

        $canceledcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus6)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus6)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus6)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount = $canceledcount1 + $canceledcount2 + $canceledcount3;

        $deliveredcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$productstatus7)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$productstatus7)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$productstatus7)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount = $deliveredcount1 + $deliveredcount2 + $deliveredcount3;


        // dd($newcount,  $convertedcount, $inprogresscount,$assignedcount2, $convertedcount,$droppedcount,$deferredcount,$assignedcount2,$convertedcount2);

        return view('admin.Vertical_FieldOfficer',compact('newcount','convertedcount','inprogresscount','assignedcount2','convertedcount','droppedcount','deferredcount','assignedcount2','convertedcount2','droppedcount2','deferredcount2','User','coords_under_vert_count','colors1','emp','statuscounts','designation','user_city','serviceType','processingcount','awaitingpickupcount','outfordeliverycount'
        ,'readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation'));
    }

    public function assigned()
    {

        if (session()->has('name'))
        {

            $logged_in_user = session()->get('name');
        }
        else{
            $logged_in_user = $_GET['name'];

        }

        $logged_in_user = Auth::guard('admin')->user()->name;

        // dd($logged_in_user);
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        // session()->put('status',$status);

        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // if($user_city=="Hyderabad" || $user_city=="Chennai" || $user_city=="Pune" || $user_city=="Hubballi-Dharwad")
        // {
        //     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //     $leads=DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('services.Branch',$user_city)
        //     ->where('services.ServiceStatus',$status)
        //     ->wherein('services.ServiceType',$Servicesarray)
        //     ->orderBy('leads.id', 'DESC')
        //     ->paginate(50);

        //     if(isset($_GET['download']))
        //     {
        //         $download = $_GET['download'];

        //         if($status=="All")
        //         {
        //             $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
        //             $empname=json_decode($empname);
        //             $empname1= $empname[0]->id;
        //             $empname2= $empname[0]->Designation;
        //             $empname3= $empname[0]->city;
        //             $empname4= $empname[0]->Department;

        //             // dd($Servicesarray);
        //             $leads=DB::table('leads')
        //             ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->where('services.Branch',$user_city)
        //             ->wherein('services.ServiceType',$Servicesarray)
        //             ->orderBy('leads.id', 'DESC')
        //             ->get();

        //             // dd($leads);


        //             $leads = json_decode($leads,true);


        //             $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
        //             ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
        //             ,'Emergency District','Emergency State','Emergency PinCode');

        //             $filename = "download.csv";

        //             $fp = fopen('download.csv', 'w');

        //             fputcsv($fp, $array );
        //             foreach ($leads as $fields) {
        //                 fputcsv($fp, $fields);
        //             }

        //             fclose($fp);

        //             $headers = array(
        //                 'Content-Type' => 'text/csv',
        //             );
        //             return Response::download($filename, 'download.csv', $headers);

        //         }

        //         else {


        //             $leads = DB::table('leads')
        //             ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
        //             ->join('services','leads.id','=','services.Leadid')
        //             ->join('personneldetails','leads.id','=','personneldetails.Leadid')
        //             ->join('addresses','leads.id','=','addresses.leadid')
        //             ->where('services.Branch',$user_city)
        //             ->where('services.ServiceStatus',$status)
        //             ->wherein('services.ServiceType',$Servicesarray)
        //             ->orderBy('leads.id', 'DESC')
        //             ->get();

        //             $leads = json_decode($leads,true);

        //             $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
        //             ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
        //             ,'Emergency District','Emergency State','Emergency PinCode');

        //             $filename = "download.csv";

        //             $fp = fopen('download.csv', 'w');

        //             fputcsv($fp, $array );
        //             foreach ($leads as $fields) {
        //                 fputcsv($fp, $fields);
        //             }

        //             fclose($fp);

        //             $headers = array(
        //                 'Content-Type' => 'text/csv',
        //             );
        //             return Response::download($filename, 'download.csv', $headers);
        //         }
        //         /*
        //         Logic for downloading CSV goes here -- ends here
        //         */
        //     }

        //     session()->put('name',$logged_in_user);

        //     return view('verticalheads.index',compact('leads'));
        // }
        // dd($_GET['download']);
        if($serviceType=="Nursing Services")
        {
            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            // dd($_GET['download']);
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                // dd($Servicesarray);

                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);
                    // dd($leads);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    // dd($leads);
                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }
        }
        else  if($serviceType=="Personal Supportive Care")
        {
            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];


            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            if(isset($_GET['download']))
            {
                $download = $_GET['download'];


                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }

        }
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            // dd($_GET['download']);
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];
                // dd($download);

                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    // dd($status);
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    // dd($leads);
                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }

            // dd($leads);

        }

        else if($serviceType=="Physiotherapy - Home")
        {
            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            if(isset($_GET['download']))
            {
                $download = $_GET['download'];


                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }
        }

        session()->put('name',$logged_in_user);

        return view('Vertical_FieldOfficer.index',compact('leads'));
    }

    public function assigned1()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        //  dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        //   dd($d);
        $leads1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads1 = json_decode($leads1,true);

        //   dd($leads1);

        $leads2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads2 = json_decode($leads2,true);

        $leads3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads3 = json_decode($leads3,true);

        $leads_all = array_merge($leads1,$leads2,$leads3);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));

        $leads1 = $leads_all;

        $count = count($leads1);

        //   dd($leads1);
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this

        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('AssignedTo',$logged_in_user)
                ->orwhere('PAssignedTo',$logged_in_user)
                ->orderBy('prodleads.id', 'DESC')
                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);

                //create a separate array to take care of the column names to be show in the CSV file
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatus',$status)
                ->where('products.AssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);

                // dd($leads1);
                $leads2  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatusrent',$status)
                ->where('products.AssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads2 = json_decode($leads2,true);

                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('pharmacies.pOrderStatus',$status)
                ->where('pharmacies.PAssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads2,$leads3);
                // dd($leads_all);

                usort($leads_all, make_comparer('id'));

                $leads = $leads_all;


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        // dd($leads);
        session()->put('name',$logged_in_user);

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        // dd($count);
        return view('Vertical_FieldOfficer.productindex',compact('leads','leads1','count','desig','d'));
    }

    public function fieldofficerfilter1(Request $request)
    {



        // dd($desig2);

        // dd("asd");
        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $keyword2=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;
        //to ensure that all tables are not fetched -- only for the logged in user


        $empid = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        // dd($filter1);
        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

                    if($keyword1 == "")
                {

                    return view('fieldexecutive.index');
                }
        // dd($status1);
        // query for assigned to and check in product and pharamchy table
                        if($filter1=="AssignedTo")
                        {

                                if($status1=="All")
                                {
                              $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                        ->orWhere('PAssignedTo', 'like',   $keyword2 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }
                    else
                    {
                         $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->orWhere('AssignedTo', 'like',   $keyword1 . '%')
                        ->orWhere('PAssignedTo', 'like',   $keyword2 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }

                        // dd($data1);

                        return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                        }

        // query for order id

                        if($filter1=="orderid")
                        {
                             if($status1=="All")
                                {
                              $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$logged_in_user)
                        ->orWhere('AssignedTo', $logged_in_user)
                        ->orWhere('PAssignedTo', $logged_in_user)
                        ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }
                    else
                    {
                         $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('prodleads.empid',$logged_in_user)
                        ->Where('PAssignedTo', $logged_in_user)
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->Where('prodleads.orderid', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(200);
                    }

                        // dd($data1);

                        return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                        }

        // query for order status and it will check for all three order status product (sell and rent) and pharamcy

                        if($filter1=="OrderStatus")
                        {

                               $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where('products.OrderStatus', 'like',   $keyword1 . '%')
                        ->Where('AssignedTo',$logged_in_user)
                        ->Where('PAssignedTo', $logged_in_user)
                        ->orWhere('products.OrderStatusrent', 'like',   $keyword1 . '%')
                        ->orWhere('pharmacies.pOrderStatus', 'like',   $keyword1 . '%')
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);

                        return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));
                        }




                        // if($filter1=="type")
                        // {



                        //     if($keyword1=="p" || $keyword1=="P")
                        //     {
                        //             $data1=DB::table('leads')
                        //        ->select('addresses.*','leads.*','prodleads.*','products.*')
                        //        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        //        ->join('products', 'leads.id', '=', 'products.leadid')
                        //        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                        //         ->orderBy('products.id', 'DESC')
                        //         ->paginate(50);


                        //          return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));

                        //     }
                        //     else
                        //     {

                        //        $data1=DB::table('leads')
                        //        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
                        //        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        //        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                        //        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                        //        ->orderBy('pharmacies.id', 'DESC')
                        //         ->paginate(50);

                        //         // dd($data1);

                        //          return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));

                        //     }
                        // }

        // common query for first name, mobile number, orderid, leadid

                         if($status1=="All")
                                {
                        $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->Where('AssignedTo',$logged_in_user)
                        ->orWhere('PAssignedTo', $logged_in_user)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                    }
                    else
                    {
                         $data1=DB::table('leads')
                        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
                        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('products', 'prodleads.Prodid', '=', 'products.id')
                        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->where('OrderStatus',$status1)
                        ->where('OrderStatusrent',$status1)
                        ->where('pOrderStatus',$status1)
                        ->Where('AssignedTo',$logged_in_user)
                        ->orWhere('PAssignedTo', $logged_in_user)
                        ->orderBy('prodleads.id', 'DESC')
                        ->paginate(50);
                    }

                        return view('fieldexecutive.productfilter',compact('data1','keyword1','filter1'));

    }
}
