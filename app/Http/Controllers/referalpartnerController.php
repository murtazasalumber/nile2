<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
use App\Log;
use App\lead;
use App\personneldetail;
use App\service;
use App\address;
use App\language;
use App\product;
use App\enquiryproduct;
use App\Activity;
use App\provisionallead;
use Session;
use Illuminate\Contracts\Auth\Guard;
use Mail;
use refer;
use App\Http\Requests;


class referalpartnerController extends Controller
{


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
     if(Auth::guard('admin')->check())
        {

     if (session()->has('name'))
    {

      $name1=session()->get('name');

    }
    else
    {
      $name1=$_GET['name'];
    }

    $leads=DB::table('leads')
    ->select('services.*','personneldetails.*','addresses.*','leads.*')
    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
    ->join('services', 'leads.id', '=', 'services.LeadId')
    ->where('leads.Source',$name1)
    ->orderBy('leads.id', 'DESC')
    ->paginate(50);
// dd($leads);
        session()->put('name',$name1);
        return view('referalpartner.index',compact('leads'));
}
else
        {
            return redirect('/admin');
        }

}

  public function show()
  {
     if(Auth::guard('admin')->check())
        {

     if (session()->has('name'))
    {

      $name1=session()->get('name');

    }
    else
    {
      $name1=$_GET['name'];
    }

    $provisionals=DB::table('provisionalleads')->where('Reference',$name1)->orderBy('id', 'DESC')->paginate(50);
// dd($leads);

    $designation="referalpartner";

        session()->put('name',$name1);
        return view('admin.provisionalview',compact('provisionals','designation'));
}
else
        {
            return redirect('/admin');
        }

}


public function view()
  {
     if(Auth::guard('admin')->check())
    {
    $name=$_GET['name'];
    $check=DB::table('employees')->where('FirstName',$name)->value('Designation');

    $provisionals=DB::table('provisionalleads')->where('active',1)->orderBY('id','DESC')->paginate(50);

    if($check=="Admin")
    {
        $designation="home";
    return view('admin.provisionalview',compact('provisionals','designation'));
    }
    else
    {
        if($check=="Management")
        {
            $designation="management";
                return view('admin.provisionalview',compact('provisionals','designation'));
        }
        else
        {
             if($check=="Marketing")
        {
                $designation="marketing";
                return view('admin.provisionalview',compact('provisionals','designation'));
        }
        else
        {
            if($check=="Branch Head")
            {
                 $designation="BranchHead";
                return view('admin.provisionalview',compact('provisionals','designation'));
            }
            else
            {
                if($check=="Customer Care")
                 {
                     return view('cc.provisionalview',compact('provisionals'));
                }
            }
                 
        }
    }
    }
  }
  else
  {
    return redirect('/admin');
    }
  }

public function create()
{

    if(Auth::guard('admin')->check())
        {

  $name=$_GET['name'];
  $id=DB::table('admins')->where('name',$name)->value('id');

$roleid=DB::table('role_admins')->where('admin_id',$id)->value('role_id');


     $data = file_get_contents("countrycode.json");

    $country_codes = json_decode($data,true);

    $count = count($country_codes);

    // $code = DB::table('leads')->where('id',$id)->value('Country_Code');
    // $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

    $data1 = file_get_contents("indian_cities.json");

    $indian_cities = json_decode($data1,true);

    $count1 = count($indian_cities);



if($roleid==1)
{

  $id=$_GET['id'];

   $reference=DB::table('refers')->get();
    $gender=DB::table('genders')->get();
    $gender1=DB::table('genders')->get();
    $city=DB::table('cities')->get();
    $pcity=DB::table('cities')->get();
    $ecity=DB::table('cities')->get();
    $branch=DB::table('cities')->get();
    $status=DB::table('leadstatus')->get();
    $leadtype=DB::table('leadtypes')->get();
    $condition=DB::table('ptconditions')->get();
    $language=DB::table('languages')->get();
    $relation=DB::table('relationships')->get();
    $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
    $emp=DB::table('employees')->where('Designation','Vertical Head')->get();

      


    $provisional=DB::table('provisionalleads')->where('id',$id)->get();
    
    $type=DB::table('provisionalleads')->where('id',$id)->value('RequestType');

$Designation="Customer Care";
$designation="customercare";
$products=DB::table('productdetails')->get();

    if($type=="Product")
    {
        return view('cc.createprovisionalproduct',compact('count1','country_codes','indian_cities','count','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','provisional','Designation','products','designation'));
    }
    else
    {
        return view('cc.createprovisional',compact('count1','country_codes','indian_cities','count','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','provisional','designation'));
    }
    
}
else
{
    
   $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
    return view('referalpartner.create',compact('count1','country_codes','indian_cities','count','vertical'));
    
}

}
else
        {
            return redirect('/admin');
        }
}




public function store(Request $request)
{
    $name=$request->loginname;


    $provisional = new provisionallead;

    $provisional->FName=$request->FName;
    $provisional->MName=$request->MName;
    $provisional->LName=$request->LName;
    $provisional->MobileNumber=$request->MobileNumber;
    $provisional->AlternateNumber=$request->AlternateNumber;
    $provisional->RequestType=$request->RequestType;
    $provisional->ServiceType=$request->ServiceType;
    $provisional->ProductName=$request->ProductName;
    $provisional->Address=$request->Address;
    $provisional->City=$request->City;
    $provisional->Reference=$request->loginname;
    $provisional->save();


$logged_in_user = Auth::guard('admin')->user()->name;
        $designation="referalpartner";
        // dd($logged_in_user);

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $newcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status0)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $inprogresscount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status1)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $convertedcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status2)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $droppedcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status3)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $deferredcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status4)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $provisionals=DB::table('provisionalleads')->where('Reference',$logged_in_user)->orderBy('id', 'DESC')->paginate(50);


session()->put('name',$name);
session()->flash('message','Lead is submitted and the status will be updated');

  return view('admin.provisionalview',compact('newcount','inprogresscount','convertedcount','deferredcount','droppedcount','designation','provisionals'));
}


}
