<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\vertical;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class verticalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::guard('admin')->check())
        {
          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {


                 $verticals= vertical::paginate(50);
                 return view('vertical.index',compact('verticals'));
                 }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                  return view('vertical.create');
                }
                else
                {
                    return redirect('/admin');
                }

        }else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $vertical = new vertical;
        $this->validate($request,[
                'verticaltype'=>'required',
                'servicetype'=>'required',

            ]);
       $vertical->verticaltype=$request->verticaltype;
       $vertical->servicetype=$request->servicetype;


       $vertical->save();

       $verticalid = DB::table('verticals')->max('id');
       //    dd($langid);

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $request->verticaltype." and ".$request->servicetype." Verticals Added ";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;
       $activity->save();


       return redirect('/verticals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                  $vertical= vertical::find($id);
                  return view('vertical.edit',compact('vertical'));
                  }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $c=DB::table('verticals')->where('id',$id)->value('verticaltype');
         $c1=DB::table('verticals')->where('id',$id)->value('servicetype');
        if($request->verticaltype!=$c || $request->servicetype!=$c1)
        {

       $vertical = vertical::find($id);
        $vertical->verticaltype=$request->verticaltype;
          $vertical->servicetype=$request->servicetype;

          $vertical1 = $vertical->getDirty();
          // dd($lang);

          foreach($vertical1 as $key=>$value)
          {
              $vertical1c[] = $key;

          }

          foreach($vertical1 as $key=>$value)
          {
              $vertical1v[] = $value;
          }

          //this is for converting array values into a string with new line

          //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
          if(empty($vertical1c))
          {
              $vertical1d = " ";
              $vertical1f = " ";
              $vertical1d = trim($vertical1d);
              $vertical1f = trim($vertical1f);
          }
          else
          {
              $vertical1d = implode("\r\n", $vertical1c);
              $vertical1f = implode("\r\n", $vertical1v);
          }


       $vertical->save();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       if($vertical1d==" " || $vertical1d==' ' || $vertical1d=="" || $vertical1d=='')
       {
           $vertical1d = trim($vertical1d);
           $vertical1f = trim($vertical1f);
       }
       else
       {
           $vertical1d = $vertical1d."\r\n";
           $vertical1f = $vertical1f."\r\n";
       }

       //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
       $fields_updated = $vertical1d;
       $values_updated = $vertical1f;

       if((trim($fields_updated)!==""))
       {
           // dd($fields_updated);
           $activity = new Activity;
           $activity->emp_id = $emp_id;
           $activity->activity = "Fields Updated";
           $activity->activity_time = new \DateTime();
           $activity->log_id = $log_id;
           $activity->field_updated = $fields_updated;
           $activity->value_updated = $values_updated;

           $activity->save();
       }

       session()->flash('message','Updated Successfully');

   }
       return redirect('/verticals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vertical=vertical::find($id);

        $vertical_del = DB::table('verticals')->where('id',$id)->value('verticaltype');
        $vertical1_del = DB::table('verticals')->where('id',$id)->value('servicetype');

       $vertical->delete();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $vertical_del." and ".$vertical1_del." Vertical Deleted";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;

       $activity->save();

       session()->flash('message','Delete Successfully');
       return redirect('/verticals');
    }
}
