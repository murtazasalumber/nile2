<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Auth;
use DB;
use Session;

class fieldexecutiveController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('fieldexecutive');
    }


    public function index()
    {
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $designation="fieldexecutive";

        // dd($logged_in_user);

        $log_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');




        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";

        $processingcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status1)
        // ->orwhere('OrderStatusrent',$status1)
        // ->orwhere('pharmacies.pOrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $processingcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status1)
        ->where('OrderStatusrent',$status1)
        // ->orwhere('pharmacies.pOrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $processingcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status1)
        // ->orwhere('OrderStatusrent',$status1)
        ->where('pharmacies.pOrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $processingcount = $processingcount1 + $processingcount2 + $processingcount3;

        $awaitingpickupcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status2)
        // ->orwhere('OrderStatusrent',$status2)
        // ->orwhere('pharmacies.pOrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status2)
        ->where('OrderStatusrent',$status2)
        // ->orwhere('pharmacies.pOrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status2)
        // ->orwhere('OrderStatusrent',$status2)
        ->where('pharmacies.pOrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount = $awaitingpickupcount1 + $awaitingpickupcount2 + $awaitingpickupcount3;

        $outfordeliverycount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status3)
        // ->orwhere('OrderStatusrent',$status3)
        // ->orwhere('pharmacies.pOrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status3)
        ->where('OrderStatusrent',$status3)
        // ->orwhere('pharmacies.pOrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status3)
        // ->orwhere('OrderStatusrent',$status3)
        ->where('pharmacies.pOrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount = $outfordeliverycount1 + $outfordeliverycount2 + $outfordeliverycount3;

        $readytoshipcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status4)
        // ->orwhere('OrderStatusrent',$status4)
        // ->orwhere('pharmacies.pOrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status4)
        ->where('OrderStatusrent',$status4)
        // ->orwhere('pharmacies.pOrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status4)
        // ->orwhere('OrderStatusrent',$status4)
        ->where('pharmacies.pOrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount = $readytoshipcount1 + $readytoshipcount2 + $readytoshipcount3;

        $orderreturncount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status5)
        // ->orwhere('OrderStatusrent',$status5)
        // ->orwhere('pharmacies.pOrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status5)
        ->where('OrderStatusrent',$status5)
        // ->orwhere('pharmacies.pOrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status5)
        // ->orwhere('OrderStatusrent',$status5)
        ->where('pharmacies.pOrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount = $orderreturncount1 + $orderreturncount2 + $orderreturncount3;

        $canceledcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status6)
        // ->orwhere('OrderStatusrent',$status6)
        // ->orwhere('pharmacies.pOrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status6)
        ->where('OrderStatusrent',$status6)
        // ->orwhere('pharmacies.pOrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status6)
        // ->orwhere('OrderStatusrent',$status6)
        ->where('pharmacies.pOrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount = $canceledcount1 + $canceledcount2 + $canceledcount3;

        $deliveredcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        ->where('OrderStatus',$status7)
        // ->orwhere('OrderStatusrent',$status7)
        // ->orwhere('pharmacies.pOrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status7)
        ->where('OrderStatusrent',$status7)
        // ->orwhere('pharmacies.pOrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('prodleads.empid',$log_id)
        // ->orwhere('OrderStatus',$status7)
        // ->orwhere('OrderStatusrent',$status7)
        ->where('pharmacies.pOrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount = $deliveredcount1 + $deliveredcount2 + $deliveredcount3;

        return view('admin.fieldexecutive',compact('processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation'));
    }

    public function assigned()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;

        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        $log_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        $leads = DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status)
        // ->orwhere('OrderStatusrent',$status)
        // ->orwhere('pharmacies.pOrderStatus',$status)
        ->where('prodleads.empid',$log_id)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads = json_decode($leads,true);


        $leads1 = DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        // ->where('OrderStatus',$status)
        ->orwhere('OrderStatusrent',$status)
        // ->orwhere('pharmacies.pOrderStatus',$status)
        ->where('prodleads.empid',$log_id)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads1 = json_decode($leads1,true);


        $leads2 = DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        // ->where('OrderStatus',$status)
        // ->orwhere('OrderStatusrent',$status)
        ->orwhere('pharmacies.pOrderStatus',$status)
        ->where('prodleads.empid',$log_id)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads2 = json_decode($leads2,true);
        // dd($leads2);

        $leads_all = array_merge($leads,$leads1,$leads2);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));


        $leads1 = $leads_all;
        $count = count($leads1);
        // dd($count);
        // dd($leads_all);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            $eid=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {

                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('prodleads.empid','=',$eid)
                ->orderBy('prodleads.id', 'DESC')
                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatus',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);

                $leads2  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatusrent',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads2 = json_decode($leads2,true);

                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('pharmacies.pOrderStatus',$status)
                ->where('prodleads.empid',$eid)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads2,$leads3);


                usort($leads_all, make_comparer('id'));

                $leads = $leads_all;


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                    $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        session()->put('name',$logged_in_user);

        return view('fieldexecutive.index1',compact('leads1','d','count','desig'));
    }



}
