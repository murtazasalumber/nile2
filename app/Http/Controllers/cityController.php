<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\city;
use DB;
use Auth;
use App\Activity;

use Session;


class cityController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {
                $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $city= city::paginate(50);
                    return view('city.index',compact('city'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    return view('city.create');
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $city = new city;
        $this->validate($request,[
            'name'=>'required',
        ]);
        $city->name=$request->name;
        $city->save();

        $cityid = DB::table('languages')->max('id');
        //   dd($cityid);

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $request->name." City Added ";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->save();

        return redirect('/city');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        if(Auth::guard('admin')->check())
        {

             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $city = city::find($id);
                    return view('city.edit',compact('city'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $c=DB::table('cities')->where('id',$id)->value('name');
        if($request->name!=$c)
        {
            $city = city::find($id);
            $city->name=$request->name;

            $city1 = $city->getDirty();
            // dd($city1);

            foreach($city1 as $key=>$value)
            {
                $cityc[] = $key;

            }

            foreach($city1 as $key=>$value)
            {
                $cityv[] = $value;
            }

            //this is for converting array values into a string with new line

            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
            if(empty($cityc))
            {
                $cityd = " ";
                $cityf = " ";
                $cityd = trim($cityd);
                $cityf = trim($cityf);
            }
            else
            {
                $cityd = implode("\r\n", $cityc);
                $cityf = implode("\r\n", $cityv);
            }

            $city->save();

            //retrieving the session id of the person who is logged in
            $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

            //retrieving the username of the presently logged in user
            $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

            // retrieving the employee id of the presently logged in user
            $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

            //retrieving the log id of the person who logged in just now
            $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

            if($cityd==" " || $cityd==' ' || $cityd=="" || $cityd=='')
            {
                $cityd = trim($cityd);
                $cityf = trim($cityf);
            }
            else
            {
                $cityd = $cityd."\r\n";
                $cityf = $cityf."\r\n";
            }

            //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
            $fields_updated = $cityd;
            $values_updated = $cityf;

            // dd($fields_updated);
            if((trim($fields_updated)!==""))
            {
                // dd($fields_updated);
                $activity = new Activity;
                $activity->emp_id = $emp_id;
                $activity->activity = "Fields Updated";
                $activity->activity_time = new \DateTime();
                $activity->log_id = $log_id;
                $activity->field_updated = $fields_updated;
                $activity->value_updated = $values_updated;

                $activity->save();

                session()->flash('message','Updated Successfully');

            }
        }
        return redirect('/city');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $city=city::find($id);

        $city_del = DB::table('cities')->where('id',$id)->value('name');
        // dd($lang_del);

        $city->delete();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $city_del." City Deleted";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;

        $activity->save();

        session()->flash('message','Delete Successfully');
        return redirect('/city');
    }
}
