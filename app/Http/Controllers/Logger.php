<?php
// <!-- This is the Log Controller -->

namespace App\Http\Controllers;

use App\Log;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Session;


class Logger extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */


    public function index()
    {

        /* Code For showing the Logs starts here -- by jatin */

        if(Auth::guard('admin')->check())
        {
            $logged_in_user = Auth::guard('admin')->user()->name;
            $designation = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

            if($designation=="Admin" || $designation=="Management")

            {
            //for retrieving all the log data from activity and log tables
            $log = DB::table('logs')
            ->select('logs.*','activities.*')
            ->join('activities', 'logs.id', '=', 'activities.log_id')
            ->orderBy('activities.id', 'DESC')
            ->paginate(50);

            // dd($log);

            return view('admin.log',compact('log'));
        }
        else
        {
            return redirect('/admin');
        }
        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
        /* Code For showing the Logs ends here -- by jatin */
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {

    }
    // filter for log
    public function filter(Request $request)
    {
        $keyword=$request->keyword1;
        $filter=$request->filter1;

        // dd($keyword, $filter);
        //to ensure that all tables are not fetched -- only for the logged in user
        if($keyword == "")
        {

            return  view('admin.alllog1');
        }
        else
        {
            //Join query to fetch the required result -- send all the retrieved data to the view
            $data=DB::table('logs')->select('logs.*','activities.*')->join('activities', 'logs.id', '=', 'activities.log_id')->Where($filter, 'like',   $keyword . '%')->get();
            // dd($data);
            return  view('admin.alllog',compact('data','keyword','filter'));
        }


    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
