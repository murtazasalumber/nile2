<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\employee;
use App\disablelist;
use App\Admin;
use App\role_admin;
use Auth;
use App\Activity;
use Session;
use Redirect;

class employeesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(Auth::guard('admin')->check())
        {

            $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                        $employees= DB::table('employees')->paginate(50);
                        return view('employee.index',compact('employees'));
                 }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $v=DB::table('employees')->where('Designation','Vertical Head')->orwhere('Designation3','Field Officer')->orwhere('Designation','Management')->orwhere('Designation2','Product Manager')->orwhere('Designation2','Pharmacy Manager')->orwhere('Designation','Branch Head')->get();
        $city=DB::table('cities')->get();
        $language=DB::table('languages')->get();


        if(Auth::guard('admin')->check())
        {
             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                     return view('employee.create',compact('v','city','e','e1','ename','ename1','language'));

             }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    public function store(Request $request)
    {


        $uname=$request->under;
        $uid=DB::table('employees')->where('FirstName',$uname)->value('id');

          $uname1=$request->under1;
        $uid1=DB::table('employees')->where('FirstName',$uname1)->value('id');

        /* Code for Employee Entry starts here -- by Murtuza and jatin */


$designation11= $request->Designationchecked;
     $department11= $request->Departmentchecked;
     // dd($department11);
      $under11=$request->underchecked;
      $LangaugesKnown11=$request->LangaugesKnownchecked;


  $LangaugesKnown11 = str_replace(array('[',']' ,),' ',$LangaugesKnown11);
                $LangaugesKnown11 = preg_replace('/"/', '', $LangaugesKnown11);
                $LangaugesKnown11 = trim($LangaugesKnown11);

                // dd($LangaugesKnown11);

                $Designationcheckedfortrim= $request->Designationchecked;


        $designation1 = $designation2 = $designation3 = $department =$department2 =$department3 = $under1 = $under2 = $under3 = NULL;


        $design1 = $design2 = $design3 = $dept1 = $dept2 = $dept3 = NULL;
        $uname1=$under1=$under2=$uname2=NULL;


                // dd($languagefortrim);





                $designation11 = str_replace(array('[',']' ,),' ',$Designationcheckedfortrim);
                $designation11 = preg_replace('/"/', '', $designation11);
                $designation11 = trim($designation11);

                $designation11 = explode(",",$designation11);
                $designationcount= count($designation11);


                    for($i=0;$i<$designationcount;$i++)
                {
                    // $designation1=$designation11[$i];
                     if($designation1==NULL)
                    {
                    $designation1=$designation11[$i];
                    }

                    else if($designation2==NULL)
                    {
                        $designation2=$designation11[$i];
                    }
                    else if($designation3==NULL)
                    {
                        $designation3=$designation11[$i];
                    }
                }


                    $Departmentcheckedfortrim= $request->Departmentchecked;
                // dd($Departmentcheckedfortrim);

                $department11 = str_replace(array('[',']' ,),' ',$Departmentcheckedfortrim);
                $department11 = preg_replace('/"/', '', $department11);
                // dd($department11);
                $department11 = trim($department11);
                 $department11 = explode(",",$department11);
                 // dd($department11);
                $departmentcount= count($department11);
// dd($departmentcount);

                   for($i=0;$i<$departmentcount;$i++)
                {
                    // $department1=$department11[$i];
                     if($department==NULL)
                    {
                    $department=$department11[$i];
                    }

                    else if($department2==NULL)
                    {
                        $department2=$department11[$i];
                    }
                    else if($department3==NULL)
                    {
                        $department3=$department11[$i];
                    }
                }

                $undercheckedfortrim= $request->underchecked;
                // dd($languagefortrim);

                $under11 = str_replace(array('[',']' ,),' ',$undercheckedfortrim);
                $under11 = preg_replace('/"/', '', $under11);
                $under11 = trim($under11);

                 $under11 = explode(",",$under11);
                $undercount= count($under11);


                        for($i=0;$i<$undercount;$i++)
                {
                    // $under1=$under11[$i];
                   $employeedesignation=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation');
                     $employeedesignation1=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation2');
                     $employeedesignation2=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation3');
// dd($employeedesignation,$employeedesignation1,$employeedesignation2);

if(($employeedesignation==Null && $employeedesignation2=="Field Officer") || ($employeedesignation!="Field Officer" && $employeedesignation2=="Field Officer") || ($employeedesignation1!="Field Officer" && $employeedesignation2=="Field Officer"))
{


            $uname2=$under11[$i];

            $employee_id2=DB::table('employees')->where('FirstName',$uname2)->value('id');

              $under2=$employee_id2;



}
else
{

        $uname1=$under11[$i];


            $employee_id1=DB::table('employees')->where('FirstName',$uname1)->value('id');

              $under1=$employee_id1;



                }


                }


 // dd($designation1,$designation2,$designation3,$department,$designation3,$designation2,$under1,$under2,$under3);


$reg_id=$request->Regid;

$EmailID=$request->Email_id;

$check_Reg_id=DB::table('employees')->where('Regid',$reg_id)->value('id');

$check_email=DB::table('employees')->where('Email_id',$EmailID)->value('id');

if($check_Reg_id != NULL)
{
    // dd('as');

    $Regid=$request->Regid;
        $FirstName=$request->FirstName;
        $MiddleName=$request->MiddleName;
        $LastName=$request->LastName;
        $Email_id=$request->Email_id;
        $DOB=$request->DOB;
        $Gender=$request->Gender;
        $MartialStatus=$request->MartialStatus;
        $BloodGroup=$request->BloodGroup;
        $DOJ=$request->DOJ;
        $EmployementType=$request->EmployementType;
        $Designation=$request->Designation;
        $MobileNumber=$request->MobileNumber;
        $AlternateNumber=$request->AlternateNumber;
        $Department=$request->Department;
        $city=$request->city;
        $under=$under1;
        $underwhom=$uname1;
        $under1=$under2;
        $ReportingTo=$uname2;
        $LangaugesKnown=$LangaugesKnown11;



        $city=DB::table('cities')->get();
        $language=DB::table('languages')->get();
         $v=DB::table('employees')->where('Designation','Vertical Head')->orwhere('Designation3','Field Officer')->orwhere('Designation','Branch Head')->orwhere('Designation','Management')->orwhere('Designation2','Product Manager')->orwhere('Designation2','Pharmacy Manager')->orwhere('Designation','Branch Head')->get();
        $v1=DB::table('employees')->where('Designation','Vertical Head')->orwhere('Designation3','Field Officer')->orwhere('Designation','Branch Head')->orwhere('Designation','Management')->orwhere('Designation2','Product Manager')->orwhere('Designation2','Pharmacy Manager')->orwhere('Designation','Branch Head')->get();
$logged_in_user=Auth::guard('admin')->user()->name;

session()->flash('message','REG ID already in use');
    return redirect('/employees');

    // $message="REG ID already in use";
    // return view('employee.error',compact('Regid','FirstName','MiddleName','LastName','Email_id','DOB','Gender','MartialStatus','BloodGroup','DOJ','EmployementType','Designation','MobileNumber','AlternateNumber','Department','city','under','underwhom','under1','ReportingTo','LangaugesKnown','message','city','language','v','v1'));
}

if($check_email != NULL)
{
    session()->flash('message','Email ID already in use');
    return redirect('/employees');
}







        $employee = new employee;

        $employee->Regid=$request->Regid;
        $employee->FirstName=$request->FirstName;
        $employee->MiddleName=$request->MiddleName;
        $employee->LastName=$request->LastName;
        $employee->Email_id=$request->Email_id;
        $employee->DOB=$request->DOB;
        $employee->Gender=$request->Gender;
        $employee->MartialStatus=$request->MartialStatus;
        $employee->BloodGroup=$request->BloodGroup;
        $employee->DOJ=$request->DOJ;
        $employee->EmployementType=$request->EmployementType;
        // $employee->Designation=$request->Designation;
        $employee->MobileNumber=$request->MobileNumber;
        $employee->AlternateNumber=$request->AlternateNumber;
        // $employee->Department=$request->Department;
        $employee->city=$request->city;
        $employee->under=$under1;
        $employee->underwhom=$uname1;
        $employee->under1=$under2;
        $employee->ReportingTo=$uname2;
        $employee->LangaugesKnown=$LangaugesKnown11;
        $employee->save();

        /* Code for Employee Entry ends here -- by Murtuza and jatin */

        /* Code for Employee Login Creation starts here -- by Murtuza and jatin */

        //search the employee with the latest entry
        $employee=DB::table('employees')->max('id');
        $id=DB::table('employees')->max('id');

        //retrieve his/her name and email id
        $empname=$request->FirstName;
        $empemail=$request->Email_id;

        //give all employee Login password as below initially
        $emppassword=bcrypt('Healthheal@123');

        //Depending on the designation selected by the creating user, retrieve the role
        //and assign corresponding role ids so that they can be stored in another table in DB


//fetching values for all Designations and Department

        // $designation1 = $request->Designation;
        // $designation2 = $request->Designation2;
        // $designation3 = $request->Designation3;
        // $department = $request->Department;
        // $department2 = $request->Department2;
        // $department3 = $request->Department3;


        // $design1 = $design2 = $design3 = $dept1 = $dept2 = $dept3 = NULL;


        /* Checking which of the designations is Higher and assigning the corresponding role starts here Eg - Admin and Vertical head -- by jatin*/

        // Checking if any of the designations selected by the user has "Admin" in them. In this case, since "Admin" will be highest of the two (Except in case of "Management and Branch Head")
        //the "Admin" role shall be assigned by default -- by jatin
        if($designation1=="Admin" || $designation2=="Admin" || $designation3=="Admin")
        {
            $desig1 ="Admin";
            $roleid=3;


             $employee = employee::find($id);
             $employee->Designation="Admin";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();

            // dd("Admin");

        }
        // Checking if any of the designations selected by the user has "Management" in them. In this case, since "Management" will be highest of the two (Except in case of "Admin and Branch Head")
        //the "Management" role shall be assigned by default -- by jatin
        else if($designation1=="Management" || $designation2=="Management" || $designation3=="Management")
        {
            $desig1 ="Management";
            $roleid=5;


             $employee = employee::find($id);
             $employee->Designation="Management";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();

            // dd("management");

        }
        // Checking if any of the designations selected by the user has "Branch Head" in them. In this case, since "Branch Head" will be highest of the two (Except in case of "Admin and Branch Head")
        //the "Branch Head" role shall be assigned by default -- by jatin
        else if($designation1=="Branch Head" || $designation2=="Branch Head" || $designation3=="Branch Head")
        {
            $desig1 ="Branch Head";
            $roleid=32;

             $employee = employee::find($id);
             $employee->Designation="Branch Head";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();            // dd("BH");

        }
        // Checking if any one of the designations selected by the user has "Vertical Head" in them -- by jatin
        else if($designation1=="Vertical Head" || $designation2=="Vertical Head" || $designation3=="Vertical Head")
        {
            // Scenario -- by jatin

            // Designation 1           Designation  2                               Designation 3
            // Vertical Head            NULL                                               NULL
            // NS                             NULL                                               NULL


            // fixing the designation place value to first position
            $design1="Vertical Head";
            $roleid=2;

            // Now that the designation has been set we check which services is provided in the department -- by jatin
            if($department == "Nursing Services" || $department2 == "Nursing Services" || $department3 == "Nursing Services" )
            {
                //fixing the place value for service selected to first position
                $dept1 = "Nursing Services";
            }
            else if($department == "Mathrutvam - Baby Care" || $department2 == "Mathrutvam - Baby Care" || $department3 == "Mathrutvam - Baby Care" )
            {
                $dept1 = "Mathrutvam - Baby Care";
            }
            else if($department == "Personal Supportive Care" || $department2 == "Personal Supportive Care" || $department3 == "Personal Supportive Care" )
            {
                $dept1 = "Personal Supportive Care";
            }
            else if($department == "Physiotherapy - Home" || $department2 == "Physiotherapy - Home" || $department3 == "Physiotherapy - Home" )
            {
                $dept1 = "Physiotherapy - Home";
            }
            else if($department == "All" || $department2 == "All" || $department3 == "All" )
            {
                $dept1 = "All";
            }




            /* if loop for checking Role as Vertical, Product Selling Manager and /or Pharmacy manager starts here -- by jatin */

            //if Product Manager comes at any place, then fix its position to second variable for designation
            if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
            {
                $design2 = "Product Manager";


                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {

                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept1"
                    //assign "Product  - Selling" here

                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                               Designation 3
                    // Product Manager           Vertical Head                              NULL
                    // Product Selling              NS                                               NULL



                    if($dept1 == NULL)
                    {
                        $dept1 = "Product - Selling";
                    }
                    //if "dept1" has already been assigned in the first place say with Vertical Head. In that case, we need to assign "dept2" with "product selling"

                    //Scenario

                    // Designation 1           Designation  2                                    Designation 3
                    // Vertical Head           Product Manager                                      NULL
                    // Nursing Services      Product - Selling                                      NULL

                    else
                    {
                        $dept2 = "Product - Selling";

                    }
                    // dd("vh pm s123");

                }
                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept2"
                    //assign "Product  - Rental" here

                    //Scenario

                    // Designation 1           Designation  2                      Designation 3
                    // Vertical Head           Product Manager                                    NULL
                    // NS                            Product Rental                                     NULL


                    // till this point the value of dept2 is NULL, because above "if" condition didn't run
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    //Scenario

                    // Designation 1           Designation  2                                       Designation 3
                    // Vertical Head           Product Manager                                    Product Manager
                    // NS                            Product Rental                                       Product Selling



                    //this means that the above "if loop for Product Selling has already run", so we assign "dept3" as "Product Rental"
                    else
                    {
                        $dept3 = "Product - Rental";

                        // Since the person is Product manager for both departments, we make it into a single department "product"
                        if($dept2=="Product - Selling" && $dept3=="Product - Rental")
                        {
                            //make the "dept2" as "product" and make the "dept3" as "NULL"
                            $dept2="Product";
                            $dept3=NULL;
                        }
                    }
                    // dd("vh pm r");
                }

                //Scenario

                // Designation 1           Designation  2                                       Designation 3
                // Vertical Head           Product Manager                                    NULL
                // NS                            Product                                                    NULL



                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    //if none of the above "if or else if" conditions ran, then "dept2" has NULL, so set it to "Product"
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }
                    //if "dept2" has already been assigned, then make "dept3" as "Product"

                    //Scenario

                    // Designation 1           Designation  2                                                          Designation 3
                    // Vertical Head           Product Manager                                                  Product Manager
                    // NS                            Product Selling/Rental                                                    Product

                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh pm p");
                }
                // dd($designation1,$designation2,$designation3);

            }

            //if any of the designation has "Pharmacy Manager"
            if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
            {
                $roleid=15;

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         NULL
                // NS                               Product                                                                 NULL


                if($design2==NULL)
                {
                    $design2 = "Pharmacy Manager";
                    $dept2 = "Product";
                }

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         Product Manager
                // NS                               Product                                                                 Product/Product - Selling/Product - Rental
                else
                {
                    $design3 = "Pharmacy Manager";
                    $dept3 = "Product";

                }
                // dd("vh ph",$design2, $dept2);
                // dd($designation1,$designation2,$designation3);

            }
            //if a person has to be made both "Product and Pharmacy Manager"
            if($designation1=="Product and Pharmacy Manager" || $designation2=="Product and Pharmacy Manager" || $designation3=="Product and Pharmacy Manager")
            {
                // dd("vh ppm");
                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Product and Pharmacy Manager                                  NULL
                // NS                               Product                                                                 NULL

                if($design2==NULL)
                {
                    $design2 = "Product and Pharmacy Manager";


                }


                //Scenario

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           Pharmacy/Product Manager                                                         Product and Pharmacy Manager
                // NS                           Product/Product Selling/Product Rental                                        Product

                else
                {
                    $design3 = "Product and Pharmacy Manager";

                }


                //checking for department if both "product and pharmacy manager" are selected

                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Selling

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Selling";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Selling

                    else
                    {
                        $dept3 = "Product - Selling";
                    }
                    // dd("vh ppm s");
                }
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Rental

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Rental

                    else
                    {
                        $dept3 = "Product - Rental";

                    }
                    // dd("vh ppm r");
                }
                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }


                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product


                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh ppm p");


                }

            }
            else if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Officer
                // NS                           NULL                                                                                                  Product

                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Officer";
                    $dept2 = "Product";
                    // dd("vh fo");
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Officer
                // NS                           Product/Rental/Selling                                                                                             Product
                else
                {
                    $design3 = "Field Officer";
                    $dept3 = "Product";

                    // dd("vh",$design2," fo");
                }

            }
            else if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
            {

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Executive
                // NS                           NULL                                                                                                  Product
                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Executive";
                    $dept2 = "Product";

                    // dd("vh  fe");
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Executive
                // NS                           Product/Rental/Selling                                                                                             Product

                else
                {
                    $design3 = "Field Executive";
                    $dept3 = "Product";
                    // dd("vh ",$design2," fe");

                }
            }

            // dd($design1, );
            // dd("d1",$design1,"d2",$design2,"d3",$design3,"<br>","d11",$dept1,"d22",$dept2,"d33",$dept3 );

            // ******* Important note  ******
            // We have ensured that all Coordinators and Vertical Heads use the variables "design1" and "dept1"
            // All Product Managers and Pharmacy Managers use the variables "design2" and "dept2"
            // All Field Officers and Field executives use the variables "design3" and "dept3"

            //Since we have defined the designations for each field we are checking all possible scenarios of matching them

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           NULL                                                                                                                 NULL
            // NS                           NULL                                                                                                                    NULL

            if($design1=="Vertical Head" && $design2==NULL && $design3==NULL)
            {
                // the role id is for "Vertical Head" only
                $roleid = 2;
            }
            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3==NULL)
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                                 NULL
                // NS                           Product - Selling                                                                                                                    NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager with department Product Selling
                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    NULL
                // NS                           Product - Rental                                                                                                        NULL

                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager with department Product Rental

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                     NULL
                // NS                           Product                                                                                                                    NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager handling both Rental and Selling

                    $roleid = 14;
                }

            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                                                 NULL
            // NS                           Product                                                                                                                    NULL

            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product"

                $roleid = 15;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                              NULL
            // NS                           Product                                                                                                                    NULL
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Vertical Head and (Product and Pharmacy Manager) handling department "Product"

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }



            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Field Executive")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product - Selling                                                                                                    Null
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Selling" and Field Executive
                    //this get combined as Product Manager for selling only

                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product - Rental                                                                                                    NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Rental" and Field Executive
                    //this get combined as Product Manager for rental only

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                                     NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Executive
                    //this get combined as Product Manager for Product only

                    $roleid = 14;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Selling                                                                                                         Product

                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Selling" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Rental                                                                                                         Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Rental" and Field Executive with department "Product"
                    //this get combined as Product Manager for Rental only

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                         Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only
                    $roleid = 14;
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                                               Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Vertical Head and (Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                $roleid = 15;
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and  Pharmacy Manager                                                                  Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Vertical Head and (Product and Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }


            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Selling                                                                                                       NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product  Manager) handling department "Product - Selling" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 16;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Rental                                                                                                       NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager ) handling department "Product - Rental" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 17;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                     NULL
                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product " and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 14;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product - Selling                                                                                                    Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product Selling" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 16;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product - Rental                                                                                                    Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product Rental" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Rental only
                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                 Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 14;
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Pharmacy Manager                                                                                                   Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Field Officer")
            {

                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Pharmacy Manager for Product only
                $roleid = 15;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and Pharmacy Manager                                                                         Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Field Officer")
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

             if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Product Manager")
            {

                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }


            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Pharmacy Manager")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Selling                                                                                                                 Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and Product Manager handling department "Product - Selling" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Selling only

                    $roleid = 19;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Rental                                                                                                                 Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {

                    // the role id is for Vertical Head and Product Manager handling department "Product - Rental" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Rental only
                    $roleid = 20;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product                                                                                                              Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and Product Manager handling department "Product" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product only
                    $roleid = 18;
                }
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Field Officer                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Vertical Head" && $design2=="Field Officer")
            {
                // the role id is for Vertical Head and Field Officer handling department "Product

                $roleid = 28;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Field Executive                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Vertical Head" && $design2=="Field Executive")
            {
                // the role id is for Vertical Head and Field Executive handling department "Product

                $roleid = 29;
            }

            /* if loop for checking Role as Vertical, Product Selling Manager and Pharmacy manager ends here */


 // putting all Product related designations in column "Designation2" and all Field related designations in "Designation3"
            // dd($dept1);
        $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation2=$design2;
        $employee->Designation3=$design3;

        $employee->Department=$dept1;
        $employee->Department2=$dept2;
        $employee->Department3=$dept3;
        $employee->save();


          if($design2=="Field Officer" || $design2=="Field Executive")
          {
            $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation3=$design2;

        $employee->Department=$dept1;
        $employee->Department3=$dept2;
        $employee->save();
          }

        }

        //coordinator logic
        //refer to comments for Coordinator to understand what is happening here
        else if($designation1=="Coordinator" || $designation2=="Coordinator" || $designation3=="Coordinator")
        {
            $design1="Coordinator";
            $roleid=6;


              // Now that the designation has been set we check which services is provided in the department -- by jatin
            if($department == "Nursing Services" || $department2 == "Nursing Services" || $department3 == "Nursing Services" )
            {
                //fixing the place value for service selected to first position
                $dept1 = "Nursing Services";
            }
            else if($department == "Mathrutvam - Baby Care" || $department2 == "Mathrutvam - Baby Care" || $department3 == "Mathrutvam - Baby Care" )
            {
                $dept1 = "Mathrutvam - Baby Care";
            }
            else if($department == "Personal Supportive Care" || $department2 == "Personal Supportive Care" || $department3 == "Personal Supportive Care" )
            {
                $dept1 = "Personal Supportive Care";
            }
            else if($department == "Physiotherapy - Home" || $department2 == "Physiotherapy - Home" || $department3 == "Physiotherapy - Home" )
            {
                $dept1 = "Physiotherapy - Home";
            }
            else if($department == "All" || $department2 == "All" || $department3 == "All" )
            {
                $dept1 = "All";
            }




            /* if loop for checking Role as Vertical, Product Selling Manager and /or Pharmacy manager starts here -- by jatin */

            //if Product Manager comes at any place, then fix its position to second variable for designation
            if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
            {
                $design2 = "Product Manager";


                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {

                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept1"
                    //assign "Product  - Selling" here

                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                               Designation 3
                    // Product Manager           Vertical Head                              NULL
                    // Product Selling              NS                                               NULL



                    if($dept1 == NULL)
                    {
                        $dept1 = "Product - Selling";
                    }
                    //if "dept1" has already been assigned in the first place say with Vertical Head. In that case, we need to assign "dept2" with "product selling"

                    //Scenario

                    // Designation 1           Designation  2                                    Designation 3
                    // Vertical Head           Product Manager                                      NULL
                    // Nursing Services      Product - Selling                                      NULL

                    else
                    {
                        $dept2 = "Product - Selling";

                    }
                    // dd("vh pm s123");

                }
                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept2"
                    //assign "Product  - Rental" here

                    //Scenario

                    // Designation 1           Designation  2                      Designation 3
                    // Vertical Head           Product Manager                                    NULL
                    // NS                            Product Rental                                     NULL


                    // till this point the value of dept2 is NULL, because above "if" condition didn't run
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    //Scenario

                    // Designation 1           Designation  2                                       Designation 3
                    // Vertical Head           Product Manager                                    Product Manager
                    // NS                            Product Rental                                       Product Selling



                    //this means that the above "if loop for Product Selling has already run", so we assign "dept3" as "Product Rental"
                    else
                    {
                        $dept3 = "Product - Rental";

                        // Since the person is Product manager for both departments, we make it into a single department "product"
                        if($dept2=="Product - Selling" && $dept3=="Product - Rental")
                        {
                            //make the "dept2" as "product" and make the "dept3" as "NULL"
                            $dept2="Product";
                            $dept3=NULL;
                        }
                    }
                    // dd("vh pm r");
                }

                //Scenario

                // Designation 1           Designation  2                                       Designation 3
                // Vertical Head           Product Manager                                    NULL
                // NS                            Product                                                    NULL



                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    //if none of the above "if or else if" conditions ran, then "dept2" has NULL, so set it to "Product"
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }
                    //if "dept2" has already been assigned, then make "dept3" as "Product"

                    //Scenario

                    // Designation 1           Designation  2                                                          Designation 3
                    // Vertical Head           Product Manager                                                  Product Manager
                    // NS                            Product Selling/Rental                                                    Product

                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh pm p");
                }
                // dd($designation1,$designation2,$designation3);

            }

            //if any of the designation has "Pharmacy Manager"
            if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
            {
                $roleid=15;

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         NULL
                // NS                               Product                                                                 NULL


                if($design2==NULL)
                {
                    $design2 = "Pharmacy Manager";
                    $dept2 = "Product";
                }

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         Product Manager
                // NS                               Product                                                                 Product/Product - Selling/Product - Rental
                else
                {
                    $design3 = "Pharmacy Manager";
                    $dept3 = "Product";

                }
                // dd("vh ph",$design2, $dept2);
                // dd($designation1,$designation2,$designation3);

            }
            //if a person has to be made both "Product and Pharmacy Manager"
            if($designation1=="Product and Pharmacy Manager" || $designation2=="Product and Pharmacy Manager" || $designation3=="Product and Pharmacy Manager")
            {
                // dd("vh ppm");
                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Product and Pharmacy Manager                                  NULL
                // NS                               Product                                                                 NULL

                if($design2==NULL)
                {
                    $design2 = "Product and Pharmacy Manager";


                }


                //Scenario

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           Pharmacy/Product Manager                                                         Product and Pharmacy Manager
                // NS                           Product/Product Selling/Product Rental                                        Product

                else
                {
                    $design3 = "Product and Pharmacy Manager";

                }


                //checking for department if both "product and pharmacy manager" are selected

                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Selling

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Selling";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Selling

                    else
                    {
                        $dept3 = "Product - Selling";
                    }
                    // dd("vh ppm s");
                }
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Rental

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Rental

                    else
                    {
                        $dept3 = "Product - Rental";

                    }
                    // dd("vh ppm r");
                }
                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }


                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product


                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh ppm p");


                }

            }
            else if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Officer
                // NS                           NULL                                                                                                  Product

                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Officer";
                    $dept2 = "Product";
                    // dd("vh fo");
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Officer
                // NS                           Product/Rental/Selling                                                                                             Product
                else
                {
                    $design3 = "Field Officer";
                    $dept3 = "Product";

                    // dd("vh",$design2," fo");
                }

            }
            else if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
            {

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Executive
                // NS                           NULL                                                                                                  Product
                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Executive";
                    $dept2 = "Product";

                    // dd("vh  fe");
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Executive
                // NS                           Product/Rental/Selling                                                                                             Product

                else
                {
                    $design3 = "Field Executive";
                    $dept3 = "Product";
                    // dd("vh ",$design2," fe");

                }
            }

            // dd($design1, );
            // dd("d1",$design1,"d2",$design2,"d3",$design3,"<br>","d11",$dept1,"d22",$dept2,"d33",$dept3 );

            // ******* Important note  ******
            // We have ensured that all Coordinators and Vertical Heads use the variables "design1" and "dept1"
            // All Product Managers and Pharmacy Managers use the variables "design2" and "dept2"
            // All Field Officers and Field executives use the variables "design3" and "dept3"

            //Since we have defined the designations for each field we are checking all possible scenarios of matching them

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           NULL                                                                                                                 NULL
            // NS                           NULL                                                                                                                    NULL

            if($design1=="Coordinator" && $design2==NULL && $design3==NULL)
            {
                // the role id is for "Coordinator" only
                $roleid = 6;
            }
            if($design1=="Coordinator" && $design2=="Product Manager" && $design3==NULL)
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                                 NULL
                // NS                           Product - Selling                                                                                                                    NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager with department Product Selling
                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    NULL
                // NS                           Product - Rental                                                                                                        NULL

                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager with department Product Rental

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                     NULL
                // NS                           Product                                                                                                                    NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager handling both Rental and Selling

                    $roleid = 21;
                }

            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                                                 NULL
            // NS                           Product                                                                                                                    NULL

            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product"

                $roleid = 22;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                              NULL
            // NS                           Product                                                                                                                    NULL
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Coordinator and (Product and Pharmacy Manager) handling department "Product"

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }



            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Field Executive")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product - Selling                                                                                                    Null
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Selling" and Field Executive
                    //this get combined as Product Manager for selling only

                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product - Rental                                                                                                    NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Rental" and Field Executive
                    //this get combined as Product Manager for rental only

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                                     NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Executive
                    //this get combined as Product Manager for Product only

                    $roleid = 21;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Selling                                                                                                         Product

                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Selling" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Rental                                                                                                         Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Rental" and Field Executive with department "Product"
                    //this get combined as Product Manager for Rental only

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                         Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only
                    $roleid = 21;
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                                               Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Coordinator and (Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                $roleid = 22;
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and  Pharmacy Manager                                                                  Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Coordinator and (Product and Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }


            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Selling                                                                                                       NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product  Manager) handling department "Product - Selling" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 23;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Rental                                                                                                       NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager ) handling department "Product - Rental" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 24;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                     NULL
                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product " and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 21;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product - Selling                                                                                                    Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product Selling" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 23;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product - Rental                                                                                                    Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product Rental" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Rental only
                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                 Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 21;
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Pharmacy Manager                                                                                                   Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Field Officer")
            {

                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Pharmacy Manager for Product only
                $roleid = 22;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and Pharmacy Manager                                                                         Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Field Officer")
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Product Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

             if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Pharmacy Manager")
            {
                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Product Manager")
            {

                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }


            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Pharmacy Manager")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Selling                                                                                                                 Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and Product Manager handling department "Product - Selling" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Selling only

                    $roleid = 26;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Rental                                                                                                                 Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {

                    // the role id is for Coordinator and Product Manager handling department "Product - Rental" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Rental only
                    $roleid = 27;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product                                                                                                              Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and Product Manager handling department "Product" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product only
                    $roleid = 25;
                }
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Field Officer                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Coordinator" && $design2=="Field Officer")
            {
                // the role id is for Coordinator and Field Officer handling department "Product

                $roleid = 30;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Field Executive                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Coordinator" && $design2=="Field Executive")
            {
                // the role id is for Coordinator and Field Executive handling department "Product

                $roleid = 31;
            }

            /* if loop for checking Role as Vertical, Product Selling Manager and Pharmacy manager ends here */


 // putting all Product related designations in column "Designation2" and all Field related designations in "Designation3"
            // dd($dept1);
        $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation2=$design2;
        $employee->Designation3=$design3;

        $employee->Department=$dept1;
        $employee->Department2=$dept2;
        $employee->Department3=$dept3;
        $employee->save();


          if($design2=="Field Officer" || $design2=="Field Executive")
          {
            $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation3=$design2;

        $employee->Department=$dept1;
        $employee->Department3=$dept2;
        $employee->save();
          }
        }

        else if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
        {
            $roleid=9;
            $design1 = "Product Manager";

            //checking for department if both "product and pharmacy manager" are selected

            if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
            {
                if($dept1 == NULL)
                {
                    $dept1 = "Product - Selling";
                }
                else
                {
                    $dept2 = "Product - Selling";
                }
            }
            else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
            {
                if($dept2 == NULL)
                {
                    $dept2 = "Product - Rental";
                }
                else
                {
                    $dept3 = "Product - Rental";

                }
            }
            else if($department=="Product" || $department2=="Product" || $department3=="Product")
            {
                if($dept2 == NULL)
                {
                    $dept2 = "Product";
                }
                else
                {
                    $dept3 = "Product";

                }
            }

               $employee = employee::find($id);
             $employee->Designation=NULL;
             $employee->Designation2="Product Manager";

             if($dept1=="Product - Selling" && $dept2=="Product - Rental")
             {
             $employee->Department2="Product";
           }
           elseif($dept1=="Product - Selling" && $dept2=="")
             {
             $employee->Department2="Product - Selling";
           }
            elseif($dept1=="Product - Rental" && $dept2=="")
             {
             $employee->Department2="Product - Rental";
           }
            elseif($dept1=="Product" && $dept2=="")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product - Selling")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product - Rental")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product - Selling" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product - Rental" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
           elseif($dept1==null && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept2==null && $dept3=="Product")
             {
             $employee->Department2="Product";
           }
              $employee->save();


        }else
        if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
        {
            $desig1 ="Pharmacy Manager";
            $dept1 = "Product";
            $roleid=10;

               $employee = employee::find($id);
             $employee->Designation2="Pharmacy Manager";
             $employee->Department2="Product";
              $employee->save();

        }else
        if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
        {

            $desig1 ="Field Officer";
            $dept1 = "Product";
            $roleid=12;

               $employee = employee::find($id);
             $employee->Designation3="Field Officer";
             $employee->Department3="Product";

              $employee->save();
        }else
        if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
        {
            $desig1 ="Field Executive";
            $dept1 = "Product";
            $roleid=11;
             $employee = employee::find($id);
             $employee->Designation3="Field Executive";
             $employee->Department3="Product";
              $employee->save();
        }
        else
        if($designation1=="Marketing" || $designation2=="Marketing" || $designation3=="Marketing")
        {
            $desig1 ="Marketing";
            $dept1 = "Marketing";
            $roleid=13;

             $employee = employee::find($id);
             $employee->Designation="Marketing";
              $employee->save();

        }
        else
        if($designation1=="Customer Care" || $designation2=="Customer Care" || $designation3=="Customer Care")
        {
            $desig1 ="Customer Care";
            $dept1 = "Customer Care";
            $roleid=1;

             $employee = employee::find($id);
             $employee->Designation="Customer Care";
              $employee->save();
        }


        // dd($roleid);
        //in the 'admins' table check whether there is any entry for the above selected employee

        $check=DB::table('admins')->where('name',$empname)->value('id');
        //$check=DB::table('admins')->where('name',$refname)->value('email');

        /*
        If there is no existing row for employees for login in admin table
        then create a login for him/her
        */
        if($check == NULL & $empemail!=NULL)
        {
            $admin = new Admin;


            //in the admin table, create a new entry with the following fields
            $admin->name=$empname;
            $admin->email=$empemail;
            $admin->password=$emppassword;
            $admin->save();
            $adminid=DB::table('admins')->max('id');

            $role_admin = new role_admin;

            //also add a new entry in the role_admin table correspondingly -- since foreign keys are involved
            $role_admin->role_id=$roleid;
            $role_admin->admin_id=$adminid;
            $role_admin->save();
        }

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = $request->FirstName." added as an Employee";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->save();


        /* Code for Employee Login Creation ends here -- by Murtuza and jatin */


        return redirect('/employees');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return $id;
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $v=DB::table('employees')->where('Designation','Vertical Head')->orwhere('Designation3','Field Officer')->orwhere('Designation','Branch Head')->orwhere('Designation','Management')->orwhere('Designation2','Product Manager')->orwhere('Designation2','Pharmacy Manager')->orwhere('Designation','Branch Head')->get();
        $v1=DB::table('employees')->where('Designation','Vertical Head')->orwhere('Designation3','Field Officer')->orwhere('Designation','Branch Head')->orwhere('Designation','Management')->orwhere('Designation2','Product Manager')->orwhere('Designation2','Pharmacy Manager')->orwhere('Designation','Branch Head')->get();
        $city=DB::table('cities')->get();
        $language=DB::table('languages')->get();

        if(Auth::guard('admin')->check())
        {
            $employee = employee::find($id);
            $e=DB::table('employees')->where('id',$id)->value('under');
            $e1=DB::table('employees')->where('id',$id)->value('under1');
            $ename=DB::table('employees')->where('id',$e)->get();
            $ename1=DB::table('employees')->where('id',$e1)->get();

             $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {

                    return view('employee.edit',compact('employee','ename','v','city','e1','ename1','v1','language'));
                }
                else
                {
                    return redirect('/admin');
                }
        }
        else
        {
            return redirect('/admin');
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
     $designation11= $request->Designationchecked;
     $department11= $request->Departmentchecked;
     // dd($department11);
      $under11=$request->underchecked;

        $LangaugesKnown11=$request->LangaugesKnownchecked;


  $LangaugesKnown11 = str_replace(array('[',']' ,),' ',$LangaugesKnown11);
                $LangaugesKnown11 = preg_replace('/"/', '', $LangaugesKnown11);
                $LangaugesKnown11 = trim($LangaugesKnown11);



                $Designationcheckedfortrim= $request->Designationchecked;

                  //fetching values for all Designations and Department

        $designation1 = $designation2 = $designation3 = $department =$department2 =$department3 = $under1 = $under2 = $under3 = NULL;


        $design1 = $design2 = $design3 = $dept1 = $dept2 = $dept3 = NULL;
        $uname1=$uname2=NULL;


                // dd($languagefortrim);

                $designation11 = str_replace(array('[',']' ,),' ',$Designationcheckedfortrim);
                $designation11 = preg_replace('/"/', '', $designation11);
                $designation11 = trim($designation11);

                $designation11 = explode(",",$designation11);
                $designationcount= count($designation11);


                    for($i=0;$i<$designationcount;$i++)
                {
                    // $designation1=$designation11[$i];
                     if($designation1==NULL)
                    {
                    $designation1=$designation11[$i];
                    // dd($designation1);
                    }

                    else if($designation2==NULL)
                    {
                        $designation2=$designation11[$i];
                    }
                    else if($designation3==NULL)
                    {
                        $designation3=$designation11[$i];
                    }
                }


                    $Departmentcheckedfortrim= $request->Departmentchecked;
                // dd($Departmentcheckedfortrim);

                $department11 = str_replace(array('[',']' ,),' ',$Departmentcheckedfortrim);
                $department11 = preg_replace('/"/', '', $department11);
                // dd($department11);
                $department11 = trim($department11);
                 $department11 = explode(",",$department11);
                 // dd($department11);
                $departmentcount= count($department11);
// dd($departmentcount);

                   for($i=0;$i<$departmentcount;$i++)
                {
                    // $department1=$department11[$i];
                     if($department==NULL)
                    {
                    $department=$department11[$i];
                    }

                    else if($department2==NULL)
                    {
                        $department2=$department11[$i];
                    }
                    else if($department3==NULL)
                    {
                        $department3=$department11[$i];
                    }
                }

                $undercheckedfortrim= $request->underchecked;
                // dd($languagefortrim);

                $under11 = str_replace(array('[',']' ,),' ',$undercheckedfortrim);
                $under11 = preg_replace('/"/', '', $under11);
                $under11 = trim($under11);

                 $under11 = explode(",",$under11);
                $undercount= count($under11);


                        for($i=0;$i<$undercount;$i++)
                {
                    // $under1=$under11[$i];
                     $employeedesignation=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation');
                     $employeedesignation1=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation2');
                     $employeedesignation2=DB::table('employees')->where('FirstName',$under11[$i])->value('Designation3');
// dd($employeedesignation,$employeedesignation1,$employeedesignation2);

if(($employeedesignation==Null && $employeedesignation2=="Field Officer") || ($employeedesignation!="Field Officer" && $employeedesignation2=="Field Officer") || ($employeedesignation1!="Field Officer" && $employeedesignation2=="Field Officer"))
{


            $uname2=$under11[$i];

            $employee_id2=DB::table('employees')->where('FirstName',$uname2)->value('id');

              $under2=$employee_id2;



}
else
{

        $uname1=$under11[$i];

            $employee_id1=DB::table('employees')->where('FirstName',$uname1)->value('id');

              $under1=$employee_id1;



                }
               }




        // dd($request->AlternateNumber);
        // $uname=$request->under;
        // $uid=DB::table('employees')->where('FirstName',$uname)->value('id');
        //  $uname1=$request->under1;
        // $uid1=DB::table('employees')->where('FirstName',$uname1)->value('id');

        $checkname=DB::table('employees')->where('id',$id)->value('FirstName');
        $checkadminid=DB::table('admins')->where('name',$checkname)->value('id');

        if($checkname!=$request->FirstName)
        {
            $admin = admin::find($checkadminid);

            $admin->name=$request->FirstName;
            $admin->save();
        }

// dd($designation1,$designation2,$designation3,$department,$department2,$department3,$under1,$under2,$under3);



        $reg_id=$request->Regid;

$EmailID=$request->Email_id;

$check_Reg_id=DB::table('employees')->where('Regid',$reg_id)->value('id');

$check_email=DB::table('employees')->where('Email_id',$EmailID)->value('id');

$check_email_count=DB::table('employees')->where('Email_id',$EmailID)->count();
// dd($check_email_count);
if($check_email_count > 1)
{
    session()->flash('message','Email ID already in use');
    return redirect('/employees');
}



        $employee = employee::find($id);

        $employee->Regid=$request->Regid;
        $employee->FirstName=$request->FirstName;
        $employee->MiddleName=$request->MiddleName;
        $employee->LastName=$request->LastName;
        $employee->Email_id=$request->Email_id;
        $employee->DOB=$request->DOB;
        $employee->Gender=$request->Gender;
        $employee->MartialStatus=$request->MartialStatus;
        $employee->BloodGroup=$request->BloodGroup;
        $employee->DOJ=$request->DOJ;
        $employee->EmployementType=$request->EmployementType;
        // $employee->Designation=$designation1;
        // $employee->Designation2=$designation2;
        // $employee->Designation3=$designation3;
        $employee->MobileNumber=$request->MobileNumber;
        $employee->AlternateNumber=$request->AlternateNumber;
        // $employee->Department=$department;
        // $employee->Department2=$department2;
        // $employee->Department3=$department3;
        $employee->under=$under1;
        $employee->under1=$under2;
        $employee->underwhom=$uname1;
        $employee->ReportingTo=$uname2;
        $employee->city=$request->city;
        $employee->LangaugesKnown=$LangaugesKnown11;

        /* Code for employees Log starts here */

        //this will retrieve all the fields and values that are updated in the form of an array
        $emp_c = $employee->getDirty();

        //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
        foreach($emp_c as $key=>$value)
        {
            $empc[] = $key;

        }

        foreach($emp_c as $key=>$value)
        {
            $empv[] = $value;
        }

        //this is for converting array values into a string with new line

        //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
        if(empty($empc))
        {
            $empd = " ";
            $empf = " ";
            $empd = trim($empd);
            $empf = trim($empf);
        }
        else
        {
            $empd = implode("\r\n", $empc);
            $empf = implode("\r\n", $empv);
        }



        /* Code for employees Log ends here */

        $employee->save();

        //To understand this code refer to comments for same code in store function of this Controller  -- by jatin
        $employee=DB::table('employees')->max('id');

        /*
        to verify whether there is any existing data for that user in the admins table
        */
        $empname=$request->FirstName;
        $empemail=$request->Email_id;
        $emppassword=bcrypt('Healthheal@123');


        $checkid=DB::table('admins')->where('name',$empname)->value('id');
        $check=DB::table('admins')->where('name',$empname)->value('email');





        /* Checking which of the designations is Higher and assigning the corresponding role starts here Eg - Admin and Vertical head -- by jatin*/

        // Checking if any of the designations selected by the user has "Admin" in them. In this case, since "Admin" will be highest of the two (Except in case of "Management and Branch Head")
        //the "Admin" role shall be assigned by default -- by jatin
        if($designation1=="Admin" || $designation2=="Admin" || $designation3=="Admin")
        {
            $desig1 ="Admin";
            $roleid=3;


             $employee = employee::find($id);
             $employee->Designation="Admin";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();

            // dd("Admin");

        }
        // Checking if any of the designations selected by the user has "Management" in them. In this case, since "Management" will be highest of the two (Except in case of "Admin and Branch Head")
        //the "Management" role shall be assigned by default -- by jatin
        else if($designation1=="Management" || $designation2=="Management" || $designation3=="Management")
        {
            $desig1 ="Management";
            $roleid=5;


             $employee = employee::find($id);
             $employee->Designation="Management";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();

            // dd("management");

        }
        // Checking if any of the designations selected by the user has "Branch Head" in them. In this case, since "Branch Head" will be highest of the two (Except in case of "Admin and Branch Head")
        //the "Branch Head" role shall be assigned by default -- by jatin
        else if($designation1=="Branch Head" || $designation2=="Branch Head" || $designation3=="Branch Head")
        {

            $desig1 ="Branch Head";
            $roleid=32;

             $employee = employee::find($id);
             $employee->Designation="Branch Head";
             $employee->Designation2=NULL;
             $employee->Designation3=NULL;
             $employee->Department=" ";
             $employee->Department2=NULL;
             $employee->Department3=NULL;
              $employee->save();            // dd("BH");

        }
        // Checking if any one of the designations selected by the user has "Vertical Head" in them -- by jatin
        else if($designation1=="Vertical Head" || $designation2=="Vertical Head" || $designation3=="Vertical Head")
        {

            // Scenario -- by jatin

            // Designation 1           Designation  2                               Designation 3
            // Vertical Head            NULL                                               NULL
            // NS                             NULL                                               NULL


            // fixing the designation place value to first position
            $design1="Vertical Head";
            $roleid=2;

            // Now that the designation has been set we check which services is provided in the department -- by jatin
            if($department == "Nursing Services" || $department2 == "Nursing Services" || $department3 == "Nursing Services" )
            {
                //fixing the place value for service selected to first position
                $dept1 = "Nursing Services";
            }
            else if($department == "Mathrutvam - Baby Care" || $department2 == "Mathrutvam - Baby Care" || $department3 == "Mathrutvam - Baby Care" )
            {
                $dept1 = "Mathrutvam - Baby Care";
            }
            else if($department == "Personal Supportive Care" || $department2 == "Personal Supportive Care" || $department3 == "Personal Supportive Care" )
            {
                $dept1 = "Personal Supportive Care";
            }
            else if($department == "Physiotherapy - Home" || $department2 == "Physiotherapy - Home" || $department3 == "Physiotherapy - Home" )
            {
                $dept1 = "Physiotherapy - Home";
            }
            else if($department == "All" || $department2 == "All" || $department3 == "All" )
            {
                $dept1 = "All";
            }




            /* if loop for checking Role as Vertical, Product Selling Manager and /or Pharmacy manager starts here -- by jatin */

            //if Product Manager comes at any place, then fix its position to second variable for designation
            if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
            {
                $design2 = "Product Manager";


                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {

                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept1"
                    //assign "Product  - Selling" here

                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                               Designation 3
                    // Product Manager           Vertical Head                              NULL
                    // Product Selling              NS                                               NULL



                    if($dept1 == NULL)
                    {
                        $dept1 = "Product - Selling";
                    }
                    //if "dept1" has already been assigned in the first place say with Vertical Head. In that case, we need to assign "dept2" with "product selling"

                    //Scenario

                    // Designation 1           Designation  2                                    Designation 3
                    // Vertical Head           Product Manager                                      NULL
                    // Nursing Services      Product - Selling                                      NULL

                    else
                    {
                        $dept2 = "Product - Selling";

                    }
                    // dd("vh pm s123");

                }
                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept2"
                    //assign "Product  - Rental" here

                    //Scenario

                    // Designation 1           Designation  2                      Designation 3
                    // Vertical Head           Product Manager                                    NULL
                    // NS                            Product Rental                                     NULL


                    // till this point the value of dept2 is NULL, because above "if" condition didn't run
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    //Scenario

                    // Designation 1           Designation  2                                       Designation 3
                    // Vertical Head           Product Manager                                    Product Manager
                    // NS                            Product Rental                                       Product Selling



                    //this means that the above "if loop for Product Selling has already run", so we assign "dept3" as "Product Rental"
                    else
                    {
                        $dept3 = "Product - Rental";

                        // Since the person is Product manager for both departments, we make it into a single department "product"
                        if($dept2=="Product - Selling" && $dept3=="Product - Rental")
                        {
                            //make the "dept2" as "product" and make the "dept3" as "NULL"
                            $dept2="Product";
                            $dept3=NULL;
                        }
                    }
                    // dd("vh pm r");
                }

                //Scenario

                // Designation 1           Designation  2                                       Designation 3
                // Vertical Head           Product Manager                                    NULL
                // NS                            Product                                                    NULL



                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    //if none of the above "if or else if" conditions ran, then "dept2" has NULL, so set it to "Product"
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }
                    //if "dept2" has already been assigned, then make "dept3" as "Product"

                    //Scenario

                    // Designation 1           Designation  2                                                          Designation 3
                    // Vertical Head           Product Manager                                                  Product Manager
                    // NS                            Product Selling/Rental                                                    Product

                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh pm p");
                }
                // dd($designation1,$designation2,$designation3);

            }

            //if any of the designation has "Pharmacy Manager"
            if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
            {
                $roleid=15;

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         NULL
                // NS                               Product                                                                 NULL


                if($design2==NULL)
                {
                    $design2 = "Pharmacy Manager";
                    $dept2 = "Product";
                }

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         Product Manager
                // NS                               Product                                                                 Product/Product - Selling/Product - Rental
                else
                {
                    $design3 = "Pharmacy Manager";
                    $dept3 = "Product";

                }
                // dd("vh ph",$design2, $dept2);
                // dd($designation1,$designation2,$designation3);

            }
            //if a person has to be made both "Product and Pharmacy Manager"
            if($designation1=="Product and Pharmacy Manager" || $designation2=="Product and Pharmacy Manager" || $designation3=="Product and Pharmacy Manager")
            {
                // dd("vh ppm");
                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Product and Pharmacy Manager                                  NULL
                // NS                               Product                                                                 NULL

                if($design2==NULL)
                {
                    $design2 = "Product and Pharmacy Manager";


                }


                //Scenario

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           Pharmacy/Product Manager                                                         Product and Pharmacy Manager
                // NS                           Product/Product Selling/Product Rental                                        Product

                else
                {
                    $design3 = "Product and Pharmacy Manager";

                }


                //checking for department if both "product and pharmacy manager" are selected

                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Selling

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Selling";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Selling

                    else
                    {
                        $dept3 = "Product - Selling";
                    }
                    // dd("vh ppm s");
                }
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Rental

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Rental

                    else
                    {
                        $dept3 = "Product - Rental";

                    }
                    // dd("vh ppm r");
                }
                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }


                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product


                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh ppm p");


                }

            }
            else if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Officer
                // NS                           NULL                                                                                                  Product

                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Officer";
                    $dept2 = "Product";
                    // dd("vh fo");
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Officer
                // NS                           Product/Rental/Selling                                                                                             Product
                else
                {
                    $design3 = "Field Officer";
                    $dept3 = "Product";

                    // dd("vh",$design2," fo");
                }

            }
            else if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
            {

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Executive
                // NS                           NULL                                                                                                  Product
                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Executive";
                    $dept2 = "Product";

                    // dd("vh  fe");
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Executive
                // NS                           Product/Rental/Selling                                                                                             Product

                else
                {
                    $design3 = "Field Executive";
                    $dept3 = "Product";
                    // dd("vh ",$design2," fe");

                }
            }

            // dd($design1, );
            // dd("d1",$design1,"d2",$design2,"d3",$design3,"<br>","d11",$dept1,"d22",$dept2,"d33",$dept3 );

            // ******* Important note  ******
            // We have ensured that all Coordinators and Vertical Heads use the variables "design1" and "dept1"
            // All Product Managers and Pharmacy Managers use the variables "design2" and "dept2"
            // All Field Officers and Field executives use the variables "design3" and "dept3"

            //Since we have defined the designations for each field we are checking all possible scenarios of matching them

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           NULL                                                                                                                 NULL
            // NS                           NULL                                                                                                                    NULL

            if($design1=="Vertical Head" && $design2==NULL && $design3==NULL)
            {
                // the role id is for "Vertical Head" only
                $roleid = 2;
            }
            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3==NULL)
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                                 NULL
                // NS                           Product - Selling                                                                                                                    NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager with department Product Selling
                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    NULL
                // NS                           Product - Rental                                                                                                        NULL

                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager with department Product Rental

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                     NULL
                // NS                           Product                                                                                                                    NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and Product Manager handling both Rental and Selling

                    $roleid = 14;
                }

            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                                                 NULL
            // NS                           Product                                                                                                                    NULL

            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product"

                $roleid = 15;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                              NULL
            // NS                           Product                                                                                                                    NULL
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Vertical Head and (Product and Pharmacy Manager) handling department "Product"

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }



            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Field Executive")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product - Selling                                                                                                    Null
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Selling" and Field Executive
                    //this get combined as Product Manager for selling only

                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product - Rental                                                                                                    NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Rental" and Field Executive
                    //this get combined as Product Manager for rental only

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                                     NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Executive
                    //this get combined as Product Manager for Product only

                    $roleid = 14;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Selling                                                                                                         Product

                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Selling" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 16;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Rental                                                                                                         Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product - Rental" and Field Executive with department "Product"
                    //this get combined as Product Manager for Rental only

                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                         Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only
                    $roleid = 14;
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                                               Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Vertical Head and (Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                $roleid = 15;
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and  Pharmacy Manager                                                                  Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Vertical Head and (Product and Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }


            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Selling                                                                                                       NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product  Manager) handling department "Product - Selling" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 16;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Rental                                                                                                       NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager ) handling department "Product - Rental" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 17;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                     NULL
                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product " and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 14;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product - Selling                                                                                                    Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product Selling" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 16;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product - Rental                                                                                                    Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product Rental" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Rental only
                    $roleid = 17;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                 Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and (Product Manager) handling department "Product" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 14;
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Pharmacy Manager                                                                                                   Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Field Officer")
            {

                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Pharmacy Manager for Product only
                $roleid = 15;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and Pharmacy Manager                                                                         Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Field Officer")
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head          Product and Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Vertical Head" && $design2=="Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

             if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Pharmacy Manager")
            {
                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Vertical Head" && $design2=="Product and Pharmacy Manager" && $design3=="Product Manager")
            {

                // the role id is for Vertical Head and Product and Pharmacy Manager handling department "Product" and Product Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 19;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 20;
                    }
                    else
                    {
                        $roleid = 18;
                    }
                }
            }


            if($design1=="Vertical Head" && $design2=="Product Manager" && $design3=="Pharmacy Manager")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Selling                                                                                                                 Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and Product Manager handling department "Product - Selling" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Selling only

                    $roleid = 19;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Rental                                                                                                                 Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {

                    // the role id is for Vertical Head and Product Manager handling department "Product - Rental" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Rental only
                    $roleid = 20;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product                                                                                                              Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Vertical Head and Product Manager handling department "Product" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product only
                    $roleid = 18;
                }
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Field Officer                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Vertical Head" && $design2=="Field Officer")
            {
                // the role id is for Vertical Head and Field Officer handling department "Product

                $roleid = 28;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           Field Executive                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Vertical Head" && $design2=="Field Executive")
            {
                // the role id is for Vertical Head and Field Executive handling department "Product

                $roleid = 29;
            }

            /* if loop for checking Role as Vertical, Product Selling Manager and Pharmacy manager ends here */


 // putting all Product related designations in column "Designation2" and all Field related designations in "Designation3"
            // dd($dept1);
        $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation2=$design2;
        $employee->Designation3=$design3;

        $employee->Department=$dept1;
        $employee->Department2=$dept2;
        $employee->Department3=$dept3;
        $employee->save();


          if($design2=="Field Officer" || $design2=="Field Executive")
          {
            $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation2=NULL;
        $employee->Designation3=$design2;

        $employee->Department=$dept1;
        $employee->Department2=NULL;
        $employee->Department3=$dept2;
        $employee->save();
          }

        }

        //coordinator logic
        //refer to comments for Coordinator to understand what is happening here
        else if($designation1=="Coordinator" || $designation2=="Coordinator" || $designation3=="Coordinator")
        {
                      $design1="Coordinator";
            $roleid=6;


              // Now that the designation has been set we check which services is provided in the department -- by jatin
            if($department == "Nursing Services" || $department2 == "Nursing Services" || $department3 == "Nursing Services" )
            {
                //fixing the place value for service selected to first position
                $dept1 = "Nursing Services";
            }
            else if($department == "Mathrutvam - Baby Care" || $department2 == "Mathrutvam - Baby Care" || $department3 == "Mathrutvam - Baby Care" )
            {
                $dept1 = "Mathrutvam - Baby Care";
            }
            else if($department == "Personal Supportive Care" || $department2 == "Personal Supportive Care" || $department3 == "Personal Supportive Care" )
            {
                $dept1 = "Personal Supportive Care";
            }
            else if($department == "Physiotherapy - Home" || $department2 == "Physiotherapy - Home" || $department3 == "Physiotherapy - Home" )
            {
                $dept1 = "Physiotherapy - Home";
            }
            else if($department == "All" || $department2 == "All" || $department3 == "All" )
            {
                $dept1 = "All";
            }




            /* if loop for checking Role as Vertical, Product Selling Manager and /or Pharmacy manager starts here -- by jatin */

            //if Product Manager comes at any place, then fix its position to second variable for designation
            if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
            {
                $design2 = "Product Manager";


                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {

                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept1"
                    //assign "Product  - Selling" here

                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                               Designation 3
                    // Product Manager           Vertical Head                              NULL
                    // Product Selling              NS                                               NULL



                    if($dept1 == NULL)
                    {
                        $dept1 = "Product - Selling";
                    }
                    //if "dept1" has already been assigned in the first place say with Vertical Head. In that case, we need to assign "dept2" with "product selling"

                    //Scenario

                    // Designation 1           Designation  2                                    Designation 3
                    // Vertical Head           Product Manager                                      NULL
                    // Nursing Services      Product - Selling                                      NULL

                    else
                    {
                        $dept2 = "Product - Selling";

                    }
                    // dd("vh pm s123");

                }
                //If product manager is selected as designation, then we check for the department and fix the position of these as well
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    //assume nothing was selected in the above "Vertical Head" if - else. In that case, no value is present in "dept2"
                    //assign "Product  - Rental" here

                    //Scenario

                    // Designation 1           Designation  2                      Designation 3
                    // Vertical Head           Product Manager                                    NULL
                    // NS                            Product Rental                                     NULL


                    // till this point the value of dept2 is NULL, because above "if" condition didn't run
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    //Scenario

                    // Designation 1           Designation  2                                       Designation 3
                    // Vertical Head           Product Manager                                    Product Manager
                    // NS                            Product Rental                                       Product Selling



                    //this means that the above "if loop for Product Selling has already run", so we assign "dept3" as "Product Rental"
                    else
                    {
                        $dept3 = "Product - Rental";

                        // Since the person is Product manager for both departments, we make it into a single department "product"
                        if($dept2=="Product - Selling" && $dept3=="Product - Rental")
                        {
                            //make the "dept2" as "product" and make the "dept3" as "NULL"
                            $dept2="Product";
                            $dept3=NULL;
                        }
                    }
                    // dd("vh pm r");
                }

                //Scenario

                // Designation 1           Designation  2                                       Designation 3
                // Vertical Head           Product Manager                                    NULL
                // NS                            Product                                                    NULL



                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    //if none of the above "if or else if" conditions ran, then "dept2" has NULL, so set it to "Product"
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }
                    //if "dept2" has already been assigned, then make "dept3" as "Product"

                    //Scenario

                    // Designation 1           Designation  2                                                          Designation 3
                    // Vertical Head           Product Manager                                                  Product Manager
                    // NS                            Product Selling/Rental                                                    Product

                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh pm p");
                }
                // dd($designation1,$designation2,$designation3);

            }

            //if any of the designation has "Pharmacy Manager"
            if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
            {


                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         NULL
                // NS                               Product                                                                 NULL


                if($design2==NULL)
                {
                    $design2 = "Pharmacy Manager";
                    $dept2 = "Product";
                }

                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Pharmacy Manager                                                         Product Manager
                // NS                               Product                                                                 Product/Product - Selling/Product - Rental
                else
                {
                    $design3 = "Pharmacy Manager";
                    $dept3 = "Product";

                }
                // dd("vh ph",$design2, $dept2);
                // dd($designation1,$designation2,$designation3);

            }
            //if a person has to be made both "Product and Pharmacy Manager"
            if($designation1=="Product and Pharmacy Manager" || $designation2=="Product and Pharmacy Manager" || $designation3=="Product and Pharmacy Manager")
            {
                // dd("vh ppm");
                //Scenario

                // Designation 1           Designation  2                                                          Designation 3
                // Vertical Head           Product and Pharmacy Manager                                  NULL
                // NS                               Product                                                                 NULL

                if($design2==NULL)
                {
                    $design2 = "Product and Pharmacy Manager";


                }


                //Scenario

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           Pharmacy/Product Manager                                                         Product and Pharmacy Manager
                // NS                           Product/Product Selling/Product Rental                                        Product

                else
                {
                    $design3 = "Product and Pharmacy Manager";

                }


                //checking for department if both "product and pharmacy manager" are selected

                if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Selling

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Selling";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Selling

                    else
                    {
                        $dept3 = "Product - Selling";
                    }
                    // dd("vh ppm s");
                }
                else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
                {
                    // ASK HERE
                    //Scenario

                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product - Rental

                    if($dept2 == NULL)
                    {
                        $dept2 = "Product - Rental";
                    }

                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product - Rental

                    else
                    {
                        $dept3 = "Product - Rental";

                    }
                    // dd("vh ppm r");
                }
                else if($department=="Product" || $department2=="Product" || $department3=="Product")
                {
                    // Designation 1           Designation  2                                                                              Designation 3
                    // Vertical Head           NULL                                                                          Product and Pharmacy Manager
                    // NS                           NULL                                                                                    Product
                    if($dept2 == NULL)
                    {
                        $dept2 = "Product";
                    }


                    // Designation 1           Designation  2                                                                                              Designation 3
                    // Vertical Head           Product / Pharmacy Manager                                                                    Product and Pharmacy Manager
                    // NS                   Product/Product - Rental/Product - Selling                                                                 Product


                    else
                    {
                        $dept3 = "Product";

                    }
                    // dd("vh ppm p");


                }

            }
            else if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Officer
                // NS                           NULL                                                                                                  Product

                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Officer";
                    $dept2 = "Product";
                    // dd("vh fo");
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Officer
                // NS                           Product/Rental/Selling                                                                                             Product
                else
                {
                    $design3 = "Field Officer";
                    $dept3 = "Product";

                    // dd("vh",$design2," fo");
                }

            }
            else if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
            {

                // Designation 1           Designation  2                                                                              Designation 3
                // Vertical Head           NULL                                                                                         Field Executive
                // NS                           NULL                                                                                                  Product
                if($design2==NULL && $dept2==NULL)
                {
                    $design2 = "Field Executive";
                    $dept2 = "Product";

                    // dd("vh  fe");
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Vertical Head           Product Manager                                                                                          Field Executive
                // NS                           Product/Rental/Selling                                                                                             Product

                else
                {
                    $design3 = "Field Executive";
                    $dept3 = "Product";
                    // dd("vh ",$design2," fe");

                }
            }

            // dd($design1, );
            // dd("d1",$design1,"d2",$design2,"d3",$design3,"<br>","d11",$dept1,"d22",$dept2,"d33",$dept3 );

            // ******* Important note  ******
            // We have ensured that all Coordinators and Vertical Heads use the variables "design1" and "dept1"
            // All Product Managers and Pharmacy Managers use the variables "design2" and "dept2"
            // All Field Officers and Field executives use the variables "design3" and "dept3"

            //Since we have defined the designations for each field we are checking all possible scenarios of matching them

            // Designation 1           Designation  2                                                                                               Designation 3
            // Vertical Head           NULL                                                                                                                 NULL
            // NS                           NULL                                                                                                                    NULL

            if($design1=="Coordinator" && $design2==NULL && $design3==NULL)
            {
                // the role id is for "Coordinator" only
                $roleid = 6;
            }
            if($design1=="Coordinator" && $design2=="Product Manager" && $design3==NULL)
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                                 NULL
                // NS                           Product - Selling                                                                                                                    NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager with department Product Selling
                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    NULL
                // NS                           Product - Rental                                                                                                        NULL

                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager with department Product Rental

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                     NULL
                // NS                           Product                                                                                                                    NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and Product Manager handling both Rental and Selling

                    $roleid = 21;
                }

            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                                                 NULL
            // NS                           Product                                                                                                                    NULL

            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product"

                $roleid = 22;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                              NULL
            // NS                           Product                                                                                                                    NULL
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3==NULL)
            {
                // the role id is for Coordinator and (Product and Pharmacy Manager) handling department "Product"

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {

                        $roleid = 25;
                    }
                }
            }



            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Field Executive")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product - Selling                                                                                                    Null
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Selling" and Field Executive
                    //this get combined as Product Manager for selling only

                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product - Rental                                                                                                    NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Rental" and Field Executive
                    //this get combined as Product Manager for rental only

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                                     NULL

                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Executive
                    //this get combined as Product Manager for Product only

                    $roleid = 21;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Selling                                                                                                         Product

                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Selling" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 23;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product    - Rental                                                                                                         Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product - Rental" and Field Executive with department "Product"
                    //this get combined as Product Manager for Rental only

                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product Manager                                                                                                    Field Executive
                // NS                           Product                                                                                                         Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Executive with department "Product"
                    //this get combined as Product Manager for Product only
                    $roleid = 21;
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                                               Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Coordinator and (Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                $roleid = 22;
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and  Pharmacy Manager                                                                  Field Executive
            // NS                           Product                                                                                                         Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Field Executive")
            {
                // the role id is for Coordinator and (Product and Pharmacy Manager) handling department "Product" and Field Executive with department "Product"
                //this get combined as Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }


            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Field Officer")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Selling                                                                                                       NULL
                if($dept2=="Product - Selling" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product  Manager) handling department "Product - Selling" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 23;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product  - Rental                                                                                                       NULL
                if($dept2=="Product - Rental" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager ) handling department "Product - Rental" and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 24;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                     NULL
                if($dept2=="Product" && $dept3==NULL)
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product " and Field Officer with department "NULL"
                    //this get combined as Product Manager for Product - Rental only

                    $roleid = 21;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product - Selling                                                                                                    Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product Selling" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Selling only

                    $roleid = 23;
                }
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product - Rental                                                                                                    Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product Rental" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product - Rental only
                    $roleid = 24;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator          Product Manager                                                                                                   Field Officer
                // NS                           Product                                                                                                                 Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and (Product Manager) handling department "Product" and Field Officer with department "Product"
                    //this get combined as Product Manager for Product only

                    $roleid = 21;
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Pharmacy Manager                                                                                                   Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Field Officer")
            {

                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Pharmacy Manager for Product only
                $roleid = 22;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and Pharmacy Manager                                                                         Field Officer
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Field Officer")
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Field Officer with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator          Product and Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Pharmacy Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product
            if($design1=="Coordinator" && $design2=="Pharmacy Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product Manager                                                                         Product and Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Product and Pharmacy Manager")
            {
                // the role id is for Coordinator and Product Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

             if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Pharmacy Manager")
            {
                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product and Pharmacy Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only
                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }


            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Product and Pharmacy Manager                                                                         Pharmacy Manager
            // NS                           Product                                                                                                                 Product

            if($design1=="Coordinator" && $design2=="Product and Pharmacy Manager" && $design3=="Product Manager")
            {

                // the role id is for Coordinator and Product and Pharmacy Manager handling department "Product" and Product Manager with department "Product"
                //this get combined as Product and Pharmacy Manager for Product only

                 if($dept2=="Product - Selling")
                {
                        $roleid = 26;
                }
                else
                {
                    if($dept2=="Product - Rental")
                    {
                        $roleid = 27;
                    }
                    else
                    {
                        $roleid = 25;
                    }
                }
            }


            if($design1=="Coordinator" && $design2=="Product Manager" && $design3=="Pharmacy Manager")
            {
                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Selling                                                                                                                 Product
                if($dept2=="Product - Selling" && $dept3=="Product")
                {
                    // the role id is for Coordinator and Product Manager handling department "Product - Selling" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Selling only

                    $roleid = 26;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product Rental                                                                                                                 Product
                if($dept2=="Product - Rental" && $dept3=="Product")
                {

                    // the role id is for Coordinator and Product Manager handling department "Product - Rental" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product - Rental only
                    $roleid = 27;
                }

                // Designation 1           Designation  2                                                                                               Designation 3
                // Coordinator           Product  Manager                                                                                         Pharmacy Manager
                // NS                           Product                                                                                                              Product
                if($dept2=="Product" && $dept3=="Product")
                {
                    // the role id is for Coordinator and Product Manager handling department "Product" and Pharmacy Manager with department "Product"
                    //this get combined as Product and Pharmacy Manager for Product only
                    $roleid = 25;
                }
            }
            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Field Officer                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Coordinator" && $design2=="Field Officer")
            {
                // the role id is for Coordinator and Field Officer handling department "Product

                $roleid = 30;
            }

            // Designation 1           Designation  2                                                                                               Designation 3
            // Coordinator           Field Executive                                                                                                        NULL
            // NS                           Product                                                                                                             NULL
            if($design1=="Coordinator" && $design2=="Field Executive")
            {
                // the role id is for Coordinator and Field Executive handling department "Product

                $roleid = 31;
            }

            /* if loop for checking Role as Vertical, Product Selling Manager and Pharmacy manager ends here */


 // putting all Product related designations in column "Designation2" and all Field related designations in "Designation3"
            // dd($dept1);



        $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation2=$design2;
        $employee->Designation3=$design3;

        $employee->Department=$dept1;
        $employee->Department2=$dept2;
        $employee->Department3=$dept3;
        $employee->save();


          if($design2=="Field Officer" || $design2=="Field Executive")
          {
            $employee = employee::find($id);

        $employee->Designation=$design1;
        $employee->Designation3=$design2;

        $employee->Department=$dept1;
        $employee->Department3=$dept2;
        $employee->save();
          }
        }

        else if($designation1=="Product Manager" || $designation2=="Product Manager" || $designation3=="Product Manager")
        {
            $roleid=9;
            $design1 = "Product Manager";

            //checking for department if both "product and pharmacy manager" are selected

            if($department=="Product - Selling" || $department2=="Product - Selling" || $department3=="Product - Selling")
            {
                if($dept1 == NULL)
                {
                    $dept1 = "Product - Selling";
                }
                else
                {
                    $dept2 = "Product - Selling";
                }
            }
            else if($department=="Product - Rental" || $department2=="Product - Rental" || $department3=="Product - Rental")
            {
                if($dept2 == NULL)
                {
                    $dept2 = "Product - Rental";
                }
                else
                {
                    $dept3 = "Product - Rental";

                }
            }
            else if($department=="Product" || $department2=="Product" || $department3=="Product")
            {
                if($dept2 == NULL)
                {
                    $dept2 = "Product";
                }
                else
                {
                    $dept3 = "Product";

                }
            }

               $employee = employee::find($id);
             $employee->Designation=NULL;
             $employee->Designation2="Product Manager";
             $employee->Department=NULL;


             if($dept1=="Product - Selling" && $dept2=="Product - Rental")
             {
             $employee->Department2="Product";
           }
           elseif($dept1=="Product - Selling" && $dept2=="")
             {
             $employee->Department2="Product - Selling";
           }
            elseif($dept1=="Product - Rental" && $dept2=="")
             {
             $employee->Department2="Product - Rental";
           }
            elseif($dept1=="Product" && $dept2=="")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product - Selling")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product - Rental")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product - Selling" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept1=="Product - Rental" && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept1==null && $dept2=="Product")
             {
             $employee->Department2="Product";
           }
            elseif($dept2==null && $dept3=="Product")
             {
             $employee->Department2="Product";
           }

// dd('asd');
              $employee->save();


        }else
        if($designation1=="Pharmacy Manager" || $designation2=="Pharmacy Manager" || $designation3=="Pharmacy Manager")
        {
            $desig1 ="Pharmacy Manager";
            $dept1 = "Product";
            $roleid=10;

               $employee = employee::find($id);
               $employee->Designation=NULL;
                $employee->Department=NULL;
             $employee->Designation2="Pharmacy Manager";
             $employee->Department2="Product";
              $employee->save();

        }else
        if($designation1=="Field Officer" || $designation2=="Field Officer" || $designation3=="Field Officer")
        {
            $desig1 ="Field Officer";
            $dept1 = "Product";
            $roleid=12;

               $employee = employee::find($id);
               $employee->Designation=NULL;
                $employee->Department=NULL;
                $employee->Designation2=NULL;
             $employee->Department2=null;
             $employee->Designation3="Field Officer";
             $employee->Department3="Product";

              $employee->save();
        }else
        if($designation1=="Field Executive" || $designation2=="Field Executive" || $designation3=="Field Executive")
        {
            $desig1 ="Field Executive";
            $dept1 = "Product";
            $roleid=11;
             $employee = employee::find($id);
             $employee->Designation=NULL;
            $employee->Department=NULL;
            $employee->Designation2=NULL;
             $employee->Department2=null;
             $employee->Designation3="Field Executive";
             $employee->Department3="Product";
              $employee->save();
        }
        else
        if($designation1=="Marketing" || $designation2=="Marketing" || $designation3=="Marketing")
        {
            $desig1 ="Marketing";
            $dept1 = "Marketing";
            $roleid=13;

             $employee = employee::find($id);
             $employee->Designation="Marketing";
              $employee->save();

        }
        else
        if($designation1=="Customer Care" || $designation2=="Customer Care" || $designation3=="Customer Care")
        {
            $desig1 ="Customer Care";
            $dept1 = "Customer Care";
            $roleid=1;

             $employee = employee::find($id);
             $employee->Designation="Customer Care";
              $employee->save();
        }











// dd($roleid);



        /*
        to verify whether there is any existing data for that user in the admins table
        */
// dd($empname);
        $checkid=DB::table('admins')->where('name',$empname)->value('id');
        $check=DB::table('admins')->where('name',$empname)->value('email');

        /*
        correct
        If there is no existing row for employees for login in admin table
        then create a login for him/her
        */

        if($check == NULL && $empemail!=NULL && $checkid == NULL)
        {

            $email=$request->Email_id;


            if($check!=$email)
            {
                if($check!=NULL)
                {
                    $admin = admin::find($checkid);

                    $admin->email=$email;
                    $admin->save();
                }
            }
            else
            {

                $admin = new Admin;

                $admin->name=$empname;
                $admin->email=$empemail;
                $admin->password=$emppassword;
                $admin->save();
                $adminid=DB::table('admins')->max('id');


                $role_admin = new role_admin;

                $role_admin->role_id=$roleid;
                $role_admin->admin_id=$adminid;
                $role_admin->save();
            }
        }
        else
        {

            $c=DB::table('role_admins')->where('admin_id',$checkid)->value('id');

            $roleadmin = role_admin::find($c);
            $roleadmin->role_id=$roleid;
            $roleadmin->save();




        }

        $email=$request->Email_id;
        // dd($email);

        if($check!=$email)
        {

            $admin = admin::find($checkid);
            $admin->email=$email;
            $admin->save();
        }





        /*end*/

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        if($empd==" " || $empd==' ' || $empd=="" || $empd=='')
        {
            $empd = trim($empd);
            $empf = trim($empf);
        }
        else
        {
            $empd = $empd."\r\n";
            $empf = $empf."\r\n";
        }

        $fields_updated = $empd;
        $values_updated = $empf;

        //If no field is updated and all are empty then don't log the activity
        if((trim($fields_updated)!==""))
        {
            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Fields Updated";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->field_updated = $fields_updated;
            $activity->value_updated = $values_updated;

            $activity->save();
        }

        session()->flash('message','Updated Successfully');
        return redirect('/employees');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $reference=refer::find($id);


        $reference->delete();
        session()->flash('message','Delete Successfully');
        return redirect('/references');
    }



    public function disablelist()
    {
        $employees= DB::table('disablelists')->paginate(50);
        return view('employee.disablelist',compact('employees'));

    }



    public function disable()
    {
        $id=$_GET['id'];


        $empname=DB::table('employees')->where('id',$id)->select('id','FirstName','MiddleName','LastName','Email_id','DOB','Gender','MartialStatus','BloodGroup','DOJ','EmployementType','Designation','MobileNumber','AlternateNumber','Department','under','LangaugesKnown','city','Regid','ReportingTo','category','underwhom')->get();
        $empname=json_decode($empname);

        $id= $empname[0]->id;
        $FirstName= $empname[0]->FirstName;
        $MiddleName= $empname[0]->MiddleName;
        $LastName= $empname[0]->LastName;
        $Email_id= $empname[0]->Email_id;
        $DOB= $empname[0]->DOB;
        $Gender= $empname[0]->Gender;
        $MartialStatus= $empname[0]->MartialStatus;
        $BloodGroup= $empname[0]->BloodGroup;
        $DOJ= $empname[0]->DOJ;
        $EmployementType= $empname[0]->EmployementType;
        $Designation= $empname[0]->Designation;
        $MobileNumber= $empname[0]->MobileNumber;
        $AlternateNumber= $empname[0]->AlternateNumber;
        $Department= $empname[0]->Department;
        $under= $empname[0]->under;
        $LangaugesKnown= $empname[0]->LangaugesKnown;
        $city= $empname[0]->city;
        $Regid= $empname[0]->Regid;
        $ReportingTo= $empname[0]->ReportingTo;
        $category= $empname[0]->category;
        $underwhom= $empname[0]->underwhom;


        $login=DB::table('admins')->where('name',$FirstName)->select('id','name','email','password')->get();
        $login=json_decode($login);
        $loginid= $login[0]->id;
        $loginname= $login[0]->name;
        $loginemail= $login[0]->email;
        $loginpassword= $login[0]->password;

        // dd($loginid);

        $employee= new disablelist;

        $employee->id=$id;
        $employee->Regid=$Regid;
        $employee->FirstName=$FirstName;
        $employee->MiddleName=$MiddleName;
        $employee->LastName=$LastName;
        $employee->Email_id=$Email_id;
        $employee->DOB=$DOB;
        $employee->Gender=$Gender;
        $employee->MartialStatus=$MartialStatus;
        $employee->BloodGroup=$BloodGroup;
        $employee->DOJ=$DOJ;
        $employee->EmployementType=$EmployementType;
        $employee->Designation=$Designation;
        $employee->MobileNumber=$MobileNumber;
        $employee->AlternateNumber=$AlternateNumber;
        $employee->Department=$Department;
        $employee->city=$city;
        $employee->under=$under;
        $employee->underwhom=$underwhom;
        $employee->LangaugesKnown=$LangaugesKnown;
        $employee->ReportingTo=$ReportingTo;
        $employee->category=$category;
        $employee->loginid=$loginid;
        $employee->loginname=$loginname;
        $employee->loginemail=$loginemail;
        $employee->loginpassword=$loginpassword;


        $employee->save();

        $employee=employee::find($id);
        $employee->delete();

        $admin=admin::find($loginid);
        $admin->delete();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Details for ".$FirstName." disabled";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;

        $activity->save();

        session()->flash('message','Disable Successfully');
        return redirect('/employees');
    }

    public function enable()
    {
        $id=$_GET['id'];


        $empname=DB::table('disablelists')->where('id',$id)->select('id','FirstName','MiddleName','LastName','Email_id','DOB','Gender','MartialStatus','BloodGroup','DOJ','EmployementType','Designation','MobileNumber','AlternateNumber','Department','under','LangaugesKnown','city','Regid','ReportingTo','category','underwhom','loginid','loginname','loginemail','loginpassword')->get();
        $empname=json_decode($empname);

        $id= $empname[0]->id;
        $FirstName= $empname[0]->FirstName;
        $MiddleName= $empname[0]->MiddleName;
        $LastName= $empname[0]->LastName;
        $Email_id= $empname[0]->Email_id;
        $DOB= $empname[0]->DOB;
        $Gender= $empname[0]->Gender;
        $MartialStatus= $empname[0]->MartialStatus;
        $BloodGroup= $empname[0]->BloodGroup;
        $DOJ= $empname[0]->DOJ;
        $EmployementType= $empname[0]->EmployementType;
        $Designation= $empname[0]->Designation;
        $MobileNumber= $empname[0]->MobileNumber;
        $AlternateNumber= $empname[0]->AlternateNumber;
        $Department= $empname[0]->Department;
        $under= $empname[0]->under;
        $LangaugesKnown= $empname[0]->LangaugesKnown;
        $city= $empname[0]->city;
        $Regid= $empname[0]->Regid;
        $ReportingTo= $empname[0]->ReportingTo;
        $category= $empname[0]->category;
        $underwhom= $empname[0]->underwhom;
        $loginid=$empname[0]->loginid;
        $loginname=$empname[0]->loginname;
        $loginemail=$empname[0]->loginemail;
        $loginpassword=$empname[0]->loginpassword;




        $employee= new employee;

        $employee->id=$id;
        $employee->Regid=$Regid;
        $employee->FirstName=$FirstName;
        $employee->MiddleName=$MiddleName;
        $employee->LastName=$LastName;
        $employee->Email_id=$Email_id;
        $employee->DOB=$DOB;
        $employee->Gender=$Gender;
        $employee->MartialStatus=$MartialStatus;
        $employee->BloodGroup=$BloodGroup;
        $employee->DOJ=$DOJ;
        $employee->EmployementType=$EmployementType;
        $employee->Designation=$Designation;
        $employee->MobileNumber=$MobileNumber;
        $employee->AlternateNumber=$AlternateNumber;
        $employee->Department=$Department;
        $employee->city=$city;
        $employee->under=$under;
        $employee->underwhom=$underwhom;
        $employee->LangaugesKnown=$LangaugesKnown;
        $employee->ReportingTo=$ReportingTo;
        $employee->category=$category;

        $employee->save();

        $admin=new admin;
        $admin->id=$loginid;
        $admin->name=$loginname;
        $admin->email=$loginemail;
        $admin->password=$loginpassword;
        $admin->save();

        $employee=disablelist::find($id);
        $employee->delete();

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Details for ".$FirstName." Enabled";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;

        $activity->save();

        session()->flash('message','Enable Successfully');
        return redirect('/employees');
    }


public function impersonate(Request $request)
{
    $logged_in_user=Auth::guard('admin')->user()->name;

    $employee_id=$_GET['id'];

    $name=DB::table('employees')->where('id',$employee_id)->value('FirstName');

    $email_id=DB::table('admins')->where('name',$name)->value('email');
    $password=DB::table('admins')->where('name',$name)->value('password');



        $token=$request->session()->getId();
        $request->session()->flush();

        $request->session()->regenerate();

    return Redirect::action('Admin\LoginController@login', array('email' => $email_id,'password' => $password,'token'=>$token,'logged_in_user'=>$logged_in_user));
}

public function filter(Request $request)
{
    // dd('as');

if($request->id == NULL)
{
if($request->filter=="REGID")

{
    $regid=$request->Regid1;
    $Email_id1=$request->Email_id1;
    $regexist=NULL;

    if($Email_id1==NULL)
    {
         $emailexist="false";
    }
    else
    {
        $data2 = DB::table('employees')->Where('Email_id',$Email_id1)
                    ->value('Email_id');
                    if($data2!=NULL)
                    {
                        $emailexist="true";
                    }
                    else
                    {
                        $emailexist="false";
                    }


    }


    $data1 = DB::table('employees')->Where('Regid',$regid)
                    ->value('Regid');

                    if($data1!=NULL)
                    {
                        $check="yes";
                        $message="already in use";
                       return view ('employee.message',compact('message','check','emailexist','regexist'));
                    }
                    else
                    {
                        $check="no";
                         $message="";
                         // dd($emailexist);
                        return view ('employee.message',compact('message','check','emailexist','regexist'));
                    }
    }
    else
    {
        $regid=$request->Regid1;
         $Email_id1=$request->Email_id1;
         $emailexist=NULl;

// dd($regid);
      if($regid==NULL)
    {
         $regexist="false";
    }
    else
    {
        $data1 = DB::table('employees')->Where('Regid',$regid)
                    ->value('Regid');

                    if($data1!=NULL)
                    {
                        $regexist="true";
                    }
                    else
                    {
                        $regexist="false";
                    }


    }

    $data2 = DB::table('employees')->Where('Email_id',$Email_id1)
                    ->value('Email_id');

                    if($data2!=NULL)
                    {
                        $check="yes";
                        $message="already in use";
                       return view ('employee.message',compact('message','check','regexist','emailexist'));
                    }
                    else
                    {
                        $check="no";
                         $message="";

                        return view ('employee.message',compact('message','check','regexist','emailexist'));
                    }
    }
}
else
{
    // dd('dont');
    if($request->filter=="REGID")

{
    $regid=$request->Regid1;
    $Email_id1=$request->Email_id1;
    $regexist=NULL;

    if($Email_id1==NULL)
    {
         $emailexist="false";
    }
    else
    {
         $verify_email = DB::table('employees')->Where('Email_id',$Email_id1)
                    ->value('id');

                     if($verify_email == $request->id)
                    {
                        $emailexist="false";
                    }
                    else
                    {

                        $data2 = DB::table('employees')->Where('Email_id',$Email_id1)
                        ->value('Email_id');
                        if($data2!=NULL)
                        {
                          $emailexist="true";
                        }
                        else
                        {
                            $emailexist="false";
                        }
                    }

    }

    $verify_id = DB::table('employees')->Where('Regid',$regid)
                    ->value('id');

                    if($verify_id == $request->id)
                    {
                         $check="no";
                             $message="";

                            return view ('employee.message',compact('message','check','emailexist','regexist'));
                    }
                    else
                    {

                        $data1 = DB::table('employees')->Where('Regid',$regid)
                        ->value('Regid');

                        if($data1!=NULL)
                        {
                             $check="yes";
                             $message="already in use";
                            return view ('employee.message',compact('message','check','emailexist','regexist'));
                        }
                        else
                        {
                            $check="no";
                             $message="";
                             // dd($emailexist);
                            return view ('employee.message',compact('message','check','emailexist','regexist'));
                        }
                    }
    }
    else
    {
        $regid=$request->Regid1;
         $Email_id1=$request->Email_id1;
         $emailexist=NULl;

// dd($regid);
      if($regid==NULL)
    {
         $regexist="false";
    }
    else
    {

         $verify_id = DB::table('employees')->Where('Regid',$regid)
                    ->value('id');

                    if($verify_id == $request->id)
                    {
                         $regexist="false";
                    }
                    else
                    {

                        $data1 = DB::table('employees')->Where('Regid',$regid)
                        ->value('Regid');

                        if($data1!=NULL)
                        {
                            $regexist="true";
                        }
                        else
                        {
                            $regexist="false";
                        }
                    }


    }

    $verify_email = DB::table('employees')->Where('Email_id',$Email_id1)
                    ->value('id');

                     if($verify_email == $request->id)
                    {
                        $check="no";
                         $message="";

                        return view ('employee.message',compact('message','check','regexist','emailexist'));
                    }
                    else
                    {

                         $data2 = DB::table('employees')->Where('Email_id',$Email_id1)
                            ->value('Email_id');

                    if($data2!=NULL)
                    {
                        $check="yes";
                        $message="already in use";
                       return view ('employee.message',compact('message','check','regexist','emailexist'));
                    }
                    else
                    {
                        $check="no";
                         $message="";

                        return view ('employee.message',compact('message','check','regexist','emailexist'));
                    }
                    }
    }
}


}


}
