<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class referalController extends Controller
{
     	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('referalpartner');
    }

    public function index()
    {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;
        $designation="referalpartner";
        // dd($logged_in_user);

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $newcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status0)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $inprogresscount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status1)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $convertedcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status2)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $droppedcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status3)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $deferredcount = DB::table('leads')
        ->select('services.*','leads.*')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->where('services.ServiceStatus',$status4)
        ->where('leads.Source',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

    	return view('admin.referalpartner',compact('newcount','inprogresscount','convertedcount','deferredcount','droppedcount','designation'));

    }

        public function assigned()
        {
            $logged_in_user = $_GET['name'];
            $status = $_GET['status'];

            $leads = DB::table('leads')
            ->select('services.*','leads.*')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('services.ServiceStatus',$status)
            ->where('leads.Source',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            return view('referalpartner.index',compact('leads'));
        }
}
