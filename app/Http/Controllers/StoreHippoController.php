<?php

namespace App\Http\Controllers;

use App\productdetail;
use DB;

use Illuminate\Http\Request;

class StoreHippoController extends Controller
{
    public function index()
    {
    /*start Checking StoreHippo API -- comments by jatin */

    // Here I have divided the url fetching into two parts
    // create a new cURL resource
    $ch1 = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch1, CURLOPT_URL, "http://healthheal.storehippo.com/api/1/entity/ms.products?start=0&limit=500");
    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('6e89b06e5981f6844b24a75e3b47c2a6', '58fb1430017e51f847a80d36'));
    curl_setopt($ch1, CURLOPT_HEADER, 0); // do not bring headers
    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

    // grab URL and pass it to the browser
    $output1 = curl_exec($ch1);
    // dd($output);
    $products1 = json_decode($output1,true);
    dd($products1);

    $status1 = curl_getinfo($ch1, CURLINFO_HTTP_CODE);


    // close cURL resource, and free up system resources
    curl_close($ch1);

    // create a new cURL resource2
    $ch2 = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch2, CURLOPT_URL, "http://healthheal.storehippo.com/api/1/entity/ms.products?start=501&limit=250");
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('6e89b06e5981f6844b24a75e3b47c2a6', '58fb1430017e51f847a80d36'));
    curl_setopt($ch2, CURLOPT_HEADER, 0); // do not bring headers
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);

    // grab URL and pass it to the browser
    $output2 = curl_exec($ch2);
    // dd($output);
    $product2 = json_decode($output2,true);
    // dd($product2);

    // count of data returned by first url
    $n1 = count($products1['data']);

    for($i=0;$i<$n1;$i++)
    {
      $product_name1 = NULL;

      // only if the index 'name' exists in the returned API save the name else put NULL in it
      if(array_key_exists('name',$products1['data'][$i]))
      {
        $product_name1 = $products1['data'][$i]['name'];
      }

      $sku_id1 = NULL;

      // only if the index 'sku' exists in the returned API save the name else put NULL in it
      if(array_key_exists('sku',$products1['data'][$i]))
      {
        $sku_id1 = $products1['data'][$i]['sku'];
      }

      $price1 = NULL;

      // only if the index 'price' exists in the returned API save the name else put NULL in it
      if(array_key_exists('price',$products1['data'][$i]))
      {
        $price1 = $products1['data'][$i]['price'];
      }

      $availability1 = NULL;

      // only if the index 'available' exists in the returned API save the name else put NULL in it
      if(array_key_exists('available',$products1['data'][$i]))
      {
        $availability1 = $products1['data'][$i]['available'];
      }

      $alias1 = NULL;

      // only if the index 'alias' exists in the returned API save the name else put NULL in it
      if(array_key_exists('alias',$products1['data'][$i]))
      {
        $alias1 = $products1['data'][$i]['alias'];
      }

      $publish1 = NULL;

      // only if the index 'publish' exists in the returned API save the name else put NULL in it
      if(array_key_exists('publish',$products1['data'][$i]))
      {
        $publish1 = $products1['data'][$i]['publish'];
      }

      // since the 'index' = 'compare_price' is not available for all products make a check
      if(array_key_exists('compare_price',$products1['data'][$i]))
      {
        $compare_price1 = $products1['data'][$i]['compare_price'];
      }

      // The categories have different values in the form of an array, Convert these into comma-separated string
      $categories_count1 = count($products1['data'][$i]['categories']);


      // Put all the data in a custom array
      for($j=0;$j<$categories_count1;$j++)
      {
        $categories_array1[$j] = $products1['data'][$i]['categories'][$j];
      }

      // Convert the array into comma separated string
      $categories_string1 = implode(',',$categories_array1);

      // Create a new model and save the data
      $product_model1 = new productdetail;
      $product_model1->Name = $product_name1;
      $product_model1->SKU = $sku_id1;
      $product_model1->Price = $price1;
      $product_model1->AliasName = $alias1;
      $product_model1->Availability = $availability1;
      $product_model1->Category = $categories_string1;
      $product_model1->Publish = $publish1;
      $product_model1->ComparePrice = $compare_price1;
      $product_model1->save();
    }



    //count of data returned by second url
    $n2 = count($product2['data']);
    // dd($n2);

    for($i=0;$i<$n2;$i++)
    {
      $product_name2 = NULL;
      if(array_key_exists('name',$product2['data'][$i]))
      {
        $product_name2 = $product2['data'][$i]['name'];
      }

      $sku_id2 = NULL;
      if(array_key_exists('sku',$products1['data'][$i]))
      {
        $sku_id2 = $product2['data'][$i]['sku'];
      }
      $price2 = NULL;
      if(array_key_exists('price',$products1['data'][$i]))
      {
        $price2 = $product2['data'][$i]['price'];
      }
      $availability2 = NULL;
      if(array_key_exists('available',$products1['data'][$i]))
      {
        $availability2 = $product2['data'][$i]['available'];
      }
      $alias2 = NULL;
      if(array_key_exists('alias',$products1['data'][$i]))
      {
        $alias2 = $product2['data'][$i]['alias'];
      }

      $publish2 = NULL;
      if(array_key_exists('publish',$product2['data'][$i]))
      {
        $publish2 = $product2['data'][$i]['publish'];
      }
      $compare_price2 = NULL;

      // since the 'index' = 'compare_price' is not available for all products make a check
      if(array_key_exists('compare_price',$product2['data'][$i]))
      {
        $compare_price2 = $product2['data'][$i]['compare_price'];
      }

      $n = $product2['data'][$i]['categories'];

      $categories_string2 = NULL;
      if(!empty($n))
      {
        $categories_count2 = count($product2['data'][$i]['categories']);

        for($j=0;$j<$categories_count2;$j++)
        {
          $categories_array2[$j] = $product2['data'][$i]['categories'][$j];
        }

        $categories_string2 = implode(',',$categories_array2);

      }

      // dd($product2['data'][0]['categories']);


      $product_model2 = new productdetail;
      $product_model2->Name = $product_name2;
      $product_model2->SKU = $sku_id2;
      $product_model2->Price = $price2;
      $product_model2->AliasName = $alias2;
      $product_model2->Availability = $availability2;
      $product_model2->Category = $categories_string2;
      $product_model2->Publish = $publish2;
      if($compare_price2!=NULL)
      {
        $product_model2->ComparePrice = $compare_price2;
      }
      else
      {
        $product_model2->ComparePrice = NULL;
      }
      $product_model2->save();
    }
    // dd($i);

    // dd($product_name2);
    // $availabiliity = $product2['data'][0]['']
    // $availability = product2['data'][0]['available'];
    // dd($sku_id);
    $status = curl_getinfo($ch2, CURLINFO_HTTP_CODE);


    // close cURL resource, and free up system resources
    curl_close($ch2);

    // dd($product_name2);


    /*end Checking StoreHippo API */
}
}
