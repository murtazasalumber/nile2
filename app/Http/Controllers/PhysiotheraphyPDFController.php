<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use \PDF;

class PhysiotheraphyPDFController extends Controller
{
    public function index(Request $request)
    {
        $leadid = $_GET['leadid'];
        // dd($leadid);
        if($request->has('download'))
        {
            $leaddata=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id','=',$leadid)
            ->orderBy('leads.id', 'DESC')
            ->get();

            $assessmentdata = DB::table('assessments')->where('Leadid',$leadid)->get();

            $physiotheraphydata = DB::table('physiotheraphies')->where('Leadid',$leadid)->get();

            $physioid = DB::table('physiotheraphies')->where('Leadid',$leadid)->value('id');
            $physioreportdata = DB::table('physioreports')->where('Physioid',$physioid)->get();
            // dd($physiotheraphydata);


            // dd($abdomendata);
            $pdf = PDF::loadView('pdf/Physiotheraphy_pdfview',compact('leaddata','assessmentdata','physiotheraphydata','physioreportdata'));
            return $pdf->download("Assessment_Form_for_$leadid.pdf");
        }

        // return view('pdf/pdfview');
    }
}
