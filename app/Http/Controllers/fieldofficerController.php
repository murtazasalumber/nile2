<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Auth;
use DB;

class fieldofficerController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('fieldofficer');
    }


    public function index()
    {
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $designation="fieldofficer";

        // dd($logged_in_user);


        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";

        $processingcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status1)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status1)
        ->where('products.AssignedTo',$logged_in_user)

        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status1)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();


        $processingcount = $processingcount1 + $processingcount2 + $processingcount3;




        $awaitingpickupcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status2)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status2)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status2)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $awaitingpickupcount = $awaitingpickupcount1 + $awaitingpickupcount2 + $awaitingpickupcount3;

        $outfordeliverycount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status3)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status3)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status3)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $outfordeliverycount = $outfordeliverycount1 + $outfordeliverycount2 + $outfordeliverycount3;

        $readytoshipcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status4)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status4)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status4)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $readytoshipcount = $readytoshipcount1 + $readytoshipcount2 + $readytoshipcount3;

        $orderreturncount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status5)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status5)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status5)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $orderreturncount = $orderreturncount1 + $orderreturncount2 + $orderreturncount3;

        $canceledcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status6)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status6)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status6)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $canceledcount = $canceledcount1 + $canceledcount2 + $canceledcount3;

        $deliveredcount1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status7)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status7)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status7)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->count();

        $deliveredcount = $deliveredcount1 + $deliveredcount2 + $deliveredcount3;

        return view('admin.fieldofficer',compact('processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation'));
    }

    public function assigned()
    {
        //If the status is present in the URL extract from there or retrieve it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        $logged_in_user = Auth::guard('admin')->user()->name;


        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');



        $leads1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatus',$status)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads1 = json_decode($leads1,true);

        // dd($leads1);

        $leads2= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('OrderStatusrent',$status)
        ->where('products.AssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads2 = json_decode($leads2,true);

        $leads3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('pharmacies.pOrderStatus',$status)
        ->where('pharmacies.PAssignedTo',$logged_in_user)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads3 = json_decode($leads3,true);

        $leads_all = array_merge($leads1,$leads2,$leads3);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));

        $leads1 = $leads_all;

        $count = count($leads1);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //Check if the status is "download" as passed in the admin/index.blade.php file
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            // Check if the status is All (this is when the user clicks on the View Leads buttton and wishes to download the data from there
            if($status=="All")
            {
                //extract the 'id','Designation,'city' and 'Department' of the logged_in_user
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);

                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('AssignedTo',$logged_in_user)
                ->orwhere('PAssignedTo',$logged_in_user)
                ->orderBy('prodleads.id', 'DESC')
                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);

                //create a separate array to take care of the column names to be show in the CSV file
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }
            //if the status is returned as "New/In Progress etc", then run this (This is the case where the user clicks on the Individual Counts and wishes to Download the CSV for that view)
            else
            {
                // dd($status);
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatus',$status)
                ->where('products.AssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);

                $leads2  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('OrderStatusrent',$status)
                ->where('products.AssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads2 = json_decode($leads2,true);

                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('pharmacies.pOrderStatus',$status)
                ->where('pharmacies.PAssignedTo',$logged_in_user)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads2,$leads3);


                usort($leads_all, make_comparer('id'));

                $leads = $leads_all;


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                    $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }
        session()->put('name',$logged_in_user);
                $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        return view('fieldexecutive.index1',compact('leads1','d','count','desig'));
    }


}
