<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SMSEmailFilterController extends Controller
{
    public function filter()
    {
        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($active_state==0)
        {
            DB::table('sms_email_filter')
            ->where('id', 1)
            ->update(['active_state' => 1]);
        }
        else
        {
            DB::table('sms_email_filter')
            ->where('id', 1)
            ->update(['active_state' => 0]);
        }

        if($active_state==0)
        {
        session()->flash('message','Your Emails and Messages have been disabled.');
    }
    else
    {
        session()->flash('message','Your Emails and Messages have been enabled.');
    }
        return redirect()->route('updatetables');
    }
}
