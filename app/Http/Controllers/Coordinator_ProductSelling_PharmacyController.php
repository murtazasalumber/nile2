<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Response;
use Session;




class Coordinator_ProductSelling_PharmacyController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('coordinator_productselling_pharmacy');
    }

    public function index()
    {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;
        $designation="coordinator";

        $under = DB::table('employees')->where('FirstName',$logged_in_user)->value('under');
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('City');
        $serviceType = DB::table('employees')->where('id',$under)->value('Department');

        // dd($serviceType);
        //assigning the possible statuses for the Coordinator
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        $status00 = "New";
        $status11 = "Processing";
        $status22  = "Awaiting Pickup";
        $status33  = "Out for Delivery";
        $status44  = "Ready to ship";
        $status55  = "Order Return";
        $status66  = "Cancelled";
        $status77  = "Delivered";
        $status88  = "Received Order Return";


        // find the count of all the leads which have been assigned to the logged in coordinator
        // a linkage exists between the vertical coordinations and employees table
        // - if a lead has been assigned to a coordinator then the coordinators employee id will be present in "vertical coordinations" table
        // - if not , then "Null" goes there
        // here, we are checking for the status "In Progress" which comes as "New" for the //coordinator
        $assignedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status1)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $convertedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status2)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $droppedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status3)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $deferredcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status4)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();



        // Product Selling count for all status
        $productnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status00)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status11)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status22)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status33)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status44)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status55)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status88)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status66)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $productdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status77)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();



        // Pharmacy count for all status

        $pharmacynewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status00)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status11)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status22)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status33)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status44)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status55)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status88)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacycanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status66)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacydeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status77)
        ->where('City',$user_city)
        ->orderBy('id', 'DESC')
        ->count();





        // product count end

        return view('admin.Coordinator_ProductSelling_Pharmacy',compact('assignedcount','convertedcount','droppedcount','deferredcount','designation','user_city','serviceType','productnewcount','productprocessingcount','productawaitingpickupcount','productoutfordeliverycount','productreadytoshipcount','productorderreturncount','productrecievedorderreturncount','productcanceledcount','productdeliveredcount','pharmacynewcount','pharmacyprocessingcount','pharmacyawaitingpickupcount','pharmacyoutfordeliverycount','pharmacyreadytoshipcount','pharmacyorderreturncount','pharmacyrecievedorderreturncount','pharmacycanceledcount','pharmacydeliveredcount'));
    }

    // This function is for the scenario when the "Coordinator" on their dashboard clicks on the status count image
    //and sees the details depending on that count for "each status"

    public function assigned(Request $request)
    {

        // retrieving the status that was passed as parameter in "admin.coordinator blade" so that it can retrieve the records
        // assigned for that status

        // dd($_GET['status']);

        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $employeeid=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

        //general query for Viewing Leads on the basis of count and status
        $leads = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status)
        ->where('FirstName',$logged_in_user)
        ->where('verticalcoordinations.empid',$employeeid)
        ->orderBy('leads.id', 'DESC')
        ->paginate(50);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('services.AssignedTo',$logged_in_user)
                ->orwhere('verticalcoordinations.empid',$empname1)
                ->orderBy('leads.id', 'DESC')
                ->get();

                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                ->join('addresses','leads.id','=','addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status)
                ->where('FirstName',$logged_in_user)
                ->where('verticalcoordinations.empid',$employeeid)
                ->orderBy('leads.id', 'DESC')
                ->get();

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        // dd($leads);

        // Checking the status returned from the URL and redirecting to the appropriate view

        session()->put('status',$status);
        return view('Coordinator_ProductSelling_Pharmacy.index',compact('leads'));
    }

    public function assigned1()
    {

        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);
        $type=$_GET['type'];

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        if($type=="Sell")
        {

            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->where('City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        else
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->where('City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];
            // dd($status);

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation2;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department2;




                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'leads.id', '=', 'products.leadid')
                ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                ->where('City',$city)
                ->orderBy('products.id', 'DESC')
                ->get();
                // dd($leads);


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                if($type=="Sell")
                {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('OrderStatus',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('OrderStatusrent',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        session()->put('name',$logged_in_user);

        return view('Coordinator_ProductSelling_Pharmacy.productindex',compact('leads'));
    }


    public function assigned2()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        //  dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        $leads = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status)
        ->where('City',$city)
        ->orderBy('pharmacies.id', 'DESC')
        ->paginate(50);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation2;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department2;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                ->where('City',$city)
                ->orderBy('pharmacies.id', 'DESC')

                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');


                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                ->where('POrderStatus',$status)
                ->where('City',$city)
                ->orderBy('pharmacies.id', 'DESC')
                ->get();
                $leads = json_decode($leads,true);


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');

                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }


        session()->put('name',$logged_in_user);

        return view('Coordinator_ProductSelling_Pharmacy.pharmacyindex',compact('leads'));
    }



    public function assigned3()
    {
        //If the status is present in the URL extract from there or retrieve it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        $logged_in_user = Auth::guard('admin')->user()->name;


        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');



        $leads1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('City',$city)
        ->where('products.Type',"Sell")
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads1 = json_decode($leads1,true);


        $leads3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('City',$city)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads3 = json_decode($leads3,true);

        $leads_all = array_merge($leads1,$leads3);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));

        $leads1 = $leads_all;

        $count = count($leads1);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //Check if the status is "download" as passed in the admin/index.blade.php file
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            // Check if the status is All (this is when the user clicks on the View Leads buttton and wishes to download the data from there
            if($status=="All")
            {
                //extract the 'id','Designation,'city' and 'Department' of the logged_in_user
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);

                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;



                // dd($status);
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('City',$city)
                ->where('MedName','!=',NULL)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);


                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('City',$city)
                ->where('products.Type',"Sell")
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads3);



                usort($leads_all, make_comparer('id'));

                $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

                // dd($leads1);
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads1 as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }
        session()->put('name',$logged_in_user);
        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        dd($count);

        return view('Coordinator_ProductSelling_Pharmacy.allproductindex',compact('leads1','d','count','desig'));
    }

    public function productfilter(Request $request)
    {
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        // dd($logged_in_user);
        $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');


        // dd($pcheck);
        // dd($status1);

        if($pcheck=="Product - Selling")
        {

            $data1=DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('products.Type',"Sell")
            ->where('OrderStatus',$status1)
            ->where('addresses.City', $city)
            ->Where($filter1, 'like',   $keyword1 . '%')
            ->orderBy('products.id', 'DESC')
            ->paginate(50);


            return view('product.productfilter',compact('data1','keyword1','filter1'));
        }




    }

    public function pharmacyfilter(Request $request)
    {

        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;

        // dd($status1);
        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        dd($desig3);
        $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        if($desig3=="Pharmacy Manager")
        {

            // if($keyword1 == "")
            // {
            //
            //     return view('pharmacy.pharmacyindex');
            // }

            // dd($filter1);
            $data1=DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
            ->where('pOrderStatus',$status1)
            ->where('addresses.City', $city)
            ->Where($filter1, 'like',   $keyword1 . '%')
            ->orderBy('pharmacies.id', 'DESC')
            ->paginate(50);


            // dd($leads);
            return view('product.pharmacyfilter',compact('data1','keyword1','filter1'));

        }

    }

    public function productselling_pharmacy_allfilter(Request $request)
    {
        dd($request->keyword1);

        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        // dd($desig3);
        $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');


    }
}
