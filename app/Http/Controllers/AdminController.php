<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Illuminate\Support\Facades\Response;

// This class gives the "Admin" the privileges to create, view, modify Leads. Admin can also see the log
class AdminController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    //checking whether the person who is trying to "Log In" is authenticated or not
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    //this function shows what we see on the dashboard
    public function index()
    {
        // This is come from the following route
        // Route::get('admin/home','AdminController@index');

        $data = DB::table('cities')->get();
        $designation="home";

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //extracting the branch where the employee is currently working
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        //extracting the department ,i.e., Nursing Services, Physiotherapy, Mathrutvam , Personal Supportive Care
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        //Code for displaying the counts on the Admin Page for different statuses
        // We are displaying all leads with the above statuses
        $newcountforall = DB::table('services')
        ->where('ServiceStatus',$status0)
        ->count();

        // dd($newcountforall);

        $inprogresscountforall = DB::table('services')
        ->where('ServiceStatus',$status1)
        ->count();

        $convertedcountforall = DB::table('services')
        ->where('ServiceStatus',$status2)
        ->count();

        $droppedcountforall = DB::table('services')
        ->where('ServiceStatus',$status3)
        ->count();

        $deferredcountforall = DB::table('services')
        ->where('ServiceStatus',$status4)
        ->count();

        // Now we are aiming for all the statuses respective to the verticals in the Health Heal team
        //Counting the number of verticals in Health Heal
        // dd($verticals_count);

        //Extract the names of all the Vertical Heads
        $vertical_names =  DB::table('employees')->select('FirstName','city','Department')->where('Designation','Vertical Head')->groupBy('city')->groupBy('Department')->get();


        //converting the names of all Verticals into an Array
        $vertical_names = json_decode($vertical_names,true);

        $verticals_count = count($vertical_names);

        // dd($vertical_names);

        $branch_head_names = DB::table('employees')->select('FirstName','city','Department')->where('Designation','Branch Head')->groupBy('city')->groupBy('Department')->get();

        $branch_head_names = json_decode($branch_head_names,true);

        // dd($branch_head_names);
        $branch_head_count = count($branch_head_names);

        // dd($branch_head_count);
        //Initalizing Vertical_names array with Null
        $Vertical_names = null;

        //Extracting the vertical names from the above array and storing it in a 1-D array
        for($i=0;$i<$verticals_count;$i++)
        {
            $Vertical_names[] = $vertical_names[$i]['FirstName'];
        }



        // dd($Vertical_names,$verticals_count);
        for($i=0;$i<$branch_head_count;$i++)
        {
            $Vertical_names[] = $branch_head_names[$i]['FirstName'];
        }

        // dd($Vertical_cities);

        // dd($Vertical_names);

        $city_name = null;
        $servicetype_name = null;

        $totalcount = $verticals_count + $branch_head_count;
        // dd($totalcount);
        // dd($Vertical_names);

        //For every Vertical, Calculating the individual counts for their statuses
        for($j=0;$j<$totalcount;$j++)
        {
            /* Retrieving the counts of all the users -- started */
            //assigning the possible statuses for the Coordinator

            $status0 = "New";
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            //extracting the name of the presently accessed Vertical
            $vertical = $Vertical_names[$j];
            // dd($Vertical_names);

            // extracting the department of that vertical
            $dept = DB::table('employees')->where('FirstName',$vertical)->value('Department');
            $desig = DB::table('employees')->where('FirstName',$vertical)->value('Designation');
            // dd($dept);
            // extracting the user city of that vertical
            $user_city = DB::table('employees')->where('FirstName',$vertical)->value('city');

            //retrieving the values of all the services for those people who are not from Bangalore, i.e. Pune, Chennai, Hyderabad, Hubbli
            $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $city_name[] = $user_city;
            $servicetype_name[] = $dept;

            //assigning default value as "NULL" for all variables

            $newcount2 = $inprogresscount2 = $convertedcount2 = $droppedcount2 = $deferredcount2 = NULL;

            // dd($Vertical_cities[0]);
            //if the user city is anything other than Bangalore, extract the counts on the basis of the service statuses mentioned in Servicearray5
            //branch is relative to that vertical
            //status can be "New", "In progress " etc.

            if($desig=="Vertical Head")
            {
                if($user_city=="Bengaluru" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Pune" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Hyderabad" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Chennai" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Hubballi-Dharwad" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses



                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
            }
            else if($desig=="Branch Head")
            {
                $newcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();



            }

            // dd($servicetype_name);

            // dd($deferredcount2);
            //putting all status counts inside a 2-D array for dynamicity
            $statuscounts[] = array
            (
                "0"=>$newcount2,"1"=>$inprogresscount2, "2" =>$convertedcount2,"3"=>$droppedcount2,"4"=>$deferredcount2
            );
            // dd($statuscounts);
        }

        // dd($Vertical_names);
        // dd($Vertical_names, ' ', $statuscounts);

        // Fetching Product count details to show on dashboard

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // $designation="productmanager";


        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";
        // dd($check);

        $newcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();


        $newcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount=count($newcountamount);

        $newcountamount=json_decode($newcountamount,true);

        $newsum=0;
        for($i=0;$i<$countamount;$i++)
        {
            $newsum+= $newcountamount[$i]['FinalAmount'];
        }




        $processingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $processingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount1=count($processingcountamount);

        $processingcountamount=json_decode($processingcountamount,true);

        $processingsum=0;
        for($i=0;$i<$countamount1;$i++)
        {
            $processingsum+= $processingcountamount[$i]['FinalAmount'];
        }


        $awaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $awaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();


        $countamount2=count($awaitingpickupcountamount);

        $awaitingpickupcountamount=json_decode($awaitingpickupcountamount,true);

        $awaitingpickupsum=0;
        for($i=0;$i<$countamount2;$i++)
        {
            $awaitingpickupsum+= $awaitingpickupcountamount[$i]['FinalAmount'];
        }



        $outfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $outfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount3=count($outfordeliverycountamount);

        $outfordeliverycountamount=json_decode($outfordeliverycountamount,true);

        $outfordeliverysum=0;
        for($i=0;$i<$countamount3;$i++)
        {
            $outfordeliverysum+= $outfordeliverycountamount[$i]['FinalAmount'];
        }




        $readytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $readytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount4=count($readytoshipcountamount);

        $readytoshipcountamount=json_decode($readytoshipcountamount,true);

        $readytoshipsum=0;
        for($i=0;$i<$countamount4;$i++)
        {
            $readytoshipsum+= $readytoshipcountamount[$i]['FinalAmount'];
        }



        $orderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $orderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount5=count($orderreturncountamount);

        $orderreturncountamount=json_decode($orderreturncountamount,true);

        $orderreturnsum=0;
        for($i=0;$i<$countamount5;$i++)
        {
            $orderreturnsum+= $orderreturncountamount[$i]['FinalAmount'];
        }




        $recievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $recievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount30=count($recievedorderreturncountamount);

        $recievedorderreturncountamount=json_decode($recievedorderreturncountamount,true);

        $recievedorderreturnsum=0;
        for($i=0;$i<$countamount30;$i++)
        {
            $recievedorderreturnsum+= $recievedorderreturncountamount[$i]['FinalAmount'];
        }





        $canceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $canceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount6=count($canceledcountamount);

        $canceledcountamount=json_decode($canceledcountamount,true);

        $canceledsum=0;
        for($i=0;$i<$countamount6;$i++)
        {
            $canceledsum+= $canceledcountamount[$i]['FinalAmount'];
        }




        $deliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $deliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount7=count($deliveredcountamount);

        $deliveredcountamount=json_decode($deliveredcountamount,true);

        $deliveredsum=0;
        for($i=0;$i<$countamount7;$i++)
        {
            $deliveredsum+= $deliveredcountamount[$i]['FinalAmount'];
        }





        $newcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $newcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount8=count($newcountrentamount);

        $newcountrentamount=json_decode($newcountrentamount,true);

        $newrentsum=0;
        for($i=0;$i<$countamount8;$i++)
        {
            $newrentsum+= $newcountrentamount[$i]['FinalAmount'];
        }





        $processingcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $processingcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();
        // dd($processingcountrentamount);

        $countamount9=count($processingcountrentamount);

        $processingcountrentamount=json_decode($processingcountrentamount,true);

        $processingrentsum=0;
        for($i=0;$i<$countamount9;$i++)
        {
            $processingrentsum+= $processingcountrentamount[$i]['FinalAmount'];
        }


        $awaitingpickupcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $awaitingpickupcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount10=count($awaitingpickupcountrentamount);

        $awaitingpickupcountrentamount=json_decode($awaitingpickupcountrentamount,true);

        $awaitingpickuprentsum=0;
        for($i=0;$i<$countamount10;$i++)
        {
            $awaitingpickuprentsum+= $awaitingpickupcountrentamount[$i]['FinalAmount'];
        }


        // dd($awaitingpickupcountrentamount);



        $outfordeliverycountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $outfordeliverycountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount11=count($outfordeliverycountrentamount);

        $outfordeliverycountrentamount=json_decode($outfordeliverycountrentamount,true);

        $outfordeliveryrentsum=0;
        for($i=0;$i<$countamount11;$i++)
        {
            $outfordeliveryrentsum+= $outfordeliverycountrentamount[$i]['FinalAmount'];
        }






        $readytoshipcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $readytoshipcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount12=count($readytoshipcountrentamount);

        $readytoshipcountrentamount=json_decode($readytoshipcountrentamount,true);

        $readytoshiprentsum=0;
        for($i=0;$i<$countamount12;$i++)
        {
            $readytoshiprentsum+= $readytoshipcountrentamount[$i]['FinalAmount'];
        }








        $orderreturncountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $orderreturncountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount13=count($orderreturncountrentamount);

        $orderreturncountrentamount=json_decode($orderreturncountrentamount,true);

        $orderreturnrentsum=0;
        for($i=0;$i<$countamount13;$i++)
        {
            $orderreturnrentsum+= $orderreturncountrentamount[$i]['FinalAmount'];
        }




        $recievedorderreturncountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $recievedorderreturncountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount26=count($recievedorderreturncountrentamount);

        $recievedorderreturncountrentamount=json_decode($recievedorderreturncountrentamount,true);

        $recievedorderreturnrentsum=0;
        for($i=0;$i<$countamount26;$i++)
        {
            $recievedorderreturnrentsum+= $recievedorderreturncountrentamount[$i]['FinalAmount'];
        }


        // dd($orderreturncountrentamount);





        $canceledcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $canceledcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount14=count($canceledcountrentamount);

        $canceledcountrentamount=json_decode($canceledcountrentamount,true);

        $canceledrentsum=0;
        for($i=0;$i<$countamount14;$i++)
        {
            $canceledrentsum+= $canceledcountrentamount[$i]['FinalAmount'];
        }






        $deliveredcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $deliveredcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount15=count($deliveredcountrentamount);

        $deliveredcountrentamount=json_decode($deliveredcountrentamount,true);

        $deliveredrentsum =0;
        for($i=0;$i<$countamount15;$i++)
        {
            $deliveredrentsum += $deliveredcountrentamount[$i]['FinalAmount'];
        }







        // product count end



        // Fetching pharmacy details count


        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";

        $pnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pnewcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount16=count($pnewcountamount);

        $pnewcountamount=json_decode($pnewcountamount,true);

        $pnewsum =0;
        for($i=0;$i<$countamount16;$i++)
        {
            $pnewsum += $pnewcountamount[$i]['PFinalAmount'];
        }






        $pprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pprocessingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount17=count($pprocessingcountamount);

        $pprocessingcountamount=json_decode($pprocessingcountamount,true);

        $pprocessingsum=0;
        for($i=0;$i<$countamount17;$i++)
        {
            $pprocessingsum += $pprocessingcountamount[$i]['PFinalAmount'];
        }








        $pawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pawaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount18=count($pawaitingpickupcountamount);

        $pawaitingpickupcountamount=json_decode($pawaitingpickupcountamount,true);

        $pawaitingpickupsum =0;
        for($i=0;$i<$countamount18;$i++)
        {
            $pawaitingpickupsum += $pawaitingpickupcountamount[$i]['PFinalAmount'];
        }






        $poutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $poutfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount19=count($poutfordeliverycountamount);

        $poutfordeliverycountamount=json_decode($poutfordeliverycountamount,true);

        $poutfordeliverysum =0;
        for($i=0;$i<$countamount19;$i++)
        {
            $poutfordeliverysum += $poutfordeliverycountamount[$i]['PFinalAmount'];
        }







        $preadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $preadytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount20=count($preadytoshipcountamount);

        $preadytoshipcountamount=json_decode($preadytoshipcountamount,true);

        $preadytoshipsum =0;
        for($i=0;$i<$countamount20;$i++)
        {
            $preadytoshipsum += $preadytoshipcountamount[$i]['PFinalAmount'];
        }








        $porderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $porderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount21=count($porderreturncountamount);

        $porderreturncountamount=json_decode($porderreturncountamount,true);

        $porderreturnsum =0;
        for($i=0;$i<$countamount21;$i++)
        {
            $porderreturnsum += $porderreturncountamount[$i]['PFinalAmount'];
        }



        $precievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $precievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount31=count($precievedorderreturncountamount);

        $precievedorderreturncountamount=json_decode($precievedorderreturncountamount,true);

        $precievedorderreturnsum =0;
        for($i=0;$i<$countamount31;$i++)
        {
            $precievedorderreturnsum += $precievedorderreturncountamount[$i]['PFinalAmount'];
        }








        $pcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcanceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount22=count($pcanceledcountamount);

        $pcanceledcountamount=json_decode($pcanceledcountamount,true);

        $pcanceledsum =0;
        for($i=0;$i<$countamount22;$i++)
        {
            $pcanceledsum += $pcanceledcountamount[$i]['PFinalAmount'];
        }







        $pdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $pdeliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount23=count($pdeliveredcountamount);

        $pdeliveredcountamount=json_decode($pdeliveredcountamount,true);

        $pdeliveredsum =0;
        for($i=0;$i<$countamount23;$i++)
        {
            $pdeliveredsum += $pdeliveredcountamount[$i]['PFinalAmount'];
        }

        // pharmacy end

        // All city data

        $city_array = DB::table('cities')->get();
        $city_array = json_decode($city_array, true);

        //Bengaluru
        $branch1 = $city_array[0]['name'];

        //Chennai
        $branch2 = $city_array[1]['name'];

        //Pune
        $branch3 = $city_array[2]['name'];

        //Hyderabad
        $branch4 = $city_array[3]['name'];

        //Hubballi-Dharwad
        $branch5 = $city_array[4]['name'];

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";

        /*Branch wise counts for product selling */

        //for bangalore
        $pbsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch1)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pbsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pbscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Chennai
        $pcsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch2)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pcsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pcscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Pune
        $ppsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch3)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $ppsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $ppscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hyderabad
        $phsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch4)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->orderBy('id', 'DESC')
        ->count();

        $phsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hubballi-Dharwad
        $phdsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch5)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phdsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phdscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        /*
        Branch wise counts for products rent
        */

        //for bangalore
        $pbrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch1)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pbrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pbrawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pbroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pbrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Chennai
        $pcrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch2)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pcrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pcrawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pcroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('OrderStatusrent',$status7)
        ->where('City',$branch2)
        ->orderBy('id', 'DESC')
        ->count();

        //for Pune
        $pprnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch3)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pprprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pprawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pproutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pprreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('OrderStatusrent',$status4)
        ->where('City',$branch3)
        ->orderBy('id', 'DESC')
        ->count();


        $pprorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pprrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pprcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pprdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hyderabad
        $phrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch4)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phrawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phrreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hubballi-Dharwad
        $phdrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch5)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phdrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phdrawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phdroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phdrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        /* Branch wise counts for pharmacy */
        // Pharmacy count for all status

        //For bangalore
        $pharmacybnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyboutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyborderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacybcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();
        // dd($servicetype_name);

        //For chennai
        $pharmacycnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyccanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Pune
        $pharmacypnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyporderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyprecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacypcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Hyderabad
        $pharmacyhnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyhcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Hubballi-Dharwad
        $pharmacyhdnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyhdcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhddeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();



//count for assessement done on dashboard with new state assessment done for all services


        $physiotherapy_count=DB::table('leads')
        ->join('physiotheraphies','leads.id','=','physiotheraphies.Leadid')
        ->where('physiotheraphies.PhysiotheraphyType','<>',NULL)
        ->count();

        $Mathruthvam_count=DB::table('leads')
        ->join('mothers','leads.id','=','mothers.Leadid')
        ->join('mathruthvams','mothers.id','=','mathruthvams.Motherid')
        ->join('vitalsigns','leads.id','=','vitalsigns.leadid')
        ->where('vitalsigns.BP','<>',NULL)
        ->where('vitalsigns.RR','<>',NULL)
        ->where('vitalsigns.Temperature','<>',NULL)
        ->where('vitalsigns.Pulse','<>',NULL)
        ->count();


        $nursing_psc_count=DB::table('leads')
        ->join('vitalsigns','leads.id','=','vitalsigns.leadid')
        ->join('genitos','leads.id','=','genitos.leadid')
        ->join('mobilities','leads.id','=','mobilities.leadid')
        ->where('mobilities.Independent','<>',NULL)
        ->where('genitos.UrinaryContinent','<>',NULL)
         ->where('vitalsigns.BP','<>',NULL)
        ->where('vitalsigns.RR','<>',NULL)
        ->where('vitalsigns.Temperature','<>',NULL)
        ->where('vitalsigns.Pulse','<>',NULL)
        
        ->count();

$allservice_count=$physiotherapy_count+$Mathruthvam_count+$nursing_psc_count;
        

 $provisionalcount=DB::table('provisionalleads')->where('active',1)->count();


        return view('admin.home',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        'deferredcountforall','verticals_count','totalcount','Vertical_names','statuscounts','designation','city_name','servicetype_name','newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','newcountrent','processingcountrent','awaitingpickupcountrent','outfordeliverycountrent','readytoshipcountrent','orderreturncountrent','canceledcountrent','deliveredcountrent','pnewcount','pprocessingcount','pawaitingpickupcount','poutfordeliverycount','preadytoshipcount','porderreturncount','pcanceledcount','pdeliveredcount','newsum','processingsum','awaitingpickupsum','outfordeliverysum','readytoshipsum','orderreturnsum','canceledsum','deliveredsum','newrentsum','processingrentsum','awaitingpickuprentsum','outfordeliveryrentsum','readytoshiprentsum','orderreturnrentsum','canceledrentsum','deliveredrentsum','pnewsum','pprocessingsum','pawaitingpickupsum','poutfordeliverysum','preadytoshipsum','porderreturnsum','pcanceledsum','pdeliveredsum','precievedorderreturncount','recievedorderreturncountrent','recievedorderreturncount','recievedorderreturnsum','recievedorderreturnrentsum','precievedorderreturnsum',
        'pbsnewcount','pbsprocessingcount','pbsawaitingpickupcount','pbsoutfordeliverycount','pbsreadytoshipcount','pbsorderreturncount','pbsrecievedorderreturncount','pbscanceledcount','pbsdeliveredcount','pcsnewcount','pcsprocessingcount','pcsawaitingpickupcount','pcsoutfordeliverycount','pcsreadytoshipcount','pcsorderreturncount','pcsrecievedorderreturncount','pcscanceledcount','pcsdeliveredcount','ppsnewcount','ppsprocessingcount','ppsawaitingpickuppount',
        'ppsoutfordeliverycount','ppsreadytoshippount','ppsorderreturncount','ppsrecievedorderreturncount','ppscanceledcount','ppsdeliveredcount','phsnewcount','phsprocessingcount','phsawaitingpickuppount','phsoutfordeliverycount'
        ,'phsreadytoshippount','phsorderreturncount','phsrecievedorderreturncount','phscanceledcount','phsdeliveredcount','phdsnewcount','phdsprocessingcount','phdsawaitingpickuppount','phdsoutfordeliverycount','phdsreadytoshippount','phdsorderreturncount','phdsrecievedorderreturncount','phdscanceledcount',
        'phdsdeliveredcount','pbrnewcount','pbrprocessingcount','pbrawaitingpickupcount','pbroutfordeliverycount','pbrreadytoshipcount','pbrorderreturncount','pbrrecievedorderreturncount','pbrcanceledcount','pbrdeliveredcount','pcrnewcount','pcrprocessingcount','pcrawaitingpickupcount','pcroutfordeliverycount','pcrreadytoshipcount','pcrorderreturncount','pcrrecievedorderreturncount','pcrcanceledcount','pcrdeliveredcount','pprnewcount','pprprocessingcount','pprawaitingpickuppount','pproutfordeliverycount','pprreadytoshippount','pprorderreturncount','pprrecievedorderreturncount'
        ,'pprcanceledcount','pprdeliveredcount','pprnewcount','pprprocessingcount','pprawaitingpickuppount','pproutfordeliverycount','pprreadytoshippount','pprorderreturncount','pprrecievedorderreturncount','pprcanceledcount','pprdeliveredcount','phrnewcount','phrprocessingcount','phrawaitingpickuppount','phroutfordeliverycount','phrreadytoshippount','phrorderreturncount','phrrecievedorderreturncount','phrcanceledcount','phrdeliveredcount','phdrnewcount',
        'phdrprocessingcount','phdrawaitingpickuppount','phdroutfordeliverycount','phdrreadytoshippount','phdrorderreturncount','phdrrecievedorderreturncount','phdrcanceledcount','phdrdeliveredcount'
        ,'pharmacybnewcount','pharmacybprocessingcount','pharmacybawaitingpickupcount','pharmacyboutfordeliverycount','pharmacybreadytoshipcount','pharmacyborderreturncount','pharmacybrecievedorderreturncount','pharmacybcanceledcount','pharmacybdeliveredcount','pharmacycnewcount','pharmacycprocessingcount','pharmacycawaitingpickupcount','pharmacycoutfordeliverycount','pharmacycreadytoshipcount',
        'pharmacycorderreturncount','pharmacycrecievedorderreturncount','pharmacyccanceledcount','pharmacycdeliveredcount','pharmacypnewcount','pharmacypprocessingcount','pharmacypawaitingpickupcount','pharmacypoutfordeliverycount','pharmacypreadytoshipcount','pharmacyporderreturncount','pharmacyprecievedorderreturncount','pharmacypcanceledcount','pharmacypdeliveredcount', 'pharmacyhnewcount','pharmacyhnewcount', 'pharmacyhprocessingcount','pharmacyhawaitingpickupcount',
        'pharmacyhoutfordeliverycount','pharmacyhreadytoshipcount','pharmacyhorderreturncount','pharmacyhrecievedorderreturncount','pharmacyhcanceledcount','pharmacyhdeliveredcount','pharmacyhdnewcount','pharmacyhdprocessingcount','pharmacyhdawaitingpickupcount','pharmacyhdoutfordeliverycount','pharmacyhdreadytoshipcount','pharmacyhdorderreturncount','pharmacyhdrecievedorderreturncount','pharmacyhdcanceledcount','pharmacyhddeliveredcount','allservice_count','provisionalcount'));
    }

    public function assigned()
    {
        // This function comes from the Route
        // Route::get('adminassign','AdminController@assigned');
        // AND
        // Route::get('admindownload','AdminController@assigned');
        // Route::get('admindownloadall','AdminController@assigned');

        //If the status is present in the URL extract from there or retrieve it from the session
        //this status was passed in the href line just above the counts of each status
 
        // <a href="/adminassign?name={{ Auth::guard('admin')->user()->name }}&status=New">

        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        //extract the name of the presently logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;

        //Displaying the Leads depending on the count generated for that particular status
        $leads = DB::table('services')
        ->select('services.*','leads.*')
        ->join('leads','leads.id','=','services.Leadid')
        ->where('ServiceStatus',$status)
        ->orderBy('leads.id','DESC')
        ->paginate(50);


        /*
        Logic for downloading CSV goes here -- starts here
        */
        //Check if the status is "download" as passed in the admin/index blade
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            // Check if the status is All (this is when the user clicks on the View Leads buttton and wishes to download the data from there
            // This status is coming from first admin/home then checked in admin/index and finally passed along with the route "admindownloadall"

            if($status=="All")
            {
                //extract the 'id','Designation,'city' and 'Department' of the logged_in_user
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);

                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;

                //This helps download the same data as was being shown on "View Leads" page but with extra columns hence an increased number of joins

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->orderBy('leads.id', 'DESC')
                // ->limit(9000)
                ->get();

                //convert it into array format
                $leads = json_decode($leads,true);

                // dd($leads);

                //create a separate array to take care of the column names to be show in the CSV file
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }
            //if the status is returned as "New/In Progress etc", then run this (This is the case where the user clicks on the Individual Counts and wishes to Download the CSV for that view)
            // This status is coming from first admin/home then moved to AdminController "assigned" function then checked in admin/index and finally passed along with the route "admindownload"

            else
            {
                // dd($status);

                // Query written for individual count statuses
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('ServiceStatus',$status)
                ->orderBy('leads.id','DESC')
                ->get();

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }


        session()->put('name',$logged_in_user);

        return view('admin.index',compact('leads'));
    }

    public function filterassigned()
    {
        /* Filter download code starts here */

        //If the status is present in the URL extract from there or retrieve it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        $branch = $_GET['branch'];
        $vertical = $_GET['vertical'];
        $Fromdate = $_GET['from'];
        $Todate = $_GET['to'];




        // dd($Todate, $Fromdate, $vertical, $branch);
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        $designation=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');


        $Servicesarray1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

        $Servicesarray2 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

        $Servicesarray3 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        $Servicesarray4 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

        $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];




        // dd($branch);
        //if branch , vertical and time all are selected
        if($branch!="All" && $vertical!="All" && $Fromdate!="NULL" && $Todate!="NULL")
        {
            // dd($branch);
            $Fromdate=$Fromdate;
            $Todate=$Todate;
            // dd($Todate);



            // check the vertical selected based on that change the array value
            // dd($vertical);

            if($vertical=="NursingServices")
            {
                $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            }

            if($vertical=="PersonalSupportiveCare")
            {
                $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
            }

            if($vertical=="Mathrutvam-Baby Care")
            {
                $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
            }

            if($vertical=="Physiotherapy-Home")
            {
                $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
            }


            // Code for displaying the counts the if only vertical is selected

            // dd($branch);
            $leads = DB::table('leads')
            ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('ServiceStatus',$status)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('leads.created_at',[$Fromdate,$Todate])
            ->orderBy('leads.id','DESC')
            ->get();

            $leads = json_decode($leads,true);

            $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
            ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
            ,'Emergency District','Emergency State','Emergency PinCode');
            // $list = array (
            //     $leads
            // );
            //
            //         $lists = array (
            // array('aaa', 'bbb', 'ccc', 'dddd'),
            // array('123', '456', '789'),
            // array('aaa', 'bbb')
            // );
            // dd($list);

            $filename = "download.csv";

            $fp = fopen('download.csv', 'w');

            fputcsv($fp, $array );
            foreach ($leads as $fields) {
                fputcsv($fp, $fields);
            }

            fclose($fp);

            $headers = array(
                'Content-Type' => 'text/csv',
            );
            return Response::download($filename, 'download.csv', $headers);


            /*
            Logic for downloading CSV goes here -- ends here
            */

            return view('admin.allfilter',compact('designation','designation','branch','vertical','Fromdate','Todate','newcountforall'));

        }
        else
        {
            if($branch!="All" && $vertical!="All")
            {


                // dd("b and v");

                if($vertical=="NursingServices")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($vertical=="PersonalSupportiveCare")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($vertical=="Mathrutvam-Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($vertical=="Physiotherapy-Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }




                // Code for displaying the counts the Admin Page for different branch and vertical

                // $newcountforall = DB::table('services')
                // ->where('ServiceStatus',$status0)
                // ->where('Branch',$branch)
                // ->wherein('ServiceType',$vertical1)
                // ->count();
                // // dd($newcountforall);

                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('ServiceStatus',$status)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('leads.id','DESC')
                ->get();

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);


                /*
                Logic for downloading CSV goes here -- ends here
                */


                return view('admin.branchtoverticalfilter',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));
            }
            else
            {
                if($branch!="All" && $Fromdate!="NULL" && $Todate!="NULL")
                {

                    // dd("b and t");

                    $Fromdate=$Fromdate;

                    $Todate=$Todate;


                    //count for individual branch for the given time period

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('ServiceStatus',$status)
                    ->where('Branch',$branch)
                    ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                    ->orderBy('leads.id','DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');
                    // $list = array (
                    //     $leads
                    // );
                    //
                    //         $lists = array (
                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                    // array('123', '456', '789'),
                    // array('aaa', 'bbb')
                    // );
                    // dd($list);

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);


                    /*
                    Logic for downloading CSV goes here -- ends here
                    */

                    return view('admin.branchtotimeline',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));
                }
                else
                {
                    if($vertical!="All" && $Fromdate!="NULL" && $Todate!="NULL")
                    {
                        // dd($Fromdate);
                        // dd("t and v");

                        $Fromdate=$Fromdate;

                        $Todate=$Todate;

                        // dd("V and t");

                        // check the vertical selected based on that change the array value

                        if($vertical=="NursingServices")
                        {
                            $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                        }

                        if($vertical=="PersonalSupportiveCare")
                        {
                            $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                        }

                        if($vertical=="Mathrutvam-Baby Care")
                        {
                            $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                        }

                        if($vertical=="Physiotherapy-Home")
                        {
                            $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                        }





                        // Code for displaying the counts the if only vertical is selected

                        $leads = DB::table('leads')
                        ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->where('ServiceStatus',$status)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                        ->orderBy('leads.id','DESC')
                        ->get();

                        $leads = json_decode($leads,true);

                        $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                        ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                        ,'Emergency District','Emergency State','Emergency PinCode');
                        // $list = array (
                        //     $leads
                        // );
                        //
                        //         $lists = array (
                        // array('aaa', 'bbb', 'ccc', 'dddd'),
                        // array('123', '456', '789'),
                        // array('aaa', 'bbb')
                        // );
                        // dd($list);

                        $filename = "download.csv";

                        $fp = fopen('download.csv', 'w');

                        fputcsv($fp, $array );
                        foreach ($leads as $fields) {
                            fputcsv($fp, $fields);
                        }

                        fclose($fp);

                        $headers = array(
                            'Content-Type' => 'text/csv',
                        );
                        return Response::download($filename, 'download.csv', $headers);


                        /*
                        Logic for downloading CSV goes here -- ends here
                        */

                        return view('admin.verticaltotimeline',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));
                    }
                    else
                    {

                        if($branch!="All")
                        {
                            // dd("branch");

                            // Code for displaying the counts the Admin Page for different statuses
                            $leads = DB::table('leads')
                            ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->where('ServiceStatus',$status)
                            ->where('Branch',$branch)
                            ->orderBy('leads.id','DESC')
                            ->get();

                            $leads = json_decode($leads,true);

                            $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                            ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                            ,'Emergency District','Emergency State','Emergency PinCode');
                            // $list = array (
                            //     $leads
                            // );
                            //
                            //         $lists = array (
                            // array('aaa', 'bbb', 'ccc', 'dddd'),
                            // array('123', '456', '789'),
                            // array('aaa', 'bbb')
                            // );
                            // dd($list);

                            $filename = "download.csv";

                            $fp = fopen('download.csv', 'w');

                            fputcsv($fp, $array );
                            foreach ($leads as $fields) {
                                fputcsv($fp, $fields);
                            }

                            fclose($fp);

                            $headers = array(
                                'Content-Type' => 'text/csv',
                            );
                            return Response::download($filename, 'download.csv', $headers);


                            /*
                            Logic for downloading CSV goes here -- ends here
                            */


                            return view('admin.Branchfilteralone',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));

                        }
                        else
                        {
                            if($vertical!="All")
                            {

                                // dd("v");


                                // check the vertical selected based on that change the array value

                                if($vertical=="NursingServices")
                                {
                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                }

                                if($vertical=="PersonalSupportiveCare")
                                {
                                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                                }

                                if($vertical=="Mathrutvam-Baby Care")
                                {
                                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                                }

                                if($vertical=="Physiotherapy-Home")
                                {
                                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                                }





                                // Code for displaying the counts the if only vertical is selected
                                $leads = DB::table('leads')
                                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                ->join('services', 'leads.id', '=', 'services.LeadId')
                                ->where('ServiceStatus',$status)
                                ->wherein('ServiceType',$vertical1)

                                ->orderBy('leads.id','DESC')
                                ->get();

                                $leads = json_decode($leads,true);

                                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                                ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                                ,'Emergency District','Emergency State','Emergency PinCode');
                                // $list = array (
                                //     $leads
                                // );
                                //
                                //         $lists = array (
                                // array('aaa', 'bbb', 'ccc', 'dddd'),
                                // array('123', '456', '789'),
                                // array('aaa', 'bbb')
                                // );
                                // dd($list);

                                $filename = "download.csv";

                                $fp = fopen('download.csv', 'w');

                                fputcsv($fp, $array );
                                foreach ($leads as $fields) {
                                    fputcsv($fp, $fields);
                                }

                                fclose($fp);

                                $headers = array(
                                    'Content-Type' => 'text/csv',
                                );
                                return Response::download($filename, 'download.csv', $headers);


                                /*
                                Logic for downloading CSV goes here -- ends here
                                */


                                return view('admin.VerticalFilteralone',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));
                            }
                            else
                            {
                                if($Fromdate!="NULL" && $Todate!="NULL")
                                {
                                    $Fromdate=$Fromdate;

                                    $Todate=$Todate;


                                    // dd("time");

                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    $vertical2 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

                                    $vertical3 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

                                    $vertical4 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

                                    $vertical5 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia","Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy", "Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    // These counts show all the leads with their respective status from start date to end date


                                    $leads = DB::table('leads')
                                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                    ->join('services', 'leads.id', '=', 'services.LeadId')
                                    ->where('ServiceStatus',$status)
                                    ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                                    ->orderBy('leads.id','DESC')
                                    ->get();

                                    $leads = json_decode($leads,true);

                                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                                    ,'Emergency District','Emergency State','Emergency PinCode');
                                    // $list = array (
                                    //     $leads
                                    // );
                                    //
                                    //         $lists = array (
                                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                                    // array('123', '456', '789'),
                                    // array('aaa', 'bbb')
                                    // );
                                    // dd($list);

                                    $filename = "download.csv";

                                    $fp = fopen('download.csv', 'w');

                                    fputcsv($fp, $array );
                                    foreach ($leads as $fields) {
                                        fputcsv($fp, $fields);
                                    }

                                    fclose($fp);

                                    $headers = array(
                                        'Content-Type' => 'text/csv',
                                    );
                                    return Response::download($filename, 'download.csv', $headers);


                                    /*
                                    Logic for downloading CSV goes here -- ends here
                                    */


                                    return view('admin.timelineonly',compact('designation','branch','vertical','Fromdate','Todate','newcountforall'));
                                }
                                else
                                {
                                    dd("nothing selected");
                                }
                            }
                        }



                    }
                }
            }
        }

        /* Filter download code ends here */
    }




    public function homeadminmobileindividualgraph()
    {
        $data = DB::table('cities')->get();

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $newcountforall = DB::table('services')
        ->where('ServiceStatus',$status0)
        ->count();

        $inprogresscountforall = DB::table('services')
        ->where('ServiceStatus',$status1)
        ->count();

        $convertedcountforall = DB::table('services')
        ->where('ServiceStatus',$status2)
        ->count();

        $droppedcountforall = DB::table('services')
        ->where('ServiceStatus',$status3)
        ->count();

        $deferredcountforall = DB::table('services')
        ->where('ServiceStatus',$status4)
        ->count();

$provisionalcount=DB::table('provisionalleads')->where('active',1)->count();

        return view('admin.indivdualgrapofadmin',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        'deferredcountforall','reassignedcountforall','existingcountforall','assignedcountforall','provisionalcount'));
    }

     public function homeadminmobileindividualgraphonclcik()
    {
        $data = DB::table('cities')->get();

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $newcountforall = DB::table('services')
        ->where('ServiceStatus',$status0)
        ->count();

        $inprogresscountforall = DB::table('services')
        ->where('ServiceStatus',$status1)
        ->count();

        $convertedcountforall = DB::table('services')
        ->where('ServiceStatus',$status2)
        ->count();

        $droppedcountforall = DB::table('services')
        ->where('ServiceStatus',$status3)
        ->count();

        $deferredcountforall = DB::table('services')
        ->where('ServiceStatus',$status4)
        ->count();


        return view('admin.indivdualgrapofadminonclick',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        'deferredcountforall','reassignedcountforall','existingcountforall','assignedcountforall'));
    }

    public function homeadminmobile()
    {


    // This is come from the following route
        // Route::get('admin/home','AdminController@index');

        $data = DB::table('cities')->get();
        $designation="home";

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //extracting the branch where the employee is currently working
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        //extracting the department ,i.e., Nursing Services, Physiotherapy, Mathrutvam , Personal Supportive Care
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        //Code for displaying the counts on the Admin Page for different statuses
        // We are displaying all leads with the above statuses
        $newcountforall = DB::table('services')
        ->where('ServiceStatus',$status0)
        ->count();

        // dd($newcountforall);

        $inprogresscountforall = DB::table('services')
        ->where('ServiceStatus',$status1)
        ->count();

        $convertedcountforall = DB::table('services')
        ->where('ServiceStatus',$status2)
        ->count();

        $droppedcountforall = DB::table('services')
        ->where('ServiceStatus',$status3)
        ->count();

        $deferredcountforall = DB::table('services')
        ->where('ServiceStatus',$status4)
        ->count();

        // Now we are aiming for all the statuses respective to the verticals in the Health Heal team
        //Counting the number of verticals in Health Heal
        // dd($verticals_count);

        //Extract the names of all the Vertical Heads
        $vertical_names =  DB::table('employees')->select('FirstName','city','Department')->where('Designation','Vertical Head')->groupBy('city')->groupBy('Department')->get();


        //converting the names of all Verticals into an Array
        $vertical_names = json_decode($vertical_names,true);

        $verticals_count = count($vertical_names);

        // dd($vertical_names);

        $branch_head_names = DB::table('employees')->select('FirstName','city','Department')->where('Designation','Branch Head')->groupBy('city')->groupBy('Department')->get();

        $branch_head_names = json_decode($branch_head_names,true);

        // dd($branch_head_names);
        $branch_head_count = count($branch_head_names);

        // dd($branch_head_count);
        //Initalizing Vertical_names array with Null
        $Vertical_names = null;

        //Extracting the vertical names from the above array and storing it in a 1-D array
        for($i=0;$i<$verticals_count;$i++)
        {
            $Vertical_names[] = $vertical_names[$i]['FirstName'];
        }



        // dd($Vertical_names,$verticals_count);
        for($i=0;$i<$branch_head_count;$i++)
        {
            $Vertical_names[] = $branch_head_names[$i]['FirstName'];
        }

        // dd($Vertical_cities);

        // dd($Vertical_names);

        $city_name = null;
        $servicetype_name = null;

        $totalcount = $verticals_count + $branch_head_count;
        // dd($totalcount);
        // dd($Vertical_names);

        //For every Vertical, Calculating the individual counts for their statuses
        for($j=0;$j<$totalcount;$j++)
        {
            /* Retrieving the counts of all the users -- started */
            //assigning the possible statuses for the Coordinator

            $status0 = "New";
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            //extracting the name of the presently accessed Vertical
            $vertical = $Vertical_names[$j];
            // dd($Vertical_names);

            // extracting the department of that vertical
            $dept = DB::table('employees')->where('FirstName',$vertical)->value('Department');
            $desig = DB::table('employees')->where('FirstName',$vertical)->value('Designation');
            // dd($dept);
            // extracting the user city of that vertical
            $user_city = DB::table('employees')->where('FirstName',$vertical)->value('city');

            //retrieving the values of all the services for those people who are not from Bangalore, i.e. Pune, Chennai, Hyderabad, Hubbli
            $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $city_name[] = $user_city;
            $servicetype_name[] = $dept;

            //assigning default value as "NULL" for all variables

            $newcount2 = $inprogresscount2 = $convertedcount2 = $droppedcount2 = $deferredcount2 = NULL;

            // dd($Vertical_cities[0]);
            //if the user city is anything other than Bangalore, extract the counts on the basis of the service statuses mentioned in Servicearray5
            //branch is relative to that vertical
            //status can be "New", "In progress " etc.

            if($desig=="Vertical Head")
            {
                if($user_city=="Bengaluru" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Pune" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Hyderabad" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Chennai" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses
                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
                else if($user_city=="Hubballi-Dharwad" )
                {
                    //initialize the array as "NULL" since the array cannot be passed values in a for loop
                    $vertical1=null;
                    // dd($dept);

                    //if the department is Nursing Services (Handled by Yathisha), set the Services available under that department
                    if($dept=="Nursing Services")
                    {
                        $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];
                    }

                    //if the department is Personal Supportive Care (Handled by Santhosh), set the Services available under that department
                    if($dept=="Personal Supportive Care")
                    {
                        $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                    }

                    //if the department is Mathrutvam Baby care (Handled by Sowmya), set the Services available under that department
                    if($dept=="Mathrutvam - Baby Care")
                    {
                        $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                    }

                    //if the department is Physiotheraphy (Handled by Sowmya), set the Services available under that department
                    if($dept=="Physiotherapy - Home")
                    {
                        $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                    }
                    // dd($vertical1);

                    //Depending on the department that was extracted from the Db, the ServiceType was set
                    //Now extract the count on the basis of this serviceType and all statuses



                    $newcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status0)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    // dd($newcount2);

                    $inprogresscount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status1)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                    $convertedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status2)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $droppedcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status3)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();

                    $deferredcount2 = DB::table('services')
                    ->where('Branch',$user_city)
                    ->where('ServiceStatus',$status4)
                    ->wherein('ServiceType',$vertical1)
                    ->orderBy('id', 'DESC')
                    ->count();


                }
            }
            else if($desig=="Branch Head")
            {
                $newcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status0)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $inprogresscount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status1)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();


                $convertedcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status2)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status3)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();

                $deferredcount2 = DB::table('services')
                ->where('Branch',$user_city)
                ->where('ServiceStatus',$status4)
                ->wherein('ServiceType',$Servicesarray5)
                ->orderBy('id', 'DESC')
                ->count();



            }

            // dd($servicetype_name);

            // dd($deferredcount2);
            //putting all status counts inside a 2-D array for dynamicity
            $statuscounts[] = array
            (
                "0"=>$newcount2,"1"=>$inprogresscount2, "2" =>$convertedcount2,"3"=>$droppedcount2,"4"=>$deferredcount2
            );
            // dd($statuscounts);
        }

        // dd($Vertical_names);
        // dd($Vertical_names, ' ', $statuscounts);

        // Fetching Product count details to show on dashboard

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // $designation="productmanager";


        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";
        // dd($check);

        $newcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();


        $newcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount=count($newcountamount);

        $newcountamount=json_decode($newcountamount,true);

        $newsum=0;
        for($i=0;$i<$countamount;$i++)
        {
            $newsum+= $newcountamount[$i]['FinalAmount'];
        }




        $processingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $processingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount1=count($processingcountamount);

        $processingcountamount=json_decode($processingcountamount,true);

        $processingsum=0;
        for($i=0;$i<$countamount1;$i++)
        {
            $processingsum+= $processingcountamount[$i]['FinalAmount'];
        }


        $awaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $awaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();


        $countamount2=count($awaitingpickupcountamount);

        $awaitingpickupcountamount=json_decode($awaitingpickupcountamount,true);

        $awaitingpickupsum=0;
        for($i=0;$i<$countamount2;$i++)
        {
            $awaitingpickupsum+= $awaitingpickupcountamount[$i]['FinalAmount'];
        }



        $outfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $outfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount3=count($outfordeliverycountamount);

        $outfordeliverycountamount=json_decode($outfordeliverycountamount,true);

        $outfordeliverysum=0;
        for($i=0;$i<$countamount3;$i++)
        {
            $outfordeliverysum+= $outfordeliverycountamount[$i]['FinalAmount'];
        }




        $readytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $readytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount4=count($readytoshipcountamount);

        $readytoshipcountamount=json_decode($readytoshipcountamount,true);

        $readytoshipsum=0;
        for($i=0;$i<$countamount4;$i++)
        {
            $readytoshipsum+= $readytoshipcountamount[$i]['FinalAmount'];
        }



        $orderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $orderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount5=count($orderreturncountamount);

        $orderreturncountamount=json_decode($orderreturncountamount,true);

        $orderreturnsum=0;
        for($i=0;$i<$countamount5;$i++)
        {
            $orderreturnsum+= $orderreturncountamount[$i]['FinalAmount'];
        }




        $recievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $recievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount30=count($recievedorderreturncountamount);

        $recievedorderreturncountamount=json_decode($recievedorderreturncountamount,true);

        $recievedorderreturnsum=0;
        for($i=0;$i<$countamount30;$i++)
        {
            $recievedorderreturnsum+= $recievedorderreturncountamount[$i]['FinalAmount'];
        }





        $canceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $canceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount6=count($canceledcountamount);

        $canceledcountamount=json_decode($canceledcountamount,true);

        $canceledsum=0;
        for($i=0;$i<$countamount6;$i++)
        {
            $canceledsum+= $canceledcountamount[$i]['FinalAmount'];
        }




        $deliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $deliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount7=count($deliveredcountamount);

        $deliveredcountamount=json_decode($deliveredcountamount,true);

        $deliveredsum=0;
        for($i=0;$i<$countamount7;$i++)
        {
            $deliveredsum+= $deliveredcountamount[$i]['FinalAmount'];
        }





        $newcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $newcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount8=count($newcountrentamount);

        $newcountrentamount=json_decode($newcountrentamount,true);

        $newrentsum=0;
        for($i=0;$i<$countamount8;$i++)
        {
            $newrentsum+= $newcountrentamount[$i]['FinalAmount'];
        }





        $processingcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $processingcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();
        // dd($processingcountrentamount);

        $countamount9=count($processingcountrentamount);

        $processingcountrentamount=json_decode($processingcountrentamount,true);

        $processingrentsum=0;
        for($i=0;$i<$countamount9;$i++)
        {
            $processingrentsum+= $processingcountrentamount[$i]['FinalAmount'];
        }


        $awaitingpickupcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $awaitingpickupcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount10=count($awaitingpickupcountrentamount);

        $awaitingpickupcountrentamount=json_decode($awaitingpickupcountrentamount,true);

        $awaitingpickuprentsum=0;
        for($i=0;$i<$countamount10;$i++)
        {
            $awaitingpickuprentsum+= $awaitingpickupcountrentamount[$i]['FinalAmount'];
        }


        // dd($awaitingpickupcountrentamount);



        $outfordeliverycountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $outfordeliverycountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount11=count($outfordeliverycountrentamount);

        $outfordeliverycountrentamount=json_decode($outfordeliverycountrentamount,true);

        $outfordeliveryrentsum=0;
        for($i=0;$i<$countamount11;$i++)
        {
            $outfordeliveryrentsum+= $outfordeliverycountrentamount[$i]['FinalAmount'];
        }






        $readytoshipcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $readytoshipcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount12=count($readytoshipcountrentamount);

        $readytoshipcountrentamount=json_decode($readytoshipcountrentamount,true);

        $readytoshiprentsum=0;
        for($i=0;$i<$countamount12;$i++)
        {
            $readytoshiprentsum+= $readytoshipcountrentamount[$i]['FinalAmount'];
        }








        $orderreturncountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $orderreturncountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount13=count($orderreturncountrentamount);

        $orderreturncountrentamount=json_decode($orderreturncountrentamount,true);

        $orderreturnrentsum=0;
        for($i=0;$i<$countamount13;$i++)
        {
            $orderreturnrentsum+= $orderreturncountrentamount[$i]['FinalAmount'];
        }




        $recievedorderreturncountrent =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $recievedorderreturncountrentamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount26=count($recievedorderreturncountrentamount);

        $recievedorderreturncountrentamount=json_decode($recievedorderreturncountrentamount,true);

        $recievedorderreturnrentsum=0;
        for($i=0;$i<$countamount26;$i++)
        {
            $recievedorderreturnrentsum+= $recievedorderreturncountrentamount[$i]['FinalAmount'];
        }


        // dd($orderreturncountrentamount);





        $canceledcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $canceledcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount14=count($canceledcountrentamount);

        $canceledcountrentamount=json_decode($canceledcountrentamount,true);

        $canceledrentsum=0;
        for($i=0;$i<$countamount14;$i++)
        {
            $canceledrentsum+= $canceledcountrentamount[$i]['FinalAmount'];
        }






        $deliveredcountrent = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $deliveredcountrentamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('OrderStatusrent',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount15=count($deliveredcountrentamount);

        $deliveredcountrentamount=json_decode($deliveredcountrentamount,true);

        $deliveredrentsum =0;
        for($i=0;$i<$countamount15;$i++)
        {
            $deliveredrentsum += $deliveredcountrentamount[$i]['FinalAmount'];
        }







        // product count end



        // Fetching pharmacy details count


        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";

        $pnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pnewcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount16=count($pnewcountamount);

        $pnewcountamount=json_decode($pnewcountamount,true);

        $pnewsum =0;
        for($i=0;$i<$countamount16;$i++)
        {
            $pnewsum += $pnewcountamount[$i]['PFinalAmount'];
        }






        $pprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pprocessingcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount17=count($pprocessingcountamount);

        $pprocessingcountamount=json_decode($pprocessingcountamount,true);

        $pprocessingsum=0;
        for($i=0;$i<$countamount17;$i++)
        {
            $pprocessingsum += $pprocessingcountamount[$i]['PFinalAmount'];
        }








        $pawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pawaitingpickupcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount18=count($pawaitingpickupcountamount);

        $pawaitingpickupcountamount=json_decode($pawaitingpickupcountamount,true);

        $pawaitingpickupsum =0;
        for($i=0;$i<$countamount18;$i++)
        {
            $pawaitingpickupsum += $pawaitingpickupcountamount[$i]['PFinalAmount'];
        }






        $poutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $poutfordeliverycountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount19=count($poutfordeliverycountamount);

        $poutfordeliverycountamount=json_decode($poutfordeliverycountamount,true);

        $poutfordeliverysum =0;
        for($i=0;$i<$countamount19;$i++)
        {
            $poutfordeliverysum += $poutfordeliverycountamount[$i]['PFinalAmount'];
        }







        $preadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $preadytoshipcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount20=count($preadytoshipcountamount);

        $preadytoshipcountamount=json_decode($preadytoshipcountamount,true);

        $preadytoshipsum =0;
        for($i=0;$i<$countamount20;$i++)
        {
            $preadytoshipsum += $preadytoshipcountamount[$i]['PFinalAmount'];
        }








        $porderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $porderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount21=count($porderreturncountamount);

        $porderreturncountamount=json_decode($porderreturncountamount,true);

        $porderreturnsum =0;
        for($i=0;$i<$countamount21;$i++)
        {
            $porderreturnsum += $porderreturncountamount[$i]['PFinalAmount'];
        }



        $precievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();

        $precievedorderreturncountamount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount31=count($precievedorderreturncountamount);

        $precievedorderreturncountamount=json_decode($precievedorderreturncountamount,true);

        $precievedorderreturnsum =0;
        for($i=0;$i<$countamount31;$i++)
        {
            $precievedorderreturnsum += $precievedorderreturncountamount[$i]['PFinalAmount'];
        }








        $pcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcanceledcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount22=count($pcanceledcountamount);

        $pcanceledcountamount=json_decode($pcanceledcountamount,true);

        $pcanceledsum =0;
        for($i=0;$i<$countamount22;$i++)
        {
            $pcanceledsum += $pcanceledcountamount[$i]['PFinalAmount'];
        }







        $pdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        $pdeliveredcountamount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $countamount23=count($pdeliveredcountamount);

        $pdeliveredcountamount=json_decode($pdeliveredcountamount,true);

        $pdeliveredsum =0;
        for($i=0;$i<$countamount23;$i++)
        {
            $pdeliveredsum += $pdeliveredcountamount[$i]['PFinalAmount'];
        }

        // pharmacy end

        // All city data

        $city_array = DB::table('cities')->get();
        $city_array = json_decode($city_array, true);

        //Bengaluru
        $branch1 = $city_array[0]['name'];

        //Chennai
        $branch2 = $city_array[1]['name'];

        //Pune
        $branch3 = $city_array[2]['name'];

        //Hyderabad
        $branch4 = $city_array[3]['name'];

        //Hubballi-Dharwad
        $branch5 = $city_array[4]['name'];

        $status0 = "New";
        $status1 = "Processing";
        $status2  = "Awaiting Pickup";
        $status3  = "Out for Delivery";
        $status4  = "Ready to ship";
        $status5  = "Order Return";
        $status6  = "Cancelled";
        $status7  = "Delivered";
        $status8  = "Received Order Return";

        /*Branch wise counts for product selling */

        //for bangalore
        $pbsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch1)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pbsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pbsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pbscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pbsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch1)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Chennai
        $pcsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch2)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pcsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pcsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pcscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch2)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Pune
        $ppsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch3)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $ppsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $ppsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $ppscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $ppsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch3)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hyderabad
        $phsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch4)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->orderBy('id', 'DESC')
        ->count();

        $phsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch4)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hubballi-Dharwad
        $phdsnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('addresses.City',$branch5)
        ->where('OrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phdsprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phdsrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phdscanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phdsdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Sell")
        ->where('City',$branch5)
        ->where('OrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        /*
        Branch wise counts for products rent
        */

        //for bangalore
        $pbrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch1)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pbrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pbrawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pbroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pbrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pbrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch1)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Chennai
        $pcrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch2)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pcrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pcrawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pcroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pcrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch2)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pcrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('OrderStatusrent',$status7)
        ->where('City',$branch2)
        ->orderBy('id', 'DESC')
        ->count();

        //for Pune
        $pprnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch3)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $pprprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pprawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pproutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $pprreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('OrderStatusrent',$status4)
        ->where('City',$branch3)
        ->orderBy('id', 'DESC')
        ->count();


        $pprorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $pprrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pprcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pprdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch3)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hyderabad
        $phrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch4)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phrawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phrreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch4)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //for Hubballi-Dharwad
        $phdrnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('addresses.City',$branch5)
        ->where('OrderStatusrent',$status0)
        ->orderBy('id', 'DESC')
        ->count();



        $phdrprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $phdrawaitingpickuppount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $phdroutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status3)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrreadytoshippount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status4)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status5)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $phdrcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $phdrdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','products.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'leads.id', '=', 'products.leadid')
        ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
        ->where('products.Type',"Rent")
        ->where('City',$branch5)
        ->where('OrderStatusrent',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        /* Branch wise counts for pharmacy */
        // Pharmacy count for all status

        //For bangalore
        $pharmacybnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyboutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyborderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacybcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacybdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch1)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();
        // dd($servicetype_name);

        //For chennai
        $pharmacycnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyccanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacycdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch2)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Pune
        $pharmacypnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyporderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyprecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacypcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacypdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch3)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Hyderabad
        $pharmacyhnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyhcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch4)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();

        //For Hubballi-Dharwad
        $pharmacyhdnewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status0)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status1)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status2)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status3)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status4)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status5)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhdrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status8)
        ->orderBy('id', 'DESC')
        ->count();


        $pharmacyhdcanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status6)
        ->orderBy('id', 'DESC')
        ->count();

        $pharmacyhddeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('City',$branch5)
        ->where('POrderStatus',$status7)
        ->orderBy('id', 'DESC')
        ->count();
            $randomnumbers = array(10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012);
            


            $provisionalcount=DB::table('provisionalleads')->where('active',1)->count();


        return view('admin.homemobile',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        'deferredcountforall','verticals_count','randomnumbers','totalcount','Vertical_names','statuscounts','designation','city_name','servicetype_name','newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','newcountrent','processingcountrent','awaitingpickupcountrent','outfordeliverycountrent','readytoshipcountrent','orderreturncountrent','canceledcountrent','deliveredcountrent','pnewcount','pprocessingcount','pawaitingpickupcount','poutfordeliverycount','preadytoshipcount','porderreturncount','pcanceledcount','pdeliveredcount','newsum','processingsum','awaitingpickupsum','outfordeliverysum','readytoshipsum','orderreturnsum','canceledsum','deliveredsum','newrentsum','processingrentsum','awaitingpickuprentsum','outfordeliveryrentsum','readytoshiprentsum','orderreturnrentsum','canceledrentsum','deliveredrentsum','pnewsum','pprocessingsum','pawaitingpickupsum','poutfordeliverysum','preadytoshipsum','porderreturnsum','pcanceledsum','pdeliveredsum','precievedorderreturncount','recievedorderreturncountrent','recievedorderreturncount','recievedorderreturnsum','recievedorderreturnrentsum','precievedorderreturnsum',
        'pbsnewcount','pbsprocessingcount','pbsawaitingpickupcount','pbsoutfordeliverycount','pbsreadytoshipcount','pbsorderreturncount','pbsrecievedorderreturncount','pbscanceledcount','pbsdeliveredcount','pcsnewcount','pcsprocessingcount','pcsawaitingpickupcount','pcsoutfordeliverycount','pcsreadytoshipcount','pcsorderreturncount','pcsrecievedorderreturncount','pcscanceledcount','pcsdeliveredcount','ppsnewcount','ppsprocessingcount','ppsawaitingpickuppount',
        'ppsoutfordeliverycount','ppsreadytoshippount','ppsorderreturncount','ppsrecievedorderreturncount','ppscanceledcount','ppsdeliveredcount','phsnewcount','phsprocessingcount','phsawaitingpickuppount','phsoutfordeliverycount'
        ,'phsreadytoshippount','phsorderreturncount','phsrecievedorderreturncount','phscanceledcount','phsdeliveredcount','phdsnewcount','phdsprocessingcount','phdsawaitingpickuppount','phdsoutfordeliverycount','phdsreadytoshippount','phdsorderreturncount','phdsrecievedorderreturncount','phdscanceledcount',
        'phdsdeliveredcount','pbrnewcount','pbrprocessingcount','pbrawaitingpickupcount','pbroutfordeliverycount','pbrreadytoshipcount','pbrorderreturncount','pbrrecievedorderreturncount','pbrcanceledcount','pbrdeliveredcount','pcrnewcount','pcrprocessingcount','pcrawaitingpickupcount','pcroutfordeliverycount','pcrreadytoshipcount','pcrorderreturncount','pcrrecievedorderreturncount','pcrcanceledcount','pcrdeliveredcount','pprnewcount','pprprocessingcount','pprawaitingpickuppount','pproutfordeliverycount','pprreadytoshippount','pprorderreturncount','pprrecievedorderreturncount'
        ,'pprcanceledcount','pprdeliveredcount','pprnewcount','pprprocessingcount','pprawaitingpickuppount','pproutfordeliverycount','pprreadytoshippount','pprorderreturncount','pprrecievedorderreturncount','pprcanceledcount','pprdeliveredcount','phrnewcount','phrprocessingcount','phrawaitingpickuppount','phroutfordeliverycount','phrreadytoshippount','phrorderreturncount','phrrecievedorderreturncount','phrcanceledcount','phrdeliveredcount','phdrnewcount',
        'phdrprocessingcount','phdrawaitingpickuppount','phdroutfordeliverycount','phdrreadytoshippount','phdrorderreturncount','phdrrecievedorderreturncount','phdrcanceledcount','phdrdeliveredcount'
        ,'pharmacybnewcount','pharmacybprocessingcount','pharmacybawaitingpickupcount','pharmacyboutfordeliverycount','pharmacybreadytoshipcount','pharmacyborderreturncount','pharmacybrecievedorderreturncount','pharmacybcanceledcount','pharmacybdeliveredcount','pharmacycnewcount','pharmacycprocessingcount','pharmacycawaitingpickupcount','pharmacycoutfordeliverycount','pharmacycreadytoshipcount',
        'pharmacycorderreturncount','pharmacycrecievedorderreturncount','pharmacyccanceledcount','pharmacycdeliveredcount','pharmacypnewcount','pharmacypprocessingcount','pharmacypawaitingpickupcount','pharmacypoutfordeliverycount','pharmacypreadytoshipcount','pharmacyporderreturncount','pharmacyprecievedorderreturncount','pharmacypcanceledcount','pharmacypdeliveredcount', 'pharmacyhnewcount','pharmacyhnewcount', 'pharmacyhprocessingcount','pharmacyhawaitingpickupcount',
        'pharmacyhoutfordeliverycount','pharmacyhreadytoshipcount','pharmacyhorderreturncount','pharmacyhrecievedorderreturncount','pharmacyhcanceledcount','pharmacyhdeliveredcount','pharmacyhdnewcount','pharmacyhdprocessingcount','pharmacyhdawaitingpickupcount','pharmacyhdoutfordeliverycount','pharmacyhdreadytoshipcount','pharmacyhdorderreturncount','pharmacyhdrecievedorderreturncount','pharmacyhdcanceledcount','pharmacyhddeliveredcount','provisionalcount'));


    }




    public function verticalgraphofadmin()
    {


        $data = DB::table('cities')->get();

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        $newcountforall = DB::table('services')
        ->where('ServiceStatus',$status0)
        ->count();

        $inprogresscountforall = DB::table('services')
        ->where('ServiceStatus',$status1)
        ->count();

        $convertedcountforall = DB::table('services')
        ->where('ServiceStatus',$status2)
        ->count();

        $droppedcountforall = DB::table('services')
        ->where('ServiceStatus',$status3)
        ->count();

        $deferredcountforall = DB::table('services')
        ->where('ServiceStatus',$status4)
        ->count();

        $verticals_count = DB::table('employees')->where('Designation','Vertical Head')->count();
        // dd($verticals_count);

        $vertical_names =  DB::table('employees')->select('FirstName')->where('Designation','Vertical Head')->get();


        $vertical_names = json_decode($vertical_names,true);

        // dd($vertical_names);

        $Vertical_names = null;
        for($i=0;$i<$verticals_count;$i++)
        {
            $Vertical_names[] = $vertical_names[$i]['FirstName'];
        }

        // dd($Vertical_names);


        for($j=0;$j<$verticals_count;$j++)
        {
            /* Retrieving the counts of all the users -- started */
            //assigning the possible statuses for the Coordinator

            $status0 = "New";
            $status1 = "In Progress";
            $status2  = "Converted";
            $status3  = "Dropped";
            $status4  = "Deferred";

            $vertical = $Vertical_names[$j];
            // dd($Vertical_names);

            $user_city = DB::table('employees')->where('FirstName',$vertical)->value('city');

            $Servicesarray1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            $Servicesarray2 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $Servicesarray3 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $Servicesarray4 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

            $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $Servicesarray6 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $Servicesarray7 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];


            $Servicesarray8 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $ServiceTypearray[] = array(
                "0" => $Servicesarray3,
                "1" =>$Servicesarray4,
                "2"=>$Servicesarray2,
                "3"=>$Servicesarray1,
                "4"=>$Servicesarray5,
                "5"=>$Servicesarray6,
                "6"=>$Servicesarray7,
                "7"=>$Servicesarray8,
            );

            $newcount2 = DB::table('services')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$ServiceTypearray[0][$j])
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount2 = DB::table('services')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$ServiceTypearray[0][$j])
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount2 = DB::table('services')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$ServiceTypearray[0][$j])
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount2 = DB::table('services')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$ServiceTypearray[0][$j])
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount2 = DB::table('services')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$ServiceTypearray[0][$j])
            ->orderBy('id', 'DESC')
            ->count();

            $statuscounts[] = array
            (
                "0" => $newcount2,"1"=>$inprogresscount2, "2" =>$convertedcount2,"3"=>$droppedcount2,"4"=>$deferredcount2
            );
            // dd($statuscounts);
        }

        // dd($verticals_count);


        return view('admin.verticalgraphofadmin',compact('data','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall',
        'deferredcountforall','verticals_count','Vertical_names','statuscounts'));
    }

    //This function is for retrieving all the counts when the filter is applied for graphs
    public function searchfilteradmin(Request $request)
    {
        // This route is coming from : Route::get('searchfilteradmin','AdminController@searchfilteradmin');

        //Whatever fields are selected by the Admin, retrieve them
        $branch = $request->Branch;
        $vertical = $request->Vertial;
        $Fromdate = $request->FromDate;
        $Todate = $request->ToDate;

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        //retrieving the designation of the people who are logged in
        $designation=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //retrieving the branch of the people(which city they belong to) who are logged in
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        //retrieving the "serviceType" ,i.e., Department(eg - Nursing Services, PSC) etc
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        //Assigning all services depending on ServiceType to specific arrays
        $Servicesarray1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

        $Servicesarray2 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

        $Servicesarray3 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        $Servicesarray4 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

        $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //if branch , vertical and time i.e. (all fields) are selected
        //Here we wish to extract the counts of all the leads, related to the branch, the department they
        // belong to and the start and end date when those leads were made
        if($branch!="All" && $vertical!="All" && $Fromdate!=NULL && $Todate!=NULL)
        {
            // dd("b v and t");

            //attach time to the date entered
            $Fromdate=$Fromdate." 00:00:00";
            $Todate=$Todate." 23:59:59";

            // Depending on the type of Department returned, pass the services for that department in an array
            if($vertical=="NursingServices")
            {
                $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            }

            if($vertical=="PersonalSupportiveCare")
            {
                $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
            }

            if($vertical=="Mathrutvam-Baby Care")
            {
                $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
            }

            if($vertical=="Physiotherapy-Home")
            {
                $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
            }

            // Calculating the counts for different statuses depending on branch, department and start and end time

            $newcountforall = DB::table('services')
            ->where('ServiceStatus',$status0)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();


            $inprogresscountforall = DB::table('services')
            ->where('ServiceStatus',$status1)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();
            // // dd($inprogresscountforall);

            $convertedcountforall = DB::table('services')
            ->where('ServiceStatus',$status2)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();

            $droppedcountforall = DB::table('services')
            ->where('ServiceStatus',$status3)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();
            // dd($droppedcountforall);
            

            $deferredcountforall = DB::table('services')
            ->where('ServiceStatus',$status4)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();

            return view('admin.allfilter',compact('designation','designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
        }
        else
        {
            //This is the scenario where the branch and the Department are selected
            //we are trying to extract all the leads for a particular city and a particular Department
            if($branch!="All" && $vertical!="All")
            {


                // dd("b and v");

                // Depending on the type of Department returned, pass the services for that department in an array
                if($vertical=="NursingServices")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($vertical=="PersonalSupportiveCare")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($vertical=="Mathrutvam-Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($vertical=="Physiotherapy-Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }




                // Calculating the counts for different statuses depending on branch and department

                $newcountforall = DB::table('services')
                ->where('ServiceStatus',$status0)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();
                // dd($newcountforall);

                $inprogresscountforall = DB::table('services')
                ->where('ServiceStatus',$status1)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();
                // // dd($inprogresscountforall);

                $convertedcountforall = DB::table('services')
                ->where('ServiceStatus',$status2)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();

                $droppedcountforall = DB::table('services')
                ->where('ServiceStatus',$status3)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();

                $deferredcountforall = DB::table('services')
                ->where('ServiceStatus',$status4)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();


                return view('admin.branchtoverticalfilter',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
            }
            else
            {
                //This is the scenario where the branch and the start and end date are selected
                //we are trying to extract all the leads for a particular city between a particular date
                //Here we also want to extract the count of ALL statuses for EACH department
                if($branch!="All" && $Fromdate!=NULL && $Todate!=NULL)
                {
                    //attach time to the date entered
                    $Fromdate=$Fromdate." 00:00:00";

                    $Todate=$Todate." 23:59:59";

                    // dd("b and t");

                    // Calculating the counts for different statuses depending on branch, start and end time

                    $newcountforall = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // dd($newcountforall);

                    $inprogresscountforall = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // // dd($inprogresscountforall);

                    $convertedcountforall = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforall = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforall = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();





                    // Calculating the Counts for each Branch for all statuses between 2 dates


                    //for nursing service

                    $newcountforns = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // dd($newcountforns);


                    $inprogresscountforns = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $convertedcountforns = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforns = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforns = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    //for PSC

                    $newcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();





                    //for Mathrutvam

                    $newcountformath = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountformath = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountformath = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountformath = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountformath = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    //for physiotherapy

                    $newcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountforphy = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    return view('admin.branchtotimeline',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforns','inprogresscountforns','convertedcountforns','droppedcountforns','deferredcountforns','newcountforpsc','inprogresscountforpsc','convertedcountforpsc','droppedcountforpsc','deferredcountforpsc','newcountformath','inprogresscountformath','convertedcountformath','droppedcountformath','deferredcountformath','newcountforphy','inprogresscountforphy','convertedcountforphy','droppedcountforphy','deferredcountforphy'));
                }
                else
                {
                    //This is the scenario where the department and the start and end date are selected
                    //we are trying to extract all the leads for a particular department between a particular date
                    //Here we also want to extract the count of ALL statuses for EACH department

                    if($vertical!="All" && $Fromdate!=NULL && $Todate!=NULL)
                    {
                        //attach time to the date entered
                        $Fromdate=$Fromdate." 00:00:00";

                        $Todate=$Todate." 23:59:59";

                        // dd("V and t");

                        // check the vertical selected based on that change the array value

                        if($vertical=="NursingServices")
                        {
                            $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                        }

                        if($vertical=="PersonalSupportiveCare")
                        {
                            $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                        }

                        if($vertical=="Mathrutvam-Baby Care")
                        {
                            $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                        }

                        if($vertical=="Physiotherapy-Home")
                        {
                            $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                        }





                        // Calculating the counts for different statuses depending on department, start and end time

                        $newcountforall = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($newcountforall);

                        $inprogresscountforall = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // // dd($inprogresscountforall);

                        $convertedcountforall = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforall = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($droppedcountforall);

                        $deferredcountforall = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();





                        // Calculating the Counts for each Service Type for all statuses between 2 dates


                        //for Bengaluru

                        $newcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($newcountforbng);


                        $inprogresscountforbng = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $convertedcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        //for pune

                        $newcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforpune = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();





                        //for Chennai

                        $newcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforchen = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        //for Hubballi-Dharwad

                        $newcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforhub = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();



                        //for Hyderabad

                        $newcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        return view('admin.verticaltotimeline',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforbng','inprogresscountforbng','convertedcountforbng','droppedcountforbng','deferredcountforbng','newcountforchen','inprogresscountforchen','convertedcountforchen','droppedcountforchen','deferredcountforchen','newcountforpune','inprogresscountforpune','convertedcountforpune','droppedcountforpune','deferredcountforpune','newcountforhyd','inprogresscountforhyd','convertedcountforhyd','droppedcountforhyd','deferredcountforhyd','newcountforhub','inprogresscountforhub','convertedcountforhub','droppedcountforhub','deferredcountforhub'));
                    }
                    else
                    {

                        //This is the scenario where only branch are selected
                        //we are trying to extract all the leads for a particular city
                        //Here we also want to extract the count of ALL statuses for EACH department w.r.t to the branch

                        if($branch!="All")
                        {
                            // dd("branch");

                            // Code for displaying the counts the Admin Page for different statuses

                            $newcountforall = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->count();
                            // dd($newcountforall);

                            $inprogresscountforall = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->count();
                            // // dd($inprogresscountforall);

                            $convertedcountforall = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->count();

                            $droppedcountforall = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->count();

                            $deferredcountforall = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->count();




                            //code to calculate status count of each service in graph


                            //for nursing service

                            $newcountforns = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();
                            // dd($newcountforns);


                            $inprogresscountforns = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();


                            $convertedcountforns = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();

                            $droppedcountforns = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();

                            $deferredcountforns = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();


                            //for PSC

                            $newcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();


                            $inprogresscountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $convertedcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $droppedcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $deferredcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();





                            //for Mathrutvam

                            $newcountformath = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();


                            $inprogresscountformath = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $convertedcountformath = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $droppedcountformath = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $deferredcountformath = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();


                            //for physiotherapy

                            $newcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();


                            $inprogresscountforphy = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $convertedcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $droppedcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $deferredcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            // dd($newcountforall);


                            return view('admin.Branchfilteralone',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforns','inprogresscountforns','convertedcountforns','droppedcountforns','deferredcountforns','newcountforpsc','inprogresscountforpsc','convertedcountforpsc','droppedcountforpsc','deferredcountforpsc','newcountformath','inprogresscountformath','convertedcountformath','droppedcountformath','deferredcountformath','newcountforphy','inprogresscountforphy','convertedcountforphy','droppedcountforphy','deferredcountforphy'));

                        }
                        else
                        {
                            //This is the scenario where the vertical are selected
                            //we are trying to extract all the leads for a particular department
                            //Here we also want to extract the count of ALL statuses for EACH department

                            if($vertical!="All")
                            {

                                // dd($vertical);


                                // check the vertical selected based on that change the array value

                                if($vertical=="NursingServices")
                                {
                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                }

                                if($vertical=="PersonalSupportiveCare")
                                {
                                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                                }

                                if($vertical=="Mathrutvam-Baby Care")
                                {
                                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                                }

                                if($vertical=="Physiotherapy-Home")
                                {
                                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                                }





                                // Calculating the counts for different statuses depending on department

                                $newcountforall = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // dd($newcountforall);

                                $inprogresscountforall = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // // dd($inprogresscountforall);

                                $convertedcountforall = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforall = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforall = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                // Calculating the Counts for each Branch and Department for all statuses


                                //for Bengaluru

                                $newcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // dd($newcountforbng);


                                $inprogresscountforbng = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $convertedcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                //for pune

                                $newcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforpune = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();





                                //for Chennai

                                $newcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforchen = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                //for Hubballi-Dharwad

                                $newcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforhub = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                //for Hyderabad

                                $newcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                return view('admin.VerticalFilteralone',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforbng','inprogresscountforbng','convertedcountforbng','droppedcountforbng','deferredcountforbng','newcountforchen','inprogresscountforchen','convertedcountforchen','droppedcountforchen','deferredcountforchen','newcountforpune','inprogresscountforpune','convertedcountforpune','droppedcountforpune','deferredcountforpune','newcountforhyd','inprogresscountforhyd','convertedcountforhyd','droppedcountforhyd','deferredcountforhyd','newcountforhub','inprogresscountforhub','convertedcountforhub','droppedcountforhub','deferredcountforhub'));
                            }
                            else
                            {
                                //This is the scenario where the start and end date are selected
                                //we are trying to extract all the leads between a particular date
                                //Here we also want to extract the count of ALL statuses for EACH department

                                if($Fromdate!=Null && $Todate!=NULL)
                                {
                                    $Fromdate=$Fromdate." 00:00:00";

                                    $Todate=$Todate." 23:59:59";


                                    // dd("time");

                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    $vertical2 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

                                    $vertical3 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

                                    $vertical4 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

                                    $vertical5 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia","Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy", "Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    // Calculating the counts for different statuses depending on start and end time


                                    $newcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status0)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $inprogresscountforall = DB::table('services')
                                    ->where('ServiceStatus',$status1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $convertedcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $droppedcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $deferredcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    // Calculating the Counts for each Branch and Department for all statuses between 2 dates

                                    //counts for nursing services
                                    $bns = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //counts for Personal Supportive Care
                                    $bpsc = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    //counts for Mathrutvam
                                    $bm = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //counts for physiotherapy
                                    $bphy = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Pune
                                    $pns = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $ppsc = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $pm = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $pphy = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Chennai
                                    $cns = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $cpsc = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $cm = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $cphy = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Hyderabad
                                    $hns = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $hpsc = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $hm = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $hphy = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Hubballi-Dharwad
                                    $hdns = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $hdpsc = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $hdm = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $hdphy = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();




                                    return view('admin.timelineonly',compact('bns','bpsc','bm','bphy','pns','ppsc','pm','pphy','cns','cpsc','cm','cphy','hns','hpsc','hm','hphy','hdns','hdpsc','hdm','hdphy','designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
                                }
                                else
                                {
                                    //incase of no filter selected
                                    dd("nothing selected");
                                }
                            }
                        }




                    }
                }
            }
        }









    }

    //This is for retrieving the values so that they can be the details of each count on the filtered graph can be seen in a tabular form on clicking on a particular count
    public function  showfiltervalues()
    {
        // This function is coming from the route : Route::get('filtervalueshow', 'AdminController@showfiltervalues');
        //Will be trigerred when "Filter" is clicked on the Admin Dashboard

        $name = $_GET['name'];
        $status = $_GET['status'];
        $branch = $_GET['branch'];
        $Fromdate = $_GET['from'];
        $vertical = $_GET['vertical'];
        $Todate = $_GET['to'];

        // dd($Fromdate);
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        $designation=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        $Servicesarray1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

        $Servicesarray2 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

        $Servicesarray3 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        $Servicesarray4 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

        $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];





        //if branch , vertical and time all are selected
        if($branch!="All" && $vertical!="All" && $Fromdate!="NULL" && $Todate!="NULL")
        {

            // dd("b and v and t");
            $Fromdate=$Fromdate." 00:00:00";
            $Todate=$Todate." 23:59:59";



            // check the vertical selected based on that change the array value
            // dd($vertical);

            if($vertical=="NursingServices")
            {
                $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            }

            if($vertical=="PersonalSupportiveCare")
            {
                $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
            }

            if($vertical=="Mathrutvam-Baby Care")
            {
                $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
            }

            if($vertical=="Physiotherapy-Home")
            {
                $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
            }

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('ServiceStatus',$status)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('leads.created_at',[$Fromdate,$Todate])
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            return view('admin.indexfilter',compact('leads','designation'));
        }
        else
        {
            if($branch!="All" && $vertical!="All")
            {
                // dd('b and  v');

                if($vertical=="NursingServices")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($vertical=="PersonalSupportiveCare")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($vertical=="Mathrutvam-Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($vertical=="Physiotherapy-Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }




                // Code for displaying the counts the Admin Page for different branch and vertical

                $leads=DB::table('leads')
                ->select('services.*','personneldetails.*','addresses.*','leads.*')
                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('services', 'leads.id', '=', 'services.LeadId')
                ->where('ServiceStatus',$status)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->orderBy('leads.id', 'DESC')
                ->paginate(50);

                return view('admin.indexfilter',compact('leads','designation'));
            }
            else
            {
                if($branch!="All" && $Fromdate!="NULL" && $Todate!="NULL")
                {

                    $Fromdate=$Fromdate." 00:00:00";

                    $Todate=$Todate." 23:59:59";

                    $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('ServiceStatus',$status)
                    ->where('Branch',$branch)
                    ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(50);

                    return view('admin.indexfilter',compact('leads','designation'));
                }
                else
                {
                    if($vertical!="All" && $Fromdate!="NULL" && $Todate!="NULL")
                    {

                        $Fromdate=$Fromdate." 00:00:00";

                        $Todate=$Todate." 23:59:59";



                        // check the vertical selected based on that change the array value

                        if($vertical=="NursingServices")
                        {
                            $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                        }

                        if($vertical=="PersonalSupportiveCare")
                        {
                            $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                        }

                        if($vertical=="Mathrutvam-Baby Care")
                        {
                            $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                        }

                        if($vertical=="Physiotherapy-Home")
                        {
                            $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                        }


                        $leads=DB::table('leads')
                        ->select('services.*','personneldetails.*','addresses.*','leads.*')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->where('ServiceStatus',$status)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);

                        return view('admin.indexfilter',compact('leads','designation'));
                    }
                    else
                    {

                        if($branch!="All")
                        {
                            // Code for displaying the counts the Admin Page for different statuses

                            $leads=DB::table('leads')
                            ->select('services.*','personneldetails.*','addresses.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->where('ServiceStatus',$status)
                            ->where('Branch',$branch)
                            ->orderBy('leads.id', 'DESC')
                            ->paginate(50);

                            // dd($leads);
                            return view('admin.indexfilter',compact('leads','designation'));
                        }
                        else
                        {
                            if($vertical!="All")
                            {



                                // check the vertical selected based on that change the array value

                                if($vertical=="NursingServices")
                                {
                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                }

                                if($vertical=="PersonalSupportiveCare")
                                {
                                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                                }

                                if($vertical=="Mathrutvam-Baby Care")
                                {
                                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                                }

                                if($vertical=="Physiotherapy-Home")
                                {
                                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                                }





                                // Code for displaying the counts the if only vertical is selected

                                $leads=DB::table('leads')
                                ->select('services.*','personneldetails.*','addresses.*','leads.*')
                                ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                ->join('services', 'leads.id', '=', 'services.LeadId')
                                ->where('ServiceStatus',$status)
                                ->wherein('ServiceType',$vertical1)
                                ->orderBy('leads.id', 'DESC')
                                ->paginate(50);


                                return view('admin.indexfilter',compact('leads','designation'));
                            }
                            else
                            {
                                if($Fromdate!="NULL" && $Todate!="NULL")
                                {
                                    // dd($branch);
                                    $Fromdate=$Fromdate." 00:00:00";

                                    $Todate=$Todate." 23:59:59";

                                    $leads=DB::table('leads')
                                    ->select('services.*','personneldetails.*','addresses.*','leads.*')
                                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                                    ->join('services', 'leads.id', '=', 'services.LeadId')
                                    ->where('ServiceStatus',$status)
                                    ->whereBetween('leads.created_at',[$Fromdate,$Todate])
                                    ->orderBy('leads.id', 'DESC')
                                    ->paginate(50);

                                    return view('admin.indexfilter',compact('leads','designation'));
                                }
                                else
                                {
                                    dd("nothing selected");
                                }
                            }
                        }




                    }
                }
            }
        }









    }



    public function  searchfilteradminmobile(Request $request)
    {

        $branch = $request->Branch;
        $vertical = $request->Vertial;
        $Fromdate = $request->FromDate;
        $Todate = $request->ToDate;


        // return view('admin.branchonlymobile');


        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        $designation=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        //assigning the possible status for the All

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');


        $Servicesarray1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

        $Servicesarray2 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

        $Servicesarray3 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        $Servicesarray4 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

        $Servicesarray5 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];





        //if branch , vertical and time all are selected
        if($branch!="All" && $vertical!="All" && $Fromdate!=NULL && $Todate!=NULL)
        {


            $Fromdate=$Fromdate." 00:00:00";
            $Todate=$Todate." 23:59:59";



            // check the vertical selected based on that change the array value
            // dd($vertical);

            if($vertical=="NursingServices")
            {
                $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            }

            if($vertical=="PersonalSupportiveCare")
            {
                $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
            }

            if($vertical=="Mathrutvam-Baby Care")
            {
                $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
            }

            if($vertical=="Physiotherapy-Home")
            {
                $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
            }






            // Code for displaying the counts the if only vertical is selected

            $newcountforall = DB::table('services')
            ->where('ServiceStatus',$status0)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();


            $inprogresscountforall = DB::table('services')
            ->where('ServiceStatus',$status1)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();
            // // dd($inprogresscountforall);

            $convertedcountforall = DB::table('services')
            ->where('ServiceStatus',$status2)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();

            $droppedcountforall = DB::table('services')
            ->where('ServiceStatus',$status3)
            ->where('Branch',$branch)
            ->where('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();
            // dd($droppedcountforall);

            $deferredcountforall = DB::table('services')
            ->where('ServiceStatus',$status4)
            ->where('Branch',$branch)
            ->wherein('ServiceType',$vertical1)
            ->whereBetween('created_at',[$Fromdate,$Todate])
            ->count();




            return view('admin.allmobile',compact('designation','designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
        }
        else
        {
            if($branch!="All" && $vertical!="All")
            {


                if($vertical=="NursingServices")
                {
                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                }

                if($vertical=="PersonalSupportiveCare")
                {
                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                }

                if($vertical=="Mathrutvam-Baby Care")
                {
                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                }

                if($vertical=="Physiotherapy-Home")
                {
                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                }




                // Code for displaying the counts the Admin Page for different branch and vertical

                $newcountforall = DB::table('services')
                ->where('ServiceStatus',$status0)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();
                // dd($newcountforall);

                $inprogresscountforall = DB::table('services')
                ->where('ServiceStatus',$status1)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();
                // // dd($inprogresscountforall);

                $convertedcountforall = DB::table('services')
                ->where('ServiceStatus',$status2)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();

                $droppedcountforall = DB::table('services')
                ->where('ServiceStatus',$status3)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();

                $deferredcountforall = DB::table('services')
                ->where('ServiceStatus',$status4)
                ->where('Branch',$branch)
                ->wherein('ServiceType',$vertical1)
                ->count();


                return view('admin.branchtoverticalmobile',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
            }
            else
            {
                if($branch!="All" && $Fromdate!=NULL && $Todate!=NULL)
                {


                    $Fromdate=$Fromdate." 00:00:00";

                    $Todate=$Todate." 23:59:59";



                    //count for individual branch for the given time period

                    $newcountforall = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // dd($newcountforall);

                    $inprogresscountforall = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // // dd($inprogresscountforall);

                    $convertedcountforall = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforall = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforall = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();





                    //code to calculate status count of each service in graph


                    //for nursing service

                    $newcountforns = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();
                    // dd($newcountforns);


                    $inprogresscountforns = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $convertedcountforns = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforns = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforns = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray2)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    //for PSC

                    $newcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforpsc = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray3)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();





                    //for Mathrutvam

                    $newcountformath = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountformath = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountformath = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountformath = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountformath = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray4)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    //for physiotherapy

                    $newcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status0)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();


                    $inprogresscountforphy = DB::table('services')
                    ->where('ServiceStatus',$status1)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $convertedcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status2)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $droppedcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status3)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();

                    $deferredcountforphy = DB::table('services')
                    ->where('ServiceStatus',$status4)
                    ->where('Branch',$branch)
                    ->wherein('ServiceType',$Servicesarray1)
                    ->whereBetween('created_at',[$Fromdate,$Todate])
                    ->count();





                    return view('admin.branchtotimelinemobile',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforns','inprogresscountforns','convertedcountforns','droppedcountforns','deferredcountforns','newcountforpsc','inprogresscountforpsc','convertedcountforpsc','droppedcountforpsc','deferredcountforpsc','newcountformath','inprogresscountformath','convertedcountformath','droppedcountformath','deferredcountformath','newcountforphy','inprogresscountforphy','convertedcountforphy','droppedcountforphy','deferredcountforphy'));
                }
                else
                {
                    if($vertical!="All" && $Fromdate!=NULL && $Todate!=NULL)
                    {

                        $Fromdate=$Fromdate." 00:00:00";

                        $Todate=$Todate." 23:59:59";



                        // check the vertical selected based on that change the array value

                        if($vertical=="NursingServices")
                        {
                            $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                        }

                        if($vertical=="PersonalSupportiveCare")
                        {
                            $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                        }

                        if($vertical=="Mathrutvam-Baby Care")
                        {
                            $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                        }

                        if($vertical=="Physiotherapy-Home")
                        {
                            $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                        }





                        // Code for displaying the counts the if only vertical is selected

                        $newcountforall = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($newcountforall);

                        $inprogresscountforall = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // // dd($inprogresscountforall);

                        $convertedcountforall = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforall = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($droppedcountforall);

                        $deferredcountforall = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();





                        //code to calculate status count of each service in graph


                        //for Bengaluru

                        $newcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();
                        // dd($newcountforbng);


                        $inprogresscountforbng = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $convertedcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforbng = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Bengaluru")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        //for pune

                        $newcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforpune = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforpune = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Pune")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();





                        //for Chennai

                        $newcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforchen = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforchen = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Chennai")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        //for Hubballi-Dharwad

                        $newcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforhub = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforhub = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Hubballi-Dharwad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();



                        //for Hyderabad

                        $newcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status0)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();


                        $inprogresscountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status1)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $convertedcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status2)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $droppedcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status3)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        $deferredcountforhyd = DB::table('services')
                        ->where('ServiceStatus',$status4)
                        ->where('Branch',"Hyderabad")
                        ->wherein('ServiceType',$vertical1)
                        ->whereBetween('created_at',[$Fromdate,$Todate])
                        ->count();

                        return view('admin.verticaltotimelinemobile',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforbng','inprogresscountforbng','convertedcountforbng','droppedcountforbng','deferredcountforbng','newcountforchen','inprogresscountforchen','convertedcountforchen','droppedcountforchen','deferredcountforchen','newcountforpune','inprogresscountforpune','convertedcountforpune','droppedcountforpune','deferredcountforpune','newcountforhyd','inprogresscountforhyd','convertedcountforhyd','droppedcountforhyd','deferredcountforhyd','newcountforhub','inprogresscountforhub','convertedcountforhub','droppedcountforhub','deferredcountforhub'));
                    }
                    else
                    {

                        if($branch!="All")
                        {


                            // Code for displaying the counts the Admin Page for different statuses

                            $newcountforall = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->count();
                            // dd($newcountforall);

                            $inprogresscountforall = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->count();
                            // // dd($inprogresscountforall);

                            $convertedcountforall = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->count();

                            $droppedcountforall = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->count();

                            $deferredcountforall = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->count();




                            //code to calculate status count of each service in graph


                            //for nursing service

                            $newcountforns = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();
                            // dd($newcountforns);


                            $inprogresscountforns = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();


                            $convertedcountforns = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();

                            $droppedcountforns = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();

                            $deferredcountforns = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray2)
                            ->count();


                            //for PSC

                            $newcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();


                            $inprogresscountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $convertedcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $droppedcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();

                            $deferredcountforpsc = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray3)
                            ->count();





                            //for Mathrutvam

                            $newcountformath = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();


                            $inprogresscountformath = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $convertedcountformath = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $droppedcountformath = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();

                            $deferredcountformath = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray4)
                            ->count();


                            //for physiotherapy

                            $newcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status0)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();


                            $inprogresscountforphy = DB::table('services')
                            ->where('ServiceStatus',$status1)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $convertedcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status2)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $droppedcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status3)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();

                            $deferredcountforphy = DB::table('services')
                            ->where('ServiceStatus',$status4)
                            ->where('Branch',$branch)
                            ->wherein('ServiceType',$Servicesarray1)
                            ->count();




                            return view('admin.branchonlymobile',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforns','inprogresscountforns','convertedcountforns','droppedcountforns','deferredcountforns','newcountforpsc','inprogresscountforpsc','convertedcountforpsc','droppedcountforpsc','deferredcountforpsc','newcountformath','inprogresscountformath','convertedcountformath','droppedcountformath','deferredcountformath','newcountforphy','inprogresscountforphy','convertedcountforphy','droppedcountforphy','deferredcountforphy'));

                        }
                        else
                        {
                            if($vertical!="All")
                            {

                                // dd($vertical);


                                // check the vertical selected based on that change the array value

                                if($vertical=="NursingServices")
                                {
                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                }

                                if($vertical=="PersonalSupportiveCare")
                                {
                                    $vertical1 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];
                                }

                                if($vertical=="Mathrutvam-Baby Care")
                                {
                                    $vertical1 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];
                                }

                                if($vertical=="Physiotherapy-Home")
                                {
                                    $vertical1 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
                                }





                                // Code for displaying the counts the if only vertical is selected

                                $newcountforall = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // dd($newcountforall);

                                $inprogresscountforall = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // // dd($inprogresscountforall);

                                $convertedcountforall = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforall = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforall = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                //code to calculate status count of each service in graph


                                //for Bengaluru

                                $newcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();
                                // dd($newcountforbng);


                                $inprogresscountforbng = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $convertedcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforbng = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Bengaluru")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                //for pune

                                $newcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforpune = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforpune = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Pune")
                                ->wherein('ServiceType',$vertical1)
                                ->count();





                                //for Chennai

                                $newcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforchen = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforchen = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Chennai")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                //for Hubballi-Dharwad

                                $newcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforhub = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforhub = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Hubballi-Dharwad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                //for Hyderabad

                                $newcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status0)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();


                                $inprogresscountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status1)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $convertedcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status2)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $droppedcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status3)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();

                                $deferredcountforhyd = DB::table('services')
                                ->where('ServiceStatus',$status4)
                                ->where('Branch',"Hyderabad")
                                ->wherein('ServiceType',$vertical1)
                                ->count();



                                return view('admin.verticalfilteralonemobile',compact('designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall','newcountforbng','inprogresscountforbng','convertedcountforbng','droppedcountforbng','deferredcountforbng','newcountforchen','inprogresscountforchen','convertedcountforchen','droppedcountforchen','deferredcountforchen','newcountforpune','inprogresscountforpune','convertedcountforpune','droppedcountforpune','deferredcountforpune','newcountforhyd','inprogresscountforhyd','convertedcountforhyd','droppedcountforhyd','deferredcountforhyd','newcountforhub','inprogresscountforhub','convertedcountforhub','droppedcountforhub','deferredcountforhub'));
                            }
                            else
                            {
                                if($Fromdate!=Null && $Todate!=NULL)
                                {
                                    $Fromdate=$Fromdate." 00:00:00";

                                    $Todate=$Todate." 23:59:59";

                                    // dd($user_city);

                                    $vertical1 = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    $vertical2 = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

                                    $vertical3 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

                                    $vertical4 = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

                                    $vertical5 = ["Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia","Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy", "Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

                                    $newcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status0)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $inprogresscountforall = DB::table('services')
                                    ->where('ServiceStatus',$status1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $convertedcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $droppedcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $deferredcountforall = DB::table('services')
                                    ->where('ServiceStatus',$status4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    //Counts for all services for city Bangalore
                                    $bns = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $bpsc = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $bm = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $bphy = DB::table('services')
                                    ->where('Branch',"Bengaluru")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Pune
                                    $pns = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $ppsc = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $pm = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $pphy = DB::table('services')
                                    ->where('Branch',"Pune")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Chennai
                                    $cns = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $cpsc = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $cm = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $cphy = DB::table('services')
                                    ->where('Branch',"Chennai")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Hyderabad
                                    $hns = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $hpsc = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $hm = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $hphy = DB::table('services')
                                    ->where('Branch',"Hyderabad")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    //Counts for all services for city Hubballi-Dharwad
                                    $hdns = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical1)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();


                                    $hdpsc = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical2)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();
                                    // // dd($inprogresscountforall);

                                    $hdm = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical3)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();

                                    $hdphy = DB::table('services')
                                    ->where('Branch',"Hubballi-Dharwad")
                                    ->wherein('ServiceType',$vertical4)
                                    ->whereBetween('created_at',[$Fromdate,$Todate])
                                    ->count();



                                    return view('admin.timelineonlymobile',compact('bns','bpsc','bm','bphy','pns','ppsc','pm','pphy','cns','cpsc','cm','cphy','hns','hpsc','hm','hphy','hdns','hdpsc','hdm','hdphy','designation','branch','vertical','Fromdate','Todate','newcountforall','inprogresscountforall','convertedcountforall','droppedcountforall','deferredcountforall'));
                                }
                                else
                                {
                                    dd("nothing selected");
                                }
                            }
                        }




                    }
                }
            }
        }


    }





    // Product onclick function


    public function productassigned()
    {
        //if the status is present in the URL get it from there, else get it from the session
        $type=$_GET['type'];
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        // $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        if($type=="Selling")
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        else
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;


                if($type=="Selling")
                {

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('products.Type','Sell')
                    ->orwhere('products.Type','')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('products.Type','Rent')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                if($type=="Selling")
                {
                    //dd($type);
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatus',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('OrderStatusrent',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Reference', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        session()->put('name',$logged_in_user);

        return view('admin.productindex',compact('leads'));
    }




    // pharmacy assigned function



    public function pharmacyassigned()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        //  dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;

        $leads = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status)
        ->orderBy('pharmacies.id', 'DESC')
        ->paginate(50);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department;

                $leads  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                ->orderBy('pharmacies.id', 'DESC')

                ->get();


                $leads = json_decode($leads,true);

                // dd($leads);


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');


                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                $leads = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                ->where('POrderStatus',$status)
                ->orderBy('pharmacies.id', 'DESC')
                ->get();
                $leads = json_decode($leads,true);


                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');

                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }


        session()->put('name',$logged_in_user);

        return view('admin.pharmacyindex',compact('leads'));
    }


public function assessmentdonelist()
{

    $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];
     $leads1=DB::table('leads')
         ->select('services.*','personneldetails.*','addresses.*','leads.*')
        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->join('physiotheraphies','leads.id','=','physiotheraphies.Leadid')
        ->where('physiotheraphies.PhysiotheraphyType','<>',NULL)
        ->get();
         $leads1 = json_decode($leads1,true);


        $leads2=DB::table('leads')
         ->select('services.*','personneldetails.*','addresses.*','leads.*')
        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->join('mothers','leads.id','=','mothers.Leadid')
        ->join('mathruthvams','mothers.id','=','mathruthvams.Motherid')
        ->join('vitalsigns','leads.id','=','vitalsigns.leadid')
        ->where('vitalsigns.BP','<>',NULL)
        ->where('vitalsigns.RR','<>',NULL)
        ->where('vitalsigns.Temperature','<>',NULL)
        ->where('vitalsigns.Pulse','<>',NULL)
        ->get();

 $leads2 = json_decode($leads2,true);


        $leads3=DB::table('leads')
         ->select('services.*','personneldetails.*','addresses.*','leads.*')
        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('services', 'leads.id', '=', 'services.LeadId')
        ->join('vitalsigns','leads.id','=','vitalsigns.leadid')
        ->join('genitos','leads.id','=','genitos.leadid')
        ->join('mobilities','leads.id','=','mobilities.leadid')
        ->where('mobilities.Independent','<>',NULL)
        ->where('genitos.UrinaryContinent','<>',NULL)
         ->where('vitalsigns.BP','<>',NULL)
        ->where('vitalsigns.RR','<>',NULL)
        ->where('vitalsigns.Temperature','<>',NULL)
        ->where('vitalsigns.Pulse','<>',NULL)
        
        ->get();

 $leads3 = json_decode($leads3,true);
       

        $leads_all = array_merge($leads1,$leads2,$leads3);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));


        $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));
        $count = count($leads1);

return view('admin.assessmentindex',compact('count','leads1'));
}
    
     

    
}
