<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Auth;
use DB;
use Session;

class CoordinatorController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('coordinator');
    }

    /* Dashboard for Coordinator code by jatin --starts here
    */

    //this function is for displaying the dashboard for the "Coordinator"
    public function index()
    {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::user()->name;
        $designation="coordinator";

        $under = DB::table('employees')->where('FirstName',$logged_in_user)->value('under');
        $user_city = DB::table('employees')->where('id',$under)->value('City');
        $serviceType = DB::table('employees')->where('id',$under)->value('Department');

        // dd($serviceType);
        //assigning the possible statuses for the Coordinator
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";


        // find the count of all the leads which have been assigned to the logged in coordinator
        // a linkage exists between the vertical coordinations and employees table
        // - if a lead has been assigned to a coordinator then the coordinators employee id will be present in "vertical coordinations" table
        // - if not , then "Null" goes there
        // here, we are checking for the status "In Progress" which comes as "New" for the //coordinator
        $assignedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status1)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $convertedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status2)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $droppedcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status3)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        $deferredcount = DB::table('leads')
        ->select('employees.*','services.*','leads.*')
        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
        ->join('employees','verticalcoordinations.empid','=','employees.id')
        ->join('services', 'leads.id', '=', 'services.Leadid')
        ->where('ServiceStatus',$status4)
        ->where('FirstName',$logged_in_user)
        ->orderBy('leads.id', 'DESC')
        ->count();

        return view('admin.coordinator',compact('assignedcount','convertedcount','droppedcount','deferredcount','designation','user_city','serviceType'));
    }

    // This function is for the scenario when the "Coordinator" on their dashboard clicks on the status count image
    //and sees the details depending on that count for "each status"

    public function assigned(Request $request)
    {
        // retrieving the status that was passed as parameter in "admin.coordinator blade" so that it can retrieve the records
        // assigned for that status

        // dd($_GET['status']);

        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
            // dd($status);

            session()->put('status',$status);
            //retrieving the name of the logged in user
            $logged_in_user = Auth::guard('admin')->user()->name;

            //general query for Viewing Leads on the basis of count and status
            $leads = DB::table('leads')
            ->select('employees.*','services.*','leads.*')
            ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
            ->join('employees','verticalcoordinations.empid','=','employees.id')
            ->join('services', 'leads.id', '=', 'services.Leadid')
            ->where('ServiceStatus',$status)
            ->where('FirstName',$logged_in_user)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            /*
            Logic for downloading CSV goes here -- starts here
            */
            //if the link generated from co/index.blade.php sets "download==true", run this
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.AssignedTo',$logged_in_user)
                    ->orwhere('verticalcoordinations.empid',$empname1)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    // dd($leads);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                    ->join('employees','verticalcoordinations.empid','=','employees.id')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.Leadid')
                    ->where('ServiceStatus',$status)
                    ->where('FirstName',$logged_in_user)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');
                    // $list = array (
                    //     $leads
                    // );
                    //
                    //         $lists = array (
                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                    // array('123', '456', '789'),
                    // array('aaa', 'bbb')
                    // );
                    // dd($list);

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);
                }
                /*
                Logic for downloading CSV goes here -- ends here
                */
            }

            // dd($leads);

            // Checking the status returned from the URL and redirecting to the appropriate view

            session()->put('status',$status);
            return view('co.index',compact('leads'));
        }



public function searchfilter(Request $request)
{

          $branch = $request->Branch;
    $vertical = $request->Vertial;
    $Fromdate = $request->FromDate;
    $Todate = $request->ToDate;


    return view('co.searchfilter',compact('branch','vertical','Fromdate','Todate'));
}
}
    /* Dashboard for Coordinator code by jatin --ends here
    */
