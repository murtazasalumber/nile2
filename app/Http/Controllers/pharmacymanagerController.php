<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Illuminate\Support\Facades\Response;

class pharmacymanagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('pharmacymanager');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //retrieving the name of the user who is logged in
         $logged_in_user = Auth::guard('admin')->user()->name;
          $designation="pharmacymanager";

         // dd($logged_in_user);

         //assigning the possible statuses for the Verticals

         $status0 = "New";
         $status1 = "Processing";
         $status2  = "Awaiting Pickup";
         $status3  = "Out for Delivery";
         $status4  = "Ready to ship";
         $status5  = "Order Return";
         $status6  = "Cancelled";
         $status7  = "Delivered";
         $status8  = "Received Order Return";

         $newcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status0)
         ->orderBy('id', 'DESC')
         ->count();

         $processingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status1)
         ->orderBy('id', 'DESC')
         ->count();

         $awaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status2)
         ->orderBy('id', 'DESC')
         ->count();

         $outfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status3)
         ->orderBy('id', 'DESC')
         ->count();

         $readytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status4)
         ->orderBy('id', 'DESC')
         ->count();

         $orderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status5)
         ->orderBy('id', 'DESC')
         ->count();

         $recievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status8)
         ->orderBy('id', 'DESC')
         ->count();


         $canceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status6)
         ->orderBy('id', 'DESC')
         ->count();

         $deliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status7)
         ->orderBy('id', 'DESC')
         ->count();

         return view('admin.pharmacymanager',compact('newcount','processingcount','awaitingpickupcount','outfordeliverycount','readytoshipcount','orderreturncount','canceledcount','deliveredcount','designation','recievedorderreturncount'));
     }

     public function assigned()
     {
         //if the status is present in the URL get it from there, else get it from the session
         if(isset($_GET['status']))
         {
             $status = $_GET['status'];
         }
         else
         {
             $status = session()->get('status');
         }
            //  dd($status);

             session()->put('status',$status);
             //retrieving the name of the logged in user
             $logged_in_user = Auth::guard('admin')->user()->name;

             $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
            ->where('POrderStatus',$status)
            ->orderBy('pharmacies.id', 'DESC')
            ->paginate(50);

            /*
            Logic for downloading CSV goes here -- starts here
            */
            //if the link generated from co/index.blade.php sets "download==true", run this
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation2;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department2;

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->orderBy('pharmacies.id', 'DESC')

                    ->get();


                    $leads = json_decode($leads,true);

                    // dd($leads);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');


                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->where('POrderStatus',$status)
                    ->orderBy('pharmacies.id', 'DESC')
                    ->get();
                    $leads = json_decode($leads,true);

                   
                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');

                    // $list = array (
                    //     $leads
                    // );
                    //
                    //         $lists = array (
                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                    // array('123', '456', '789'),
                    // array('aaa', 'bbb')
                    // );
                    // dd($list);

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);
                }
                /*
                Logic for downloading CSV goes here -- ends here
                */
            }


         session()->put('name',$logged_in_user);

         return view('product.pharmacyindex',compact('leads'));
 }
}
