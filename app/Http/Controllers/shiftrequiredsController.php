<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\shiftrequired;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class shiftrequiredsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $shiftrequireds= shiftrequired::paginate(50);
                     return view('shiftrequired.index',compact('shiftrequireds'));
                }
                else
                {
                    return redirect('/admin');
                }
      }
       else
        {
            return redirect('/admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                   return view('shiftrequired.create');

               }
                else
                {
                    return redirect('/admin');
                }
      }
       else
        {
            return redirect('/admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shiftrequired = new shiftrequired;
        $this->validate($request,[
                'shiftrequired'=>'required',

            ]);
       $shiftrequired->shiftrequired=$request->shiftrequired;


       $shiftrequired->save();

       $shiftid = DB::table('shiftrequireds')->max('id');
       //    dd($langid);

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $request->shiftrequired." Shift Added ";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;
       $activity->save();



       return redirect('/shiftrequireds');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       if(Auth::guard('admin')->check())
        {

          $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                    $shiftrequired= shiftrequired::find($id);
                    return view('shiftrequired.edit',compact('shiftrequired'));
                }
                else
                {
                    return redirect('/admin');
                }
      }
       else
        {
            return redirect('/admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $c=DB::table('shiftrequireds')->where('id',$id)->value('shiftrequired');
        if($request->shiftrequired!=$c)
        {
       $shiftrequired = shiftrequired::find($id);
        $shiftrequired->shiftrequired=$request->shiftrequired;

        $shift = $shiftrequired->getDirty();
        // dd($lang);

        foreach($shift as $key=>$value)
        {
            $shiftc[] = $key;

        }

        foreach($shift as $key=>$value)
        {
            $shiftv[] = $value;
        }

        //this is for converting array values into a string with new line

        //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
        if(empty($shiftc))
        {
            $shiftd = " ";
            $shiftf = " ";
            $shiftd = trim($shiftd);
            $shiftf = trim($shiftf);
        }
        else
        {
            $shiftd = implode("\r\n", $shiftc);
            $shiftf = implode("\r\n", $shiftv);
        }


       $shiftrequired->save();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       if($shiftd==" " || $shiftd==' ' || $shiftd=="" || $shiftd=='')
       {
           $shiftd = trim($shiftd);
           $shiftf = trim($shiftf);
       }
       else
       {
           $shiftd = $shiftd."\r\n";
           $shiftf = $shiftf."\r\n";
       }

       //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
       $fields_updated = $shiftd;
       $values_updated = $shiftf;

       if((trim($fields_updated)!==""))
       {
           // dd($fields_updated);
           $activity = new Activity;
           $activity->emp_id = $emp_id;
           $activity->activity = "Fields Updated";
           $activity->activity_time = new \DateTime();
           $activity->log_id = $log_id;
           $activity->field_updated = $fields_updated;
           $activity->value_updated = $values_updated;

           $activity->save();
       }

       session()->flash('message','Updated Successfully');

   }
       return redirect('/shiftrequireds');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $shiftrequired=shiftrequired::find($id);

       $shift_del = DB::table('shiftrequireds')->where('id',$id)->value('shiftrequired');

       $shiftrequired->delete();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $shift_del." Shift Deleted";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;

       $activity->save();

       session()->flash('message','Delete Successfully');
       return redirect('/shiftrequireds');
    }
}
