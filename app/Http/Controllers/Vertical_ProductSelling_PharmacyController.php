<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Response;


class Vertical_ProductSelling_PharmacyController extends Controller
{
     	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('vertical_productselling_pharmacy');
    }

    public function index()
    {
        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $name = Auth::guard('admin')->user()->name;

        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');


        // $designation="productmanager";

        $designation="vertical";


        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";



        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status00 = "New";
        $status11 = "Processing";
        $status22  = "Awaiting Pickup";
        $status33  = "Out for Delivery";
        $status44  = "Ready to ship";
        $status55  = "Order Return";
        $status66  = "Cancelled";
        $status77  = "Delivered";
        $status88  = "Received Order Return";




        //grabbing the city for which we want the counts
        //this city will be specific to the logged in user
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // dd($serviceType);

        // checking if the service type is "Physiotherapy" and calculating counts according to that

        // if($user_city=="Hyderabad" || $user_city=="Chennai" || $user_city=="Pune" || $user_city=="Hubballi-Dharwad")
        // {
        //     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //     $newcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status0)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $inprogresscount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status1)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();


        //     $convertedcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status2)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $droppedcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status3)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $deferredcount =DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status4)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();
        // }

        if($serviceType=="Physiotherapy - Home")
        {

            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            // Depending on the branch , the ServiceStatus from above, the subtypes of Physiotherapy , we are fetching the count
            // this count consists of all counts (for vertical, all coordinators under him/her)
            $newcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "Nursing Services" and calculating counts according to that
        else if($serviceType=="Nursing Services")
        {

            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $newcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "PSC" and calculating counts according to that
        else if($serviceType=="Personal Supportive Care")
        {

            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $newcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        // checking if the service type is "Mathrutvam" and calculating counts according to that
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];


            $newcount=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $inprogresscount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount =DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        /*Code for Graphs and Pie Chart of Vertical Dashboard starts here */

        //Find the id of the vertical that is currently logged in
        $logged_in_user_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
        // dd($logged_in_user_id);

        $coords_under_vert = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->get();
        $coords_under_vert = json_decode($coords_under_vert,true);

        $statuscounts[-1][-1] = null;

        $coords_under_vert_count = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->count();

        // dd($coords_under_vert_count);

        // dd($coords_under_vert[0]['FirstName']);
        $User=null;

        for($i=0;$i<$coords_under_vert_count;$i++)
        {
            $User[] =  $coords_under_vert[$i]['FirstName'];

        }
        // dd($User[1]);

        if($User!=NULL)
        {
            for($j=0;$j<$coords_under_vert_count;$j++)
            {
                /* Retrieving the counts of all the users -- started */
                //assigning the possible statuses for the Coordinator
                $status1 = "In Progress";
                $status2  = "Converted";
                $status3  = "Dropped";
                $status4  = "Deferred";

                $logged_in_user = $User[$j];

                $assignedcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $convertedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status2)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status3)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $deferredcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status4)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $statuscounts[] = array
                (
                    "0" => $assignedcount2, "1" =>$convertedcount2,"2"=>$droppedcount2,"3"=>$deferredcount2
                );

                // $statuscounts[$j][0] = $assignedcount2;
                // $statuscounts[$j][1] = $convertedcount2;
                // $statuscounts[$j][2] = $droppedcount2;
                // $statuscounts[$j][3] = $deferredcount2;

                $coord_id1 = DB::table('employees')->where('FirstName',$User[$j])->value('id');
                $totalcount = DB::table('verticalcoordinations')->where('empid',$coord_id1)->count();


                // $percentage[$j][0] = ($statuscounts[$j][1]/$totalcount)*100;

                /* Retrieving the counts of all the users -- end */
            }

            // dd($statuscounts);


            // $colors1[-1] = null;
            //
            // // $colors1[0] = #4DFF4D;
            // // $colors1[1] = #ff0000;
            // // $colors1[2] = #0066ff;
            // // $colors1[3] = #b87333;
        }
        // dd($colors1[0]);
        $colors1 = array('#b87333','silver','gold','#e5e4e2');

        /*Code for Graphs and Pie Chart of Vertical Dashboard ends here */

        // dd($deferredcount2);

// dd($user_city);


        // Product Selling count for all status
        $productnewcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status00)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productprocessingcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status11)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productawaitingpickupcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status22)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productoutfordeliverycount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status33)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productreadytoshipcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status44)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status55)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productrecievedorderreturncount =DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status88)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productcanceledcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status66)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();

            $productdeliveredcount = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status77)
            ->where('City',$user_city)
            ->orderBy('id', 'DESC')
            ->count();



         // Pharmacy count for all status

        $pharmacynewcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status00)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyprocessingcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status11)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyawaitingpickupcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status22)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyoutfordeliverycount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status33)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyreadytoshipcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status44)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status55)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacyrecievedorderreturncount =DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status88)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();


         $pharmacycanceledcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status66)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();

         $pharmacydeliveredcount = DB::table('leads')
        ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
        ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
        ->where('POrderStatus',$status77)
        ->where('City',$user_city)
         ->orderBy('id', 'DESC')
         ->count();




        session()->put('name',$name);
        return view('admin.Vertical_ProductSelling_Pharmacy',compact('newcount','convertedcount','inprogresscount','assignedcount2','convertedcount','droppedcount','deferredcount','assignedcount2','convertedcount2','droppedcount2','deferredcount2','User','coords_under_vert_count','colors1','emp','statuscounts','designation','user_city','serviceType','productnewcount','productprocessingcount','productawaitingpickupcount','productoutfordeliverycount','productreadytoshipcount','productorderreturncount','productrecievedorderreturncount','productcanceledcount','productdeliveredcount','pharmacynewcount','pharmacyprocessingcount','pharmacyawaitingpickupcount','pharmacyoutfordeliverycount','pharmacyreadytoshipcount','pharmacyorderreturncount','pharmacyrecievedorderreturncount','pharmacycanceledcount','pharmacydeliveredcount'));


    }

    public function assigned()
    {

        if (session()->has('name'))
        {

            $logged_in_user = session()->get('name');
        }
        else{
            $logged_in_user = $_GET['name'];

        }

        $logged_in_user = Auth::guard('admin')->user()->name;

        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        // dd($status);

        // session()->put('status',$status);

        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // if($user_city=="Hyderabad" || $user_city=="Chennai" || $user_city=="Pune" || $user_city=="Hubballi-Dharwad")
        // {
        //     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //     $leads=DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('services.Branch',$user_city)
        //     ->where('services.ServiceStatus',$status)
        //     ->wherein('services.ServiceType',$Servicesarray)
        //     ->orderBy('leads.id', 'DESC')
        //     ->paginate(50);

        //     if(isset($_GET['download']))
        //     {
        //         $download = $_GET['download'];

        //         if($status=="All")
        //         {
        //             $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
        //             $empname=json_decode($empname);
        //             $empname1= $empname[0]->id;
        //             $empname2= $empname[0]->Designation;
        //             $empname3= $empname[0]->city;
        //             $empname4= $empname[0]->Department;

        //             // dd($Servicesarray);
        //             $leads=DB::table('leads')
        //             ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->where('services.Branch',$user_city)
        //             ->wherein('services.ServiceType',$Servicesarray)
        //             ->orderBy('leads.id', 'DESC')
        //             ->get();

        //             // dd($leads);


        //             $leads = json_decode($leads,true);


        //             $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
        //             ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
        //             ,'Emergency District','Emergency State','Emergency PinCode');

        //             $filename = "download.csv";

        //             $fp = fopen('download.csv', 'w');

        //             fputcsv($fp, $array );
        //             foreach ($leads as $fields) {
        //                 fputcsv($fp, $fields);
        //             }

        //             fclose($fp);

        //             $headers = array(
        //                 'Content-Type' => 'text/csv',
        //             );
        //             return Response::download($filename, 'download.csv', $headers);

        //         }

        //         else {


        //             $leads = DB::table('leads')
        //             ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
        //             ->join('services','leads.id','=','services.Leadid')
        //             ->join('personneldetails','leads.id','=','personneldetails.Leadid')
        //             ->join('addresses','leads.id','=','addresses.leadid')
        //             ->where('services.Branch',$user_city)
        //             ->where('services.ServiceStatus',$status)
        //             ->wherein('services.ServiceType',$Servicesarray)
        //             ->orderBy('leads.id', 'DESC')
        //             ->get();

        //             $leads = json_decode($leads,true);

        //             $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
        //             ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
        //             ,'Emergency District','Emergency State','Emergency PinCode');

        //             $filename = "download.csv";

        //             $fp = fopen('download.csv', 'w');

        //             fputcsv($fp, $array );
        //             foreach ($leads as $fields) {
        //                 fputcsv($fp, $fields);
        //             }

        //             fclose($fp);

        //             $headers = array(
        //                 'Content-Type' => 'text/csv',
        //             );
        //             return Response::download($filename, 'download.csv', $headers);
        //         }
        //         /*
        //         Logic for downloading CSV goes here -- ends here
        //         */
        //     }

        //     session()->put('name',$logged_in_user);

        //     return view('verticalheads.index',compact('leads'));
        // }

        if($serviceType=="Nursing Services")
        {
            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                // dd($Servicesarray);

                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {

                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    // dd($leads);
                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }
        }
        else  if($serviceType=="Personal Supportive Care")
        {
            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];


           $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            if(isset($_GET['download']))
            {
                $download = $_GET['download'];


                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }

        }
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

           $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);


            if(isset($_GET['download']))
            {
                $download = $_GET['download'];


                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }

            // dd($leads);

        }

        else if($serviceType=="Physiotherapy - Home")
        {
            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            $leads=DB::table('leads')
                    ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('services.Branch',$user_city)
            ->where('services.ServiceStatus',$status)
            ->wherein('services.ServiceType',$Servicesarray)
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            if(isset($_GET['download']))
            {
                $download = $_GET['download'];


                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation','city','Department')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department;

                    // dd($Servicesarray);
                    $leads=DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('services.Branch',$user_city)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    // dd($leads);


                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','services.Branch','leads.Source','services.ServiceType','services.requested_service','services.ServiceStatus','services.Remarks','leads.AssesmentReq','services.GeneralCondition','services.RequestDateTime','services.AssignedTo','services.QuotedPrice','services.ExpectedPrice','services.PreferedGender','services.PreferedLanguage','personneldetails.PtfName','personneldetails.PtmName','personneldetails.PtlName','personneldetails.age','personneldetails.Gender','personneldetails.Relationship','personneldetails.Occupation','personneldetails.AadharNum','personneldetails.AlternateUHIDType','personneldetails.AlternateUHIDNumber','personneldetails.PTAwareofDisease','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode')
                    ->join('services','leads.id','=','services.Leadid')
                    ->join('personneldetails','leads.id','=','personneldetails.Leadid')
                    ->join('addresses','leads.id','=','addresses.leadid')
                    ->where('services.Branch',$user_city)
                    ->where('services.ServiceStatus',$status)
                    ->wherein('services.ServiceType',$Servicesarray)
                    ->orderBy('leads.id', 'DESC')
                    ->get();

                    $leads = json_decode($leads,true);

                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'City', 'Source', 'Service Type','Requested Service', 'Lead Status','Comments','Assessment Required','General Condition','Requested DateTime', 'Assigned To','Quoted Price', 'Expected Price','Preferred Gender', 'Preferred Language','Patient First Name'
                    ,'Patient Middle Name','Patient Last Name','Patient Age','Patient Gender','Relationship','Occupation','Aadhar Number','Alternate UHID Type','Alternate UHID Number', 'Patient Aware of Disease', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode');

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                    /*
                    Logic for downloading CSV goes here -- ends here
                    */
                }
            }
        }

        session()->put('name',$logged_in_user);

        return view('Vertical_ProductSelling_Pharmacy.index',compact('leads'));
    }


    public function searchfilter(Request $request)
    {

                  $branch = $request->Branch;
            $vertical = $request->Vertial;
            $Fromdate = $request->FromDate;
            $Todate = $request->ToDate;

                return view('verticalheads.serachfilter',compact('branch','vertical','Fromdate','Todate'));
    }



        public function assigned1()
    {
        //if the status is present in the URL get it from there, else get it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }
        if(isset($_GET['type']))
        {
            $type = $_GET['type'];
        }
        else
        {
            $type = "All";
        }
        // dd($status);

        session()->put('status',$status);
        //retrieving the name of the logged in user
        $logged_in_user = Auth::guard('admin')->user()->name;
        $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        if($type=="Sell")
        {

            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatus',$status)
            ->where('City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        else
        {
            $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('OrderStatusrent',$status)
            ->where('City',$city)
            ->orderBy('products.id', 'DESC')
            ->paginate(50);
        }
        /*
        Logic for downloading CSV goes here -- starts here
        */
        //if the link generated from co/index.blade.php sets "download==true", run this
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
            if($status=="All")
            {
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                $empname=json_decode($empname);
                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation2;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department2;


                if($type=="Sell")
                {

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('products.Type','Sell')
                    ->orwhere('products.Type','')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('products.Type','Rent')
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }


                $leads = json_decode($leads,true);

                // dd($leads);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);

            }

            //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
            else
            {
                if($type=="Sell")
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('OrderStatus',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                    ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('products', 'leads.id', '=', 'products.leadid')
                    ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
                    ->where('City',$city)
                    ->where('OrderStatusrent',$status)
                    ->orderBy('products.id', 'DESC')
                    ->get();
                }

                $leads = json_decode($leads,true);

                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id'
                ,'Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','Product Name','Demo Required', 'Availability Status','Availability Address','SellingPrice','Type','Order Status','Quantity','Mode of Payment','Mode of Paymentrent','Order Status rent','Advance Amt','Start Date','End Date','Overdue Amount'
                ,'Rental Price','Created by','Comments');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }

        session()->put('name',$logged_in_user);

        return view('Vertical_ProductSelling_Pharmacy.productindex',compact('leads'));
    }

        public function assigned2()
     {
         //if the status is present in the URL get it from there, else get it from the session
         if(isset($_GET['status']))
         {
             $status = $_GET['status'];
         }
         else
         {
             $status = session()->get('status');
         }
            //  dd($status);

             session()->put('status',$status);
             //retrieving the name of the logged in user
             $logged_in_user = Auth::guard('admin')->user()->name;
              $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

             $leads = DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
            ->where('POrderStatus',$status)
            ->where('City',$city)
            ->orderBy('pharmacies.id', 'DESC')
            ->paginate(50);

            /*
            Logic for downloading CSV goes here -- starts here
            */
            //if the link generated from co/index.blade.php sets "download==true", run this
            if(isset($_GET['download']))
            {
                $download = $_GET['download'];

                //if the status sent from the admin/coordinator.blade is All(this happens when we click on "View Leads" for coordinator), run this
                if($status=="All")
                {
                    $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation2','city','Department2')->get();
                    $empname=json_decode($empname);
                    $empname1= $empname[0]->id;
                    $empname2= $empname[0]->Designation2;
                    $empname3= $empname[0]->city;
                    $empname4= $empname[0]->Department2;

                    $leads  = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->where('City',$city)
                    ->orderBy('pharmacies.id', 'DESC')

                    ->get();


                    $leads = json_decode($leads,true);

                    // dd($leads);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');


                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);

                }

                //if the status sent is "New/In Progress etc (in the case when the "count" buttons are clicked and the download option is clicked for that page)",  then run this
                else
                {
                    $leads = DB::table('leads')
                    ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode'
                    ,'prodleads.orderid','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
                    ->join('prodleads', 'pharmacies.id', '=', 'prodleads.PharmacyId')
                    ->where('POrderStatus',$status)
                    ->where('City',$city)
                    ->orderBy('pharmacies.id', 'DESC')
                    ->get();
                    $leads = json_decode($leads,true);


                    $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Last Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id','Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                    ,'Emergency District','Emergency State','Emergency PinCode'
                ,'Order Id', 'MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amoun Paid','Discount','FinalAmount','PostDiscountPrice');

                    // $list = array (
                    //     $leads
                    // );
                    //
                    //         $lists = array (
                    // array('aaa', 'bbb', 'ccc', 'dddd'),
                    // array('123', '456', '789'),
                    // array('aaa', 'bbb')
                    // );
                    // dd($list);

                    $filename = "download.csv";

                    $fp = fopen('download.csv', 'w');

                    fputcsv($fp, $array );
                    foreach ($leads as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return Response::download($filename, 'download.csv', $headers);
                }
                /*
                Logic for downloading CSV goes here -- ends here
                */
            }


         session()->put('name',$logged_in_user);

         return view('Vertical_ProductSelling_Pharmacy.pharmacyindex',compact('leads'));
 }



 public function assigned3()
    {
        //If the status is present in the URL extract from there or retrieve it from the session
        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = session()->get('status');
        }

        $logged_in_user = Auth::guard('admin')->user()->name;


        $d=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');
        $city=DB::table('employees')->where('FirstName',$logged_in_user)->value('city');



        $leads1= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('City',$city)
        ->where('products.Type',"Sell")
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads1 = json_decode($leads1,true);


        $leads3= DB::table('leads')
        ->select('products.*','pharmacies.*','addresses.*','leads.*','orders.*','prodleads.*')
        ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        ->join('products', 'prodleads.Prodid', '=', 'products.id')
        ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
        ->join('orders', 'prodleads.orderid', '=', 'orders.id')
        ->where('City',$city)
        ->orderBy('prodleads.id', 'DESC')
        ->get();

        $leads3 = json_decode($leads3,true);

        $leads_all = array_merge($leads1,$leads3);

        //function for sorting array into descending order
        function make_comparer()
        {
            $leads_all = func_get_args();
            $comparer = function($first, $second) use ($leads_all) {
                // Do we have anything to compare?
                while(!empty($leads_all)) {
                    // What will we compare now?
                    $criterion = array_shift($leads_all);

                    // Used to reverse the sort order by multiplying
                    // 1 = ascending, -1 = descending
                    $sortOrder = -1;
                    if (is_array($criterion)) {
                        $sortOrder = $criterion[1] == SORT_DESC ? -1 : 1;
                        $criterion = $criterion[0];
                    }

                    // Do the actual comparison
                    if ($first[$criterion] < $second[$criterion]) {
                        return -1 * $sortOrder;
                    }
                    else if ($first[$criterion] > $second[$criterion]) {
                        return 1 * $sortOrder;
                    }

                }

                // Nothing more to compare with, so $first == $second
                return 0;
            };

            return $comparer;
        }

        usort($leads_all, make_comparer('id'));

        $leads1 = $leads_all;

        $count = count($leads1);

        /*
        Logic for downloading CSV goes here -- starts here
        */
        //Check if the status is "download" as passed in the admin/index.blade.php file
        if(isset($_GET['download']))
        {
            $download = $_GET['download'];

            // Check if the status is All (this is when the user clicks on the View Leads buttton and wishes to download the data from there
            if($status=="All")
            {
                //extract the 'id','Designation,'city' and 'Department' of the logged_in_user
                $empname=DB::table('employees')->where('FirstName',$logged_in_user)->select('id','Designation3','city','Department3')->get();
                $empname=json_decode($empname);

                $empname1= $empname[0]->id;
                $empname2= $empname[0]->Designation3;
                $empname3= $empname[0]->city;
                $empname4= $empname[0]->Department3;



                // dd($status);
                $leads1  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('City',$city)
                ->where('MedName','!=',NULL)
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads1 = json_decode($leads1,true);


                $leads3  = DB::table('leads')
                ->select('leads.id','leads.created_at','leads.createdby','leads.fName','leads.mName','leads.lName','leads.MobileNumber','leads.Alternatenumber','leads.EmailId','leads.Source','leads.AssesmentReq','addresses.Address1','addresses.Address2','addresses.City','addresses.District','addresses.State','addresses.PinCode','addresses.PAddress1','addresses.PAddress2','addresses.PCity','addresses.PDistrict','addresses.PState','addresses.PPinCode','addresses.EAddress1','addresses.EAddress2','addresses.ECity','addresses.EDistrict','addresses.EState','addresses.EPinCode','products.SKUid','products.ProductName','products.DemoRequired','products.AvailabilityStatus','products.AvailabilityAddress','products.SellingPrice','products.Type','products.OrderStatus','products.Quantity','products.ModeofPayment','products.ModeofPaymentrent','products.OrderStatusrent','products.AdvanceAmt','products.StartDate','products.EndDate','products.OverdueAmt'
                ,'products.RentalPrice','products.Requestcreatedby','prodleads.comments','pharmacies.MedName','pharmacies.Strength','pharmacies.PQuantity','pharmacies.MedType','pharmacies.Price','pharmacies.PAvailabilityStatus','pharmacies.POrderStatus','pharmacies.PModeofpayment','pharmacies.Prequestcreatedby','pharmacies.PAssignedTo','pharmacies.PReceipt','pharmacies.PCheque','pharmacies.PCashStatus','pharmacies.PAmountPaid','pharmacies.PDiscount','pharmacies.PFinalAmount','pharmacies.PPostDiscountPrice')
                ->join('prodleads', 'leads.id', '=', 'prodleads.Leadid')
                ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                ->join('products', 'prodleads.Prodid', '=', 'products.id')
                ->join('pharmacies', 'prodleads.PharmacyId', '=', 'pharmacies.id')
                ->join('orders', 'prodleads.orderid', '=', 'orders.id')
                ->where('City',$city)
                ->where('products.Type',"Sell")
                ->orderBy('prodleads.id','DESC')
                ->get();

                $leads3 = json_decode($leads3,true);

                $leads_all = array_merge($leads1,$leads3);



                usort($leads_all, make_comparer('id'));

                $leads1 = array_values(array_unique($leads_all, SORT_REGULAR));;

// dd($leads1);
                $array = array( 'Lead Id', 'Created At' , 'Created By', 'Customer First Name', 'Customer Middle Name', 'Customer Middle Name', 'Customer Mobile', 'Customer Alternate Mobile Number',  'Email Id', 'Source','Assessment Required', 'Address1', 'Address2', 'City','District','State','PinCode', 'Present Address1', 'Present Address2', 'Present City','Present District','Present State','Present PinCode', 'Emergency Address1', 'Emergency Address2', 'Emergency City'
                ,'Emergency District','Emergency State','Emergency PinCode','SKUid','ProductName','DemoRequired','AvailabilityStatus','AvailabilityAddress','SellingPrice','Type','OrderStatus','Quantity','ModeofPayment','ModeofPaymentrent','OrderStatusrent','AdvanceAmt','StartDate','EndDate','OverdueAmt'
                ,'RentalPrice','Requestcreatedby','Comments','MedName','Strength','Quantity','MedType','Price','AvailabilityStatus','OrderStatus','Modeofpayment','requestcreatedby','AssignedTO','Receipt','Cheque','CashStatus','Amount Paid','Discount','FinalAmount','PostDiscountPrice');
                // $list = array (
                //     $leads
                // );
                //
                //         $lists = array (
                // array('aaa', 'bbb', 'ccc', 'dddd'),
                // array('123', '456', '789'),
                // array('aaa', 'bbb')
                // );
                // dd($list);

                    $filename = "download.csv";

                $fp = fopen('download.csv', 'w');

                fputcsv($fp, $array );
                foreach ($leads1 as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download($filename, 'download.csv', $headers);
            }
            /*
            Logic for downloading CSV goes here -- ends here
            */
        }
        session()->put('name',$logged_in_user);
                $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
                dd($count);

        return view('Vertical_ProductSelling_Pharmacy.allproductindex',compact('leads1','d','count','desig'));
    }

    public function productfilter(Request $request)
    {
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;

        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        // dd($logged_in_user);
        $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');


        // dd($pcheck);
        // dd($status1);

        if($pcheck=="Product - Selling")
        {

            $data1=DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','products.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('products', 'leads.id', '=', 'products.leadid')
            ->join('prodleads', 'products.id', '=', 'prodleads.Prodid')
            ->where('products.Type',"Sell")
            ->where('OrderStatus',$status1)
            ->where('addresses.City', $city)
            ->Where($filter1, 'like',   $keyword1 . '%')
            ->orderBy('products.id', 'DESC')
            ->paginate(50);


            return view('product.productfilter',compact('data1','keyword1','filter1'));
        }
    }

    public function pharmacyfilter(Request $request)
    {

        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;
        $logged_in_user=$request->name;

        // dd($status1);
        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        $desig2 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation2');
        $desig3 = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation3');

        // dd($desig3);
        $pcheck=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department2');
        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        if($desig3=="Pharmacy Manager")
        {

            // if($keyword1 == "")
            // {
            //
            //     return view('pharmacy.pharmacyindex');
            // }

            // dd($filter1);
            $data1=DB::table('leads')
            ->select('addresses.*','leads.*','prodleads.*','pharmacies.*')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('pharmacies', 'leads.id', '=', 'pharmacies.leadid')
            ->join('prodleads', 'pharmacies.id', '=', 'prodleads.Pharmacyid')
            ->where('pOrderStatus',$status1)
            ->where('addresses.City', $city)
            ->Where($filter1, 'like',   $keyword1 . '%')
            ->orderBy('pharmacies.id', 'DESC')
            ->paginate(50);


            // dd($leads);
            return view('product.pharmacyfilter',compact('data1','keyword1','filter1'));

        }

    }



}
