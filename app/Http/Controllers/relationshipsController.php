<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\relationship;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Session;


class relationshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      if(Auth::guard('admin')->check())
        {

           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                      $relationships= relationship::paginate(50);
                   return view('relationship.index',compact('relationships'));
                }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      if(Auth::guard('admin')->check())
        {
           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {

                return view('relationship.create');
                }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $relationship = new relationship;
        $this->validate($request,[
                'relationshiptype'=>'required',

            ]);
       $relationship->relationshiptype=$request->relationshiptype;


       $relationship->save();

       $langid = DB::table('relationships')->max('id');
       //    dd($langid);

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $request->relationshiptype." Relationship Added ";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;
       $activity->save();

       return redirect('/relationships');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::guard('admin')->check())
        {

           $logged_in_user=Auth::guard('admin')->user()->name;
                $check=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

                if($check=="Admin" || $check=="Management")
                {
                        $relationship= relationship::find($id);
                       return view('relationship.edit',compact('relationship'));
                }
                else
                {
                    return redirect('/admin');
                }
      }
      else
        {
            return redirect('/admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


             $c=DB::table('relationships')->where('id',$id)->value('relationshiptype');
        if($request->relationshiptype!=$c)
        {

        $relationship = relationship::find($id);
        $relationship->relationshiptype=$request->relationshiptype;

        $rel = $relationship->getDirty();
        // dd($lang);

        foreach($rel as $key=>$value)
        {
            $relc[] = $key;

        }

        foreach($rel as $key=>$value)
        {
            $relv[] = $value;
        }

        //this is for converting array values into a string with new line

        //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
        if(empty($relc))
        {
            $reld = " ";
            $relf = " ";
            $reld = trim($reld);
            $relf = trim($relf);
        }
        else
        {
            $reld = implode("\r\n", $relc);
            $relf = implode("\r\n", $relv);
        }


       $relationship->save();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       if($reld==" " || $reld==' ' || $reld=="" || $reld=='')
       {
           $reld = trim($reld);
           $relf = trim($relf);
       }
       else
       {
           $reld = $reld."\r\n";
           $relf = $relf."\r\n";
       }

       //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
       $fields_updated = $reld;
       $values_updated = $relf;

       if((trim($fields_updated)!==""))
       {
           // dd($fields_updated);
           $activity = new Activity;
           $activity->emp_id = $emp_id;
           $activity->activity = "Fields Updated";
           $activity->activity_time = new \DateTime();
           $activity->log_id = $log_id;
           $activity->field_updated = $fields_updated;
           $activity->value_updated = $values_updated;

           $activity->save();
       }

       session()->flash('message','Updated Successfully');
   }
       return redirect('/relationships');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $relationship=relationship::find($id);

        $rel_del = DB::table('relationships')->where('id',$id)->value('relationshiptype');

       $relationship->delete();

       //retrieving the session id of the person who is logged in
       $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

       //retrieving the username of the presently logged in user
       $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

       // retrieving the employee id of the presently logged in user
       $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

       //retrieving the log id of the person who logged in just now
       $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

       $activity = new Activity;
       $activity->emp_id = $emp_id;
       $activity->activity = $rel_del." Relationship Deleted";
       $activity->activity_time = new \DateTime();
       $activity->log_id = $log_id;

       $activity->save();

       session()->flash('message','Delete Successfully');
       return redirect('/relationships');
    }
}
