<?php

// This is the controller for customer care

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
use App\Log;
use App\lead;
use App\personneldetail;
use App\employee;
use App\service;
use App\address;
use App\language;
use App\product;
use App\enquiryproduct;
use App\Activity;
use App\productdetail;
use App\verticalcoordination;
use App\multipleservice;
use Session;
use Illuminate\Contracts\Auth\Guard;
use Mail;

use App\Http\Requests;

class ccController extends Controller
{
    //Checking whether the second project merge request is working or not
    /*
    function for authenticating middleware   -- code by murtuz
    */

    //checking whether the person who is trying to "Log In" is authenticated or not
    public function __construct(Guard $guard)
    {
        $this->user = $guard->user();
    }


    /**
    end of code
    */



    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    //this function gets call when the user clicks on "View Service Leads " in customer care dashboard
    public function index()
    {
        // This is coming from the route: Route::resource('cc','ccController');

        //if the session times out this is first check else redirect to login page
        if(Auth::guard('admin')->check())
        {
            //Join query for retrieving data from different tables

            /*
            We are joining 4 tables here:

            leads, services, personneldetails and addresses

            Basis of joins:
            a. Leads and PersonnelDetails on the basis of lead id
            b. Leads and Addresses on the basis of lead id
            c. Leads and Services on the basis of lead id

            Order all the data received on the basis
            */

            $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->orderBy('leads.id', 'DESC')
            ->paginate(50);

            // Checking if anything was passed as a session in the variable "name" or in the URL and trying to access it

            if (session()->has('name'))
            {
                $name1=session()->get('name');
            }
            else
            {
                $name1=$_GET['name'];
            }

            // "$name1" holds the name of the presently Logged in person

            //retrieving the Designation of the Logged in user
            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $city=DB::table('employees')->where('FirstName',$name1)->value('city');



            //if marketing manager try to access the page it will redirect to index page
            if($designation == "Marketing")
            {
                //this will set the session variable 'name' with the logged in user
                //can be accessed on the page using "session->has()"
                //moving to the "View Leads" section of Admin
                session()->put('name',$name1);
                return view('marketing.index',compact('leads'));
            }

            //If logged in user is ADMIN show the Admin page -- and so on for the others
            if($designation == "Admin")
            {
                //this will set the session variable 'name' with the logged in user
                //can be accessed on the page using "session->has()"
                //moving to the "View Leads" section of Admin
                session()->put('name',$name1);
                return view('admin.index',compact('leads'));
            }
            else
            {
                if($designation == "Management")
                {
                    //this will set the session variable 'name' with the logged in user
                    //can be accessed on the page using "session->has()"
                    //moving to the "View Leads" section of Management
                    session()->put('name',$name1);
                    return view('management.index',compact('leads'));
                }
                else{
                    if($designation == "Care_provider")
                    {
                        session()->put('name',$name1);
                        return view('careprovider.index',compact('leads'));
                    }
                    else
                    {
                        if($designation == "Branch Head")
                        {

                            $leads=DB::table('leads')
                            ->select('services.*','personneldetails.*','addresses.*','leads.*')
                            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                            ->join('services', 'leads.id', '=', 'services.LeadId')
                            ->where('Branch',$city)
                            ->orderBy('leads.id', 'DESC')
                            ->paginate(50);
                            session()->put('name',$name1);
                            return view('BranchHead.index',compact('leads'));
                        }
                        else
                        {

                            //this will set the session variable 'name' with the logged in user
                            //can be accessed on the page using "session->has()"
                            //moving to the "View Leads" section of Customer Care

                            session()->put('name',$name1);

                            return view('cc.index',compact('leads'));
                        }
                    }
                }
            }

        }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }

    public function dashboard()
    {

    }


    public function mobilevertical(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;
        $name = Auth::guard('admin')->user()->name;
        $designation="vertical";

        $adminid = DB::table('admins')->where('name',$logged_in_user)->value('id');

        $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
        $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //grabbing the city for which we want the counts
        //this city will be specific to the logged in user
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // dd($serviceType);

        // checking if the service type is "Physiotherapy" and calculating counts according to that

        // if($user_city=="Hyderabad" || $user_city=="Chennai" || $user_city=="Pune" || $user_city=="Hubballi-Dharwad")
        // {
        //     $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

        //     $newcount = DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status0)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $inprogresscount = DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status1)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();


        //     $convertedcount = DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status2)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $droppedcount = DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status3)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();

        //     $deferredcount = DB::table('leads')
        //             ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //             ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //             ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //             ->join('services', 'leads.id', '=', 'services.LeadId')
        //             ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //     ->where('Branch',$user_city)
        //     ->where('ServiceStatus',$status4)
        //     ->wherein('ServiceType',$Servicesarray)
        //     ->orderBy('id', 'DESC')
        //     ->count();
        // }

        if($serviceType=="Physiotherapy - Home")
        {

            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            // Depending on the branch , the ServiceStatus from above, the subtypes of Physiotherapy , we are fetching the count
            // this count consists of all counts (for vertical, all coordinators under him/her)
            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "Nursing Services" and calculating counts according to that
        else if($serviceType=="Nursing Services")
        {
            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "PSC" and calculating counts according to that
        else if($serviceType=="Personal Supportive Care")
        {
            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        // checking if the service type is "Mathrutvam" and calculating counts according to that
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        /*Code for Graphs and Pie Chart of Vertical Dashboard starts here */

        //Find the id of the vertical that is currently logged in
        $logged_in_user_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
        // dd($logged_in_user_id);

        $coords_under_vert = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->get();
        $coords_under_vert = json_decode($coords_under_vert,true);

        $statuscounts[-1][-1] = null;

        $coords_under_vert_count = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->count();

        // dd($coords_under_vert_count);

        // dd($coords_under_vert[0]['FirstName']);
        $User=null;

        for($i=0;$i<$coords_under_vert_count;$i++)
        {
            $User[] =  $coords_under_vert[$i]['FirstName'];

        }
        // dd($User[1]);

        if($User!=NULL)
        {
            for($j=0;$j<$coords_under_vert_count;$j++)
            {
                /* Retrieving the counts of all the users -- started */
                //assigning the possible statuses for the Coordinator
                $status1 = "In Progress";
                $status2  = "Converted";
                $status3  = "Dropped";
                $status4  = "Deferred";

                $logged_in_user = $User[$j];

                $assignedcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $convertedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status2)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status3)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $deferredcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status4)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $statuscounts[] = array
                (
                    "0" => $assignedcount2, "1" =>$convertedcount2,"2"=>$droppedcount2,"3"=>$deferredcount2
                );

                // $statuscounts[$j][0] = $assignedcount2;
                // $statuscounts[$j][1] = $convertedcount2;
                // $statuscounts[$j][2] = $droppedcount2;
                // $statuscounts[$j][3] = $deferredcount2;

                $coord_id1 = DB::table('employees')->where('FirstName',$User[$j])->value('id');
                $totalcount = DB::table('verticalcoordinations')->where('empid',$coord_id1)->count();


                // $percentage[$j][0] = ($statuscounts[$j][1]/$totalcount)*100;

                /* Retrieving the counts of all the users -- end */
            }

            // dd($statuscounts);


            // $colors1[-1] = null;
            //
            // // $colors1[0] = #4DFF4D;
            // // $colors1[1] = #ff0000;
            // // $colors1[2] = #0066ff;
            // // $colors1[3] = #b87333;
        }
        // dd($colors1[0]);
        $colors1 = array('#b87333','silver','gold','#e5e4e2');

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);


        // $code = DB::table('leads')->where('id',$id)->value('Country_Code');
        // $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

        $data1 = file_get_contents("indian_cities.json");

        $indian_cities = json_decode($data1,true);

        $count1 = count($indian_cities);


        session()->put('name',$name);
        return view('admin.mobilevertical',compact('count1','country_codes','indian_cities','count','newcount','convertedcount','inprogresscount','assignedcount2','convertedcount','droppedcount','deferredcount','assignedcount2','convertedcount2','droppedcount2','deferredcount2','User','coords_under_vert_count','colors1','emp','statuscounts','designation','user_city','serviceType'));

}
else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }


    }


    public function graphmobilevertial(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //grabbing the city for which we want the counts
        //this city will be specific to the logged in user
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // dd($serviceType);

        // checking if the service type is "Physiotherapy" and calculating counts according to that
        if($serviceType=="Physiotherapy - Home")
        {

            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            // Depending on the branch , the ServiceStatus from above, the subtypes of Physiotherapy , we are fetching the count
            // this count consists of all counts (for vertical, all coordinators under him/her)
            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "Nursing Services" and calculating counts according to that
        else if($serviceType=="Nursing Services")
        {
            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "PSC" and calculating counts according to that
        else if($serviceType=="Personal Supportive Care")
        {
            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        // checking if the service type is "Mathrutvam" and calculating counts according to that
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        /*Code for Graphs and Pie Chart of Vertical Dashboard starts here */

        //Find the id of the vertical that is currently logged in
        $logged_in_user_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
        // dd($logged_in_user_id);

        $coords_under_vert = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->get();
        $coords_under_vert = json_decode($coords_under_vert,true);

        $statuscounts[-1][-1] = null;

        $coords_under_vert_count = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->count();

        // dd($coords_under_vert_count);

        // dd($coords_under_vert[0]['FirstName']);
        $User=null;

        for($i=0;$i<$coords_under_vert_count;$i++)
        {
            $User[] =  $coords_under_vert[$i]['FirstName'];

        }
        // dd($User[1]);

        if($User!=NULL)
        {
            for($j=0;$j<$coords_under_vert_count;$j++)
            {
                /* Retrieving the counts of all the users -- started */
                //assigning the possible statuses for the Coordinator
                $status1 = "In Progress";
                $status2  = "Converted";
                $status3  = "Dropped";
                $status4  = "Deferred";

                $logged_in_user = $User[$j];

                $assignedcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $convertedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status2)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status3)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $deferredcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status4)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $statuscounts[] = array
                (
                    "0" => $assignedcount2, "1" =>$convertedcount2,"2"=>$droppedcount2,"3"=>$deferredcount2
                );

                // $statuscounts[$j][0] = $assignedcount2;
                // $statuscounts[$j][1] = $convertedcount2;
                // $statuscounts[$j][2] = $droppedcount2;
                // $statuscounts[$j][3] = $deferredcount2;


                $coord_id1 = DB::table('employees')->where('FirstName',$User[$j])->value('id');
                $totalcount = DB::table('verticalcoordinations')->where('empid',$coord_id1)->count();

                // $percentage[$j][0] = ($statuscounts[$j][1]/$totalcount)*100;

                /* Retrieving the counts of all the users -- end */
            }

            // dd($statuscounts);


            // $colors1[-1] = null;
            //
            // // $colors1[0] = #4DFF4D;
            // // $colors1[1] = #ff0000;
            // // $colors1[2] = #0066ff;
            // // $colors1[3] = #b87333;
        }
        // dd($colors1[0]);
        $colors1 = array('#b87333','silver','gold','#e5e4e2');

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $code = DB::table('leads')->where('id',$id)->value('Country_Code');
        $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

        $data1 = file_get_contents("indian_cities.json");

        $indian_cities = json_decode($data1,true);

        $count1 = count($indian_cities);




        return view('admin.graphmobilevertical',compact('code','country_name','count1','country_codes','indian_cities','count','newcount','convertedcount','inprogresscount','assignedcount2','convertedcount','droppedcount','deferredcount','assignedcount2','convertedcount2','droppedcount2','deferredcount2','percentage','User','coords_under_vert_count','colors1','emp','statuscounts'));

    }
     else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }
    }
    public function indivdualgraphvertial(Request $request)
    {

        if(Auth::guard('admin')->check())
        {

        //retrieving the name of the user who is logged in
        $logged_in_user = Auth::guard('admin')->user()->name;

        // dd($logged_in_user);

        //assigning the possible statuses for the Verticals

        $status0 = "New";
        $status1 = "In Progress";
        $status2  = "Converted";
        $status3  = "Dropped";
        $status4  = "Deferred";

        //grabbing the city for which we want the counts
        //this city will be specific to the logged in user
        $user_city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');
        $serviceType = DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');

        // dd($serviceType);

        // checking if the service type is "Physiotherapy" and calculating counts according to that
        if($serviceType=="Physiotherapy - Home")
        {

            $Servicesarray = ["Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy"];

            // Depending on the branch , the ServiceStatus from above, the subtypes of Physiotherapy , we are fetching the count
            // this count consists of all counts (for vertical, all coordinators under him/her)
            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "Nursing Services" and calculating counts according to that
        else if($serviceType=="Nursing Services")
        {
            $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();
        }

        // checking if the service type is "PSC" and calculating counts according to that
        else if($serviceType=="Personal Supportive Care")
        {
            $Servicesarray = ["Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        // checking if the service type is "Mathrutvam" and calculating counts according to that
        else if($serviceType=="Mathrutvam - Baby Care")
        {
            $Servicesarray = ["Mathrutvam - Baby Care","Infant Care","Nanny Care"];

            $newcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status0)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $inprogresscount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status1)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();


            $convertedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status2)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $droppedcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status3)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

            $deferredcount = DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
            ->where('Branch',$user_city)
            ->where('ServiceStatus',$status4)
            ->wherein('ServiceType',$Servicesarray)
            ->orderBy('id', 'DESC')
            ->count();

        }

        /*Code for Graphs and Pie Chart of Vertical Dashboard starts here */

        //Find the id of the vertical that is currently logged in
        $logged_in_user_id = DB::table('employees')->where('FirstName',$logged_in_user)->value('id');
        // dd($logged_in_user_id);

        $coords_under_vert = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->get();
        $coords_under_vert = json_decode($coords_under_vert,true);

        $statuscounts[-1][-1] = null;

        $coords_under_vert_count = DB::table('employees')->select('employees.FirstName')->where('under',$logged_in_user_id)->count();

        // dd($coords_under_vert_count);

        // dd($coords_under_vert[0]['FirstName']);
        $User=null;

        for($i=0;$i<$coords_under_vert_count;$i++)
        {
            $User[] =  $coords_under_vert[$i]['FirstName'];

        }
        // dd($User[1]);

        if($User!=NULL)
        {
            for($j=0;$j<$coords_under_vert_count;$j++)
            {
                /* Retrieving the counts of all the users -- started */
                //assigning the possible statuses for the Coordinator
                $status1 = "In Progress";
                $status2  = "Converted";
                $status3  = "Dropped";
                $status4  = "Deferred";

                $logged_in_user = $User[$j];

                $assignedcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $convertedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status2)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $droppedcount2 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status3)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $deferredcount2= DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status4)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->count();

                $statuscounts[] = array
                (
                    "0" => $assignedcount2, "1" =>$convertedcount2,"2"=>$droppedcount2,"3"=>$deferredcount2
                );

                // $statuscounts[$j][0] = $assignedcount2;
                // $statuscounts[$j][1] = $convertedcount2;
                // $statuscounts[$j][2] = $droppedcount2;
                // $statuscounts[$j][3] = $deferredcount2;


                $coord_id1 = DB::table('employees')->where('FirstName',$User[$j])->value('id');
                $totalcount = DB::table('verticalcoordinations')->where('empid',$coord_id1)->count();

                // $percentage[$j][0] = ($statuscounts[$j][1]/$totalcount)*100;

                /* Retrieving the counts of all the users -- end */
            }

            // dd($statuscounts);


            // $colors1[-1] = null;
            //
            // // $colors1[0] = #4DFF4D;
            // // $colors1[1] = #ff0000;
            // // $colors1[2] = #0066ff;
            // // $colors1[3] = #b87333;
        }
        // dd($colors1[0]);
        $colors1 = array('#b87333','silver','gold','#e5e4e2');



        return view('admin.individualgraphvertical',compact('newcount','convertedcount','inprogresscount','assignedcount2','convertedcount','droppedcount','deferredcount','assignedcount2','convertedcount2','droppedcount2','deferredcount2','percentage','User','coords_under_vert_count','colors1','emp','statuscounts'));

}
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }

    public function branchdeatilsadmin(Request $request)
    {
        if(Auth::guard('admin')->check())
        {

        $selectedbranch = $request->dropdownval1;
        return view('admin.branchdetails',compact('selectedbranch'));
          }
        else
        {
            //if after checking we realise the user session has timed out, redirect to the login page
            return redirect('/admin');
        }

    }


    public function assignto(Request $request)
    {


        $servicetype = $request->servicetype;
        $branch = $request->branch;


        if($branch == "Bengaluru")
        {

            $matchThese = ['Designation' => 'Vertical Head','Department' => $servicetype,'city' => $branch];
            // $data=DB::table('employees')->where('Designation','Vertical Head')->get();
            $data = DB::table('employees')->where($matchThese)->get();
            return view('cc.dropdown',compact('data'));
        }
        else
        {

            // $matchThese = ['Designation' => 'Vertical Head','city' => $branch];
            // $data = DB::table('employees')->where($matchThese)->get();

            $data=DB::table('employees')->where('city',$branch)->where('Designation',"Vertical Head")->value('id');
            // dd('asd');
            if($data=="" || $data==NULL)
            {

                $data=DB::table('employees')->where('city',$branch)->where('Designation',"Branch Head")->get();
                // dd($data);
                return view('cc.dropdown',compact('data'));
            }
            else
            {
                $data=DB::table('employees')->where('city',$branch)->where('Designation',"Vertical Head")->get();
                return view('cc.dropdown',compact('data'));

            }

        }

    }


        public function assignto1(Request $request)
    {


        $servicetype = $request->servicetype;
        $branch = $request->branch;
        $i=$request->i;


        if($branch == "Bengaluru")
        {

            $matchThese = ['Designation' => 'Vertical Head','Department' => $servicetype,'city' => $branch];
            // $data=DB::table('employees')->where('Designation','Vertical Head')->get();
            $data = DB::table('employees')->where($matchThese)->get();
            return view('cc.ajaxcalldropdown',compact('data','i'));
        }
        else
        {

            // $matchThese = ['Designation' => 'Vertical Head','city' => $branch];
            // $data = DB::table('employees')->where($matchThese)->get();

            $data=DB::table('employees')->where('city',$branch)->where('Designation',"Vertical Head")->value('id');
            // dd('asd');
            if($data=="" || $data==NULL)
            {

                $data=DB::table('employees')->where('city',$branch)->where('Designation',"Branch Head")->get();
                // dd($data);
                return view('cc.ajaxcalldropdown',compact('data','i'));
            }
            else
            {
                $data=DB::table('employees')->where('city',$branch)->where('Designation',"Vertical Head")->get();
                return view('cc.ajaxcalldropdown',compact('data','i'));

            }

        }

    }

    // This function is for adding the Search functionality on the View Leads page and Individual Coutns page for Customer care, Admin, Management, Vertical Heads
    public function filter(Request $request)
    {
        // This is coming from the route :: Route::get('allleadc','ccController@filter');

        //values retrieved here from index page
        $keyword1=$request->keyword1;
        $filter1=$request->filter1;
        $status1=$request->status1;

        // dd($status1);
        // return view('alldata',compact('keyword1','filter1'));
        //$data = DB::table('leads')->where($filter1, 'like', $keyword1 .'%')->get();
        //$data = DB::table('leads')->Where($filter1, 'like',   $keyword1 . '%')->get();
        // $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')->join('addresses', 'leads.id', '=', 'addresses.leadid')->join('services', 'leads.id', '=', 'services.LeadId')->Where($filter1, 'like',   $keyword1 . '%')->get();

        //$data=DB::table('leads')->get();

        // $lead=DB::table('leads')
        //      ->select('services.*','personneldetails.*','addresses.*','verticalcoordinations.*','leads.*')
        //      ->join('verticalcoordinations', 'leads.id', '=', 'verticalcoordinations.leadid')
        //      ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        //      ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        //      ->join('services', 'leads.id', '=', 'services.LeadId')
        //      ->where('verticalcoordinations.empid',$empname1)
        //      ->orderBy('leads.id', 'DESC')
        //      ->get();

        //to ensure that all tables are not fetched -- only for the logged in user

        $logged_in_user = Auth::guard('admin')->user()->name;


        $desig = DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');

        $city = DB::table('employees')->where('FirstName',$logged_in_user)->value('city');

        //if the user, deletes the value that he/she entered in the "keyword" field, the page should reload with the "non-filtered" content
        if($keyword1 == "")
        {

            return view('cc.index1');
        }

        //checking whether the status received from the URL is "NULL" or not
        if($status1!=NULL)
        {
            //search filter for Customer care
            if($desig=="Customer Care")
            {
                //if we have clicked on "View Service Leads" in "Customer Care" and we wish to apply search functionality there, then status is ALL
                if($status1=="All")
                {

                    //this will retreive the values based on the type of filter(dropdown) and the keyword(like Customer name, Assigned to, Lead source, Service Type) as entered by the user
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')->paginate(200);


                    return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
                }
                else
                {
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('services.ServiceStatus',$status1)
                    ->where('leads.createdby',$logged_in_user)
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
                }
            }
            if($desig=="Marketing")
            {
                if($status1=="All")
                {

                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('marketing.alldata',compact('data1','keyword1','filter1','status1'));
                }
                else
                {
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('services.ServiceStatus',$status1)
                    ->where('leads.createdby',$logged_in_user)
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('marketing.alldata',compact('data1','keyword1','filter1','status1'));

                }
            }

            //search filter for "Admin" or "Management"
            if($desig=="Admin" || $desig=="Management")
            {
                //refer to Customer Care "if" for comments
                if($status1=="All")
                {

                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1'));
                }
                else
                {
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('ServiceStatus',$status1)
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1'));
                }
            }

            //search filter for "Branch Head"
            if($desig=="Branch Head")
            {
                //refer to Customer Care "if" for comments
                if($status1=="All")
                {

                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('Branch',$city)
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1'));
                }
                else
                {
                    $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                    ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                    ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                    ->join('services', 'leads.id', '=', 'services.LeadId')
                    ->where('Branch',$city)
                    ->Where($filter1, 'like',   $keyword1 . '%')
                    ->where('ServiceStatus',$status1)
                    ->orderBy('leads.id', 'DESC')->paginate(200);

                    return view('admin.alldata',compact('data1','keyword1','filter1','status1'));
                }
            }


            //This search filter is for vertical heads except "Bangalore"
            if($city=="Hubballi-Dharwad" || $city=="Hyderabad" || $city=="Pune" || $city=="Chennai")
            {
                //if it's a vertical head
                if($desig=="Vertical Head")
                {

                    //assign all "Service Types" to vertical heads except the ones from "bangalore"
                    $Servicesarray = ["Nursing Services","Respiratory Care","Intravenous Therapy","Registered Nurses","Medication Administration","Nutrition","Assisting in Elimination","Rehabilitation","Diabetic/Wound Care","Post Operative Care", "Physiotherapy - clinic","Physiotherapy - Home","Physiotherapy","Mathrutvam - Baby Care","Infant Care","Nanny Care","Personal Supportive Care","Personal Care","Companionship","Live -in Care","Palliative/End of Life Care","Alzheimers and Dementia"];

                    //refer to comments in "customer care" ifs
                    if($status1=="All")
                    {

                        $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->where('Branch',$city)
                        ->wherein('ServiceType',$Servicesarray)
                        ->orderBy('leads.id', 'DESC')->paginate(200);

                        return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1'));

                    }
                    else
                    {
                        $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
                        ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
                        ->join('addresses', 'leads.id', '=', 'addresses.leadid')
                        ->join('services', 'leads.id', '=', 'services.LeadId')
                        ->Where($filter1, 'like',   $keyword1 . '%')
                        ->where('Branch',$city)
                        ->where('ServiceStatus',$status1)
                        ->wherein('ServiceType',$Servicesarray)
                        ->orderBy('leads.id', 'DESC')->paginate(200);

                        return view('verticalheads.alldata',compact('data1','keyword1','filter1','status1'));
                    }
                }
                //if it's a Coordinator from all cities except "Bangalore"
                else
                {
                    if($status1=="All")
                    {
                        $data1 = DB::table('leads')
                        ->select('employees.*','services.*','leads.*')
                        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                        ->join('employees','verticalcoordinations.empid','=','employees.id')
                        ->join('services', 'leads.id', '=', 'services.Leadid')
                        ->where('FirstName',$logged_in_user)
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);

                        return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
                    }
                    else
                    {
                        $data1 = DB::table('leads')
                        ->select('employees.*','services.*','leads.*')
                        ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                        ->join('employees','verticalcoordinations.empid','=','employees.id')
                        ->join('services', 'leads.id', '=', 'services.Leadid')
                        ->where('ServiceStatus',$status1)
                        ->where('FirstName',$logged_in_user)
                        ->orderBy('leads.id', 'DESC')
                        ->paginate(50);

                        return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
                    }
                }
            }
            //if the people belong to "Bangalore"
            else
            {
                //refer to comments in "customer care" ifs above
                if($status1=="All")
                {
                    $data1 = DB::table('leads')
                    ->select('employees.*','services.*','leads.*')
                    ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                    ->join('employees','verticalcoordinations.empid','=','employees.id')
                    ->join('services', 'leads.id', '=', 'services.Leadid')
                    ->where('FirstName',$logged_in_user)
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(50);

                    return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
                }
                else
                {
                    $data1 = DB::table('leads')
                    ->select('employees.*','services.*','leads.*')
                    ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                    ->join('employees','verticalcoordinations.empid','=','employees.id')
                    ->join('services', 'leads.id', '=', 'services.Leadid')
                    ->where('ServiceStatus',$status1)
                    ->where('FirstName',$logged_in_user)
                    ->orderBy('leads.id', 'DESC')
                    ->paginate(50);

                    return view('cc.alldata',compact('data1','keyword1','filter1','status1'));

                }
            }
        }
        else
        {
            if($status1=="All")
            {
                $data1 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->paginate(50);

                return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
            }
            else
            {
                $data1 = DB::table('leads')
                ->select('employees.*','services.*','leads.*')
                ->join('verticalcoordinations','leads.id','=','verticalcoordinations.leadid')
                ->join('employees','verticalcoordinations.empid','=','employees.id')
                ->join('services', 'leads.id', '=', 'services.Leadid')
                ->where('ServiceStatus',$status1)
                ->where('FirstName',$logged_in_user)
                ->orderBy('leads.id', 'DESC')
                ->paginate(50);

                return view('cc.alldata',compact('data1','keyword1','filter1','status1'));
            }

        }
        // dd($city);

    }
    // else
    // {

    //     //join query to retrieve all the search filter values
    //     $data1 = DB::table('leads')->select('services.*','personneldetails.*','addresses.*','leads.*')
    //     ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
    //     ->join('addresses', 'leads.id', '=', 'addresses.leadid')
    //     ->join('services', 'leads.id', '=', 'services.LeadId')
    //     ->Where($filter1, 'like',   $keyword1 . '%')
    //     ->orderBy('leads.id', 'DESC')->paginate(200);

    //     return view('cc.alldata',compact('data1','keyword1','filter1'));
    // }
    // }



    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Respo
    */

    //on clicking New Lead Button this function is called
    //this function helps the customer care create the Lead Registration form
    public function create()
    {
        //checking whether the session has timed out or not and then redirecting the user is login page if session has timed out
        if(Auth::guard('admin')->check())
        {
            //retrieving the name that was passed in admin/index.blade view as cc/create route
            $name=$_GET['name'];

            //retrieve the data values in json format
            //retrieving the values from the table to populate the dropdowns in the create form
            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();

            //retrieve the designation of the logged in user
            $designation=DB::table('employees')->where('FirstName',$name)->value('Designation');
            $designation2=DB::table('employees')->where('FirstName',$name)->value('Designation2');
            $designation3=DB::table('employees')->where('FirstName',$name)->value('Designation3');
            $department2=DB::table('employees')->where('FirstName',$name)->value('department2');

            $adminid = DB::table('admins')->where('name',$name)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');
            // dd($roles);

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            // $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            // $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);


            // dd($count);

            //retrieve the Lead registration form view depending on the Designation from above line

            //retrieve the view depending on the Designation from above line


            if($designation == "Marketing")
            {
                $designation="marketing";
                    return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
            }
            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling")
                {

                    $designation=$roles;
                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                }
                else
                {
                    if($roles=="Coordinator_ProductRental")
                    {
                         $designation=$roles;
                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager")
                        {
                            $designation=$roles;
                           return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager")
                            {
                                $designation=$roles;
                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy")
                                {
                                    $designation=$roles;
                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy")
                                    {

                                         $designation=$roles;
                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy")
                                        {
                                            $designation=$roles;
                                            return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                                        }
                                        else
                                        {

                                            if($roles=="Coordinator_FieldOfficer")
                                            {
                                              $designation=$roles;
                                                return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldExecutive")
                                                {
                                                    $designation=$roles;
                                                    return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                                                }
                                                else
                                                {
                                                    $designation="coordinator";

                                                        return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));

                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }



            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
            }
            else
            if($designation == "Management")
            {
                $designation="management";
                return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
            }
            else
            {
                if($designation == "Branch Head")
                {
                    return view('BranchHead.leads',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status'));
                }
                else
                {
                    $designation="customercare";
                    return view('admin.lead',compact('count1','country_codes','indian_cities','count','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status','designation'));
                }
            }
        }
        else
        {
            return redirect('/admin');
        }

    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    // All the data entered in the fields is stored in the db in this function
    public function store(Request $request)
    {


 $logged_in_user = $request->loginname;

$client_firstname=$request->clientfname;

$client_mobile_number=$request->clientmob;

$check_for_existing=DB::table('leads')->where('MobileNumber',$client_mobile_number)->value('id');
$existingcheck=$request->existing;


//fetching no. of service client requested for 
$countofservice=$request->countofservice;


// Checking if the client is already existing then show the leads related to that and ask user whether they want to create the same lead or not


$Preferredlanguages=$request->Preferredlanguages;

$Preferredlanguages = str_replace(array('[',']' ,),' ',$Preferredlanguages);
                $Preferredlanguages = preg_replace('/"/', '', $Preferredlanguages);
                $Preferredlanguages = trim($Preferredlanguages);

// dd($request->all());

if($check_for_existing!=NULL && $existingcheck=="existing")
{

    $logged_in_user = Auth::guard('admin')->user()->name;


    $leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('MobileNumber',$client_mobile_number)
            ->orderBy('leads.id', 'DESC')
            ->paginate(150);


             $fName=$request->clientfname;
        $mName=$request->clientmname;
        $lName=$request->clientlname;
        $EmailId=$request->clientemail;
        $Source=$request->source;
        // $Country_Name=$country_name;
        $Country_Code= $request->code;
        $MobileNumber=$request->clientmob;
        $Alternatenumber=$request->clientalternateno;
        $EmergencyContact =$request->EmergencyContact;
        $AssesmentReq=$request->assesmentreq;
        $createdby=$request->loginname;
        $Referenceid=$request->reference;


        //fetch and store all the address details
        $Address1=$request->Address1;
        $Address2=$request->Address2;
        $City=$request->City;
        $District=$request->District;
        $State=$request->State;
        $PinCode=$request->PinCode;


        $same1=$request->presentcontact;

            $PAddress1=$request->PAddress1;
            $PAddress2=$request->PAddress2;
            $PCity=$request->PCity;
            $PDistrict=$request->PDistrict;
            $PState=$request->PState;
            $PPinCode=$request->PPinCode;


        $same=$request->emergencycontact;


            $EAddress1=$request->EAddress1;
            $EAddress2=$request->EAddress2;
            $ECity=$request->ECity;
            $EDistrict=$request->EDistrict;
            $EState=$request->EState;
            $EPinCode=$request->EPinCode;





        $PtfName=$request->patientfname;
        $PtmName=$request->patientmname;
        $PtlName=$request->patientlname;
        $age=$request->age;
        $Gender=$request->gender;
        $Relationship=$request->relationship;
        $Occupation=$request->Occupation;
        $AadharNum=$request->aadhar;
        $AlternateUHIDType=$request->AlternateUHIDType;
        $AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $PTAwareofDisease=$request->PTAwareofDisease;




       $ServiceType=$request->servicetype;
       $GeneralCondition=$request->GeneralCondition;
       $LeadType=$request->leadtype;
       $Branch=$request->branch;
       $RequestDateTime=$request->requesteddate;
       $AssignedTo=$request->assigntonew;
       $QuotedPrice=$request->quotedprice;
       $ExpectedPrice=$request->expectedprice;
       $ServiceStatus=$request->servicestatus;
       $PreferedGender=$request->preferedgender;
       $PreferedLanguage=$Preferredlanguages;
       $Remarks=$request->remarks;
       $langId=$request->preferedlanguage;
       $AssignedTo=$request->assigntonew;
       $Preferredlanguages=$request->Preferedlanguages;


       $ServiceType2=$request->servicetype2;
       $GeneralCondition2=$request->GeneralCondition2;
       $LeadType2=$request->leadtype2;
       $Branch2=$request->branch2;
       $RequestDateTime2=$request->requesteddate2;
       $AssignedTo2=$request->assigntonew2;
       $QuotedPrice2=$request->quotedprice2;
       $ExpectedPrice2=$request->expectedprice2;
       $ServiceStatus2=$request->servicestatus2;
       $PreferedGender2=$request->preferedgender2;
       $Remarks2=$request->remarks2;
       $langId2=$request->preferedlanguage2;
       $AssignedTo2=$request->assigntonew2;
       $PreferredLanguages2=$request->Preferedlanguages2;


       $ServiceType3=$request->servicetype3;
       $GeneralCondition3=$request->GeneralCondition3;
       $LeadType3=$request->leadtype3;
       $Branch3=$request->branch3;
       $RequestDateTime3=$request->requesteddate3;
       $AssignedTo3=$request->assigntonew3;
       $QuotedPrice3=$request->quotedprice3;
       $ExpectedPrice3=$request->expectedprice3;
       $ServiceStatus3=$request->servicestatus3;
       $PreferedGender3=$request->preferedgender3;

       $Remarks3=$request->remarks3;
       $langId3=$request->preferedlanguage3;
       $AssignedTo3=$request->assigntonew3;
       $PreferredLanguages3=$request->Preferedlanguages3;

       $ServiceType4=$request->servicetype4;
       $GeneralCondition4=$request->GeneralCondition4;
       $LeadType4=$request->leadtype4;
       $Branch4=$request->branch4;
       $RequestDateTime4=$request->requesteddate4;
       $AssignedTo4=$request->assigntonew4;
       $QuotedPrice4=$request->quotedprice4;
       $ExpectedPrice4=$request->expectedprice4;
       $ServiceStatus4=$request->servicestatus4;
       $PreferedGender4=$request->preferredgender4;

       $Remarks4=$request->remarks4;
       $langId4=$request->preferedlanguage4;
       $AssignedTo4=$request->assigntonew4;
       $PreferredLanguages4=$request->Preferedlanguages4;




    //    dd($Preferredlanguages);


        $pid=$request->pid;
        $checkbox=$request->addproduct;
        $checkdropbutton=$request->droplead;

        // dd($checkbox);

        $designations=DB::table('employees')->where('FirstName',$logged_in_user)->value('Designation');
        // dd($designations);

        if($designations=="Admin")
        {
            $layout="home";
        }
        else
        {
            if($designations=="Coordinator")
            {
                $layout="coordinator";
            }
            else
            {
                if($designations=="Vertical Head")
                {
                    $layout="vertical";
                }
                else
                {
                    if($designations=="Management")
                    {
                        $layout="management";
                    }
                    else
                    {
                        if($designations=="Branch Head")
                        {
                            $layout="BranchHead";
                        }
                        else
                        {
                            if($designations=="Marketing")
                            {
                                $layout="marketing";
                            }
                            else
                            {
                                $layout="customercare";
                            }
                        }
                    }
                }
            }
        }


    return view('admin.showlead',compact('leads','fName','mName','lName','EmailId','Source','Country_Code','MobileNumber','Alternatenumber','EmergencyContact','AssesmentReq','createdby','Referenceid','Address1','Address2','City','District','State','PinCode','same1','PAddress1','PAddress2','PCity','PDistrict','PState','PPinCode','same','EAddress1','EAddress2','ECity','EDistrict','EState','EPinCode','PtfName','PtmName','PtlName','age','Gender','Relationship','Occupation','AadharNum','AlternateUHIDType','AlternateUHIDNumber','PTAwareofDisease','ServiceType','GeneralCondition','LeadType','Branch','RequestDateTime','AssignedTo','QuotedPrice','ExpectedPrice','ServiceStatus','PreferedGender','PreferedLanguage','Remarks','langId','checkbox','logged_in_user','layout','pid','checkdropbutton','ServiceType2','GeneralCondition2','LeadType2','Branch2','RequestDateTime2','AssignedTo2','QuotedPrice2','ExpectedPrice2','PreferedGender2','PreferredLanguages2','Remarks2','ServiceType3','GeneralCondition3','LeadType3','Branch3','RequestDateTime3','AssignedTo3','QuotedPrice3','ExpectedPrice3','PreferedGender3','PreferredLanguages3','Remarks3','ServiceType4','GeneralCondition4','LeadType4','Branch4','RequestDateTime4','AssignedTo4','QuotedPrice4','ExpectedPrice4','PreferedGender4','PreferredLanguages4','Remarks4','countofservice'));
}
else
{



        /* Code for storing the details entered in the form starts here */
        //Whatever reference is selected by the user , it's id needs to be retrieved and passed into the Lead reference id column
        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=Null;

        //retrieve the employee id of the logged in user
        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        $code = $request->code;



        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $country_name=NULL;
        for($i=0;$i<$count;$i++)
        {
            if($code == $country_codes[$i]['dial_code'])
            {
                $country_name = $country_codes[$i]['name'];
            }
        }

        // dd($country_name);

        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        //If the source field is not filled, then take the reference id of the selected Reference
        if($request->source == Null)
        {
            $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
            $source=$refer;
        }
        //if the Other option is selected, then whatever is filled in the source field
        else
        {
            $source=$request->source;
        }

$time = time();

$current_time=date('Y-m-d',$time);

$mytime=  date('Y-m-d', strtotime("-1 month"));

// dd($mytime);

$check_for_existing=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')->where('MobileNumber',$client_mobile_number)->where('ServiceType',$request->servicetype)->where('Branch',$request->branch)->where('ServiceStatus',"Dropped")->orwhere('leads.created_at',$current_time)->whereBetween('leads.created_at',[$mytime,$current_time])->orderby('leads.id','DESC')->get();
// $check_for_existing=DB::table('leads')->where('MobileNumber',$client_mobile_number)->orderby('id','DESC')->first();

// dd($check_for_existing);

if(count($check_for_existing)>0)
{

$check_for_existing=json_decode($check_for_existing,true);
        $lid= $check_for_existing[0]['id'];
        // dd($lid);

$created_at_time=DB::table('leads')->where('id',$lid)->value('created_at');
$lead_time = substr($created_at_time, 0,10);





$leads=DB::table('leads')
            ->select('services.*','personneldetails.*','addresses.*','leads.*')
            ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            ->join('services', 'leads.id', '=', 'services.LeadId')
            ->where('leads.id',$lid)
            ->orderBy('leads.id', 'DESC')
            ->get();

            $leads=json_decode($leads);
        $branch_name= $leads[0]->Branch;
        $service_request= $leads[0]->ServiceType;
        $service_status= $leads[0]->ServiceStatus;


if($lead_time>=$mytime)
{

   if($service_status=="Dropped")
   {

    if($branch_name==$request->branch && $service_request== $request->servicetype)
    {


        $leadid=$lid;

        $service_id=DB::table('services')->where('Leadid',$lid)->value('id');

       // dd($lid);
       if($request->droplead!=NULL)
       {

        $update_status=service::find($service_id);
        $update_status->ServiceStatus='Dropped';
       }
       else
       {
        $update_status=service::find($service_id);
       $update_status->ServiceStatus='New';
       $update_status->save();
        }
    }
    else
    {

        $lead = new lead;
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;
        $lead->createdby=$request->loginname;
        $lead->Referenceid=$ref;
        $lead->empid=$empid;
        $lead->save();

        $leadid=DB::table('leads')->max('id');

        $address = new address;

        //fetch and store all the address details
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;


        $same1=$request->presentcontact;

        if($same1=='same')
        {
            $address->PAddress1=$request->Address1;
            $address->PAddress2=$request->Address2;
            $address->PCity=$request->City;
            $address->PDistrict=$request->District;
            $address->PState=$request->State;
            $address->PPinCode=$request->PinCode;
        }

        else
        {
            $address->PAddress1=$request->PAddress1;
            $address->PAddress2=$request->PAddress2;
            $address->PCity=$request->PCity;
            $address->PDistrict=$request->PDistrict;
            $address->PState=$request->PState;
            $address->PPinCode=$request->PPinCode;
        }

        $same=$request->emergencycontact;
        if($same=='same')
        {
            $address->EAddress1=$request->Address1;
            $address->EAddress2=$request->Address2;
            $address->ECity=$request->City;
            $address->EDistrict=$request->District;
            $address->EState=$request->State;
            $address->EPinCode=$request->PinCode;
        }

        else
        {
            $address->EAddress1=$request->EAddress1;
            $address->EAddress2=$request->EAddress2;
            $address->ECity=$request->ECity;
            $address->EDistrict=$request->EDistrict;
            $address->EState=$request->EState;
            $address->EPinCode=$request->EPinCode;
        }


        $address->leadid=$leadid;
        $address->empid=$empid;
        $address->save();
        $addressid=DB::table('addresses')->max('id');


        //enter the personnel details under the SERVICE REQUIRED form
        $personnel = new personneldetail;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
        $personnel->Addressid=$addressid;
        $personnel->Leadid=$leadid;

        $personnel->save();



        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
        $langid=json_decode($l);







        //Enter the Service Details and store them
        $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assignedto;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;

        if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }

        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$Preferredlanguages;
        $service->Remarks=$request->remarks;
        $service->langId=$langid;
        $service->LeadId=$leadid;

        $service->save();




        //for loop to get all the details of other service requested

//fetch value to count how many service has been requested
$countofservice=$request->countofservice;

// dd($countofservice);

if($countofservice > 1)
        {

            for($m=2;$m<=$countofservice;$m++)
            {

               
                $ServiceType1="servicetype".$m;
                $GeneralCondition1="GeneralCondition".$m;
                $LeadType1="leadtype".$m;
                $Branch1="branch".$m;
                $RequestDateTime1="requesteddate".$m;
                $AssignedTo1="assignedto".$m;
                $QuotedPrice1="quotedprice".$m;
                $ExpectedPrice1="expectedprice".$m;
                $ServiceStatus1="servicestatus".$m;
                $PreferedGender1="preferedgender".$m;
                $PreferedLanguage1="$Preferredlanguages".$m;
                $Remarks1="remarks".$m;
                $langId1="preferedlanguage".$m;
                


                $ServiceType=$request->$ServiceType1;
                $GeneralCondition=$request->$GeneralCondition1;
                $LeadType=$request->$LeadType1;
                $Branch=$request->$Branch1;
                $RequestDateTime=$request->$RequestDateTime1;
                $AssignedTo=$request->$AssignedTo1;
                $QuotedPrice=$request->$QuotedPrice1;
                $ExpectedPrice=$request->$ExpectedPrice1;
                //$ServiceStatus=$request->$ServiceStatus1;
                $PreferedGender=$request->$PreferedGender1;
                $PreferedLanguage=$PreferedLanguage1;
                $Remarks=$request->$Remarks1;
                $langId=$request->$langId1;
               



                  $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$ServiceType;
        $service->GeneralCondition=$GeneralCondition;
        $service->LeadType=$LeadType;
        $service->Branch=$Branch;
        $service->RequestDateTime=$RequestDateTime;
        $service->AssignedTo=$AssignedTo;
        $service->QuotedPrice=$QuotedPrice;
        $service->ExpectedPrice=$ExpectedPrice;

        if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }

        $service->PreferedGender=$PreferedGender;
        $service->PreferedLanguage=$PreferedLanguage;
        $service->Remarks=$Remarks;
        $service->langId=$langId;
        $service->LeadId=$leadid;

        $service->save();

        $maxcountforservice=DB::table('services')->max('id');

        $multipleservice=new multipleservice;
        $multipleservice->servicesid=$maxcountforservice;
        $multipleservice->leadid=$leadid;
        $multipleservice->save();





            }
        }



        $verticalcoordination = new verticalcoordination;
        $verticalcoordination->leadid=$leadid;
        $verticalcoordination->save();

    }
}
    else
    {

        $lead = new lead;
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;
        $lead->createdby=$request->loginname;
        $lead->Referenceid=$ref;
        $lead->empid=$empid;
        $lead->save();

        $leadid=DB::table('leads')->max('id');

        $address = new address;

        //fetch and store all the address details
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;


        $same1=$request->presentcontact;

        if($same1=='same')
        {
            $address->PAddress1=$request->Address1;
            $address->PAddress2=$request->Address2;
            $address->PCity=$request->City;
            $address->PDistrict=$request->District;
            $address->PState=$request->State;
            $address->PPinCode=$request->PinCode;
        }

        else
        {
            $address->PAddress1=$request->PAddress1;
            $address->PAddress2=$request->PAddress2;
            $address->PCity=$request->PCity;
            $address->PDistrict=$request->PDistrict;
            $address->PState=$request->PState;
            $address->PPinCode=$request->PPinCode;
        }

        $same=$request->emergencycontact;
        if($same=='same')
        {
            $address->EAddress1=$request->Address1;
            $address->EAddress2=$request->Address2;
            $address->ECity=$request->City;
            $address->EDistrict=$request->District;
            $address->EState=$request->State;
            $address->EPinCode=$request->PinCode;
        }

        else
        {
            $address->EAddress1=$request->EAddress1;
            $address->EAddress2=$request->EAddress2;
            $address->ECity=$request->ECity;
            $address->EDistrict=$request->EDistrict;
            $address->EState=$request->EState;
            $address->EPinCode=$request->EPinCode;
        }


        $address->leadid=$leadid;
        $address->empid=$empid;
        $address->save();
        $addressid=DB::table('addresses')->max('id');


        //enter the personnel details under the SERVICE REQUIRED form
        $personnel = new personneldetail;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
        $personnel->Addressid=$addressid;
        $personnel->Leadid=$leadid;

        $personnel->save();



        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
        $langid=json_decode($l);


        //Enter the Service Details and store them
        $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assignedto;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
         if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$request->preferedlanguage;
        $service->Remarks=$request->remarks;
        $service->langId=$langid;
        $service->LeadId=$leadid;

        $service->save();



        //fetch value to count how many service has been requested
$countofservice=$request->countofservice;

// dd($countofservice);

if($countofservice > 1)
        {

            for($m=2;$m<=$countofservice;$m++)
            {

               
                $ServiceType1="servicetype".$m;
                $GeneralCondition1="GeneralCondition".$m;
                $LeadType1="leadtype".$m;
                $Branch1="branch".$m;
                $RequestDateTime1="requesteddate".$m;
                $AssignedTo1="assignedto".$m;
                $QuotedPrice1="quotedprice".$m;
                $ExpectedPrice1="expectedprice".$m;
                $ServiceStatus1="servicestatus".$m;
                $PreferedGender1="preferedgender".$m;
                $PreferedLanguage1="$Preferredlanguages".$m;
                $Remarks1="remarks".$m;
                $langId1="preferedlanguage".$m;
                


                $ServiceType=$request->$ServiceType1;
                $GeneralCondition=$request->$GeneralCondition1;
                $LeadType=$request->$LeadType1;
                $Branch=$request->$Branch1;
                $RequestDateTime=$request->$RequestDateTime1;
                $AssignedTo=$request->$AssignedTo1;
                $QuotedPrice=$request->$QuotedPrice1;
                $ExpectedPrice=$request->$ExpectedPrice1;
               // $ServiceStatus=$request->$ServiceStatus1;
                $PreferedGender=$request->$PreferedGender1;
                $PreferedLanguage=$PreferedLanguage1;
                $Remarks=$request->$Remarks1;
                $langId=$request->$langId1;
                



                  $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$ServiceType;
        $service->GeneralCondition=$GeneralCondition;
        $service->LeadType=$LeadType;
        $service->Branch=$Branch;
        $service->RequestDateTime=$RequestDateTime;
        $service->AssignedTo=$AssignedTo;
        $service->QuotedPrice=$QuotedPrice;
        $service->ExpectedPrice=$ExpectedPrice;

        if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }

        $service->PreferedGender=$PreferedGender;
        $service->PreferedLanguage=$PreferedLanguage;
        $service->Remarks=$Remarks;
        $service->langId=$langId;
        $service->LeadId=$leadid;

        $service->save();

        $maxcountforservice=DB::table('services')->max('id');

        $multipleservice=new multipleservice;
        $multipleservice->servicesid=$maxcountforservice;
        $multipleservice->leadid=$leadid;
        $multipleservice->save();





            }
        }





        $verticalcoordination = new verticalcoordination;
        $verticalcoordination->leadid=$leadid;
        $verticalcoordination->save();
   }
}

else
{

        //fetch all the corresponding fields for "lead" table
         $lead = new lead;
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;
        $lead->createdby=$request->loginname;
        $lead->Referenceid=$ref;
        $lead->empid=$empid;
        $lead->save();

        $leadid=DB::table('leads')->max('id');

        $address = new address;

        //fetch and store all the address details
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;


        $same1=$request->presentcontact;

        if($same1=='same')
        {
            $address->PAddress1=$request->Address1;
            $address->PAddress2=$request->Address2;
            $address->PCity=$request->City;
            $address->PDistrict=$request->District;
            $address->PState=$request->State;
            $address->PPinCode=$request->PinCode;
        }

        else
        {
            $address->PAddress1=$request->PAddress1;
            $address->PAddress2=$request->PAddress2;
            $address->PCity=$request->PCity;
            $address->PDistrict=$request->PDistrict;
            $address->PState=$request->PState;
            $address->PPinCode=$request->PPinCode;
        }

        $same=$request->emergencycontact;
        if($same=='same')
        {
            $address->EAddress1=$request->Address1;
            $address->EAddress2=$request->Address2;
            $address->ECity=$request->City;
            $address->EDistrict=$request->District;
            $address->EState=$request->State;
            $address->EPinCode=$request->PinCode;
        }

        else
        {
            $address->EAddress1=$request->EAddress1;
            $address->EAddress2=$request->EAddress2;
            $address->ECity=$request->ECity;
            $address->EDistrict=$request->EDistrict;
            $address->EState=$request->EState;
            $address->EPinCode=$request->EPinCode;
        }


        $address->leadid=$leadid;
        $address->empid=$empid;
        $address->save();
        $addressid=DB::table('addresses')->max('id');


        //enter the personnel details under the SERVICE REQUIRED form
        $personnel = new personneldetail;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
        $personnel->Addressid=$addressid;
        $personnel->Leadid=$leadid;

        $personnel->save();



        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
        $langid=json_decode($l);


        //Enter the Service Details and store them
        $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assignedto;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
         if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$request->preferedlanguage;
        $service->Remarks=$request->remarks;
        $service->langId=$langid;
        $service->LeadId=$leadid;

        $service->save();


        //fetch value to count how many service has been requested
$countofservice=$request->countofservice;

// dd($countofservice);

if($countofservice > 1)
        {

            for($m=2;$m<=$countofservice;$m++)
            {

               
                $ServiceType1="servicetype".$m;
                $GeneralCondition1="GeneralCondition".$m;
                $LeadType1="leadtype".$m;
                $Branch1="branch".$m;
                $RequestDateTime1="requesteddate".$m;
                $AssignedTo1="assignedto".$m;
                $QuotedPrice1="quotedprice".$m;
                $ExpectedPrice1="expectedprice".$m;
                $ServiceStatus1="servicestatus".$m;
                $PreferedGender1="preferedgender".$m;
                $PreferedLanguage1="$Preferredlanguages".$m;
                $Remarks1="remarks".$m;
                $langId1="preferedlanguage".$m;
                


                $ServiceType=$request->$ServiceType1;
                $GeneralCondition=$request->$GeneralCondition1;
                $LeadType=$request->$LeadType1;
                $Branch=$request->$Branch1;
                $RequestDateTime=$request->$RequestDateTime1;
                $AssignedTo=$request->$AssignedTo1;
                $QuotedPrice=$request->$QuotedPrice1;
                $ExpectedPrice=$request->$ExpectedPrice1;
                //$ServiceStatus=$request->$ServiceStatus1;
                $PreferedGender=$request->$PreferedGender1;
                $PreferedLanguage=$PreferedLanguage1;
                $Remarks=$request->$Remarks1;
                $langId=$request->$langId1;
                



                  $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$ServiceType;
        $service->GeneralCondition=$GeneralCondition;
        $service->LeadType=$LeadType;
        $service->Branch=$Branch;
        $service->RequestDateTime=$RequestDateTime;
        $service->AssignedTo=$AssignedTo;
        $service->QuotedPrice=$QuotedPrice;
        $service->ExpectedPrice=$ExpectedPrice;

        if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }

        $service->PreferedGender=$PreferedGender;
        $service->PreferedLanguage=$PreferedLanguage;
        $service->Remarks=$Remarks;
        $service->langId=$langId;
        $service->LeadId=$leadid;

        $service->save();

        $maxcountforservice=DB::table('services')->max('id');

        $multipleservice=new multipleservice;
        $multipleservice->servicesid=$maxcountforservice;
        $multipleservice->leadid=$leadid;
        $multipleservice->save();





            }
        }





        $verticalcoordination = new verticalcoordination;
        $verticalcoordination->leadid=$leadid;
        $verticalcoordination->save();


}
}
else
{



     //fetch all the corresponding fields for "lead" table
         $lead = new lead;
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;
        $lead->createdby=$request->loginname;
        $lead->Referenceid=$ref;
        $lead->empid=$empid;
        $lead->save();

        $leadid=DB::table('leads')->max('id');

        $address = new address;

        //fetch and store all the address details
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;


        $same1=$request->presentcontact;

        if($same1=='same')
        {
            $address->PAddress1=$request->Address1;
            $address->PAddress2=$request->Address2;
            $address->PCity=$request->City;
            $address->PDistrict=$request->District;
            $address->PState=$request->State;
            $address->PPinCode=$request->PinCode;
        }

        else
        {
            $address->PAddress1=$request->PAddress1;
            $address->PAddress2=$request->PAddress2;
            $address->PCity=$request->PCity;
            $address->PDistrict=$request->PDistrict;
            $address->PState=$request->PState;
            $address->PPinCode=$request->PPinCode;
        }

        $same=$request->emergencycontact;
        if($same=='same')
        {
            $address->EAddress1=$request->Address1;
            $address->EAddress2=$request->Address2;
            $address->ECity=$request->City;
            $address->EDistrict=$request->District;
            $address->EState=$request->State;
            $address->EPinCode=$request->PinCode;
        }

        else
        {
            $address->EAddress1=$request->EAddress1;
            $address->EAddress2=$request->EAddress2;
            $address->ECity=$request->ECity;
            $address->EDistrict=$request->EDistrict;
            $address->EState=$request->EState;
            $address->EPinCode=$request->EPinCode;
        }


        $address->leadid=$leadid;
        $address->empid=$empid;
        $address->save();
        $addressid=DB::table('addresses')->max('id');


        //enter the personnel details under the SERVICE REQUIRED form
        $personnel = new personneldetail;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;
        $personnel->Addressid=$addressid;
        $personnel->Leadid=$leadid;

        $personnel->save();



        $l=DB::table('languages')->where('Languages',$request->preferedlanguage)->value('id');
        $langid=json_decode($l);


        //Enter the Service Details and store them
        $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assignedto;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
         if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$request->preferedlanguage;
        $service->Remarks=$request->remarks;
        $service->langId=$langid;
        $service->LeadId=$leadid;

        $service->save();


        //fetch value to count how many service has been requested
$countofservice=$request->countofservice;

// dd($countofservice);

if($countofservice > 1)
        {

            for($m=2;$m<=$countofservice;$m++)
            {

               
                $ServiceType1="servicetype".$m;
                $GeneralCondition1="GeneralCondition".$m;
                $LeadType1="leadtype".$m;
                $Branch1="branch".$m;
                $RequestDateTime1="requesteddate".$m;
                $AssignedTo1="assignedto".$m;
                $QuotedPrice1="quotedprice".$m;
                $ExpectedPrice1="expectedprice".$m;
                // $ServiceStatus1="servicestatus".$m;
                $PreferedGender1="preferedgender".$m;
                $PreferedLanguage1="$Preferredlanguages".$m;
                $Remarks1="remarks".$m;
                $langId1="preferedlanguage".$m;
                


                $ServiceType=$request->$ServiceType1;

                $GeneralCondition=$request->$GeneralCondition1;
                $LeadType=$request->$LeadType1;
                $Branch=$request->$Branch1;
                $RequestDateTime=$request->$RequestDateTime1;
                $AssignedTo=$request->$AssignedTo1;
                $QuotedPrice=$request->$QuotedPrice1;
                $ExpectedPrice=$request->$ExpectedPrice1;
                // $ServiceStatus=$request->$ServiceStatus1;
                $PreferedGender=$request->$PreferedGender1;
                $PreferedLanguage=$PreferedLanguage1;
                $Remarks=$request->$Remarks1;
                $langId=$request->$langId1;
                



                  $service = new service;
        // $this->validate($request,[
        //         'Languages'=>'required',
        //     ]);

        $service->ServiceType=$ServiceType;
        $service->GeneralCondition=$GeneralCondition;
        $service->LeadType=$LeadType;
        $service->Branch=$Branch;
        $service->RequestDateTime=$RequestDateTime;
        $service->AssignedTo=$AssignedTo;
        $service->QuotedPrice=$QuotedPrice;
        $service->ExpectedPrice=$ExpectedPrice;

        if($request->droplead!=NULL)
        {
        $service->ServiceStatus="Dropped";
        }
        else
        {
            $service->ServiceStatus=$request->servicestatus;
        }

        $service->PreferedGender=$PreferedGender;
        $service->PreferedLanguage=$PreferedLanguage;
        $service->Remarks=$Remarks;
        $service->langId=$langId;
        $service->LeadId=$leadid;

        $service->save();

        $maxcountforservice=DB::table('services')->max('id');

        $multipleservice=new multipleservice;
        $multipleservice->servicesid=$maxcountforservice;
        $multipleservice->leadid=$leadid;
        $multipleservice->save();





            }
        }



        $verticalcoordination = new verticalcoordination;
        $verticalcoordination->leadid=$leadid;
        $verticalcoordination->save();
}

        $pid=$request->pid;
        if($pid!=NULL)
        {
            DB::table('provisionalleads')->where('id',$pid)->update(['active'=>'0']);
        }





        /* Code for storing the details entered in the form ends here */

        //Sending the Mail to different people starts here

        //vertical head contact number
        $vmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('MobileNumber');

        //vertical alternate contact number
        $valtmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('AlternateNumber');

        // dd($valtmob );
        //referal partner name
        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

        //referal partner contact number
        $rmob = DB::table('refers')->where('Reference',$request->reference)->value('ReferenceContact');

        //customer name
        $name=$request->clientfname;

        //vertical head name
        $user=$request->assignedto;


        //customer mobile
        $contact="$request->clientmob";

        //customer alternate mobile
        $altcontact="$request->clientalternateno";

        // dd($altcontact);

        //location selected during lead creation
        $location=$request->branch;

        //who created
        $who = $request->loginname;

        $reference=DB::table('refers')->get();
        $gender=DB::table('genders')->get();
        $gender1=DB::table('genders')->get();
        $city=DB::table('cities')->get();
        $pcity=DB::table('cities')->get();
        $ecity=DB::table('cities')->get();
        $branch=DB::table('cities')->get();
        $status=DB::table('leadstatus')->get();
        $leadtype=DB::table('leadtypes')->get();
        $condition=DB::table('ptconditions')->get();
        $language=DB::table('languages')->get();
        $relation=DB::table('relationships')->get();
        $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
        $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
        $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
        $em=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
        $em1=DB::table('employees')->where('id',$em)->get();



        // retrieve all the column and the corresponding details from the particular row
        $lead = lead::find($leadid);

        $s=DB::table('services')->where('leadid',$leadid)->value('id');
        $service=service::find($s);

        $p=DB::table('personneldetails')->where('leadid',$leadid)->value('id');
        $personneldetail=personneldetail::find($p);

        $a=DB::table('addresses')->where('leadid',$leadid)->value('id');
        $address=address::find($a);


        $p=DB::table('products')->where('Leadid',$leadid)->value('id');

        $product=product::find(1);
        if($p != NULL)
        {

            $product=product::find($p);
        }


        // if (session()->has('name'))
        // {
        //
        //   $name1=session()->get('name');
        //
        // }
        // else
        // {
        //   $name1=$_GET['name'];
        // }



        //this refers to the person who created the lead (could be Customer care, Vertical Head, Coordinator)
        $from = DB::table('admins')->where('name',$request->loginname)->value('email');

        //this refers to the Vertical Head name to whom this lead was assigned
        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');

        //retrieving the clients email id
        $to1=$request->clientemail;

        //service type retrieval done here
        $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

        $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

        $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];


        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($to!=NULL && $active_state==0)
        {
            Mail::send('mail', $data, function($message)use($data) {
                $message->to($data['to'], $data['user'] )->subject
                ('Lead ['.$data['leadid'].'] Created!!!');
                $message->from($data['from'],$data['who']);
            });
        }

        $phonenimber=$contact;
        $phonenumber1 = $altcontact;

        /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to customer -- by jatin --WORKING*/

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenimber!=NULL && $active_state==0)
        {
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";


            //for extracting the date for generating reports
            $time = time();
            $date = date('m/d/Y',$time);

            //actual sending of the message
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            // dd(curl_exec($ch));
            //expecting the report 'OK' here
            $report = substr($curl_scraped_page,0,2);

            //extracting the job no of the message sent if it is at all
            $jobno = trim(substr($curl_scraped_page,3));


            curl_close($ch);

            //url for retrieving the report
            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch1 =curl_init($report1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page1 =curl_exec($ch1);
            // $report = substr($curl_scraped_page1,0,2);
            curl_close($ch1);

            //retrieving the invalid mobile no text part out of the report
            $invalidmno = substr($curl_scraped_page1,0,11);

            //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            if($invalidmno == "Invalid Mno")
            {


                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$leadid)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$leadid)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);



                $checkbox=$request->addproduct;

                session()->put('name',$who);
                // Session::flash('warning','The Mobile number is invalid');

                Session::flash('message', 'Customer Mobile number is invalid');
                Session::flash('alert-class', 'alert-danger');

                $empname = DB::table('employees')->where('Firstname',$who)->value('Designation');
                // dd($empname)
                //if it is the vertical head who has logged in , then redirect to the vertical head update page

                // if($empname == "Vertical Head")
                // {
                //     return view('vh.leadedit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','vert','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12'));
                // }
                // else
                // {
                //     return view('admin.leadedit',compact('code','country_name','count1','country_codes','indian_cities','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                // }

$id=$leadid;

                $logged_in_user=Auth::guard('admin')->user()->name;

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
            $em=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $em1=DB::table('employees')->where('id',$em)->get();

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $service_name=DB::table('services')->where('Leadid',$id)->value('ServiceType');

            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');



            if (session()->has('name'))

            {

                $name1=session()->get('name');

            }
            else{
                $name1=Auth::guard('admin')->user()->name;
            }
            $empid=DB::table('employees')->where('FirstName',$name1)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);
            $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$id)->value('ename');


            $p=DB::table('products')->where('Leadid',$id)->value('id');

            $product=product::find(1);
            if($p != NULL)
            {

                $product=product::find($p);
            }


            if (session()->has('name'))
            {

                $name1=session()->get('name');

            }
            else
            {
                $name1=Auth::guard('admin')->user()->name;
            }

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');
            $department3=DB::table('employees')->where('FirstName',$name1)->value('Department3');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                {
                    $designation=$roles;

                    $checkdesig="coordinator";

                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));

                }
                else
                {
                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                    {
                        $designation=$roles;
                        $checkdesig="coordinator";
                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                        {
                            $designation=$roles;
                            $checkdesig="coordinator";
                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                $checkdesig="coordinator";
                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    $checkdesig="coordinator";
                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        $checkdesig="coordinator";
                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            $checkdesig="coordinator";
                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                        }
                                        else
                                        {
                                            if($roles=="Branch Head" && $user_city==$service_city)
                                            {
                                                $designation=$roles;
                                                $checkdesig="branchhead";
                                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="coordinator";
                                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="coordinator";
                                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        if($designation == "Coordinator" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation="coordinator";
                                                            $checkdesig="coordinator";
                                                            // dd($checkdesig);
                                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('/admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            else
            if($designation == "Management")
            {
                $designation="management";
                $checkdesig="management";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                $checkdesig="admin";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            {

                if($roles=="Vertical_ProductSelling" && $user_department==$service_name)
                {
                    $designation=$roles;
                    $checkdesig="vertical";
                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));

                }
                else
                {
                    if($roles=="Vertical_ProductRental" && $user_department==$service_name)
                    {
                        $designation=$roles;
                        $checkdesig="vertical";
                        return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager" && $user_department==$service_name)
                        {
                            $designation=$roles;
                            $checkdesig="vertical";
                            return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager" && $user_department==$service_name)
                            {
                                $designation=$roles;
                                $checkdesig="vertical";
                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$service_name)
                                {
                                    $designation=$roles;
                                    $checkdesig="vertical";
                                   return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$service_name)
                                    {
                                        $designation=$roles;
                                        $checkdesig="vertical";
                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$service_name)
                                        {
                                            $designation=$roles;
                                            $checkdesig="leadeditformobilenumber";
                                           return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                        }
                                        else
                                        {
                                            if($designation == "Branch Head" && $user_city==$service_city )
                                            {
                                                $designation="BranchHead";
                                                $checkdesig="branchhead";
                                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer" && $user_department==$service_name)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="vertical";
                                                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$service_name)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="vertical";
                                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        // dd($roles,$user_department,$service_name);
                                                        if($roles=="vertical" && $user_department==$service_name)
                                                        {
                                                            $designation="vertical";
                                                            $checkdesig="vertical";
                                                           return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('\admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }




            }

            $i=0;

            //sending a message again if not sent first time

            //while the report generated doesn't have the message OK in it
            while($report!="OK" || $i<=1)
            {
                //even if i<1 trigger this only in cases where report is not = OK
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$contact;
                    $time = time();
                    $date = date('d/m/Y',$time);
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);
                }
                $i++;
            }
        }

        $phonenumber1 = $altcontact;

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenumber1!=NULL && $active_state==0)
        {
            //alternate number messaging for Customer
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            $phonenumber1 = $altcontact;

            //for extracting the date for generating reports
            $time = time();
            $date = date('m/d/Y',$time);

            //actual sending of the message
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenumber1)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            $ch3 =curl_init($url);

            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page3 =curl_exec($ch3);
            // dd(curl_exec($ch));
            //expecting the report 'OK' here
            $report3 = substr($curl_scraped_page3,0,2);

            //extracting the job no of the message sent if it is at all
            $jobno3 = trim(substr($curl_scraped_page3,3));


            curl_close($ch3);

            //url for retrieving the report
            $report4 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch4 =curl_init($report4);
            curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page4 =curl_exec($ch4);
            // $report = substr($curl_scraped_page4,0,2);
            curl_close($ch4);

            //retrieving the invalid mobile no text part out of the report
            $invalidmno = substr($curl_scraped_page4,0,11);

            //if the mobile number is invalid , redirect it to cc.edit page with a flash message
            if($invalidmno == "Invalid Mno")
            {

                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$id)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);

                $checkbox=$request->addproduct;

                session()->put('name',$who);

                Session::flash('message', 'Customer Alternative number is invalid');
                Session::flash('alert-class', 'alert-danger');


                // Session::flash('warning','The Mobile number is invalid');
                // dd($empname);
                // if($empname == "Vertical Head")
                // {
                //     return view('vh.leadedit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','vert','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12'));
                // }
                // else
                // {
                //     return view('cc.edit',compact('code','country_name','count1','country_codes','indian_cities','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','checkbox'));
                // }




$id=$leadid;

                $logged_in_user=Auth::guard('admin')->user()->name;

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
            $em=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $em1=DB::table('employees')->where('id',$em)->get();

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $service_name=DB::table('services')->where('Leadid',$id)->value('ServiceType');

            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');



            if (session()->has('name'))

            {

                $name1=session()->get('name');

            }
            else{
                $name1=Auth::guard('admin')->user()->name;
            }
            $empid=DB::table('employees')->where('FirstName',$name1)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);
            $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$id)->value('ename');


            $p=DB::table('products')->where('Leadid',$id)->value('id');

            $product=product::find(1);
            if($p != NULL)
            {

                $product=product::find($p);
            }


            if (session()->has('name'))
            {

                $name1=session()->get('name');

            }
            else
            {
                $name1=Auth::guard('admin')->user()->name;
            }

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');
            $department3=DB::table('employees')->where('FirstName',$name1)->value('Department3');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                {
                    $designation=$roles;

                    $checkdesig="coordinator";

                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));

                }
                else
                {
                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                    {
                        $designation=$roles;
                        $checkdesig="coordinator";
                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                        {
                            $designation=$roles;
                            $checkdesig="coordinator";
                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                $checkdesig="coordinator";
                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    $checkdesig="coordinator";
                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        $checkdesig="coordinator";
                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            $checkdesig="coordinator";
                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                        }
                                        else
                                        {
                                            if($roles=="Branch Head" && $user_city==$service_city)
                                            {
                                                $designation=$roles;
                                                $checkdesig="branchhead";
                                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="coordinator";
                                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="coordinator";
                                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        if($designation == "Coordinator" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation="coordinator";
                                                            $checkdesig="coordinator";
                                                            // dd($checkdesig);
                                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('/admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            else
            if($designation == "Management")
            {
                $designation="management";
                $checkdesig="management";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                $checkdesig="admin";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            {

                if($roles=="Vertical_ProductSelling" && $user_department==$service_name)
                {
                    $designation=$roles;
                    $checkdesig="vertical";
                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));

                }
                else
                {
                    if($roles=="Vertical_ProductRental" && $user_department==$service_name)
                    {
                        $designation=$roles;
                        $checkdesig="vertical";
                        return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager" && $user_department==$service_name)
                        {
                            $designation=$roles;
                            $checkdesig="vertical";
                            return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager" && $user_department==$service_name)
                            {
                                $designation=$roles;
                                $checkdesig="vertical";
                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$service_name)
                                {
                                    $designation=$roles;
                                    $checkdesig="vertical";
                                   return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$service_name)
                                    {
                                        $designation=$roles;
                                        $checkdesig="vertical";
                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$service_name)
                                        {
                                            $designation=$roles;
                                            $checkdesig="leadeditformobilenumber";
                                           return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                        }
                                        else
                                        {
                                            if($designation == "Branch Head" && $user_city==$service_city )
                                            {
                                                $designation="BranchHead";
                                                $checkdesig="branchhead";
                                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer" && $user_department==$service_name)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="vertical";
                                                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$service_name)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="vertical";
                                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        // dd($roles,$user_department,$service_name);
                                                        if($roles=="vertical" && $user_department==$service_name)
                                                        {
                                                            $designation="vertical";
                                                            $checkdesig="vertical";
                                                           return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('\admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            }

            $i=0;

            //sending a message again if not sent first time

            //while the report generated doesn't have the message OK in it
            while($report!="OK" || $i<=1)
            {
                //even if i<1 trigger this only in cases where report is not = OK
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$altcontact;
                    $time = time();
                    $date = date('d/m/Y',$time);
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch5 =curl_init($url);
                    curl_setopt($ch5, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page5 =curl_exec($ch5);
                    $report5 = substr($curl_scraped_page5,0,2);
                    curl_close($ch5);
                }
                $i++;
            }
        }

        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to customer -- by jatin*/

        //service type retrieved here
        $service_type = DB::table('services')->where('Leadid',$leadid)->value('ServiceType');

        /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to vertical head -- by jatin --WORKING*/

        $phonenimber=$vmob;
        $active_state = DB::table('sms_email_filter')->value('active_state');


        if($phonenimber!=NULL && $active_state==0)
        {

            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Contact: ".$contact."
            Request Type: ".$service_type."
            Location:".$location;
            $phonenimber=$vmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$vmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }

        //message going to alternate number of vh
        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($valtmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Contact: ".$contact."
            Request Type: ".$service_type."
            Location:".$location;
            $phonenimber=$valtmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Request Type: ".$service_type."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$valtmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }



        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to vertical head -- by jatin --WORKING*/

        /* SMS for Lead Creation by  Vertical Leads starts here..Message going to referal partner -- by jatin --WORKING*/

        $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.".Our Care Team will get in touch with the ".$name.", shortly.
        - Health Heal";
        $phonenimber=$rmob;

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenimber!=NULL && $active_state==0)
        {
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.".Our Care Team will get in touch with the ".$name.", shortly.
                    - Health Heal";
                    $phonenimber=$rmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }

        }
        /* SMS for Lead Creation by  Vertical Leads ends here..Message going to referal partner -- by jatin*/

        $active_state = DB::table('sms_email_filter')->value('active_state');

        //if client doesn't give email id
        if($to1!=NULL && $active_state==0)
        {
            Mail::send('mail1', $data1, function($message)use($data1) {
                $message->to($data1['to'], $data1['name'] )->subject
                ('[Happy to Care] Health Heal');
                $message->from('care@healthheal.in','Healthheal');

                // Sending the Mail to different people ends here


            });


        }




        /*
        Code for creation log starts here -- by jatin
        */
        //We are trying to retrieve the session id for the person who is logged in

        //retrieving the session id of the person who is logged in
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        //retrieving the username of the presently logged in user
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        // retrieving the employee id of the presently logged in user
        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');

        //retrieving the log id of the person who logged in just now
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Lead Created";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->lead_id = $leadid;
        $activity->save();

        /*
        Code for creation log ends here -- by jatin
        */


        /* checking if products has to include or not */



        $checkbox=$request->addproduct;

        //if the person selects the checkbox, then take the user to the "product" creation page after "submit" button is clicked
        if($checkbox == "add")
        {

            $products=DB::table('productdetails')->get();
            $name1=DB::table('leads')->where('id',$leadid)->value('createdby');

            session()->put('name',$name1);
            return view("product.addproduct",compact('leadid','products'));

        }




        //retrieve the role name in all the four lines
        $name=DB::table('leads')->where('id',$leadid)->select('createdby','id')->get();
        $name=json_decode($name);
        // dd($name);
        $name1=$name[0]->createdby;
        $id=$name[0]->id;

        $n=DB::table('admins')->where('name',$name1)->select('id')->get();
        $n=json_decode($n);
        $n1=$n[0]->id;

        $a=DB::table('role_admins')->where('admin_id',$n1)->select('role_id')->get();
        $a=json_decode($a);
        $a1=$a[0]->role_id;

        $b=DB::table('roles')->where('id',$a1)->select('name')->get();
        $b=json_decode($b);
        $b1=$b[0]->name;

        //checking the roles
        if($b1=="vertical")
        {
            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();

            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);

            $p=DB::table('products')->where('Leadid',$id)->value('id');
            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);


            $product=product::find(1);
            if($p != NULL)
            {
                $product=product::find($p);
            }

            //  $leads=DB::table('leads')
            // ->select('services.*','personneldetails.*','addresses.*','leads.*')
            // ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
            // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
            // ->join('services', 'leads.id', '=', 'services.LeadId')
            // ->orderBy('leads.id', 'DESC')->where('leads.id',$id)
            // ->get();


            return view('cc.edit',compact('code','country_name','count1','country_codes','indian_cities','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product'));
        }


        //   return redirect('/vh?name="$name1"');
        // }

        //if the role is not Vertical Head
        else{

            $c=DB::table('employees')->where('FirstName',$request->loginname)->value('Designation');
            if($c == "Coordinator")
            {
                session()->put('name',$name1);
                return redirect('/vh');
            }
            else
            if($c == "Admin")
            {
                session()->put('name',$name1);
                return redirect('/cc');
            }
            else
            {
                if($c == "Management")
                {
                    session()->put('name',$name1);
                    return redirect('/cc');
                }
                else
                {
                    if($c == "Marketing")
                    {
                        session()->put('name',$name1);
                        return redirect('/cc');
                    }
                    else
                    {
                        if($c=="Branch Head")
                        {
                            session()->put('name',$name1);
                            return redirect('/vh');
                        }
                        else
                        {
                            session()->put('name',$name1);
                            return redirect('/cc');
                        }
                    }
                }
            }
        }


}

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    //this is the scenario in which the invalid mobile number occurs and we wish to be redirected to the EDIT form
    public function update(Request $request, $id)
    {


        $leadid = DB::table('leads')->where('id',$id)->value('id');

        $checkbox=$request->addproduct;

        //echo $lead;

        $name1=$request->loginname;


        /*
        Check if any value has been given to products or not
        check the lead id for that ..
        if it gives null, enter the data first time
        if for SKUID or Product name nothing is entered which means
        most fields have null so don't enter anything in db and don't create an activity -- by jatin
        */

        $p2=DB::table('products')->where('Leadid',$leadid)->select('id')->get();
        $p2=json_decode($p2);
        //echo $p2;
        //echo $lead;


        if($p2 == NULL)
        {


            $products = new product;
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;
            $products->Leadid=$leadid;
            $pid=NULL;

            if($products->SKUid !=NULL || $products->ProductName !=NULL)
            {
                $products->save();
                $pid=DB::table('products')->max('id');
            }
            else
            {
                $prod = " ";
                $prof = " ";
            }

        }

        /*
        if some product values have been already entered , then perform the updation thing -- by jatin
        */

        else
        {

            $products=DB::table('products')->where('Leadid',$leadid)->select('id')->get();
            $products=json_decode($products);
            $products=$products[0]->id;
            $pid=$products;
            // echo $pid;
            $products=product::find($products);
            $products->SKUid=$request->SKUid;
            $products->ProductName=$request->ProductName;
            $products->DemoRequired=$request->DemoRequired;
            $products->AvailabilityStatus=$request->AvailabilityStatus;
            $products->AvailabilityAddress=$request->AvailabilityAddress;
            $products->SellingPrice=$request->SellingPrice;
            $products->RentalPrice=$request->RentalPrice;

            /*
            Code for capturing the products part of Log Edit starts here -- by jatin
            */

            //this will retrieve all the fields and values that are updated in the form of an array
            $products_c = $products->getDirty();

            //Here we are trying to capture the field and it's corresponding value by putting them in a separate arrray
            foreach($products_c as $key=>$value)
            {
                $proc[] = $key;

            }

            foreach($products_c as $key=>$value)
            {
                $prov[] = $value;
            }

            //this is for converting array values into a string with new line

            //  if no data is there then put spaces or null in the variables else convert the array into string by using commas
            if(empty($proc))
            {
                $prod = " ";
                $prof = " ";
                $prod = trim($prod);
                $prof = trim($prof);
            }
            else
            {
                $prod = implode("\r\n", $proc);
                $prof = implode("\r\n", $prov);
            }

            // dd($prod,$prof);
            /*
            Code for capturing the products part of Log Edit ends here -- by jatin
            */

            $products->save();
        }

        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

        if($request->source != $refer)
        {

            $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');
            $source=$refer;
        }
        else
        {
            $source=$request->source;
        }

        $ref=DB::table('refers')->where('Reference',$request->reference)->value('id');

        $empid=DB::table('employees')->where('FirstName',$request->assignedto)->value('id');

        //these values will be used later -- by jatin
        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');

        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');

        $lead_id = DB::table('assessments')->where('Leadid',$leadid)->value('id');
        //these values will be used later -- by jatin

        $data = file_get_contents("countrycode.json");

        $country_codes = json_decode($data,true);

        $count = count($country_codes);

        $country_name=NULL;
        // for($i=0;$i<$count;$i++)
        // {
        //     if($code == $country_codes[$i]['dial_code'])
        //     {
        //         $country_name = $country_codes[$i]['name'];
        //     }
        // }

        $code = DB::table('leads')->where('id',$leadid)->value('Country_Code');
        $country_name = DB::table('leads')->where('id',$leadid)->value('Country_Name');

        $data1 = file_get_contents("indian_cities.json");

        $indian_cities = json_decode($data1,true);

        $count1 = count($indian_cities);
        $lead=DB::table('leads')->where('id',$leadid)->value('id');

        $lead=lead::find($lead);
        $lead->fName=$request->clientfname;
        $lead->mName=$request->clientmname;
        $lead->lName=$request->clientlname;
        $lead->EmailId=$request->clientemail;
        $lead->Source=$source;
        $lead->Country_Name=$country_name;
        $lead->Country_Code=$code;
        $lead->MobileNumber=$request->clientmob;
        $lead->Alternatenumber=$request->clientalternateno;
        $lead->EmergencyContact =$request->EmergencyContact;
        $lead->AssesmentReq=$request->assesmentreq;

        $lead->Referenceid=$ref;
        $lead->empid=$empid;

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $lead_c = $lead->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($lead_c as $key=>$value)
        {
            $leadc[] = $key;

        }

        foreach($lead_c as $key=>$value)
        {
            $leadv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($leadc))
        {
            $leadd = " ";
            $leadd = trim($leadd);

            $leadf = " ";
            $leadf = trim($leadf);
        }
        else
        {
            $leadd = implode("\r\n", $leadc);
            $leadf = implode("\r\n", $leadv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $lead->save();


        $address=DB::table('addresses')->where('leadid',$leadid)->value('id');

        $address=address::find($address);
        $address->Address1=$request->Address1;
        $address->Address2=$request->Address2;
        $address->City=$request->City;
        $address->District=$request->District;
        $address->State=$request->State;
        $address->PinCode=$request->PinCode;
        $address->PAddress1=$request->PAddress1;
        $address->PAddress2=$request->PAddress2;
        $address->PCity=$request->PCity;
        $address->PDistrict=$request->PDistrict;
        $address->PState=$request->PState;
        $address->PPinCode=$request->PPinCode;
        $address->EAddress1=$request->EAddress1;
        $address->EAddress2=$request->EAddress2;
        $address->ECity=$request->ECity;
        $address->EDistrict=$request->EDistrict;
        $address->EState=$request->EState;
        $address->EPinCode=$request->EPinCode;

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $address_c = $address->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($address_c as $key=>$value)
        {
            $addc[] = $key;

        }

        foreach($address_c as $key=>$value)
        {
            $addv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($addc))
        {
            $addd = " ";
            $addd = trim($addd);

            $addf = " ";
            $addf = trim($addf);
        }
        else
        {
            $addd = implode("\r\n", $addc);
            $addf = implode("\r\n", $addv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $address->save();

        // dd($addd, $addf);

        $personnel=DB::table('personneldetails')->where('leadid',$leadid)->value('id');

        $personnel=personneldetail::find($personnel);
        $personnel->PtfName=$request->patientfname;
        $personnel->PtmName=$request->patientmname;
        $personnel->PtlName=$request->patientlname;
        $personnel->age=$request->age;
        $personnel->Gender=$request->gender;
        $personnel->Relationship=$request->relationship;
        $personnel->Occupation=$request->Occupation;
        $personnel->AadharNum=$request->aadhar;
        $personnel->AlternateUHIDType=$request->AlternateUHIDType;
        $personnel->AlternateUHIDNumber=$request->AlternateUHIDNumber;
        $personnel->PTAwareofDisease=$request->PTAwareofDisease;

        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $personnel_c = $personnel->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($personnel_c as $key=>$value)
        {
            $perc[] = $key;

        }

        foreach($personnel_c as $key=>$value)
        {
            $perv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($perc))
        {
            $perd = " ";
            $perd = trim($perd);

            $perf = " ";
            $perf = trim($perf);
        }
        else
        {
            $perd = implode("\r\n", $perc);
            $perf = implode("\r\n", $perv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */
        $personnel->save();



        $service=DB::table('services')->where('leadid',$leadid)->value('id');

        $service=service::find($service);

        $service->ServiceType=$request->servicetype;
        $service->GeneralCondition=$request->GeneralCondition;
        $service->LeadType=$request->leadtype;
        $service->Branch=$request->branch;
        $service->RequestDateTime=$request->requesteddate;
        $service->AssignedTo=$request->assigntonew;
        $service->QuotedPrice=$request->quotedprice;
        $service->ExpectedPrice=$request->expectedprice;
        $service->PreferedGender=$request->preferedgender;
        $service->PreferedLanguage=$request->preferedlanguage;
        $service->Remarks=$request->remarks;
        // dd($request->assignedto);
        /*
        Code for capturing assessment part of Log Edit starts here -- by jatin
        Refer to comments for Products to understand what is happening in each field
        */

        //this will retrieve all the fields and values that are updated in the form of an array
        $service_c = $service->getDirty();

        // $matc[] = " ";
        // $matv[] = " ";

        //the array fetched needs to be fetched on the basis of key and their corresponding value
        foreach($service_c as $key=>$value)
        {
            $serc[] = $key;

        }

        foreach($service_c as $key=>$value)
        {
            $serv[] = $value;
        }

        //this is for converting array values into a string with new line
        if(empty($serc))
        {
            $serd = " ";
            $serd = trim($serd);

            $serf = " ";
            $serf = trim($serf);
        }
        else
        {
            $serd = implode("\r\n", $serc);
            $serf = implode("\r\n", $serv);
        }

        /*
        Code for capturing assessment part of Log Edit ends here -- by jatin
        */

        $service->save();


        /*
        Code for final updation of activity with field and values starts here -- by jatin
        */

        if($leadd==" " || $leadd==' ' || $leadd=="" || $leadd=='')
        {
            $leadd = trim($leadd);
            $leadf = trim($leadf);
        }
        else
        {
            $leadd = $leadd."\r\n";
            $leadf = $leadf."\r\n";
        }
        if($addd==" " || $addd==' ' || $addd=="" || $addd=='')
        {
            $addd = trim($addd);
            $addf = trim($addf);
        }
        else
        {
            $addd = $addd."\r\n";
            $addf = $addf."\r\n";
        }
        if($perd==" " || $perd==' ' || $perd=="" || $perd=='')
        {
            $perd = trim($perd);
            $perf = trim($perf);
        }
        else
        {
            $perd = $perd."\r\n";
            $perf = $perf."\r\n";
        }
        if($serd==" " || $serd==' ' || $serd=="" || $serd=='')
        {
            $serd = trim($serd);
            $serf = trim($serf);
        }
        else
        {
            $serd = $serd."\r\n";
            $serf = $serf."\r\n";
        }
        if($prod==" " || $prod==' ' || $prod=="" || $prod=='')
        {
            $prod = trim($prod);
            $prof = trim($prof);
        }
        else
        {
            $prod = $prod."\r\n";
            $prof = $prof."\r\n";
        }

        //all the values which were changed in the Mathrutvam form are being passed as variables with proper indentation
        $fields_updated = $leadd.''.$addd.''.$perd.''.$serd.''.$prod;
        $values_updated = $leadf.''.$addf.''.$perf.''.$serf.''.$prof;


        //If no field is updated and all are empty then don't log the activity
        if((trim($fields_updated)!==""))
        {
            $activity = new Activity;
            $activity->emp_id = $emp_id;
            $activity->activity = "Fields Updated";
            $activity->activity_time = new \DateTime();
            $activity->log_id = $log_id;
            $activity->lead_id = $leadid;
            $activity->field_updated = $fields_updated;
            $activity->value_updated = $values_updated;

            $activity->save();
        }

        session()->put('name',$name1);


        $coordinatorassigned=$request->assigned;

        $e1=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
        $coor_name=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('ename');
        $service_check=DB::table('services')->where('leadid',$leadid)->value('AssignedTo');


        $vertical_assign=$request->assigntonew;
        // if lead is transfer to another vertical head remove coordinator from vertical coordination table if assigned
        // dd($service_check);
        $coordinatorassigned=$request->assigned;

        if($service_check!=$vertical_assign)
        {

            $null_value=NULL;

            // checking if the coordinator value is passed from view but the vertical head is changed then make the request null
            if($coor_name==$coordinatorassigned)
            {
                $coordinatorassigned=NULL;
                // dd($request->assigned);
            }
            $coordinatorassigned=NULL;
            $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('id');

            $verticalcoordination=verticalcoordination::find($verticalcoordination);

            $verticalcoordination->empid=$null_value;
            $verticalcoordination->ename=$null_value;
            $verticalcoordination->save();

        }


        $verticalcoordination123=DB::table('verticalcoordinations')->where('leadid',$leadid)->get();


        $e=DB::table('employees')->where('FirstName',$coordinatorassigned)->value('id');
        // dd($e);
        //$e=json_decode($e);
        //      $e1=$e[0]->id;
        $e1=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');

        $verticalcoordination=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('id');



        if($verticalcoordination!=NULL)
        {
            $verticalcoordination=verticalcoordination::find($verticalcoordination);

            if($e!=$e1)
            {

                /*
                Code for capturing assigned to status starts here -- by jatin
                */
                if($e!=NULL)
                {
                    $verticalcoordination->empid=$e;
                    $verticalcoordination->ename=$coordinatorassigned;
                    $asid = 'AssignedTo:';
                    $asif = $coordinatorassigned.', ';
                    $verticalcoordination->save();
                }
                else
                {
                    $asid = ' ';
                    $asif = ' ';
                }
            }
            else
            {

                $asid = ' ';
                $asif = ' ';
            }
            // dd($request->assigned);

        }
        else
        {
            $verticalcoordination= new verticalcoordination;
            $verticalcoordination->empid=$e;
            $verticalcoordination->ename=$coordinatorassigned;
            $verticalcoordination->leadid=$leadid;
            $verticalcoordination->save();

            $asid = ' ';
            $asif = ' ';
        }


        if($e != NULL)
        {
            DB::table('services')->where('leadid',$leadid)->update(['ServiceStatus' =>'In Progress' ]);
        }



        /* Code for storing the details entered in the form ends here */

        //Sending the Mail to different people starts here

        //vertical head contact number
        $vmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('MobileNumber');

        //referal partner name
        $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

        //referal partner contact number
        $rmob = DB::table('refers')->where('Reference',$request->reference)->value('ReferenceContact');

        //customer name
        $name=$request->clientfname;

        //vertical head name
        $user=$request->assignedto;
        // dd($user);

        //customer mobile
        $contact="$request->clientmob";

        //location selected during lead creation
        $location=$request->branch;

        //customer alternate mobile
        $altcontact="$request->clientalternateno";

        //vertical alternate contact number
        $valtmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('AlternateNumber');

        //who created
        $who = $request->loginname;

        $reference=DB::table('refers')->get();
        $gender=DB::table('genders')->get();
        $gender1=DB::table('genders')->get();
        $city=DB::table('cities')->get();
        $pcity=DB::table('cities')->get();
        $ecity=DB::table('cities')->get();
        $branch=DB::table('cities')->get();
        $status=DB::table('leadstatus')->get();
        $leadtype=DB::table('leadtypes')->get();
        $condition=DB::table('ptconditions')->get();
        $language=DB::table('languages')->get();
        $relation=DB::table('relationships')->get();
        $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
        $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
        // $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
        $em=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
        $em1=DB::table('employees')->where('id',$em)->get();
        $eid = DB::table('employees')->where('FirstName',$who)->value('id');

        $emp12 = DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

        $v=DB::table('verticalcoordinations')->where('leadid',$leadid)->value('empid');
        $nv=DB::table('employees')->where('id',$v)->value('id');
        $vert=employee::find($nv);


        if (session()->has('name'))

        {

            $who=session()->get('name');

        }


        $lead = lead::find($leadid);

        $s=DB::table('services')->where('leadid',$leadid)->value('id');
        $service=service::find($s);

        $p=DB::table('personneldetails')->where('leadid',$leadid)->value('id');
        $personneldetail=personneldetail::find($p);

        $a=DB::table('addresses')->where('leadid',$leadid)->value('id');
        $address=address::find($a);


        $p=DB::table('products')->where('Leadid',$leadid)->value('id');

        $product=product::find(1);
        if($p != NULL)
        {

            $product=product::find($p);
        }


        if (session()->has('name'))
        {

            $name1=session()->get('name');

        }
        else
        {
            $name1=$_GET['name'];
        }



        $from = DB::table('admins')->where('name',$request->loginname)->value('email');
        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
        $to1=$request->clientemail;


        //service type retrieval done here
        $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

        $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who];

        $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($to!=NULL && $active_state==0 )
        {
            Mail::send('mail', $data, function($message)use($data) {
                $message->to($data['to'], $data['user'] )->subject
                ('Lead ['.$data['leadid'].'] Created!!!');
                $message->from($data['from'],$data['who']);
            });
        }

        $phonenimber=$contact;
        $phonenumber1 = $altcontact;

        // /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to customer -- by jatin --WORKING*/
        //

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenimber!=NULL && $active_state==0)
        {
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            $phonenimber=$contact;

            //grabbing the date for report generation
            $time = time();
            $date = date('m/d/Y',$time);

            //url for sending sms
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);

            //grabbing the word "OK" out of the report generated if at all it is there
            $report = substr($curl_scraped_page,0,2);

            //grabbing the job no of the message sent
            $jobno = trim(substr($curl_scraped_page,3));
            curl_close($ch);

            //report generation url
            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch1 =curl_init($report1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page1 =curl_exec($ch1);
            curl_close($ch1);

            //grabbing the term "invalid mobile no" out of the report generated if the mobile is invalied
            $invalidmno = substr($curl_scraped_page1,0,11);

            //checking for invalid mobile no
            if($invalidmno == "Invalid Mno")
            {

                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$id)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);


                session()->put('name',$who);
                 Session::flash('message', 'Customer Mobile number is invalid');
                Session::flash('alert-class', 'alert-danger');

                $empname = DB::table('employees')->where('Firstname',$who)->value('Designation');
                // dd($empname);
                //if it is the vertical head who has logged in , then redirect to the vertical head update page
                // if($empname == "Vertical Head")
                // {
                //     return view('vh.leadedit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','vert','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12'));
                // }

                // //if the anyone except the vertical head has logged in , redirect to the respective update page
                // else
                // {
                //     return view('cc.edit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1'));
                // }



                $id=$leadid;

                $logged_in_user=Auth::guard('admin')->user()->name;

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
            $em=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $em1=DB::table('employees')->where('id',$em)->get();

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $service_name=DB::table('services')->where('Leadid',$id)->value('ServiceType');

            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');



            if (session()->has('name'))

            {

                $name1=session()->get('name');

            }
            else{
                $name1=Auth::guard('admin')->user()->name;
            }
            $empid=DB::table('employees')->where('FirstName',$name1)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);
            $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$id)->value('ename');


            $p=DB::table('products')->where('Leadid',$id)->value('id');

            $product=product::find(1);
            if($p != NULL)
            {

                $product=product::find($p);
            }


            if (session()->has('name'))
            {

                $name1=session()->get('name');

            }
            else
            {
                $name1=Auth::guard('admin')->user()->name;
            }

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');
            $department3=DB::table('employees')->where('FirstName',$name1)->value('Department3');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                {
                    $designation=$roles;

                    $checkdesig="coordinator";

                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));

                }
                else
                {
                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                    {
                        $designation=$roles;
                        $checkdesig="coordinator";
                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                        {
                            $designation=$roles;
                            $checkdesig="coordinator";
                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                $checkdesig="coordinator";
                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    $checkdesig="coordinator";
                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        $checkdesig="coordinator";
                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            $checkdesig="coordinator";
                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                        }
                                        else
                                        {
                                            if($roles=="Branch Head" && $user_city==$service_city)
                                            {
                                                $designation=$roles;
                                                $checkdesig="branchhead";
                                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="coordinator";
                                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="coordinator";
                                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        if($designation == "Coordinator" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation="coordinator";
                                                            $checkdesig="coordinator";
                                                            // dd($checkdesig);
                                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('/admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            else
            if($designation == "Management")
            {
                $designation="management";
                $checkdesig="management";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                $checkdesig="admin";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            {

                if($roles=="Vertical_ProductSelling" && $user_department==$service_name)
                {
                    $designation=$roles;
                    $checkdesig="vertical";
                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));

                }
                else
                {
                    if($roles=="Vertical_ProductRental" && $user_department==$service_name)
                    {
                        $designation=$roles;
                        $checkdesig="vertical";
                        return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager" && $user_department==$service_name)
                        {
                            $designation=$roles;
                            $checkdesig="vertical";
                            return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager" && $user_department==$service_name)
                            {
                                $designation=$roles;
                                $checkdesig="vertical";
                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$service_name)
                                {
                                    $designation=$roles;
                                    $checkdesig="vertical";
                                   return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$service_name)
                                    {
                                        $designation=$roles;
                                        $checkdesig="vertical";
                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$service_name)
                                        {
                                            $designation=$roles;
                                            $checkdesig="leadeditformobilenumber";
                                           return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                        }
                                        else
                                        {
                                            if($designation == "Branch Head" && $user_city==$service_city )
                                            {
                                                $designation="BranchHead";
                                                $checkdesig="branchhead";
                                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer" && $user_department==$service_name)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="vertical";
                                                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$service_name)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="vertical";
                                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        // dd($roles,$user_department,$service_name);
                                                        if($roles=="vertical" && $user_department==$service_name)
                                                        {
                                                            $designation="vertical";
                                                            $checkdesig="vertical";
                                                           return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('\admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }



            }
            $i=0;

            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$contact;
                    $time = time();
                    $date = date('d/m/Y',$time);
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }

        $phonenumber1 = $altcontact;

        $active_state = DB::table('sms_email_filter')->value('active_state');

        //Message going to alternate number of the customer
        if($altcontact!=NULL && $active_state==0)
        {
            $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
            $phonenimber=$altcontact;

            //grabbing the date for report generation
            $time = time();
            $date = date('m/d/Y',$time);

            //url for sending sms
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&DR=Y&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);

            //grabbing the word "OK" out of the report generated if at all it is there
            $report = substr($curl_scraped_page,0,2);

            //grabbing the job no of the message sent
            $jobno = trim(substr($curl_scraped_page,3));
            curl_close($ch);

            //report generation url
            $report1 = "http://info.bulksms-service.com/SMSCWebService_SMS_GetDR.aspx?user=T201301202115&pwd=Rohan@healthheal&fromdate=".$date."&todate=".$date."&jobno=".$jobno;
            $ch1 =curl_init($report1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page1 =curl_exec($ch1);
            curl_close($ch1);

            //grabbing the term "invalid mobile no" out of the report generated if the mobile is invalied
            $invalidmno = substr($curl_scraped_page1,0,11);

            //checking for invalid mobile no
            if($invalidmno == "Invalid Mno")
            {


                $data = file_get_contents("countrycode.json");

                $country_codes = json_decode($data,true);

                $count = count($country_codes);

                $code = DB::table('leads')->where('id',$id)->value('Country_Code');
                $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

                $data1 = file_get_contents("indian_cities.json");

                $indian_cities = json_decode($data1,true);

                $count1 = count($indian_cities);

                session()->put('name',$who);
                // Session::flash('warning','Customer Mobile number is invalid');

                Session::flash('message', 'Alternate Mobile number is invalid');
                Session::flash('alert-class', 'alert-danger');


                $empname = DB::table('employees')->where('Firstname',$who)->value('Designation');



                //if it is the vertical head who has logged in , then redirect to the vertical head update page
                // if($empname == "Vertical Head")
                // {
                //     return view('vh.leadedit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','vert','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12'));
                // }

                // //if the anyone except the vertical head has logged in , redirect to the respective update page
                // else
                // {
                //     return view('cc.edit',compact('checkbox','code','country_name','count1','country_codes','indian_cities','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1'));
                // }



 $id=$leadid;

                $logged_in_user=Auth::guard('admin')->user()->name;

            $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();
            $emp1=DB::table('employees')->where('Designation','Vertical Head')->get();
            $em=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $em1=DB::table('employees')->where('id',$em)->get();

            $user_department=DB::table('employees')->where('FirstName',$logged_in_user)->value('Department');
            $user_city=DB::table('employees')->where('FirstName',$logged_in_user)->value('City');

            $check_coordinator=DB::table('verticalcoordinations')->where('leadid',$id)->value('empid');
            $coordinator_id=DB::table('employees')->where('FirstName',$logged_in_user)->value('id');

            $service_name=DB::table('services')->where('Leadid',$id)->value('ServiceType');

            $service_city=DB::table('services')->where('Leadid',$id)->value('Branch');



            if (session()->has('name'))

            {

                $name1=session()->get('name');

            }
            else{
                $name1=Auth::guard('admin')->user()->name;
            }
            $empid=DB::table('employees')->where('FirstName',$name1)->select('id')->get();
            $empid=json_decode($empid);
            $eid= $empid[0]->id;
            $emp=DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

            $adminid = DB::table('admins')->where('name',$name1)->value('id');

            $role_adminsid=DB::table('role_admins')->where('admin_id',$adminid)->value('role_id');
            $roles=DB::table('roles')->where('id',$role_adminsid)->value('name');




            $lead = lead::find($id);

            $s=DB::table('services')->where('leadid',$id)->value('id');
            $service=service::find($s);

            $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
            $personneldetail=personneldetail::find($p);

            $a=DB::table('addresses')->where('leadid',$id)->value('id');
            $address=address::find($a);
            $coordinator_name=DB::table('verticalcoordinations')->where('leadid',$id)->value('ename');


            $p=DB::table('products')->where('Leadid',$id)->value('id');

            $product=product::find(1);
            if($p != NULL)
            {

                $product=product::find($p);
            }


            if (session()->has('name'))
            {

                $name1=session()->get('name');

            }
            else
            {
                $name1=Auth::guard('admin')->user()->name;
            }

            $designation=DB::table('employees')->where('FirstName',$name1)->value('Designation');
            $designation3=DB::table('employees')->where('FirstName',$name1)->value('Designation3');
            $designation2=DB::table('employees')->where('FirstName',$name1)->value('Designation2');
            $department2=DB::table('employees')->where('FirstName',$name1)->value('Department2');
            $department3=DB::table('employees')->where('FirstName',$name1)->value('Department3');

            $data = file_get_contents("countrycode.json");

            $country_codes = json_decode($data,true);

            $count = count($country_codes);

            $code = DB::table('leads')->where('id',$id)->value('Country_Code');
            $country_name = DB::table('leads')->where('id',$id)->value('Country_Name');

            $data1 = file_get_contents("indian_cities.json");

            $indian_cities = json_decode($data1,true);

            $count1 = count($indian_cities);

            if($designation == "Coordinator")
            {

                if($roles=="Coordinator_ProductSelling" && $check_coordinator==$coordinator_id)
                {
                    $designation=$roles;

                    $checkdesig="coordinator";

                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));

                }
                else
                {
                    if($roles=="Coordinator_ProductRental" && $check_coordinator==$coordinator_id)
                    {
                        $designation=$roles;
                        $checkdesig="coordinator";
                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                    }
                    else
                    {
                        if($roles=="Coordinator_ProductManager" && $check_coordinator==$coordinator_id)
                        {
                            $designation=$roles;
                            $checkdesig="coordinator";
                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                        }
                        else
                        {
                            if($roles=="Coordinator_PharmacyManager" && $check_coordinator==$coordinator_id)
                            {
                                $designation=$roles;
                                $checkdesig="coordinator";
                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                            }
                            else
                            {
                                if($roles=="Coordinator_Product_Pharmacy" && $check_coordinator==$coordinator_id)
                                {
                                    $designation=$roles;
                                    $checkdesig="coordinator";
                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Coordinator_ProductSelling_Pharmacy" && $check_coordinator==$coordinator_id)
                                    {
                                        $designation=$roles;
                                        $checkdesig="coordinator";
                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Coordinator_ProductRental_Pharmacy" && $check_coordinator==$coordinator_id)
                                        {
                                            $designation=$roles;
                                            $checkdesig="coordinator";
                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                        }
                                        else
                                        {
                                            if($roles=="Branch Head" && $user_city==$service_city)
                                            {
                                                $designation=$roles;
                                                $checkdesig="branchhead";
                                                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Coordinator_FieldOfficer" && $check_coordinator==$coordinator_id)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="coordinator";
                                                    return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Coordinator_FieldExecutive" && $check_coordinator==$coordinator_id)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="coordinator";
                                                        return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        if($designation == "Coordinator" && $check_coordinator==$coordinator_id)
                                                        {
                                                            $designation="coordinator";
                                                            $checkdesig="coordinator";
                                                            // dd($checkdesig);
                                                            return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('/admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            else
            if($designation == "Management")
            {
                $designation="management";
                $checkdesig="management";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            if($designation == "Admin")
            {
                $designation="home";
                $checkdesig="admin";
                return view('admin.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','coordinator_name','designation','checkdesig','checkbox'));
            }
            else
            {

                if($roles=="Vertical_ProductSelling" && $user_department==$service_name)
                {
                    $designation=$roles;
                    $checkdesig="vertical";
                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));

                }
                else
                {
                    if($roles=="Vertical_ProductRental" && $user_department==$service_name)
                    {
                        $designation=$roles;
                        $checkdesig="vertical";
                        return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                    }
                    else
                    {
                        if($roles=="Vertical_ProductManager" && $user_department==$service_name)
                        {
                            $designation=$roles;
                            $checkdesig="vertical";
                            return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                        }
                        else
                        {
                            if($roles=="Vertical_PharmacyManager" && $user_department==$service_name)
                            {
                                $designation=$roles;
                                $checkdesig="vertical";
                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                            }
                            else
                            {
                                if($roles=="Vertical_Product_Pharmacy" && $user_department==$service_name)
                                {
                                    $designation=$roles;
                                    $checkdesig="vertical";
                                   return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                }
                                else
                                {
                                    if($roles=="Vertical_ProductSelling_Pharmacy" && $user_department==$service_name)
                                    {
                                        $designation=$roles;
                                        $checkdesig="vertical";
                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                    }
                                    else
                                    {
                                        if($roles=="Vertical_ProductRental_Pharmacy" && $user_department==$service_name)
                                        {
                                            $designation=$roles;
                                            $checkdesig="leadeditformobilenumber";
                                           return view('vh.edit',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                        }
                                        else
                                        {
                                            if($designation == "Branch Head" && $user_city==$service_city )
                                            {
                                                $designation="BranchHead";
                                                $checkdesig="branchhead";
                                                return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                            }
                                            else
                                            {
                                                if($roles=="Vertical_FieldOfficer" && $user_department==$service_name)
                                                {
                                                    $designation=$roles;
                                                    $checkdesig="vertical";
                                                    return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                }
                                                else
                                                {
                                                    if($roles=="Vertical_FieldExecutive" && $user_department==$service_name)
                                                    {
                                                        $designation=$roles;
                                                        $checkdesig="vertical";
                                                       return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                    }
                                                    else
                                                    {
                                                        // dd($roles,$user_department,$service_name);
                                                        if($roles=="vertical" && $user_department==$service_name)
                                                        {
                                                            $designation="vertical";
                                                            $checkdesig="vertical";
                                                           return view('vh.leadeditformobilenumber',compact('count1','indian_cities','code','country_name','country_codes','count','lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product','emp1','em1','emp12','coordinator_name','designation','checkdesig','vert','checkbox'));
                                                        }
                                                        else
                                                        {
                                                            return redirect('\admin');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }




            }
            $i=0;

            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$name.",Thank you for contacting Health Heal. Your ref id is ".$leadid.". For more info about our Services & Products, pls visit www.healthheal.in.";
                    $phonenimber=$altcontact;
                    $time = time();
                    $date = date('d/m/Y',$time);
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }


        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to customer -- by jatin*/

        /* SMS for Lead Creation by all except Vertical Leads starts here..Message going to vertical head -- by jatin --WORKING*/

        //service type retrieved here
        $service_type = DB::table('services')->where('Leadid',$leadid)->value('ServiceType');


        //vertical head contact number
        $vmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('MobileNumber');

        //vertical alternate contact number
        $valtmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('AlternateNumber');

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($vmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Request Type: ".$service_type."
            Contact: ".$contact."
            Location:".$location;
            $phonenimber=$vmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$vmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }

        // message going to alternate number of vh
        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($valtmob!=NULL && $active_state==0)
        {
            $message="Dear ".$user.",
            A new request is created.

            ID: ".$leadid."
            Name: ".$name."
            Request Type: ".$service_type."
            Contact: ".$contact."
            Location:".$location;
            $phonenimber=$valtmob;
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$user.",
                    A new request is created.

                    ID: ".$leadid."
                    Name: ".$name."
                    Contact: ".$contact."
                    Location:".$location;
                    $phonenimber=$valtmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }
        }

        /* SMS for Lead Creation by all except Vertical Leads ends here..Message going to vertical head -- by jatin --WORKING*/

        /* SMS for Lead Creation by  Vertical Leads starts here..Message going to referal partner -- by jatin --WORKING*/

        $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.".Our Care Team will get in touch with the ".$name.", shortly.
        - Health Heal";
        $phonenimber=$rmob;

        $active_state = DB::table('sms_email_filter')->value('active_state');

        if($phonenimber!=NULL && $active_state==0)
        {
            $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
            $ch =curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_scraped_page =curl_exec($ch);
            $report = substr($curl_scraped_page,0,2);
            curl_close($ch);

            $i=0;
            //sending a message again if not sent first time
            while($report!="OK" || $i<=1)
            {
                if($report!="OK")
                {
                    $message="Dear ".$refer.", Thank you for your reference. Your ref ID is ".$leadid.".Our Care Team will get in touch with the ".$name.", shortly.
                    - Health Heal";
                    $phonenimber=$rmob;
                    $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($phonenimber)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                    $ch =curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $curl_scraped_page =curl_exec($ch);
                    $report = substr($curl_scraped_page,0,2);
                    curl_close($ch);

                }
                $i++;
            }

        }
        // /* SMS for Lead Creation by  Vertical Leads ends here..Message going to referal partner -- by jatin*/


        $active_state = DB::table('sms_email_filter')->value('active_state');


        //if client doesn't give email id
        if($to1!=NULL && $active_state==0)
        {
            Mail::send('mail1', $data1, function($message)use($data1) {
                $message->to($data1['to'], $data1['name'] )->subject
                ('[Happy to Care] Health Heal');
                $message->from('care@healthheal.in','Healthheal');

                // Sending the Mail to different people ends here


            });
        }

        /*
        Code for creation log starts here -- by jatin
        */
        //We are trying to retrive the session id for the person who is logged in


        $log_session_id = DB::table('logs')->where('session_id', session()->getId())->value('session_id');
        $username = DB::table('logs')->where('session_id', session()->getId())->value('name');

        $emp_id = DB::table('employees')->where('FirstName', $username)->value('id');
        $log_id = DB::table('logs')->where('session_id',session()->getId())->value('id');


        $activity = new Activity;
        $activity->emp_id = $emp_id;
        $activity->activity = "Lead Created";
        $activity->activity_time = new \DateTime();
        $activity->log_id = $log_id;
        $activity->lead_id = $leadid;
        $activity->save();




        if($checkbox == "add")
        {

            $products=DB::table('productdetails')->get();
            $name1=DB::table('leads')->where('id',$leadid)->value('createdby');

            session()->put('name',$name1);
            return view("product.addproduct",compact('leadid','products'));

        }


        /*
        Code for creation log ends here -- by jatin
        */

        //retrieve the role name in all the four lines
        $name=DB::table('leads')->where('id',$leadid)->select('createdby','id')->get();
        $name=json_decode($name);
        $name1=$name[0]->createdby;
        $id=$name[0]->id;

        $n=DB::table('admins')->where('name',$name1)->select('id')->get();
        $n=json_decode($n);
        $n1=$n[0]->id;

        $a=DB::table('role_admins')->where('admin_id',$n1)->select('role_id')->get();
        $a=json_decode($a);
        $a1=$a[0]->role_id;

        $b=DB::table('roles')->where('id',$a1)->select('name')->get();
        $b=json_decode($b);
        $b1=$b[0]->name;

        //checking the roles
        // if($b1=="vertical")
        // {
        $reference=DB::table('refers')->get();
        $gender=DB::table('genders')->get();
        $gender1=DB::table('genders')->get();
        $city=DB::table('cities')->get();
        $pcity=DB::table('cities')->get();
        $ecity=DB::table('cities')->get();
        $branch=DB::table('cities')->get();
        $status=DB::table('leadstatus')->get();
        $leadtype=DB::table('leadtypes')->get();
        $condition=DB::table('ptconditions')->get();
        $language=DB::table('languages')->get();
        $relation=DB::table('relationships')->get();
        $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
        $emp=DB::table('employees')->where('Designation','Vertical Head')->get();

        $eid = DB::table('employees')->where('FirstName',$who)->value('id');

        $emp12 = DB::table('employees')->where('Designation','Coordinator')->where('under',$eid)->get();

        $lead = lead::find($id);

        $s=DB::table('services')->where('leadid',$id)->value('id');
        $service=service::find($s);

        $p=DB::table('personneldetails')->where('leadid',$id)->value('id');
        $personneldetail=personneldetail::find($p);

        $a=DB::table('addresses')->where('leadid',$id)->value('id');
        $address=address::find($a);

        $p=DB::table('products')->where('Leadid',$id)->value('id');

        $product=product::find(1);
        if($p != NULL)
        {
            $product=product::find($p);
        }

        //  $leads=DB::table('leads')
        // ->select('services.*','personneldetails.*','addresses.*','leads.*')
        // ->join('personneldetails', 'leads.id', '=', 'personneldetails.Leadid')
        // ->join('addresses', 'leads.id', '=', 'addresses.leadid')
        // ->join('services', 'leads.id', '=', 'services.LeadId')
        // ->orderBy('leads.id', 'DESC')->where('leads.id',$id)
        // ->get();


        //   return view('cc.edit',compact('lead','service','address','personneldetail','reference','gender','gender1','city','pcity','ecity','branch','status','leadtype','condition','language','relation','vertical','emp','product'));
        // }
        //
        //
        // //   return redirect('/vh?name="$name1"');
        // // }
        // else{

        $c=DB::table('employees')->where('FirstName',$request->loginname)->value('Designation');
        if($c == "Coordinator")
        {
            session()->put('name',$name1);
            return redirect('/vh');
        }
        elseif($c == "Admin")
        {
            session()->put('name',$name1);
            return redirect('/cc');
        }
        else
        {
            if($c == "Management")
            {
                session()->put('name',$name1);
                return redirect('/cc');
            }
            else
            {
                if($c== "Vertical Head")
                {
                    //sending a message to the assigned coordinator in the case when number is correct and the vertical head assigns it in the creation part itself
                    if($request->assigned)
                    {
                        /* SMS functionality when Coordinator is assigned in the Creation part only starts here -- by jatin   ---- Working*/
                        //name of coordinator to whom assessment has been assigned
                        $coordname = $request->assigned;

                        //coordinator contact number
                        $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

                        //customer name
                        $name=DB::table('leads')->where('id',$leadid)->value('fName');

                        //customer mobile number
                        $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

                        //location selected during lead creation
                        $location=DB::table('services')->where('leadid',$leadid)->value('Branch');

                        //mail
                        $name=$request->clientfname;
                        $user=$request->assignedto;
                        $contact=$request->clientmob;
                        $location=$request->branch;
                        $who = $request->loginname;
                        $from = DB::table('admins')->where('name',$request->loginname)->value('email');
                        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
                        $to1=$request->clientemail;
                        $to2=DB::table('admins')->where('name',$request->assigned)->value('email');

                        $active_state = DB::table('sms_email_filter')->value('active_state');

                        if($active_state==0)
                        {
                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Contact: ".$contact." Location: ".$location;
                        $phonenimber=$coordmob;
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);

                        $i=0;
                        //sending a message again if not sent first time
                        while($report!="OK" || $i<=1)
                        {
                            if($report!="OK")
                            {
                                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Contact: ".$contact." Location: ".$location;
                                $phonenimber=$coordmob;
                                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                                $ch =curl_init($url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $curl_scraped_page =curl_exec($ch);
                                $report = substr($curl_scraped_page,0,2);
                                curl_close($ch);
                            }
                            $i++;
                        }

                        /* SMS functionality when Coordinator is assigned in the Creation part only ends here -- by jatin*/
                    }
                    }

                    //mail


                    //customer name
                    $name=$request->clientfname;

                    //vertical head name
                    $user=$request->assignedto;

                    //vertical head contact number
                    $vmob = DB::table('employees')->where('FirstName',$request->assignedto)->value('MobileNumber');

                    //customer mobile number
                    $contact=$request->clientmob;

                    //location selected during lead creation
                    $location=$request->branch;

                    //who created the lead
                    $who = $request->loginname;
                    $from = DB::table('admins')->where('name',$request->loginname)->value('email');
                    $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
                    $to1=$request->clientemail;
                    $to2=DB::table('admins')->where('name',$request->assigned)->value('email');

                    //service type retrieval done here
                    $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

                    $data = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to,'who'=>$who,'to1'=>$to2];

                    $data1 = ['leadid'=>$leadid,'name'=>$name,'user'=>$user,'service_type'=>$service_type,'contact'=>$contact,'location'=>$location,'from'=>$from,'to'=>$to1,'who'=>$who];

                    //referal partner name
                    $refer=DB::table('refers')->where('Reference',$request->reference)->value('Reference');

                    //referal partner contact number
                    $rmob = DB::table('refers')->where('Reference',$request->reference)->value('ReferenceContact');

                    $e = DB::table('employees')->where('FirstName',$request->assigned)->value('id');
                    /*check if coordinator is assigned or not */

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($e != NULL && $active_state==0)
                    {
                        DB::table('services')->where('id',$leadid)->update(['ServiceStatus' =>'In Progress' ]);

                        Mail::send('mail', $data, function($message)use($data) {
                            $message->to($data['to1'], $data['user'] )->subject
                            ('Lead ['.$data['leadid'].'] Assigned!!!');
                            $message->from($data['from'],$data['who']);
                        });

                    }
                    /* check end */

                    $active_state = DB::table('sms_email_filter')->value('active_state');

                    if($to!=NULL && $active_state==0)
                    {
                        Mail::send('mail', $data, function($message)use($data) {
                            $message->to($data['to'], $data['user'] )->subject
                            ('Lead ['.$data['leadid'].'] Created!!!');
                            $message->from($data['from'],$data['who']);
                        });
                    }



                            //name of coordinator to whom assessment has been assigned
        $coordname = $request->assigned;

        //coordinator contact number
        $coordmob = DB::table('employees')->where('FirstName',$coordname)->value('MobileNumber');

        //coordinator alternate contact number
        $altcoordmob = DB::table('employees')->where('FirstName',$coordname)->value('AlternateNumber');

        //customer name
        $name=DB::table('leads')->where('id',$leadid)->value('fName');

        //customer mobile number
        $contact=DB::table('leads')->where('id',$leadid)->value('MobileNumber');

        //alternate customer mobile number
        $altcontact=DB::table('leads')->where('id',$leadid)->value('Alternatenumber');

        //location selected during lead creation
        $location=DB::table('services')->where('leadid',$leadid)->value('Branch');


        //mail
        $name=$request->clientfname;
        $user=$request->assignedto;
        $contact=$request->clientmob;
        $location=$request->branch;
        $who = $request->loginname;
        $from = DB::table('admins')->where('name',$request->loginname)->value('email');
        $to = DB::table('admins')->where('name',$request->assignedto)->value('email');
        $to1=$request->clientemail;
        $to2=DB::table('admins')->where('name',$request->assigned)->value('email');



        if($request->assigned)
        {
            /* SMS functionality when Coordinator is assigned in the Creation part only starts here -- by jatin*/

            //service type retrieval done here
            $service_type=DB::table('services')->where('leadid',$leadid)->value('ServiceType');

            $active_state = DB::table('sms_email_filter')->value('active_state');

            if($coordmob!=NULL && $active_state==0)
            {
                // sending message to coordinator
                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                $phonenimber=$coordmob;
                $time = time();
                $date = date('m/d/Y',$time);
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                $report = substr($curl_scraped_page,0,2);
                $jobno = trim(substr($curl_scraped_page,3));
                curl_close($ch);



                $i=0;
                //sending a message to coordinator again if not sent first time
                while($report!="OK" || $i<=1)
                {
                    if($report!="OK")
                    {
                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                        $phonenimber=$coordmob;
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($coordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }

            //Message sending to alternate contact number of Coordinator

            $active_state = DB::table('sms_email_filter')->value('active_state');


            if($altcoordmob!=NULL && $active_state==0)
            {
                // sending message to coordinator
                $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                $phonenimber=$altcoordmob;
                $time = time();
                $date = date('m/d/Y',$time);
                $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($altcoordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                $ch =curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_scraped_page =curl_exec($ch);
                $report = substr($curl_scraped_page,0,2);
                $jobno = trim(substr($curl_scraped_page,3));
                curl_close($ch);



                $i=0;
                //sending a message to coordinator again if not sent first time
                while($report!="OK" || $i<=1)
                {
                    if($report!="OK")
                    {
                        $message="Dear ".$coordname.", A new request is assigned. ID: ".$leadid." Name: ".$name." Request Type: ".$service_type." Contact: ".$contact." Location: ".$location;
                        $phonenimber=$altcoordmob;
                        $url ="http://info.bulksms-service.com/WebServiceSMS.aspx?User=T201301202115&passwd=Rohan@healthheal&mobilenumber=".urlencode($altcoordmob)."&message=".urlencode($message)."&sid=HTHEAL&mtype=N";
                        $ch =curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $curl_scraped_page =curl_exec($ch);
                        $report = substr($curl_scraped_page,0,2);
                        curl_close($ch);
                    }
                    $i++;
                }
            }

            /* SMS functionality when Coordinator is assigned in the Creation part only ends here -- by jatin*/

        }




                    session()->put('name',$name1);
                    return redirect('/vh');


                }
                else
                {
                    if($c == "Marketing")
                    {
                        session()->put('name',$name1);
                        return redirect('/admin/marketing');
                    }
                    else
                    {
                        session()->put('name',$name1);
                        return redirect('/cc');
                    }

                }
            }
            // }


            return redirect("/cc");

            // return redirect('vh/create?id=155&name=Ligo');

            // /Verticalhead/'.$lead->id.'/edit
        }

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }

    public function multiservice(Request $request)
    {
        $i=$request->i;


           $reference=DB::table('refers')->get();
            $gender=DB::table('genders')->get();
            $gender1=DB::table('genders')->get();
            $city=DB::table('cities')->get();
            $pcity=DB::table('cities')->get();
            $ecity=DB::table('cities')->get();
            $branch=DB::table('cities')->get();
            $status=DB::table('leadstatus')->get();
            $leadtype=DB::table('leadtypes')->get();
            $condition=DB::table('ptconditions')->get();
            $language=DB::table('languages')->get();
            $relation=DB::table('relationships')->get();
            $vertical=DB::table('verticals')->select('verticaltype')->distinct()->get();
            $emp=DB::table('employees')->where('Designation','Vertical Head')->get();


        return view('admin.multipleserviceinput',compact('i','reference','gender','relation','city','branch','pcity','ecity','language','gender1','leadtype','condition','vertical','emp','status'));
    }


}
