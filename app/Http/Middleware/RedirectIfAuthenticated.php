<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {


        if (Auth::guard($guard)->check()) {


            foreach (Auth::guard('admin')->user()->role as $role) {
            if($role->name == 'Coordinator'){
                 return redirect('admin/coordinator');
            }elseif($role->name == 'customercare'){
                 return redirect('admin/customercare');
            }elseif($role->name == 'admin'){
                 return redirect('admin/home');
            }elseif($role->name == 'Management'){
                 return redirect('admin/management');
            }elseif($role->name == 'Care_provider'){
                 return redirect('admin/careprovider');
            }elseif($role->name == 'vertical'){
                 return redirect('admin/vertical');
            }
            elseif($role->name == 'super_user'){
                 return redirect('admin/superuser');
            }elseif($role->name == 'Referal_Partner'){
                 return redirect('admin/referalpartner');
            }elseif($role->name == 'Pharmacy Manager'){
                 return redirect('admin/pharmacymanager');
            }elseif($role->name == 'Product Manager'){
                 return redirect('admin/productmanager');
            }elseif($role->name == 'Field Officer'){
                 return redirect('admin/fieldofficer');
            }elseif($role->name == 'Field Executive'){
                 return redirect('admin/fieldexecutive');
            }elseif($role->name == 'Vertical_ProductManager'){
                 return redirect('admin/Vertical_ProductManager');
            }elseif($role->name == 'Vertical_PharmacyManager'){
                 return redirect('admin/Vertical_PharmacyManager');
            }elseif($role->name == 'Vertical_ProductSelling'){
                 return redirect('admin/Vertical_ProductSelling');
            }elseif($role->name == 'Vertical_ProductRental'){
                 return redirect('admin/Vertical_ProductRental');
            }elseif($role->name == 'Vertical_Product_Pharmacy'){
                 return redirect('admin/Vertical_Product_Pharmacy');
            }elseif($role->name == 'Vertical_ProductSelling_Pharmacy'){
                 return redirect('admin/Vertical_ProductSelling_Pharmacy');
            }elseif($role->name == 'Vertical_ProductRental_Pharmacy'){
                 return redirect('admin/Vertical_ProductRental_Pharmacy');
            }elseif($role->name == 'Coordinator_ProductManager'){
                 return redirect('admin/Coordinator_ProductManager');
            }elseif($role->name == 'Coordinator_PharmacyManager'){
                 return redirect('admin/Coordinator_PharmacyManager');
            }elseif($role->name == 'Coordinator_ProductSelling'){
                 return redirect('admin/Coordinator_ProductSelling');
            }elseif($role->name == 'Coordinator_ProductRental'){
                 return redirect('admin/Coordinator_ProductRental');
            }elseif($role->name == 'Coordinator_Product_Pharmacy'){
                 return redirect('admin/Coordinator_Product_Pharmacy');
            }elseif($role->name == 'Coordinator_ProductSelling_Pharmacy'){
                 return redirect('admin/Coordinator_ProductSelling_Pharmacy');
            }elseif($role->name == 'Coordinator_ProductRental_Pharmacy'){
                 return redirect('admin/Coordinator_ProductRental_Pharmacy');
            }elseif($role->name == 'Vertical_FieldOfficer'){
                 return redirect('admin/Vertical_FieldOfficer');
            }elseif($role->name == 'Vertical_FieldExecutive'){
                 return redirect('admin/Vertical_FieldExecutive');
            }elseif($role->name == 'Coordinator_FieldOfficer'){
                 return redirect('admin/Coordinator_FieldOfficer');
            }elseif($role->name == 'Coordinator_FieldExecutive'){
                 return redirect('admin/Coordinator_FieldExecutive');
            }elseif($role->name == 'Branch Head'){
                 return redirect('admin/BranchHead');
            }
        }


        }

        return $next($request);
    }
}
