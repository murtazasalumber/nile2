<!-- This file has been created for reference for all to know which blade view represents what -->

<!-- View Leads for coordinator -->
 co/index.blade.php

<!-- View Leads for customer care -->
cc/index.blade.php

<!-- View Leads for admin -->
admin/index.blade.php

<!-- View Leads for Product Manager -->
product/productindex.blade.php

<!-- View Leads for Pharmacy Manager -->
product/pharmacyindex.blade.php

<!-- View Leads for management -->
management/index.blade.php

<!-- View Leads for fieldexecutive and fieldofficer-->
fieldexecutive/index.blade.php

<!-- View Leads for fieldexecutive and fieldofficer-->
fieldexecutive/index.blade1.php

<!-- View Leads for Customer care -->
cc/index.blade.php

<!-- Dashboard for admin -->
admin/home.blade.php

<!-- Dashboard for coordinator -->
admin/coordinator.blade.php

<!-- Dashboard for customer care -->
admin/customercare.blade.php

<!-- Dashboard for management -->
admin/management1.blade.php

<!-- Dashboard for product Manager -->
admin/productmanager.blade.php

<!-- Dashboard for pharmacy Manager -->
admin/pharmacymanager.blade.php

<!-- Dashboard for field Executive -->
admin/fieldexecutive.blade.php

<!-- Dashboard for field officer -->
admin/fieldofficer.blade.php

<!-- View page when Admin/Management selects Branch, Vertical, Start and End date -->
admin/allfilter.blade.php

<!-- View page when Admin/Management selects the Branch and the Vertical -->
admin/branchtoverticalfilter.blade.php

<!-- View page when Admin/Management selects the Timeline and  Vertical -->
admin/verticaltotimeline.blade.php

<!-- View page when Admin/Management selects the Branch and the Timeline -->
admin/branchtotimeline.blade.php

<!-- View page when Admin/Management selects the Branch only-->
admin/Branchfilteralone.blade.php

<!-- View page when Admin/Management selects the Vertical only  -->
admin/Verticalfilteralone.blade.php

<!-- View page when Admin/Management selects the Timeline alone -->
admin/timelineonly.blade.php

<!-- View page when each of the statues are clikcked after graph filtering -->
admin/indexfilter.blade.php

<!-- SearchFilter for all scenarios (Graph filtered data and Normal View leads data) -->
verticalheads/alldata.blade.php
cc/alldata.blade.php

<!-- Lead Registration form for customer care -->
cc/create.blade.php

<!-- Lead Registration form for management -->
management/leads.blade.php

<!-- Lead Registration form for Admin -->
admin/lead.blade.php

<!-- Lead Registration form for coordinator -->
co/lead.blade.php
